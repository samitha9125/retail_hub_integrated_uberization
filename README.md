#### Create React Native App | DialogSARN ####

##Install React Native

npm install -g npm@latest-4
npm install -g react-native-cli
npm install -g react-devtools
npm install -g yarn
npm install -g yo generator-rn-toolbox
npm install -g react-native-rename 
npm install -g eslint

##Create Project
react-native init DialogRetailerApp
cd DialogSARN

##Run App on Android
react-native run-android

##Install ESlint (IDE config - install as Admin)

npm install --save-dev eslint
npm install --save-dev eslint-config-rallycoding
npm install --save-dev reactotron-react-native
npm install --save-dev react-devtools


##Rename With custom Bundle Identifier (Android)
react-native-rename DialogRetailerApp -b net.omobio.dialog.dialogra
npm start --reset-cache

##Using React Native Toolbox

#change launcher icon
yo rn-toolbox:assets --icon images\launcher\icon_sa2.png

##add splash screen
yo rn-toolbox:assets --splash images\splash\splash.png 

##Other dependasis

npm install --save axios
npm install --save react-native-svg 
npm install --save react-native-vector-icons
npm install --save react-native-progress 
npm install --save react-native-check-box 
npm install --save react-native-simple-radio-button
npm install --save react-native-swiper 
npm install --save redux
npm install --save react-redux
npm install --save redux-thunk
npm install --save immutability-helper 
npm install --save react-native-camera 
npm install --save react-native-localization 
npm install --save-dev reactotron-react-native
npm install --save react-native-firebase
react-native link react-native-firebase
npm install --save react-native-device-info
react-native link react-native-device-info
npm install --save react-native-sim-data
react-native link react-native-sim-data
npm --save install react-native-fs 
react-native link react-native-fs
npm install --save react-native-rn-modal-dropdown
npm install --save react-native-orientation
npm install --save react-native-tab-view
npm install --save react-native-android-sms-listener
npm install --save react-native-sms-retriever
npm install --save jest-cli
npm uninstall reactotron-react-native

##DevTools
npm install --save-dev babel-eslint
npm install --save-dev eslint-plugin-react
npm install --save-dev eslint-plugin-react-native
npm install --save-dev redux-devtools-extension
npm install --save-dev reactotron-react-native

##Build Release

cd android  
gradlew clean
gradlew assembleRelease

##Build Release Mac OSX

cd android  
./gradlew clean
./gradlew assembleRelease

react-native start --reset-cache

react-native run-android

##Enable Analytics Debug mode

adb shell setprop debug.firebase.analytics.app net.omobio.dialog.dialogra

This behavior persists until you explicitly disable Debug mode by executing the following command line.

adb shell setprop debug.firebase.analytics.app .none.


##React Native Open Developer Menu

adb shell input keyevent KEYCODE_MENU

##Create React Native Module

npm install -g react-native-create-library
react-native new-library --name react-native-imageuploader

react-native-create-library --version 1.0.0 --package-identifier net.omobio.reactnative.imageuploader --platforms android react-native-imageuploader


