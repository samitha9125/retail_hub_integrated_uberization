package com.scanlibrary;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;

/**
 * Created by jhansi on 05/04/15.
 */
public class Utils {
    private static final String TAG = "Utils";

    private static final int MAX_IMAGE_HEIGHT = 1000;
	
    private Utils() {

    }

    public static void saveBitmap(Context context, Bitmap bitmap, Uri uri) {
        Log.e(TAG, "saveBitmap");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap = resizeBitmap(bitmap);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bytes);

        File file = new File(uri.getPath());

        try {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(bytes.toByteArray());
            } catch (IOException e) {
                Log.e(TAG, "IO Exception", e);
            }

            try {
                fos.close();
            } catch (IOException e) {
                Log.e(TAG, "IO Exception", e);
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found", e);
        }
    }

    public static Bitmap getBitmap(Context context, Uri uri) throws IOException {
        Log.e(TAG, "getBitmap");

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        Bitmap bitmap = BitmapFactory.decodeFile(uri.getPath(), bmOptions);

        return bitmap;
    }

    public static Bitmap resizeBitmap(Bitmap source) {
        Log.e(TAG, "resizeBitmap");

        int height = source.getHeight();

        Log.e(TAG, "resizeBitmap height: " + height);

        if (height > MAX_IMAGE_HEIGHT) {
            int width = source.getWidth() * MAX_IMAGE_HEIGHT / height;
            height = MAX_IMAGE_HEIGHT;

            source = Bitmap.createScaledBitmap(source, width, height, true);
        }

        Log.e(TAG, "resizeBitmap height2: " + source.getHeight());

        return source;
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}