package net.omobio.docscanner;

import android.support.annotation.Nullable;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.*;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.List;
import java.util.ArrayList;

public class DSCameraViewManager extends ViewGroupManager<DSCameraView> {
    private static final String REACT_CLASS = "DSCamera";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public DSCameraView createViewInstance(ThemedReactContext context) {
        return new DSCameraView(context);
    }

    @ReactProp(name = "aspect")
    public void setAspect(DSCameraView view, int aspect) {
        view.setAspect(aspect);
    }

    @ReactProp(name = "captureMode")
    public void setCaptureMode(DSCameraView view, final int captureMode) {
        // Note that this in practice only performs any additional setup necessary for each mode;
        // the actual indication to capture a still or record a video when capture() is called is
        // still ultimately decided upon by what it in the options sent to capture().
        view.setCaptureMode(captureMode);
    }

    @ReactProp(name = "captureTarget")
    public void setCaptureTarget(DSCameraView view, int captureTarget) {
        // No reason to handle this props value here since it's passed again to the DSCameraModule capture method
    }

    @ReactProp(name = "type")
    public void setType(DSCameraView view, int type) {
        view.setCameraType(type);
    }

    @ReactProp(name = "captureQuality")
    public void setCaptureQuality(DSCameraView view, String captureQuality) {
        view.setCaptureQuality(captureQuality);
    }

    @ReactProp(name = "torchMode")
    public void setTorchMode(DSCameraView view, int torchMode) {
        view.setTorchMode(torchMode);
    }

    @ReactProp(name = "flashMode")
    public void setFlashMode(DSCameraView view, int flashMode) {
        view.setFlashMode(flashMode);
    }

    @ReactProp(name = "orientation")
    public void setOrientation(DSCameraView view, int orientation) {
        view.setOrientation(orientation);
    }

    @ReactProp(name = "captureAudio")
    public void setCaptureAudio(DSCameraView view, boolean captureAudio) {
        // TODO - implement video mode
    }

    @ReactProp(name = "imageProcessorEnabled")
    public void setImageProcessorEnabled(DSCameraView view, boolean imageProcessorEnabled) {
        view.setImageProcessorEnabled(imageProcessorEnabled);
    }

    @ReactProp(name = "imageType")
    public void setImageType(DSCameraView view, String imageType) {
        if (imageType == null) {
            return;
        }

        view.setImageType(imageType);
    }
}
