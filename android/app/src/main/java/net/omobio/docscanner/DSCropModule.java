/**
 * Created by Fabrice Armisen (farmisen@gmail.com) on 1/4/16.
 * Android video recording support by Marc Johnson (me@marc.mn) 4/2016
 */

package net.omobio.docscanner;

import android.hardware.Camera;
import android.media.MediaActionSound;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

//public class DSCameraModule extends ReactContextBaseJavaModule implements IScanner, ComponentCallbacks2,
public class DSCropModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static final String TAG = "DSCameraModule";

    public static final int DS_CROP_OTHER = 0;
    public static final int DS_CROP_NIC = 1;
    public static final int DS_CROP_POB = 2;

    private static ReactApplicationContext _reactContext;
    private DSSensorOrientationChecker _sensorOrientationChecker;

    private Promise mRecordingPromise = null;

    public DSCropModule(ReactApplicationContext reactContext) {
        super(reactContext);

        _reactContext = reactContext;
        _sensorOrientationChecker = new DSSensorOrientationChecker(_reactContext);
        _reactContext.addLifecycleEventListener(this);
    }

    public static ReactApplicationContext getReactContextSingleton() {
      return _reactContext;
    }

    @Override
    public String getName() {
        return "DSCropModule";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        return Collections.unmodifiableMap(new HashMap<String, Object>() {
            {
                put("ImageType", getImageTypeConstants());
            }

            private Map<String, Object> getImageTypeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("other", DS_CROP_OTHER);
                        put("nic", DS_CROP_NIC);
                        put("pob", DS_CROP_POB);
                    }
                });
            }
        });
    }

    @ReactMethod
    public void process(final ReadableMap options, final Promise promise) {
        Log.d(TAG, "xxxx process 1");

        int orientation = options.hasKey("orientation") ? options.getInt("orientation") : DSCamera.getInstance().getOrientation();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                processImage(options, promise);
            }
        });
    }

    /**
     * synchronized in order to prevent the user crashing the app by taking many photos and them all being processed
     * concurrently which would blow the memory (esp on smaller devices), and slow things down.
     */
    private synchronized void processImage(ReadableMap options, Promise promise) {
        Log.d(TAG, "xxxx processImage");

        boolean shouldFixOrientation = options.hasKey("fixOrientation") && options.getBoolean("fixOrientation");
        if(shouldFixOrientation) {
            try {
                Log.d(TAG, "xxxx try");
            } catch (Exception e) {
                promise.reject("Error fixing orientation image", e);
            }
        }

        WritableMap response = new WritableNativeMap();
        response.putString("data", "OK");
        promise.resolve(response);
    }

    /**
     * LifecycleEventListener overrides
     */
    @Override
    public void onHostResume() {
        Log.d(TAG, "onHostResume");
        // ... do nothing
    }

    @Override
    public void onHostPause() {
        Log.d(TAG, "onHostPause");
        // On pause, we stop any pending recording session
        if (mRecordingPromise != null) {
        }
    }

    @Override
    public void onHostDestroy() {
        Log.d(TAG, "onHostDestroy");
        // ... do nothing
    }
}
