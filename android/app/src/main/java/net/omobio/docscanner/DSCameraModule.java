/**
 * Created by Fabrice Armisen (farmisen@gmail.com) on 1/4/16.
 * Android video recording support by Marc Johnson (me@marc.mn) 4/2016
 */

package net.omobio.docscanner;

import android.content.Context;
import android.app.Activity;
import android.content.Intent;
import android.content.ContentValues;
import android.hardware.Camera;
import android.media.*;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Surface;
import android.database.Cursor;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.scanlibrary.ScanConstants;

//public class DSCameraModule extends ReactContextBaseJavaModule implements IScanner, ComponentCallbacks2,
public class DSCameraModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static final String TAG = "DSCameraModule";

    public static final int DS_CAMERA_ASPECT_FILL = 0;
    public static final int DS_CAMERA_ASPECT_FIT = 1;
    public static final int DS_CAMERA_ASPECT_STRETCH = 2;
    public static final int DS_CAMERA_CAPTURE_MODE_STILL = 0;
    public static final int DS_CAMERA_CAPTURE_MODE_VIDEO = 1;
    public static final int DS_CAMERA_CAPTURE_TARGET_MEMORY = 0;
    public static final int DS_CAMERA_CAPTURE_TARGET_DISK = 1;
    public static final int DS_CAMERA_CAPTURE_TARGET_CAMERA_ROLL = 2;
    public static final int DS_CAMERA_CAPTURE_TARGET_TEMP = 3;
    public static final int DS_CAMERA_ORIENTATION_AUTO = Integer.MAX_VALUE;
    public static final int DS_CAMERA_ORIENTATION_PORTRAIT = Surface.ROTATION_0;
    public static final int DS_CAMERA_ORIENTATION_PORTRAIT_UPSIDE_DOWN = Surface.ROTATION_180;
    public static final int DS_CAMERA_ORIENTATION_LANDSAPE_LEFT = Surface.ROTATION_90;
    public static final int DS_CAMERA_ORIENTATION_LANDSAPE_RIGHT = Surface.ROTATION_270;
    public static final int DS_CAMERA_TYPE_FRONT = 1;
    public static final int DS_CAMERA_TYPE_BACK = 2;
    public static final int DS_CAMERA_FLASH_MODE_OFF = 0;
    public static final int DS_CAMERA_FLASH_MODE_ON = 1;
    public static final int DS_CAMERA_FLASH_MODE_AUTO = 2;
    public static final int DS_CAMERA_TORCH_MODE_OFF = 0;
    public static final int DS_CAMERA_TORCH_MODE_ON = 1;
    public static final int DS_CAMERA_TORCH_MODE_AUTO = 2;
    public static final String DS_CAMERA_CAPTURE_QUALITY_PREVIEW = "preview";
    public static final String DS_CAMERA_CAPTURE_QUALITY_HIGH = "high";
    public static final String DS_CAMERA_CAPTURE_QUALITY_MEDIUM = "medium";
    public static final String DS_CAMERA_CAPTURE_QUALITY_LOW = "low";
    public static final String DS_CAMERA_CAPTURE_QUALITY_1080P = "1080p";
    public static final String DS_CAMERA_CAPTURE_QUALITY_720P = "720p";
    public static final String DS_CAMERA_CAPTURE_QUALITY_480P = "480p";
    public static final int DS_CROP_OTHER = 0;
    public static final int DS_CROP_NIC = 1;
    public static final int DS_CROP_POB = 2;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private static ReactApplicationContext _reactContext;
    private DSSensorOrientationChecker _sensorOrientationChecker;
    private MediaActionSound sound = new MediaActionSound();

    private MediaRecorder mMediaRecorder;
    private long MRStartTime;
    private File mVideoFile;
    private Camera mCamera = null;
    private Promise mRecordingPromise = null;
    private ReadableMap mRecordingOptions;
    private Boolean mSafeToCapture = true;
    private Promise capturePromise;

    public DSCameraModule(ReactApplicationContext reactContext) {
        super(reactContext);

        _reactContext = reactContext;
        _sensorOrientationChecker = new DSSensorOrientationChecker(_reactContext);
        _reactContext.addLifecycleEventListener(this);
        sound.load(MediaActionSound.SHUTTER_CLICK);
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            Log.d(TAG, "xxxx ActivityEventListener");

            mSafeToCapture = true;

            if (intent != null) {
                Uri uri = intent.getParcelableExtra(ScanConstants.SCANNED_RESULT);
                if (uri != null) {
                    Log.d(TAG, "xxxx ActivityEventListener " + uri.toString());
                    final WritableMap response = new WritableNativeMap();
                    response.putString("uri", uri.toString());
                    capturePromise.resolve(response);
                } else {
                    Log.d(TAG, "xxxx ActivityEventListener uri == null");
                    capturePromise.reject("Cancelled", "Cancelled 1");
                }
            } else {
                Log.d(TAG, "xxxx ActivityEventListener intent == null");
                // Stay in Camera view
                //capturePromise.reject("Cancelled", "Cancelled 2");
            }
        }
    };

    public static ReactApplicationContext getReactContextSingleton() {
      return _reactContext;
    }

    @Override
    public String getName() {
        return "DSCameraModule";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        return Collections.unmodifiableMap(new HashMap<String, Object>() {
            {
                put("Aspect", getAspectConstants());
                put("BarCodeType", getBarCodeConstants());
                put("Type", getTypeConstants());
                put("CaptureQuality", getCaptureQualityConstants());
                put("CaptureMode", getCaptureModeConstants());
                put("CaptureTarget", getCaptureTargetConstants());
                put("Orientation", getOrientationConstants());
                put("FlashMode", getFlashModeConstants());
                put("TorchMode", getTorchModeConstants());
            }

            private Map<String, Object> getAspectConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("stretch", DS_CAMERA_ASPECT_STRETCH);
                        put("fit", DS_CAMERA_ASPECT_FIT);
                        put("fill", DS_CAMERA_ASPECT_FILL);
                    }
                });
            }

            private Map<String, Object> getBarCodeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        // @TODO add barcode types
                    }
                });
            }

            private Map<String, Object> getTypeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("front", DS_CAMERA_TYPE_FRONT);
                        put("back", DS_CAMERA_TYPE_BACK);
                    }
                });
            }

            private Map<String, Object> getCaptureQualityConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("low", DS_CAMERA_CAPTURE_QUALITY_LOW);
                        put("medium", DS_CAMERA_CAPTURE_QUALITY_MEDIUM);
                        put("high", DS_CAMERA_CAPTURE_QUALITY_HIGH);
                        put("photo", DS_CAMERA_CAPTURE_QUALITY_HIGH);
                        put("preview", DS_CAMERA_CAPTURE_QUALITY_PREVIEW);
                        put("480p", DS_CAMERA_CAPTURE_QUALITY_480P);
                        put("720p", DS_CAMERA_CAPTURE_QUALITY_720P);
                        put("1080p", DS_CAMERA_CAPTURE_QUALITY_1080P);
                    }
                });
            }

            private Map<String, Object> getCaptureModeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("still", DS_CAMERA_CAPTURE_MODE_STILL);
                        put("video", DS_CAMERA_CAPTURE_MODE_VIDEO);
                    }
                });
            }

            private Map<String, Object> getCaptureTargetConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("memory", DS_CAMERA_CAPTURE_TARGET_MEMORY);
                        put("disk", DS_CAMERA_CAPTURE_TARGET_DISK);
                        put("cameraRoll", DS_CAMERA_CAPTURE_TARGET_CAMERA_ROLL);
                        put("temp", DS_CAMERA_CAPTURE_TARGET_TEMP);
                    }
                });
            }

            private Map<String, Object> getOrientationConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("auto", DS_CAMERA_ORIENTATION_AUTO);
                        put("landscapeLeft", DS_CAMERA_ORIENTATION_LANDSAPE_LEFT);
                        put("landscapeRight", DS_CAMERA_ORIENTATION_LANDSAPE_RIGHT);
                        put("portrait", DS_CAMERA_ORIENTATION_PORTRAIT);
                        put("portraitUpsideDown", DS_CAMERA_ORIENTATION_PORTRAIT_UPSIDE_DOWN);
                    }
                });
            }

            private Map<String, Object> getFlashModeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("off", DS_CAMERA_FLASH_MODE_OFF);
                        put("on", DS_CAMERA_FLASH_MODE_ON);
                        put("auto", DS_CAMERA_FLASH_MODE_AUTO);
                    }
                });
            }

            private Map<String, Object> getTorchModeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("off", DS_CAMERA_TORCH_MODE_OFF);
                        put("on", DS_CAMERA_TORCH_MODE_ON);
                        put("auto", DS_CAMERA_TORCH_MODE_AUTO);
                    }
                });
            }

            private Map<String, Object> getCropTypeConstants() {
                return Collections.unmodifiableMap(new HashMap<String, Object>() {
                    {
                        put("other", DS_CROP_OTHER);
                        put("nic", DS_CROP_NIC);
                        put("pob", DS_CROP_POB);
                    }
                });
            }
        });
    }

    public static byte[] convertFileToByteArray(File f)
    {
        Log.d(TAG, "xxxx convertFileToByteArray");

        byte[] byteArray = null;
        try
        {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024*8];
            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return byteArray;
    }

    @ReactMethod
    public void capture(final ReadableMap options, final Promise promise) {
        Log.d(TAG, "xxxx capture");

        if (!mSafeToCapture)
            return;

        int orientation = options.hasKey("orientation") ? options.getInt("orientation") : DSCamera.getInstance().getOrientation();

        capturePromise = promise;

        if (orientation == DS_CAMERA_ORIENTATION_AUTO) {
            _sensorOrientationChecker.onResume();
            _sensorOrientationChecker.registerOrientationListener(new DSSensorOrientationListener() {
                @Override
                public void orientationEvent() {
                    int deviceOrientation = _sensorOrientationChecker.getOrientation();
                    _sensorOrientationChecker.unregisterOrientationListener();
                    _sensorOrientationChecker.onPause();
                    captureWithOrientation(options, promise, deviceOrientation);
                }
            });
        } else {
            captureWithOrientation(options, promise, orientation);
        }
    }

    private void captureWithOrientation(final ReadableMap options, final Promise promise, int deviceOrientation) {
        Log.d(TAG, "xxxx captureWithOrientation");

        Camera camera = DSCamera.getInstance().acquireCameraInstance(options.getInt("type"));
        if (null == camera) {
            promise.reject("No camera found.");
            return;
        }

        DSCamera.getInstance().setCaptureQuality(options.getInt("type"), options.getString("quality"));

        if (options.hasKey("playSoundOnCapture") && options.getBoolean("playSoundOnCapture")) {
            sound.play(MediaActionSound.SHUTTER_CLICK);
        }

        if (options.hasKey("quality")) {
            DSCamera.getInstance().setCaptureQuality(options.getInt("type"), options.getString("quality"));
        }

        DSCamera.getInstance().adjustCameraRotationToDeviceOrientation(options.getInt("type"), deviceOrientation);
        camera.setPreviewCallback(null);

        Camera.PictureCallback captureCallback = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(final byte[] data, Camera camera) {
                camera.stopPreview();
                //camera.startPreview();

                android.hardware.Camera.Size pictureSize = camera.getParameters().getPictureSize();

                Log.d(TAG, "xxxx onPictureTaken - received image " + pictureSize.width + "x" + pictureSize.height);

                Mat mat = new Mat(new Size(pictureSize.width, pictureSize.height), CvType.CV_8U);
                mat.put(0, 0, data);

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        processImage(new MutableImage(data), options, promise);
                    }
                });
            }
        };

        try {
            camera.takePicture(null, null, captureCallback);
        } catch(RuntimeException ex) {
            Log.e(TAG, "Couldn't capture photo.", ex);
        }
    }

    /**
     * synchronized in order to prevent the user crashing the app by taking many photos and them all being processed
     * concurrently which would blow the memory (esp on smaller devices), and slow things down.
     */
    private synchronized void processImage(MutableImage mutableImage, ReadableMap options, Promise promise) {
        Log.d(TAG, "xxxx processImage");

        boolean shouldFixOrientation = options.hasKey("fixOrientation") && options.getBoolean("fixOrientation");
        if(shouldFixOrientation) {
            try {
                mutableImage.fixOrientation();
            } catch (MutableImage.ImageMutationFailedException e) {
                promise.reject("Error fixing orientation image", e);
            }
        }

        int jpegQualityPercent = 80;
        if(options.hasKey("jpegQuality")) {
            jpegQualityPercent = options.getInt("jpegQuality");
        }

        switch (options.getInt("target")) {
            case DS_CAMERA_CAPTURE_TARGET_MEMORY:
                Log.d(TAG, "xxxx DS_CAMERA_CAPTURE_TARGET_MEMORY");

                String encoded = mutableImage.toBase64(jpegQualityPercent);
                WritableMap response = new WritableNativeMap();
                response.putString("data", encoded);
                promise.resolve(response);
                break;
            case DS_CAMERA_CAPTURE_TARGET_CAMERA_ROLL: {
                Log.d(TAG, "xxxx DS_CAMERA_CAPTURE_TARGET_CAMERA_ROLL");

                File cameraRollFile = getOutputCameraRollFile(MEDIA_TYPE_IMAGE);
                if (cameraRollFile == null) {
                    promise.reject("Error creating media file.");
                    return;
                }

                try {
                    mutableImage.writeDataToFile(cameraRollFile, options, jpegQualityPercent);
                } catch (IOException | NullPointerException e) {
                    promise.reject("failed to save image file", e);
                    return;
                }

                addToMediaStore(cameraRollFile.getAbsolutePath());

                resolveImage(cameraRollFile, promise, true);

                break;
            }
            case DS_CAMERA_CAPTURE_TARGET_DISK: {
                Log.d(TAG, "xxxx DS_CAMERA_CAPTURE_TARGET_DISK");

                File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                if (pictureFile == null) {
                    promise.reject("Error creating media file.");
                    return;
                }

                try {
                    mutableImage.writeDataToFile(pictureFile, options, 85);
                } catch (IOException e) {
                    promise.reject("failed to save image file", e);
                    return;
                }

                resolveImage(pictureFile, promise, false);

                break;
            }
            case DS_CAMERA_CAPTURE_TARGET_TEMP: {
                Log.d(TAG, "xxxx DS_CAMERA_CAPTURE_TARGET_TEMP");

                File tempFile = getTempMediaFile(MEDIA_TYPE_IMAGE);
                if (tempFile == null) {
                    promise.reject("Error creating media file.");
                    return;
                }

                try {
                    mutableImage.writeDataToFile(tempFile, options, 85);
                } catch (IOException e) {
                    promise.reject("failed to save image file", e);
                    return;
                }

                resolveImage(tempFile, promise, false);

                break;
            }
        }
    }

    @ReactMethod
    public void stopCapture(final Promise promise) {
        Log.d(TAG, "xxxx stopCapture");

        if (mRecordingPromise != null) {
            promise.resolve("Finished recording.");
        } else {
            promise.resolve("Not recording.");
        }
    }

    @ReactMethod
    public void hasFlash(ReadableMap options, final Promise promise) {
        Camera camera = DSCamera.getInstance().acquireCameraInstance(options.getInt("type"));
        if (null == camera) {
            promise.reject("No camera found.");
            return;
        }
        List<String> flashModes = camera.getParameters().getSupportedFlashModes();
        promise.resolve(null != flashModes && !flashModes.isEmpty());
    }

    private File getOutputMediaFile(int type) {
        // Get environment directory type id from requested media type.
        String environmentDirectoryType;

        if (type == MEDIA_TYPE_IMAGE) {
            environmentDirectoryType = Environment.DIRECTORY_PICTURES;
        } else {
            Log.e(TAG, "Unsupported media type:" + type);
            return null;
        }

        return getOutputFile(
                type,
                Environment.getExternalStoragePublicDirectory(environmentDirectoryType)
        );
    }

    private File getOutputCameraRollFile(int type) {
        return getOutputFile(
                type,
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
        );
    }

    private File getOutputFile(int type, File storageDir) {
        // Create the storage directory if it does not exist
        if (!storageDir.exists()) {
            if (!storageDir.mkdirs()) {
                Log.e(TAG, "failed to create directory:" + storageDir.getAbsolutePath());
                return null;
            }
        }

        // Create a media file name
        String fileName = String.format("%s", new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));

        if (type == MEDIA_TYPE_IMAGE) {
            fileName = String.format("IMG_%s.jpg", fileName);
        } else {
            Log.e(TAG, "Unsupported media type:" + type);
            return null;
        }

        return new File(String.format("%s%s%s", storageDir.getPath(), File.separator, fileName));
    }

    private File getTempMediaFile(int type) {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File outputDir = _reactContext.getCacheDir();
            File outputFile;

            if (type == MEDIA_TYPE_IMAGE) {
                outputFile = File.createTempFile("IMG_" + timeStamp, ".jpg", outputDir);
            } else {
                Log.e(TAG, "Unsupported media type:" + type);
                return null;
            }
            return outputFile;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    private void addToMediaStore(String path) {
        MediaScannerConnection.scanFile(_reactContext, new String[] { path }, null, null);
    }

    /**
     * LifecycleEventListener overrides
     */
    @Override
    public void onHostResume() {
        Log.d(TAG, "xxxx onHostResume");
        // ... Start camera preview

        /*
        DSCamera dsCamera = DSCamera.getInstance();

        if (dsCamera != null) {
            Log.d(TAG, "xxxx onHostResume DSCamera != null");

            Camera camera = dsCamera.acquireCameraInstance(DS_CAMERA_TYPE_BACK);
            if (camera != null) {
                camera.startPreview();
            } else {
                Log.d(TAG, "xxxx onHostResume Camera == null");
                capturePromise.reject("Cancelled", "No camera found.");
            }
        } else {
            Log.d(TAG, "xxxx onHostResume DSCamera == null");
        }
        */
    }

    @Override
    public void onHostPause() {
        Log.d(TAG, "xxxx onHostPause");
        // On pause, we stop Camera preview

        /*
        DSCamera dsCamera = DSCamera.getInstance();

        if (dsCamera != null) {
            Log.d(TAG, "xxxx onHostPause DSCamera != null");

            Camera camera = dsCamera.acquireCameraInstance(DS_CAMERA_TYPE_BACK);
            if (camera != null) {
                camera.stopPreview();
            } else {
                Log.d(TAG, "xxxx onHostPause Camera == null");
                capturePromise.reject("Cancelled", "No camera found.");
            }
        } else {
            Log.d(TAG, "xxxx onHostPause DSCamera == null");
        }
        */
    }

    @Override
    public void onHostDestroy() {
        Log.d(TAG, "xxxx onHostDestroy");
        // ... do nothing
    }

    private void resolveImage(final File imageFile, final Promise promise, boolean addToMediaStore) {
        Log.d(TAG, "xxxx resolveImage");

        final WritableMap response = new WritableNativeMap();
        String imagePath = Uri.fromFile(imageFile).toString();

        Log.d(TAG, "xxxx resolveImage URI: " + imagePath);

        Intent intentCapture = new Intent(getReactApplicationContext(), com.scanlibrary.ScanActivity.class);
        intentCapture.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, ScanConstants.OPEN_CAMERA);
        intentCapture.putExtra("IMAGE_URI", imagePath);
        getReactApplicationContext().startActivityForResult(intentCapture, 0, null);
    }

}
