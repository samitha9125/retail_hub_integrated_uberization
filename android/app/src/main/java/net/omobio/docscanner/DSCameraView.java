/**
 * Created by Fabrice Armisen (farmisen@gmail.com) on 1/3/16.
 */

package net.omobio.docscanner;

import android.content.Context;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View;

import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Point;

public class DSCameraView extends ViewGroup {
    private static final String TAG = "DSCameraView";
    private final OrientationEventListener _orientationListener;
    private final Context _context;
    private DSCameraViewFinder _viewFinder = null;
    private int _actualDeviceOrientation = -1;
    private int _aspect = DSCameraModule.DS_CAMERA_ASPECT_FIT;
    private int _captureMode = DSCameraModule.DS_CAMERA_CAPTURE_MODE_STILL;
    private String _captureQuality = "high";
    private int _torchMode = -1;
    private int _flashMode = -1;

    public DSCameraView(Context context) {
        super(context);
        this._context = context;
        DSCamera.createInstance(getDeviceOrientation(context));

        _orientationListener = new OrientationEventListener(context, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {
                //Log.d(TAG, "xxxx onOrientationChanged");

                if (setActualDeviceOrientation(_context)) {
                    layoutViewFinder();
                }
            }
        };

        if (_orientationListener.canDetectOrientation()) {
            _orientationListener.enable();
        } else {
            _orientationListener.disable();
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d(TAG, "xxxx onLayout");

        layoutViewFinder(left, top, right, bottom);
    }

    @Override
    public void onViewAdded(View child) {
        Log.d(TAG, "xxxx onViewAdded");

        if (this._viewFinder == child) return;
        // remove and readd view to make sure it is in the back.
        // @TODO figure out why there was a z order issue in the first place and fix accordingly.
        this.removeView(this._viewFinder);
        this.addView(this._viewFinder, 0);
    }

    public void setAspect(int aspect) {
        Log.d(TAG, "xxxx setAspect");

        this._aspect = aspect;
        //layoutViewFinder();
        loadOpenCV();
    }


    public void loadOpenCV() {
        if (getContext() == null) return;
        BaseLoaderCallback loaderCallback = new BaseLoaderCallback(getContext()) {
            @Override
            public void onManagerConnected(int status) {
                Log.d(TAG, "xxxx onManagerConnected");

                switch (status) {
                    case LoaderCallbackInterface.SUCCESS: {
                        Log.i(TAG, "xxxx OpenCV loaded successfully");
                        /*
                        if (getContext() != null) {
                            //setCvCameraViewListener(createCvCameraViewListener());
                            //ALPRCameraView.this.enableView();
                            layoutViewFinder();
                        }
                        */
                    }
                    break;
                    default: {
                        super.onManagerConnected(status);
                    }
                    break;
                }
            }
        };
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "xxxx Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_3_0, getContext(), loaderCallback);
        } else {
            Log.d(TAG, "xxxx OpenCV library found inside package. Using it!");
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void setCameraType(final int type) {
        Log.d(TAG, "xxxx setCameraType");

        if (null != this._viewFinder) {
            this._viewFinder.setCameraType(type);
            DSCamera.getInstance().adjustPreviewLayout(type);
        } else {
            _viewFinder = new DSCameraViewFinder(_context, type);
            if (-1 != this._flashMode) {
                _viewFinder.setFlashMode(this._flashMode);
            }
            if (-1 != this._torchMode) {
                _viewFinder.setTorchMode(this._torchMode);
            }
            addView(_viewFinder);
        }
    }

    public void setCaptureMode(final int captureMode) {
        Log.d(TAG, "xxxx setCaptureMode");

        this._captureMode = captureMode;
        if (this._viewFinder != null) {
            this._viewFinder.setCaptureMode(captureMode);
        }
    }

    public void setCaptureQuality(String captureQuality) {
        Log.d(TAG, "xxxx setCaptureQuality");

        this._captureQuality = captureQuality;
        if (this._viewFinder != null) {
            this._viewFinder.setCaptureQuality(captureQuality);
        }
    }

    public void setTorchMode(int torchMode) {
        Log.d(TAG, "xxxx setTorchMode");

        this._torchMode = torchMode;
        if (this._viewFinder != null) {
            this._viewFinder.setTorchMode(torchMode);
        }
    }

    public void setFlashMode(int flashMode) {
        Log.d(TAG, "xxxx setFlashMode");

        this._flashMode = flashMode;
        if (this._viewFinder != null) {
            this._viewFinder.setFlashMode(flashMode);
        }
    }

    public void setOrientation(int orientation) {
        Log.d(TAG, "xxxx setOrientation");

        DSCamera.getInstance().setOrientation(orientation);
        if (this._viewFinder != null) {
            layoutViewFinder();
        }
    }

    public void setImageProcessorEnabled(boolean imageProcessorEnabled) {
        Log.d(TAG, "xxxx setImageProcessorEnabled");

        DSCamera.getInstance().setImageProcessorEnabled(imageProcessorEnabled);
    }

    public void setImageType(String imageType) {
        DSCamera.getInstance().setImageType(imageType);
    }

    private boolean setActualDeviceOrientation(Context context) {
        int actualDeviceOrientation = getDeviceOrientation(context);
        if (_actualDeviceOrientation != actualDeviceOrientation) {
            _actualDeviceOrientation = actualDeviceOrientation;
            DSCamera.getInstance().setActualDeviceOrientation(_actualDeviceOrientation);
            return true;
        } else {
            return false;
        }
    }

    private int getDeviceOrientation(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
    }

    private void layoutViewFinder() {
        layoutViewFinder(this.getLeft(), this.getTop(), this.getRight(), this.getBottom());
    }

    private void layoutViewFinder(int left, int top, int right, int bottom) {
        Log.d(TAG, "xxxx layoutViewFinder");
        if (null == _viewFinder) {
            return;
        }
        float width = right - left;
        float height = bottom - top;
        int viewfinderWidth;
        int viewfinderHeight;
        double ratio;
        switch (this._aspect) {
            case DSCameraModule.DS_CAMERA_ASPECT_FIT:
                ratio = this._viewFinder.getRatio();
                if (ratio * height > width) {
                    viewfinderHeight = (int) (width / ratio);
                    viewfinderWidth = (int) width;
                } else {
                    viewfinderWidth = (int) (ratio * height);
                    viewfinderHeight = (int) height;
                }
                break;
            case DSCameraModule.DS_CAMERA_ASPECT_FILL:
                ratio = this._viewFinder.getRatio();
                if (ratio * height < width) {
                    viewfinderHeight = (int) (width / ratio);
                    viewfinderWidth = (int) width;
                } else {
                    viewfinderWidth = (int) (ratio * height);
                    viewfinderHeight = (int) height;
                }
                break;
            default:
                viewfinderWidth = (int) width;
                viewfinderHeight = (int) height;
        }

        int viewFinderPaddingX = (int) ((width - viewfinderWidth) / 2);
        int viewFinderPaddingY = (int) ((height - viewfinderHeight) / 2);

        this._viewFinder.layout(viewFinderPaddingX, viewFinderPaddingY, viewFinderPaddingX + viewfinderWidth, viewFinderPaddingY + viewfinderHeight);
        this.postInvalidate(this.getLeft(), this.getTop(), this.getRight(), this.getBottom());
    }


}
