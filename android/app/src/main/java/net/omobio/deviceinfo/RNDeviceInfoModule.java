/*
 * File: RNDeviceInfoModule.java
 * Project: Dialog Sales App
 * File Created: Thursday, 19th July 2018 4:19:48 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 19th July 2018 4:28:57 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

package  net.omobio.deviceinfo;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.json.JSONObject;


public class RNDeviceInfoModule extends ReactContextBaseJavaModule {

   ReactApplicationContext reactContext;

   public RNDeviceInfoModule(ReactApplicationContext reactContext) {
       super(reactContext);
       this.reactContext = reactContext;
   }

    @Override
    public String getName() {
        return "DeviceInfo";
    }

    //Retrieve Device IMEI
    @ReactMethod
    public void getIMEI(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getDeviceId());

        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

    //Retrieve Device IMSI
    @ReactMethod
    public void getIMSI(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getSubscriberId());

        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

    //Retrieve Device SIM Serial Number
    @ReactMethod
    public void getSimSerialNumber(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getSimSerialNumber());
          
        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }
}