package net.omobio.dialog.dialogra;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

import com.facebook.react.ReactPackage;

import org.reactnative.camera.RNCameraPackage;

import com.reactnativenavigation.NavigationApplication;
import com.rssignaturecapture.RSSignatureCapturePackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;

import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;

import eu.sigrlami.rnsimdata.RNSimDataReactPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;

import com.rnfs.RNFSPackage;
import com.github.yamill.orientation.OrientationPackage;
import me.furtado.smsretriever.RNSmsRetrieverPackage;

import net.omobio.imageuploader.RNReactNativeImageuploaderPackage;
import net.omobio.docscanner.DSCameraPackage;
import net.omobio.imeigetter.RNIMEIPackage;

import java.util.Arrays;
import java.util.List;
import org.wonday.pdf.RCTPdfView;
import com.RNFetchBlob.RNFetchBlobPackage;
import net.omobio.locationservice.LocationServicePackage;

import com.airbnb.android.react.maps.MapsPackage;
import com.wix.interactable.Interactable;

// import net.omobio.dialog.dialogra.MadmePackage;
// import com.madme.mobile.sdk.MadmeService;
import net.omobio.dialog.dialogra.NetworkMonitoringPackage; // TODO: Update to the latest SDK

public class MainApplication extends NavigationApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        // MadmeService.init(getApplicationContext());
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                //activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    @Override
    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    @Override
public String getJSMainModuleName() {
  return "index";
}


    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                // eg. new VectorIconsPackage()   
                new MapsPackage(),
                new Interactable(),          
                new NetworkMonitoringPackage(), // TODO: Update to the latest SDK
                new RSSignatureCapturePackage(),
                new RNCameraPackage(),
                new RNReactNativeImageuploaderPackage(),
                new DSCameraPackage(),
                new ReactNativeLocalizationPackage(),
                new RNSimDataReactPackage(),
                new RNFirebasePackage(),
                new RNFirebaseAnalyticsPackage(),
                new RNFirebaseMessagingPackage(),
                new RNFSPackage(),
                new RNDeviceInfo(),
                new OrientationPackage(),
				new RNIMEIPackage(),
                new RNFetchBlobPackage(),
                new RCTPdfView(),                                
                new LocationServicePackage(),
                new RNSmsRetrieverPackage()
                // new MadmePackage()
        );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }
}
