package net.omobio.dialog.dialogra;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.widget.TextView;
import android.view.Gravity;
import android.util.TypedValue;

import com.reactnativenavigation.controllers.SplashActivity;

import java.util.UUID;

import android.content.res.Configuration;

public class MainActivity extends SplashActivity {


    @Override
    public LinearLayout createSplashLayout() {
        LinearLayout view = new LinearLayout(this);
        TextView textView = new TextView(this);


        view.setBackgroundResource(R.drawable.splash);
        view.setGravity(Gravity.CENTER);
        //view.setBackgroundColor(Color.parseColor("#4A148C")); //607D8B
        //textView.setTextColor(Color.parseColor("#FFFFFF")); //FFFFFF
        //textView.setText("Dialog Sales App");
        //textView.setGravity(Gravity.CENTER);
        //textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        view.addView(textView);
        return view;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Intent intent = new Intent("onConfigurationChanged");
        intent.putExtra("newConfig", newConfig);
        this.sendBroadcast(intent);
    }
}
    