//package net.omobio.dialog.dialogra;
//
//import android.os.Bundle;
//
//import com.facebook.react.bridge.ReactApplicationContext;
//import com.facebook.react.bridge.ReactContextBaseJavaModule;
//import com.facebook.react.bridge.ReactMethod;
//import com.facebook.react.module.annotations.ReactModule;
//import com.madme.mobile.sdk.MadmeService;
//import com.madme.mobile.sdk.GetAdParams;
//import com.madme.mobile.sdk.HostApplication;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@ReactModule(name = "MadmeServiceReact")
//public class MadmeServiceModule extends ReactContextBaseJavaModule {
//  public MadmeServiceModule(ReactApplicationContext reactContext) {
//    super(reactContext);
//  }
//
//  @Override
//  public String getName() {
//    return "MadmeServiceReact";
//  }
//
//  @ReactMethod
//  public void initService() {
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    MadmeService.init(reactApplicationContext);
//  }
//
//  @ReactMethod
//  public void requestPermissionsIfNeeded() {
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    MadmeService.requestPermissionsIfNeeded(reactApplicationContext, new String[]
//    {
//      android.Manifest.permission.ACCESS_FINE_LOCATION,
//      android.Manifest.permission.ACCESS_COARSE_LOCATION,
//      android.Manifest.permission.RECEIVE_SMS,
//      android.Manifest.permission.READ_PHONE_STATE
//    });
//  }
//
//  @ReactMethod
//  public void hostApplication(final String clientid1){
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    final HostApplication hostApplication = new HostApplication() {
//      @Override
//      public void onMadmeEvent(MadmeEvent madmeEvent, Bundle bundle) {
//
//      }
//
//      @Override
//      public Map<String, Object> onSupplySurveySubmissionAttributes(String s, String s1) {
//        return null;
//      }
//
//      @Override
//      public Map<String, String> onSupplyClientIds() {
//        final Map<String, String> result = new HashMap<String, String>();
//        //Put any custom IDs you need to send to the mAdme backend into the result map
//        result.put("clientid1", clientid1);
//        //Return the result map or null if no custom IDs are being sent from the host application to the mAdme backend
//        return result;
//      }
//
//      @Override
//      public boolean canAutoRegisterNow() {
//        return false;
//      }
//
//      @Override
//      public void onHostAppOptOut(Long aLong) {
//
//      }
//
//      @Override
//      public void onRefreshInbox() {
//
//      }
//    };
//    MadmeService.init(reactApplicationContext, hostApplication);
//  }
//
//  @ReactMethod
//  public void showNextAdIfAvailable() {
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    //Attempt to show a generic end-of-call campaign
//    MadmeService.viewAd(reactApplicationContext);
//  }
//
//  @ReactMethod
//  public void showNextVoicemailAdIfAvailable(String phoneNumber) {
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    //Attempt to show an end-of-call campaign for a specific phone number
//    final GetAdParams params = new GetAdParams();
//    params.setPhoneNumber(phoneNumber);
//    params.setCallDuration(60000l); //We say that we made a 1 minute call
//    MadmeService.viewAd(reactApplicationContext, params);
//  }
//
//  @ReactMethod
//   // Call mAdme API to request an Survey at app exit based on the Tag configured on the campaign: 'APP_SURVEY_EXIT'
//   public void showExitSurveyIfAvailable() {
//    ReactApplicationContext reactApplicationContext = getReactApplicationContext();
//    final GetAdParams params = new GetAdParams();
//    //We supply the tag we're interested in
//    params.setTags(new String[]{"APP_SURVEY_EXIT"});
//    //Attempt to show an campaign for the specified tag
//    MadmeService.viewAd(reactApplicationContext, params);
//  }
//
////   @ReactMethod
////   public void inactiveHostApplication(){
////     ReactApplicationContext reactApplicationContext = getReactApplicationContext();
////     MadmeService.registerHostApplicationUsage();
////     MadmeService.finish();
////   }
//}
