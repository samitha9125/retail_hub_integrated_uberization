package net.omobio.dialog.dialogra;

import android.util.Log;
import android.widget.Toast;
import android.content.Intent;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

import lk.dialog.cemstatslibrary.CEMService;

import static lk.dialog.cemstatslibrary.containers.actions.Actions.EXTRA_MSISDN;
import static lk.dialog.cemstatslibrary.containers.actions.Actions.EXTRA_VERSION;
import static lk.dialog.cemstatslibrary.containers.actions.Actions.EXTRA_HOST_APP;

public class NetworkMonitoringModule extends ReactContextBaseJavaModule {
  public static final String TAG = "NetworkMonitoring";

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";
  //Intent serviceIntent;

  public NetworkMonitoringModule(ReactApplicationContext reactContext) {
    super(reactContext);
    //serviceIntent = new Intent(getReactApplicationContext(), CEMService.class);

  }

  @Override
  public String getName() {
    Log.d(TAG, "getName called.");
    return "NetworkMonitoring";
  }

  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
    constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
    return constants;
  }

  @ReactMethod
  public void serviceStartStop(String msisdn, String version, String hostApp,boolean status ) {
    //Toast.makeText(getReactApplicationContext(), message, duration).show();
    Log.d(TAG, "serviceStartStop called.");
    Log.d(TAG, "MSISDN: "+msisdn);
    Log.d(TAG, "Version: "+version);
    Log.d(TAG, "Host App: "+hostApp);
    Log.d(TAG, "Status: "+status);

    Intent serviceIntent = new Intent(getReactApplicationContext(), CEMService.class);

    if(status == true){
      Log.d(TAG, "Status is true. Starting service.");
      serviceIntent.putExtra(EXTRA_MSISDN, msisdn);
      serviceIntent.putExtra(EXTRA_VERSION, version);
      serviceIntent.putExtra(EXTRA_HOST_APP, hostApp);
      getReactApplicationContext().startService(serviceIntent);
//       Toast.makeText(getReactApplicationContext(), "Service Start\n"+msisdn+"\n"+version+"\n"+hostApp, Toast.LENGTH_LONG).show();
    } else {
      Log.d(TAG, "Status is false. Stopping service.");
      getReactApplicationContext().stopService(serviceIntent);
      //Toast.makeText(getReactApplicationContext(), "Service Stop", Toast.LENGTH_SHORT).show();
    }
  }
}
