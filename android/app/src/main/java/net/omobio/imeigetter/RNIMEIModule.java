/**
 * Created by Arafath on 18/06/18.
 */

package  net.omobio.imeigetter;

import android.content.Context;
import android.hardware.Camera;
import android.telephony.TelephonyManager;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.json.JSONObject;


public class RNIMEIModule extends ReactContextBaseJavaModule {

   ReactApplicationContext reactContext;

   public RNIMEIModule(ReactApplicationContext reactContext) {
       super(reactContext);
       this.reactContext = reactContext;
   }

    @Override
    public String getName() {
        return "IMEI";
    }

    @ReactMethod
    public void getIMEI(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getDeviceId());

        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

    // Retrive Device IMSI 
    @ReactMethod
    public void getIMSI(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getSubscriberId());

        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

    // Retrive Device SIM Serial Number
    @ReactMethod
    public void getSimSerialNumber(Promise promise) {
        try {
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve( tm.getSimSerialNumber());
          
        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

    // Retrive Device Camera Resolution
    @ReactMethod
    public void getCameraRes(Promise promise) {
        try {
            int noOfCameras = Camera.getNumberOfCameras();
            float maxResolution = -1;
            long pixelCount = -1;
            for (int i = 0;i < noOfCameras;i++)
            {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(i, cameraInfo);

                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                {
                    Camera camera = Camera.open(i);;
                    Camera.Parameters cameraParams = camera.getParameters();
                    for (int j = 0;j < cameraParams.getSupportedPictureSizes().size();j++)
                    {
                        long pixelCountTemp = cameraParams.getSupportedPictureSizes().get(j).width * cameraParams.getSupportedPictureSizes().get(j).height; // Just changed i to j in this loop
                        if (pixelCountTemp > pixelCount)
                        {
                            pixelCount = pixelCountTemp;
                            maxResolution = ((float)pixelCountTemp) / (1024000.0f);
                        }
                    }

                    camera.release();
                }
            }
            promise.resolve( maxResolution );

        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }
}