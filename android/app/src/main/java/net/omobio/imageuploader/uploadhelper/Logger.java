package net.omobio.imageuploader.uploadhelper;

import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.util.zip.GZIPOutputStream;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class Logger {
    private static final String TAG = "ImageUploaderLogger";
    private static final String LIVE_HOST = "o2a.dialog.lk";
    private static final String STAGING_HOST = "o2astg.dialog.lk";
    private static final String ENVIRONMENT_LIVE = "LIVE";
    private static final String ENVIRONMENT_STAGING = "STAGING";



    public static void logHandler() {
        Log.d(TAG, "logHandler");
        boolean sendLogsToBackend = false; //TODO: Enable uploading logcat
        if(sendLogsToBackend) {
            Log.d(TAG, "logHandler :: PROCEED log upload");
            uploadAllLogs();
        } else {
            Log.d(TAG, "logHandler :: SKIP log upload");
        }
    }

    public static void uploadLogs(String id, String url) {
        Timestamp timestamp  = new Timestamp(System.currentTimeMillis());
        long      time       = timestamp.getTime();
        File      outputFile = new File(Environment.getExternalStorageDirectory(), "logcat_" + id + "_" + time + ".txt");
        StringBuilder sb     = new StringBuilder();
        try {
            String[] command = new String[] { "logcat", "-d", "-v", "threadtime" };
            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line + "\n");
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
            final int aLength = sb.length();
            final int aChunk = 1024;// 1 kb buffer to read data from
            final char[] aChars = new char[aChunk];

            for (int aPosStart = 0; aPosStart < aLength; aPosStart += aChunk) {
                final int aPosEnd = Math.min(aPosStart + aChunk, aLength);
                sb.getChars(aPosStart, aPosEnd, aChars, 0); // Create no new buffer
                bw.write(aChars, 0, aPosEnd - aPosStart); // This is faster than just copying one byte at the time
            }
            bw.flush();

            File gzippedFile = new File(outputFile.getAbsolutePath() + ".gz");
            gzipFile(outputFile, gzippedFile);

            int                 sizeInt    = (int) gzippedFile.length();
            byte[]              filedata   = new byte[sizeInt];
            BufferedInputStream buf        = new BufferedInputStream(new FileInputStream(gzippedFile));
            int                 readStatus = buf.read(filedata, 0, filedata.length);

            if (readStatus > 0) {
                String logTrackerUrl = Logger.getLogTrackerUrlFromCaptureUrl(url);
                Log.d(TAG, "logTrackerUrl " + logTrackerUrl);
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("id", id)
                        .add("filename", gzippedFile.getName())
                        .add("file", Base64.encodeToString(filedata, Base64.DEFAULT))
                        .add("my_pid", String.valueOf(android.os.Process.myPid()))
                        .add("my_tid", String.valueOf(android.os.Process.myTid()))
                        .build();

                Request request = new Request.Builder()
                        .url(logTrackerUrl)
                        .post(formBody)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    if (!response.isSuccessful()) {
                        Log.e(TAG, "Log upload failed " + response.body().string());
                    } else {
                        gzippedFile.delete();
                        Log.d(TAG, "Log upload success " + response.body().string());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Log upload failed");
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Log extraction failed");
            e.printStackTrace();
        }
    }

    public static void uploadAllLogs() {
        Log.d(TAG,"uploadAllLogs");
        Timestamp timestamp  = new Timestamp(System.currentTimeMillis());
        long      time       = timestamp.getTime();
        File      outputFile = new File(Environment.getExternalStorageDirectory(), "logcat_" + "_" + time + ".txt");
        StringBuilder sb     = new StringBuilder();
        try {
            String[] command = new String[] { "logcat", "-d", "-v", "threadtime" };
            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line + "\n");
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
            final int aLength = sb.length();
            final int aChunk = 1024;// 1 kb buffer to read data from
            final char[] aChars = new char[aChunk];

            for (int aPosStart = 0; aPosStart < aLength; aPosStart += aChunk) {
                final int aPosEnd = Math.min(aPosStart + aChunk, aLength);
                sb.getChars(aPosStart, aPosEnd, aChars, 0); // Create no new buffer
                bw.write(aChars, 0, aPosEnd - aPosStart); // This is faster than just copying one byte at the time
            }
            bw.flush();

            File gzippedFile = new File(outputFile.getAbsolutePath() + ".gz");
            gzipFile(outputFile, gzippedFile);

            int                 sizeInt    = (int) gzippedFile.length();
            byte[]              filedata   = new byte[sizeInt];
            BufferedInputStream buf        = new BufferedInputStream(new FileInputStream(gzippedFile));
            int                 readStatus = buf.read(filedata, 0, filedata.length);

            if (readStatus > 0) {
                String logTrackerUrl = Logger.getLogTrackerUrl();
                Log.d(TAG, "logTrackerUrl " + logTrackerUrl);
                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new FormBody.Builder()
                        .add("filename", gzippedFile.getName())
                        .add("file", Base64.encodeToString(filedata, Base64.DEFAULT))
                        .add("my_pid", String.valueOf(android.os.Process.myPid()))
                        .add("my_tid", String.valueOf(android.os.Process.myTid()))
                        .build();

                Request request = new Request.Builder()
                        .url(logTrackerUrl)
                        .post(formBody)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    if (!response.isSuccessful()) {
                        Log.e(TAG, "Log upload failed " + response.body().string());
                    } else {
                        gzippedFile.delete();
                        Log.d(TAG, "Log upload success " + response.body().string());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Log upload failed");
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Log extraction failed"+ e.toString());
            e.printStackTrace();
        }
    }

    public static void gzipFile(File src, File dest) {

        byte[] buffer = new byte[1024];

        try {

            GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(dest));
            FileInputStream in = new FileInputStream(src);

            int len;
            while ((len = in.read(buffer)) > 0) {
                gzos.write(buffer, 0, len);
            }

            in.close();

            gzos.finish();
            gzos.close();
            src.delete();
            Log.d(TAG, "Gzip done");

        } catch (IOException ex) {
            Log.e(TAG, "Gzip failed");
            ex.printStackTrace();
        }
    }

    private static String getLogTrackerUrlFromCaptureUrl(String url) throws Exception {
        URL captureUrl = new URL(url);
        //URL logTrackerUrl = new URL(captureUrl.getProtocol(), captureUrl.getHost(), "logTracker.php");
        String logTrackerUrl;
        if(captureUrl.getHost().equals(STAGING_HOST)) {
            logTrackerUrl = "http://o2astg.dialog.lk/O2A-Retailer-LTE/DEV/data/logTracker.php"; //STAGING URL
        } else {
            logTrackerUrl = "http://o2a.dialog.lk/O2A-Retailer-Live/logTracker.php"; //TODO: LIVE URL
        }
        return logTrackerUrl;
    }

    private static String getLogTrackerUrl() {
        String logTrackerUrl;
        boolean isStagingEnvironment = false; //TODO: Change here
        if(isStagingEnvironment) {
            logTrackerUrl = "http://o2astg.dialog.lk/O2A-Retailer-LTE/DEV/data/logTracker.php"; //STAGING URL
        } else {
            logTrackerUrl = "http://o2a.dialog.lk/O2A-Retailer-Live/logTracker.php"; //TODO: LIVE URL
        }
        return logTrackerUrl;
    }

    public static String getUploadToRecoveryUrl() {
        String imageRecoveryUrl;
        boolean isStagingEnvironment = false; //TODO: Change here
        if(isStagingEnvironment) {
            imageRecoveryUrl = "http://o2astg.dialog.lk/O2A-Retailer-LTE/DEV/index.php?r=recover/images"; //STAGING URL
        } else {
            imageRecoveryUrl = "https://o2a.dialog.lk/O2A-Retailer-Live/index.php?r=recover/images"; // TODO: LIVE URL
        }
        return imageRecoveryUrl;
    }
}