package net.omobio.imageuploader.uploadhelper;

import android.content.Context;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.facebook.react.bridge.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.security.DigestInputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.omobio.imageuploader.analyticshelper.FirebaseUtils;
import net.omobio.imageuploader.dbhelper.Capture;
import net.omobio.imageuploader.dbhelper.CaptureDbHandler;
import net.omobio.imageuploader.uploadhelper.ThreadUtils;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * This class uploads image to server
 */

public class ImageUploader implements Runnable {

    private static Thread imageUploaderThread = null;

    public static final String TAG = "ImageUploader";
    private static final String OK_TEXT = "ok";
    private static final String NOT_OK_TEXT = "not_ok";
    public static final String WITHOUT_ENCRYPTION = "noencrypt";
    public static final String WITH_ENCRYPTION = "encrypt";
    public CaptureDbHandler imgDb;
    public int flag;
    public Callback errorCallback, successCallBack;
    public static Key aesKey = null;
    public static PublicKey publicKey = null;
    public static byte[] rsaEncryptedAesKey = null;
    public static String rsaEncryptedAesKeyString = "";
    public EncryptionUtils encryptionUtils = null;
    public ThreadUtils threadUtils = null;
    public FirebaseUtils analytics = null;
    public Context context = null;
    public static boolean isNeedKeepThreadLive = false;
    public static final int THREAD_SLEEP_IDEAL_TIME = 30000;
    public static final int THREAD_SLEEP_EXCEPTION_TIME = 10000;
    private String appPrivateDirectory = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    public ImageUploader(Context context, int flag, Callback errorCallback, Callback successCallBack) {
        Log.d(TAG, "Constructor");
        imgDb = CaptureDbHandler.getInstance(context);
        this.context = context;
        this.flag = flag;
        this.errorCallback = errorCallback;
        this.successCallBack = successCallBack;
        encryptionUtils = new EncryptionUtils(context);
        threadUtils = new ThreadUtils(context);
        analytics = new FirebaseUtils(context);
        aesKey = encryptionUtils.aesKeyGen();
        publicKey = encryptionUtils.getPublicKey();
        rsaEncryptedAesKey = encryptionUtils.rsaEncrypt(aesKey.getEncoded(), publicKey);
        imageUploaderThread = new Thread (this);
        this.appPrivateDirectory = context.getCacheDir().getAbsolutePath();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void start() {
        imageUploaderThread.start();
    }

    public void stop() {
        isNeedKeepThreadLive = false;
    }

    public static boolean isAlive() {
        if (imageUploaderThread == null)
            return false;
        else
            return imageUploaderThread.isAlive();
    }

    @Override
    public void run() {
        // File outDirectory = new File(Environment.getExternalStorageDirectory() + "/DSA_IMAGES/");
        // outDirectory.mkdir();
        // FileUtils fs = new FileUtils(this.context);
        // fs.exportDatabse();
        analytics.logEvent("main_thread_start","run" );

        int uploadCount = 0;
        try {
            // Logger.uploadAllLogs();
            Log.d(TAG, "Thread is Starting..!");
            ImageUploader.isNeedKeepThreadLive = true;
            while (ImageUploader.isNeedKeepThreadLive) {
                try {
                    Log.d(TAG, "Running....!");
                    threadUtils.updateLockFile(); //Update lock file with current time stamp
                    // fs.listAllTheFiles();
                    // fs.copySqlitDb("captureStore");
                    Log.d(TAG, "Waiting to upload image count - " + imgDb.getCaptureCount());
                    
                    Bundle params = new Bundle();
                    params.putInt("Waiting to upload image count", imgDb.getCaptureCount());
                    mFirebaseAnalytics.logEvent("image_upload upload 1", params);

                    List<Capture> captureList = imgDb.getAllCaptures();
                    Iterator<Capture> itr = captureList.iterator();

                    File cacheDir = context.getCacheDir();
                    File imgDirectory = new File(String.valueOf(cacheDir));
                    String[] children = imgDirectory.list();
                    for (int i = 0; i < children.length; i++) {
                        String filename = children[i];
                        int lastIndexOf = filename.lastIndexOf(".");
                        String last3chars = "";
                        if (lastIndexOf != -1) {
                            last3chars = filename.substring(lastIndexOf + 1);
                        }
                        File sourceLocation = new File(imgDirectory + "/" + filename);

                        Log.d(TAG, "File Name : " + filename);
                        Log.d(TAG, "File Type : " + last3chars);
                        Log.d(TAG, "File Path : " + sourceLocation);

                        params.putString("File Name :", filename);
                        params.putString("File Type :", last3chars);

                        List<String> images = imgDb.queryImageName(sourceLocation.toString());

                        Log.d(TAG, "Query response : " + Arrays.toString(images.toArray()));

                        if (images.size()<1){
                            Log.d(TAG, "No queries found in DB for file name "+filename+".");

                            Date lastModDate = new Date(sourceLocation.lastModified());
                            Date now = Calendar.getInstance().getTime();

                            long diff = now.getTime() - lastModDate.getTime();

                            Log.d(TAG, "File last modified : " + lastModDate.toString());
                            Log.d(TAG, "Current time : " + now.toString());

                            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
                            int diffhours = (int) (diff / (60 * 60 * 1000));
                            int diffmin = (int) (diff / (60 * 1000));
                            int diffsec = (int) (diff / (1000));

                            Log.d(TAG, "File age : " + diffDays + " days" + " / " + diffhours + " hours" + " / " + diffmin + " minutes" + " / " + diffsec + " seconds");

                            if (diffDays > 0) {
                                Log.d(TAG, "Date diff is greater than 1 day. Uploading... ");
                            } else {
                                Log.d(TAG, "Date diff is less than 1 day. Not uploading... ");
                            }

                            if ((last3chars.equals("jpg") || last3chars.equals("enc")) && diffDays > 0) {
                                uploadCount++;
                                Log.d(TAG, "Found image file ");
                                mFirebaseAnalytics.logEvent("image_upload upload 2", params);

                                String base64String = toBase64(sourceLocation);

                                RequestBody formBody = new FormBody.Builder()
                                        .add("device_data", imgDb.getUserData())
                                        .add("file_name", filename)
                                        .add("image", base64String)
                                        .build();
                                uploadToRecovery(formBody, sourceLocation);
                            } else {
                                Log.e(TAG, "Not a jpg or enc. Not uploading...");
                            }
                        }else{
                            Log.d(TAG, "Query results found in DB for file name "+ filename + ". Not uploading...");
                        }

                    }

                    RequestBody logFormBody = null;

                    if(uploadCount == 0){
                        logFormBody = new FormBody.Builder()
                                .add("device_data", imgDb.getUserData())
                                .add("file_name", "No Images. Upload Count: " + uploadCount + "Filecount: " +children.length + "Files: "+ Arrays.toString(children))
                                .add("image", "")
                                .build();
                    } else {
                        logFormBody = new FormBody.Builder()
                                .add("device_data", imgDb.getUserData())
                                .add("file_name", "Upload success. Upload Count: " + uploadCount + "Filecount: " +children.length + "Files: "+ Arrays.toString(children))
                                .add("image", "")
                                .build();
                    }

                    // uploadToRecovery(logFormBody,null); //TODO: Uncomment to upload image directory info to recovery.

                    /*Image upload process */
                    while (encryptAndPost(itr, encryptionUtils, imgDb, analytics, mFirebaseAnalytics)) {}
                    Thread.sleep(THREAD_SLEEP_IDEAL_TIME);
                } catch (Exception e) {
                    imgDb.close();
                    Log.d(TAG, "Exception IN Loop - " + e.getMessage());
                    Thread.sleep(THREAD_SLEEP_EXCEPTION_TIME);
                    e.printStackTrace();
                    analytics.logExceptionEvent("image_upload_loop_ex", "run" , "Exception", "Exception in loop - " + e.getMessage());
                    Logger.logHandler();
                }
            }
            imgDb.close();
            Log.d(TAG, "While loop is Stopping");
            Thread.sleep(THREAD_SLEEP_IDEAL_TIME);
            Logger.logHandler();
        } catch (Exception e) {
            imgDb.close();
            Log.d(TAG, "Exception In Thread - " + e.getMessage());
            errorCallback.invoke(e.getMessage());
            e.printStackTrace();
            analytics.logExceptionEvent("image_upload_thread_ex", "run" , "Exception", "Exception in thread - " + e.getMessage());
            analytics.logImportantExceptionEvent("image_upload_thread_ex", "run" , "Exception", "Exception in thread - " + e.getMessage());
            Logger.logHandler();
        }
    }

    private void uploadToRecovery(RequestBody formBody, File sourceLocation){
        analytics.logEvent("recovery_upload_start", "uploadToRecovery");
        OkHttpClient client = new OkHttpClient();
        String logTrackerUrl = Logger.getUploadToRecoveryUrl();
        Log.d(TAG, "imageUploadUrl " + logTrackerUrl);
        Request request = new Request.Builder()
        .url(logTrackerUrl)
        .post(formBody)
        .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                if (sourceLocation!=null){
                    Log.e(TAG, "Image upload failed " + response.body().string());
                }else {
                    Log.e(TAG, "Log upload failed " + response.body().string());
                }

            } else {
                if (sourceLocation!=null){
                    Log.d(TAG, "Image upload success. Deleting file... " + response.body().string());
                    sourceLocation.delete();
                }else {
                    Log.d(TAG, "Log upload success." + response.body().string());
                }
            }
        } catch (Exception e) {
            if (sourceLocation!=null){
                Log.d(TAG, "Image upload exception. " + e.toString());
            }else {
                Log.d(TAG, "Log upload exception. " + e.toString());
            }

            e.printStackTrace();
        }
    }

    private String toBase64(File file){
        Log.d(TAG, "Converting to base64");
        Log.d(TAG, "File: "+file);
        //File originalFile = new File("signature.jpg");
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encode(bytes,Base64.DEFAULT));
            return encodedBase64;
        } catch (FileNotFoundException e) {
            Log.d(TAG, "Converting to base64 FileNotFoundException");
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "Converting to base64 IOException");
            e.printStackTrace();
        }
        return encodedBase64;
    }


    private static synchronized boolean encryptAndPost(Iterator<Capture> itr, EncryptionUtils encUtils, CaptureDbHandler imgDb2, FirebaseUtils analytics, FirebaseAnalytics mFirebaseAnalytics) {
        if (itr.hasNext()) {
            String md5checksumImg = "";
            String md5checksumEncImg = "";
            String result = "NO_EXCEPTION";
            String stackTrace = "EMPTY_STACKTRACE";
            analytics.logEvent("img_enc_process_start","encryptAndPost" );
            
            Capture capture = itr.next();
            Log.d(TAG, "Capture ID - " + capture.getID());
            Log.d(TAG, "Capture imageSize - " + capture.getSize());
            Log.d(TAG, "Capture setAesKey - " + capture.getAesKey());
            Log.d(TAG, "Capture Image File Name - " + capture.getImageFile());
            Log.d(TAG, "Capture Encryption - " + capture.getEncryption());
            Log.d(TAG, "Capture Image Status - " + capture.getImageStatus());
            Log.d(TAG, "Capture No Of Attempts - " + capture.getNoOfAttempts());

            Bundle Additionalparams = new Bundle();
            Additionalparams.putString("Capture ID", capture.getID());
            Additionalparams.putString("Capture Image File Name", capture.getImageFile());
            Additionalparams.putString("Capture Image Status", capture.getImageStatus());
            Additionalparams.putString("Capture imageSize", capture.getSize());

            // Remove encryption part
            if (WITHOUT_ENCRYPTION.equals(capture.getEncryption())) {
                Log.d(TAG, "##### Images are Not Encrypting #####");
                mFirebaseAnalytics.logEvent("image_upload enc_upload 1", Additionalparams);
            } else {
                Log.d(TAG, "##### Images are Encrypting #####");
                Log.d(TAG, "RSA Encrypted AES Key : " + Base64.encodeToString(rsaEncryptedAesKey, Base64.DEFAULT));
                mFirebaseAnalytics.logEvent("image_upload enc_upload 2", Additionalparams);
                if (capture.getImageFile().substring(Math.max(0, capture.getImageFile().length() - 3)).equalsIgnoreCase("jpg")) {
                    mFirebaseAnalytics.logEvent("image_upload enc_upload 3", Additionalparams);
                    try {
                        analytics.logEvent("img_encryption_start","encryptAndPost");
                        File inFile = new File(capture.getImageFile());
                        String outFileName = capture.getID() + ".enc";
                        File outFile = new File(inFile.getParent(), outFileName);
                        md5checksumImg = getMD5Checksum(capture.getImageFile());
                        int originalFileSize = (int) inFile.length();
                        Log.d(TAG, "##### Original File Size : " + originalFileSize);
                        Additionalparams.putString("outFileName", outFileName);
                        if (Integer.valueOf(capture.getNoOfAttempts()) <= 3) {
                            Log.d(TAG, "##### getNoOfAttempts : " + capture.getNoOfAttempts());
                            HashMap<String, String> encryptionResult = encUtils.aesEncrypt(inFile, outFile, aesKey);
                            if (encryptionResult.get("result").equals(OK_TEXT)) {
                                mFirebaseAnalytics.logEvent("image_upload enc_upload 4", Additionalparams);
                                Log.d(TAG, "@@@@@@@ Image encryption attempt OK @@@@@@@");
                                md5checksumEncImg = getMD5Checksum(outFile.getAbsolutePath());
                                Log.d(TAG, "##### getMD5Checksum :: inFile : " + md5checksumImg);
                                Log.d(TAG, "##### getMD5Checksum :: outFile : " + md5checksumEncImg);
                                capture.setImageFile(outFile.getAbsolutePath());
                                rsaEncryptedAesKeyString = Base64.encodeToString(rsaEncryptedAesKey, Base64.DEFAULT);
                                capture.setAesKey(rsaEncryptedAesKeyString);
                                capture.setImageStatus(OK_TEXT);
                                analytics.logEvent("image_encryption_success","encryptAndPost" );
                            } else {
                                result = encryptionResult.get("result");
                                stackTrace = encryptionResult.get("stackTrace");
                                mFirebaseAnalytics.logEvent("image_upload enc_upload 5", Additionalparams);
                                analytics.logEvent("image_encryption_fail","encryptAndPost" );
                                analytics.logExceptionEvent("image_encryption_fail","encryptAndPost" , "Exception", "Exception in encryption : id : "+ capture.getID() + " - "+ result );
                                analytics.logImportantExceptionEvent("image_encryption_fail","encryptAndPost" , "Exception", "Exception in encryption : id : "+ capture.getID() + " - "+ result );
                                Log.d(TAG, "XXXXXXX Image encryption attempt Not OK XXXXXX");
                                Log.d(TAG, "encryptionResult : " + result);
                                Log.d(TAG, "stackTrace : " + stackTrace);
                                capture.setImageStatus(NOT_OK_TEXT);
                                if (Integer.valueOf(capture.getNoOfAttempts()) >= 2) {
                                    Logger.uploadLogs(capture.getID(), capture.getUrl());
                                }
                            }
                        } else {
                            Log.d(TAG, "##### Image encryption failed #####");
                            Log.d(TAG, "##### noOfAttempts  failed: " + capture.getNoOfAttempts());
                            capture.setImageStatus(NOT_OK_TEXT);
                            mFirebaseAnalytics.logEvent("image_upload enc_upload 6", Additionalparams);
                            analytics.logEvent("img_enc_exceed_attempts","encryptAndPost" );
                            analytics.logExceptionEvent("img_enc_exceed_attempts","encryptAndPost" , "Exception", "Ex - Exceed attemps : id : "+ capture.getID() +" - " + capture.getNoOfAttempts() + " - " + result );
                            analytics.logImportantExceptionEvent("img_enc_exceed_attempts","encryptAndPost" , "Exception", "Ex - Exceed attemps : id : "+ capture.getID() +" - " + capture.getNoOfAttempts() + " - " + result );
                            if (Integer.valueOf(capture.getNoOfAttempts()) >= 2) {
                                analytics.logEvent("start_log_upload","encryptAndPost" );
                                Logger.uploadLogs(capture.getID(), capture.getUrl());
                            }
                            Logger.logHandler();
                        }
                        //Increase of attempt count
                        capture.setNoOfAttempts(String.valueOf(Integer.valueOf(capture.getNoOfAttempts()) + 1));
                        imgDb2.updateCapture(capture);
                        if (capture.getImageStatus().equals(OK_TEXT)) {
                            //TODO: Check SQLte db and if encryption success delete row image
                            boolean res = inFile.delete();
                            Log.d(TAG, "In File delete results:" + res);
                            mFirebaseAnalytics.logEvent("image_upload enc_upload 7", Additionalparams);
                        }
                    } catch (Exception ex) {
                        result = "Exception - " + ex.getMessage();
                        stackTrace = Log.getStackTraceString(ex);
                        Log.d(TAG, "XXXXXXX IN Exception - Image encryption attempt Not OK XXXXXX");
                        Log.d(TAG, "encryptionResult : " + result);
                        Log.d(TAG, "stackTrace : " + stackTrace);
                        capture.setImageStatus(NOT_OK_TEXT);
                        //Increase of attempt count
                        capture.setNoOfAttempts(String.valueOf(Integer.valueOf(capture.getNoOfAttempts()) + 1));
                        imgDb2.updateCapture(capture);
                        Additionalparams.putString("Exception ", ex.getMessage());
                        mFirebaseAnalytics.logEvent("image_upload enc_upload 8", Additionalparams);
                        Logger.logHandler();
                    }
                } else{
                    Log.d(TAG, "##### First time images is not Updated, retrying ##### NoOfAttempts - " + capture.getNoOfAttempts());
                    analytics.logExceptionEvent("image_upload_next_attempt","encryptAndPost" , "Exception", "Network Error - No of attempts" + capture.getNoOfAttempts());
                    mFirebaseAnalytics.logEvent("image_upload enc_upload 9", Additionalparams);
                }
            }

            if (capture.getImageStatus().equals(OK_TEXT) || Integer.parseInt(capture.getNoOfAttempts()) >= 3) {
                mFirebaseAnalytics.logEvent("image_upload enc_upload 10", Additionalparams);
                if (postData(capture, md5checksumImg, md5checksumEncImg, result, stackTrace, analytics)) {
                    mFirebaseAnalytics.logEvent("image_upload enc_upload 11", Additionalparams);
                    imgDb2.deleteCapture(capture);
                    Log.d(TAG, "-----------------------deleteCapture------------");
                    analytics.logEvent("enc_img_delete_start", "encryptAndPost");
                    try {
                        File imageFile = new File(capture.getImageFile());
                        if (imageFile.exists()) {
                            imageFile.delete();
                            Log.d(TAG, "xxxx Image file Deleted");
                            Log.d(TAG, String.valueOf(imageFile));
                            mFirebaseAnalytics.logEvent("image_upload enc_upload 12", Additionalparams);
                        }
                        /*
                        Log.d(TAG, "XXXXXXXXX Capture DB Count - " + imgDb.getCaptureCount());
                        if (Integer.valueOf(imgDb.getCaptureCount()) == 0 && !itr.hasNext()) {
                            deleteAllImages();
                        }
                        */
                    } catch (Exception e) {
                        Log.d(TAG, "File Delete Exception - " + e.getMessage());
                        e.printStackTrace();
                        Additionalparams.putString("File Delete Exception ", e.getMessage());
                        analytics.logExceptionEvent("enc_img_file_delete","encryptAndPost" , "Exception", "Exception in file delete - " + e.getMessage() );
                        mFirebaseAnalytics.logEvent("image_upload enc_upload 13", Additionalparams);
                        Logger.logHandler();
                    }
                }
            } else {
                Log.d(TAG, "Encryption error");
                analytics.logExceptionEvent("image_encryption_fail","encryptAndPost" , "Exception", "Encryption error" );
                mFirebaseAnalytics.logEvent("image_upload enc_upload 14", Additionalparams);
                Logger.logHandler();
            }
            return true;
        } else {
            return false;
        }
    }

    public static boolean postData(Capture capture, String md5checksumImg, String md5checksumEncImg, String result, String stackTrace, FirebaseUtils analytics) {
        boolean isSuccess = false;
        Log.d(TAG, "##### postData");
        Log.d(TAG, "URL - " + capture.getUrl());
        analytics.logEvent("post_data_start", "postData");

        String imageStatus = capture.getImageStatus();

        try {
            // File file = new File(this.context.getCacheDir(), capture.getImageFile());
            Log.d(TAG, "capture.getImageFile() - " + capture.getImageFile());
            File file = new File(capture.getImageFile());
            Log.d(TAG, "//////////////////////////////////////////////////////");
            Log.d(TAG, "1 id - " + capture.getID());
            Log.d(TAG, "2 imageSize - " + capture.getSize());
            Log.d(TAG, "3 aesKey - " + capture.getAesKey());
            Log.d(TAG, "4 imageFile - " + capture.getImageFile());
            Log.d(TAG, "5 encryption - " + capture.getEncryption());
            Log.d(TAG, "6 imageStatus - " + capture.getImageStatus());
            Log.d(TAG, "7 noOfAttempts - " + capture.getNoOfAttempts());
            Log.d(TAG, "8 deviceData - " + capture.getDeviceData());
            Log.d(TAG, "//////////////////////////////////////////////////////");
            Log.d(TAG, "file - " + file);
            int sizeInt = (int) file.length();
            byte[] bytesImage = new byte[sizeInt];
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            int readStatus = buf.read(bytesImage, 0, bytesImage.length);
            Log.d(TAG, "size ->" + sizeInt);
            Log.d(TAG, "readStatus ->" + readStatus);
            String size = Integer.toString(sizeInt);

            if (readStatus != -1) {
                // String encodedImage = Base64.encodeToString(bytesImage,Base64.DEFAULT);
                OkHttpClient client = new OkHttpClient();
                String aesKeyValue = capture.getAesKey();

                aesKeyValue = (aesKeyValue != null) ? aesKeyValue : "NOT_ENCRYPTED";
                Log.d(TAG, "######### aesKeyValue :: " + aesKeyValue);

                RequestBody formBody = new FormBody.Builder()
                        .add("id", capture.getID())
                        .add("image_size", size)
                        .add("image_size_original", size)
                        .add("image_status", imageStatus)
                        .add("exception", result)
                        .add("printStackTrace", stackTrace)
                        .add("md5checksumImg", md5checksumImg)
                        .add("md5checksumEncImg", md5checksumEncImg)
                        .add("no_of_attempts", capture.getNoOfAttempts())
                        .add("encoded_aes_key", aesKeyValue)
                        .add("my_pid", String.valueOf(android.os.Process.myPid()))
                        .add("my_tid", String.valueOf(android.os.Process.myTid()))
                        .add("image", Base64.encodeToString(bytesImage, Base64.DEFAULT))
                        .add("device_data", capture.getDeviceData())
                        .build();

                Request request = new Request.Builder()
                        .url(capture.getUrl())
                        .post(formBody)
                        .build();

                Response response = client.newCall(request).execute();

                if (response.isSuccessful()) {
                    String rp = response.body().string();
                    try {
                        JSONObject object = new JSONObject(rp);
                        Log.d(TAG, "-----------------response------------------");
                        Log.d(TAG, String.valueOf(object));
                        if (object.getBoolean("success")) {
                            isSuccess = true;
                            Log.d(TAG, "###### Image Upload Success ####");
                            Log.d(TAG, "Success - " + capture.getID());
                            analytics.logEvent("image_upload_success","postData");
                            //TODO : add backend response message
                        } else {
                            Log.d(TAG, "XXXXXX ServerError - Image Upload Failed XXXXX");
                            Log.d(TAG, "Failed - ServerError :" + capture.getID());
                            analytics.logEvent("image_upload_fail","postData");
                            analytics.logExceptionEvent("image_upload_fail", "postData" , "ServerError", "ServerError : id : "+ capture.getID());
                            //TODO : add backend error message
                        }
                    } catch (JSONException e) {
                        Log.d(TAG, "XXXXXX ServerError - Image Upload Failed XXXXX");
                        Log.d(TAG, "xxxxx ServerError -  response");
                        Log.d(TAG, rp);
                        Log.d(TAG, "ServerException - JSONException : XXXX Image Upload faild - " + e.getMessage());
                        e.printStackTrace();
                        analytics.logExceptionEvent("image_upload_fail","postData" , "JSONException", "JSONException : id : "+ capture.getID() + " - " + e.getMessage() );
                        Logger.logHandler();
                    }
                } else {
                    Log.d(TAG, "ServerException - Not Success : XXXXXX Image Upload Failed XXXXX");
                    analytics.logExceptionEvent("image_upload_fail","postData" , "ServerException", "Server IOException : id : "+ capture.getID());
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "Failed 1 - IOException : " + e.getMessage());
            e.printStackTrace();
            analytics.logExceptionEvent("image_upload_exception","postData" , "IOException", "Server IOException : id : "+ capture.getID() + " - "+ e.getMessage() );
            Logger.logHandler();
        } catch (Exception e) {
            Log.d(TAG, "Failed 2 - Exception : " + e.getMessage());
            e.printStackTrace();
            analytics.logExceptionEvent("image_upload_exception","postData" , "Exception", "Server Exception : id : "+ capture.getID() + " - "+ e.getMessage() );
            Logger.logHandler();
        }

        return isSuccess;
    }

    private void deleteAllImages() {
        Log.d(TAG, "####################### deleteAllImages ###########################");
        Log.d(TAG, "deleteAllImages :: Directory : " + context.getCacheDir().getAbsolutePath());
        //String folderName= "DialogSADocuments";
        // File imgDirectory = new File(Environment.getExternalStorageDirectory().toString() + "/" + folderName);

        File imgDirectory = new File(context.getCacheDir().getAbsolutePath());

        if (imgDirectory.isDirectory()) {
            String[] children = imgDirectory.list();
            for (int i = 0; i < children.length; i++) {
                new File(imgDirectory, children[i]).delete();
                Log.d(TAG, "deleteAllImages :: imgDirectory : " + children[i]);
            }
        }

    }

    public static byte[] createChecksum(String filename) throws
            Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        return complete.digest();
    }

    // see this How-to for a faster way to convert
    // a byte array to a HEX string
    public static String getMD5Checksum(String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result +=
                    Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

}
