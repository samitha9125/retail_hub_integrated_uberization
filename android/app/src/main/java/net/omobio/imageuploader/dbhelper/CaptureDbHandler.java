package net.omobio.imageuploader.dbhelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CaptureDbHandler extends SQLiteOpenHelper {

    // All Static variables

    private static final String OK_TEXT = "ok";

    private static final String TAG = "CaptureDbHandler";

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "captureStore";

    // Image table name
    private static final String TABLE_IMAGES = "images";

    // Image table name
    private static final String TABLE_USER_DATA = "user";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_URL = "url";
    private static final String KEY_IMG_FILE = "image_file";
    private static final String KEY_SIZE = "size";
    private static final String KEY_AES = "aes_key";
    private static final String KEY_ENCRYPTION = "encryption";
    private static final String KEY_IMAGE_STATUS = "image_status";
    private static final String KEY_NO_OF_ATTEMPTS = "no_of_attempts";
    private static final String KEY_DEVICE_DATA = "device_data";

    //SQL queries
    private static final String CREATE_IMAGE_TABLE = "CREATE TABLE " + TABLE_IMAGES + "(" + KEY_ID + " TEXT PRIMARY KEY," + KEY_URL
            + " TEXT," + KEY_IMG_FILE + " TEXT," + KEY_SIZE + " TEXT," + KEY_AES + " TEXT," + KEY_ENCRYPTION + " TEXT," + KEY_IMAGE_STATUS + " TEXT," +
            KEY_NO_OF_ATTEMPTS + " TEXT," + KEY_DEVICE_DATA + " TEXT" + ")";

    private static final String ALTER_IMAGE_TABLE = "ALTER TABLE "
            + TABLE_IMAGES + " ADD COLUMN " + KEY_DEVICE_DATA + " TEXT;";

    private static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USER_DATA + "(" +
            "id PRIMARY KEY," +
            "deviceData TEXT)";

    private static CaptureDbHandler sInstance;

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     */
    private CaptureDbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "Constructor");
    }

    public static synchronized CaptureDbHandler getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new CaptureDbHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate");
        db.execSQL(CREATE_IMAGE_TABLE);
        db.execSQL(CREATE_USER_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade");
        if (oldVersion < 2) {
            db.execSQL(ALTER_IMAGE_TABLE);
        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     *
     * @throws IOException
     */

    // Adding new Image
    synchronized public HashMap<String, String> addCapture(Capture capture) {
        Log.d(TAG, "addCapture");
        HashMap<String, String> dbResult = new HashMap<>();
        String message = OK_TEXT;
        String stackTrace = "";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Log.d(TAG, "TRY - addCapture");
            ContentValues values = new ContentValues();
            values.put(KEY_ID, capture.getID());
            values.put(KEY_URL, capture.getUrl());
            values.put(KEY_IMG_FILE, capture.getImageFile());
            values.put(KEY_SIZE, capture.getSize());
            values.put(KEY_AES, capture.getAesKey());
            values.put(KEY_ENCRYPTION, capture.getEncryption());
            values.put(KEY_IMAGE_STATUS, capture.getImageStatus());
            values.put(KEY_NO_OF_ATTEMPTS, capture.getNoOfAttempts());
            values.put(KEY_DEVICE_DATA, capture.getDeviceData());
            // Inserting Row
            db.insert(TABLE_IMAGES, null, values);
            //throw new Exception(); //manual error //TODO
        } catch (Exception e) {
            Log.e(TAG, "addCapture -  Exception: " + e.getMessage() + " Cause: " + e.getCause());
            message = "Exception - " + e.getMessage();
            stackTrace = Log.getStackTraceString(e);
            e.printStackTrace();
        } finally {
            db.close(); // Closing database connection
            dbResult.put("message", message);
            dbResult.put("stackTrace", stackTrace);
        }

        return dbResult;
    }

    // Getting single contact
    synchronized Capture getCapture(String id) {
        Log.d(TAG, "getCapture");
        Capture capture = null;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_IMAGES,
                new String[]{KEY_ID, KEY_URL, KEY_IMG_FILE, KEY_SIZE, KEY_AES, KEY_ENCRYPTION, KEY_IMAGE_STATUS, KEY_NO_OF_ATTEMPTS, KEY_DEVICE_DATA}, KEY_ID + "=?",
                new String[]{id}, null, null, null, null);
        if ((cursor != null) && cursor.moveToFirst()) {
            capture = new Capture(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            cursor.close();
        }

        return capture;
    }

    synchronized void addDeviceDataToDb(String msisdn) {

        Log.d(TAG, "addDeviceData");
        SQLiteDatabase db = this.getWritableDatabase();

        if (this.getUserDataCount() == 0) {
            ContentValues values = new ContentValues();
            values.put("deviceData", msisdn);

            // Inserting Row
            db.insert(TABLE_USER_DATA, null, values);
        }
        db.close(); // Closing database connection
    }

    // Getting All Contacts
    synchronized public List<Capture> getAllCaptures() {
        Log.d(TAG, "getAllCaptures");
        List<Capture> captureList = new ArrayList<Capture>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_IMAGES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if ((cursor != null) && cursor.moveToFirst()) {
            do {
                Capture capture = new Capture();
                capture.setID(cursor.getString(0));
                capture.setUrl(cursor.getString(1));
                capture.setImageFile(cursor.getString(2));
                capture.setSize(cursor.getString(3));
                capture.setAesKey(cursor.getString(4));
                capture.setEncryption(cursor.getString(5));
                capture.setImageStatus(cursor.getString(6));
                capture.setNoOfAttempts(cursor.getString(7));
                capture.setDeviceData(cursor.getString(8));

                // Adding capture to list
                captureList.add(capture);
            } while (cursor.moveToNext());
        }

        cursor.close();

        // return capture list
        return captureList;
    }

    // Updating single capture
    synchronized public int updateCapture(Capture capture) {
        Log.d(TAG, "updateCapture");
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_URL, capture.getUrl());
        values.put(KEY_IMG_FILE, capture.getImageFile());
        values.put(KEY_SIZE, capture.getSize());
        values.put(KEY_AES, capture.getAesKey());
        values.put(KEY_IMAGE_STATUS, capture.getImageStatus());
        values.put(KEY_NO_OF_ATTEMPTS, capture.getNoOfAttempts());
        values.put(KEY_DEVICE_DATA, capture.getDeviceData());

        // updating row
        return db.update(TABLE_IMAGES, values, KEY_ID + " = ?", new String[]{capture.getID()});
    }

    // Deleting single capture
    synchronized public void deleteCapture(Capture capture) {
        Log.d(TAG, "deleteCapture");

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_IMAGES, KEY_ID + " = ?", new String[]{capture.getID()});
        db.close();
    }

    // Getting capture Count
    synchronized public int getCaptureCount() {
        Log.d(TAG, "getCaptureCount");
        int count;

        String countQuery = "SELECT * FROM " + TABLE_IMAGES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    // Getting image name
    synchronized public List<String> queryImageName(String imageName) {

        Log.d(TAG, "queryImageName");
        List<String> images = new ArrayList<String>();

        String imageQuery = "SELECT * FROM " + TABLE_IMAGES + " WHERE image_file = '" + imageName + "'";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(imageQuery, null);

        // looping through all rows and adding to list
        if ((cursor != null) && cursor.moveToFirst()) {
            do {
                // Adding capture to list
                images.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        cursor.close();
        // return capture list
        return images;
    }

    // Getting capture Count
    synchronized public int getUserDataCount() {

        Log.d(TAG, "getUserDataCount");
        int count = 0;

        String countQuery = "SELECT * FROM " + TABLE_USER_DATA;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Log.e(TAG, "getUserDataCount : try");
            db.execSQL(CREATE_USER_TABLE);
            Cursor cursor = db.rawQuery(countQuery, null);
            count = cursor.getCount();
            cursor.close();

        } catch (Exception e) {
            Log.e(TAG, "getUserDataCount: " + e.getMessage() + " Cause: " + e.getCause());
            e.printStackTrace();
        }

        return count;
    }

    // Getting UserData
    synchronized public String getUserData() {
        Log.d(TAG, "getUserData");
        String userDataString = "{}";
        SQLiteDatabase db = this.getWritableDatabase();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_USER_DATA;

        try {
            Log.e(TAG, "getUserData : try");
            Cursor cursor = db.rawQuery(selectQuery, null);
            if ((cursor != null) && cursor.moveToFirst()) {
                userDataString = cursor.getString(1);
            }

            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, "getUserData: " + e.getMessage() + " Cause: " + e.getCause());
            e.printStackTrace();
        }

        return userDataString;
    }
}
