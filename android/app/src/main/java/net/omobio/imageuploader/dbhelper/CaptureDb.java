package net.omobio.imageuploader.dbhelper;

import android.content.Context;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;

import net.omobio.imageuploader.analyticshelper.FirebaseUtils;

import java.io.File;
import java.util.HashMap;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
/**
 * This class store captured image details in SQLite db
 */
public class CaptureDb {

    private static final String TAG = "CaptureDbPlugin";
    private static final String OK_TEXT = "ok";
    private static final int MAXIMUM_NO_OF_ATTEMPTS = 20;
    private int dbUpdateAttemptsCount = 0;
    private CaptureDbHandler imgDb;
    private FirebaseUtils analytics;
    private FirebaseAnalytics mFirebaseAnalytics;

    public CaptureDb(Context context) {
        Log.d(TAG, "CaptureDb");
        imgDb = CaptureDbHandler.getInstance(context);
        analytics = new FirebaseUtils(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void addToCaptureDb(ReadableMap args, Callback errorCallback, Callback successCallBack) {

        Log.d(TAG, "addToCaptureDb");
        analytics.logEvent("add_to_capture_db_start","addToCaptureDb" );
        String id = "0";
        String url, imgFile, uploadSelector, deviceData;
        dbUpdateAttemptsCount++;
        Log.d(TAG, "addToCaptureDb - dbUpdateAttemptsCount : " + dbUpdateAttemptsCount);
        Bundle params = new Bundle();
        try {
            Log.d(TAG, "ReadableMap " + args);
            id = args.getString("id");
            url = args.getString("url");
            imgFile = args.getString("imageFile");
            uploadSelector = args.getString("uploadSelector");
            if(args.hasKey("deviceData")) {
                deviceData = args.getString("deviceData");
            } else {
                deviceData = "{}";
            }
            Log.d(TAG, "ID: " + id);
            Log.d(TAG, "URL: " + url);
            Log.d(TAG, "Image File: " + imgFile);
            Log.d(TAG, "uploadSelector: " + uploadSelector);
            Log.d(TAG, "deviceData: " + deviceData);

            File outFile = new File(imgFile);
            Capture capture = imgDb.getCapture(id);

            
            params.putString("ID:", id);
            params.putString("URL:", url);
            // params.putString("Image_File:", imgFile);

            mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb_1", params);

            if (capture == null) {
                capture = new Capture(id, url, outFile.getAbsolutePath(), Long.toString(outFile.length()), null, uploadSelector, "not_ok", "0", deviceData);

                HashMap<String, String> dbUpdateResult = imgDb.addCapture(capture);

                if(dbUpdateResult.get("message").equals(OK_TEXT)) {
                    Log.d(TAG, "DB Update SUCCESS");
                    successCallBack.invoke(true);
                    mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb 2", params);

                } else {
                    Log.d(TAG, "DB Update FAILED");
                    //Re try  - 20 attempt
                    if(dbUpdateAttemptsCount <= MAXIMUM_NO_OF_ATTEMPTS) {
                        Log.d(TAG, "DB Update FAILED - RETRY : dbUpdateAttemptsCount : "+ dbUpdateAttemptsCount);
                        Thread.sleep(1000);
                        this.addToCaptureDb(args, errorCallback, successCallBack);
                        mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb 3", params);
                    } else {
                        Log.d(TAG, "DB Update ERROR FINAL - "+ dbUpdateResult.get("message"));
                        errorCallback.invoke(dbUpdateResult.get("message"));
                        mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb 4", params);
                        analytics.logExceptionEvent("add_to_capture_db_error", "addToCaptureDb" , "Error", "Error - addToCaptureDb - id : "+ id + " "+ dbUpdateResult.get("message"));
                        analytics.logImportantExceptionEvent("add_to_capture_db_error", "addToCaptureDb" , "Error", "Error - addToCaptureDb - id : "+ id + " "+ dbUpdateResult.get("message"));
                    }
                }

            } else {
                capture.setImageFile(imgFile);
                Log.d(TAG, "updateCapture ELSE");
                imgDb.updateCapture(capture);
                mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb 5", params);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "addToCaptureDb :: Exception");
            errorCallback.invoke(e.getMessage());
            params.putString("Exception :", e.getMessage());
            mFirebaseAnalytics.logEvent("add_to_capture_db addToCaptureDb 6", params);
            analytics.logExceptionEvent("add_to_capture_db_ex", "addToCaptureDb" , "Exception", "Exception - addToCaptureDb - id : "+ id + " "+  e.getMessage());
            analytics.logImportantExceptionEvent("add_to_capture_db_ex", "addToCaptureDb" , "Exception", "Exception - addToCaptureDb - id : "+ id + " "+ e.getMessage());

        }
    }

    public void addDeviceDataToDb(ReadableMap args, Callback errorCallback, Callback successCallBack) {

        Log.d(TAG, "addDeviceDataToDb");
        analytics.logEvent("add_to_device_data_start","addDeviceDataToDb");
        String userData;

        try {
            Log.d(TAG, "addDeviceDataToDb :: ReadableMap " + args);

            if(args.hasKey("deviceData")) {
                userData = args.getString("deviceData");
            } else {
                userData = "";
            }
            Log.d(TAG, "userData: " + userData);

            imgDb.addDeviceDataToDb(userData);
            successCallBack.invoke(true);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "addDeviceDataToDb :: JSONException");
            errorCallback.invoke(e.getMessage());
        }

    }


}
