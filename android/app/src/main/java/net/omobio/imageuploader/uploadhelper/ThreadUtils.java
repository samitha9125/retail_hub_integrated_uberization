package net.omobio.imageuploader.uploadhelper;

import android.content.Context;
import android.util.Log;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Timestamp;

public class ThreadUtils {

    private Context reactContext = null;

    public ThreadUtils(Context reactContext) {

        this.reactContext = reactContext;

    }

    public boolean isValidLockFile() {
        Log.d("ImageUploader", "########## isValidLockFile");
        if (!this.isLockFileExists()) {
            Log.d("ImageUploader", "isValidLockFile  #1");
            this.createLockFile();
            return true;
        } else if (!this.isValidProcessId()) {
            Log.d("ImageUploader", "isValidLockFile  #2");
            this.updateLockFile();
            return true;
        } else {
            Log.d("ImageUploader", "isValidLockFile #3");
            return false;
        }
    }

    public boolean isValidProcessId() {
        Log.d("ImageUploader", "########## isValidProcessId");

        String lockFileTxt = this.readLockFile();
        String[] strArray = lockFileTxt.split("\\_");
        boolean processStatus = false;
        try {
            int currentProcessId = android.os.Process.myPid();
            int savedPid = Integer.valueOf(strArray[1]);
            Log.d("ImageUploader", "isValidProcessId :: currentProcessId => " + currentProcessId);
            Log.d("ImageUploader", "isValidProcessId :: savedPid => " + savedPid);
            processStatus = currentProcessId == savedPid;

        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d("ImageUploader", "isValidProcessId :: ArrayIndexOutOfBoundsException - " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d("ImageUploader", "isValidProcessId :: Exception - " + e.getMessage());
            e.printStackTrace();
        }

        return processStatus;
    }

    public boolean isValidTimeStamp() {
        Log.d("ImageUploader", "########## isValidTimeStamp");
        String lockFileTxt = this.readLockFile();
        String[] strArray = lockFileTxt.split("\\_");
        Log.d("ImageUploader", "isValidTimeStamp :: lockFileTxt - Time => " + strArray[0]);

        return (Long.valueOf(ThreadUtils.getTimeStampMill()) - Long.valueOf(strArray[0])) >= 60000;
    }


    public void createLockFile() {
        Log.d("ImageUploader", "@@@ createLockFile");

        try {
            File lockFileDirectory = new File(this.reactContext.getCacheDir().getAbsolutePath());
            Log.d("ImageUploader", "createLockFile :: lockFileDirectory At => " + lockFileDirectory);
            File lockFile = new File(lockFileDirectory, "threadLock.txt");
            Log.d("ImageUploader", "createLockFile :: lockFile => " + lockFile);
            FileOutputStream is = new FileOutputStream(lockFile);
            OutputStreamWriter osw = new OutputStreamWriter(is);
            Writer w = new BufferedWriter(osw);
            w.write(getTimeStampMill() + '_' + android.os.Process.myPid());
            w.close();
        } catch (IOException e) {
            Log.d("ImageUploader", "createLockFile :: Problem writing to the file threadLock.txt - " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d("ImageUploader", "createLockFile :: Exception - " + e.getMessage());
            e.printStackTrace();
        }

    }

    public void updateLockFile() {
        // Logger.uploadAllLogs();
        Log.d("ImageUploader", "@@@@ updateLockFile");

        try {
            File lockFileDirectory = new File(this.reactContext.getCacheDir().getAbsolutePath());
            Log.d("ImageUploader", "updateLockFile :: lockFileDirectory At => " + lockFileDirectory);
            File lockFile = new File(lockFileDirectory, "threadLock.txt");
            Log.d("ImageUploader", "updateLockFile :: lockFile => " + lockFile);

            if (lockFile.exists()) {
                Log.d("ImageUploader", "updateLockFile :: lockFile exists");
                lockFile.delete();

            } else {
                Log.d("ImageUploader", "updateLockFile :: lockFile not exists");
            }

            FileOutputStream is = new FileOutputStream(lockFile);
            OutputStreamWriter osw = new OutputStreamWriter(is);
            Writer w = new BufferedWriter(osw);
            w.write(getTimeStampMill() + '_' + android.os.Process.myPid());
            w.close();
        } catch (IOException e) {
            Log.d("ImageUploader", "updateLockFile :: Problem writing to the file threadLock.txt - " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d("ImageUploader", "updateLockFile :: Exception - " + e.getMessage());
            e.printStackTrace();
        }

    }

    public String readLockFile() {
        Log.d("ImageUploader", "readLockFile");
        File lockFileDirectory = new File(this.reactContext.getCacheDir().getAbsolutePath());
        Log.d("ImageUploader", "readLockFile :: lockFileDirectory At => " + lockFileDirectory);
        File lockFile = new File(lockFileDirectory, "threadLock.txt");
        // This will reference one line at a time
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String lockFileTxt = null;
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(lockFile);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            lockFileTxt = stringBuilder.toString();
            // Always close files.
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            Log.d("ImageUploader", "readLockFile :: Unable to open file '" + lockFile + "'");
            Log.d("ImageUploader", "readLockFile :: FileNotFoundException - " + e.getMessage());
        } catch (IOException e) {
            Log.d("ImageUploader", "readLockFile :: Problem reading from the file threadLock.txt - " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d("ImageUploader", "readLockFile :: Exception - " + e.getMessage());
            e.printStackTrace();
        }

        return lockFileTxt;
    }


    public boolean isLockFileExists() {
        Log.d("ImageUploader", "isLockFileExists");
        File lockFileDirectory = new File(this.reactContext.getCacheDir().getAbsolutePath());
        Log.d("ImageUploader", "isLockFileExists :: lockFileDirectory At => " + lockFileDirectory);
        File lockFile = new File(lockFileDirectory, "threadLock.txt");
        if (lockFile.exists()) {
            Log.d("ImageUploader", "isLockFileExists :: exists");
            return true;

        } else {
            Log.d("ImageUploader", "isLockFileExists :: not exists");
            return false;
        }

    }


    public static String getTimeStampMill() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long time = timestamp.getTime();
        Log.d("ImageUploader", "getTimeStampMill => " + String.valueOf(time));

        return String.valueOf(time);
    }

}
