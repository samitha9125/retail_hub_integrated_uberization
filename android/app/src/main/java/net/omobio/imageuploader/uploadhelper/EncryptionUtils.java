package net.omobio.imageuploader.uploadhelper;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtils {

    private static CipherInputStream cipherIn = null;
    private static Cipher cipher = null;
    private static FileOutputStream fos = null;
    private static String TAG = "EncryptionUtils";
    private Context context = null;
    public static Key aesKey = null;
    public static PublicKey publicKey = null;

    public EncryptionUtils(Context context) {
        this.context = context;
        try {
            Log.d(TAG, "EncryptionUtils");
            cipher = Cipher.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, "EncryptionUtils - NoSuchAlgorithmException");
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            Log.d(TAG, "EncryptionUtils - NoSuchPaddingException");
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(TAG, "EncryptionUtils: " + e.getMessage() + " Cause: " + e.getCause());
            e.printStackTrace();
        }
    }

    public Key aesKeyGen() {
        // Set up secret key spec for 128-bit AES encryption and decryption
        Key sks = null;

        try {
            cipher = Cipher.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(("DSA random seed" + Math.random() * 100).getBytes());
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128, sr);
            sks = new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");
        } catch (Exception e) {
            Log.e(TAG, "AES secret key spec error> Message: " + e.getMessage() + " Cause: " + e.getCause());
        }
        //	aesKey = sks;
        return sks;
    }

    public byte[] rsaEncrypt(byte[] message, PublicKey publickey) {
        // Encode the original data with RSA private key
        byte[] encodedBytes = null;
        try {
            Log.d(TAG, "rsaEncrypt is running...");
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.ENCRYPT_MODE, publickey);
            encodedBytes = c.doFinal(message);
        } catch (Exception e) {
            Log.d(TAG, "rsaEncrypt - Exception");
            e.printStackTrace();
        }

        return encodedBytes;
    }

    public HashMap<String, String> aesEncrypt(File inFile, File outFile, Key key) {
        HashMap<String, String> encryptionResult = new HashMap<>();
        String result = "ok";
        String stackTrace = "";
        try {
            Log.d(TAG, "aesEncrypt is running...");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            cipherIn = new CipherInputStream(new FileInputStream(inFile), cipher);
            fos = new FileOutputStream(outFile);
            int i;
            while ((i = cipherIn.read()) != -1) {
                fos.write(i);
            }

            fos.close();
            //TODO : throw new InvalidKeyException(); manual error
        } catch (InvalidKeyException e) {
            Log.d(TAG, "aesEncrypt - InvalidKeyException");
            result = "InvalidKeyException - " + e.getMessage();
            stackTrace = Log.getStackTraceString(e);
            e.printStackTrace();
            fileDelete(outFile);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "aesEncrypt - FileNotFoundException");
            result = "FileNotFoundException - " + e.getMessage();
            stackTrace = Log.getStackTraceString(e);
            e.printStackTrace();
            fileDelete(outFile);
        } catch (IOException e) {
            Log.d(TAG, "aesEncrypt - IOException");
            result = "IOException - " + e.getMessage();
            stackTrace = Log.getStackTraceString(e);
            e.printStackTrace();
            fileDelete(outFile);
        } catch (Exception e) {
            Log.e(TAG, "aesEncrypt: " + e.getMessage() + " Cause: " + e.getCause());
            result = "Exception - " + e.getMessage();
            stackTrace = Log.getStackTraceString(e);
            e.printStackTrace();
            fileDelete(outFile);
        } finally {
            try {
                cipherIn.close();
                Log.d(TAG, "aesEncrypt : cipherIn.close()");
            } catch (IOException e) {
                Log.e(TAG, "aesEncrypt : cipherIn " + e.getMessage() + " Cause: " + e.getCause());
                result = result + "\n--IOException - cipherIn.close() - " + e.getMessage();
                stackTrace = stackTrace + "\n--" + Log.getStackTraceString(e);
                e.printStackTrace();
            }
        }
        encryptionResult.put("result", result);
        encryptionResult.put("stackTrace", stackTrace);
        return encryptionResult;
    }

    private void fileDelete(File file) {
        try {
            file.delete();
            Log.d(TAG, "fileDelete : file.delete()");
        } catch (SecurityException e) {
            Log.e(TAG, "fileDelete :" + e.getMessage() + " Cause: " + e.getCause());
            e.printStackTrace();
        }
    }

//	public void aesEncrypt(byte[] message, File outFile, Key key) {
//		byte[] encrypted = this.aesEncrypt(message, key);
//		FileOutputStream fos = null;
//
//		try{
//		    // create new file output stream
//	        fos=new FileOutputStream(outFile);
//
//	        // writes bytes to the output stream
//	        fos.write(encrypted);
//
//	        // flush
//	        fos.flush();
//	        fos.close();
//
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

    public byte[] aesEncrypt(byte[] message, Key key) {
        byte[] encrypted = null;
        SecretKeySpec skeySpec = new SecretKeySpec(key.getEncoded(), "AES");
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            encrypted = cipher.doFinal(message);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "aesEncrypt byte[]  - Exception");
            e.printStackTrace();
        }

        return encrypted;
    }

    public byte[] aesEncrypt(byte[] message) {
        byte[] encrypted = null;
        SecretKeySpec skeySpec = new SecretKeySpec(aesKeyGen().getEncoded(), "AES");
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            encrypted = cipher.doFinal(message);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "aesEncrypt byte[] message  - Exception");
            e.printStackTrace();
        }

        return encrypted;
    }

    public PublicKey getPublicKey() {
        PublicKey pubKey = null;
        try {
            Log.d(TAG, "getPublicKey");
            ObjectInputStream inputStream = new ObjectInputStream(context
                    .getAssets().open("public.key"));
            pubKey = (PublicKey) inputStream.readObject();
            inputStream.close();
        } catch (Exception e) {
            Log.d(TAG, "getPublicKey - Exception");
            e.printStackTrace();
        }
        return pubKey;
    }
}
