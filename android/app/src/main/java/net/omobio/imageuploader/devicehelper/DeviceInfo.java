package net.omobio.imageuploader.devicehelper;

import android.content.Context;
import android.util.Log;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;

/**
 * Created by Damith Nuwan Sampath on 5/22/2018.
 */

public class DeviceInfo {

    public static final String TAG = "DeviceInfo";
    public Callback errorCallback, successCallBack;
    public Context context = null;
    public ReadableMap args;

    public DeviceInfo(Context context, ReadableMap args, Callback errorCallback, Callback successCallBack) {
        Log.d(TAG, "Constructor");
        this.context = context;
        this.args = args;
        this.errorCallback = errorCallback;
        this.successCallBack = successCallBack;
    }

    /**
     * Returns the unique identifier for the device
     *
     * @return unique identifier for the device
     */
    public String getDeviceIMEI() {
        String imei = "getDeviceIMEI";
        Log.d(TAG, "getDeviceIMEI");
        
        return imei;
    }


    /**
     * Returns the unique identifier for the device sim
     *
     * @return unique identifier for the device sim
     */
    public String getDeviceIMSI() {
        String imsi = "getDeviceIMEI";
        Log.d(TAG, "getDeviceIMSI");
        
        
        
        return imsi;
    }
    
}
