package net.omobio.imageuploader.dbhelper;

/**
 * @author eranga
 */
public class Capture {
    // Private variables
    private String _id;
    private String _url;
    private String _image_file;
    private String _size;
    private String _aes_key;
    private String _encryption;
    private String _image_status;
    private String _no_of_attempts;
    private String _device_data;

    // Empty constructor
    public Capture() {

    }

    // Constructor
    public Capture(String id, String url, String image_file, String size, String aes, String encrypt, String image_status, String no_of_attempts, String device_data) {
        this._id = id;
        this._url = url;
        this._image_file = image_file;
        this._size = size;
        this._aes_key = aes;
        this._encryption = encrypt;
        this._image_status = image_status;
        this._no_of_attempts = no_of_attempts;
        this._device_data = device_data;
    }

    // Get ID
    public String getID() {
        return this._id;
    }

    // Set id
    public void setID(String id) {
        this._id = id;
    }

    // Get Url
    public String getUrl() {
        return this._url;
    }

    // Set Url
    public void setUrl(String url) {
        this._url = url;
    }

    // Get image file
    public String getImageFile() {
        return this._image_file;
    }

    // Set image file
    public void setImageFile(String image_file) {
        this._image_file = image_file;
    }

    // Get image size
    public String getSize() {
        return this._size;
    }

    // Set image size
    public void setSize(String _size) {
        this._size = _size;
    }

    // Get AES key
    public String getAesKey() {
        return this._aes_key;
    }

    // Set AES key
    public void setAesKey(String aes_key) {
        this._aes_key = aes_key;
    }

    // Get image Encryption
    public String getEncryption() {
        return this._encryption;
    }

    // Set Encryption
    public void setEncryption(String encrypt) {
        this._encryption = encrypt;
    }

    // Get Image status
    public String getImageStatus() {
        return this._image_status;
    }

    // Set Image status
    public void setImageStatus(String image_status) {
        this._image_status = image_status;
    }

    // Get No of attempts
    public String getNoOfAttempts() {
        return this._no_of_attempts;
    }

    // Set No of attempts
    public void setNoOfAttempts(String no_of_attempts) {
        this._no_of_attempts = no_of_attempts;
    }

    //get device dat
    public String getDeviceData() {
        return this._device_data;
    }

    //set device dat
    public void setDeviceData(String device_data) {
        this._device_data = device_data;
    }
}
