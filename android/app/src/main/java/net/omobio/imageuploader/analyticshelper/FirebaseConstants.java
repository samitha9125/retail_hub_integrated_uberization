package net.omobio.imageuploader.analyticshelper;

public class FirebaseConstants {
    public static final String EVENT_IMAGE_UPLOAD_EXCEPTION = "uploader_exception";
    public static final String EVENT_IMAGE_UPLOAD_IMPORTANT_EXCEPTION = "uploader_im_exception";
    public static final String EVENT_IMAGE_UPLOAD_PROCESS = "uploader_process";
    public static final String EVENT_IMAGE_STATUS = "uploader_img_status";

    public static final String PARAMS_EXCEPTION_METHOD = "ex_method";
    public static final String PARAMS_EXCEPTION_NAME = "ex_name";
    public static final String PARAMS_EXCEPTION_MESSAGE = "ex_message";
    public static final String PARAMS_PROCESS_NAME = "process_name";
    public static final String PARAMS_PID = "process_id";
    public static final String PARAMS_TID = "thread_id";
    public static final String PARAMS_METHOD_NAME = "method";
    public static final String PARAMS_NO_OF_IMAGES = "no_of_images";
    public static final String PARAMS_IMAGE_ID = "image_id";
    public static final String PARAMS_IMAGE_ENCRYPTION_STATUS = "encryption_status";
        }
