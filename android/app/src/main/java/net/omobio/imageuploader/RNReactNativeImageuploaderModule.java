
/*
 * File: RNReactNativeImageuploaderModule.java
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 11:51:22 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
package net.omobio.imageuploader;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import android.support.v4.content.FileProvider;

import net.omobio.imageuploader.dbhelper.CaptureDb;
import net.omobio.imageuploader.uploadhelper.ImageUploader;
import net.omobio.imageuploader.uploadhelper.ThreadUtils;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class RNReactNativeImageuploaderModule extends ReactContextBaseJavaModule {

    public static final String TAG = "RNImageuploaderModule";

    private final ReactApplicationContext reactContext;
    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";

    public RNReactNativeImageuploaderModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNReactNativeImageuploader";
    }

    @ReactMethod
    public void showToast(String message, int duration) {
        Toast.makeText(getReactApplicationContext(), message, duration).show();

    }

    @ReactMethod
    public void addToDb(ReadableMap options, Callback errorCallback, Callback successCallBack) {
        CaptureDb db = new CaptureDb(getReactApplicationContext());
        db.addToCaptureDb(options, errorCallback, successCallBack);

    }

    @ReactMethod
    public void addDeviceDataToDb(ReadableMap options, Callback errorCallback, Callback successCallBack) {
        CaptureDb db = new CaptureDb(getReactApplicationContext());
        db.addDeviceDataToDb(options, errorCallback, successCallBack);

    }

    @ReactMethod
    public void startUpload(int flag, Callback errorCallback, Callback successCallBack) {
        Log.d(TAG, "#### startUpload - " + android.os.Process.myPid());

        synchronized (ImageUploader.class) {
            if (!ImageUploader.isAlive()) {
                ImageUploader imageUploader = new ImageUploader(getReactApplicationContext(), flag, errorCallback, successCallBack);
                ThreadUtils threadUtils = new ThreadUtils(this.reactContext);
                Log.d("ImageUploader", "######## readLockFile =====> " + threadUtils.readLockFile());

                if (threadUtils.isValidLockFile()) {
                    Log.d(TAG, "#### startUpload => start new Image Uploader thread");
                    imageUploader.start();
                } else {
                    Log.d(TAG, "#### startUpload => thread Image Uploader is already running 2");
                }
            } else {
                Log.d(TAG, "#### startUpload => thread Image Uploader is Alive");
            }
        }
    }

    @ReactMethod
    public void getDeviceInfo(ReadableMap options, Callback errorCallback, Callback successCallBack) {
        Log.d(TAG, "#### getDeviceInfo");
        Toast.makeText(getReactApplicationContext(), "Hello", Toast.LENGTH_LONG).show();
    }

    @ReactMethod
    public void installApk(String path) {
        Log.d(TAG, "#### installApk");
        File newFile = new File(path);
        Uri contentUri = null;
        Log.d(TAG, "#### installApk :: File : " + newFile);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.d(TAG, "#### installApk :: Build.VERSION : " + Build.VERSION.SDK_INT);
                final String packageName = getCurrentActivity().getPackageName();
                final String authority = new StringBuilder(packageName).append(".provider").toString();
                contentUri = FileProvider.getUriForFile(getCurrentActivity(), authority, newFile);
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setData(contentUri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                this.reactContext.startActivity(intent);
            } else {
                Log.d(TAG, "#### installApk ::API 24 Build.VERSION : " + Build.VERSION.SDK_INT);
                Uri apkUri = Uri.fromFile(newFile);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.reactContext.startActivity(intent);
            }

        } catch (IllegalArgumentException e) {
            Log.d(TAG, "#### installApk :: IllegalArgumentException : " + e.getMessage());
            Toast.makeText(getReactApplicationContext(), "IllegalArgumentException : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "#### installApk :: Exception : " + e.getMessage());
            Toast.makeText(getReactApplicationContext(), "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }


}