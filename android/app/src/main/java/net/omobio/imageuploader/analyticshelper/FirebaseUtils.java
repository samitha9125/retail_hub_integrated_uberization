package net.omobio.imageuploader.analyticshelper;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import net.omobio.dialog.dialogra.BuildConfig;
import net.omobio.imageuploader.dbhelper.CaptureDbHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseUtils {
    private static final String TAG = "FirebaseUtils";
    private FirebaseAnalytics firebaseAnalytics;
    private CaptureDbHandler imgDb;

    public FirebaseUtils(Context reactContext) {

        firebaseAnalytics = FirebaseAnalytics.getInstance(reactContext);
        imgDb = CaptureDbHandler.getInstance(reactContext);

    }

    public void logEvent(String processName, String method) {
        Log.d(TAG, "Firebase :: logEvent");
        Bundle user_data = getUserData(imgDb);
        Bundle bundle = new Bundle();
        bundle.putAll(user_data);
        bundle.putString(FirebaseConstants.PARAMS_PROCESS_NAME, processName);
        bundle.putString(FirebaseConstants.PARAMS_METHOD_NAME, method);
        bundle.putString(FirebaseConstants.PARAMS_PID, String.valueOf(android.os.Process.myPid()));
        bundle.putString(FirebaseConstants.PARAMS_TID, String.valueOf(android.os.Process.myTid()));
        Log.d(TAG, "Firebase :: logEvent :: bundle " + bundle);
        firebaseAnalytics.logEvent(FirebaseConstants.EVENT_IMAGE_UPLOAD_PROCESS, bundle);

    }

    public void logExceptionEvent(String processName, String method, String exception, String message) {
        Log.d(TAG, "Firebase :: logExceptionEvent");
        Bundle user_data = getUserData(imgDb);
        Bundle bundle = new Bundle();
        bundle.putAll(user_data);
        bundle.putString(FirebaseConstants.PARAMS_PROCESS_NAME, processName);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_METHOD, method);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_NAME, exception);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_MESSAGE, message);
        bundle.putString(FirebaseConstants.PARAMS_METHOD_NAME, method);
        bundle.putString(FirebaseConstants.PARAMS_PID, String.valueOf(android.os.Process.myPid()));
        bundle.putString(FirebaseConstants.PARAMS_TID, String.valueOf(android.os.Process.myTid()));
        Log.d(TAG, "Firebase :: logExceptionEvent :: bundle " + bundle);
        firebaseAnalytics.logEvent(FirebaseConstants.EVENT_IMAGE_UPLOAD_EXCEPTION, bundle);

    }

    public void logImportantExceptionEvent(String processName, String method, String exception, String message) {
        Log.d(TAG, "Firebase :: logImportantExceptionEvent");
        Bundle user_data = getUserData(imgDb);
        Bundle bundle = new Bundle();
        bundle.putAll(user_data);
        bundle.putString(FirebaseConstants.PARAMS_PROCESS_NAME, processName);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_METHOD, method);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_NAME, exception);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_MESSAGE, message);
        bundle.putString(FirebaseConstants.PARAMS_METHOD_NAME, method);
        bundle.putString(FirebaseConstants.PARAMS_PID, String.valueOf(android.os.Process.myPid()));
        bundle.putString(FirebaseConstants.PARAMS_TID, String.valueOf(android.os.Process.myTid()));
        Log.d(TAG, "Firebase :: logImportantExceptionEvent :: bundle " + bundle);
        firebaseAnalytics.logEvent(FirebaseConstants.EVENT_IMAGE_UPLOAD_IMPORTANT_EXCEPTION, bundle);

    }

    public void logImageStatus(String no_of_images, String image_id, String encryption_status, String processName, String method, String exception, String message) {
        Log.d(TAG, "Firebase :: logImageStatus");
        Bundle user_data = getUserData(imgDb);
        Bundle bundle = new Bundle();
        bundle.putAll(user_data);
        bundle.putString(FirebaseConstants.PARAMS_PROCESS_NAME, processName);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_METHOD, method);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_NAME, exception);
        bundle.putString(FirebaseConstants.PARAMS_EXCEPTION_MESSAGE, message);
        bundle.putString(FirebaseConstants.PARAMS_METHOD_NAME, method);
        bundle.putString(FirebaseConstants.PARAMS_NO_OF_IMAGES, no_of_images);
        bundle.putString(FirebaseConstants.PARAMS_IMAGE_ID, image_id);
        bundle.putString(FirebaseConstants.PARAMS_IMAGE_ENCRYPTION_STATUS, encryption_status);
        bundle.putString(FirebaseConstants.PARAMS_PID, String.valueOf(android.os.Process.myPid()));
        bundle.putString(FirebaseConstants.PARAMS_TID, String.valueOf(android.os.Process.myTid()));
        Log.d(TAG, "Firebase :: logImageStatus :: bundle " + bundle);
        firebaseAnalytics.logEvent(FirebaseConstants.EVENT_IMAGE_STATUS, bundle);

    }

    private static Bundle getUserData(CaptureDbHandler imgDb) {
        Bundle userOb = new Bundle();

        String versionName = BuildConfig.VERSION_NAME;
        int versionCode = BuildConfig.VERSION_CODE;

        userOb.putString("appVersion", versionName);
        userOb.putString("versionCode", String.valueOf(versionCode));

        try {
            String user_data = imgDb.getUserData();
            JSONObject user_data_obj = new JSONObject(user_data);
            Log.d(TAG, "Firebase :: getUserData => Try");
            try {
                userOb.putString("conn", user_data_obj.getString("conn"));
                userOb.putString("imei", user_data_obj.getString("imei"));
                userOb.putString("parent_msisdn", user_data_obj.getString("parent_msisdn"));
                userOb.putString("sub_agent_msisdn", user_data_obj.getString("sub_agent_msisdn"));
                userOb.putString("deviceModel", user_data_obj.getString("deviceModel"));
                userOb.putString("role", user_data_obj.getString("role"));
            } catch (Exception e) {
                Log.d(TAG, "Firebase :: getUserData => Exception LIST " + e.getMessage());
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.d(TAG, "Firebase :: getUserData => JSONException " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "Firebase :: getUserData => Exception " + e.getMessage());
            e.printStackTrace();
        }

        return userOb;
    }

}
