package net.omobio.imageuploader.uploadhelper;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

public class FileUtils {

    private Context reactContext = null;
    private String appPrivateDirectory = null;
    private String appDirectory = null;
    public static final String TAG = "ImageUploader";

    public FileUtils(Context reactContext) {

        this.reactContext = reactContext;
        this.appPrivateDirectory = reactContext.getCacheDir().getAbsolutePath();
        this.appDirectory = reactContext.getFilesDir().getAbsolutePath();
//        if (!checkIfAlreadyhavePermission()){
//            requestForSpecificPermission();
//        }
    }

    public void deleteAllImages() {
        Log.d(TAG, "##### deleteAllImages #####");
        Log.d(TAG, "deleteAllImages :: Directory : " + appPrivateDirectory);
        File imgDirectory = new File(appPrivateDirectory);

        if (imgDirectory.isDirectory()) {
            String[] children = imgDirectory.list();
            for (int i = 0; i < children.length; i++) {
                new File(imgDirectory, children[i]).delete();
                Log.d(TAG, "deleteAllImages :: imgDirectory : " + children[i]);
            }
        }

    }

    public void listAllTheFiles() {
        Log.d(TAG, "##### listAllTheFiles #####");
        Log.d(TAG, "listAllTheFiles :: Directory : " + appPrivateDirectory);
        File imgDirectory = new File(appPrivateDirectory);

        if (imgDirectory.isDirectory()) {
            String[] children = imgDirectory.list();
            for (int i = 0; i < children.length; i++) {
                Log.d(TAG, "listAllTtheFiles :: imgDirectory : " + children[i]);
                this.copyFile(children[i]);
            }
        }

    }

    public void listAllTheFilesDB() {
        Log.d(TAG, "##### listAllTheFilesDB #####");
        Log.d(TAG, "listAllTheFiles :: Directory : " + this.reactContext.getPackageResourcePath());
        File imgDirectory = new File(appPrivateDirectory);

        if (imgDirectory.isDirectory()) {
            String[] children = imgDirectory.list();
            for (int i = 0; i < children.length; i++) {
                Log.d(TAG, "listAllTtheFiles :: imgDirectory : " + children[i]);
                this.copyFile(children[i]);
            }
        }

    }

    public static byte[] createChecksum(String filename) throws
            Exception {
        InputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        return complete.digest();
    }

    public static String getMD5Checksum(String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result +=
                    Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }


    public void copyFile(String filename) {
        Log.d(TAG, "##### copyFile #####");
        File imgDirectory = new File(appPrivateDirectory);
        File outDirectory = new File(Environment.getExternalStorageDirectory() + "/DSA_IMAGES/");

        Log.d(TAG, "copyFile :: imgDirectory : " + imgDirectory);
        Log.d(TAG, "copyFile :: outDirectory : " + outDirectory);
        Log.d(TAG, "copyFile :: appDirectory : " + appDirectory);
        Log.d(TAG, "copyFile :: getPackageResourcePath : " + this.reactContext.getPackageResourcePath());
        Log.d(TAG, "copyFile :: getDatabasePath : " + this.reactContext.getDatabasePath("captureStore"));


//        if (!outDirectory.exists()) {
//            outDirectory.mkdirs();
//        }

        File sourceLocation = new File(imgDirectory + "/" + filename);
        File targetLocation = new File(outDirectory + "/" + filename);
        File sourceDBLocation = new File("/data/user/0/net.omobio.dialog.dialogra/databases/captureStore");

        Log.d(TAG, "copyFile :: sourceLocation : " + sourceLocation);
        Log.d(TAG, "copyFile :: targetLocation : " + targetLocation);
        Log.d(TAG, "copyFile :: sourceDBLocation : " + sourceDBLocation);

        InputStream in = null;
        OutputStream out = null;
        try {

            if (!sourceLocation.isDirectory()) {
                Log.d(TAG, "copyFile :: copping file");
                in = new FileInputStream(sourceLocation);
                out = new FileOutputStream(targetLocation);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();

            } else {
                Log.d(TAG, "copyFile :: isDirectory : skip");
            }

        } catch (FileNotFoundException e) {
            Log.d(TAG, "copyFile :: FileNotFoundException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "copyFile :: IOException : " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "copyFile :: Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void exportDatabse() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+"net.omobio.dialog.dialogra"+"//databases//"+"captureStore"+"";
                String backupDBPath = "/DSA_IMAGES/captureStore.jpg";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public void copySqlitDb(String dbName) {
        String databasePath = this.reactContext.getDatabasePath(dbName).getPath();
        final String inFileName = databasePath;
        String outFileName = Environment.getExternalStorageDirectory() + "/DSA_IMAGES/database_copy.db";
        File dbFile = new File(inFileName);
        FileInputStream fis = null;
        OutputStream output = null;
        try {
            fis = new FileInputStream(dbFile);
            //Open the empty db as the output stream
            output = new FileOutputStream(outFileName);
            // Transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);

            }
            // Close the streams
            output.flush();
            output.close();
            fis.close();

        } catch (FileNotFoundException e) {
            Log.d(TAG, "copySqlitDb :: FileNotFoundException : " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "copySqlitDb :: IOException : " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(TAG, "copycopySqlitDbFile :: Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

}

