package net.omobio.locationservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import android.location.LocationManager;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class GeoLocationModule extends ReactContextBaseJavaModule {
    private Integer timeInterval;
    protected LocationManager locationManager;

    boolean isGPSEnabled = false;

    ReactApplicationContext reactApplicationContext;

    public GeoLocationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        timeInterval = 0;
        reactApplicationContext = reactContext;
        BroadcastReceiver geoLocationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("GeoLocationModule", "onReceive");

                Double lat = intent.getDoubleExtra("latitude", 0.0);
                Double lon = intent.getDoubleExtra("longitude", 0.0);
                Log.d("GeoLocationModule", "onReceive2 " + lat + " " + lon );

                GeoLocationModule.this.sendEvent(lat, lon);
            }
        };

        reactContext.registerReceiver(geoLocationReceiver, new IntentFilter("GeoLocation"));
    }

    @Override
    public String getName() {
        return "GeoLocationService";
    }

    @ReactMethod
    public void setTimeInterval(Integer period) {
        timeInterval = period;
        Log.d("GeoLocationModule", "setTimeInterval " + period + " " + timeInterval);
    }

    @ReactMethod
    public void startService(Promise promise) {
        String result = "Successfully started";
        Log.d("GeoLocationModule", "startService " + timeInterval);

        try {
            Log.d("GeoLocationModule", "startService :: try ");
            Intent intent = new Intent(LocationService.FOREGROUND);
            intent.putExtra("timeInterval", timeInterval);
            intent.setClass(this.getReactApplicationContext(), LocationService.class);
            getReactApplicationContext().startService(intent);
            promise.resolve(result);
        } catch (Exception e) {
            Log.d("GeoLocationModule", "startService :: Exception " + e.getMessage());
            promise.reject(e);
        }
    }

    @ReactMethod
    public void stopService(Promise promise) {
        String result = "Successfully stopped";
        Log.d("GeoLocationModule", "stopService");
        try {
            Log.d("GeoLocationModule", "stopService :: try ");
            Intent intent = new Intent(LocationService.FOREGROUND);
            intent.setClass(this.getReactApplicationContext(), LocationService.class);
            getReactApplicationContext().stopService(intent);
            promise.resolve(result);
        } catch (Exception e) {
            Log.d("GeoLocationModule", "stopService :: Exception " + e.getMessage());
            promise.reject(e);
        }
    }

    @ReactMethod
    public void checkPermission(Promise promise) {
        try {
            String permissionGranted = "false";
            if (ContextCompat.checkSelfPermission(reactApplicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionGranted = "false";
            } else {
                permissionGranted = "true";
            }
            promise.resolve(permissionGranted);
        } catch (Exception e) {
            promise.reject(e);
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void checkGPSStatus(Promise promise){
        String gpsEnabled = "false";

        locationManager = (LocationManager) reactApplicationContext.getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(isGPSEnabled){
            gpsEnabled =  "true";
            promise.resolve(gpsEnabled);
        } else {
            promise.reject(gpsEnabled);
        }
    }

    private void sendEvent(Double lat, Double lon) {
        Log.d("GeoLocationModule", "sendEvent " + lat + lon);

        WritableMap map = Arguments.createMap();
        WritableMap coordMap = Arguments.createMap();
        coordMap.putDouble("latitude", lat);
        coordMap.putDouble("longitude", lon);

        map.putMap("coords", coordMap);

        getReactApplicationContext().
                getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).
                emit("updateLocation", map);
    }
}
