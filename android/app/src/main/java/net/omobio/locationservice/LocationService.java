package net.omobio.locationservice;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class LocationService extends Service {
    private GPSTracker gpsTracker;
    public static final String FOREGROUND = "net.omobio.locationservice.FOREGROUND";
    public Context mContext;
    private Timer timer;
    private TimerTask timerTask;
    private static boolean isRunning = false;

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    public void onCreate() {
        super.onCreate();
    }

    private void sendMessage() {
        Log.d("LocationService", "sendMessage");
        double latitude;
        double longitude;
        try {
        
            gpsTracker = new GPSTracker();
            gpsTracker.getGeoLocation(mContext);
            Log.d("LocationService", "sendMessage 2 " + gpsTracker.canGetLocation());

            if (gpsTracker.canGetLocation()) {
                Log.d("LocationService", "sendMessage 3 ");

                Intent intent = new Intent("GeoLocation");
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                Log.d("LocationService", "sendMessage 4 " + latitude + " " + longitude);

                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);

                sendBroadcast(intent);
            } else {
            Log.d("LocationService", "false  " );

                Intent intent = new Intent("GeoLocation");
                latitude = 0.0;
                longitude = 0.0;
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                sendBroadcast(intent);
            }
        } catch (Exception e) {
            Log.d("LocationService", "catch  " + e.getMessage());

            Intent intent = new Intent("GeoLocation");
            latitude = 0.0;
            longitude = 0.0;
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            sendBroadcast(intent);
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("LocationService", "onDestroy" );
        if(timer != null ){
            timer.cancel();
            timer.purge();
            timerTask.cancel();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            int timeInterval = intent.getExtras().getInt("timeInterval");
            mContext = this.getBaseContext();
            runMethod(timeInterval);
        } catch (Exception e) {
            Log.d("LocationService", "onStartCommand Exception ");
        }
       
        return super.onStartCommand(intent, flags, startId);
    }

    public void runMethod(int timeInterval) {
        Log.d("LocationService", "runMethod timeInterval " + timeInterval);

        if(isRunning) {
            timer.cancel();
            timer.purge();
            timerTask.cancel();
        }

        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.d("LocationService", "runMethod run " );
                isRunning = true;
                sendMessage();
            }
        };
        timer.schedule(timerTask, 0000, timeInterval);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
