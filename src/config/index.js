/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Thursday, 18th October 2018 5:08:26 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 18th October 2018 7:15:56 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import Colors from './colors';
import Styles from './styles';
import Images from './images';
import Constants from './constants';
import globalConfig from './globalConfig';

export { 
  Colors, 
  Styles, 
  Images, 
  Constants, 
  globalConfig 
};
