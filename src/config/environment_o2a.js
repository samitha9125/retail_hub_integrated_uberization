/*
 * File: environment_o2a.js
 * Project: Dialog Sales App
 * File Created: Friday, 21st September 2018 9:34:59 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 21st September 2018 9:48:45 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import Constants from './constants';
const ENVIRONMENT = Constants.ENVIRONMENT;

/*==================================== Dev & Staging URLs ===================================*/

//Dev version URLs - DTV
const baseUrlDev = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/DEV/index.php?r=';
const baseUrlLoginDev = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/DEV/index.php?r=';

//Dev version URLs - DTV-2
// const baseUrlDev = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/DEV-2/index.php?r=';
// const baseUrlLoginDev = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/DEV-2/index.php?r=';

//Staging version URLs - DTV
const baseUrlStg = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/STG/index.php?r=';
const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/STG/index.php?r=';

// O2A CFSS merge staging final - LTE
// const baseUrlStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/BETA-LIVE/index.php?r=';
// const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/BETA-LIVE/index.php?r=';

/*
//Dev version URLs - LTE
const baseUrlDev = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/DEV/index.php?r=';
const baseUrlLoginDev = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/DEV/index.php?r=';
*/

/*
//Staging version URLs - LTE
const baseUrlStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/STG/index.php?r=';
const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/STG/index.php?r=';
*/
//Staging environment equivalent to live
const baseUrlTest = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/LIVE/index.php?r=';
const baseUrlLoginTest = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/LIVE/index.php?r=';

//Staging environment equivalent to live
const baseUrlSIT = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/SIT/index.php?r=';
const baseUrlLoginSIT = 'http://o2astg.dialog.lk/O2A-Retailer-DTV/SIT/index.php?r=';

//Staging Test version URLs - LTE
// const baseUrlTest = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/TEST/index.php?r=';
// const baseUrlLoginTest = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/TEST/index.php?r=';

// O2A CFSS merge staging final - LTE
// const baseUrlStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/BETA-LIVE/index.php?r=';
// const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/BETA-LIVE/index.php?r=';

/*
//Dev version URL
const baseUrlDev = 'http://o2astg.dialog.lk/DEV-O2A-Retailer/index.php?r=';
const baseUrlLoginDev = 'http://o2astg.dialog.lk/DEV-O2A-Retailer/index.php?r=';
*/

/*
//Dev version URL New
const baseUrlDev = 'https://o2astg.dialog.lk/retailer_dev/index.php?r=';
const baseUrlLoginDev = 'http://o2astg.dialog.lk/retailer_dev/index.php?r=';
*/

/*
//Staging version URL
const baseUrlStg = 'https://o2astg.dialog.lk/O2A-Retailer/index.php?r=';
const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer/index.php?r=';
*/

/*
//Revamp version URL
const baseUrlDev2 = 'https://o2astg.dialog.lk/O2A-Revamp/index.php?r=';
const baseUrlLoginDev2 = 'http://o2astg.dialog.lk/O2A-Revamp/index.php?r=';
*/

/*==================================== LIVE URLs===================================*/
//Live alpha version URL
const baseUrlLiveAlpha = 'https://o2a.dialog.lk/O2A-Retailer-Alpha/index.php?r=';
const baseUrlLoginLiveAlpha = 'http://o2a.dialog.lk/O2A-Retailer-Alpha/index.php?r=';

//Live beta version URL
// const baseUrlLive = 'https://o2a.dialog.lk/O2A-Retailer-Beta/index.php?r=';
// const baseUrlLoginLive = 'http://o2a.dialog.lk/O2A-Retailer-Beta/index.php?r=';

//Live Live STG version URL
// const baseUrlLive = 'https://o2astg.dialog.lk/O2A-Retailer-LTE/LIVE/index.php?r=';
// const baseUrlLoginLive = 'http://o2astg.dialog.lk/O2A-Retailer-LTE/LIVE/index.php?r=';

//Live release version URL
// const baseUrlLive = 'https://o2a.dialog.lk/O2A-Retailer/index.php?r=';
// const baseUrlLoginLive = 'http://o2a.dialog.lk/O2A-Retailer/index.php?r=';

//Live Live release version URL
const baseUrlLive = 'https://o2a.dialog.lk/O2A-Retailer-Live/index.php?r=';
const baseUrlLoginLive = 'http://o2a.dialog.lk/O2A-Retailer-Live/index.php?r=';



const getO2aUrls = (environment) => {
  switch (environment) {
    case ENVIRONMENT.DEV:
      return { login: baseUrlLoginDev, base: baseUrlDev };
    case ENVIRONMENT.STAGING:
      return { login: baseUrlLoginStg, base: baseUrlStg };
    case ENVIRONMENT.TEST:
      return { login: baseUrlLoginTest, base: baseUrlTest };
    case ENVIRONMENT.SIT:
      return { login: baseUrlLoginSIT, base: baseUrlSIT };
    case ENVIRONMENT.ALPHA:
      return { login: baseUrlLoginLiveAlpha, base: baseUrlLiveAlpha };
    case ENVIRONMENT.LIVE:
      return { login: baseUrlLoginLive, base: baseUrlLive };
    default:
      return { login: baseUrlLoginLive, base: baseUrlLive };
  }
};

export default {
  getO2aUrls
};