/*
 * File: colors.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 11:47:25 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

const Color = {
  app: '------',
  statusBarColor: '#340040',
  appBackgroundColor: '#f9f9f9',
  navBarBackgroundColor: '#61206C',
  navBarTextColor: '#FFF',
  navBarButtonColor: '#FFF',
  startUpViewColor: '#584078',
  defaultIconColorBlack: '#000000',
  activityIndicaterColor: '#bc2b78',
  activityIndicatorColor: '#bc2b78',
  background: '#f9f9f9',
  primaryText: '#000',
  modalOverlayColor: 'rgba(0, 0, 0, 0.7)',
  modalOverlayColorLow: 'rgba(0, 0, 0, 0.6)',
  cameraOverlayColor: 'rgba(0, 0, 0, 0.4)',
  underlayColor:  'rgba(0,1,1,0.7)',
  backgroundColorWhite: '#ffff',
  backgroundColorWhiteWithAlpa: '#ffffff',
  yellow: '#ffc400',
  showPwtintColor: 'rgba(0,0,0,0.2)',
  placeholderTextColor: '#C7C7CD',
  underlineColorAndroid: 'transparent',
  texInputUnderlineColor: '#909090',
  loginBackgroundColor: 'rgba(255, 255, 255, 0.4)',
  borderColorGray: 'gray',
  borderLineColor: '#d6d7da',
  borderLineColorLightGray: '#dddddd',
  btnActive: '#ffc400',
  btnDeactive: '#dddddd',
  btnDisable: '#dddddd',
  btnActiveTxtColor: 'black',
  btnDisableTxtColor: '#808080',
  btnDeactiveTxtColor: '#808080',
  radioBtn: {
    innerCircleColor: '#ffc400',
    outerCircleColor: 'grey'
  },
  radioFormLabelColor: '#1B1B1B',
  topSelctionBorderColor: '#d6d7da',
  statusIconColor: {
    success: '#ffc400',
    pending: '#B2BEB5'
  },
  radioBtnSelected: '#ffc400',
  radioBtnDefault: '#939393',
  signatureBorderColor: '#000033',
  signatureButtonColor: '#eeeeee',
  urlLinkColor: '#2E3192',
  colorWhite: '#fff',
  colorBlack: '#000',
  colorLightBlack: '#252525',//'#4B4B4B'//'#545454',//#252525//'#3B3C3B'
  colorBlue: 'blue',
  colorGreen: 'green',
  colorPureYellow: 'yellow',
  colorYellow: '#ffc400',
  colorRed: 'red',
  colorGrey:'#808080',
  colorDarkOrange: 'darkorange',
  colorBlackDelivery: '#111111',
  colorBackground: 'transparent',
  colorTransparent: 'transparent',
  borderColor: '#DDD',
  barcodeBackgroundColor: '#acacac',
  cameraPreviewBackgroundColor: '#F5FCFF',
  thumbnailTxtColor: '#808080',
  offerBackgroundColor: '#dddddd',
  grey:'#808080',
  lightGrey: '#d6d7da',
  lightTextGrey: '#989797',
  black: '#000',
  white: '#ffffff',
  transparent: 'transparent',
  colorWhiteAlpa: '#ffffff',
  colorWhiteAlpha: '#ffffff',
  green: 'green',
  barcodeBottomColor: '#acacac',
  simChangeButtonSubmitColor: '#D7CEB9',
  gettingStartedButtonTextColor: '#ff6d00',
  listSeparatorColor: '#CED0CE',
  colorAgentActive: '#00a651',
  colorAgentPending: '#cd9b4c',
  colorAgentInActive: '#ed1c24',
  generalModalTextColor:'#808080',
  helperTextFontColor: '#61206C',
  colorUnitAdjustment: '#c7bdba',
  button: {
    activeColor: '#ffc400',
    disableColor: '#424F4F4F',
    activeTextColor: '#000',
    disableTextColor: '#808080',
  },
  barcodeBackground: '#D8D8D8',
  barcodeOverlay: 'rgba(0,0,0,0.8)',
  hyperLinkColor: '#0000EE',
  buttonColor : '#FFC400',
  barcodeBottomBackColor: 'rgba(216,216,216,1)',
  rootcauseEditbutton: '#FF6D00',
  cardborder: '#eaeaea',
 
};


export default Color;
