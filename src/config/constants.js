const ENVIRONMENT = {
  LIVE: 'LIVE',
  ALPHA: 'ALPHA',
  DEV: 'DEV',
  STAGING:'STAGING',
  SIT: 'SIT',
  TEST: 'TEST'
};

const PREPAID = 'PREPAID';
const POSTPAID = 'POSTPAID';
const CX_TYPE_LOCAL = 'LOCAL';
const CX_TYPE_FOREIGNER = 'FOREIGNER';
const LOB_GSM = 'GSM';
const LOB_LTE = 'LTE';
const LOB_DTV = 'DTV';
const LOB_ALL = 'ALL';
const ID_PP = 'PP';
const ID_NIC = 'NIC';
const CX_ID_TYPE_NIC = 'NIC';
const CX_ID_TYPE_PASSPORT = 'PASSPORT';
const CX_INFO_TYPE_NIC = 'NIC';
const CX_INFO_TYPE_PASSPORT = 'PASSPORT';
const CX_INFO_TYPE_DRIVING_LICENCE = 'DRIVING_LICENCE';
const ID_TYPE_NIC = 1;
const ID_TYPE_PP = 2;
const ID_TYPE_DL = 3;

const imageCaptureTypes = {
  "NIC_FRONT": 1,
  "NIC_BACK": 2,
  "PASSPORT": 3,
  "DRIVING_LICENCE": 4,
  "PROOF_OF_BILLING": 5,
  "CUSTOMER_IMAGE": 6,
  "SIGNATURE": 7,
  "ADDITIONAL_POB": 8
};

const SAME_DAY_INSTALLATION_TYPE = 9;

const IMAGE_CAPTURE_TYPES = {
  NIC_FRONT: 1,
  NIC_BACK: 2,
  PASSPORT: 3,
  DRIVING_LICENCE: 4,
  PROOF_OF_BILLING: 5,
  CUSTOMER_IMAGE: 6,
  SIGNATURE: 7,
  ADDITIONAL_POB: 8
};

const workOrdrState = {
  PENDING: 1,
  DISPATCHED: 2,
  INPROGRESS: 3
};

const workOrderStatus = {
  IN_PROGRESS:'IN_PROGRESS',
  PENDING: 'PENDING',
  DISPATCHED: 'DISPATCHED'
};
const idType = {
  NIC: 'NIC No',
  PP: 'Passport',
  DL: 'Driving Licence'
};

const otpStatus = {
  NEW_CUSTOMER: 0,
  OTP_VERIFYED: 1,
  OTP_NOT_VERIFYED: 2,
  OTP_VERIFYED_MCONNECT: 3,
  OTP_NOT_VERIFYED_OTHER: 4,
};

const TILE_ID = {
  BILL_PAYMENT: 1,
  SIM_CHANGE: 2,
  GSM_ACTIVATION: 3,
  DTV_ACTIVATION: 4,
  TRACTIONS_HISTORY: 6,
  DELIVERY_APP: 7,
  DOC_990: 8,
  LTE_ACTIVATION: 9,
  EZ_CASH: 16, 
};

const icon = {
  barcode: {
    name: 'Barcode Icon',
    iconType: 'MaterialCommunityIcons',
    id: 'barcode',
    defaultFontSize: 25
  },
  edit: {
    name: 'Edit Icon',
    iconType: 'MaterialIcons',
    id:  'mode-edit',
    defaultFontSize: 16
  },
  next: {
    name: 'Arrow dropright circle Icon',
    iconType: 'FontAwesome',
    id:  'chevron-circle-right',
    defaultFontSize: 22
  },
  camera: {
    name: 'Camera Icon',
    iconType: 'Ionicons',
    id: 'ios-camera',
    defaultFontSize: 37
  },
  thumbnail: {
    name: 'Thumbnail Icon',
    iconType: 'FontAwesome',
    id: 'picture-o',
    defaultFontSize: 26
  },
  info: {
    name: 'Information Icon',
    iconType: 'Entypo',
    id: 'info-with-circle',
    defaultFontSize: 25
  },
  arrow_dropdown: {
    name: 'Arrow Dropdown Icon',
    iconType: 'Ionicons',
    id: 'md-arrow-dropdown',
    defaultFontSize: 20
  }
};

const MAIN_SECTION = {
  NIC_VALIDATION: 1,
  PRODUCT_INFORMATION: 2,
  CUSTOMER_INFORMATION: 3,  
};


const ERROR_MESSAGES = {
  en : {
    SYSTEM_ERROR: 'System error. Please try again',
    NETWORK_ERROR: 'Network error'
  },
  si : {
    SYSTEM_ERROR: 'පද්ධති දෝෂයකි. කරුණාකර නැවත උත්සාහ කරන්න',
    NETWORK_ERROR: 'ජාල දෝෂයකි'
  },
  ta : {
    SYSTEM_ERROR: 'கணினி பிழை. தயவு செய்து மீண்டும் முயற்சிக்கவும்',
    NETWORK_ERROR: 'நெட்வொர்க் பிழை'
  }
};

const MSG_TYPE_ALERT = "MSG_TYPE_ALERT";
const MSG_TYPE_MODAL = "MSG_TYPE_MODAL";
const MSG_TYPE_INLINE = "MSG_TYPE_INLINE";
const MSG_TYPE_NA = "NA";

const CX_EXIST_YES = 'Y';
const CX_EXIST_NO = 'N';
const DOCUMENT_EXIST_YES = 'Y';
const DOCUMENT_EXIST_NO = 'N';

const CX_OTP_NOT_APPLICABLE = 0;
const CX_OTP_VERIFIED = 1;
const CX_OTP_NOT_VERIFIED = 2;

const INDIVIDUAL_SERIAL = 0;
const BUNDLE_SERIAL = 1;

const AUTO_SEARCH_MIN_TEXT_LENGTH = 3;
const EZ_CASH_PIN_MAX_LENGTH = 4;
const MOBILE_NUMBER_MAX_LENGTH = 10;
const DEFAULT_SERIAL_MAX_LENGTH = 18;
const BUNDLE_SERIAL_MAX_LENGTH = 18;
const PACK_SERIAL_MAX_LENGTH = 18;
const SIM_SERIAL_MAX_LENGTH = 8;
const ANALOG_PHONE_SERIAL_MAX_LENGTH = 18;

const PRODUCT_FAMILY_LTE_ANALOG_PHONE = "LTE_ANALOG_PHONE";
const PRODUCT_FAMILY_LTE_CPE = "LTE_CPE";
const PRODUCT_FAMILY_LTE_SIM = "LTE_SIM";

const DTV_INDIVIDUAL_CHANNEL = 'INDIVIDUAL';
const DTV_CHANNEL_PACK = 'PACK';
const CASH_AND_CARRY = 'CARRY';
const CASH_AND_DELIVERY = 'DELIVERY';

// Number Pool Release Types
const RELEASE_TYPES = {
  RELEASE: 'release', // Release all reserved numbers except currently selected number
  REFRESH: 'refresh', // Release all reserved numbers except currently selected number and reserve new 10 numbers.
  SEARCH: 'search',   // Query a connection number
  RELEASE_ALL: 'release_all' // Release all reserved numbers including currently selected number
};

const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

const LOCATION_ERROR_CODES = {
  NO_PERMISSION: 'no_permission',
  REJECTED: 'rejected',
  DATA_ERROR: 'data_error',
  TIMEOUT: 'timeout',
  DEFAULT : 'default'
};

const Constants = {
  ENVIRONMENT,
  PREPAID,
  POSTPAID,
  CX_TYPE_LOCAL,
  CX_TYPE_FOREIGNER,
  CX_ID_TYPE_NIC,
  CX_ID_TYPE_PASSPORT,
  CX_INFO_TYPE_NIC,
  CX_INFO_TYPE_PASSPORT,
  CX_INFO_TYPE_DRIVING_LICENCE,
  ID_PP,
  ID_NIC,
  LOB_GSM,
  LOB_LTE,
  LOB_DTV,
  LOB_ALL,
  CX_EXIST_YES,
  CX_EXIST_NO,
  DOCUMENT_EXIST_YES,
  DOCUMENT_EXIST_NO,
  INDIVIDUAL_SERIAL,
  CX_OTP_NOT_APPLICABLE,
  CX_OTP_VERIFIED,
  CX_OTP_NOT_VERIFIED,
  BUNDLE_SERIAL,
  ID_TYPE_NIC,
  ID_TYPE_PP,
  ID_TYPE_DL,
  imageCaptureTypes,
  IMAGE_CAPTURE_TYPES,
  SAME_DAY_INSTALLATION_TYPE,
  workOrdrState,
  workOrderStatus,
  idType,
  otpStatus,
  ERROR_MESSAGES,
  DEBOUNCE_OPTIONS,
  TILE_ID,
  icon,
  MAIN_SECTION,
  MSG_TYPE_NA,
  MSG_TYPE_ALERT,
  MSG_TYPE_MODAL,
  MSG_TYPE_INLINE,
  RELEASE_TYPES,
  AUTO_SEARCH_MIN_TEXT_LENGTH,
  EZ_CASH_PIN_MAX_LENGTH,
  MOBILE_NUMBER_MAX_LENGTH,
  PRODUCT_FAMILY_LTE_ANALOG_PHONE,
  PRODUCT_FAMILY_LTE_CPE,
  PRODUCT_FAMILY_LTE_SIM,
  DEFAULT_SERIAL_MAX_LENGTH,
  BUNDLE_SERIAL_MAX_LENGTH,
  PACK_SERIAL_MAX_LENGTH,
  SIM_SERIAL_MAX_LENGTH,
  ANALOG_PHONE_SERIAL_MAX_LENGTH,
  LOCATION_ERROR_CODES,
  DTV_INDIVIDUAL_CHANNEL,
  DTV_CHANNEL_PACK,
  CASH_AND_CARRY,
  CASH_AND_DELIVERY,
};

export default Constants;
