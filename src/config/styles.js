/*
 * File: styles.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 11:49:26 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import { Dimensions } from 'react-native';
const deviceHeight =  Dimensions.get('window').height;
const deviceWidth =  Dimensions.get('window').width;

const Styles = {
  app: '',
  statusIconSize: 24,
  bannerHeight: deviceHeight*3/8,
  menuHeight: (deviceHeight)/5 ,
  inputFieldIconSize: 25,
  fontWeight: 500,
  defaultBtnFontSize: 17,
  defaultFontSize: 17,
  btnFontSize: 20,
  otpModalFontSize: 16,
  otpEnterModalFontSize: 15,
  appVersionFontSize: 14,
  barcodeDtectedTxtFontSize: 22,
  thumbnailTxtFontSize: 14,
  captureIconFontSize: 37,
  mobileAct: {
    defaultBtnFontSize: 18,
    defaultFontSize: 18
  },
  actStatus: {
    defaultBtnFontSize: 18,
    defaultFontSize: 18,
    defaultTextFontSize:15.5
  },
  delivery: {
    defaultBtnFontSize: 16,
    defaultFontSize: 18,
    notificationFontSize: 20,
    checkboxFontSize: 16,
    titleFontSize: 23,
    nicLableFontSize: 21,
    modalTextFontSize: 20,
    modalFontSize: 22,
    stockAcceptancemodalFontSize: 18,
  },
  
  button: {
    defaultFontSize: 16,
    defaultFontWeight: '500',
    defaultButtonHeight: 36
  },
  matirialInput: {
    marginBottom: 0,
    marginTop: -18,
    marginLeft: 15,
    marginRight: 12,
    imageStyle: {
      flex: 1,
      justifyContent: 'flex-end',
      paddingBottom: 10,
      alignItems: 'flex-end'
    }
  }, 
  modal: {
    errorDescrptionFontSize: 23

  },
  otpModalHeight: 210,
  otpModelWidth: 350,

  dropDownModal: {
    titleFontSize: 23,
    defaultBtnFontSize: 20,
    defaultFontSize: 18,
    dropdownDataElemint: 14
  },

  generalModalButtonTextSize: 20,
  generalModalTextSize: 17,
  generalAlertTitleTextSize: 18,
  generalModalTitleTextSize: 20,

};

export default Styles;
