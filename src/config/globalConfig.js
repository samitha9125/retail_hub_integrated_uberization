/*
 * File: globalConfig.js
 * Project: Dialog Retail Hub
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 4th December 2018 4:29:04 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import Constants from './constants';
import environment_o2a from './environment_o2a';
import environment_cfss from './environment_cfss';

const appName = 'Dialog Retail Hub';
let appVersion = '1.5.2';
const appVersionName = '1.5.2'; /** Reference only, Please update gradle build file also ******/
const versionNumber = appVersion;   /********************* When building a APK ********************/
const versionCode = 156;           /********* Please update versionCode and releaseNumber ********/
const releaseNumber = '';         /**** These values will be reflected on the login page ********/

const ENVIRONMENT = Constants.ENVIRONMENT;

let AppEnvironment = ENVIRONMENT.STAGING; /** Select App environment (LIVE / ALPHA / DEV / STAGING) **/

/* ################################################################################ */
/* ############ Select App type (O2A or CFSS) and AppEnvironment ################## */
const APP_TYPE = { O2A: 'O2A',  CFSS: 'CFSS' };
let AppType = APP_TYPE.O2A;  //Select O2A or CFSS (O2A / CFSS)

/* ################################################################################ */

const lang = 'en';
const agentMssdn = '';
let subAgentMsisdn = '';

let baseUrlValue;
let baseUrlLoginValue ;
let imsiValue = '';
let modeValue = 'live';

if (AppType === APP_TYPE.O2A) {
  console.log('## O2A_ENVIRONMENT :: '+ AppEnvironment);
  baseUrlValue = environment_o2a.getO2aUrls(AppEnvironment).base;
  baseUrlLoginValue = environment_o2a.getO2aUrls(AppEnvironment).login;
} else {
  console.log('## CFSS_ENVIRONMENT :: '+ AppEnvironment);
  baseUrlValue = environment_cfss.getCfssUrls(AppEnvironment).base;
  baseUrlLoginValue = environment_cfss.getCfssUrls(AppEnvironment).login;
}
if (AppEnvironment === ENVIRONMENT.DEV) {
  imsiValue = 413027000434392; 
  modeValue = 'dev';
  appVersion = appVersion +'_DEV'+ releaseNumber;

} else if (AppEnvironment === ENVIRONMENT.STAGING) {
  imsiValue = 413027000434392;
  modeValue = 'staging';
  appVersion = appVersion +'_STG'+ releaseNumber;

} else if (AppEnvironment === ENVIRONMENT.TEST) {
  imsiValue = 413027000434392;
  modeValue = 'test';
  appVersion = appVersion +'_TEST'+ releaseNumber;

} else if (AppEnvironment === ENVIRONMENT.SIT) {
  imsiValue = 413027000434392;
  modeValue = 'sit';
  appVersion = appVersion +'_SIT'+ releaseNumber;

} else if (AppEnvironment === ENVIRONMENT.ALPHA) {
  modeValue = 'alpha';
  appVersion = appVersion +'_alpha';
}

const baseUrlLogin = baseUrlLoginValue;
const baseUrl = baseUrlValue;
const imsi = imsiValue;
const mode = modeValue;

const heartBeatInterval = 900000;
const searchTimeInterval = 800;
const searchDebounceDelay = 1000;
const listItemTapInterval = 300;
const defaultDelayTime = 500;
const transactionHistoryLoadLimit = 15;
const buttonTapDelayTime = 200;
const EZ_CASH_PIN_MAX_LENGTH = 4;
const MOBILE_NUMBER_MAX_LENGTH = 10;
const locationPopUpInterval = 5000;


const Global = {
  appVersion,
  versionNumber,
  appVersionName,
  versionCode,
  appName,
  ENVIRONMENT,
  mode,
  lang,
  agentMssdn,
  subAgentMsisdn,
  baseUrl,
  baseUrlLogin,
  imsi,
  heartBeatInterval,
  searchTimeInterval,
  searchDebounceDelay,
  listItemTapInterval,
  defaultDelayTime,
  transactionHistoryLoadLimit,
  buttonTapDelayTime,
  EZ_CASH_PIN_MAX_LENGTH,
  MOBILE_NUMBER_MAX_LENGTH,
  locationPopUpInterval
};

export default Global;
