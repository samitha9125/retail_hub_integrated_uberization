import { Platform } from 'react-native';

export const colors = {
  primaryColor1: '#65246E', //purple
  primaryColor2: '#EB275A', //pink
  secondaryColor1: '#F88E13', //orange
  secondaryColor2: '#178DE7', //blue
  secondaryColor3: '#C42378', //pink+purple
  secondaryColor4: '#23E94E', //green
  secondaryColor: '#b30000', //red
  statusBarBgColor: '#8c6395', //dark purple
  BtnColor1: '#EB275A',
  appBgColor: '#ffffff', //white
  textColor1: '#727272', //gray
  textColor2: '#ffffff', //white
  textColor3: '#000000', //black
  burnColor: '#D0021B', //red
  modalBodyColor: '#ffffff',
  modalHeaderBlack: '#2C2C2C',
  modalTextInputColor: '#000000',
  progressbarBackground: 'rgba(0,0,0,0.08)',
  modalTitle: '#727272',
  backdropColor: 'rgba(255,255,255,0.6)',
  backdropColor2: 'rgba(0,0,0,0.7)',
  shadowColor1: 'rgba(0,0,0,0.1)',
  shadowColor2: 'rgba(243,99,65,0.25)',
  bonusDataProgress: '#BB1B39',
  bonusDataProgBgColor: 'rgba(247, 234, 237, 1)',
  voiceProgressBgColor: 'rgba(248, 142, 19, 0.36)',
  nightDataProgressBgColor: 'rgba(194, 214, 220, 0.36)',
  extendedDataProgressBg: 'rgba(254, 242, 248, 0.85)',
  dayDataProgressBg: 'rgba(250, 221, 188, 0.36)',
  supportCardImageCircleColor: 'purple',
  accountDropDownBgColor: '#ffffff',
  accountDropDownTextColor: '#000000',
  textFieldAccentColor: '#65246E',
  termsConditionsColor: '#5F5F5F',
  btnText: '#ffffff',
  chooseMethodColor: '#000000',
  fieldInputTextColor: '#000000',
  placeholderTextColor: '#6B000000',
  switchColorOne: 'rgba(196,35,120,0.5)',
  switchColorTwo: 'rgba(196,35,120,1)',
  tabUnderlineColor2: '#ffffff',
  searchBarBg: '#FFFAFAF',
  tabUnderlayColor: '#f9ad59',
  greenText: 'green',
  redText: 'red',
  dataSlider: 'orange',
  callSlider: '#F6089A',
  smsSlider: 'blue',
  profileName: 'black',
  myProfileRadioButtonColor: '#FDC228',
  expandViewBorderColor: 'rgba(0,0,0,0.15)',
  tierPoints: '#FDB934',
  greyColor: '#e4e4e4',
  goldColor: '#FDB934',
  platinumColor: '#222f6f',
  silver: '#cbcbcb',
  loyaltyBlue: '#6c70ab',
  //extendDataColor: '#BB1B39',
  extendDataColor: '#C42378',
  lteBorderColor: 'rgba(0,0,0,0.15)',
  ltePayboxBorder: 'rgba(0,0,0,0.6)',
  channelBg: 'rgba(0,0,0,0.32)',
  seperateLine: '#727272',
  barUnselectColor: '#dad8dd', //gray
  supportBtnColor: '#FDB934',
  itemSeperatorColor: '#BCBCBC',
  failed: '#EB3167',
  succes: '#74CD11',
  incomplete: '#F7921C',
  availableText: '#17c76d',
  priority: '#971b8a',
  platinum: '#054663',
  bronze: '#735233',
  notiUnreadColor: '#F3F3F3', //#FFDCE0E5
  buttonDisableColor: '#727272',
  colorTransperant: 'rgba(0,0,0,0)',
  colorWhite: '#FFFFFF',
  errorColor: 'rgb(213, 0, 0)',
  noInernetColor:'#A6A7A9',
  appRatingTxt: "#727272"
};

export const appRatingColors = {
  default: '#c8c8c8',
  detractors: '#fb0200',
  passive: '#ffb44e',
  promoters: '#1e9307'
};

export const customSizes = {
  checkBoxSize1: 20
};

export const fontSizes = {
  headerXLarge: 65,
  headerSemiXLarge: 30,
  headerLarge: 36,
  headerMedium: 25,
  headerSemiMedium: 24,
  header: 20,
  title: 20,
  subHeader: 18,
  buttons: 16,
  buttons2: 14,
  body1: 16,
  body2: 14,
  caption: 13,
  modalHeader: 22,
  modalBody: 18,
  nextButtonFont: 18,
  defaultSize: 12,
  whiteCardTitles: 15,
  profileName: 25
};

export const fontWeights = {
  normal: 'normal',
  hard: '600',
  medium: '500',
  semiMedium: '400'
};





export const dashboardBannerHeight = 250;

export const materialTextFieldStyle = {
  tintColor: colors.textFieldAccentColor,
  activeLineWidth: 2,
  textColor: colors.fieldInputTextColor,
  titleFontSize: fontSizes.body1,
  labelFontSize: fontSizes.body1
};
