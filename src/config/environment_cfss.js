/*
 * File: environment_cfss.js
 * Project: Dialog Sales App
 * File Created: Thursday, 20th September 2018 5:50:02 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 21st September 2018 9:51:00 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import Constants from './constants';
const ENVIRONMENT = Constants.ENVIRONMENT;

// // DEV BE
const baseUrlDev = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/DEV-2/index.php?r=';
const baseUrlLoginDev = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/DEV-2/index.php?r=';

//old_dev
// const baseUrlDev = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS-DEV-0.1/index.php?r=';
// const baseUrlLoginDev = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS-DEV-0.1/index.php?r=';

// SIT BE
const baseUrlSIT = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/SIT/index.php?r=';
const baseUrlLoginSIT = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/SIT/index.php?r=';

// STG BE
const baseUrlStg = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/STG/index.php?r=';
const baseUrlLoginStg = 'http://o2astg.dialog.lk/O2A-Retailer-CFSS/STG/index.php?r=';

//production
// const baseUrlStg = 'https://o2a.dialog.lk/O2A-Production/index.php?r=';
// const baseUrlLoginStg = 'https://o2a.dialog.lk/O2A-Production/index.php?r=';

//live O2A-Retailer-Live
const baseUrlLive = 'https://o2a.dialog.lk/O2A-Retailer-Live/index.php?r=';
const baseUrlLoginLive = 'https://o2a.dialog.lk/O2A-Retailer-Live/index.php?r=';

const getCfssUrls = (environment) => {
  switch (environment) {
    case ENVIRONMENT.DEV:
      return { login: baseUrlLoginDev, base: baseUrlDev };
    case ENVIRONMENT.STAGING:
      return { login: baseUrlLoginStg, base: baseUrlStg };
    case ENVIRONMENT.LIVE:
      return { login: baseUrlLoginLive, base: baseUrlLive };
    case ENVIRONMENT.SIT:
      return { login: baseUrlLoginSIT, base: baseUrlSIT };
    default:
      return { login: baseUrlLoginDev, base: baseUrlDev };
  }
};

export default {
  getCfssUrls
};