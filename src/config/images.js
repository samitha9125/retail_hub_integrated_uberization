/*
 * @export
 * @class  images
 * @extends {React.Component}
 * Copyright (C) Dialog axiata PVT LTD
 * Authors : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : 2018/05/28
 * This block of code is used to import image assets .
 *
 */
import alert from '@Images/icons/alert.png';
import success_alert from '@Images/common/icons/icon_state_success.png';
import failure_alert from '@Images/common/icons/icon_state_fail.png';
import notice_alert from '@Images/common/icons/icon_state_notice.png';
import attention_alert from '@Images/common/icons/icon_attention.png';
import dialogLogo from '@Images/common/icons/dialog_logo.png';
import error from '@Images/common/error_msg.png';
import success from '@Images/common/success_msg.png';
import success_200px from '@Images/common/icons/icon200px/icon_success.png';
import pending_200px from '@Images/common/icons/icon200px/icon_pending.png';
import failure_200px from '@Images/common/icons/icon200px/icon_failure.png';
import attention_200px from '@Images/common/icons/icon200px/icon_attention.png';
import mobile_icon from '@Images/common/icons/icon_mobile_black.png';
import lte_icon from '@Images/common/icons/icon_lte_black.png';
import dtv_icon from '@Images/common/icons/icon_dtv_black.png';


const icons = {
  Alert: alert,
  DialogLogo: dialogLogo,
  SuccessAlert : success_alert,
  FailureAlert : failure_alert,
  NoticeAlert : notice_alert,
  AttentionAlert : attention_alert,
  Error: error,
  Success: success,
  Attention_200px: attention_200px,
  Success_200px: success_200px,
  Pending_200px: pending_200px,
  Failure_200px: failure_200px,
  Mobile_icon: mobile_icon,
  Lte_icon: lte_icon,
  Dtv_icon: dtv_icon,
};

export default {
  icons
};
