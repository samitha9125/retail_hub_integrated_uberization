import React from 'react';
import Orientation from 'react-native-orientation';
import { Colors } from '@Config';
import { Analytics } from '@Utils';
import DtvAdditionalChannelsScreen from '../../views/dtvActivation/DtvAdditionalChannels';
class DtvAdditionalChannels extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DtvAdditionalChannelsScreen :: componentDidMount');
    Orientation.lockToPortrait();
    Analytics.logEvent('dsa_dtv_act_chanel_selection');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class: 'dtv_act_chanel_selection',
      screen_name: 'dtv_act_chanel_selection',
      screen_details: 'DTV additional channel selection',
    });
  }

  render() {
    return (<DtvAdditionalChannelsScreen {...this.props} />);
  }
}

export default DtvAdditionalChannels;
