import React from 'react';
import Orientation from 'react-native-orientation';
import { Colors } from '@Config';
import { Analytics } from '@Utils';
import DtvActMainScreen from '../../views/dtvActivation';

class DtvActScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DtvActMainScreen :: componentDidMount');
    Orientation.lockToPortrait();
    Analytics.logEvent('dsa_dtv_activation');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class : 'dtv_activation', 
      screen_name : 'dtv_activation', 
      screen_details : 'DTV Activation', 
    });  
  }

  render() {
    return (<DtvActMainScreen {...this.props}/>);
  }
}

export default DtvActScreen;
