import React from 'react';
import Orientation from 'react-native-orientation';
import { Colors } from '@Config';
import { Analytics } from '@Utils';
import PackageSelectionMain from '../../../views/dtvActivation/modals/PackageSelectionModal';

class PackageSelectionModal extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_dtv_act_package_selection');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class: 'dtv_act_package_selection',
      screen_name: 'dtv_act_package_selection',
      screen_details: 'DTV package selection',
    });
  }

  render() {
    return (<PackageSelectionMain {...this.props} />);
  }
}

export default PackageSelectionModal;
