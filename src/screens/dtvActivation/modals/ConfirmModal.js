import React from 'react';
import Orientation from 'react-native-orientation';
import { Colors } from '@Config';
import { Analytics } from '@Utils';
import ConfirmModal from '../../../views/dtvActivation/modals/ConfirmModal';

class ConfirmModalLte extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_dtv_act_payment_confirm_modal');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class: 'dtv_act_payment_confirm_modal',
      screen_name: 'dtv_act_payment_confirm_modal',
      screen_details: 'DTV payment confirm',
    });
  }

  render() {
    return (<ConfirmModal {... this.props} />);
  }
}

export default ConfirmModalLte;
