import React from 'react';
import Orientation from 'react-native-orientation';
import { Colors } from '@Config';
import { Analytics } from '@Utils';
import EoafConfirmModalMain from '../../../views/dtvActivation/modals/EoafConfirmModal';

class EoafConfirmModal extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_dtv_act_eof_confirm_modal');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class : 'dtv_act_eof_confirm_modal', 
      screen_name : 'dtv_act_eof_confirm_modal', 
      screen_details : 'DTV eOF confirm modal', 
    }); 
  }

  render() {
    return (<EoafConfirmModalMain {... this.props}/>);
  }
}

export default EoafConfirmModal;