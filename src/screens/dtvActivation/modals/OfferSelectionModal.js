import React from 'react';
import Orientation from 'react-native-orientation';
import OfferSelectionModal from '../../../views/dtvActivation/modals/OfferSelectionModal';
import { Colors } from '@Config';
import { Analytics } from '@Utils';

class OfferSelectionModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_act_offer_selection_modal');
    Analytics.logFirebaseEvent('screen_view_event', {
      screen_class : 'dtv_act_offer_selection_modal', 
      screen_name : 'dtv_act_offer_selection_modal', 
      screen_details : 'DTV offer selection', 
    }); 
  }

  render() {
    return (<OfferSelectionModal {... this.props}/>);
  }
}

export default OfferSelectionModalScreen;