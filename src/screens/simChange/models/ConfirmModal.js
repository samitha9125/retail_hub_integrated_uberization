import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import ConfirmModal from '../../../views/simChange/models/Confirm';
import Colors from '../../../config/colors';

class ConfirmModalLte extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_confirm_modal');
  }

  render() {
    return (<ConfirmModal {... this.props}/>);
  }
}

export default ConfirmModalLte;