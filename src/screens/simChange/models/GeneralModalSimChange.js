import React, { Component } from 'react';

import GeneralModalSimChange from '../../../views/simChange/models/GeneralModalSimChange';
import Colors from '../../../config/colors';

class SimChangeModal extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        const { navigator } = this.props;
        return (<GeneralModalSimChange navigator={navigator} />);
    }
}

export default SimChangeModal;
