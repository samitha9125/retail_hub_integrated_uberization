import React, { Component } from 'react';
import { View, StyleSheet, Image, AsyncStorage } from 'react-native';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import launcherImg from '../../images/splash/splash.png';
import Colors from '../config/colors';

class StartupScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor,
      orientation: 'portrait'
    };
    constructor(props) {
      super(props);
      this.state = {
        isLoaded: false
      };
    }

    async componentWillMount() {
      this.setState({ isLoaded: true });
      try {
        const isFirstTime = await AsyncStorage.getItem('isFirstTime');
        const appKey = await AsyncStorage.getItem('app_key');
        const token = await AsyncStorage.getItem('token');
        console.log(isFirstTime);
        console.log(appKey);
        console.log(token);
        this.startApp(isFirstTime, token);
      } catch (error) {
        console.error(`AsyncStorage error: ${error.message}`);
      }
    }

    startImageUpload() {
      console.log('@@@@@@@@ startImageUpload :: startup');
      const errorCb = (msg) => {
        console.log('xxxx startImageUpload errorCb');
        console.log(msg);
      };
      const sucessCb = (msg) => {
        console.log('xxxx startImageUpload sucessCb');
        console.log(msg);
      };
      const flag = 0;
      RNReactNativeImageuploader.startUpload(flag, errorCb, sucessCb);
    }

    startApp(isFirstTime, token) {
      if (isFirstTime == null || token == null) {
        this
          .props
          .navigator
          .resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
      } else {
        this.startImageUpload();
        this
          .props
          .navigator
          .resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
      }
    }

    render() {
      return (
        <View style={styles.viewStyle}>
          <Image source={launcherImg} style={styles.image} />
        </View>
      );
    }
}

const styles = StyleSheet.create({

  viewStyle: {
    flex: 1,
    backgroundColor: '#584078'
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  }

});

export default StartupScreen;
