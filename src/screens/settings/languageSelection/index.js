import React, { Component } from 'react';

import LanguageSelectionMain from '../../../views/settings/languageSelection';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class LanguageSelectionScreen extends Component {
    static navigatorStyle = {
      navBarHidden: false,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }
    componentDidMount() {
      console.log('### LanguageSelectionScreen :: componentDidMount');
      Analytics.logEvent('dsa_language_change');
    }

    render() {
      const { navigator, initialSetup=undefined, languageSelectionCb=undefined } = this.props;

      return (<LanguageSelectionMain navigator={navigator} initialSetup={initialSetup} languageSelectionCb={languageSelectionCb} />);
    }
}

export default LanguageSelectionScreen;
