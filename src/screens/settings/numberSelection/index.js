import React, { Component } from 'react';
import { View } from 'react-native';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import NumberSelection from '../../../views/settings/numberSelection';
import Orientation from 'react-native-orientation';

class NumberSelectionScreen extends Component {
    static navigatorStyle = {
      navBarHidden: false,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }
  
    componentWillUnmount() {
      Orientation.lockToPortrait();
    }

    componentDidMount() {
      console.log('### NumberSelectionScreen :: componentDidMount');
      Analytics.logEvent('dsa_number_selection');
    }

    render() {
      // const { navigator } = this.props;

      return (<NumberSelection {... this.props}/>);
    }
}

export default NumberSelectionScreen;
