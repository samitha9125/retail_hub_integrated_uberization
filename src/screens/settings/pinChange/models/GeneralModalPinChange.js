import React, { Component } from 'react';

import GeneralModalPinChange from '../../../../views/settings/pinChange/models/GeneralModalPinChange';
import Colors from '../../../../config/colors';

class PinChangeModal extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        const { navigator } = this.props;
        return (<GeneralModalPinChange navigator={navigator} />);
    }
}

export default PinChangeModal;
