import React, { Component } from 'react';

import PinChangeMain from '../../../views/settings/pinChange';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class PinChangeScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### PinChangeScreen :: componentDidMount');
        Analytics.logEvent('dsa_pin_change');
    }

    render() {
        const { navigator } = this.props;

        return (<PinChangeMain navigator={navigator} />);
    }
}

export default PinChangeScreen;
