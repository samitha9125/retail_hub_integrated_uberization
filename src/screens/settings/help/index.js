import React from 'react';
import Orientation from 'react-native-orientation';
import Help from '../../../views/settings/help';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class HelpScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('HelpScreen:: componentDidMount');
    Orientation.lockToLandscape();
    Analytics.logEvent('dsa_help_screen');
  }

  render() {
    return (<Help {...this.props}/>);
  }
}

export default HelpScreen;
