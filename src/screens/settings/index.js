import React, { Component } from 'react';

import SettingsMain from '../../views/settings';

import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class SettingsScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### SettingsScreen :: componentDidMount');
        Analytics.logEvent('dsa_settings_page');
    }

    render() {
        const { navigator } = this.props;

        return (<SettingsMain navigator={navigator} />);
    }
}

export default SettingsScreen;
