import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import CommonAlertModel from '../../views/general/CommonAlertModel';

class CommonAlertModelScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentWillMount() {
        Orientation.lockToPortrait();
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        return (<CommonAlertModel {... this.props} />);
    }
}

export default CommonAlertModelScreen;
