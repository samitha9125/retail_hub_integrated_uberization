import React, { Component } from 'react';
import GeneralModalException from '../../views/general/GeneralModalException';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class GeneralExceptionModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralExceptionModal :: componentDidMount');
    Analytics.logEvent('dsa_genaral_exception_modal');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralModalException {...this.props}/>);
  }
}

export default GeneralExceptionModal;
