import React, { Component } from 'react';
import GeneralModelMobileActPostPaid from '../../views/general/GeneralModelMobileActPostPaid';
import Colors from '../../config/colors';

class GeneralModelMobileActPostPaidScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralModelMobileActPostPaid
      {... this.props}
      outstandingData={this.props.outstandingData}/>);
  }
}

export default GeneralModelMobileActPostPaidScreen;
