import React, { Component } from 'react';
import GeneralModelApproval from '../../views/general/GenaralModelApproval';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class GeneralModelApprovals extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralModalApproval :: componentDidMount');
    Analytics.logEvent('dsa_genaral__modal_approval');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralModelApproval {...this.props}/>);
  }
}

export default GeneralModelApprovals;
