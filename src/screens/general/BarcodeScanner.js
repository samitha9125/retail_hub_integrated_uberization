import React, { Component } from 'react';

import BarcodeScannerMain from '../../views/general/BarcodeScanner';

import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class BarcodeScannerScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentWillUnmount() {
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: true });
    }

    componentDidMount() {
      console.log('### BarcodeScannerScreen :: componentDidMount');
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: false });
      Analytics.logEvent('dsa_sim_scan');
    }

    render() {
      return (<BarcodeScannerMain {...this.props} />);
    }
}

export default BarcodeScannerScreen;
