import React from 'react';
import SignaturePad from '../../views/general/SignaturePad';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import Orientation from 'react-native-orientation';

class SignaturePadScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true,
    orientation: 'landscape'
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### SignaturePadScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });

    Orientation.lockToLandscapeLeft();
    Orientation.addOrientationListener(this._orientationDidChange);

    Analytics.logEvent('dsa_get_signature');
  }

  _orientationDidChange = (orientation) => {
    console.log('orientation: ',orientation);
    if (orientation === 'LANDSCAPE') {
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  }

  componentWillMount(){
    Orientation.lockToLandscapeLeft();
    // const orientationMode = Orientation.getInitialOrientation();

    // if (orientationMode === 'PORTRAIT') {
    //   console.log("App is on Portrait mode. Switching to LANDSCAPE");
    //   Orientation.lockToLandscape();    
    // } else {
    //   console.log("App is on LANDSCAPE mode.");
    // }
  }

  componentWillUnmount() {
    console.log("Component Unmounting. Switching to Portrait");
    Orientation.lockToPortrait();

  }

  render() {
    return (<SignaturePad {... this.props}/>);
  }
}

export default SignaturePadScreen;
