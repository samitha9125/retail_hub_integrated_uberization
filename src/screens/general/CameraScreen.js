import React, { Component } from 'react';

import CameraScreenMain from '../../views/general/CameraScreen';

import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class CameraScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### CameraScreen :: componentDidMount');
        Analytics.logEvent('dsa_camera_capture');
    }

    render() {
        const { navigator } = this.props;

        return (<CameraScreenMain navigator={navigator} />);
    }
}

export default CameraScreen;
