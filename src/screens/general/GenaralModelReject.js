import React, { Component } from 'react';
import GenaralModelRejectApprovals from '../../views/general/GenaralModelRejectstock';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class GenaralModelReject extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralModalReject :: componentDidMount');
    Analytics.logEvent('dsa_genaral__modal_approval');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GenaralModelRejectApprovals {...this.props}/>);
  }
}

export default GenaralModelReject;