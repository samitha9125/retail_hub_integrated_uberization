import React, { Component } from 'react';
import GeneralModalSucess from '../../views/general/GeneralModalSucess';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class GeneralSucessModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralSucessModal :: componentDidMount');
    Analytics.logEvent('dsa_genaral_sucess_modal');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralModalSucess {...this.props}/>);
  }
}

export default GeneralSucessModal;