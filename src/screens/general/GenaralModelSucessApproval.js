import React, { Component } from 'react';
import GenaralModelSucessApprovals from '../../views/general/GenaralModelSucessApproval';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class GenaralModelSucessApproval extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralModalApproval :: componentDidMount');
    Analytics.logEvent('dsa_genaral__modal_approval');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GenaralModelSucessApprovals {...this.props}/>);
  }
}

export default GenaralModelSucessApproval;