import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../../config/colors';
import ConfirmModelWarrantyReplacement from '../../../../views/directSale/warrentySales/model/ConfirmModel';

class ConfirmModelWarrantyReplacementScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentWillMount() {
        Orientation.lockToPortrait();
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        return (<ConfirmModelWarrantyReplacement {... this.props} />);
    }
}

export default ConfirmModelWarrantyReplacementScreen;
