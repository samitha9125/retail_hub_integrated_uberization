import React, { Component } from 'react';
import GeneralConfirm from '../../views/general/GeneralConfirmModel';
import Colors from '../../config/colors';

class GeneralModalConfirmScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralConfirm {...this.props}/>);
  }
}

export default GeneralModalConfirmScreen;