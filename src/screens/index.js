import { Navigation, ScreenVisibilityListener } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from '../reducers';

// import created reactotron config file
import Reactotron from '../config/ReactotronConfig'; //path to reactotronConfig file
import { composeWithDevTools } from 'redux-devtools-extension';

//Screens goes here
import Drawer from './drawer/Drawer';
import StartupScreen from './startup';
import LoginScreen from './authz';
import HomeScreen from './home';
import MobileActivationScreen from './mobileActivation';
import SettingsScreen from './settings/';
import LanguageSelectionScreen from './settings/languageSelection';
import PinChangeScreen from './settings/pinChange';
import HelpScreen from './settings/help';
import GeneralModalPinChange from './settings/pinChange/models/GeneralModalPinChange';
import SignaturePadScreen from './mobileActivation/SignaturePad';
import SimChangeScreen from './simChange';
import BillPaymentScreen from './billPayment';
import MobileActModel from './mobileActivation/models/MobileActModel';
import MobileActOtpModal from './mobileActivation/models/MobileActOtpModal';
import OfferSelectionModel from './mobileActivation/models/offerSelectionModel';
import NumberSelectionModel from './mobileActivation/models/numberSelectionModel';
import OutstandingModal from './mobileActivation/models/OutstandingModal';
import PackageInformation from './mobileActivation/models/PackageInformation';
import NicCaptureModel from './mobileActivation/models/NicCaptureModel';
import GeneralModel from './general/GeneralModel';
import GeneralModelApprovals from './general/GenaralModelApproval';
import GeneralModalSimChange from './simChange/models/GeneralModalSimChange';
import GeneralModalBill from './billPayment/models/GeneralModalBill';
import GeneralModalConfirmBill from './billPayment/models/GeneralModalConfirmBill';
import GeneralModelMobileAct from './general/GeneralModelMobileAct';
import GeneralModelMobileActPostPaid from './general/GeneralModelMobileActPostPaid';
import GeneralModalException from './general/GeneralModalException';
import GeneralSucessModal from './general/GeneralModelSucess';
import GeneralModelSuccessMobileAct from './general/GeneralModelSuccessMobileAct';
import OTPModelMobileAct from './general/OTPModelMobileAct';
import BarcodeScannerScreen from './general/BarcodeScanner';
import BarcodeScannerCommonScreen from './general/BarcodeScannerCommon';
import CameraScreen from './general/CameraScreen';
import CameraReCaptureScreen from './general/CameraReCaptureScreen';
import CameraUI from './general/CameraUI';
import TotalPayment from '../views/mobileActivation/TotalPayment';
import SignatuerConfirmModal from './mobileActivation/models/ConfirmModal';
import DeliveryMainScreen from './delivery';
import WorkOrderDetailsScreen from './delivery/WorkOrderDetails';
import ConfirmAlertModal from './delivery/modals/ConfirmAlert';
import ActivateAlertModal from './delivery/modals/ActivateAlert';
import RejectAlertModal from './delivery/modals/RejectAlert';
import GeneralAlertModal from './delivery/modals/GeneralAlert';
import CustomerConfirmationScreen from './delivery/CustomerConfirmation';
import CustomerVerificationScreen from './delivery/CustomerVerification';
import FeedbackScreen from './delivery/Feedback';
import RejectOrderScreen from './delivery/RejectOrder';
import SlaBreachScreen from './delivery/SlaBreach';
import PendingWorkOrdersIndex from './delivery/PendingWorkOrdersIndex';
import FilterWorkOrdersIndex from './delivery/FilterWorkOrders';
import NoWorkOrderScreen from './delivery/NoWorkOrder';
import SignaturePadCommonScreen from './general/SignaturePad';
import EzCashErrorPopup from './mobileActivation/models/EzCashErrorPopup';
import NumberSelectionScreen from './settings/numberSelection';
import TransactionHistoryMainScreen from './transactionHistory';
import SimChangeHistoryMainScreen from './transactionHistory/simChange';
import SimChangeDetailsScreen from './transactionHistory/simChange/SimChangeDetails';
import FilterSimChangeHistoryScreen from './transactionHistory/simChange/FilterSimChangeHistoryScreen';
import BillPaymentHistoryMainScreen from './transactionHistory/billPayment';
import BillPaymentDetailsScreen from './transactionHistory/billPayment/BillPaymentDetails';
import FilterBillPaymentHistoryScreen from './transactionHistory/billPayment/FilterBillPaymentHistoryScreen';
import ActivationStatusMainScreen from './transactionHistory/activationStatus';
import ActivationStatusDetailsScreen from './transactionHistory/activationStatus/ActivationStatusDetails';
import FilterActivationStatusScreen from './transactionHistory/activationStatus/FilterActivationStatusScreen';
import ImageResubmitScreen from './transactionHistory/activationStatus/ImageResubmitView';
import SimChangeConfirmModal from './simChange/models/ConfirmModal';

//Authentication related screens
import GeneralModalAuthScreen from './authz/modals/GeneralModalAuthScreen';
import PinResetModalScreen from './authz/modals/PinResetModalScreen';
import OTPModalScreen from './authz/modals/OTPModalScreen';
import GeneralModalAlert from './authz/modals/GeneralModalAlert';
import RetailerRegistrationScreen from './authz/RetailerRegistration';

//Sub agent management screens
import OTPModalScreenAgent from './subManagement/modals/modals/OTPModal';
import NumberModel from './subManagement/modals/modals/NumberModel';
import subManagementContainer from './subManagement/';
import SubAgentList from './subManagement/subAgentList';
import SubAgentDetails from '../views/myAccount/SubAgentList/SubAgentDetails';

//Others
import ImagePreviewScreen from './mobileActivation/models/ImagePreview';

// LTE Screens
import CoverageMapScreen from './lteActivation/CoverageMapScreen';
import LteActScreen from './lteActivation';
import ConfirmModalLte from './lteActivation/modals/ConfirmModal';
import DropdownModalLte from './lteActivation/modals/DropdownModal';
import OutstandingModalLte from './lteActivation/modals/OutstandingModal';
import OtpNumberSelectionModal from './lteActivation/modals/OtpNumberSelectionModal';
import OtpEnterModal from './lteActivation/modals/OtpEnterModal';
import LteGeneralModal from './lteActivation/modals/GeneralModalAlert';
import NumberSelectionModalScreen from './lteActivation/modals/NumberSelectionModalScreen';
import DataSachetSelectionModalScreen from './lteActivation/modals/DataSachetSelectionModal';
import HbbActivationModal from './lteActivation/modals/HbbActivationModal';

// DTV Screens
import DtvActScreen from './dtvActivation';
import DtvAdditionalChannelsScreen from './dtvActivation/DtvAdditionalChannels';
import PackageSelectionModal from './dtvActivation/modals/PackageSelectionModal';
import OutstandingModalDtv from './dtvActivation/modals/OutstandingModal';
import OfferSelectionModal from './dtvActivation/modals/OfferSelectionModal';
import ConfirmModal from '../screens/dtvActivation/modals/ConfirmModal';
import EoafConfirmModal from './dtvActivation/modals/EoafConfirmModal';

//Common Screens
import BarcodeScannerModuleScreen from './common/BarcodeScannerModule';
import ImageCaptureModuleScreen from './common/ImageCaptureModule';
import SignaturePadModuleScreen from './common/SignaturePadModule';
import CommonModalAlert from './common/CommonModalAlert';
import CustomerOutstandingModal from './common/CustomerOutstandingModal';
import PdfReaderScreen from './common/PdfReader';


//==============================  CFSS related Screens =================================================

//Stock Management related screens
import CpeTrackerScreen from './stockManagement/cpeTracker/index';
import CpeTrackerDetailScreen from './stockManagement/cpeTracker/cpeTrackerDetailIndex';
import BarcodeScannerCpeTrackerScreen from './stockManagement/cpeTracker/BarcodeScanner';
import StockManagementScreen from './stockManagement/index';
import StockAvailabilityScreen from './stockManagement/stockAvailability/index';
//-------------------------------------------------------------------------------
import StockAcceptanceIndex from './stockManagement/stockAcceptance/StockAcceptanceIndex';
import StockAcceptanceScreen2 from './stockManagement/stockAcceptance/Screen2';
import StockAcceptanceScreen3 from './stockManagement/stockAcceptance/Screen3';
import StockAcceptanceScreen4 from './stockManagement/stockAcceptance/Screen4';
import AlertSuccessModal from './stockManagement/stockAcceptance/modals/AlertSuccess';
import AlertSuccessCoustmerModal from './stockManagement/cpeTracker/modals/AlertSuccessCoustmer';
import StockAcceptBarcodeScannerScreen from './stockManagement/stockAcceptance/BarcodeScanner';
import ConfirmModalScreen from './stockManagement/stockAcceptance/modals/ConfirmModelScreen';
import LobListScreen from './stockManagement/stockAvailability/LobView/index';
import LobListDetailScreen from './stockManagement/stockAvailability/LobView/lobList';
import DateListScreen from './stockManagement/stockAvailability/DateView/index';
import DateListDetailScreen from './stockManagement/stockAvailability/DateView/dateList';

import StockRequestApprovalScreen from './stockRequestApproval/index';
import RequestApprovalDetailScreen from './stockRequestApproval/StockRequestApprovalDetailIndex';
import ApprovalModal from './stockRequestApproval/models/ApprovalModal';
import ApprovalConfirmModal from './stockRequestApproval/models/ApprovalConfirmModal';
import ApprovalRejectModal from './stockRequestApproval/models/GeneralModalConfirmReject';
import GeneralModalConfirmScreen from '../screens/general/GeneralConfirmScreenModel';
import GenaralModelSucessApproval from './general/GenaralModelSucessApproval';
import GenaralModelReject from './general/GenaralModelReject';

//Direct Sale related screens
import DirectSaleScreen from './directSale/index';
import WarrentyReplacementScreen from './directSale/warrentySales/index';
import WarrentyReplacementDetailScreen from './directSale/warrentySales/screen2';
import SerialSuccessModalScreen from './directSale/warrentySales/serialSuccessModalScreen';
import WarrentyReplacementItemInfo from './directSale/warrentySales/replaceItemInfo';
import ProvisioningScreen from './directSale/warrentySales/provision';
import AccessorySaleTileScreen from './directSale/accessorySales';
import AccessorySaleScreen2 from './directSale/accessorySales/AccessorySalePage2';
import AccessorySaleScreen3 from './directSale/accessorySales/AccessorySalePage3';
import AccessorySalesGeneralAlertScreenModal from './directSale/accessorySales/modals/GeneralAlertScreen';
import AccessorySaleScreen1 from './directSale/accessorySales/AccessorySalePage1';

//WOM related screens
import WomLandingScreen from './wom/WomLandingScreen';
import PendingWOdersScreen from './wom/PendingWOdersScreen';
import WomDetailScreen from './wom/WomDetailScreen';
import ExtendedSortingScreen from './wom/ExtendedSortingScreen';
import RejectWOScreen from './wom/RejectWOScreen';
import DTVInstallationScreen from './wom/dtvInstallation/DTVInstallationIndex';
import DTVInstallationItemScreen from './wom/dtvInstallation/DTVInsatllationItemIndex';
import DTVProvisionStatusScreen from './wom/dtvInstallation/DTVProvisionStatusScreen';
import CommonAlertModelScreen from './general/CommonAlertModelScreen';
import WomSlaBreachScreen from './wom/dtvInstallation/SlaBreach';
import RtCrsLstScreen from './wom/dtvTroubleShooting/RtCrsLstScreen';
import RtCrsFrmScreen from './wom/dtvTroubleShooting/RtCrsFrmScreen';
import VisitHistoryScreen from './wom/VisitHistoryScreen';
import VisitHistoryDetailScreen from './wom/VisitHistoryDetailScreen';
import DTVHybridScreen from './wom/dtvInstallation/DTVHybridScreen';
import SIMSTBChangeScreen from './wom/SIMSTBChange/SIMSTBChange';
import ReplaceInstalledItemsScreen from './wom/dtvInstallation/ReplaceInstalledItemsScreen';
import ReplaceConfirmAlertScreen from './wom/dtvInstallation/ReplaceConfirmAlertScreen';
import WarrantyConfirmAlertScreen from './wom/dtvTroubleShooting/WarrantyConfirmAlertScreen';
import MaterialValidationAlertScreen from './wom/dtvInstallation/MaterialValidationAlertScreen';
import RemovalStatusScreen from './wom/dtvRemoval/RemovalStatusScreen';
import RemovalItemsScreen from './wom/dtvRemoval/RemovalItemsScreen';
import CustomerFeedbackScreen from './wom/general/CustomerFeedbackScreen';
import WOMCustomerConfirmationScreen from './wom/general/CustomerConfirmationScreen';
import SignaturePadComponentWomScreen from './wom/general/SignaturePadComponentWomScreen';
import RefixItemsScreen from './wom/dtvRelocation/dtvRefix/RefixItemsScreen';
import RecoveryItemsScreen from './wom/dtvRelocation/dtvRecovery/RecoveryItemsScreen';
import AddNewItemsScreen from './wom/SalesandReplacement/AddNewItemsScreen';
import GeneralExceptionSerialModal from './wom/general/GeneralExceptionModalSerialScreen';
import WOMBScannerScreen from './wom/general/WOMBCScannerScreen';
import StbDeliveryDialogOwnedScreen from './wom/dtvStbUpgrade/stbDeliveryDialogOwnedScreen';
import stbDeliveryCustomerOwnedScreen from './wom/dtvStbUpgrade/stbDeliveryCustomerOwnedScreen';
import PostToPreConversionScreen from './wom/dtvPostToPreConv/PostToPreConversionScreen';
import DisconnectionStatusScreen from './wom/dtvPostToPreConv/DisconnectionStatusScreen';
import WomModalApprovalScreen from './wom/general/WomModalApprovalScreen';
import WomModalSuccessScreen from './wom/general/WomModalSuccessScreen';
import WomModalFailureScreen from './wom/general/WomModalFailureScreen';
import TRBReplacmentScreen from './wom/dtvTroubleShooting/TRBReplacmentScreen';
import WOMBarcodeScannerScreen from './wom/general/WOMBarcodeScannerScreen';
import DetailDisplayScreen from './wom/general/DetailDisplayScreen';
import InitialTRBViewScreen from './wom/dtvTroubleShooting/InitialTRBViewScreen';
import EZCashPINMissingAlertScreen from './wom/general/EZCashPINMissingAlertScreen';
import NetworkScreen from './wom/NetworkScreen';

////////////////////////////////////////////////////////////////////////////////////////////////////////////
import Uber from './Uber';

////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==============================  End CFSS related Screens ===============================================

// modify the store created via redux using reactron
const middleware = applyMiddleware(ReduxThunk);
const store = Reactotron.createStore(reducers, composeWithDevTools(middleware));
//const store = createStore(reducers, {}, applyMiddleware(ReduxThunk)); //without reactron

export function registerScreens() {
  Navigation.registerComponent('DialogRetailerApp.views.Drawer', () => Drawer, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.StartupScreen', () => StartupScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.LoginScreen', () => LoginScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.HomeTileScreen', () => HomeScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.settings.MainScreen', () => SettingsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.settings.LanguageSelectionScreen', () => LanguageSelectionScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.settings.PinChangeScreen', () => PinChangeScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModel', () => GeneralModel, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModalSimChange', () => GeneralModalSimChange, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModalPinChange', () => GeneralModalPinChange, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModelMobileAct', () => GeneralModelMobileAct, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModelMobileActPostPaid', () => GeneralModelMobileActPostPaid, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModalException', () => GeneralModalException, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralSucessModal', () => GeneralSucessModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModalBill', () => GeneralModalBill, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModalConfirmBill', () => GeneralModalConfirmBill, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.OutstandingModal', () => OutstandingModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.PackageInformation', () => PackageInformation, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BarcodeScannerScreen', () => BarcodeScannerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BarcodeScannerCommonScreen', () => BarcodeScannerCommonScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CameraScreen', () => CameraScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CameraReCaptureScreen', () => CameraReCaptureScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.MobileActivationScreen', () => MobileActivationScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.OTPScreen', () => OTPModelMobileAct, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModelSuccessMobileAct', () => GeneralModelSuccessMobileAct, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SignaturePadScreen', () => SignaturePadScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.MobileActModel', () => MobileActModel, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.MobileActOtpModal', () => MobileActOtpModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.OfferSelectionModel', () => OfferSelectionModel, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.NumberSelectionModel', () => NumberSelectionModel, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.NicCaptureModel', () => NicCaptureModel, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SimChangeScreen', () => SimChangeScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BillPaymentScreen', () => BillPaymentScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CameraUI', () => CameraUI, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.TotalPayments', () => TotalPayment, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.SignatuerConfirmModal', () => SignatuerConfirmModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DeliveryMainScreen', () => DeliveryMainScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.PendingOrderDeliveryIndex', () => PendingWorkOrdersIndex, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WorkOrderDetailsScreen', () => WorkOrderDetailsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CustomerConfirmationScreen', () => CustomerConfirmationScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CustomerVerificationScreen', () => CustomerVerificationScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.FeedbackScreen', () => FeedbackScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RejectOrderScreen', () => RejectOrderScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SlaBreachScreen', () => SlaBreachScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.ConfirmAlertModal', () => ConfirmAlertModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.ActivateAlertModal', () => ActivateAlertModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.RejectAlertModal', () => RejectAlertModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.GeneralAlertModal', () => GeneralAlertModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.FilterWorkOrdersIndex', () => FilterWorkOrdersIndex, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.NoWorkOrderScreen', () => NoWorkOrderScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SignaturePadCommonScreen', () => SignaturePadCommonScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.EzCashErrorPopup', () => EzCashErrorPopup, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.NumberSelectionScreen', () => NumberSelectionScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.TransactionHistoryMainScreen', () => TransactionHistoryMainScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SimChangeHistoryMainScreen', () => SimChangeHistoryMainScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SimChangeDetailsScreen', () => SimChangeDetailsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.FilterSimChangeHistoryScreen', () => FilterSimChangeHistoryScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BillPaymentHistoryMainScreen', () => BillPaymentHistoryMainScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BillPaymentDetailsScreen', () => BillPaymentDetailsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.FilterBillPaymentHistoryScreen', () => FilterBillPaymentHistoryScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ActivationStatusMainScreen', () => ActivationStatusMainScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ActivationStatusDetailsScreen', () => ActivationStatusDetailsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.FilterActivationStatusScreen', () => FilterActivationStatusScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ImageResubmitScreen', () => ImageResubmitScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SimChangeConfirmModal', () => SimChangeConfirmModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.settings.HelpScreen',()=> HelpScreen,store,Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ImagePreviewScreen',()=> ImagePreviewScreen,store,Provider);

  ///////////////////////////////
  Navigation.registerComponent('DialogRetailerApp.views.Uber',()=> Uber,store,Provider);

  //Sub agent management screens
  Navigation.registerComponent('DialogRetailerApp.views.myAccount.MainScreen', () => subManagementContainer, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.myAccount.SubAgents', () => SubAgentList, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.myAccount.SubAgentDetails', () => SubAgentDetails, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OTPModalScreenAgent', () => OTPModalScreenAgent, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.NumberModelAgent', () => NumberModel, store, Provider);

  //Authentication related screens
  Navigation.registerComponent('DialogRetailerApp.modals.GeneralModalAuthScreen', () => GeneralModalAuthScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OTPModalScreen', () => OTPModalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.PinResetModalScreen', () => PinResetModalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.GeneralModalAlert', () => GeneralModalAlert, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RetailerRegistrationScreen', () => RetailerRegistrationScreen, store, Provider);

  // LTE Activation Screens
  Navigation.registerComponent('DialogRetailerApp.screens.CoverageMapScreen', () => CoverageMapScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.LteActScreen', () => LteActScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.ConfirmModalLte', () => ConfirmModalLte, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.DropdownModalLte', () => DropdownModalLte, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OutstandingModalLte', () => OutstandingModalLte, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OtpNumberSelectionModal', () => OtpNumberSelectionModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OtpEnterModal', () => OtpEnterModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.LteGeneralModal', () => LteGeneralModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.LteNumberSelectionModal', () => NumberSelectionModalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.DataSachetSelectionModal', () => DataSachetSelectionModalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.HbbActivationModal', () => HbbActivationModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.DTVConfirmModal', () => ConfirmModal, store, Provider);

  // DTV Activation Screens
  Navigation.registerComponent('DialogRetailerApp.views.DtvActScreen', () => DtvActScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DtvAdditionalChannelsScreen', () => DtvAdditionalChannelsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.PackageSelectionModal', () => PackageSelectionModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OutstandingModalDtv', () => OutstandingModalDtv, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.OfferSelectionModal', () => OfferSelectionModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.EoafConfirmModal', () => EoafConfirmModal, store, Provider);
  //Common Screens
  Navigation.registerComponent('DialogRetailerApp.views.BarcodeScannerModule', () => BarcodeScannerModuleScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ImageCaptureModule', () => ImageCaptureModuleScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SignaturePadModule', () => SignaturePadModuleScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.CommonModalAlert', () => CommonModalAlert, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.CustomerOutstandingModal', () => CustomerOutstandingModal, store, Provider);

  //============================== CFSS related screens ============================================================================

  Navigation.registerComponent('DialogRetailerApp.views.CpeTracker', () => CpeTrackerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CpeTrackerDetailView', () => CpeTrackerDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.BarcodeScannerCpeTrackerScreen', () => BarcodeScannerCpeTrackerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockManagement', () => StockManagementScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAvailability', () => StockAvailabilityScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.LobList', () => LobListScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.LobListDetail', () => LobListDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DateList', () => DateListScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DateListDetail', () => DateListDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockRequestApproval', () => StockRequestApprovalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockRequestDetailView', () => RequestApprovalDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GenaralModelSucessApproval', () => GenaralModelSucessApproval, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.ApprovalRejectModal', () => ApprovalRejectModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.ApprovalModal', () => ApprovalModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.ApprovalConfirmModal', () => ApprovalConfirmModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GenaralModelReject', () => GenaralModelReject, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralModelApprovals', () => GeneralModelApprovals, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DirectSale', () => DirectSaleScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WarrentyReplacement', () => WarrentyReplacementScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WarrentyReplacementDetailsPage', () => WarrentyReplacementDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WarrentyReplacementItemInfo', () => WarrentyReplacementItemInfo, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WarrentyProvisioning', () => ProvisioningScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WarrentyReplacementSerialModal', () => SerialSuccessModalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AccessorySaleTileScreen', () => AccessorySaleTileScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AccessorySalePage2', () => AccessorySaleScreen2, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AccessorySalesDetailsPage', () => AccessorySaleScreen3, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AccessorySalesGeneralAlert', () => AccessorySalesGeneralAlertScreenModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AccessorySalePage1', () => AccessorySaleScreen1, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAcceptance', () => StockAcceptanceIndex, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAcceptancePage2', () => StockAcceptanceScreen2, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAcceptancePage3', () => StockAcceptanceScreen3, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAcceptancePage4', () => StockAcceptanceScreen4, store, Provider); //TODO : Please review
  Navigation.registerComponent('DialogRetailerApp.views.AlertSuccess', () => AlertSuccessModal, store, Provider); //TODO : Please review
  Navigation.registerComponent('DialogRetailerApp.views.AlertSuccess', () => AlertSuccessCoustmerModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StockAcceptanceBarcodeScanner', () => StockAcceptBarcodeScannerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ConfirmModal', () => ConfirmModalScreen, store, Provider);  
  Navigation.registerComponent('DialogRetailerApp.modals.PdfReaderModal', () => PdfReaderScreen, store, Provider);  

  //WOM related screens
  Navigation.registerComponent('DialogRetailerApp.views.WomLandingView', () => WomLandingScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.PendingWOdersView', () => PendingWOdersScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WomDetailView', () => WomDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.EtendedSortingView', () => ExtendedSortingScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RejectIndex', () => RejectWOScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DTVInstallation', () => DTVInstallationScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DTVInstallationItem', () => DTVInstallationItemScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DTVProvisionStatus', () => DTVProvisionStatusScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CustomerFeedback', () => CustomerFeedbackScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WOMCustomerConfirmation', () => WOMCustomerConfirmationScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RtCrsLstScreen', () => RtCrsLstScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RtCrsFrmScreen', () => RtCrsFrmScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SignaturePadComponentWom', () => SignaturePadComponentWomScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.AddNewItem', () => AddNewItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WOMBCScanner', () => WOMBScannerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.CommonAlertModel', () => CommonAlertModelScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WomSlaBreachScreen', () => WomSlaBreachScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.VisitHistory', () => VisitHistoryScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.VisitHistoryDetail', () => VisitHistoryDetailScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.GeneralModalConfirm', () => GeneralModalConfirmScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DTVHybridScreen', () => DTVHybridScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.ReplaceInstalledItems', () => ReplaceInstalledItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.ReplaceConfirmAlert', () => ReplaceConfirmAlertScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.SIMSTBChangeScreen', () => SIMSTBChangeScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RemovalItemsScreen', () => RemovalItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RemovalStatusScreen', () => RemovalStatusScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RefixItemsScreen', () => RefixItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RecoveryItemsScreen', () => RecoveryItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.MaterialValidationAlert', () => MaterialValidationAlertScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DetailDisplayView', () => DetailDisplayScreen, store, Provider);

  Navigation.registerComponent('DialogRetailerApp.views.RemovalItemsScreen', () => RemovalItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RemovalStatusScreen', () => RemovalStatusScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.RecoveryItemsScreen', () => RecoveryItemsScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.models.GeneralExceptionSerialModal', () => GeneralExceptionSerialModal, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WOMBarcodeScannerScreen', () => WOMBarcodeScannerScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.StbDeliveryDialogOwnedScreen', () => StbDeliveryDialogOwnedScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.stbDeliveryCustomerOwnedScreen', () => stbDeliveryCustomerOwnedScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.TRBReplacmentScreen', () => TRBReplacmentScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.WarrantyConfirmAlertScreen', () => WarrantyConfirmAlertScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.PostToPreConversionScreen', () => PostToPreConversionScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.DisconnectionStatusScreen', () => DisconnectionStatusScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WomModalApprovalScreen', () => WomModalApprovalScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WomModalSuccessScreen', () => WomModalSuccessScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.WomModalFailureScreen', () => WomModalFailureScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.InitialTRBView', () => InitialTRBViewScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.views.EZCashPINMissingAlert', () => EZCashPINMissingAlertScreen, store, Provider);
  Navigation.registerComponent('DialogRetailerApp.modals.NetworkScreen', () => NetworkScreen, store, Provider);
 
  //==============================  End CFSS related Screens =================================================================== 
}

export function registerScreenVisibilityListener() {
  new ScreenVisibilityListener({
    willAppear: ({ screen }) => console.log(`Displaying screen ${screen}`),
    didAppear: ({ screen, startTime, endTime, commandType }) => console.log('screenVisibility', `Screen ${screen} 
        displayed in ${endTime - startTime} millis [${commandType}]`),
    willDisappear: ({ screen }) => console.log(`Screen will disappear ${screen}`),
    didDisappear: ({ screen }) => console.log(`Screen disappeared ${screen}`)
  }).register();
}
