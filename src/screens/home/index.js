import React from 'react';
import Orientation from 'react-native-orientation';
import HomeMainView from '../../views/home/';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class HomeTileScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: false,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true,
    navBarButtonColor: Colors.navBarButtonColor
  };

  static navigatorButtons = {
    leftButtons: [
      {
        id: 'sideMenu'
      }
    ],
    animated: true
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### HomeTileScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    Analytics.logEvent('dsa_home_page');
  }

  render() {
    return (<HomeMainView {... this.props}/>);
  }
}

export default HomeTileScreen;
