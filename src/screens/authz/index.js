/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author:  Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:22:11 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Parthipan Kugadoss (parthipan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import Orientation from 'react-native-orientation';
import LoginMain from '../../views/authz';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class LoginScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### LoginScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_login');
  }

  render() {
    return (<LoginMain {...this.props}/>);
  }
}

export default LoginScreen;
