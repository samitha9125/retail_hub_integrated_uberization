/*
 * File: RetailerRegistration.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 6th June 2018 11:33:48 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:21:07 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import Orientation from 'react-native-orientation';
import RetailerRegistration from '../../views/authz/RetailerRegistration';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class RetailerRegistrationScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### RetailerRegistration :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_retailer_registration');
  }

  render() {
    return (<RetailerRegistration {...this.props}/>);
  }
}

export default RetailerRegistrationScreen;
