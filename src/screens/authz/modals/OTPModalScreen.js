import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import OTPModal from '../../../views/authz/modals/OTPModal';

class OTPModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<OTPModal {... this.props} />);
  }
}

export default OTPModalScreen;
