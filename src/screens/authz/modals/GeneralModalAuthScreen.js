import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import GeneralModalAuth from '../../../views/authz/modals/GeneralModalAuth';

class GeneralModalAuthScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralModalAuth {... this.props} />);
  }
}

export default GeneralModalAuthScreen;
