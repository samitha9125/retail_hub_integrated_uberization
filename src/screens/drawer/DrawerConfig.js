const DtawerConfig = { // optional, add this if you want a side menu drawer in your app
  left: { // optional, define if you want a drawer from the left
    screen: 'DialogRetailerApp.views.Drawer', // unique ID registered with Navigation.registerScreen
    passProps: {} // simple serializable object that will pass as props to all top screens (optional)
  },
  style: { // ( iOS only )
    drawerShadow: true, // optional, add this if you want a side menu drawer shadow
    contentOverlayColor: 'rgba(0,0,0,0.25)', // optional, add this if you want a overlay color when drawer is open
    leftDrawerWidth: 50, // optional, add this if you want a define left drawer width (50=percent)
    rightDrawerWidth: 50 // optional, add this if you want a define right drawer width (50=percent)
  },
  disableOpenGesture: false // optional, can the drawer be opened with a swipe instead of button
};

export default DtawerConfig;
