import React from 'react';
import Drawer from '../../views/drawer/Drawer';

class DrawerScreen extends React.Component {
  render() {
    const { navigator } = this.props;
    return (<Drawer navigator={navigator} />);
  }
}

export default DrawerScreen;
