import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import PendingOrderDeliveryList from '../../views/delivery/PendingOrderDeliveryList';

class PendingWorkOrdersIndex extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### Pending Work Orders list :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app_order_list');
  }

  render() {
    return (<PendingOrderDeliveryList {... this.props}/>);
  }
}

export default PendingWorkOrdersIndex;
