import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import RejectOrder from '../../views/delivery/RejectOrder';

class RejectOrderScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### RejectOrder :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app_reject_order');
  }

  render() {
    return (<RejectOrder {... this.props}/>);
  }
}

export default RejectOrderScreen;
