import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import CustomerVerification from '../../views/delivery/CustomerVerification';

class CustomerVerificationScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }
  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### CustomerVerification :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app_customer_verify');
  }

  render() {
    return (<CustomerVerification {... this.props}/>);
  }
}

export default CustomerVerificationScreen;
