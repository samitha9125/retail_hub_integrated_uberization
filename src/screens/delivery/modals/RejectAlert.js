import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import RejectAlert from '../../../views/delivery/modals/RejectAlert';
import Colors from '../../../config/colors';

class RejectAlertModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<RejectAlert {... this.props} />);
  }
}

export default RejectAlertModal;
