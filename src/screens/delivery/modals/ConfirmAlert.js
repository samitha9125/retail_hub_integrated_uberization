import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import ConfirmAlert from '../../../views/delivery/modals/ConfirmAlert';
import Colors from '../../../config/colors';

class ConfirmAlertModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }
  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<ConfirmAlert {... this.props} />);
  }
}

export default ConfirmAlertModal;
