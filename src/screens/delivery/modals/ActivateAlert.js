import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import ActivateAlert from '../../../views/delivery/modals/ActivateAlert';
import Colors from '../../../config/colors';

class ActivateAlertModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<ActivateAlert {... this.props} />);
  }
}

export default ActivateAlertModal;
