import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import GeneralAlert from '../../../views/delivery/modals/GeneralAlert';
import Colors from '../../../config/colors';

class GeneralAlertModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<GeneralAlert {... this.props} />);
  }
}

export default GeneralAlertModal;
