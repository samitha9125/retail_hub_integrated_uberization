import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import { View } from 'react-native';
import StockRequestApproval from '../../views/stockRequestApproval/index';

class RequestApprovalScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### StockRequestApprovalMain :: componentDidMount');
    Analytics.logEvent('dsa_mobile_cfss_app');
  }

  render() {
    return (<View>

      <StockRequestApproval {...this.props} />
    </View>);
  }
}

export default RequestApprovalScreen;
