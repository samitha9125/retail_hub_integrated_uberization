import React, { Component } from 'react';

import GeneralModalConfirmApproval from '../../../views/stockRequestApproval/models/GenaralModalConfirmApproval';
import Colors from '../../../config/colors';

class ApprovalConfirmModal extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
      return (<GeneralModalConfirmApproval {...this.props}  message={this.props.message} />);
    }
}

export default ApprovalConfirmModal;
