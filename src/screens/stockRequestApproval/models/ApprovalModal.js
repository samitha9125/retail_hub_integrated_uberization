import React, { Component } from 'react';

import GeneralModalApproval from '../../../views/stockRequestApproval/models/GenaralModalApproval';
import Colors from '../../../config/colors';

class ApprovalModal extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
      const { navigator } = this.props;
      return (<GeneralModalApproval navigator={navigator} />);
    }
}

export default ApprovalModal;
