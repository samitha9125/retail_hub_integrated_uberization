import React, { Component } from 'react';

import GeneralModalConfirmReject from '../../../views/stockRequestApproval/models/GenaralModelConfirmReject';
import Colors from '../../../config/colors';

class ApprovalRejectModal extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
      return (<GeneralModalConfirmReject {...this.props}  message={this.props.message} />);
    }
}

export default ApprovalRejectModal;