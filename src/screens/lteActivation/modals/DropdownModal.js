import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import DropDownModal from '../../../views/lteActivation/modals/DropdownModal';
import Colors from '../../../config/colors';

class DropDownModalLte extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_drop_down_modal');
  }

  render() {
    return (<DropDownModal {... this.props}/>);
  }
}

export default DropDownModalLte;
