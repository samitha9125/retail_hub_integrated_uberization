import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import DataSachetSelectionModal from '../../../views/lteActivation/modals/DataSachetSelectionModal';
import Colors from '../../../config/colors';

class DataSachetSelectionModalLte extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_offer_selection_modal');
  }

  render() {
    return (<DataSachetSelectionModal {... this.props}/>);
  }
}

export default DataSachetSelectionModalLte;

