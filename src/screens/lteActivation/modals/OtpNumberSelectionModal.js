import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import OtpNumberSelectionModal from '../../../views/lteActivation/modals/OtpNumberSelectionModal';
import Colors from '../../../config/colors';

class OtpNumberSelectionModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_confirm_modal');
  }

  render() {
    return (<OtpNumberSelectionModal {... this.props}/>);
  }
}

export default OtpNumberSelectionModalScreen;
