import React, { Component } from 'react';
import HbbActivationMain from '../../../views/lteActivation/modals/HbbActivation';
import Colors from '../../../config/colors';

class HbbActivationModel extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<HbbActivationMain {...this.props}/>);
  }
}

export default HbbActivationModel;
