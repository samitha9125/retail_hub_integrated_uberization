import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import OtpEnterModal from '../../../views/lteActivation/modals/OtpEnterModal';
import Colors from '../../../config/colors';

class OtpEnterModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_confirm_modal');
  }

  render() {
    return (<OtpEnterModal {... this.props}/>);
  }
}

export default OtpEnterModalScreen;
