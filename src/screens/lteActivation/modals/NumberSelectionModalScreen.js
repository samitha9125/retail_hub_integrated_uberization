import React from 'react';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import Colors from '../../../config/colors';
import NumberSelectionModal from '../../../views/lteActivation/modals/NumberSelectionModal';

class NumberSelectionModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_lte_activation_drop_down_modal');
  }

  render() {
    return (<NumberSelectionModal {... this.props}/>);
  }
}

export default NumberSelectionModalScreen;
