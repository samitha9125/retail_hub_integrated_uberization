import React from 'react';
import Orientation from 'react-native-orientation';
import LteActMain from '../../views/lteActivation/';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class LteActScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### LteActMainScreen :: componentDidMount');
    Orientation.lockToPortrait();
    Analytics.logEvent('dsa_lte_activation');
  }

  render() {
    return (<LteActMain {...this.props}/>);
  }
}

export default LteActScreen;
