import React from 'react';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import ImageCaptureModule from '../../views/common/ImageCaptureModule';

class ImageCaptureModuleScreen extends React.Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
    }

    componentDidMount() {
      console.log('### ImageCaptureModuleScreen :: componentDidMount');
      Analytics.logEvent('dsa_image_capture');
    }

    render() {
      return (<ImageCaptureModule { ... this.props} />);
    }
}

export default ImageCaptureModuleScreen;
