import React from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  BackHandler,
} from 'react-native';
import Pdf from 'react-native-pdf';
import Orientation from 'react-native-orientation';
import { Header } from '../../views/common/components/Header';
import Colors from '../../config/colors';

class PdfReaderScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    Orientation.lockToLandscape();
  }

  handleBackButtonClick = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animated: true, animationType: 'fade' });
  }

  render() {
    let source = { uri: this.props.tncUrl, cache: false };
    
    return (
      <View style={styles.container}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.props.title} />
        <Pdf
          source={source}
          style={styles.pdfL}
          scale={3}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width
  },
  pdfL: {
    flex: 1,
    width: Dimensions.get('window').height
  },
});

export default PdfReaderScreen;