import React from 'react';
import Orientation from 'react-native-orientation';
import SignaturePadModule from '../../views/common/SignaturePadModule';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class SignaturePadScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true,
    orientation: 'landscape'
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log('### SignaturePadScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Orientation.lockToLandscapeLeft();
    Orientation.addOrientationListener(this._orientationDidChange);
    Analytics.logEvent('dsa_common_get_signature');
  }

  _orientationDidChange = (orientation) => {
    console.log('orientation: ',orientation);
    if (orientation === 'LANDSCAPE') {
      console.log('do something with landscape layout');
    } else {
      console.log('do something with portrait layout');
    }
  }

  componentWillMount(){
    Orientation.lockToLandscapeLeft();
  }

  componentWillUnmount() {
    console.log("Component Unmounting. Switching to Portrait");
    Orientation.lockToPortrait();
    if (this.props.confirmModal){
      this.props.confirmModal();
    }
  }

  render() {
    return (<SignaturePadModule {... this.props}/>);
  }
}

export default SignaturePadScreen;
