import React from 'react';
import Orientation from 'react-native-orientation';
import { Analytics } from '@Utils';
import { Colors } from '@Config';
import CustomerOutstandingModal from '../../views/common/CustomerOutstandingModal';

class CustomerOutstandingModalScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_common_outstanding_modal');
  }

  render() {
    return (<CustomerOutstandingModal {... this.props}/>);
  }
}

export default CustomerOutstandingModalScreen;
