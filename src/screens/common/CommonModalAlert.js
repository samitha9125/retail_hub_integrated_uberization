import React from 'react';
import Orientation from 'react-native-orientation';
import { Analytics } from '@Utils';
import { Colors } from '@Config';
import CommonModalAlert from '../../views/common/CommonModalAlert';

class CommonModalAlertScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_common_modal');
  }

  render() {
    return (<CommonModalAlert {... this.props}/>);
  }
}

export default CommonModalAlertScreen;
