import React from 'react';
import Orientation from 'react-native-orientation';
import BarcodeScannerModule from '../../views/common/BarcodeScannerModule';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class BarcodeScannerModuleScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
  }

  componentDidMount() {
    console.log('### BarcodeScannerModuleScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    Analytics.logEvent('dsa_barcode_scan');
  }

  render() {
    return (<BarcodeScannerModule {... this.props}/>);
  }
}

export default BarcodeScannerModuleScreen;
