import React,{ Component } from 'react';
import { } from 'react-native';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import StockApprovalDetails from '../../views/stockRequestApproval/StockReAppDetail';

class StockReAppIndex extends Component {
  static navigatorStyle = {
    navBarHidden: false,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
     
  };
  componentDidMount() {
    console.log('### STOCK ACCEPTANCE :: componentDidMount');
    //   Analytics.logEvent('dsa_activation_status');
  }

  render() {
    return (<StockApprovalDetails {...this.props}/>);
  } 
}

export default StockReAppIndex;
