import React, { Component } from 'react';
import { View, Text, PixelRatio } from 'react-native';
import StockAcceptancePage3 from '../../../views/stockManagement/stockAcceptance/page3';
import Colors from './../../../config/colors';

class StockAcceptanceScreen3 extends Component {
    static navigatorStyle = {
        topTabTextColor: '#ffffff',
        selectedTopTabTextColor: '#ffffff',
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true,

        // Icons
        topTabIconColor: '#ffffff',
        selectedTopTabIconColor: '#ffffff',

        // Tab indicator
        selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
        selectedTopTabIndicatorColor: '#ffffff'
    };

    render() {
        return (<StockAcceptancePage3 {...this.props} />);
    }
}

export default StockAcceptanceScreen3;