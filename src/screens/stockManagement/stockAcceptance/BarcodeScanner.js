import React, { Component } from 'react';

import StockAcceptanceBarcodeScanner from '../../../views/stockManagement/stockAcceptance/BarcodeScanner';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class StockAcceptBarcodeScannerScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### BarcodeScannerScreen :: componentDidMount');
      Analytics.logEvent('dsa_sim_scan');
    }

    render() {
      const { navigator } = this.props;

      return (<StockAcceptanceBarcodeScanner {...this.props} />);
    }
}

export default StockAcceptBarcodeScannerScreen;
