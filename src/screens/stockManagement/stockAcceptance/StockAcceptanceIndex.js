import React, { Component } from 'react';
import { PixelRatio, View, Text } from 'react-native';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import StockAcceptance from '../../../views/stockManagement/stockAcceptance/index';

class StockAcceptanceIndex extends Component {
  static navigatorStyle = {
    topTabTextColor: '#ffffff',
    selectedTopTabTextColor: '#ffffff',
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true,

    // Icons
    topTabIconColor: '#ffffff',
    selectedTopTabIconColor: '#ffffff',

    // Tab indicator
    selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
    selectedTopTabIndicatorColor: '#ffffff'
  };
  componentDidMount() {
    console.log('### STOCK ACCEPTANCE :: componentDidMount');
    //   Analytics.logEvent('dsa_activation_status');
  }

  render() {
    
    return (<StockAcceptance {...this.props} />);
  }
}

export default StockAcceptanceIndex;
