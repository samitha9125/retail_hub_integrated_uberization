import React, { Component } from 'react';
import { View, Text, PixelRatio } from 'react-native';
import StockAcceptancePage4 from '../../../views/stockManagement/stockAcceptance/page4';
import Colors from '../../../config/colors';

class StockAcceptanceScreen4 extends Component {

    static navigatorStyle = {
        topTabTextColor: '#ffffff',
        selectedTopTabTextColor: '#ffffff',
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true,

        // Icons
        topTabIconColor: '#ffffff',
        selectedTopTabIconColor: '#ffffff',

        // Tab indicator
        selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
        selectedTopTabIndicatorColor: '#ffffff'
    };


    render() {
        return (
            <StockAcceptancePage4 {...this.props} />
        );
    }

}

export default StockAcceptanceScreen4;