import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import ConfirmModal from '../../../../views/stockManagement/stockAcceptance/modals/ConfirmModal';
import Colors from '../../../../config/colors';

class ConfirmModalScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentWillMount() {
        Orientation.lockToPortrait();
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }
    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        return (<ConfirmModal {... this.props} />);
    }
}

export default ConfirmModalScreen;
