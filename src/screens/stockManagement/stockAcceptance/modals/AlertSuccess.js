import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import AlertSuccess from '../../../../views/stockManagement/stockAcceptance/modals/AlertSuccess';
import Colors from '../../../../config/colors';

class AlertSuccessModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<AlertSuccess {... this.props} />);
  }
}

export default AlertSuccessModal;
