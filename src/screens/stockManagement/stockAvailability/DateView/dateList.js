import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../../config/colors';
import Analytics from '../../../../utills/Analytics';
import { View  } from 'react-native';
import DateListDetail from '../../../../views/stockManagement/stockAvailability/DateView/dateListDetail';

class DateListDetailScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DeliveryMain :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app');
  }

  render() {
    // return (<StockManagement {...this.props}/>);
    return (<View>
      {/* <Text>SOLD LIST SCREEN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>></Text> */}
      <DateListDetail {...this.props} />
    </View>);
  }
}

export default DateListDetailScreen;
