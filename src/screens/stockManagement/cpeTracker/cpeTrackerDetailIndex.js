import React, { Component } from 'react';

import CpeTrackerDetailView from '../../../views/stockManagement/cpeTracker/CpeTrackerDetailIndex';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class CpeTrackerDetailScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }
    componentDidMount() {
      console.log('### SimChangeScreen :: componentDidMount');
      Analytics.logEvent('dsa_sim_change');
    }

    render() {
      // const { navigator } = this.props;

      return (<CpeTrackerDetailView {...this.props} />);
    }
}

export default CpeTrackerDetailScreen;
