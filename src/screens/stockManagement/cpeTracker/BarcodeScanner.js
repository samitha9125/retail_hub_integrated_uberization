import React, { Component } from 'react';
import BarcodeScannerCpeTrackerView from '../../../views/stockManagement/cpeTracker/BarcodeScanner';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class BarcodeScannerCpeTrackerScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('dsa_home_page');
  }

  render() {
    // const { navigator } = this.props;
    console.log("CpeTracker RENDERED>>>>>>>>>>");
    return (<BarcodeScannerCpeTrackerView {...this.props} />);
  }
}

export default BarcodeScannerCpeTrackerScreen;