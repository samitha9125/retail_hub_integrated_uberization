import React, { Component } from 'react';

import CpeTracker from '../../../views/stockManagement/cpeTracker/index';

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class CpeTrackerScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }
    componentDidMount() {
      console.log('### SimChangeScreen :: componentDidMount');
      Analytics.logEvent('dsa_sim_change');
    }

    render() {
      // const { navigator } = this.props;

      return (<CpeTracker {...this.props} />);
    }
}

export default CpeTrackerScreen;
