import React, { Component } from 'react';
import MobileActModelMain from '../../../views/mobileActivation/models/MobileActModel';
import Colors from '../../../config/colors';

class MobileActModel extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<MobileActModelMain {...this.props}/>);
  }
}

export default MobileActModel;
