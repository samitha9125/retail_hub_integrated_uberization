import React from 'react';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import ImagePreview from '../../../views/transactionHistory/activationStatus/components/ImagePreview';
class ImagePreviewScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### ImagePreview :: componentDidMount');
    Analytics.logEvent('dsa_genaral_exception_modal');
  }

  render() {
    return (<ImagePreview {...this.props}/>);
  }
}

export default ImagePreviewScreen;
