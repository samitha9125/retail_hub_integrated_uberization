import React, { Component } from 'react';
import ConfirmModalMain from '../../../views/mobileActivation/models/Confirm';
import Colors from '../../../config/colors';

class OutstandingModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<ConfirmModalMain {...this.props}/>);
  }
}

export default OutstandingModal;
