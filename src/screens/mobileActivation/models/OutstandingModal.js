import React, { Component } from 'react';
import OutstandingModalMain from '../../../views/mobileActivation/models/OutstandingModal';
import Colors from '../../../config/colors';

class OutstandingModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<OutstandingModalMain
      {... this.props}
      outstandingData={this.props.outstandingData}/>);
  }
}

export default OutstandingModal;
