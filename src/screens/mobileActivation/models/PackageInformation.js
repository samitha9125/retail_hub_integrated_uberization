import React, { Component } from 'react';

import PackageInfoMain from '../../../views/mobileActivation/models/PackageInformation';
import Colors from '../../../config/colors';

class PackageInformation extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
      console.log("RENDERING PackageInfo. Props: ", this.props);
      const { navigator } = this.props;
      return (<PackageInfoMain navigator={navigator} packageData={this.props} />);
    }
}

export default PackageInformation;