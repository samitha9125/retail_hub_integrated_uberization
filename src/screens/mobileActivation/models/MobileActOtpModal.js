import React, { Component } from 'react';

import MobileActOtpModalMain from '../../../views/mobileActivation/models/MobileActOtpModal';
import Colors from '../../../config/colors';

class MobileActOtpModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    const { navigator } = this.props;
    return (<MobileActOtpModalMain navigator={navigator}/>);
  }
}

export default MobileActOtpModal;
