import React, { Component } from 'react';
import ActivateAlert from '../../../views/mobileActivation/models/EzCashError';
import Colors from '../../../config/colors';

class EzCashErrorPopup extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<ActivateAlert {...this.props} />);
  }
}

export default EzCashErrorPopup;
