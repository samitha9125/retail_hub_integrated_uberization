import React, { Component } from 'react';
import MobileActMain from '../../views/mobileActivation/MobileActMain';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import Orientation from 'react-native-orientation';
// import { Header } from '../../components/others';
import { View } from 'react-native';

class MobileActScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### MobileActScreen :: componentDidMount');
    Orientation.lockToPortrait();
    Analytics.logEvent('dsa_mobile_activation');
  }

  // handleClick() {
  //   console.log("===> Button Tapped \n ") ;
  //   console.log(JSON.stringify(this.props));
  // }

  // handleBackButtonClick() {
  //   this.showAlertMsg();
  //   return true;
  // }

  //   goBackToHome = () => {
  //     console.log('xxx goBackToHome');
  //     const navigatorOb = this.props.navigator;
  //     navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  //     const me = this.props;
  //     me.resetMobileActivationState();
  //   }

  //   showAlertMsg = () => {
  //     console.log('xxx showAlertMsg');
  //     Alert.alert('', this.state.backMessage, [
  //       {
  //         text: 'Cancel',
  //         onPress: () => console.log('Cancel Pressed'),
  //         style: 'cancel'
  //       }, {
  //         text: 'OK',
  //         onPress: () => this.goBackToHome()
  //       }
  //     ], { cancelable: true });
  //   }

  render() {
    const { navigator } = this.props;
    return (
      <MobileActMain navigator={navigator} defaultEzNumber={this.props.defaultEzNumber}/>

    );
  }
}

export default MobileActScreen;
