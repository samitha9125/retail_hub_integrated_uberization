import React, { Component } from 'react';

import GeneralModalBill from '../../../views/billPayment/models/GeneralModalBill';
import Colors from '../../../config/colors';

class BillPaymentModal extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        const { navigator } = this.props;
        return (<GeneralModalBill navigator={navigator} />);
    }
}

export default BillPaymentModal;
