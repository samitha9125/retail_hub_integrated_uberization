import React, { Component } from 'react';

import GeneralModalConfirmBill from '../../../views/billPayment/models/GeneralModalConfirmBill';
import Colors from '../../../config/colors';

class BillPaymentConfirmModal extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: true,
        statusBarColor: Colors.statusBarColor
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: false });
    }

    render() {
        const { navigator } = this.props;
        return (<GeneralModalConfirmBill navigator={navigator} message={this.props.message} start_ts={this.props.start_ts} />);
    }
}

export default BillPaymentConfirmModal;
