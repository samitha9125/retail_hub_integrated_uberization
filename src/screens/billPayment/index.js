import React from 'react';
import BillPaymentMain from '../../views/billPayment';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class BillPaymentScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### BillPaymentScreen :: componentDidMount');
    console.log("In bill payment did mount screen\n" + JSON.stringify(this.props));
    Analytics.logEvent('dsa_bill_payment');
  }

  render() {
    const { navigator } = this.props;
    return (<BillPaymentMain
      navigator={navigator}
      defaultEzNumber={this.props.defaultEzNumber}/>);
  }
}

export default BillPaymentScreen;
