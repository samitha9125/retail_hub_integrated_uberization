import React, { Component } from 'react';
import AccessorySalePage2 from '../../../views/directSale/accessorySales/AccessorySalePage2';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class AccessorySaleScreen2 extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: false
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### AccessorySaleTileScreen :: componentDidMount');
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: true });
      // Analytics.logEvent('dsa_home_page');
    }

    render() {
      console.log("ACCESSORY 2 RENDERED>>>>>>>>>>");
      return (<AccessorySalePage2 {...this.props}/>);
    }
}

export default AccessorySaleScreen2;