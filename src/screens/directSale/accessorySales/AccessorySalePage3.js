import React, { Component } from 'react'
import AccessorySalesDetailsPage from '../../../views/directSale/accessorySales/AccessorySalePage3'
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class AccessorySaleScreen3 extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: false
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### AccessorySaleTileScreen :: componentDidMount');
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('dsa_home_page');
    }

    render() {
        const { navigator } = this.props;
        console.log("ACCESSORY 2 RENDERED>>>>>>>>>>");
        return (<AccessorySalesDetailsPage navigator={navigator} {...this.props} />);
    }
}

export default AccessorySaleScreen3;