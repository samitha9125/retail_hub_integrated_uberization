import React, { Component } from 'react'
import AccessorySales from '../../../views/directSale/accessorySales/AccessorySales';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class AccessorySaleTileSceen extends Component {
    static navigatorStyle = {
        navBarHidden: false, 
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: false
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### AccessorySaleTileScreen :: componentDidMount');
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('dsa_home_page');
    }

    render() {
        console.log("Accessory Sale Rendered");
        return (<AccessorySales {...this.props} />);
    }
}

export default AccessorySaleTileSceen;