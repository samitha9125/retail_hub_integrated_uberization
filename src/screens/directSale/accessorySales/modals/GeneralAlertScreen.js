import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import AccessorySalesGeneralAlert from '../../../../views/directSale/accessorySales/modals/GeneralAlert';
import Colors from '../../../../config/colors';

class AccessorySalesGeneralAlertScreenModal extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
  }

  render() {
    return (<AccessorySalesGeneralAlert {... this.props} />);
  }
}

export default AccessorySalesGeneralAlertScreenModal;
