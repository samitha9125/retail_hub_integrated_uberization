import React, { Component } from 'react';
import { View, Text, PixelRatio } from 'react-native';
import GeneralExceptionSerialModal from '../../../views/directSale/warrentySales/serialSuccessModal';
import Colors from '../../../config/colors';

class SerialSuccessModalScreen extends Component {

    static navigatorStyle = {
        topTabTextColor: '#ffffff',
        selectedTopTabTextColor: '#ffffff',
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true,

        // Icons
        topTabIconColor: '#ffffff',
        selectedTopTabIconColor: '#ffffff',

        // Tab indicator
        selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
        selectedTopTabIndicatorColor: '#ffffff'
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    render() {
        return (
            <GeneralExceptionSerialModal {...this.props} />
        );
    }
}

export default SerialSuccessModalScreen;