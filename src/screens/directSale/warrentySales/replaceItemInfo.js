import React, { Component } from 'react';
import { View, Text, PixelRatio } from 'react-native';
import ReplaceItemInfoView from '../../../views/directSale/warrentySales/replaceItemInfoView';
import Colors from '../../../config/colors';

class WarrentyReplacementItemInfo extends Component {

    static navigatorStyle = {
        topTabTextColor: '#ffffff',
        selectedTopTabTextColor: '#ffffff',
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true,

        // Icons
        topTabIconColor: '#ffffff',
        selectedTopTabIconColor: '#ffffff',

        // Tab indicator
        selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
        selectedTopTabIndicatorColor: '#ffffff'
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    render() {
        return (
            <ReplaceItemInfoView {...this.props} />
        );
    }
}

export default WarrentyReplacementItemInfo;