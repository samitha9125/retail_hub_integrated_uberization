import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import WarrentyReplacementDetailsPage from '../../../views/directSale/warrentySales/CustomerConfirmation';
import Colors from './../../../config/colors';

class WarrentyReplacementDetailScreen extends Component {

    static navigatorStyle = {
        topTabTextColor: '#ffffff',
        selectedTopTabTextColor: '#ffffff',
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true,

        // Icons
        topTabIconColor: '#ffffff',
        selectedTopTabIconColor: '#ffffff',

        // Tab indicator
        selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
        selectedTopTabIndicatorColor: '#ffffff'
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    render() {
        return (
            <WarrentyReplacementDetailsPage {...this.props} />
        );
    }
}

export default WarrentyReplacementDetailScreen;