/*
 * File: startup.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:22:11 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Parthipan Kugadoss (parthipan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { View, StyleSheet, Image, AsyncStorage } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../actions';
import Analytics from '../utills/Analytics';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import launcherImg from '../../images/splash/splash.png';
import Colors from '../config/colors';
import Utills from '../utills/Utills';
// import Global from '../config/globalConfig';
// import MadmeServiceModule, {} from '../service/MadmeService';

const Utill = new Utills();

class StartupScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor,
    orientation: 'portrait'
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false
    };
  }

  async componentWillMount() {
    Orientation.lockToPortrait();
    this.setState({ isLoaded: true });
    try {
      const isFirstTime = await AsyncStorage.getItem('isFirstTime');
      const appKey = await AsyncStorage.getItem('app_key');
      const token = await AsyncStorage.getItem('token');
      const language = await AsyncStorage.getItem('lang');
      const langIndex = await AsyncStorage.getItem('langIndex');
      const allowedServices = await AsyncStorage.getItem('allowedServices');
      const tileData = await AsyncStorage.getItem('tileData');
      const bannerData = await AsyncStorage.getItem('bannerData');

      console.log('isFirstTime: ', isFirstTime);
      console.log('app_key: ', appKey);
      console.log('token: ', token);
      console.log('lang: ', language);
      console.log('langIndex: ', langIndex);
      console.log('allowedServices: ', allowedServices);
      console.log('tileData: ', tileData);

      this.startApp(isFirstTime, token, allowedServices);
      this.props.setAllowedServices(JSON.parse(allowedServices));
      this.props.setTileData(tileData);
      this.props.commonSetBannerImages(JSON.parse(bannerData));

      if (isFirstTime == null || token == null) {
        this.props.setLanguage('en', 0);
        global.lang = 'en';
      } else {
        this.props.setLanguage(language, langIndex);
        global.lang = language;
      }
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    this.getRetailer();
    this.getAgentType();
  }


  async getRetailer() {
    try {
      let retailer = await Utill.getRetailer();
      console.log("Utill.getRetailer: ", retailer); 
      if (retailer){
        console.log("if (retailer == true)");
        this.props.setRetailer(true);
      } else {
        console.log("if (retailer == false)");       
        this.props.setRetailer(false); 
      }
      // console.log("this.props.setRetailer: ", this.props.retailer);
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async getAgentType() {
    try {
      let retailer = await Utill.getAgentType();
      console.log("Utill.getAgentType: ", retailer);
    
      if (retailer){
        console.log("if (is_cfss_agent == true)");
        this.props.setAgentType(true);
      } else {
        console.log("if (is_cfss_agent == false)");       
        this.props.setAgentType(false); 
      }
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async componentDidMount() {
    // MadmeServiceModule.requestPermissionsIfNeeded();
    Analytics.setAnalyticsCollectionEnabled(true);
    Analytics.logEvent('dsa_app_startup');
  }

  startImageUpload() {
    console.log('@@@@@@@@ startImageUpload :: startup');
    const errorCb = (msg) => {
      console.log('xxxx startImageUpload errorCb');
      console.log(msg);
    };
    const successCb = (msg) => {
      console.log('xxxx startImageUpload successCb');
      console.log(msg);
    };
    const flag = 0;
    RNReactNativeImageuploader.startUpload(flag, errorCb, successCb);
  }

  startApp(isFirstTime, token, allowedServices) {
    this.startImageUpload();
    // ApiUtils.startupCheck(token); // TODO:
    if ((isFirstTime == null || token == null) || allowedServices == null) {
      this
        .props
        .navigator
        .resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.LoginScreen' });
    } else {
      this
        .props
        .navigator
        .resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    }
  }

  render() {
    return (
      <View style={styles.viewStyle}>
        <Image source={launcherImg} style={styles.image} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("IN STARTUP MAP_STATE_TO_PROPS: ", JSON.stringify(state));
  return { allowedServices: state.auth.allowedServices };
};

const styles = StyleSheet.create({

  viewStyle: {
    flex: 1,
    backgroundColor: Colors.startUpViewColor
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  }

});

export default connect(mapStateToProps, actions)(StartupScreen);
