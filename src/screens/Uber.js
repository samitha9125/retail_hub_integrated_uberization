import React from 'react';
import Colors from '../config/colors';
import Uberization from 'uberization';
class Uber extends React.Component {

  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  onBackHome = () => {
    this.props.navigator.pop();
  }

  render() {
    return (<Uberization onPressHome={this.onBackHome} />);
  }

}

export default Uber;