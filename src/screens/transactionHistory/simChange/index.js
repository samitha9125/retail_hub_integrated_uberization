import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import SimChangeHistoryMainView from '../../../views/transactionHistory/simChange';

class SimChangeHistoryMainScreen extends React.Component {
  static navigatorStyle = {
    topTabTextColor: Colors.colorWhiteAlpa,
    selectedTopTabTextColor: Colors.colorWhiteAlpa,
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor
  };
  
  componentDidMount() {
    console.log('### SimChangeHistoryMainScreen :: componentDidMount');
    Analytics.logEvent('dsa_sim_change_history');
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }


  render() {
    return (
      <SimChangeHistoryMainView {...this.props}/>
    );
  }
}

export default SimChangeHistoryMainScreen;
