import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import SimChangeDetails from '../../../views/transactionHistory/simChange/SimChangeDetails';

class SimChangeDetailsScreen extends React.Component {
    static navigatorStyle = {
      topTabTextColor: Colors.colorWhiteAlpa,
      selectedTopTabTextColor: Colors.colorWhiteAlpa,
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor
    };

    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### SimChangeDetailsScreen :: componentDidMount');
      Analytics.logEvent('dsa_sim_change_history_details');
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }

    componentWillUnmount() {
      Orientation.lockToPortrait();
    }

    render() {
      return (<SimChangeDetails { ...this.props} />);
    }
}

export default SimChangeDetailsScreen;