import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import FilterSimChangeHistoryView from '../../../views/transactionHistory/billPayment/FilterBillPaymentHistoryView';

class FilterBillPaymentHistoryScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }
  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### Filter Bill Payment History :: componentDidMount');
    Analytics.logEvent('dsa_mobile_filter_bill_payment_history');
  }

  render() {
    return (<FilterSimChangeHistoryView {... this.props}/>);
  }
}

export default FilterBillPaymentHistoryScreen;
