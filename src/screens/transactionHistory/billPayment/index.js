/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 19th June 2018 6:33:14 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 20th June 2018 4:32:08 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import BillPaymentHistoryMainView from '../../../views/transactionHistory/billPayment';

class BillPaymentHistoryMainScreen extends React.Component {
  static navigatorStyle = {
    topTabTextColor: Colors.colorWhiteAlpa,
    selectedTopTabTextColor: Colors.colorWhiteAlpa,
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor
  };

  constructor(props) {
    super(props);
  }
  
  componentDidMount() {
    console.log('### BillPaymentHistoryMainScreen :: componentDidMount');
    Analytics.logEvent('dsa_bill_payment_history');
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <BillPaymentHistoryMainView {...this.props}/>
    );
  }
}

export default BillPaymentHistoryMainScreen;
