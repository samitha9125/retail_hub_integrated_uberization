import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import ActivationStatusDetails from '../../../views/transactionHistory/activationStatus/ActivationStatusDetails';

class ActivationStatusDetailsScreen extends React.Component {
    static navigatorStyle = {
      topTabTextColor: Colors.colorWhiteAlpa,
      selectedTopTabTextColor: Colors.colorWhiteAlpa,
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor
    };

    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### ActivationStatusDetailsScreen :: componentDidMount');
      Analytics.logEvent('dsa_activation_status_details');
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }

    componentWillUnmount() {
      Orientation.lockToPortrait();
    }

    render() {
      return (<ActivationStatusDetails { ...this.props} />);
    }
}

export default ActivationStatusDetailsScreen;