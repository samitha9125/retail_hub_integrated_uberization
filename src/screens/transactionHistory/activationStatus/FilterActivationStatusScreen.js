import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import FilterActivationStatusView from '../../../views/transactionHistory/activationStatus/FilterActivationStatusView';

class FilterActivationStatusScreen extends React.Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }
  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### FilterActivationStatusScreen :: componentDidMount');
    Analytics.logEvent('dsa_activation_status_filter');
  }

  render() {
    return (<FilterActivationStatusView {... this.props}/>);
  }
}

export default FilterActivationStatusScreen;
