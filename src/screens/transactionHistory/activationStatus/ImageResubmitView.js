import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import ImageResubmitView from '../../../views/transactionHistory/activationStatus/ImageResubmitView';

class ImageResubmitScreen extends React.Component {
    static navigatorStyle = {
      topTabTextColor: Colors.colorWhiteAlpa,
      selectedTopTabTextColor: Colors.colorWhiteAlpa,
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor
    };

    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### ImageResubmitScreen :: componentDidMount');
      Analytics.logEvent('dsa_activation_status_resubmit_image');
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }

    componentWillUnmount() {
      Orientation.lockToPortrait();
    }

    render() {
      return (<ImageResubmitView { ...this.props} />);
    }
}

export default ImageResubmitScreen;