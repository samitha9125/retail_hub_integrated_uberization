import React from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import ActivationStatusMainView from '../../../views/transactionHistory/activationStatus';

class ActivationStatusMainScreen extends React.Component {
  static navigatorStyle = {
    topTabTextColor: Colors.colorWhiteAlpa,
    selectedTopTabTextColor: Colors.colorWhiteAlpa,
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### ActivationStatusMainScreen :: componentDidMount');
    Analytics.logEvent('dsa_activation_status');
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (<ActivationStatusMainView {...this.props}/>);
  }
}

export default ActivationStatusMainScreen;
