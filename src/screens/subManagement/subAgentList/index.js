import React from 'react';
import Orientation from 'react-native-orientation';
import AgentList from '../../../views/myAccount/SubAgentList/index';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class SubAgentList extends React.Component {
  static navigatorStyle = {
    navBarHidden: false,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }
  
  componentDidMount() {
    console.log('### SubAgentList :: componentDidMount');
    Analytics.logEvent('dsa_SubAgentList_change');
  }

  render() {
    return (<AgentList {...this.props}/>);
  }
}

export default SubAgentList;
