/*
 * @export
 * @class  OTPModal
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This is a Common OTP module component.
 *
 */
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import Colors from '../../../../config/colors';
import { TextField } from 'react-native-material-textfield';
import Button from '@Components/others/Button';
import Utills from '../../../../utills/Utills';
import strings from '../../../../Language/settings';

const Utill = new Utills();
class OTPModalScreenAgent extends React.Component {

  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  subscription;
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      locals: {
        OTP_text: strings.pinSentDesc,
        Cancel: strings.cancel,
        Confirm: strings.confirm,
        lblResendPIN: strings.resendPin,
        lblDintReceivePIN: strings.didntReceivePin,
        lblEnterPIN: strings.enterPin
      },

      pinNumber : ''

    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  onCancel = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  pinTextChange = (pin) => {
    this.setState({ pinNumber: pin });
  }

  getButtonEnableStatus = (pin) => { 
    return pin !=='' && pin.length == 4 && Utill.validateNumberFormat(pin);
  }


  render() {
    const {
      descriptionTextStyle = {},
      primaryButtonStyle = {},
      secondaryButtonStyle = {}
    } = this.props;

    console.log('this.props.pin', this.props.pin);
    return (
      <View style={styles.modalStyle}>
        <View style={[styles.scrollViewContainer]}>
          <View style={styles.descriptionContainer}>
            <Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{this.state.locals.OTP_text}{this.props.mobile_number}</Text>
          </View>
          <View style={styles.textInputContainer}>
            <View style={styles.textFieldTitle}>
              <Text textAlign="center" style={styles.textFieldTitleStyle}>{this.state.locals.lblEnterPIN}</Text>
            </View>
            <TextField
              label=''
              value={this.props.pin}
              maxLength={4}
              keyboardType={"numeric"}
              onChangeText={(text) => { 
                this.props.onTextChange(text); 
                //this.pinTextChange(text);
              }}
            />
            <View style={styles.getInStart}>
              <View style={styles.getInStartTextContainer}>
                <Text style={styles.getInStartText}>{this.state.locals.lblDintReceivePIN}</Text>
              </View>
              <TouchableOpacity
                onPress={this.props.resendOtp}
                style={styles.getInStartTextButtonContainer}>
                <Text
                  style={styles.getInStartTextButton}
                > {this.state.locals.lblResendPIN} </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <Button children={this.state.locals.Cancel}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={this.onCancel}
            />
            <Button children={this.state.locals.Confirm}
              //childStyle={[styles.buttonStyle, Utill.getButtonTextColor(this.getButtonEnableStatus(this.state.pinNumber))]}
              //disabled={!this.getButtonEnableStatus(this.state.pinNumber)}
              childStyle={[styles.buttonStyle, primaryButtonStyle]}
              ContainerStyle={styles.rightButtonContainerStyle}
              onPress={this.props.onBnPressYes}
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: subManagement => OTPModal ', state);
  const language = state.lang.current_lang;
  return { language };
};


const styles = StyleSheet.create({
  modalStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
    padding: 0
  },
  scrollViewContainer: {
    height: 250,
    backgroundColor: Colors.colorWhite,
    width: '90%',
  },
  descriptionContainer: {
    flex: 0.3,
    justifyContent: 'center',
    marginRight: 20,
    marginLeft: 20,
    marginTop: 10,
    // backgroundColor: 'red'
  },
  textInputContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 5,
    marginLeft: 20,
    marginRight: 20,
    // backgroundColor: 'green'
  },
  textFieldTitle: {
    justifyContent: 'center',
    marginRight: 20,
    marginTop: 5,
    marginBottom: -14,
  },
  buttonContainer: {
    flex: 0.3,
    flexDirection: 'row',
    paddingBottom: 15,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    // backgroundColor: 'blue',
  },
  descriptionTextStyle: {
    fontSize: 20,
    color: Colors.black
  },
  textFieldTitleStyle: {
    fontSize: 16,
    color: Colors.black,
    fontWeight: '500'
  },

  leftButtonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle: {
    paddingRight: 10,
    padding: 5
  },

  buttonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
  getInStart: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 20,
    // backgroundColor: 'blue'
  },
  getInStartTextContainer: {
    flex: 0.6
  },
  getInStartTextButtonContainer: {
    flex: 0.4
  },
  getInStartText: {
    fontSize: 16,
    marginRight: 8,
    color: Colors.colorLightBlack
  },
  getInStartTextButton: {
    fontSize: 16,
    alignSelf: 'flex-end',
    fontWeight: '500',
    color: Colors.gettingStartedButtonTextColor
  }
});

export default connect(mapStateToProps, actions)(OTPModalScreenAgent);
