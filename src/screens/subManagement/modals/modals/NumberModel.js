/*
 * @export
 * @class  NumberModel
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This is the component used to add new agent on 
 * subagent module this serves as a number adding module.
 *
 */
import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import Colors from '../../../../config/colors';
import { TextField } from 'react-native-material-textfield';
import Button from '@Components/others/Button';
import strings from '../../../../Language/settings';
import Utills from '../../../../utills/Utills';
const { width, height } = Dimensions.get('window');
const Utill = new Utills();
class NumberModel extends React.Component {

    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: true,
      statusBarColor: Colors.statusBarColor
    };

    constructor(props) {
      super(props);
      strings.setLanguage(this.props.language);
      this.state = {
        sendPin: strings.sendpin,
        AgentMob: strings.AgentsMobNumber,
        number: '',
        invalidNum: strings.invalidNum,
        nickName: strings.nickName,
        name: '',
        invalidName: strings.invalidName,
        locals: {    
          cancel: strings.cancel, 
        },
      };
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }
  
    componentWillUnmount() {
      Orientation.lockToPortrait();
    }

    onTextChangeNumber(text) {
      this.setState({ number: text });

    }

    onTextChangeName(text) {
      this.setState({ name: text });
    }

    onCancel = () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    }

    getButtonEnableStatus = () => {
      return Utill.mobileNumValidate(this.state.number) 
      && Utill.nameValidation(this.state.name) 
      && this.state.number !== '' 
      && this.state.name !== '';
    }


    render() {
      const {
        primaryButtonStyle = {},
        secondaryButtonStyle = {}
      } = this.props;

      return (
        <View style={styles.modalStyle}>
          <View style={[styles.scrollViewContainer]}>
            <View style={styles.textInputContainer}>
              <View style={styles.textFieldTitle}>
                <Text style={styles.textFieldTitleStyle}>{this.state.AgentMob}</Text>
              </View>
              <TextField
                //tintColor={Colors.btnActive}
                label=''
                style={styles.textInputFieldStyle}
                value={this.state.number}
                maxLength={Utill.getMobileNumberLengthLimit(this.state.number)}
                keyboardType={"numeric"}
                onChangeText={text => this.onTextChangeNumber(text)}
                error={(this.state.number == '' || Utill.mobileNumValidate(this.state.number)) ? "" : this.state.invalidNum}

              />
              <View style={styles.textFieldTitle}>
                <Text style={styles.textFieldTitleStyle}>{this.state.nickName}</Text>
              </View>
              <TextField
                //tintColor={Colors.btnActive}
                label=''
                style={styles.textInputFieldStyle}
                value={this.state.name}
                maxLength={50}
                onChangeText={text => this.onTextChangeName(text)}
                error={(this.state.name == '' || Utill.alphaNumericValidationWithSpace(this.state.name)) ? "" : this.state.invalidName}

              />
            </View>
            <View style={styles.buttonContainer}>
              <Button children={this.state.locals.cancel}
                childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
                ContainerStyle={styles.leftButtonContainerStyle}
                onPress={this.onCancel}
              />
              <Button children={this.state.sendPin}
                childStyle={[styles.buttonStyle, Utill.getButtonTextColor(this.getButtonEnableStatus())]}
                disabled={!this.getButtonEnableStatus()}
                ContainerStyle={styles.rightButtonContainerStyle}
                onPress={() => this.props.onSendpinPress(this.state.number, this.state.name)}
              />
            </View>
          </View>
        </View>
      );
    }
}


const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: subManagement => NumberModel ', state);
  const language = state.lang.current_lang;
  return { language };
};


const styles = StyleSheet.create({
  modalStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
    padding: 0
  },
  scrollViewContainer: {
    height: 260,
    width: width * 0.9,
    backgroundColor: Colors.colorWhite,
   
  },
  textInputContainer: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingTop: 10,
    marginTop: 10
  },
  textFieldTitle: {
    marginRight: 20,
    marginTop: 5,
    marginBottom: -14,
  },

  textInputFieldStyle: {
  },

  buttonContainer: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingBottom: 15,
    paddingRight: 20, 
    // backgroundColor: 'blue',
  },

  textFieldTitleStyle: {
    fontSize: 16,
    color: Colors.black,
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  leftButtonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
  rightButtonContainerStyle: {
    paddingRight: 10,
    padding: 5
  },
  buttonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
});

export default connect(mapStateToProps, actions)(NumberModel);
