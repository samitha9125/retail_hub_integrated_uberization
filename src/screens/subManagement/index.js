/*
 * @export
 * @class  subManagementContainer
 * @extends {React.Component}
 * Copyright (C) Dialog axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * The Root index for Subagent module
 *
 */
import React from 'react';
import Orientation from 'react-native-orientation';
import MyAccount from '../../views/myAccount';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';

class subManagementContainer extends React.Component {
  static navigatorStyle = {
    navBarHidden: false,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);

  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### MyAccount_Container :: componentDidMount');
    Analytics.logEvent('dsa_myAccount_page');
  }

  render() {
    return (<MyAccount {...this.props}/>);
  }
}

export default subManagementContainer;
