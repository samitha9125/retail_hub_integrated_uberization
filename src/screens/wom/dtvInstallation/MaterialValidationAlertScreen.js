import React, { Component } from 'react';
import MaterialValidationAlert from '../../../views/wom/components/MaterialValidationAlert';
import Colors from '../../../config/colors';

class MaterialValidationAlertScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Material_Validation_Alert');
    }

    render() {
        return (<MaterialValidationAlert {...this.props} />);
    }
}

export default MaterialValidationAlertScreen;