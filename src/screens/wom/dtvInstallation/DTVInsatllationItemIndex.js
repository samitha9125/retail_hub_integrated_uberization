import React, { Component } from 'react';

import DTVInstallationItem from '../../../views/wom/dtvInstallation/DTVInstallationItemIndex'

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class DTVInstallationItemScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }
    componentDidMount() {
      console.log('### SimChangeScreen :: componentDidMount');
      // Analytics.logEvent('DTV_Installation_Item');
    }

    render() {
      // const { navigator } = this.props;

      return (<DTVInstallationItem {...this.props} />);
    }
}

export default DTVInstallationItemScreen;