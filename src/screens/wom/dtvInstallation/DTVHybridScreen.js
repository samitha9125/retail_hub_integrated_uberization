import React, { Component } from 'react';
import DTVHybridView from '../../../views/wom/dtvInstallation/DTVHybridView';
import Colors from '../../../config/colors';

class DTVHybridScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: false
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('DTV_Hybrid_View');
    }

    render() {
        return (<DTVHybridView {...this.props} />);
    }
}

export default DTVHybridScreen;