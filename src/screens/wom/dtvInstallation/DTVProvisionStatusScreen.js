import React, { Component } from 'react';
import DTVProvisionStatus from '../../../views/wom/dtvInstallation/DTVProvisionStatus';
import Colors from '../../../config/colors';

class DTVProvisionStatusScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('DTV_Provision_Status');
    }

    render() {
        return (<DTVProvisionStatus {...this.props} />);
    }
}

export default DTVProvisionStatusScreen;