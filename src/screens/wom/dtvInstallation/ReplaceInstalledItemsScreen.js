import React, { Component } from 'react';
import ReplaceInstalledItems from '../../../views/wom/dtvInstallation/ReplaceInstalledItems';
import Colors from '../../../config/colors';

class ReplaceInstalledItemsScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Replace_Installed_Items');
  }

  render() {
    console.log("CpeTracker RENDERED>>>>>>>>>>");
    return (<ReplaceInstalledItems {...this.props} />);
  }
}

export default ReplaceInstalledItemsScreen;