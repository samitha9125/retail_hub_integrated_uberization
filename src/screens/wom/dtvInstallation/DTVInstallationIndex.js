import React, { Component } from 'react';

import DTVInstallation from '../../../views/wom/dtvInstallation/DTVInstallationIndex'

import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class DTVInstallationScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }
    componentDidMount() {
      console.log('### SimChangeScreen :: componentDidMount');
      // Analytics.logEvent('DTV_Installation');
    }

    render() {
      // const { navigator } = this.props;

      return (<DTVInstallation {...this.props} />);
    }
}

export default DTVInstallationScreen;