import React, { Component } from 'react';
import WomLandingView from '../../views/wom/WomLandingView';
import { View, Text } from 'react-native';
import Colors from '../../config/colors';

class WomLandingScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: false,
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('dsa_home_page');
  }

  render() {
    return (<WomLandingView {...this.props} />);
  }
}

export default WomLandingScreen;