import React, { Component } from 'react';
import DetailDisplayView from '../../../views/wom/general/DetailDisplayView';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class DetailDisplayScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        console.log('### AccessorySaleTileScreen :: componentDidMount');
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Detail_Display_View');
    }

    render() {
        console.log("ACCESSORY 2 RENDERED>>>>>>>>>>");
        return (<DetailDisplayView {...this.props} />);
    }
}

export default DetailDisplayScreen;