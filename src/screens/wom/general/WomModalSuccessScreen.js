import React, { Component } from 'react';
import WomModalSuccess from '../../../views/wom/general/WomModalSuccess';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class WomModalSuccessScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    // Analytics.logEvent('Wom_Modal_Success');
  }

  render() {
    return (<WomModalSuccess {...this.props} />);
  }
}

export default WomModalSuccessScreen;