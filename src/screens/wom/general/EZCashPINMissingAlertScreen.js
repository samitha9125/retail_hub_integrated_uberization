import React, { Component } from 'react';
import EZCashPINMissingAlert from '../../../views/wom/general/EZCashPINMissingAlert';
import Colors from '../../../config/colors';

class EZCashPINMissingAlertScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('EZCash_PIN_Missing_Alert');
  }

  render() {
    console.log("CpeTracker RENDERED>>>>>>>>>>");
    return (<EZCashPINMissingAlert {...this.props} />);
  }
}

export default EZCashPINMissingAlertScreen;