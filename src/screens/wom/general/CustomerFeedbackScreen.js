import React, { Component } from 'react';
import CustomerFeedback from './../../../views/wom/general/CustomerFeedback';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

class CustomerFeedbackScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Customer_Feedback');
    }

    render() {
        return (<CustomerFeedback {...this.props} />);
    }
}

export default CustomerFeedbackScreen;