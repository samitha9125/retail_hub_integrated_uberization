import React, { Component } from 'react';
import Colors from '../../../config/colors';
import CustomerConfirmationWOM from '../../../views/wom/general/CustomerConfirmation';
class WOMCustomerConfirmationScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: false
    };

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Customer_Confirmation_WOM');
    }

    render() {
        return (
            <CustomerConfirmationWOM {...this.props} />
        );
    }
}

export default WOMCustomerConfirmationScreen;