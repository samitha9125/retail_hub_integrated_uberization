import React, { Component } from 'react';
import Colors from '../../../config/colors';
import WOMBarcodeScannerView from '../../../views/wom/general/WOMBarcodeScanner';

class WOMBarcodeScannerScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    //Analytics.logEvent('WOM_Barcode_Scanner_View');
  }

  render() {
    return (<WOMBarcodeScannerView {...this.props} />);
  }
}

export default WOMBarcodeScannerScreen;