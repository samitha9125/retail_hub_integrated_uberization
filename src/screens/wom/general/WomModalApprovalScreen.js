import React, { Component } from 'react';
import WomModalApproval from '../../../views/wom/general/WomModalApproval';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class WomModalApprovalScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    // Analytics.logEvent('Wom_Modal_Approval');
  }

  render() {
    return (<WomModalApproval {...this.props} />);
  }
}

export default WomModalApprovalScreen;