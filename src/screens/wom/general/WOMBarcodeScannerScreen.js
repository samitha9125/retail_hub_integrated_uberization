import React, { Component } from 'react';
import BarcodeScanner from '../../../views/wom/components/BarcodeScanner';
import Colors from '../../../config/colors';

class WOMBarcodeScannerScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Barcode_Scanner');
  }

  render() {
    // const { navigator } = this.props;
    console.log("CpeTracker RENDERED>>>>>>>>>>");
    return (<BarcodeScanner {...this.props} />);
  }
}

export default WOMBarcodeScannerScreen;