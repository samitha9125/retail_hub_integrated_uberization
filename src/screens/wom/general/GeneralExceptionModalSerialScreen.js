import React, { Component } from 'react';
import GeneralExceptionSerialModal from '../../../views/wom/general/GeneralModalExceptionSerial';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class GeneralExceptionModalScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    console.log('### GeneralExceptionSerialModal :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    // Analytics.logEvent('dsa_genaral_exception_modal');
  }

  render() {
    return (<GeneralExceptionSerialModal {...this.props}/>);
  }
}

export default GeneralExceptionModalScreen;