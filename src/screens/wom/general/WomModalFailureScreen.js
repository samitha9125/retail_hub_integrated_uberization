import React, { Component } from 'react';
import WomModalFailure from '../../../views/wom/general/WomModalFailure';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';

class WomModalFailureScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: true,
    statusBarColor: Colors.statusBarColor
  };

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: false });
    // Analytics.logEvent('Wom_Modal_Failure');
  }

  render() {
    return (<WomModalFailure {...this.props} />);
  }
}

export default WomModalFailureScreen;