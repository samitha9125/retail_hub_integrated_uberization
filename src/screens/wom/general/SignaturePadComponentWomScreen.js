import React, { Component } from 'react';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import Orientation from 'react-native-orientation';
import SignaturePadComponentWOM from '../../../views/wom/general/SignaturePadComponent';

class SignaturePadComponentWomScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true,
    orientation: 'landscape'
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### SignaturePadScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({
      side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
      enabled: false // should the drawer be enabled or disabled (locked closed)
    });

    const orientationMode = Orientation.getInitialOrientation();
    console.log("Component Did Mount. Orientation Mode: ", orientationMode);

    if (orientationMode === 'PORTRAIT') {
      console.log("App is on Portrait mode. Switching to LANDSCAPE");
      // Orientation.unlockAllOrientations();
      Orientation.lockToLandscape();
    } else {
      console.log("App is on LANDSCAPE mode.");
    }

    Analytics.logEvent('Signature_Pad_Component_WOM');
  }

  componentWillUnmount() {
    // const orientationMode = Orientation.getInitialOrientation(); console.log(".
    // Orientation Mode: ",orientationMode); if (orientationMode === 'LANDSCAPE') {
    console.log("Component Unmounting. Switching to Portrait");
    // Orientation.unlockAllOrientations();
    Orientation.lockToPortrait();
    // } else {     console.log("App is on PORTRAIT mode."); }
  }

  render() {
    return (<SignaturePadComponentWOM {... this.props}/>);
  }
}

export default SignaturePadComponentWomScreen;
