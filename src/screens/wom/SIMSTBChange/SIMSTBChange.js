import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import SIMSTBChangeView from '../../../views/wom/SIMSTBChangeViews/SIMSTBChange';
class SIMSTBChangeScreen extends Component {
  static navigatorStyle = {
    navBarHidden: false,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### Reject Screen :: componentDidMount');
    // Analytics.logEvent('SIM_STB_Change_View');
  }

  render() {
    // return (<StockManagement {...this.props}/>);
    return ( <SIMSTBChangeView {...this.props} />);
  }
}

export default SIMSTBChangeScreen;