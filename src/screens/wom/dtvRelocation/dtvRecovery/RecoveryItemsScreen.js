import React, { Component } from 'react';
import Colors from '../../../../config/colors';
import RecoveryItemsView from '../../../../views/wom/dtvRelocation/dtvRecovery/RecoveryItemsView';

class RecoveryItemsScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Recovery_Items_View');
  }

  render() {
    return (<RecoveryItemsView {...this.props} />);
  }
}

export default RecoveryItemsScreen;