import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Colors from '../../../../config/colors';
import RefixItemsView from '../../../../views/wom/dtvRelocation/dtvRefix/RefixItemsView';

class RefixItemsScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Refix_Items_View');
  }

  render() {
    return (<RefixItemsView {...this.props} />);
  }
}

export default RefixItemsScreen;