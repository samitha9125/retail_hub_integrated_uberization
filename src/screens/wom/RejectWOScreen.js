import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';
import Analytics from '../../utills/Analytics';
import { View } from 'react-native';
import RejectIndex from '../../views/wom/RejectIndex';

class RejectWOScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### Reject Screen :: componentDidMount');
    Analytics.logEvent('dsa_mobile_Wom_app');
  }

  render() {
    // return (<StockManagement {...this.props}/>);
    return (<View>
      
      <RejectIndex {...this.props} />
    </View>);
  }
}

export default RejectWOScreen;