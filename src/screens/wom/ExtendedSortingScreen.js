import React, { Component } from 'react';
import ExtendedSortingView from '../../views/wom/ExtendedSortingView';
import Colors from '../../config/colors';

class ExtendedSortingScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### AccessorySaleTileScreen :: componentDidMount');
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: true });
      // Analytics.logEvent('dsa_home_page');
    }

    render() {
      console.log("ACCESSORY 2 RENDERED>>>>>>>>>>");
      return (<ExtendedSortingView {...this.props}/>);
    }
}

export default ExtendedSortingScreen;