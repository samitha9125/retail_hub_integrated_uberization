import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';
import StbDeliveryCustomerOwned from '../../../views/wom/dtvStbUpgrade/stbDeliveryCustomerOwned';

class stbDeliveryCustomerOwnedScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Stb_Delivery_Customer_Owned');
  }

  render() {
    return (<StbDeliveryCustomerOwned {...this.props} />);
  }
}
export default stbDeliveryCustomerOwnedScreen;