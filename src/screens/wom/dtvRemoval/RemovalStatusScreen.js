import React, { Component } from 'react';
import RemovalStatusView from '../../../views/wom/dtvRemoval/RemovalStatusView';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';

export default class RemovalStatusScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    console.log('### RemovalStatusScreen :: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Removal_Status_View');
  }

  render() {
    console.log("RemovalStatusScreen RENDERED>>>>>>>>>>");
    return (<RemovalStatusView {...this.props} />);
  }
}
