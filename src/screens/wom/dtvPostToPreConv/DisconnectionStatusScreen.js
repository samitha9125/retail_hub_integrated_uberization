import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';
import DisconnectionStatusView from '../../../views/wom/dtvPostToPreConv/DisconnectionStatusView';

class DisconnectionStatusScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Disconnection_Status_View');
  }

  render() {
    return (<DisconnectionStatusView {...this.props} />);
  }
}

export default DisconnectionStatusScreen;