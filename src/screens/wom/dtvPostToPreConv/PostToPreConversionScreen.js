import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';
import PostToPreConversionView from '../../../views/wom/dtvPostToPreConv/PostToPreConversionView';

class PostToPreConversionScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentDidMount() {
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Post_To_Pre_Conversion_View');
  }

  render() {
    return (<PostToPreConversionView {...this.props} />);
  }
}

export default PostToPreConversionScreen;