import React, { Component } from 'react';
import WomDetailView from '../../views/wom/WomDetailView';
import Colors from '../../config/colors';

class WomDetailScreen extends Component {
    static navigatorStyle = {
      navBarHidden: true, 
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true
    };
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: true
      };
    }

    componentDidMount() {
      console.log('### AccessorySaleTileScreen :: componentDidMount');
      const { navigator } = this.props;
      navigator.setDrawerEnabled({ side: 'left', enabled: true });
      // Analytics.logEvent('dsa_home_page');
    }

    render() {
      console.log("ACCESSORY 2 RENDERED>>>>>>>>>>");
      return (<WomDetailView {...this.props}/>);
    }
}

export default WomDetailScreen;