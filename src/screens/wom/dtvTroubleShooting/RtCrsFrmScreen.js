import React, { Component } from 'react';
import Colors from '../../../config/colors';
import RootCauseForm from '../../../views/wom/dtvTroubleShooting/RootCauseForm';

class RtCrsFrmScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Root_Cause_Form');
    }

    render() {
        return (<RootCauseForm {...this.props} />);
    }
}

export default RtCrsFrmScreen;