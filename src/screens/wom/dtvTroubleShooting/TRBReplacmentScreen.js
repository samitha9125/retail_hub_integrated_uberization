import React, { Component } from 'react';
import Colors from '../../../config/colors';
import TRBReplacmentView from '../../../views/wom/dtvTroubleShooting/TRBReplacmentView';

class TRBReplacmentScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('TRB_Replacment_View');
    }

    render() {
        return (<TRBReplacmentView {...this.props} />);
    }
}

export default TRBReplacmentScreen;