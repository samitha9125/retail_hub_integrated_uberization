import React, { Component } from 'react';
import ReplaceConfirmAlert from '../../../views/wom/dtvTroubleShooting/WarrantyConfirmAlert';
import Colors from '../../../config/colors';

class WarrantyConfirmAlertScreen extends Component {
  static navigatorStyle = {
    navBarHidden: true,
    drawUnderNavBar: false,
    statusBarColor: Colors.statusBarColor,
    navBarTextColor: Colors.navBarTextColor,
    navBarButtonColor: Colors.navBarButtonColor,
    navBarBackgroundColor: Colors.navBarBackgroundColor,
    navBarComponentAlignment: 'center',
    navBarTitleTextCentered: true
  };

  componentDidMount() {
    console.log('### CpeTracker:: componentDidMount');
    const { navigator } = this.props;
    navigator.setDrawerEnabled({ side: 'left', enabled: true });
    // Analytics.logEvent('Replace_Confirm_Alert');
  }

  render() {
    console.log("CpeTracker RENDERED>>>>>>>>>>");
    return (<ReplaceConfirmAlert {...this.props} />);
  }
}

export default WarrantyConfirmAlertScreen;