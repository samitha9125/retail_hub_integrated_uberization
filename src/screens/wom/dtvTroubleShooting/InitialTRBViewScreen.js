import React, { Component } from 'react';
import Colors from '../../../config/colors';
import InitialTRBView from '../../../views/wom/dtvTroubleShooting/InitialTRBView';

class InitialTRBViewScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Initial_TRB_View');
    }

    render() {
        return (<InitialTRBView {...this.props} />);
    }
}

export default InitialTRBViewScreen;