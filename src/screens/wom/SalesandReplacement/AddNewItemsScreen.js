

/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/


import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Colors from '../../../config/colors';
import AddNewItems from '../../../views/wom/SalesandReplacement/AddNewItems';

class AddNewItemsScreen extends Component {
    static navigatorStyle = {
        navBarHidden: false,
        drawUnderNavBar: false,
        statusBarColor: Colors.statusBarColor,
        navBarTextColor: Colors.navBarTextColor,
        navBarButtonColor: Colors.navBarButtonColor,
        navBarBackgroundColor: Colors.navBarBackgroundColor,
        navBarComponentAlignment: 'center',
        navBarTitleTextCentered: true
    };
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: true
        };
    }

    componentDidMount() {
        const { navigator } = this.props;
        navigator.setDrawerEnabled({ side: 'left', enabled: true });
        // Analytics.logEvent('Accessory_Sale_Add_New_Items');
    }

    render() {
        return (<AddNewItems {...this.props} />);
    }
}

export default AddNewItemsScreen;