export default {
  success : true,
  message_type : 'NA',
  sachet_list : [
    {
      offer_key: 805001,
      offer_val: {
        CODE: 97,
        RANK: null,
        OFFER_TYPE: null,
        CHARGE: 970000,
        CHARGE_FMTED: '970,000.00',
        CHARGE_TAX: 970000,
        CHARGE__TAX_FMTED: '970,000.00',
        RESOURCES: [
          {
            LABEL: 'DATA_DAY',
            VALUE: '0.8 GB',
            TYPE: ''
          }, {
            LABEL: 'DATA_NIGHT',
            VALUE: '0.8 GB',
            TYPE: ''
          }
        ],
        DESC: 'Rs 97'
      }
    }, {
      offer_key: 805002,
      offer_val: {
        CODE: 197,
        RANK: null,
        OFFER_TYPE: null,
        CHARGE: 1970000,
        CHARGE_FMTED: '1,970,000.00',
        CHARGE_TAX: 1970000,
        CHARGE__TAX_FMTED: '1,970,000.00',
        RESOURCES: [
          {
            LABEL: 'DATA_DAY',
            VALUE: '1.8 GB',
            TYPE: ''
          }, {
            LABEL: 'DATA_NIGHT',
            VALUE: '1.8 GB',
            TYPE: ''
          }
        ],
        DESC: 'Rs 197'
      }
    }, {
      offer_key: 805003,
      offer_val: {
        CODE: 397,
        RANK: null,
        OFFER_TYPE: null,
        CHARGE: 3970000,
        CHARGE_FMTED: '3,970,000.00',
        CHARGE_TAX: 3970000,
        CHARGE__TAX_FMTED: '3,970,000.00',
        RESOURCES: [
          {
            LABEL: 'DATA_DAY',
            VALUE: '4 GB',
            TYPE: ''
          }, {
            LABEL: 'DATA_NIGHT',
            VALUE: '4 GB',
            TYPE: ''
          }
        ],
        DESC: 'Rs 397'
      }
    }, {
      offer_key: 805004,
      offer_val: {
        CODE: 997,
        RANK: null,
        OFFER_TYPE: null,
        CHARGE: 9970000,
        CHARGE_FMTED: '9,970,000.00',
        CHARGE_TAX: 9970000,
        CHARGE__TAX_FMTED: '9,970,000.00',
        RESOURCES: [
          {
            LABEL: 'DATA_DAY',
            VALUE: '14.3 GB',
            TYPE: ''
          }, {
            LABEL: 'DATA_NIGHT',
            VALUE: '14.3 GB',
            TYPE: ''
          }
        ],
        DESC: 'Rs 997'
      }
    }
  ]
};