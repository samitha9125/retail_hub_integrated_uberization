import React from 'react';
import { StyleSheet, View, Keyboard, PermissionsAndroid, Alert } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { AccountUtils, FuncUtils, LocalStorageUtils } from '../../utills';
import { Colors } from '../../config/';
import ActIndicator from './ActIndicator';
import AdBanner from './AdBanner';
import TileView from './TileView';
import Utills from '../../utills/Utills';
import strings from '../../Language/settings';
import Global from '../../config/globalConfig';

const Utill = new Utills();

async function requestAppPermission() {
  console.log("************** Request Camera Permission 2");
  try {
    console.log("************** Request Camera Permission 3 0");
    var granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
    console.log("************** Request Camera Permission 3 1");

    if (!granted) {
      console.log("************** Camera Permission Not Granted 4 0");
      granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("************** You can use the camera 5 0");
      } else {
        console.log("************** Camera permission denied 5 1");
      }
    } else {
      console.log("************** Camera permission is already granted 5 2");
    }

    granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    console.log("************** Request WRITE_EXTERNAL_STORAGE Permission 1");

    if (!granted) {
      console.log("************** WRITE_EXTERNAL_STORAGE Permission Not Granted 2");
      granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("************** You can use the WRITE_EXTERNAL_STORAGE 3");
      } else {
        console.log("************** WRITE_EXTERNAL_STORAGE permission denied 4");
      }
    } else {
      console.log("************** WRITE_EXTERNAL_STORAGE permission is already granted 5");
    }

  } catch (err) {
    console.log("************** Request Permission error");
    console.log(err);
  }
  checkLocationPermissionForCFSS();
}

async function checkLocationPermissionForCFSS() {
  console.log('checkLocationPermissionForCFSS1 ');
  try {
    let isCFSSAgent = await AccountUtils.isCFSSAgent();
    console.log('checkLocationPermissionForCFSS2 ', isCFSSAgent);
    if (isCFSSAgent) {
      console.log('checkLocationPermissionForCFSS3 ', isCFSSAgent);
      var granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      console.log("************** Request ACCESS_FINE_LOCATION Permission 3 1");

      if (!granted) {
        console.log("************** ACCESS_FINE_LOCATION Permission Not Granted");
        granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("************** You can use the ACCESS_FINE_LOCATION");
        } else {
          showPermissionRequestAlert();
          console.log("************** ACCESS_FINE_LOCATION permission denied");
        }
      } else {
        console.log("************** ACCESS_FINE_LOCATION permission is already granted");
      }
    }
  } catch (error) {
    showPermissionRequestAlert();
    console.log('checkLocationPermissionForCFSS error ', error);
  }
}


function showPermissionRequestAlert() {
  Alert.alert('', 'Please grant location permission to proceed.', [
    {
      text: 'OK',
      onPress: () => {
        this.timeout = setTimeout(() => {
          checkLocationPermissionForCFSS();
        }, Global.locationPopUpInterval)
      }
    }
  ], { cancelable: false });
}

class HomeMain extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: false,
      userData: '',
      isLoading: false,
      availableNumbers: {
        "ezCash":
          [],
        "rapidEz":
          [],
        "selectedIndexes": {
          "ezCash": '',
          "rapidEz": ''
        }
      },
      locals: {
        loginButtnText: 'LOGIN',
        txtNumberSelectionTitle: strings.txtNumberSelectionTitle
      }
    };

    console.log("************** Going to Request Camera Permission");
    requestAppPermission();
  }

  componentDidMount() {
    Keyboard.dismiss();
    if (FuncUtils.isNullOrUndefined(this.props.bannerImages)) {
      console.log("Loading banner images");
      this.props.commonGetBannerImages(this.saveBannerImages);
    }

    if (this.props.didPerformNumberCheck == true) return;
    // this.setState({ isLoading: true });
    this.props.getApiLoading(true);
    var self = this;
    const data = {
      type: 'avaibableList'
    };

    Utill.apiRequestPost('checkPaymentAccounts', 'gsmConnection', 'ccapp', data, (response) => {
      // self.setState({ isLoading: false });
      self.props.getApiLoading(false);
      self.props.setNumberCheckPerformed(true);

      const { ezCash = [], rapidEz = [], selectedIndexes = { ezCash: "", rapidEz: "" } } = response.data;

      console.log("availableList: ", response);
      var numberSelectionArray = {
        ezCash: ezCash,
        rapidEz: rapidEz,
        selectedIndexes: selectedIndexes
      };

      console.log("numberSelectionArray: ", numberSelectionArray);

      let ezCashArray = numberSelectionArray.ezCash == null ? [] : numberSelectionArray.ezCash;
      let rapidEzArray = numberSelectionArray.rapidEz == null ? [] : numberSelectionArray.rapidEz;
      // let selectedIndexes = numberSelectionArray.selectedIndexes;

      let showNumberSelectionModal = false;

      if (ezCashArray.length == 0 && rapidEzArray.length === 0) {
        showNumberSelectionModal = false;
      }

      if (ezCashArray.length == 1 && rapidEzArray.length === 1) {
        showNumberSelectionModal = false;
      }

      if ((ezCashArray.length == 1 && rapidEzArray.length > 1) && selectedIndexes.rapidEz === "") {
        console.log('In case 1');
        showNumberSelectionModal = true;
      }

      if ((rapidEzArray.length == 1 && ezCashArray.length > 1) && selectedIndexes.ezCash === "") {
        console.log('In case 2');
        showNumberSelectionModal = true;
      }

      if (ezCashArray.length > 1 && selectedIndexes.ezCash === "") {
        console.log('In case 3');
        showNumberSelectionModal = true;
      }

      if (rapidEzArray.length > 1 && selectedIndexes.rapidEz === "") {
        console.log('In case 4');
        showNumberSelectionModal = true;
      }

      if (selectedIndexes.ezCash !== "" && selectedIndexes.rapidEz !== "") {
        showNumberSelectionModal = false;
      }

      console.log("show_HOME_MENU DATA: ", JSON.stringify(response.data));
      console.log("numberSelectionArray DATA: ", JSON.stringify(numberSelectionArray));

      if (showNumberSelectionModal == true && this.props.isRetailer == false) {
        const navigatorOb = this.props.navigator;
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.views.NumberSelectionScreen',
          title: this.state.locals.txtNumberSelectionTitle,
          passProps: {
            availableNumbers: numberSelectionArray,
            onUnmount: () => {
              console.log('xxx showHomeMenu :', response);
              // self.setState({ isLoading: false });
              self.props.getApiLoading(false);
              Keyboard.dismiss();
            }
          },
          navigatorButtons: {
            leftButtons: [
              {}
            ]
          },
          overrideBackPress: true
        });
      }

    }, (failCb) => {
      // self.setState({ isLoading: false });
      self.props.getApiLoading(false);
      console.log("home/index.js :: checkPaymentAccounts :: in fail cb\n", failCb);

    }, (exCb) => {
      console.log("home/index.js :: checkPaymentAccounts :: in ex cb\n", exCb);
      // self.setState({ isLoading: false });
      self.props.getApiLoading(false);
    });
  }

  /**
 * @description Save banner images
 * @param {Object} data
 * @memberof ButtonSubmit
 */
  saveBannerImages = async (data) => {
    console.log('saveBannerImages', data);
    await LocalStorageUtils.saveBannerData(data);
  }


  render() {
    let loadingIndicator;
    if (this.props.api_loading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.container}>
        {loadingIndicator}
        <View style={styles.containerBanner}>
          <AdBanner bannerImages={this.props.bannerImages} />
        </View>
        <View style={styles.containerMenuItems}>
          <TileView {...this.props} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: home :: index => genaral ', state.genaral);
  return {
    language: state.lang.current_lang,
    didPerformNumberCheck: state.genaral.didPerformNumberCheck,
    api_loading: state.auth.api_loading,
    bannerImages: state.common.bannerImages

  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerBanner: {
    flex: 0.375,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerMenuItems: {
    flex: 0.625,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  }
});

export default connect(mapStateToProps, actions)(HomeMain);
