import React from 'react';
import { View, StyleSheet } from 'react-native';
import Slideshow from './SlideShow';
import bgSrc from '../../../images/ad_banner_1.png';
import Color from '../../config/colors';

class AdBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position: 1,
      interval: null,
      dataSource: [
        {
          title: '',
          caption: '',
          url: bgSrc
        }
      ]
    };
  }

  componentWillMount() {
    this.setState({ interval: setInterval(() => {
      this.setState({
        position: this.state.position === this.state.dataSource.length
          ? 0
          : this.state.position + 1
      });
    }, 2000) });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  render() {

    let bannerImages = this.props.bannerImages !=null ? this.props.bannerImages : this.state.dataSource;

    return (
      <View style={styles.containerWrapper}>
        <Slideshow
          style={styles.picture}
          dataSource={bannerImages}
          position={this.state.position}
          onPositionChanged={position => this.setState({ position })}
          arrowSize={0}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerWrapper: {
    flex: 1,
    zIndex: -1,
    margin: 4,
    marginBottom: 5,
    // borderRadius: 4,
    borderWidth: 0.5,
    borderColor: Color.black
  },
  picture: {
    flex: 1,
  }
});

export default AdBanner;
