import React from 'react';
import {
  Text, View, Image, StyleSheet, Dimensions,
  Alert, AsyncStorage, TouchableOpacity, DeviceEventEmitter,
  Linking, ScrollView, ListView, NativeModules, PermissionsAndroid
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import DeviceCompatibleModel from '../../views/authz/DeviceCompatibleModal';
import { Screen, AccountUtils } from '../../utills';
import { Colors, Constants, Images, globalConfig as Global } from '../../config/';
import Utills from '../../utills/Utills';
import strings from '../../Language/Home';
import img2 from '../../../images/menu_icons/sim_change.png';
import img1 from '../../../images/menu_icons/bill_payment.png';
import img3 from '../../../images/menu_icons/mobile_activation.png';
import img_dtv from '../../../images/menu_icons/television.png';
import img6 from '../../../images/menu_icons/activation_status.png';
import img7 from '../../../images/menu_icons/delivery_truck.png';
import img8 from '../../../images/menu_icons/Doc990.png';
import img9 from '../../../images/menu_icons/hbb.png';
import img10 from '../../../images/menu_icons/cart.png';
import img11 from '../../../images/menu_icons/customer_tracker.png';
import img12 from '../../../images/menu_icons/stock.png';
import img13 from '../../../images/menu_icons/stock_request_approval.png';
import img14 from '../../../images/menu_icons/workoder.png';
import img15 from '../../../images/menu_icons/cart.png';
import img16 from '../../../images/menu_icons/ezcash.png';

const UNDERLAY_COLOR = 'rgba(0,1,1,0.7)';
const TILE_HEIGHT = (((Dimensions.get('window').height - 55) * 0.625) / 3) - 10;

const Utill = new Utills();

class TileView extends React.Component {
  constructor(props) {
    super(props);
    console.log('xxxxxxx LANGUAGE', this.props.language);
    strings.setLanguage(this.props.language);
    this.state = {
      simChange: strings.SimChange,
      BillPay: strings.BillPay,
      MobActivate: strings.MobActivate,
      ActivationState: strings.ActivationState,
      doc990: strings.doc990,
      WorkOrders: strings.WorkOrders,
      stockManagement: strings.stockManagement,//cfss
      customerTracker: strings.customerTracker, //cfss
      stockRequestApproval: strings.stockRequestApproval, //cfss
      directSales: strings.directSales,
      wom: strings.wom,//cfsss
      transactionHistory: strings.transactionHistory,
      postToPre: strings.postToPre, //cfss
      hbb: strings.hbb,
      dtvActivation: strings.DTVActivation,
      rejected: strings.rejected,
      askLocationServiceToEnable: strings.askLocationServiceToEnable,
      all: strings.all,
      allowedServices: {},
      tileData: [],
      showEmptyTileView: false,
      modalVisible: false,
      systemError: strings.system_error_please_try_again,
      ezCash: strings.ezCash,
      btnOk: strings.btnOk,
      askLocationPermission: strings.askLocationPermission,
      deviceCompatibleInfo: {
        state: 'pending',
        description: ''
      },
      locals: {
        please_contact: 'Seems you don\'t have any features assigned to you. Please contact the retailer hotline'
      },
      isLoading: false,
    };
  }

  /**
   * This function will create and invoke the heartbeat
   *
   * @memberof TileView
   */
  async componentDidMount() {
    console.log('################ Home :: componentDidMount ###############');
    console.log("Global.heartBeatTimer", Global.heartBeatTimer);
    this.addDeviceDataToDb();
    Utill.getConnNumber();
    this.props.getTileUrls();
    this.props.configSetLteActivationStatus(false);

    await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    console.log("Service Start\n" + global.subAgentMsisdn + "\n" + Global.appVersion);
    let subAgentMsisdn = await AsyncStorage.getItem('conn');
    NativeModules.NetworkMonitoring.serviceStartStop(
      subAgentMsisdn,
      Global.appVersion,
      Global.appName,
      true
    );

    DeviceEventEmitter.removeAllListeners("updateLocation");
    DeviceEventEmitter.addListener("updateLocation", async (geoData) => {
      console.log('updateLocation TileView ', geoData);
      let data = {};
      let isCFSSAgent = await AccountUtils.isCFSSAgent();
      if (isCFSSAgent) {
        NativeModules.GeoLocationService.checkGPSStatus()
          .then(gpsStatus => {
            console.log('checkGPSStatus ', gpsStatus);
            if (gpsStatus == 'true') {
              NativeModules.GeoLocationService.checkPermission()
                .then(result => {
                  console.log('checkPermission ', result);
                  if (result == 'true') {
                    data = {
                      latitude: geoData.coords.latitude,
                      longitude: geoData.coords.longitude
                    };
                    this.checkHeartBeat(data);
                  } else {
                    this.getGPSLocation();
                  }
                }).catch(result => {
                  console.log('checkPermission catch ', result);
                  this.showPermissionRequestAlert(this.getGPSLocation());
                });
            } else {
              this.showGPSStatusAlert(this.state.askLocationServiceToEnable);
            }
          })
          .catch(result => {
            console.log('checkGPSStatus ', result);
            this.showGPSStatusAlert(this.state.askLocationServiceToEnable);
          });
      } else {
        this.checkHeartBeat(data);
      }
    });

    this.isHeartBeatTimerAvailable();
  }

  componentWillUnmount() {
    console.log('## TileView :: componentWillUnmount');
    DeviceEventEmitter.removeListener("updateLocation", (geoData) => {
      console.log('updateLocation removeListener ', geoData);
      NativeModules.GeoLocationService.stopService()
        .then(result => {
          console.log('stopService ', result);
        })
        .catch(result => {
          console.log('stopService ', result);
        });
      // Do stuff with the geoData
    });
  }

  addDeviceDataToDb = async () => {
    console.log('xxx addToCaptureDb');
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const options = {
        deviceData: JSON.stringify(deviceData)
      };
      const errorCb = (msg) => {
        console.log('xxxx addDeviceDataToDb errorCb ');
        console.log(msg);
      };
      const successCb = (msg) => {
        console.log('xxxx addDeviceDataToDb successCb');
        console.log(msg);
      };
      RNReactNativeImageuploader.addDeviceDataToDb(options, errorCb, successCb);
    } catch (e) {
      console.log('addDeviceDataToDb Failed', e.message);
    }
  }

  isHeartBeatTimerAvailable = async () => {
    console.log('## isHeartBeatTimerAvailable');
    if (_.isUndefined(Global.heartBeatTimer)) {
      console.log('## isHeartBeatTimerAvailable :: NOT_RUNNING');
      let heartBeatInterval = Global.heartBeatInterval;
      let heartBeatIntervalRes = await AsyncStorage.getItem('heartBeatInterval');
      console.log('CURRENT :: heartBeatInterval : ', heartBeatIntervalRes);
      if (!_.isNil(heartBeatIntervalRes) && heartBeatIntervalRes !== '') {
        heartBeatInterval = parseInt(heartBeatIntervalRes);
      }
      console.log('NEW :: heartBeatInterval : ', heartBeatIntervalRes);
      let isCFSSAgent = await AccountUtils.isCFSSAgent();
      if (isCFSSAgent) {
        console.log('## isHeartBeatTimerAvailable - CFSS');
        this.checkForPermission(heartBeatInterval);
      } else {
        console.log('## isHeartBeatTimerAvailable - O2A');
        this.startService(heartBeatInterval);
      }
    } else {
      console.log('## isHeartBeatTimerAvailable :: ALREADY_RUNNING');
    }
  }

  checkForPermission = async (heartBeatInterval) => {
    console.log('#### getGPSLocation ###');
    const locationPermission = await Utill.requestPermission('LOCATION');
    if (locationPermission || locationPermission == PermissionsAndroid.RESULTS.GRANTED) {
      this.startService(heartBeatInterval);
    } else {
      this.showPermissionRequestAlert(this.checkForPermission(heartBeatInterval));
    }
  }

  getGPSLocation = () => {
    console.log('#### getGPSLocation ###');
    let data = {};
    Utill.getWOMUserLocation(this.props.language)
      .then((coords) => {
        console.log('getGPSLocation ', coords);
        data = {
          latitude: coords.latitude,
          longitude: coords.longitude
        };
        this.checkHeartBeat(data);
      })
      .catch((result) => {
        this.showGPSStatusAlert(result.message);
      });
  }

  showGPSStatusAlert = (message) => {
    Alert.alert('', message, [
      {
        text: 'OK',
        onPress: () => {
          this.timeout = setTimeout(() => {
            this.getGPSLocation();
          }, Global.locationPopUpInterval)
        }
      }
    ], { cancelable: false });
  }

  showPermissionRequestAlert(func) {
    Alert.alert('', this.state.askLocationPermission, [
      {
        text: 'OK',
        onPress: () => {
          this.timeout = setTimeout(() => {
            func();
          }, Global.locationPopUpInterval)
        }
      }
    ], { cancelable: false });
  }

  startService = (heartBeatInterval) => {
    console.log('#### startService ###');
    NativeModules.GeoLocationService.setTimeInterval(heartBeatInterval);
    NativeModules.GeoLocationService.startService()
      .then(value => {
        Global.heartBeatTimer = 'true';
        console.log('startService ', value);
      })
      .catch(response => {
        this.isHeartBeatTimerAvailable();
        console.log('startService error ', response);
      });
  }

  checkHeartBeat = (data) => {
    console.log('#### checkHeartBeat ###');
    Utill.apiRequestPost2('heartBeatCheck', 'account', 'ccapp', data, this.heartBeatSuccessCb, this.heartBeatFailureCb, this.heartBeatExCb);
  }

  heartBeatSuccessCb = response => {
    console.log("Heartbeat Success Response: ", JSON.stringify(response));
    if (response.data.force_logout == true) {
      console.log("Logging out...");
      Alert.alert('', Utill.getStringifyText(response.data.message), [
        {
          text: 'OK',
          onPress: () => {
            this.clearPersistanceDataToLogout();
          }
        }
      ], { cancelable: false });
    }
  }

  heartBeatFailureCb = response => {
    console.log("Heartbeat Fail Response: ", JSON.stringify(response));
  }

  heartBeatExCb = response => {
    console.log("Heartbeat Ex Response: ", JSON.stringify(response));
  }

  clearPersistanceDataToLogout = async () => {
    console.log('clearPersistanceDataToLogout');
    const navigator = this.props.navigator;
    try {
      let isFirstTime = await AsyncStorage.getItem('isFirstTime');
      await AsyncStorage.removeItem('isFirstTime');
      await AsyncStorage.removeItem('api_key');
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('conn');
      await AsyncStorage.setItem('isFirstTime', isFirstTime);
      console.log(AsyncStorage.getItem('isFirstTime'));
      console.log(AsyncStorage.getItem('api_key'));
      console.log(AsyncStorage.getItem('token'));
      console.log(AsyncStorage.getItem('conn'));
      navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.LoginScreen' });
    } catch (error) {
      console.log(`clearPersistanceDataToLogout :: error: ${error.message}`);
    }
  }

  /**
   * This function maps the image url and the title of the tile when an id is passed.
   * 
   * @param {Number} id 
   * @returns {Object} {imgUrl:'',title:''}
   * @memberof TileView
   */
  mapIdToData = (id) => {
    const DATA_MAPPING = [{
      "id": 1,
      "img": img1,
      "title": this.state.BillPay
    },
    {
      "id": 2,
      "img": img2,
      "title": this.state.simChange
    },
    {
      "id": 3,
      "img": img3,
      "title": this.state.MobActivate
    },
    {
      "id": 4,
      "img": img_dtv,
      "title": this.state.dtvActivation
    },
    {
      "id": 6,
      "img": img6,
      "title": this.state.transactionHistory
    },
    {
      "id": 7,
      "img": img7,
      "title": this.state.WorkOrders
    },
    {
      "id": 8,
      "img": img8,
      "title": this.state.doc990
    },
    {
      "id": 9,
      "img": img9,
      "title": this.state.hbb
    },
    {
      "id": 10,
      "img": img10,
      "title": this.state.directSales
    },
    {
      "id": 11,
      "img": img11,
      "title": this.state.customerTracker
    },
    {
      "id": 12,
      "img": img12,
      "title": this.state.stockManagement
    },
    {
      "id": 13,
      "img": img13,
      "title": this.state.stockRequestApproval
    },
    {
      "id": 14,
      "img": img14,
      "title": this.state.wom
    },
    {
      "id": 15,
      "img": img15,
      "title": this.state.postToPre
    }, {
      "id": 16,
      "img": img16,
      "title": this.state.ezCash
    }];

    let data = {};
    DATA_MAPPING.forEach(function (item) {
      console.log('itemId', item.id);
      console.log('Id', id);
      console.log('item.id === id', item.id == id);
      if (item.id == id) {
        data.imgUrl = item.img;
        data.title = item.title;
        console.log('imgUrl', data);
      }
    });
    return data;
  }

  /**
   * This function will be triggered when a tile is pressed. The user will be pushed to the relevant screen
   * according to the id that is passed.
   * 
   * @param {Number} key - id of the tile selected
   * @memberof TileView
   */
  onAssetPress = (key, menuObject) => {
    console.log('## onAssetPress');
    this.props.configSetActivityStartTime();
    console.log('Key =>', key);
    console.log('menuObject =>', menuObject);

    if (menuObject.tile_status == false) {
      this.setState({
        modalVisible: true,
        deviceCompatibleInfo: {
          state: 'pending',
          description: menuObject.compat_data.description,
          url: menuObject.compat_data.url
        }
      });
      return;
    }

    const navigator = this.props.navigator;
    switch (key) {
      case Constants.TILE_ID.SIM_CHANGE:
        navigator.push({
          title: this.state.simChange,
          screen: 'DialogRetailerApp.views.SimChangeScreen'
        });
        break;
      case Constants.TILE_ID.BILL_PAYMENT:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.BILL_PAYMENT);
        break;
      case Constants.TILE_ID.GSM_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.GSM_ACTIVATION);
        break;
      case Constants.TILE_ID.DTV_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.DTV_ACTIVATION);
        break;
      case Constants.TILE_ID.TRACTIONS_HISTORY:
        navigator.push({
          screen: 'DialogRetailerApp.views.TransactionHistoryMainScreen',
          title: this.state.ActivationState
        });
        break;
      case Constants.TILE_ID.DELIVERY_APP:
        navigator.push({
          title: "DELIVERY APP",
          screen: 'DialogRetailerApp.views.DeliveryMainScreen'
        });
        break;
      case Constants.TILE_ID.DOC_990:
        Linking.openURL(this.props.doc990URL);
        break;
      case Constants.TILE_ID.LTE_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.LTE_ACTIVATION);
        break;
      case 10:
        navigator.push({
          title: "DIRECT SALES",
          screen: 'DialogRetailerApp.views.DirectSale',
          passProps: {
            screenTitle: "DIRECT SALES",
          },
        });
        break;
      case 11:
        navigator.push({
          title: "CUSTOMER TRACKER",
          screen: 'DialogRetailerApp.views.CpeTracker'
        });
        break;
      case 12:
        navigator.push({
          title: "STOCK MANAGEMENT",
          screen: 'DialogRetailerApp.views.StockManagement'
        });
        break;
      case 13:
        navigator.push({
          title: "STOCK REQUEST APPROVAL",
          screen: 'DialogRetailerApp.views.StockRequestApproval'
        });
        break;
      case 14:
        navigator.push({
          title: "WORK ORDER",
          screen: 'DialogRetailerApp.views.WomLandingView'
        });
        break;
      case 15:
        navigator.push({
          title: "DTV POST TO PRE CONVERSION",
          screen: 'DialogRetailerApp.views.PostToPreConversionScreen',
          passProps: {
            name: 'DTV POST TO PRE CONVERSION'
          }
        });
        break;
      case Constants.TILE_ID.EZ_CASH:
        Linking.canOpenURL(this.props.ezCashSchemUrl).then(supported => {
          if (supported) {
            console.log('accepted');
            return Linking.openURL(this.props.ezCashSchemUrl);
          } else {
            return Linking.openURL(this.props.eZcashUrl);
          }
        }).catch(
          err => Screen.showAlert(this.state.systemError, err)
        );
        break;
      default:
        navigator.push({
          title: this.state.ActivationState,
          screen: 'DialogRetailerApp.views.MobileActivationScreen'
        });
        break;
    }
  };


  /**
   * @description This function  will check ezcash availability
   * @param {Object} pushView
   * @memberof TileView
   */
  checkEzcashAccount = (pushView) => {
    if (this.props.getConfiguration.ez_cash_account.ez_cash_account_availability == false) {
      let description = this.props.ezCash_error;
      let passProps = {
        primaryText: this.state.btnOk,
        primaryPress: () => this.onPressOk(),
        primaryButtonStyle: { color: Colors.colorRed },
        disabled: false,
        hideTopImageView: false,
        icon: Images.icons.FailureAlert,
        description: Utill.getStringifyText(description),
      };
      Screen.showGeneralModal(passProps);
    } else {
      const navigator = this.props.navigator;
      navigator.push(pushView);
    }
  }

  /**
   * @description Back to homeScreen
   * @memberof TileView
   */
  onPressOk = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }


  render() {
    return (
      <View style={styles.containerWrapperVertical}>
        {this.state.showEmptyTileView ? this.renderEmptyMessageView() :
          (<ScrollView
            style={styles.scrollViewStyle}
            contentContainerStyle={styles.containerWrapper}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
          >
            <View style={[styles.tileColumnLeft]}>
              {this.renderMenuTiles('left')}
            </View>
            <View style={[styles.tileColumnRight]}>
              {this.renderMenuTiles('right')}
            </View>
          </ScrollView>)
        }
        <DeviceCompatibleModel
          msisdn={111}
          deviceCompatibleInfo={this.state.deviceCompatibleInfo}
          onOkPress={() => this.onOkPress()}
          modalVisible={this.state.modalVisible}
          locals={{ ok: 'OK' }}
          openUrl={() => this.openUrl()}
        />
      </View>

    );
  }

  onOkPress = () => {
    this.setState({ modalVisible: false });
  }

  openUrl = () => {
    var spillable = this.state.deviceCompatibleInfo.url.split('/');
    var action = spillable[0];
    var controller = spillable[1];
    let url = Utill.createApiUrl(controller, action, '');
    console.log(url);
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        return Linking.openURL(url);
      }
    }).catch(
      err => Screen.showAlert(this.state.systemError, err)
    );
  }

  /**
   * This function will render a tile according to the data of the Menu Item
   * 
   * @param {Number} item - Id of the tile
   * @param {String} column - Side of the column (left/right)
   * @returns <TouchableOpacity> component (A Tile)
   * @memberof TileView
   */
  renderGridItem = (item, column) => {
    console.log("Rendering ITEM: " + parseInt(item));
    var itemId = parseInt(item);
    console.log(typeof (itemId));

    //if (itemId == 4 || itemId == 9) return null; // TODO - REMOVE THIS LINE TO MAKE HBB AND DTV TILE VISIBLE
    // Gets the menu item object from services array retrieved from backend.
    const menuObject = this.menuItemForKey(item, column);

    if (menuObject == undefined) {
      return null;
    }

    let defaultHeight = TILE_HEIGHT;

    // Set the tile height according to the data object retrieved by backend.
    let tileHeight = defaultHeight * parseInt(menuObject.height);

    // Add an excess 10px if the height is greater than 1 in order to cover the padding spaces.
    if (parseInt(menuObject.height) > 1) tileHeight = tileHeight + 5;

    let mappedData = this.mapIdToData(parseInt(menuObject.id));

    return (
      <TouchableOpacity
        key={menuObject.id}
        underlayColor={UNDERLAY_COLOR}
        onPress={() => {
          this.onAssetPress(itemId, menuObject);
        }}
        style={[styles.gridItem, { height: tileHeight }]}
        accessibilityLabel={`tile_item_${menuObject.id}`}
        testID={`tile_item_${menuObject.id}`}
      >
        <View style={styles.innerView}>
          <View style={styles.imageView_c1}>
            <Image source={mappedData.imgUrl} style={styles.image_c1} />
            <View style={styles.textView_c1}>
              <Text style={styles.contentTxt}>
                {mappedData.title}
              </Text>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    );
  }

  /**
   * This function will push the user from the home menu to relevant screen after checking if the required
   * EzCash and RapidEz accounts are configured.
   * 
   * @param {Number} key - id of the tile selected
   * @memberof TileView
   */
  checkDefaultAccountApiCall = (key) => {
    console.log('## checkDefaultAccountApiCall');
    this.props.getConfigurations(this, key);
  }

  checkDefaultAccountAndPush = (key) => {
    console.log('## checkDefaultAccountAndPush :: ', key);
    let _this = this;
    const navigator = _this.props.navigator;
    let defaultEzNumber = _this.props.getConfiguration.payment_accounts.ezCashNumber !== null ? _this.props.getConfiguration.payment_accounts.ezCashNumber[0] : "";

    this.props.configSetDefaultEzCashAccount(defaultEzNumber);

    switch (key) {
      case 1:
        navigator.push({
          title: _this.state.BillPay,
          screen: 'DialogRetailerApp.views.BillPaymentScreen',
          passProps: {
            defaultEzNumber: defaultEzNumber
          }
        });
        break;
      case 3:
        navigator.push({
          title: _this.state.MobActivate,
          screen: 'DialogRetailerApp.views.MobileActivationScreen',
          passProps: {
            title: _this.state.MobActivate,
            defaultEzNumber: defaultEzNumber
          }
        });
        break;
      case 4:
        _this.checkEzcashAccount({
          title: _this.state.dtvActivation,
          screen: 'DialogRetailerApp.views.DtvActScreen',
          passProps: {
            title: _this.state.dtvActivation,
            defaultEzNumber: defaultEzNumber
          }
        });
        break;
      case 9:
        _this.checkEzcashAccount({
          title: _this.state.hbb,
          screen: 'DialogRetailerApp.screens.CoverageMapScreen',
          passProps: {
            title: this.state.hbb,
            defaultEzNumber: defaultEzNumber
          }
        });
        break;
      default:
        console.log('Check Default Acc Default block');
    }
  }

  /**
   * This function will return an object from the object array related to the passed column.
   * 
   * @param {Number} key - id of the tile
   * @param {String} column - column that the tile of the id belongs to (left/right)
   * @returns 
   * @memberof TileView
   */
  menuItemForKey = (key, column) => {
    let services = JSON.parse(this.props.tileData); //TODO:- UnComment to support data from backend
    let columnId = 0;
    if (column == 'right') columnId = 1;

    console.log("MenuItemForKey:", services[columnId]);
    console.log('Key: ', key);
    for (var value of services[columnId]) {
      if (value.id == key) {
        console.log("returning ", value);
        return value;
      }
    }
  }

  /**
   * Renders a empty View .
   * 
   * @param {String} column 
   * @returns <View>
   * @memberof TileView
   */
  renderEmptyMessageView = () => {
    return (
      <View style={styles.emptyMessageView}>
        <Text style={styles.emptyMessageText}>
          {this.state.locals.please_contact}
        </Text>
      </View>
    );
  }

  /**
   * Renders a List View as a column according to an array that is provided.
   * 
   * @param {String} column 
   * @returns <Listview>
   * @memberof TileView
   */
  renderMenuTiles = (column) => {
    console.log('## renderMenuTiles');
    var menuIds = [];
    var allowedServices = [];
    var services = []; //TODO:- UnComment to support data from backend
    var dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    if (_.isEmpty(this.props.tileData)) {
      console.log('## Drawer :: tileData => empty ');
      return;
    }
    console.log('## Drawer :: tileData => this.props.tileData', this.props.tileData);

    if (this.props.tileData === undefined || this.props.tileData === null || this.props.tileData.length === 0) {
      console.log("TileView :: tileData are null or undefined", this.props.tileData);
      return;
    } else {
      console.log("TileView :: tileData are available or empty", this.props.tileData);
      console.log("JSON.parse", this.props.tileData);
      services = JSON.parse(this.props.tileData); //TODO:- UnComment to support data from backend
    }

    if (services[0] == undefined || services[1] == undefined || _.isEmpty(services[0]) || _.isEmpty(services[1])) {
      console.log('## Drawer :: services => empty ');
      this.setState({ showEmptyTileView: true });
      return;
    }

    if (column == 'left') {
      allowedServices = services[0]; // Services array will look like [[left column items],[left column items]]
    } else {
      allowedServices = services[1];
    }

    if (allowedServices == null || allowedServices == undefined) return;

    let shouldEnableScroll = services[0].length > 3 || services[1].length > 3;


    console.log("allowedServices props: ", this.props.tileData);
    console.log("services_left: ", services[0]);
    console.log("services_right ", services[1]);
    console.log("Rendering allowedServices: ", allowedServices);

    // Extracts the ids of each array related to each column in order to render tiles using list view.
    allowedServices.forEach(function (item) {
      menuIds.push(item.id);
    });

    var listItems = dataSource.cloneWithRows(menuIds);
    console.log('menuIds', menuIds);

    return (
      <ListView
        contentContainerStyle={styles.grid}
        scrollEnabled={shouldEnableScroll}
        dataSource={listItems}
        renderRow={(item) =>
          this.renderGridItem(item, column)
        }
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TileView => Home ', state);
  const doc990URL = state.auth.doc990URL;
  const ezCashSchemUrl = state.auth.ezCashSchemUrl;
  const eZcashUrl = state.auth.eZcashUrl;
  console.log("doc990URL: ", doc990URL);
  const getConfiguration = state.auth.getConfiguration;
  const { ezCash_error = '' } = getConfiguration.ez_cash_account;

  return {
    language: state.lang.current_lang,
    mobile_activation: state.mobile.mobile_activation !== null
      ? state.mobile.mobile_activation
      : null,
    allowedServices: state.auth.allowedServices,
    tileData: state.auth.tileData,
    doc990URL,
    ezCashSchemUrl,
    eZcashUrl,
    getConfiguration,
    ezCash_error
  };
};

const styles = StyleSheet.create({
  containerWrapper: {
    flexGrow: 1,
    margin: 5,
    marginTop: 0,
    flexDirection: 'row',
  },
  containerWrapperVertical: {
    flex: 1,
    flexDirection: 'row'
  },
  tileColumnLeft: {
    flex: 1,
    flexDirection: 'column',
    marginRight: 2.5
  },
  tileColumnRight: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 2.5
  },
  grid: {
    flexDirection: 'column',
    flex: 1
  },
  gridItem: {
    width: Dimensions.get('window').width / 2 - 7.5,
    height: TILE_HEIGHT,
    marginTop: 0,
    marginBottom: 5,
    borderRadius: 4,
    backgroundColor: Colors.yellow,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollViewStyle: {
    flex: 1
  },
  innerView: {
    flex: 1,
    padding: 5,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },

  imageView_c1: {
    width: '100%',
    height: 100,
    paddingVertical: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textView_c1: {
    // flex: 1,
    height: 40,
    padding: 0,
    margin: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: Colors.green
  },
  contentTxt: {
    fontSize: 16,
    color: Colors.colorBlack,
    // padding: 2,
    fontWeight: '500',
    textAlign: 'center',
    // backgroundColor: Colors.colorBlue
  },
  image_c1: {
    width: Dimensions.get('window').height * 0.078125,
    height: Dimensions.get('window').height * 0.078125,
    margin: 5,
    marginBottom: 0,
    bottom: 0,
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
  },

  emptyMessageView: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  emptyMessageText: {
    fontSize: 15,
    color: Colors.colorLightBlack,
    fontWeight: '400',
    textAlign: 'center',
  }


});

export default connect(mapStateToProps, actions)(TileView);