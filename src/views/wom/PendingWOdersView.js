import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  Dimensions,
  RefreshControl,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import Modal from "react-native-modal";
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RadioForm, { RadioButton, RadioButtonInput } from 'react-native-simple-radio-button';
import _ from 'lodash';

import * as actions from '../../actions';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import { Header } from './Header';
import { SearchBar } from './components/SearchBar';
import DTVImage from '../../../images/wom/dtv.png';
import DBNImage from '../../../images/wom/dbn.png';
import GSMImage from '../../../images/wom/gsm.png';
import DEImage from '../../../images/wom/de.png';
import CDMAImage from '../../../images/wom/cdma.png';
import FBBImage from '../../../images/wom/fbb.png';
import BIZImage from '../../../images/wom/biz.png';
import Communications from 'react-native-communications';
import strings from '../../Language/Wom';
import Color from '../../config/colors';
import globalConfig from '../../config/globalConfig';

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class PendingWOdersView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);

    this.state = {
      selectedSort: 'All',
      selectedSortToDisplay: 'All',
      workORders: [],
      isLoading: false,
      workORdersLists: [],
      apiLoading: false,
      isModalVisible: false,
      api_error: false,
      isModalVisibleDispatch: false,
      selectedItem: '',
      sortTypes: ["DTV", "DBN", "GSM", "DTTL"],
      selectedPhoneNumber: '',
      googleMapsMessage: strings.googleMapsMessage,
      screenTitle: '',
      locals: {
        lblPriority: strings.lblPriority,
        error: strings.error,
        screenTitle: strings.screenTitle,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        pendingTitle: strings.pendingTitle,
        dispatchTitle: strings.dispatchTitle,
        inProgressTitle: strings.inProgressTitle,
        slaBreachedWOTitle: strings.slaBreachedWOTitle,
        slaAbtToBreachWOTitle: strings.slaAbtToBreachWOTitle,
        priorityWOTitle: strings.priorityWOTitle,
        lblNICNo: strings.lblNICNo,
        lblType: strings.lblType,
        lblConnectionNo: strings.lblConnectionNo,
        lblSTBPack: strings.lblSTBPack,
        lblAccessoryPack: strings.lblAccessoryPackDetail,
        lblWarrantyPeriod: strings.lblWarrantyPeriod,
        noworkoderfound: strings.noworkoderfound,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        lblSLALapse: strings.lblSLALapse,
        lblSchdule: strings.lblSchdule,
        lblAssignTime: strings.lblAssignTime,
        lblAll: strings.lblAll,
        alertHeader: strings.alertHeader,
        woID: strings.WOID,
        searchPlaceholder: strings.searchPlaceholder
      },
      langValue: 0,
      phoneNumberList: []
    };
    this.count = this.props.count;
    this.navigatorOb = this.props.navigator;
    this.initialWOList = [];
    this.searchEnabled = false;
    this.searchedText = '';

    this.debounceOnPressDetails = _.debounce((item) => this.OnPressDetails(item), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceOndipatchConfirm = _.debounce(() => this.OndipatchConfirm(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceNavigateFilter = _.debounce(() => this.NavigateFilter(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    if (this.props.searchEnabled) {
      this.searchBar.show();
      this.initialWOList = this.props.initialWoList
      this.searchBar.setSearchedValue(this.props.searchedText);
    }

    let viewTiltle = this.state.locals.pendingTitle;
    if (this.props.woId == "DISPATCHED") {
      viewTiltle = this.state.locals.dispatchTitle;
    } else if (this.props.woId == "IN_PROGRESS") {
      viewTiltle = this.state.locals.inProgressTitle;
    } else if (this.props.woId == 'SLA_BREACHED') {
      viewTiltle = this.state.locals.slaBreachedWOTitle;
    } else if (this.props.woId == 'PRIORITY') {
      viewTiltle = this.state.locals.priorityWOTitle;
    } else if (this.props.woId == 'SLA_ABOUT_TO_BREACH') {
      viewTiltle = this.state.locals.slaAbtToBreachWOTitle;
    } else if (this.props.woId == 'INITIAL') {
      viewTiltle = this.state.locals.pendingTitle;
    }
    this.getLanguage();

    this.setState({ screenTitle: viewTiltle }, () => {
      if (this.props.load_filer_call == 1) {
        this.LoadData();
      } else {
        this.setState({ workORdersLists: this.props.woList });
      }
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.resetTo({ title: this.state.locals.screenTitle, screen: 'DialogRetailerApp.views.WomLandingView' });
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  OndipatchConfirm() { this.setState({ isModalVisibleDispatch: false }) }

  onRefresh() {
    this.searchBar.hide();
    this.setState({ isLoading: false }, () => this.LoadData());
  }

  LoadData = () => {
    this.setState({ apiLoading: true }, () => {
      const data = { job_status: this.props.woId };

      Utill.apiRequestPost('GetWorkOrders', 'WorkOrders', 'wom', data,
        this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
    });
  }

  LoadDataSuccessCb = (response) => {
    this.setState({
      apiLoading: false,
      api_error: false,
      workORdersLists: response.data.data,
      BIZ_UNIT: response.data.data
    });
    this.dissmissNoNetworkModal()
  }

  LoadDataFailureCb = (response) => {
    this.setState({ apiLoading: false, api_error: true }, () => this.displayCommonAlert('defaultAlert', this.state.locals.error, response));
  }

  LoadDataExcb = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.LoadData);
      } else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  async sortTypeSelected(par1, par2) {
    if (par2 === "Priority") {
      await this.setState({ selectedSort: 'priority', selectedSortToDisplay: this.state.locals.lblPriority }, () => this.LoadSortData());
    } else if (par2 === "SLA Lapse") {
      await this.setState({ selectedSort: 'sla_lapse', selectedSortToDisplay: this.state.locals.lblSLALapse }, () => this.LoadSortData());
    } else if (par2 === "Schedule Time") {
      await this.setState({ selectedSort: 'schedule_time', selectedSortToDisplay: this.state.locals.lblSchdule }, () => this.LoadSortData());
    } else if (par2 === "Assigned Time") {
      await this.setState({ selectedSort: 'assigned_time', selectedSortToDisplay: this.state.locals.lblAssignTime }, () => this.LoadSortData());
    } else if (par2 === "All") {
      await this.setState({ selectedSort: 'all', selectedSortToDisplay: this.state.locals.lblAll }, () => this.LoadSortData());
    }
  }

  LoadSortData = () => {
    this.searchBar.hide();
    this.setState({ apiLoading: true }, () => {
      const data = { job_status: this.props.woId, sort_by: this.state.selectedSort };

      Utill.apiRequestPost('GetWorkOrders', 'WorkOrders', 'wom', data,
        this.LoadDataSuccessSort, this.LoadDataFailureSort, this.LoadDataExcbSort);
    });
  }

  LoadDataSuccessSort = (response) => {
    this.setState({ workORdersLists: response.data.data, apiLoading: false, api_error: false }, () => {
      this.dissmissNoNetworkModal()
    });
  }

  LoadDataFailureSort = (response) => {
    this.setState({ apiLoading: false }, () => this.displayCommonAlert('defaultAlert', this.state.locals.error, response));
  }

  LoadDataExcbSort = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.LoadSortData);
      } else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  filterFunc(obj) {
    this.setState({ apiLoading: true }, () => {
      const data = {
        filter_biz_unit: obj.biz_unit,
        filter_lob: obj.lob,
        filter_wo_type: obj.wo_type,
        filter_schedule: obj.schedule,
        job_status: obj.woId,
      };
      Utill.apiRequestPost('GetWorkOrders', 'WorkOrders', 'wom', data,
        this.LoadfilterFuncSuccessCb, this.LoadfilterFuncFailure, this.LoadfilterFuncExcbSort);
    });
  }

  LoadfilterFuncSuccessCb = (response) => {
    this.setState({ workORdersLists: response.data.data, apiLoading: false, api_error: false }, () => {
      this.dissmissNoNetworkModal();
    });
  }

  LoadfilterFuncFailure = (response) => {
    this.setState({ apiLoading: false, api_error: true }, () => this.displayCommonAlert('defaultAlert', this.state.locals.error, response));
  }

  LoadfilterFuncExcbSort = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.LoadSortData);
      } else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  NavigateFilter() {
    this.navigatorOb.push({
      title: this.state.screenTitle,
      screen: 'DialogRetailerApp.views.EtendedSortingView',
      passProps: {
        woId: this.props.woId,
        woNameParam: this.state.screenTitle,
        woList: this.state.workORdersLists,
        filterWOCb: (obj) => this.filterFunc(obj)
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, response) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType,
        messageHeader,
        messageBody: response.message ? response.message : response.data ? response.data.error ? response.data.error : response.data.info ? response.data.info : '' : '',
        messageFooter: "",
      }
    });
  }

  handleSearch() {
    console.log("Search Clicked");
    this.searchBar.show();
    this.searchEnabled = true;
    this.initialWOList = this.state.workORdersLists;
  }

  toggleModal = (visible, multi_contact, contactNo, phoneNumberList) => {
    if (multi_contact) {
      this.setState({ isModalVisible: visible, phoneNumberList: phoneNumberList });
    } else {
      Communications.phonecall(contactNo, true);
    }
  }

  toggleModalHide = (visible) => {
    this.setState({ isModalVisible: visible });
  }

  callToMultipleFunc = () => {
    let num = this.state.selectedPhoneNumber ? this.state.selectedPhoneNumber : this.state.phoneNumberList[0];
    Communications.phonecall(num, true);
    this.setState({ isModalVisible: false });
  }

  async onRadioButtonPress(i, obj) {
    await this.setState({ selectedPhoneNumber: obj, langValue: i });
  }

  async getLanguage() {
    let perData = await Utill.getPersistenceData();
    for (let index = 0; index < this.state.phoneNumberList.length; index++) {
      const element = this.state.phoneNumberList[index];
      if (element.value == perData.lang) {
        this.setState({ langValue: index });
        return;
      }
    }
  }

  toggleModalHideDispatch(visible) {
    this.setState({ isModalVisibleDispatch: visible });
  }

  setWOMDetails(entry) {
    this.props.workOrderGetDetail(entry);
    const nic = entry.nic;
    const conn_type = entry.conn_type;
    const conn_no = entry.conn_no;
    const warrenty = entry.warrenty;
    const stbPack = entry.stb_type;
    const accessoryType = entry.stb_type;

    let ProvArray = [
      {
        "label": this.state.locals.lblNICNo,
        value: nic
      },
      {
        "label": this.state.locals.lblType,
        value: conn_type
      },
      {
        "label": this.state.locals.lblConnectionNo,
        value: conn_no
      },
      {
        "label": this.state.locals.lblSTBPack,
        value: stbPack
      },
      {
        "label": this.state.locals.lblAccessoryPack,
        value: accessoryType
      },
      {
        "label": this.state.locals.lblWarrantyPeriod,
        value: warrenty
      }
    ];
    this.props.workOrderGetDTVProvisioning(ProvArray);
  }

  OnPressDetails(item) {
    this.setWOMDetails(item);
    this.navigatorOb.push({
      title: this.state.screenTitle,
      screen: 'DialogRetailerApp.views.WomDetailView',
      passProps: {
        dispatch_status: this.props.dispatch_status,
        woId: this.props.woId,
        woNameParam: this.state.screenTitle,
        woList: this.state.workORdersLists,
        count: this.count,
        searchEnabled: this.searchEnabled,
        initialWoList: this.initialWOList,
        searchedText: this.searchedText
      }
    });
  }

  getIconImg = (value) => {
    let iconImg;
    switch (value) {
      case 'dtv':
        iconImg = DTVImage;
        break;
      case 'gsm':
        iconImg = GSMImage;
        break;
      case 'de':
        iconImg = DEImage;
        break;
      case 'dbn':
        iconImg = DBNImage;
        break;
      case 'cdma':
        iconImg = CDMAImage;
        break;
      case 'fbb':
        iconImg = FBBImage;
        break;
      default:
        iconImg = BIZImage;
    }
    return iconImg;
  };

  doChangeWO = (item, command) => {
    this.setState({ apiLoading: true }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {
          const data = {
            order_id: item.order_id,
            job_id: item.job_id,
            job_status: item.job_status,
            command: command,
            latitude: loc.latitude,
            longitude: loc.longitude
          };
          Utill.apiRequestPost('ChangeOrderStatus', 'WorkOrders', 'wom', data,
            this.onSuccessDispatch, this.onFailureCb, this.onExCb);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.error, response, '');
          });
        });
    });
  };

  onSuccessDispatch = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.flag == "already_dispatched") {
        this.navigatorOb.push({
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.alertHeader,
            messageBody: response.data.info ? response.data.info : '',
            messageFooter: "",
            overrideBackPress: true,
            onPressOK: () => this.navigatorOb.pop()
          }
        });
      } else {
        let item = this.state.selectedItem
        item.job_status = response.data.job_status;
        this.OnPressDetails(item)
      }
    });
  }

  onFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => this.displayCommonAlert('defaultAlert', this.state.locals.error, response));
  }

  onExCb = (res) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  onClickDispatch = (item) => {
    this.setState({ selectedItem: item }, () => {
      this.navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'alert',
          messageHeader: this.state.locals.woID + this.state.selectedItem.order_id,
          messageBody: strings.dispatchConfirm,
          messageFooter: "",
          btnFun: () => this.doChangeWO(this.state.selectedItem, 'DISPATCH'),
        },
        overrideBackPress: true,
      });
    });
  }

  searchFilterFunction = (text) => {
    this.searchedText = text;

    let filteredArray = this.initialWOList.filter(item => {
      if (item.order_id.includes(text)) {
        return item;
      } else if (item.cir.includes(text)) {
        return item;
      } else if (item.conn_no.includes(text)) {
        return item;
      }
    });

    this.setState({ workORdersLists: filteredArray })
  };

  renderRadioButton = (i, obj) => {
    if (obj !== '') {
      return (
        <RadioButton key={i}>
          <RadioButtonInput
            obj={obj}
            index={i}
            isSelected={this.state.langValue === i}
            onPress={() => this.onRadioButtonPress(i, obj)}
            buttonInnerColor={this.state.langValue === i
              ? Colors.radioBtnSelected
              : Colors.radioBtnDefault}
            buttonOuterColor={this.state.langValue === i
              ? Colors.radioBtnSelected
              : Colors.radioBtnDefault}
            buttonSize={14}
            buttonStyle={{}}
            buttonWrapStyle={styles.buttonWrapStyle} />
          <View style={{ paddingLeft: 10 }}>
            <Text style={{
              color: Color.black, fontSize: 15,
              fontWeight: 'bold'
            }}>{obj}</Text>
          </View>

        </RadioButton>
      );
    } else {
      return true;
    }
  };

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating /></View>
      );
    }
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  render() {
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (
        <View style={styles.wholeContain}>
          <View style={styles.pendingWOBackView}>
            <View style={styles.dropdowContainer}>
              <View style={styles.sortBytxtView}>
                <Text style={styles.sortTypeTextstyle}>{strings.sortby}</Text>
              </View>
              <View style={styles.modalDropDownView}>
                <View style={styles.modalDropContainer}>
                  <ModalDropdown
                    options={['All', 'Priority', 'SLA Lapse', 'Schedule Time', 'Assigned Time']}
                    style={styles.dropDownMain}
                    dropdownStyle={styles.dropDownStyle}
                    dropdownTextStyle={styles.dropDownTextStyle}
                    dropdownTextHighlightStyle={styles.dropDownTextHighlightStyle}
                    defaultIndex={0}
                    onSelect={(idx, value) => this.sortTypeSelected(idx, value)}>
                    <View style={styles.dropDownSelectedItemView}>
                      <View style={styles.selectedItemTextContainer}>
                        <Text style={styles.sortTypeText}>
                          {this.state.selectedSortToDisplay}
                        </Text>
                      </View>
                      <View style={styles.dropDownIcon}>
                        <Ionicons name='md-arrow-dropdown' size={20} style={styles.iconArror} />
                      </View>
                    </View>
                  </ModalDropdown>
                </View>
              </View>
            </View>
            <FlatList
              disabled={true}
              data={this.state.workORdersLists}
              keyExtractor={(x, i) => i.toString()}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isLoading}
              ListEmptyComponent={
                <View style={{ marginVertical: '55%', alignItems: 'center' }}>
                  <Text style={{ color: Colors.black, fontWeight: 'bold', fontSize: 14 }}>
                    {this.state.locals.noworkoderfound}
                  </Text>
                </View>}
              renderItem={({ item, index }) => (
                <View
                  key={index}
                  style={styles.pendingWoCard}>
                  <View style={styles.topContainer}>
                    <View style={styles.pendingWocardTopRow}>
                      <View style={styles.leftColTop}>
                        <View style={styles.woIDView}>
                          <Image source={this.getIconImg(item.order_icon)} style={styles.imageorder} />
                        </View>
                        <View style={styles.orderIconView}>
                          <Text style={styles.woIDText}>{item.order_id}</Text>
                        </View>
                        <View style={styles.woTypeView}>
                          <Text style={styles.woTypeText}>{item.request_type}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.prioButRow}>
                      <View>
                        <View style={styles.priorityButView}>
                          {item.sla_lapse_code === 0 ?
                            <Text style={[styles.dateText, styles.dateText0]}>{item.sla_lapse}</Text>
                            : <Text></Text>}
                          {item.sla_lapse_code === 1 ?
                            <Text style={[styles.dateText, styles.dateText1]}>{item.sla_lapse}</Text>
                            : <Text></Text>}
                          {item.sla_lapse_code === 2 ? <Text style={[styles.dateText, styles.dateText2]}>{item.sla_lapse}</Text> : <Text></Text>}

                          {item.priority === "Y" ?
                            <Text style={styles.priorityButText}>{strings.PRIORITY}</Text>
                            : <View></View>}
                          {item.enterprise_type != "" ?
                            <Text style={styles.priorityButText}>{item.enterprise_type}</Text>
                            : <View></View>}
                          {item.job_type != "" ?
                            <Text style={styles.woTypeButText}>{item.job_type}</Text>
                            : <View></View>}
                          {item.is_hybrid === "Y" ?
                            <Text style={styles.hybridTypeButText}>{strings.HYBRID}</Text>
                            : <View></View>}
                        </View>
                      </View>
                    </View>
                    {item.cx_name ? (
                      <View style={styles.scheduleTimeRow}>
                        <View style={styles.imageIconContainer}>
                          <Image source={require('../../../images/wom/location.png')} style={styles.imageIconorder} />
                        </View>
                        <View style={styles.contentRow}>
                          <Text style={styles.schTimeText}>{item.cx_name} - {item.cx_address_2 + ' '}{item.cx_address}</Text>
                        </View>
                      </View>) : null}
                    {item.schedule_time ? (
                      <View style={styles.scheduleTimeRow}>
                        <View style={styles.imageIconContainer}>
                          <Image source={require('../../../images/wom/clock.png')} style={styles.imageIconorder} />
                        </View>
                        <View style={styles.contentRow}>
                          <Text style={styles.schTimeText}>{strings.Scheduled_time}</Text>
                          <Text style={styles.schTimeText}>{item.schedule_time}</Text>
                        </View>
                      </View>) : null}
                    {item.cx_requested_time ? (
                      <View style={styles.scheduleTimeRow}>
                        <View style={styles.imageIconContainer}>
                          <Image source={require('../../../images/wom/man-user.png')} style={styles.imageIconorder} />
                        </View>
                        <View style={styles.contentRow}>
                          <Text style={styles.schTimeText}>{strings.Cx_Requested_time}</Text>
                          <Text style={styles.schTimeText}>{item.cx_requested_time}</Text>
                        </View>
                      </View>) : null}
                  </View>
                  <View style={styles.disDetRow}>
                    <View style={styles.botContainer}>
                      <View style={styles.imageLeftCol}>
                        <View>
                          <TouchableOpacity
                            onPress={() => Utill.onMapsPress(item, this.state.googleMapsMessage)} >
                            <Image source={require('../../../images/wom/direction.png')} style={styles.image_c1} />
                          </TouchableOpacity>
                        </View>
                        <View style={styles.callICNBot}>
                          <TouchableOpacity
                            onPress={() => this.toggleModal(true, item.multi_contact, item.contact, item.phone_number_list)}>
                            <Image source={require('../../../images/wom/phonereceiver.png')} style={styles.image_c1} />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>

                    {this.props.woId === "DISPATCHED" || this.props.woId === "IN_PROGRESS" ?
                      <View style={styles.detCol}>
                        <View>
                          <TouchableOpacity
                            onPress={() => this.debounceOnPressDetails(item)}>
                            <Text style={styles.dispatchBut}>{strings.DETAILS}</Text>
                          </TouchableOpacity>
                        </View>
                      </View> :
                      <View style={styles.disdetCol}>
                        <View>
                          <TouchableOpacity onPress={() => this.debounceOnPressDetails(item)}>
                            <Text style={styles.dispatchBut}>{strings.DETAILS}</Text>
                          </TouchableOpacity>
                        </View>
                        <View>
                          <TouchableOpacity onPress={() => this.onClickDispatch(item)}>
                            <Text style={styles.dispatchBut}>{strings.DISPATCH}</Text>
                          </TouchableOpacity>
                        </View>
                      </View>}
                  </View>
                </View>)}
            />
          </View>
          <View style={styles.modalContainer}>
            <Modal isVisible={this.state.isModalVisible}>
              <View style={{ backgroundColor: '#ffffff', height: '30%' }}>
                <View style={{ padding: 20, }}>
                  <View style={{ marginBottom: 5 }}>
                    <Text style={{ color: Colors.black, fontSize: 17 }}>{strings.callMsg}</Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ padding: 8 }}>
                      <View style={styles.radioFormContainer}>
                        <RadioForm animation>
                          {this.state.phoneNumberList.map((obj, i) => this.renderRadioButton(i, obj))}
                        </RadioForm>
                      </View>
                    </View>
                    <View style={styles.cancelCallRow}>
                      <View style={styles.cancelView}>
                        <TouchableOpacity onPress={() => this.toggleModalHide(false)} >
                          <Text style={{ color: Colors.black, fontSize: 16 }}>{strings.cancel}</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.callView}>
                        <TouchableOpacity onPress={() => this.callToMultipleFunc()} >
                          <Text style={{ color: Colors.radioBtnSelected, fontSize: 16 }}>{strings.Call}</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <View style={styles.modalContainer}>
            <Modal isVisible={this.state.isModalVisibleDispatch}>
              <View style={styles.modalInnerContainer}>
                <View style={styles.innerModal}>
                  <View style={styles.alertImageRow}>
                    <Image source={require('../../../images/wom/alert.png')} style={styles.image_alert} />
                  </View>
                  <View style={styles.container}>
                    <View style={styles.workOrderIdRow}>
                      <Text style={styles.workConfirmationText}>{strings.headerTitle} {this.state.ordRID} </Text>
                    </View>
                    <View style={styles.dispatchDiscription}>
                      <Text style={styles.workDesConfrimation}>{strings.dispatchConfirm}</Text>
                    </View>
                    <View style={{
                      flexDirection: 'row', justifyContent: 'flex-end',
                      alignItems: 'flex-end', paddingBottom: 20
                    }}>
                      <View style={styles.cancelView}>
                        <TouchableOpacity onPress={() => this.toggleModalHideDispatch(false)} >
                          <Text style={styles.noButModal}>{strings.no}</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.callView}>
                        <TouchableOpacity onPress={() => this.debounceOndipatchConfirm()} >
                          <Text style={styles.yesButModal}>{strings.yes}</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      );

    } else {
      screenContent = (
        <View style={styles.wholeContain}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />
            }>
          </ScrollView>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.headContiner}>
          <Header
            backButtonPressed={() => this.handleBackButtonClick()}
            searchButtonPressed={() => this.handleSearch()}
            filterButtonPressed={() => this.debounceNavigateFilter()}
            displaySearch
            displayFilter={true}
            headerText={this.state.screenTitle} />
          <SearchBar
            ref={(ref) => this.searchBar = ref}
            onChangeText={(text) => this.searchFilterFunction(text)}
            placeholder={this.state.locals.searchPlaceholder}
            onPressClear={() => {
              this.searchBar.hide();
              this.searchEnabled = false;
              this.setState({ workORdersLists: this.initialWOList })
            }}
          />
        </View>
        {this.showIndicator()}
        <View style={styles.middleContainer}>
          {screenContent}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const pendingWorkOrder = state.pendingWO;
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, pendingWorkOrder, WOM };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  headContiner: {
    flex: 0.1
  },
  middleContainer: {
    flex: 0.9
  },
  topContainer: {
    padding: 8,
  },
  yesButModal: {
    fontSize: 18,
    color: '#ff8329'
  },
  noButModal: {
    fontSize: 18,
    color: '#000000'
  },
  workConfirmationText: {
    fontSize: 18,
  },
  workDesConfrimation: {
    fontSize: 18,
    color: '#000000'
  },
  workOrderIdRow: {
    paddingBottom: 15
  },
  dispatchDiscription: {
    paddingBottom: 25,
    paddingRight: 20
  },
  pendingWocardTopRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#eeeeee',
    paddingBottom: 8,
  },
  pendingWOBackView: {
    backgroundColor: Color.appBackgroundColor,
    flex: 1,
    height: height
  },
  leftColTop: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flex: 1
  },
  image_c1: {
    width: 20,
    height: 20,
  },
  pendingWoCard: {
    borderColor: Colors.borderColorGray, backgroundColor: Colors.white,
    borderRadius: 5, marginVertical: 5, marginHorizontal: 5, shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
  },
  woIDText: {
    fontSize: 16,
    color: Colors.disabled,
    marginLeft: 8,
    marginRight: 8
  },
  woTypeText: {
    fontSize: 15,
    marginLeft: 5,
    fontWeight: 'bold',
    color: '#000000'
  },
  dateText: {
    fontSize: 14,
    textAlign: 'right',
    justifyContent: 'center',
    marginLeft: 8,
    marginRight: 8,
  },
  dateText0: {
    color: '#D0021B'
  },
  dateText1: {
    color: '#FF6D00'
  },
  dateText2: {
    color: '#65AC5B'
  },
  imageIconContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: 30,
    height: 30,
  },
  contentRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scheduleTimeRow: {
    flexDirection: 'row',
    paddingTop: 10,
    marginRight: 10,
    paddingBottom: 5,
  },
  schTimeText: {
    color: '#000000',
    marginRight: 8,
    marginLeft: 5
  },
  disDetRow: {
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 10,
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1
  },
  imageLeftCol: {
    flexDirection: 'row',
    marginLeft: 8,
    marginRight: 8,
    width: (width / 2) - 16
  },
  callICNBot: {
    marginLeft: 8,
    marginRight: 8,
    paddingLeft: 10
  },
  botContainer: {
    paddingLeft: 8,
    paddingRight: 8,
    paddingBottom: 8
  },
  detCol: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1,
    paddingLeft: 8,
    paddingRight: 8
  },
  disdetCol: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 2
  },
  dispatchBut: {
    color: '#ff8e3e'
  },
  prioButRow: {
    flexDirection: 'column',
    paddingTop: 15
  },
  priorityButView: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    marginRight: 10,
  },
  priorityButText: {
    backgroundColor: '#ff0000',
    paddingTop: 5,
    paddingRight: 17,
    paddingBottom: 5,
    paddingLeft: 17,
    color: "#ffffff",
    fontSize: 12,
    borderRadius: 6,
    marginLeft: 8,
  },
  woTypeButText: {
    backgroundColor: '#f46917',
    paddingTop: 5,
    paddingRight: 17,
    paddingBottom: 5,
    paddingLeft: 17,
    color: "#ffffff",
    fontSize: 12,
    borderRadius: 6,
    marginLeft: 8,
  },
  hybridTypeButText: {
    backgroundColor: '#039BE5',
    paddingTop: 5,
    paddingRight: 17,
    paddingBottom: 5,
    paddingLeft: 17,
    color: "#ffffff",
    fontSize: 12,
    borderRadius: 6,
    marginLeft: 8,
  },
  dropdowContainer: {
    flexDirection: 'row',
    margin: 10,
    height: 35
  },
  sortBytxtView: {
    width: ((width - 10) / 4),
    backgroundColor: Color.appBackgroundColor
  },
  modalDropDownView: {
    width: ((width - 20) / 4) * 3,
    backgroundColor: '#ffffff',
    borderColor: Colors.borderColorGray, backgroundColor: Colors.white,
    borderRadius: 2, marginHorizontal: 4, shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
  },
  dropDownIcon: {
    position: 'absolute',
    right: 0,
    marginRight: 25,
    paddingTop: 5
  },
  sortTypeTextstyle: {
    color: '#000000',
    alignItems: 'center',
    fontSize: 20,
    marginTop: 6
  },
  indiView: {
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  wholeContain: {
    height: '100%',
    backgroundColor: Colors.appBackgroundColor
  },
  imageorder: {
    width: 20,
    height: 20,
    marginRight: 10,
    marginLeft: 5,
    borderWidth: 1,
  },
  imageIconorder: {
    width: 20,
    height: 20,
    marginRight: 10,
    marginLeft: 10,
    alignSelf: 'center'
  },
  modalInnerContainer: {
    backgroundColor: '#ffffff',
    height: '40%',
  },
  innerModal: {
    padding: 0
  },
  modalContainer: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  radioFormContainer: {
    height: '50%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  cancelCallRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingBottom: 15
  },
  cancelView: {
    marginRight: 25,
  },
  alertImageRow: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15
  },
  image_alert: {
    width: 50,
    height: 50
  },
  dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  dropDownMain: {
    width: Dimensions.get('window').width * 0.7,
  },
  dropDownStyle: {
    flex: 1,
    width: Dimensions.get('window').width * 0.71,
    height: 160
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropDownSelectedItemView: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  },
  selectedItemTextContainer: {
    width: '90%',
    paddingLeft: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sortTypeText: {
    flex: 1,
    color: Colors.black,
    fontSize: 20,
    marginTop: 5,
    paddingLeft: 8,
  },
  dropDownIcon: {
    width: '10%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
});

export default connect(mapStateToProps, actions)(PendingWOdersView);


