import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import strings from './../../Language/Wom';
import Colors from '../../config/colors';
import { Header } from './Header';
class NetworkView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      jobHistory: [],
      refresh: false,
      locals: {
        no_network: strings.no_network,
        no_network_sub: strings.no_network_sub
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  retryOption = () => {
    this.props.retryFunc()
  }
  onRefresh() {
    this.retryOption();
    this.setState({ refresh: false });

  }
  RetryButton() {
    const { metarialInput, imageContainer1, iconStyle } = styles;
    return (
      <View style={{ height: 50, width: 100, paddingTop: 10 }}>
        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.retryOption()}>
          <Text style={{ fontWeight: 'bold', color: Colors.btnLabel }}>
            RETRY
    </Text>
        </TouchableOpacity>
      </View>
    );
  }

  NetworkText() {
    return (
      <View style={styles.viewCon}>
        <View style={styles.topView}>
          <Text style={styles.toptxt}> {this.state.locals.no_network}</Text>

        </View>
        <View style={styles.bottomView}>
          <Text style={styles.bottomtxt}>{this.state.locals.no_network_sub}</Text>
        </View>
      </View>
    );
  }

  render() {
    return (

      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.props.screenTitle} />
        <ScrollView style={{ height: '100%' }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refresh}
              onRefresh={this.onRefresh.bind(this)}
            />
          }>
          <View>
            {this.NetworkText()}
          </View>
          <View style={{ paddingTop: 25, alignItems: 'center' }}>
            {this.RetryButton()}
          </View>
        </ScrollView>
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Oder ===> NetworkView');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: Colors.appBackgroundColor
  },
  viewCon: {
    flex: 1,
    paddingTop: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 5,
  },
  topView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomView: {
    paddingTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  toptxt: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
  bottomtxt: {
    textAlign: 'center',
    fontSize: 15,
    color: Colors.black
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
  },
});

export default connect(mapStateToProps, actions)(NetworkView);


