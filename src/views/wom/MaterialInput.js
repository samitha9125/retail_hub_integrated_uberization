import React, { Component } from 'react';
import { TextField } from 'react-native-material-textfield';
import { fontSizes, materialTextFieldStyle } from '../../config/stylescfss';

export default class MaterialInput extends Component {
  // TODO: apply translation
  render() {
    return (
      <TextField
        activeLineWidth={materialTextFieldStyle.activeLineWidth}
        textColor={materialTextFieldStyle.textColor}
        tintColor={materialTextFieldStyle.tintColor}
        titleFontSize={fontSizes.body1}
        labelFontSize={fontSizes.body1}
        {...this.props}
      />
    );
  }
}