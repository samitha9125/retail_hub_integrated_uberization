import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
    Keyboard,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation'

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import { Header } from '../Header';
import strings from '../../../Language/Wom';

const { height, width } = Dimensions.get('window');
const maskRowHeight = Math.round((height - 300) / 5);
const maskColWidth = (width - 300) / 2;

const VIBRATION_DURATION = 200;
class WOMBarcodeScannerView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      simBackMsg: strings.simBackMsg,
      barcodeValue: null,
      continue: strings.continueButtonText,
      locals: {
        permission_to_use_camera: strings.permission_to_use_camera,
        permission_to_use_camera_message: strings.permission_to_use_camera_message,
      },
      uniqueValue: 0,
    };
    this.navigatorOb = this.props.navigator;
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    Orientation.lockToPortrait()
  }

  onHandleBackButton = () => {
    this.goBack();
    return true;
  }

  goBack = () => { this.navigatorOb.pop({ animated: true, animationType: 'fade' }); }

  forceRemount = () => {
    this.setState(({ uniqueValue }) => ({
      uniqueValue: uniqueValue + 1
    }));
  }

  async handleContinueClick() {
    this.props.successCb(this.state.barcodeValue);
    Keyboard.dismiss();
    this.goBack();
  }

  onBarCodeReadCb = (e) => {
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e
      .data
      .toString();

    console.log(`xxx onBarCodeReadCb :${last8Digit}`);
    this.setState({ barcodeValue: last8Digit });
    console.log("barcodeState", this.state.barcodeValue);
    return;
  };

  onChangeText = (value) => {
    this.setState({ barcodeValue: value });
  };

  handleClick() { this.onHandleBackButton(); }

  render() {
    const {
      title,
      scannerTopMessage,
      scannerBottomMessage,
      scannerBottomMessage2,
      btnLabel
    } = this.props;

    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.handleClick()}
          headerText={title} />
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.cameraView}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => this.onBarCodeReadCb(e)}
        >
        </RNCamera>
        <View style={styles.maskOutter}>

          <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]}
          />
          <View style={[{ flex: 30 }, styles.maskCenter]}>
            <View style={[{ width: maskColWidth }, styles.maskFrame]} />
            <View style={styles.maskInner} />
            <View style={[{ width: maskColWidth }, styles.maskFrame]}
            />

          </View>
          <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
        </View>
        <View style={{ top: 75, left: '30%', position: 'absolute' }}>
          <Text style={{ color: Colors.appBackgroundColor }}>{scannerTopMessage}</Text>
        </View>

        <View style={{ bottom: 150, left: '25%', position: 'absolute' }}>
          <Text style={{ color: Colors.appBackgroundColor }}>{scannerBottomMessage}</Text>
          <Text style={{ color: Colors.appBackgroundColor }}>{scannerBottomMessage2}</Text>
        </View>
        <View style={[styles.barcodeOverlay, styles.barcodeBottomOverlay]}>
          <View style={styles.containerBottomBarcode}>
            <View style={styles.barcodeInputContainer}>
              <TextInput
                style={styles.barcodeTxtInput}
                underlineColorAndroid='transparent'
                keyboardType={'numeric'}
                value={this.state.barcodeValue}
                placeholder='Enter serial'
                autoFocus={false}
                returnKeyType={'done'}
                selectionColor={'yellow'}
                onChangeText={(value) => this.setState({ barcodeValue: value })} />
            </View>
            <TouchableOpacity
              style={styles.barcodeContinueBtn}
              onPress={() => this.handleContinueClick()}>
              <Text style={styles.barcodeContinueBtnTxt}>
                {btnLabel}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  const sim_validation = state.sim.sim_validation;

  return { Language, simApiFail: state.sim.sim_api_fail, sim_number: state.mobile.sim_number, sim_validation };
};
const styles = StyleSheet.create({
  container: { flex: 1 },
  containerBarcodeScan: { flex: 1 },
  cameraView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  maskInner: {
    width: 300,
    backgroundColor: Colors.colorTransparent,
    borderColor: Colors.colorPureYellow,
    borderWidth: 1,
  },
  maskFrame: { backgroundColor: Colors.colorBlack },
  maskRow: { width: '100%' },
  maskCenter: { flexDirection: 'row' },
  containerBottomBarcode: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackgroundColor
  },
  barcodeOverlay: {
    position: 'absolute',
    right: 0,
    left: 0,
    alignItems: 'flex-start'
  },
  barcodeBottomOverlay: {
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  barcodeInputContainer: {
    flex: 1,
    margin: 10,
  },
  barcodeTxtInput: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  barcodeContinueBtn: {
    flex: 1,
    margin: 10,
    marginRight: 7,
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, actions)(WOMBarcodeScannerView);