import React from 'react';
import {
    StyleSheet,
    View,
    BackHandler,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import Communications from 'react-native-communications';

import * as actions from '../../../actions';
import Modal from "react-native-modal";
import { Header } from '../Header';
import strings from './../../../Language/Wom';
import Color from '../../../config/colors';
import { colors } from '../../../config/stylescfss';

var { width } = Dimensions.get('window');
class DetailDisplayView extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            detailArrRec: this.props.WOM.work_order_detail.summary,
            jobStatus: this.props.WOM.work_order_detail.job_status,
            woType: this.props.WOM.work_order_detail.wo_type,
            visitCount: '',
            appFlow: '',
            jobHistory: [],
            screenTitle: '',
            locals: {
                cancel: strings.cancel,
                pendingTitle: strings.pendingTitle,
                dispatchTitle: strings.dispatchTitle,
                inProgressTitle: strings.inProgressTitle,
                lblBtnOk: strings.btnOk
            }
        };
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        var screenTitle = this.getTitle(this.props.WOM.work_order_detail.job_status);
        let visitCount = ''
        let jobHistory = []

        if (this.props.WOM.work_order_detail.wo_type == "TRB") {           
            visitCount = this.props.WOM.job_history_details.visitCount ? this.props.WOM.job_history_details.visitCount : ''
            jobHistory = this.props.WOM.job_history_details.jobHistory ? this.props.WOM.job_history_details.jobHistory : []
        }

        this.setState({ screenTitle: screenTitle, visitCount, jobHistory });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.props.navigator.pop();
        return true;
    }

    showHistory = () => {
        this.props.navigator.push({
            screen: 'DialogRetailerApp.views.VisitHistory',
            passProps: {
                history: this.state.jobHistory
            }
        });
    }

    getTitle(status) {
        let viewTiltle = this.state.locals.pendingTitle;
        if (status == "DISPATCHED") {
            viewTiltle = this.state.locals.dispatchTitle;
        } else if (status == "IN_PROGRESS") {
            viewTiltle = this.state.locals.inProgressTitle;
        }
        return viewTiltle;
    }

    numberclicked = (value) => {
        Communications.phonecall(value, true);
    }

    contactItem = (item) => {
        return (
            <TouchableOpacity onPress={() => this.numberclicked(item)}>
                <Text style={styles.highlightTxt}>{item}</Text>
            </TouchableOpacity>
        );
    }

    renderItem = ({ item, index }) => {
        let itemContent = true;
        let dot = true;
        let rightSideContent = true;

        if (((item.displayName || item.name) && item.value !== '') || item.fieldType === 'visit') {
            dot = (
                <Text> : </Text>
            );
        }

        if (item.fieldType === 'address') {
            if (item.value.length > 0) {
                rightSideContent = (
                    <View>
                        {item.value.map(addressLine => {
                            return (
                                <Text selectable={true} style={styles.nameValText}>{addressLine}</Text>
                            );
                        })}
                    </View>
                );
            }
        } else if (item.fieldType === 'contact') {
            rightSideContent = (
                <View>
                    {item.value.map((value, index) => {
                        return this.contactItem(value, index);
                    })}
                </View>
            );
        } else if (item.fieldType === 'visit') {
            rightSideContent = (
                <TouchableOpacity onPress={() => this.showHistory()}>
                    <Text style={styles.highlightTxt}>{this.state.visitCount}</Text>
                </TouchableOpacity>
            );
        } else {
            rightSideContent = (
                <Text selectable={true} style={styles.nameValText}>{item.value}</Text>
            );
        }

        if (item.value != "" || (item.fieldType === 'visit' && this.state.visitCount != "")) {
            itemContent = (
                <View style={styles.detailsRow}>
                    <View style={styles.labelCol}>
                        <Text>{item.displayName ? item.displayName : item.name ? item.name : ''}</Text>
                    </View>
                    {dot}
                    <View style={styles.labelColRight}>
                        {rightSideContent}
                    </View>
                </View>
            );
        }

        return (
            <View key={index}>{itemContent}</View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', marginBottom: 20 }}>
                <Header
                    backButtonPressed={() => this.props.navigator.pop()}
                    headerText={this.state.screenTitle} />
                <View style={styles.contentContainer}>
                    <ScrollView>
                        <View style={styles.detailsContainer}>
                            <FlatList
                                disabled={true}
                                data={this.state.detailArrRec}
                                keyExtractor={(x, i) => i}
                                renderItem={this.renderItem}
                                extraData={this.state}
                            />
                        </View>
                    </ScrollView>
                </View>
            </View >
        );
    }
}

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        backgroundColor: Color.appBackgroundColor
    },
    detailsContainer: {
        flex: 1,

    }, topContainer: {
        flex: 0.05

    }, middleContainer: {
        flex: 1.2
    }, bottomContainer: {
        flex: 0.15
    },
    detailsRow: {
        flexDirection: 'row',
        marginTop: 10
    },
    labelCol: {
        width: (width / 2) - 40,
        paddingLeft: 15
    },
    labelColRight: {
        width: (width / 2) + 30,
        paddingLeft: 15
    },
    nameValText: {
        color: '#000000',
        fontSize: 16
    },
    highlightTxt: {
        color: '#0000EE', textDecorationLine: 'underline', fontSize: 16
    },
    direbutRow: {
        flexDirection: 'row',
        marginLeft: 10,
        paddingTop: 10,
        borderTopColor: '#eeeeee',
        borderTopWidth: 1,
        backgroundColor: colors.appBackgroundColor
    },
    image_c1: {
        width: 25,
        height: 25,
    },
    filterText: {
        width: 130,
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        backgroundColor: '#ffc400',
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center',
        marginRight: 10
    },
    unDisBut: {
        marginRight: 20,
        color: '#000000',
        paddingTop: 10,
        fontSize: 16
    },
    resolveRow: {
        flexDirection: 'row'
    },
    resolveArea: {
        top: -3,
        marginTop: -5,
        marginBottom: -5,
    },
    resolveBut: {
        marginLeft: 20,
        color: Color.colorGreen,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        borderWidth: 1,
        borderRadius: 5,
    },
    proceedBtn: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#ffc400',
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center',
        marginRight: 10
    },
    modalContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    modalInnerContainer: {
        backgroundColor: '#ffffff'
    },
    innerModal: {
        padding: 20
    },
    workOrderIdRow: {
        paddingBottom: 15
    },
    dispatchDiscription: {
        paddingBottom: 25,
        paddingRight: 20
    },
    workDesConfrimation: {
        fontSize: 18,
        color: '#000000'
    },
    cancelCallRow: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingBottom: 15
    },
    cancelView: {
        marginRight: 25
    },
    alertImageRow: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    image_alert: {
        width: 50,
        height: 50
    },
    noButModal: {
        fontSize: 18,
        color: '#000000'
    }

});

export default connect(mapStateToProps, actions)(DetailDisplayView);


