import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';

import strings from './../../../Language/Wom';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import imageErrorsAlert from '../../../../images/common/error_msg.png';
import * as actions from '../../../actions';
class WomModalFailure extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      apiData: {},
      locals: {
        lblOK: strings.btnOk
      }
    };
  }

  onBnPressOk = () => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  renderAdditionalInformation = ({ item }) => {
    return (
      <View style={styles.additionalInformationRow}>
        <Text style={styles.additionalInformationText}>{item.label}</Text>
        <Text style={styles.additionalInformationText}>: {item.value}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageErrorsAlert} style={styles.alertImageStyle} />
            </View>
            <View style={styles.descriptionContainer}>
              {this.state.isLoading ? <ActIndicator animating />
                : true}
              <View>
                <Text style={styles.stockId}>{this.props.Header}</Text>
              </View>
              { this.props.showAdditionalInformation ?
                  <View style={{ margin: 10 }}>
                    <FlatList
                      data={this.props.additionalInformation}
                      renderItem={this.renderAdditionalInformation}
                      scrollEnabled={true}
                      extraData={this.state}
                    />
                  </View>
                  : true }
              <View >
                <Text style={styles.descriptionText}>{this.props.descriptionText}</Text>
              </View>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <View style={styles.dummyView} />
              <TouchableOpacity
                onPress={() => this.onBnPressOk()}
                style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {this.state.locals.lblOK}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.5
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    flex: 0.7,
    alignItems: 'center',
    padding: 5
  },

  alertImageStyle: {
    width: 90,
    height: 90,
    marginTop: 5,
    marginBottom: 5
  },

  descriptionContainer: {
    flex: 1.5,
    flexDirection: 'column',
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 15,
    //marginTop: 10
  },
  descriptionText: {
    fontSize: Styles.delivery.modalFontSize,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontWeight: '100'

  },
  additionalInformationRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  additionalInformationText: {
    flex: 1,
    fontSize: Styles.delivery.modalFontSize,
    marginVertical: 5,
    fontWeight: 'bold'
  },
  stockId: {
    alignItems: 'flex-start',
    fontSize: Styles.delivery.modalFontSize,
    textAlign: 'left',
    color: Colors.colorBlack,
    paddingTop: 5,
    marginLeft: 10,
    marginBottom: 15,
    paddingBottom: 5,
    marginRight: 10,
    marginTop: 20,
    fontWeight: 'bold'
  },

  bottomCantainerBtn: {
    flex: 0.4,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 10
  },

  dummyView: {
    flex: 3,
    justifyContent: 'center'
  },

  bottomBtnCancel: {
    flex: 1,
    justifyContent: 'center'
  },
  bottomBtn: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'center'
  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorRed
  },
  bottomCantainerCancelBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorBlack
  }

});

export default connect(mapStateToProps, actions)(WomModalFailure);