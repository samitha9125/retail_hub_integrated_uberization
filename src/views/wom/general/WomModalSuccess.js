import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';

import strings from '../../../Language/Wom';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import imageSucessAleart from '../../../../images/common/success_msg.png';
import * as actions from '../../../actions';
class WomModalSuccess extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        lblOk: strings.btnOk
      }
    }
  }

  onBnPress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this.props.onApproval();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageSucessAleart} style={styles.alertImageStyle} />
            </View>
            <View>
              <Text style={styles.descriptionText}>{this.props.message}</Text>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity onPress={() => this.onBnPress()} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {this.state.locals.lblOk}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    backgroundColor: Colors.backgroundColorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },
  descriptionText: {
    alignItems: 'flex-start',
    fontSize: Styles.modal.errorDescrptionFontSize,
    color: Colors.colorBlack,
    marginBottom: 20,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'

  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtn: {
    width: 45

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen

  }

});
const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};


export default connect(mapStateToProps, actions)(WomModalSuccess);
