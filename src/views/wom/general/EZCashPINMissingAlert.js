import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import strings from '../../../Language/Wom';
import TextFieldComponent from '../components/TextFieldComponent';
import Utills from '../../../utills/Utills';
import * as actions from '../../../actions';

const Utill = new Utills();
class EZCashPINMissingAlert extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      ezCashPIN: '',
      persistenceData: '',
      locals: {
        ezCashPINMissing: strings.ezCashPINMissing,
        ezCashAccntLbl: strings.ezCashAccntLbl,
        lblEzcashPIN: strings.lblezCashPIN
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    this.getPersistData();
  }

  getPersistData = async () => {
    let persistenceData = await Utill.getPersistenceData();
    await this.setState({ persistenceData: persistenceData });
  }

  dismissModal() {
    this.navigatorOb.dismissModal({
      animated: true, animationType: 'slide-down'
    });
  }

  onPressOk() {
    this.dismissModal();
    this.props.ezCashPIN(this.state.ezCashPIN);
  }

  render() {
    const { container, body, descText, bottomContainer, replaceText } = styles;
    return (
      <View style={container}>
        <View style={body}>
          <Text style={descText}>{this.state.locals.ezCashPINMissing}</Text>
          <TextFieldComponent
            label={this.state.locals.lblEzcashPIN}
            title={this.state.persistenceData.conn + '' + this.state.locals.ezCashAccntLbl}
            maxLength={4}
            keyboardType={'numeric'}
            onChangeText={(text) => this.setState({ ezCashPIN: text })}
          >
          </TextFieldComponent>
          <View style={bottomContainer} >
            {this.state.ezCashPIN.length == 4 ?
              <TouchableOpacity onPress={() => this.onPressOk()}  >
                <Text style={replaceText} >OK</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity disabled={true} onPress={() => this.onPressOk()} >
                <Text style={replaceText} >OK</Text>
              </TouchableOpacity>}
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColor
  },
  alertImageContainer: {
    marginBottom: 24,
    alignItems: 'center',
  },
  alertIcon: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  body: {
    backgroundColor: 'white',
    height: 268,
    marginHorizontal: 40,
    paddingHorizontal: 24,
    paddingBottom: 13,
    paddingTop: 21,
  },
  descText: {
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'left',
    // marginHorizontal: 8,
    marginBottom: 38.5,
  },
  bottomContainer: {
    height: 52,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  cancelText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: 'black',
    marginRight: 17,
  },
  replaceText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: '#009688'
  }
};

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};

export default connect(mapStateToProps, actions)(EZCashPINMissingAlert)

