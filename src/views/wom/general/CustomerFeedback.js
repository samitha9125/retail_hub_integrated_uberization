import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  BackHandler,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from './../ActIndicator';
import strings from '../../../Language/Wom';
import MaterialInput from './../MaterialInput';
import DropdownInput from '../components/DropDownInput';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import Constants from '../../../config/constants';
import { Header } from '../Header';

const Utill = new Utills();
var { width } = Dimensions.get('window');
class CustomerFeedback extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      api_error: false,
      dropdownChanged: false,
      persistenceData: '',
      isSignaturepadRequired: false,
      signatureData: false,
      feedbackOptions: [],
      customerAnswers: [],
      feedbackFormated: false,
      dropDownElements: this.props.dropDownElements,
      locals: {
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        btnSubmit: strings.btnSubmit,
        customerSignature: strings.customerSignature,
        lblError: strings.error,
        lblSuccess: strings.lblSuccess,
        msgFillForm: strings.msgFillForm,
        msgFillSignature: strings.msgFillSignature,
        inProgressTitle: strings.inProgressTitle
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.setState({
      feedbackOptions: this.props.feedbackOptions,
      isSignaturepadRequired: this.props.enableSignaturePad ? this.props.enableSignaturePad : false
    }, () => {
      this.formatFeedbackForm();
      this.getPersistData();
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setState({ feedbackOptions: [], customerAnswers: [] });
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.setState({ feedbackOptions: [], customerAnswers: [] }, () => {
      this.navigatorOb.resetTo({
        title: '',
        screen: 'DialogRetailerApp.views.WomDetailView'
      });
    });
  }

  getPersistData = async () => {
    let persistenceData = await Utill.getPersistenceData();
    await this.setState({ persistenceData: persistenceData });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => { }
      }
    });
  }

  ratingSelected(idx, value, questionId, indexArray) {
    let key = indexArray[idx];
    const answeredArray = this.state.feedbackOptions.map(item => {
      if (item.questionID == questionId) {
        let ans = { ...item, ansKey: key, ansValue: value };
        return ans;
      } else {
        return item;
      }
    })

    this.setState({
      feedbackOptions: answeredArray,
      dropdownChanged: true
    })
  }

  formatFeedbackForm() {
    let feedbackArray = [];

    for (let i = 0; i < this.props.feedbackOptions.length; i++) {
      let question = this.props.feedbackOptions[i].q_text;
      let questionID = this.props.feedbackOptions[i].id;
      let questionType = this.props.feedbackOptions[i].q_type;
      let isMandatory = this.props.feedbackOptions[i].validation;
      let choices = this.props.feedbackOptions[i].options;

      let ansKey = '';
      let ansValue = '';

      let newOb = {
        question,
        questionID,
        questionType,
        isMandatory,
        choices,
        ansKey,
        ansValue
      }
      feedbackArray.push(newOb);
    }

    this.setState({
      feedbackOptions: feedbackArray,
      feedbackFormated: true
    }, () => {
      this.props.workOrderCustomerFeedback(feedbackArray);
    })
  }

  answerEntered(questionId, answer) {
    const answeredArray = this.state.feedbackOptions.map(item => {

      if (item.questionID == questionId) {
        return { ...item, ansValue: answer }
      } else {
        return item;
      }
    })
    this.setState({
      feedbackOptions: answeredArray
    })
  }

  getSelectedAnswerforPicker(questionId, description) {
    const feedbackAnswer = this.state.feedbackOptions;

    let answer = '';
    answer = feedbackAnswer.find((item) => {

      return item.questionID == questionId;
    });
    if (answer) {
      return answer.ansValue;
    } else {
      return '';
    }
  }

  displaySuccessAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => this.gotoHome()
      },
      overrideBackPress: true,
    });
  }

  isValidForm() {
    const enteredAnswers = this.state.feedbackOptions;
    let status;
    let ansStatus;
    let signatureStatus;
    for (const i = 0; i < enteredAnswers.length; i++) {
      if (enteredAnswers[i].isMandatory == 'Y') {
        if (enteredAnswers[i].ansValue !== '') {
          ansStatus = true;
        } else {
          ansStatus = false;
          break;
        }
      }
    }

    if (this.state.isSignaturepadRequired) {
      if (this.state.signatureData) {
        signatureStatus = true;
      } else {
        signatureStatus = false;
      }
    }

    if (this.state.isSignaturepadRequired) {
      if (ansStatus && signatureStatus) {
        status = true;
      } else {
        status = false;
      }
    } else {
      status = ansStatus;
    }

    return status;
  }

  addToCaptureDb = async (txnId, imageId, imageUri) => {
    try {
      console.log('addToCaptureDb ', imageUri);
      const additionalData = {
        customer_nic: this.props.WOM.work_order_detail.nic,
        customer_name: this.props.WOM.work_order_detail.cx_name,
        customer_msisdn: this.props.WOM.work_order_detail.contact,
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        order_id: this.props.WOM.work_order_detail.order_id
      }

      let deviceNUserData = await Utill.getDeviceAndUserData();

      const deviceData = { ...additionalData, ...deviceNUserData };

      const id1 = `${txnId}_${imageId}`;
      const url1 = Utill.createApiUrl('captureSignature', 'dtvInstallation', 'wom');
      const imageFile1 = imageUri.replace('file://', '');
      const uploadSelector1 = 'encrypt'; //TODO

      const options = {
        id: id1,
        url: url1,
        imageFile: imageFile1,
        uploadSelector: uploadSelector1,
        deviceData: JSON.stringify(deviceData)
      };

      const errorCb = (msg) => {
        console.log('addToCaptureDb errorCb');
      };
      const sucessCb = (msg) => {
        console.log('addToCaptureDb sucessCb');
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, sucessCb);
    } catch (e) {
      console.log('addToCaptureDb Image Uploading Failed', e.message);
    }
  }

  displayAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => { }
      },
      overrideBackPress: true,
    });
  }

  isValidSignature() {
    if (this.props.WOM.work_order_signature_data.signatureUri == '' || this.props.WOM.work_order_signature_data.signatureUri == undefined) {
      return false;
    }
    return true;
  }

  submitFeedBack() {
    this.setState({ apiLoading: true }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {

          let phone_number_list = '';
          const noArray = this.props.WOM.work_order_detail.phone_number_list

          if (noArray.length > 0) {
            for (const i = 0; i < noArray.length && i < 2; i++) {
              if (noArray[i] !== '') {
                if (i == 1) {
                  phone_number_list += noArray[i];
                } else {
                  phone_number_list += noArray[i] + ','
                }
              }
            }
          }

          let id;
          let ans;
          let isMandatory;
          let value;

          const customerAnswers = this.state.feedbackOptions.map(item => {
            id = item.questionID;
            ans = item.ansKey;
            isMandatory = item.isMandatory;
            value = item.ansValue;

            let obj = {
              id,
              ans,
              isMandatory,
              value
            }

            return obj;
          });

          const packageobj = this.props.WOM.work_order_detail.summary.find(item => {
            if (item.name == 'Package') {
              return true;
            }
          });

          const accountObj = this.props.WOM.work_order_detail.summary.find(item => {
            if (item.name == 'Account Number') {
              return true;
            }
          });

          let packageValue;

          if (packageobj !== undefined) {
            packageValue = packageobj.value;
          }

          let accountValue;

          if (accountObj !== undefined) {
            accountValue = accountObj.value;
          }

          const data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            answers: customerAnswers,
            breached: this.props.WOM.work_order_detail.breached,
            app_flow: this.props.WOM.work_order_app_flow,
            id: this.props.WOM.work_order_signature_data.signatureId,
            customer_nic: this.props.WOM.work_order_detail.nic,
            customer_name: this.props.WOM.work_order_detail.cx_name,
            customer_msisdn: this.props.WOM.work_order_detail.contact,
            biz_unit: this.props.WOM.work_order_detail.biz_unit,
            request_type: this.props.WOM.work_order_detail.wo_type,
            contact_no: this.props.WOM.work_order_detail.contact_no,
            contact: phone_number_list,
            image_status: 'ok',
            no_of_attempts: 0,
            latitude: loc.latitude,
            longitude: loc.longitude,
            ownership: this.props.WOM.work_order_detail.ownership,
            lob: this.props.WOM.work_order_detail.biz_unit,
            account_no: accountValue,
            package: packageValue,
            nic: this.props.WOM.work_order_detail.nic,
            invoiceNo: this.props.WOM.work_order_detail.product_detail.cpos_invoice_no
          };
          Utill.apiRequestPost('updateUserFeedback', 'Feedback', 'wom', data, this.submitFeedBackSuccess, this.submitFeedBackFailure, this.submitFeedBackEx);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.message, '');
          });
        });
    });
  }

  submitFeedBackSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.success && !response.data.data) {
        this.displaySuccessAlert('successWithOk', this.state.locals.lblSuccess, response.data.info, '');
      } else if (response.data.data.delay_reason) {
        this.setState({ customerAnswers: [] }, () => this.props.navigator.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals.inProgressTitle)));
      }
    })
  }

  submitFeedBackFailure = (response) => {
    this.setState({
      apiLoading: false,
      customerAnswers: []
    }, () => {
      this.navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
    });
  }

  submitFeedBackEx = (error) => {
    this.setState({
      apiLoading: false,
      customerAnswers: []
    }, () => {
      if (error == 'Network Error') {
        this.navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: this.state.locals.network_error_message
          },
          overrideBackPress: true
        });
      } else {
        this.navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: this.state.locals.api_error_message
          },
          overrideBackPress: true
        });
      }
    });
  }

  gotoHome() {
    this.navigatorOb.resetTo({
      title: '',
      screen: 'DialogRetailerApp.views.WomLandingView',
      passProps: {
        isResetWO: true
      }
    });
  }

  renderListItem = ({ item }) => {
    if (this.state.feedbackFormated) {
      if (item.questionType == 'DROPDOWN') {
        let optionsArray = [];
        let indexArray = [];
        for (const i = 0; i < item.choices.length; i++) {
          optionsArray.push(item.choices[i].value);
          indexArray.push(item.choices[i].key);
        }
        if (optionsArray.length) {
          return (
            <DropdownInput
              alignDropDownnormal={true}
              dropDownTitle={item.question}
              hideRightIcon={true}
              selectedValue={this.getSelectedAnswerforPicker(item.questionID, item.question)}
              dropDownData={optionsArray}
              onSelect={(idx, value) => {
                this.ratingSelected(idx, value, item.questionID, indexArray);
              }}
            />
          );
        }
      } else if (item.questionType == 'TEXT') {
        return (
          <MaterialInput
            ref={'contactNoAlt'}
            isNeedToFocus
            label={item.question}
            visible={false}
            maxLength={this.props.maxLength}
            onEndEditing={(text) => this.answerEntered(item.questionID, text.nativeEvent.text)}
          />
        );
      } else if (item.questionType == 'TIME') {
        return (
          <MaterialInput
            ref={'contactNoAlt'}
            isNeedToFocus
            label={item.question}
            visible={false}
            maxLength={this.props.maxLength}
            onEndEditing={(text) => this.answerEntered(item.questionID, text.nativeEvent.text)}
          />
        );
      }
    }
  }

  accessSignatrePad() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.SignaturePadComponentWom',
      passProps: {
        downlodUrl: 'https://www.dialog.lk/tc',
        uploadSignature: (signatureData) => {
          console.log('uploadSignature ', signatureData);
          this.setState({
            signatureData
          });
        }
      }
    });
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        {this.state.apiLoading ?
          <ActIndicator animating={true} />
          : true}
        <ScrollView>
          <View style={styles.topContainer} />
          <View style={styles.middleContainer}>
            <View style={styles.detailsContainer}>
              <FlatList
                data={this.state.feedbackOptions}
                keyExtractor={(x, i) => i}
                renderItem={this.renderListItem.bind(this)}
                scrollEnabled={true}
                extraData={this.state}
              />
            </View>
            {this.state.isSignaturepadRequired ?
              <View style={styles.signatureView}>
                <TouchableOpacity
                  style={styles.signatureContainer}
                  onPress={() => this.accessSignatrePad()}
                  disabled={this.props.simNumber == ''}
                >
                  {this.state.signatureData !== null && this.state.signatureData !== undefined && this.state.signatureData !== false ?
                    <Image
                      style={styles.signatureImage}
                      key={this.state.signatureData.signatureRand}
                      source={{ uri: `data:image/jpg;base64,${this.state.signatureData.signatureBase}` }}
                      resizeMode="stretch"
                    />
                    :
                    <Text style={styles.signatureTxt}>  {this.state.locals.customerSignature} </Text>}
                </TouchableOpacity>
              </View>
              : true}
          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.direbutRow}>
              <View style={styles.dirImageView}>

              </View>
              <View style={styles.filterButView}>
                <View>
                </View>
                <View>
                  {this.isValidForm() ?
                    <TouchableOpacity onPress={() => {
                      this.state.signatureData !== false ?
                        this.addToCaptureDb(this.props.WOM.work_order_detail.order_id, Constants.imageCaptureTypes.SIGNATURE, this.state.signatureData.signatureUri)
                        : true
                      this.submitFeedBack();
                    }}>
                      <Text style={styles.filterText}>{this.state.locals.btnSubmit}</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                      disabled={true}
                      onPress={() => this.submitFeedBack()}>
                      <Text style={styles.filterTextDisabled}>{this.state.locals.btnSubmit}</Text>
                    </TouchableOpacity>}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const signature = state.mobile.signature;
  return { Language, WOM, signature };
};

const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1,
    marginHorizontal: 40
  },
  topContainer: {
    flex: 0.05
  },
  middleContainer: {
    flex: 0.8
  },
  bottomContainer: {
    flex: 0.15
  },
  sortBytxtView: {
    width: (width / 4),
  },
  modalDropDownView: {
    width: (width / 4) * 4,
    backgroundColor: '#ffffff'
  },
  dropDownIcon: {
    position: 'absolute',
    right: 0,
    marginRight: 25,
    paddingTop: 5
  }, sortTypeTextstyle: {
    color: '#000000',
    alignItems: 'center',
    fontSize: 20
  }, direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 20,
    borderTopColor: '#eeeeee',
    marginBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  }, unDisBut: {
    marginRight: 20,
    color: '#000000',
    paddingTop: 10,
    fontSize: 16
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  dropDownMain: {
    marginTop: 16,
    paddingBottom: 16,
    width: '100%',
  },
  dropDownStyle: {
    flex: 1,
    width: Dimensions.get('window').width * 0.9,
    marginTop: 55,
    height: 160
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  }, dropDownSelectedItemView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  }, selectedItemTextContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    borderBottomWidth: 1,
    backgroundColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderLineColorLightGray
  }, sortTypeText: {
    flex: 1,
    color: Colors.black,
    fontSize: 15,
    marginTop: 5,
    fontWeight: 'bold'
  }, sortTypeTextPlaceHolder: {
    flex: 1,
    color: Colors.btnDeactiveTxtColor,
    fontSize: 15,
    marginTop: 5,
  }, signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    borderWidth: 0,
    borderColor: Colors.grey,
    borderRadius: 3,
  },
  signatureTxt: {
    color: Colors.black,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  }, signatureView: {
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: Colors.cardborder,
    height: 100,
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  },
});

export default connect(mapStateToProps, actions)(CustomerFeedback);