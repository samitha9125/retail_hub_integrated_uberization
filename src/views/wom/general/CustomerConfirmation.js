import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  ScrollView,
  BackHandler,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import { Header } from './../Header';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from './../ActIndicator';
import DragComponent from './../../general/Drag';
import strings from '../../../Language/Wom';
import Constants from '../../../config/constants';
import DropDownInput from '../components/DropDownInput';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import TextFieldComponent from '../components/TextFieldComponent';
import globalConfig from '../../../config/globalConfig'

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class CustomerConfirmationWOM extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      payment_Modes: this.props.WOM.work_order_detail.payment_methods,
      paymentModeIndex: [],
      paymentModeOptions: [],
      paymentMode: '',
      selectedPayMode: null,
      ezCashPIN: '',
      signatureData: false,
      selectedPayModeIndex: '',
      persistenceData: '',
      formattedCollection: this.props.dataArray,
      isEzCashBalanceFailure: true,
      locals: {
        api_error_message: strings.api_error_message,
        lblType: strings.lblType,
        lblConnectionNo: strings.lblConnectionNo,
        lblItem: strings.lblItem,
        lblSerialNo: strings.lblSerialNo,
        lblRemWarranty: strings.lblRemWarranty,
        lblPrice: strings.lblPrice,
        customerSignature: strings.customerSignature,
        customerConfirmation: strings.customerConfirmation,
        btnProvision: strings.provisionButton,
        btnConfirm: strings.btnConfirm,
        totalGrand: strings.totalGrand,
        lblPaymentMode: strings.lblPaymentMode,
        free: strings.free,
        ezCashPINText: strings.ezCashPINText,
        network_error_message: strings.network_error_message,
        error: strings.error,
        lblSuccess: strings.lblSuccess,
        lblWorkOrderSuccessSubmitted: strings.lblWorkOrderSuccessSubmitted,
        freeOfCharge: strings.free_of_change,
        lblItemRequest: strings.lblItemRequest,
        lblOffer: strings.lblOffer,
        lblWarrantyPeriod: strings.lblWarrantyPeriod,
        lblWarranty: strings.lblWarranty,
        inProgressTitle: strings.inProgressTitle,
        lblReplacement: strings.lblReplacement,
        lblezCashPIN: strings.lblezCashPIN
      },
    };
    this.debouncebtnOnPress = _.debounce(() => this.btnOnPress(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });   
    this.navigatorOb = this.props.navigator;
    this.provisionAttempts = 1;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.getPersistData();
    this.formatPaymentModes();
    this.formatDatatoDisplay();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.props.getSignatureMobileAct(null);
    this.navigatorOb.pop();
  }

  formatPaymentModes() {
    let paymentModeIndex = [];
    let paymentModeOptions = [];

    this.state.payment_Modes.map((item) => {
      paymentModeIndex.push(item.id);
      paymentModeOptions.push(item.value);
    });

    this.setState({ paymentModeIndex, paymentModeOptions });
  }

  formatDatatoDisplay() {
    console.log('------------formatDatatoDisplay---------');
    let collectionHolder = '';
    let tempArray = [];
    let innerTemp = [];
    let elementTemp = [];
    let elementWiseTemp = [];
    if (this.props.WOM.work_order_serial_list) {
      collectionHolder = this.props.WOM.work_order_serial_list;
    } else {
      collectionHolder = this.state.formattedCollection;
    }

    collectionHolder.map((item) => {
      console.log('collectionHolder item ', item);
      if (Array.isArray(item) && this.props.isRecoveryFlow) {
        tempArray.push(item);
      } else {
        if (item.inner_items) {
          item.inner_items.map((innerItem) => {
            if (innerItem.warranty) {
              let newInner = [
                {
                  label: innerItem.name + ' ' + this.state.locals.lblWarranty,
                  value: innerItem.warranty,
                }
              ];
              innerTemp.push(newInner);
            }
          });
        }

        if (item.elementData) {
          item.elementData.map((element) => {
            if (element.warranty) {
              const elementInner = [
                {
                  label: element.name + ' ' + this.state.locals.lblWarranty,
                  value: element.warranty
                }
              ];
              elementTemp.push(elementInner);
            }
          });
        }

        let newObj;
        if (item.sale_type) {
          let sale_type;
          if (item.sale_type == 'REPLACE') {
            sale_type = 'Replacement';
            if (item.warrantyPeriod) {
              newObj = [
                {
                  label: this.state.locals.lblItemRequest,
                  value: sale_type
                },
                {
                  label: this.state.locals.lblItem,
                  value: item.name
                },
                {
                  label: this.state.locals.lblSerialNo,
                  value: item.serial
                },
                {
                  label: this.state.locals.lblRemWarranty,
                  value: item.warrantyPeriod
                },
                {
                  label: this.state.locals.lblPrice,
                  value: item.price == 0 ? this.state.locals.freeOfCharge : item.price
                }
              ];
            } else {
              newObj = [
                {
                  label: this.state.locals.lblItemRequest,
                  value: sale_type
                },
                {
                  label: this.state.locals.lblItem,
                  value: item.name
                },
                {
                  label: this.state.locals.lblSerialNo,
                  value: item.serial
                },
                {
                  label: this.state.locals.lblPrice,
                  value: item.price == 0 ? this.state.locals.freeOfCharge : item.price
                }
              ];
            }
          } else {
            sale_type = 'Additional sale';
            if (item.offers) {
              if (item.offers.elementwise_details) {
                item.offers.elementwise_details.map((element) => {
                  if (element.warranty) {
                    const elementInner = [
                      {
                        label: element.name + ' ' + this.state.locals.lblWarranty,
                        value: element.warranty
                      }
                    ];
                    elementWiseTemp.push(elementInner);
                  }
                });
              }
            }

            newObj = [
              {
                label: this.state.locals.lblItemRequest,
                value: sale_type
              },
              {
                label: this.state.locals.lblItem,
                value: item.name
              },
              {
                label: this.state.locals.lblSerialNo,
                value: item.serial
              },
              {
                label: this.state.locals.lblOffer,
                value: item.offers.offer_name
              },
              {
                label: this.state.locals.lblPrice,
                value: item.price == 0 ? this.state.locals.freeOfCharge : item.price
              }
            ];
          }
        } else if (this.props.isRemoveList) {
          newObj = [
            {
              label: this.state.locals.lblItem,
              value: item.name
            },
            {
              label: item.name + ' ' + this.state.locals.lblSerialNo,
              value: item.serial
            }
          ];
        } else if (this.props.simstbChange) {
          newObj = [
            {
              label: this.state.locals.lblItemRequest,
              value: this.state.locals.lblReplacement
            },
            {
              label: this.state.locals.lblItem,
              value: item.name
            },
            {
              label: item.name + ' ' + this.state.locals.lblSerialNo,
              value: item.serial
            },
            {
              label: this.state.locals.lblPrice,
              value: item.price == 0 ? this.state.locals.freeOfCharge : item.price
            }
          ];
        } else {
          newObj = [
            {
              label: this.state.locals.lblItem,
              value: item.name
            },
            {
              label: item.name + ' ' + this.state.locals.lblSerialNo,
              value: item.serial
            },
            {
              label: this.state.locals.lblPrice,
              value: item.price == 0 ? this.state.locals.freeOfCharge : item.price
            }
          ];
        }

        innerTemp.map((item) => {
          item.map((item1) => {
            newObj.push(item1);
          });
        });

        elementTemp.map((item) => {
          item.map((item1) => {
            newObj.push(item1);
          });
        });

        elementWiseTemp.map(item => {
          item.map(item1 => {
            newObj.push(item1);
          });
        });
        innerTemp = [];
        elementTemp = [];
        elementWiseTemp = [];
        tempArray.push(newObj);
      }
    });

    this.setState({ formattedCollection: tempArray });
  }
  getPersistData = async () => {
    let persistenceData = await Utill.getPersistenceData();
    await this.setState({ persistenceData: persistenceData });
  }

  async getSelectedPaymentMode(selectedIndex) {
    await this.setState({
      paymentMode: this.state.paymentModeOptions[selectedIndex],
      selectedPayMode: this.state.paymentModeOptions[selectedIndex],
      selectedPayModeIndex: this.state.paymentModeIndex[selectedIndex],
      ezCashPIN: '',
    });
  }

  accessSignatrePad() {
    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.SignaturePadComponentWom',
      passProps: {
        downlodUrl: 'https://www.dialog.lk/tc',
        uploadSignature: (signatureData) => {
          this.setState({ signatureData });
        }
      }
    });
  }

  removalConfirm() {
    const data = {
      dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
      job_id: this.props.WOM.work_order_detail.job_id,
      app_flow: 'DTV_REMOVAL',
      cir: this.props.WOM.work_order_detail.cir,
      order_id: this.props.WOM.work_order_detail.order_id,
      request_type: "EQU_REMOVE",
      removal_items: this.props.RemovalItem
    };
    Utill.apiRequestPost('confirm', 'ConnectionRemoval', 'wom', data, this.connectionRemovalSuccessCB, this.confirmFailureCB, this.confirmExCB);
  }

  connectionRemovalSuccessCB = (response) => {
    this.setState({ apiLoading: false }, () => this.gotoFeedbackPage(response));
  }

  recoveryConfirm() {
    const data = {
      dtvAccNo: this.props.WOM.work_order_detail.conn_no,
      job_id: this.props.WOM.work_order_detail.job_id,
      cir: this.props.WOM.work_order_detail.cir,
      app_flow: this.props.WOM.work_order_detail.app_flow,
      order_id: this.props.WOM.work_order_detail.order_id,
      request_type: this.props.WOM.work_order_detail.wo_type,
      recoverd_items: this.props.RecoveryItem,
      ownership: this.props.WOM.work_order_detail.ownership
    };
    Utill.apiRequestPost('Recovery', 'Recovery', 'wom', data, this.recoverySuccessCB, this.confirmFailureCB, this.confirmExCB);
  }

  recoverySuccessCB = (response) => {
    this.setState({ apiLoading: false }, () => this.gotoFeedbackPage(response));
  }

  confirmFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('provisionFailed', response.data.error, response.data.serials, '', '');
    });
  }

  confirmExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }


  gotoFeedbackPage(response) {
    this.props.getSignatureMobileAct(null);
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CustomerFeedback',
      passProps: {
        displayRefresh: true,
        screenTitle: this.state.locals.inProgressTitle,
        feedbackOptions: response.data.data.feedback_question,
        isSignaturepadRequired: false
      }
    });
  }

  btnConfirmActions() {
    this.setState({ apiLoading: true }, () => {
      switch (this.props.WOM.work_order_app_flow) {
        case 'DTV_REMOVAL':
          this.removalConfirm();
          break;

        case 'DTV_RECOVERY':
          this.recoveryConfirm();
          break;

        case 'DTV_ACCESSORY_DELV': //Accessory Delivery Confirm
        case 'DTV_NEW_CX':
        case 'DTV_NEW_DTL':
        case 'DTV_NEW_SECONDARY_CX':
        case 'FEATURE_HD':
        case 'FEATURE_HD_CX':
        case 'DTV_RELOCATION_REFIX':
        case 'DTV_REFIX':
        default:
          this.doProvision();
          break;
      }
    });
  }

  doProvision() {
    let provAttempts = this.provisionAttempts;

    const data = {
      app_flow: this.props.WOM.work_order_app_flow,
      cir: this.props.WOM.work_order_detail.cir,
      dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
      order_id: this.props.WOM.work_order_detail.order_id,
      job_id: this.props.WOM.work_order_detail.job_id,
      ccbs_order_id: this.props.WOM.work_order_detail.ccbs_order_id,
      request_type: this.props.WOM.work_order_detail.wo_type,
      ownership: this.props.WOM.work_order_detail.ownership,
      serial_list: this.props.WOM.work_order_serial_list,
      retry_attempt: provAttempts,
      pay_amount: this.props.totalGrand,
      pay_mode: this.state.selectedPayModeIndex,
      product_detail: this.props.WOM.work_order_detail.product_detail,
      biz_unit: this.props.WOM.work_order_detail.biz_unit,
      sub_area: this.props.WOM.work_order_detail.sub_area,
      ezcash_pin: this.state.ezCashPIN,
      conn_type: this.props.WOM.work_order_detail.conn_type,
      contactNo: this.props.WOM.work_order_detail.contact
    };

    if (this.props.WOM.work_order_detail.job_type == 'REFIX' && this.props.WOM.work_order_refix_warranty_details) {
      data.warranty_list = this.props.WOM.work_order_refix_warranty_details;
    }

    provAttempts = provAttempts + 1;
    this.provisionAttempts = provAttempts;
    Utill.apiRequestPost('ProvisioningInstallation', 'DtvProvision', 'wom', data, this.doProvisionSuccessCB, this.doProvisionFailureCB, this.doProvisionExCB);
  }

  doProvisionSuccessCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.warning_msg == true) {
        this.navigatorOb.push({
          title: 'IN-PROGRESS WORKORDER',
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.error,
            messageBody: response.data.error,
            messageFooter: '',
            onPressOK: () => {
              this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
            }
          }
        });
      }

      const details = {
        ezcash_pin: this.state.ezCashPIN,
        pay_mode: this.state.selectedPayModeIndex,
        pay_amount: this.props.totalGrand
      };

      this.props.setPaymentDetails(details);

      if (response.data.data.app_screen) {
        this.props.workOrderAppScreen(response.data.data.app_screen);
        this.props.getSignatureMobileAct({});
        this.props.workOrderSignatureData({});
        this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
      } else {
        this.gotoProvStatus(response);
      }
    });
  }

  doProvisionFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.retry == true) {
        this.displayRetryProvisionAlert('failureWithRetry', '', response.data.error, '');
      } else {
        this.displayCommonAlert('provisionFailed', response.data.error, response.data.serials, '', '');
      }
    });
  }

  displayRetryProvisionAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressRetry: () => { this.doProvision(); }
      },
      overrideBackPress: true
    });
  }

  doProvisionExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }

  gotoProvStatus = (response) => {
    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.DTVProvisionStatus',
      passProps: {
        provStatus: response.data.data.status,
        provisionedStatusData: response.data.data.serial_list,
        displayRefresh: true,
        screenTitle: this.state.locals.inProgressTitle
      }
    });
  }

  addToCaptureDb = async (txnId, imageId, imageUri) => {
    console.log('addToCaptureDb ');

    try {
      const additionalData = {
        customer_nic: this.props.WOM.work_order_detail.nic,
        customer_name: this.props.WOM.work_order_detail.cx_name,
        customer_msisdn: this.props.WOM.work_order_detail.contact,
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        order_id: this.props.WOM.work_order_detail.order_id
      }
      let deviceNUserData = await Utill.getDeviceAndUserData();

      const deviceDataObj = { ...additionalData, ...deviceNUserData };

      const deviceDataString = JSON.stringify(deviceDataObj);
      const id1 = `${txnId}_${imageId}`;
      const url1 = Utill.createApiUrl('captureSignature', 'dtvInstallation', 'wom');
      const imageFile1 = imageUri.replace('file://', '');
      const uploadSelector1 = 'encrypt'; //TODO

      const options = {
        id: id1,
        url: url1,
        imageFile: imageFile1,
        uploadSelector: uploadSelector1,
        deviceData: deviceDataString
      };

      const errorCb = (msg) => {
        console.log('addToCaptureDb errorCb');
      };
      const sucessCb = (msg) => {
        console.log('addToCaptureDb sucessCb');
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, sucessCb);
    } catch (e) {
      console.log('addToCaptureDb Image Uploading Failed', e.message);
    }
  }

  onEzCashPINChange = (text) => {
    this.setState({ ezCashPIN: text }, () => {
      if (this.state.ezCashPIN.length == 4) {
        Keyboard.dismiss();
        this.checkEzCashBalance();
      }
    });
  }

  checkEzCashBalance() {
    const data = {
      ezcash_pin: this.state.ezCashPIN,
      app_flow: this.props.WOM.work_order_app_flow,
      cir: this.props.WOM.work_order_detail.cir,
      dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
      order_id: this.props.WOM.work_order_detail.order_id,
      job_id: this.props.WOM.work_order_detail.job_id,
      request_type: this.props.WOM.work_order_detail.wo_type,
      ownership: this.props.WOM.work_order_detail.ownership,
      sub_area: this.props.WOM.work_order_detail.sub_area,
      price: this.props.totalGrand
    };

    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('checkEzCashBalance', 'EzCash', 'wom', data,
        this.checkBalanceSuccessCb, this.checkBalanceFailureCb, this.checkBalanceExCb);
    });
  }

  checkBalanceSuccessCb = (response) => {
    this.setState({
      apiLoading: false,
      isEzCashBalanceFailure: false
    });
  }

  checkBalanceFailureCb = (response) => {
    this.setState({
      apiLoading: false,
      ezCashPIN: '',
      isEzCashBalanceFailure: true
    }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.error, response.data.error, '');
    });
  }

  checkBalanceExCb = (res) => {
    this.setState({
      apiLoading: false,
      isEzCashBalanceFailure: true
    }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  isValidForm() {
    let signatureEnabled = this.props.isSignatureRequired;
    let paymentEnabled = this.props.isPaymentAvailable;

    if (signatureEnabled == true && (this.state.signatureData == false || this.state.signatureData == undefined || this.state.signatureData == null)) {
      return false;
    } else if (paymentEnabled == true && (this.state.selectedPayMode == null || this.state.selectedPayMode == undefined) && this.props.totalGrand != 'NaN') {
      return false;
    } else if (this.state.selectedPayModeIndex == 'ez_cash' && (this.state.ezCashPIN == '' || this.state.ezCashPIN.length > 4 || this.state.ezCashPIN.length < 4) && this.state.isEzCashBalanceFailure == true) {
      return false;
    } else if (this.state.isEzCashBalanceFailure == true && this.state.ezCashPIN.length == 4) {
      return false;
    }
    else return true;
  }

  btnOnPress() {
    this.props.isSignatureRequired ?
      this.addToCaptureDb(this.props.WOM.work_order_detail.order_id, Constants.imageCaptureTypes.SIGNATURE, this.state.signatureData.signatureUri)
      : true;
    this.btnConfirmActions();
  }

  elementDotView = () => (
    <View style={styles.elementMiddle}>
      <View>
        <Text>:    </Text>
      </View>
    </View>
  );

  renderIDType = () => {
    if (this.props.WOM.work_order_detail.id_type !== '') {
      return (
        <View style={styles.detailElimentContainer}>
          <View style={styles.elementLeft}>
            <Text style={styles.contentContainerLbl}>
              {this.props.WOM.work_order_detail.id_type}
            </Text>
          </View>
          {this.elementDotView()}
          <View style={styles.elementRight}>
            <Text style={styles.contentContainerTxt}>
              {this.props.WOM.work_order_detail.nic}
            </Text>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }

  renderConnType = () => {
    if (this.props.WOM.work_order_detail.conn_type !== '') {
      return (<View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>

          <Text style={styles.contentContainerLbl}>
            {this.state.locals.lblType}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>
            {this.props.WOM.work_order_detail.conn_type}
          </Text>
        </View>
      </View>
      );
    } else {
      return null;
    }
  }

  renderConn = () => {
    return (
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerLbl}>
            {this.state.locals.lblConnectionNo}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>
            {this.props.WOM.work_order_detail.conn_no}
          </Text>
        </View>
      </View>
    );
  }

  renderListItem = ({ item, index }) => {
    if (item.label != '' || item.value != '') {
      return (
        <View key={index} style={styles.detailElimentContainer}>
          <View style={styles.elementLeft}>
            <Text style={styles.contentContainerLbl}>
              {item.label}
            </Text>
          </View>
          {this.elementDotView()}
          <View style={styles.elementRight}>
            <Text style={styles.contentContainerTxt}>
              {item.value}
            </Text>
          </View>
        </View>
      );
    }
    return (<View />);
  }
  renderAccessoryDeliveryListItem = ({ item, index }) => (
    <View key={index} style={{ paddingBottom: 10 }}>
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerLbl}>
            {item.label1}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>: {item.value1}</Text>
        </View>
      </View>
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerLbl}>
            {item.label2}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>: {item.value2}</Text>
        </View>
      </View>
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerLbl}>
            {item.label3}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>: {item.value3}</Text>
        </View>
      </View>
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerLbl}>
            {item.label4}
          </Text>
        </View>
        {this.elementDotView()}
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>: {item.value4}</Text>
        </View>
      </View>
    </View>
  )

  render() {
    let btnText;
    if (this.props.WOM.work_order_detail.job_type == 'RECOVERY') {
      btnText = this.state.locals.btnConfirm;
    }
    else if (this.props.WOM.work_order_detail.wo_type == 'TRB' || this.props.WOM.work_order_detail.wo_type == 'REFIX' || this.props.WOM.work_order_detail.wo_type == 'TECH_SUPPORT' || this.props.WOM.work_order_detail.wo_type == 'RELOCATION' || this.props.WOM.work_order_detail.wo_type == 'ACCESSORY_DELV') {
      let provItem = this.props.dataArray.find((element) => {
        console.log('ProvionItem::', this.state.formattedCollection)
        if (element.provisionable == 'Y') {
          return element;
        }
      });
      if (provItem !== undefined) {
        btnText = this.state.locals.btnProvision;
      }
      else {
        btnText = this.state.locals.btnConfirm;
      }
    }
    else {
      btnText = this.state.locals.btnProvision;
    }

    let renderItem;
    return (
      <View style={{ flex: 1, backgroundColor: Colors.backgroundColorWhiteWithAlpa }}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        {this.state.apiLoading ? <View style={styles.indiView}><ActIndicator /></View> : true}
        <View style={{ flex: 1 }} >
          <ScrollView>
            <View style={styles.topContainer}>
              <Text style={styles.titleText}>
                {this.state.locals.customerConfirmation}
              </Text>
            </View>
            <View style={styles.middleContainer}>
              {this.props.isRemoveList ?
                renderItem = this.renderListItem
                :
                [this.props.isAccessoryDeliveryList ?
                  renderItem = this.renderAccessoryDeliveryListItem
                  :
                  renderItem = this.renderListItem]}
              <View style={{ marginTop: 16 }}>
                {this.renderIDType()}
                {this.renderConnType()}
                {this.renderConn()}
              </View>
              {this.state.formattedCollection.map((item) => {
                return (
                  <View style={{ marginTop: 16, marginBottom: 16 }}>
                    <FlatList
                      data={item}
                      keyExtractor={(x, i) => i.toString()}
                      renderItem={renderItem}
                      scrollEnabled={true}
                      extraData={this.state}
                    />
                  </View>
                );
              })}
              {this.props.isPaymentAvailable && this.props.totalGrand != 'NaN' ?
                <View style={{ margin: 8, marginTop: 16, flexDirection: 'column' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locals.totalGrand}
                      </Text>
                    </View>
                    {this.elementDotView()}
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>Rs. {this.props.totalGrand}</Text>
                    </View>
                  </View>
                  <View>
                    <DropDownInput
                      alignDropDownnormal={true}
                      dropDownTitle={this.state.locals.lblPaymentMode}
                      hideRightIcon={true}
                      selectedValue={this.state.paymentMode}
                      dropDownData={this.state.paymentModeOptions}
                      onSelect={selectedIndex => {
                        this.getSelectedPaymentMode(selectedIndex);
                      }}
                    />
                  </View>
                  {this.state.paymentMode != '' && this.state.selectedPayModeIndex != 'add_bill' ?
                    <TextFieldComponent
                      label={this.state.locals.lblezCashPIN}
                      title={this.state.persistenceData.conn + ' ' + this.state.locals.ezCashPINText}
                      maxLength={4}
                      keyboardType={'numeric'}
                      onChangeText={(text) => this.onEzCashPINChange(text)}
                    >
                    </TextFieldComponent>
                    : true}
                </View>
                : true}
              {this.props.isSignatureRequired ?
                <View style={styles.signatureView}>
                  <TouchableOpacity
                    style={styles.signatureContainer}
                    onPress={() => this.accessSignatrePad()}
                    disabled={this.props.simNumber == ''}
                  >
                    {this.state.signatureData !== false && this.state.signatureData !== undefined && this.state.signatureData !== null ?
                      <Image
                        style={styles.signatureImage}
                        key={this.state.signatureData.signatureRand}
                        source={{
                          uri: `data:image/jpg;base64,${this.state.signatureData.signatureBase}`
                        }}
                        resizeMode="stretch"
                      />
                      :
                      <Text style={styles.signatureTxt}>
                        {this.state.locals.customerSignature}
                      </Text>}
                  </TouchableOpacity>
                </View> :
                <View />}
            </View>
            <View style={styles.bottomContainer}>
              <View style={styles.direbutRow}>
                <View style={styles.dirImageView}>
                </View>
                <View style={styles.filterButView}>
                  <View>
                    {this.isValidForm() ?
                      <TouchableOpacity onPress={() => this.debouncebtnOnPress()}>
                        <Text style={styles.filterText}>{btnText}</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity disabled={true}>
                        <Text style={styles.filterTextDisabled}>{btnText}</Text>
                      </TouchableOpacity>}
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
          <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
            <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
          </DragComponent>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const signature = state.mobile.signature;

  return { Language, WOM, signature };
};

const styles = StyleSheet.create({
  topContainer: {
    flex: 0.05,
    marginTop: 8,
    marginBottom: 8,
  },
  middleContainer: {
    flex: 0.8,
    flexDirection: 'column',
    marginLeft: 8,
    marginRight: 8
  },
  bottomContainer: { flex: 0.15 },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1,
    paddingBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  titleText: {
    fontSize: 18,
    color: '#000000',
    marginBottom: 8,
    marginTop: 8,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  detailElimentContainer: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 2,
    marginTop: 2,
    marginLeft: 8,
    marginRight: 8,
  },
  elementLeft: {
    flex: 0.4,
    borderWidth: 0
  },
  elementRight: {
    flex: 0.6,
    borderWidth: 0
  },
  contentContainerTxt: {
    fontSize: 15,
    fontWeight: 'bold',
    color: Colors.btnActiveTxtColor,
  },
  contentContainerLbl: {
    fontSize: 15,
    color: Colors.btnActiveTxtColor,
  }, signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
  },
  signatureTxt: {
    color: Colors.black,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  }, signatureView: {
    marginLeft: 15,
    backgroundColor: Colors.cardborder,
    marginRight: 15,
    marginTop: 15,
    marginBottom: 15,
    height: 100,
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  }
});

export default connect(mapStateToProps, actions)(CustomerConfirmationWOM);