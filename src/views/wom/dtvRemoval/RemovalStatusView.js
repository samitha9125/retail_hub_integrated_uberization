import React from 'react';
import {
  Text,
  View,
  BackHandler,
  NetInfo,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';

import ActIndicator from '../ActIndicator';
import { Header } from '../Header';
import DropDownComponent from '../components/DropdownComponent';
import ButtonComponent from '../components/ButtonComponent';
import RadioButton from '../components/RadioButtonComponent';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/Wom';

const Utill = new Utills();
const { height } = Dimensions.get('screen')
class RemovalStatusView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      removalStatuses: [
        { label: strings.successRemovalEquipment, value: 0 },
        { label: strings.failureRemovalEquipment, value: 1 },
      ],
      selectedRemovalStatus: null, // By default
      failureReasons: this.props.failureReasons,
      selectedFailureReason: null,
      locals: {
        screenTitle: this.props.name,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        inProgressTitle: strings.inProgressTitle,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      },
      failureReasonsIndex: [],
      failureReasonsOptions: [],
      selectedFailureReason: null,
      selectedFailureReasonCode: null,

      apiLoading: false,
      app_States: '',
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.formatFailureReasons();
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      failureReasons: newProps.failureReasons
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    this.navigatorOb.pop();
    // this.navigatorOb.resetTo({
    //   title: this.props.name,
    //   screen: 'DialogRetailerApp.views.WomDetailView',
    //   passProps: { List: this.props.List, name: this.props.name }
    // });
  }

  submitFailureReasonData = () => {
    const {
      order_id, job_id, app_flow, cir, conn_no
    } = this.props.WOM.work_order_detail;
    const { selectedRemovalStatus, selectedFailureReasonCode } = this.state;

    const data = {
      dtv_acc_no: conn_no,
      action: selectedRemovalStatus,
      job_id,
      app_flow,
      reasonId: selectedFailureReasonCode,
      cir,
      order_id,
      request_type: 'EQU_REMOVE'
    }

    console.log('Data', data);
    this.setState({ apiLoading: true }, () => {
    Utill.apiRequestPost('remove', 'connectionRemoval', 'wom',
      data, this.onSuccess, this.onFail, this.onError);
    });
  }

  onSuccess = (response) => {
    console.log('Response', response.data.data);
    this.setState({ apiLoading: false, app_States: response.data.data.app_screen }, () => {
      let navigateTo = '';
      let props = {};
      let title = this.props.woNameParam;
      // this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
      switch (this.state.app_States) {
        case 'VIEW_COMMON_SERIAL':
          title = this.state.locals.inProgressTitle;
          navigateTo = 'DialogRetailerApp.views.RemovalItemsScreen';
          props = {
            warranty_details: response.data.data.warranty_details
          }
          break;
        case 'VIEW_FEEDBACK':
          if (this.state.selectedFailureReasonCode == 'PP') {
            title = this.state.locals.inProgressTitle;
            navigateTo = 'DialogRetailerApp.views.CustomerFeedback';
            props = {
              screenTitle: this.state.locals.inProgressTitle,
              feedbackOptions: response.data.data.feedback_question,
              isSignaturepadRequired: true
            }
          } else if (this.state.selectedFailureReasonCode == 'NO') {
            title = this.state.locals.inProgressTitle;
            navigateTo = 'DialogRetailerApp.views.CustomerFeedback';
            props = {
              screenTitle: this.state.locals.inProgressTitle,
              feedbackOptions: response.data.data.feedback_question,
              isSignaturepadRequired: false
            }
          }
          break;
        default:
          navigateTo = 'DialogRetailerApp.views.WomDetailView'
      }

      this.navigatorOb.push({ title: title, screen: navigateTo, passProps: props });
    });
  }

  onFail = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', strings.error, response.data.error, '');
    })
  }

  onError = (error) => {
    this.setState({ apiLoading: false }, () => {
      if (error == 'Network Error') {
        error = strings.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = strings.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    })
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }

  formatFailureReasons() {
    let failureReasonsIndex = [];
    let failureReasonsOptions = [];

    if (this.state.failureReasons != undefined || this.state.failureReasons != null) {
      this.state.failureReasons.map((item) => {
        failureReasonsIndex.push(item.id);
        failureReasonsOptions.push(item.value);
      });

      this.setState({ failureReasonsIndex, failureReasonsOptions })
    }
  }

  validation() {
    const { selectedRemovalStatus, selectedFailureReasonCode } = this.state;

    let isButtonDisabled = true;
    if (selectedRemovalStatus !== null) {
      if (selectedRemovalStatus === 1 && selectedFailureReasonCode !== null) {
        isButtonDisabled = false;
      } else if (selectedRemovalStatus === 0 || selectedRemovalStatus === 2) {
        isButtonDisabled = false;
      }
    }
    return isButtonDisabled;
  }

  onPressCancel() { this.okHandler(); }

  onPressConfirm = () => { this.submitFailureReasonData(); }

  onSelectRemovalStatus(value) { this.setState({ selectedRemovalStatus: value }); }

  onSelectFailureReason(value) {
    this.setState({
      selectedFailureReason: this.state.failureReasonsOptions[value],
      selectedFailureReasonCode: this.state.failureReasonsIndex[value]
    });
  }

  renderFailureReasonDropdown() {
    const { dropdownContainerStyle } = styles;

    return (
      <View style={dropdownContainerStyle}>
        <DropDownComponent
          options={this.state.failureReasonsOptions}
          onSelect={(value) => { this.onSelectFailureReason(value) }}
          selectedOption={this.state.selectedFailureReason}
          placeHolder={strings.failureReason}
        />
      </View>
    );
  }

  renderFormContainer() {
    const { formContainerStyle, radioButtonContainerStyle, radioButtonLabelStyle } = styles;

    return (
      <View style={formContainerStyle}>
        <View style={radioButtonContainerStyle}>
          <RadioButton
            currentValue={this.state.selectedRemovalStatus}
            value={0}
            outerCircleSize={20}
            innerCircleColor={Colors.radioBtn.innerCircleColor}
            outerCircleColor={Colors.radioBtn.outerCircleColor}
            onPress={(value) => this.onSelectRemovalStatus(value)}>
            <Text style={radioButtonLabelStyle}>{strings.successRemovalEquipment}</Text>
          </RadioButton>
        </View>
        <View style={radioButtonContainerStyle}>
          <RadioButton
            currentValue={this.state.selectedRemovalStatus}
            value={1}
            outerCircleSize={20}
            innerCircleColor={Colors.radioBtn.innerCircleColor}
            outerCircleColor={Colors.radioBtn.outerCircleColor}
            onPress={(value) => this.onSelectRemovalStatus(value)}>
            <Text style={radioButtonLabelStyle}>{strings.failureRemovalEquipment}</Text>
          </RadioButton>
          {this.state.selectedRemovalStatus === 1 ?
            this.renderFailureReasonDropdown()
            : true}
        </View>
      </View>
    );
  }

  renderButtonContainer() {
    const { bottomContainerStyle, buttonContainerStyle } = styles;

    return (
      <View style={bottomContainerStyle}>
        <View style={buttonContainerStyle}>
          <ButtonComponent
            buttonText={strings.cancel}
            backgroundColor={'transparent'}
            onPress={() => this.onPressCancel()}
          />
          <ButtonComponent
            buttonText={strings.btnConfirm}
            backgroundColor={'#ffc400'}
            onPress={() => this.onPressConfirm()}
            isButtonDisabled={this.validation()}
          />
        </View>
      </View>
    );
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView} >
          <ActIndicator />
        </View>
      )
    } else {
      return true;
    }
  }

  render() {
    const { containerStyle } = styles;
    return (
      <View style={containerStyle}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={strings.screenTitle}
        />
        {this.showIndicator()}
        {this.renderFormContainer()}
        {this.renderButtonContainer()}
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  formContainerStyle: {
    flex: 0.85,
    marginVertical: 40,
    marginHorizontal: 23
  },
  radioButtonContainerStyle: {
    marginBottom: 56
  },
  radioButtonLabelStyle: {
    marginLeft: 10
  },
  dropdownContainerStyle: {
    marginHorizontal: 30,
    marginTop: 23
  },
  bottomContainerStyle: {
    flex: 0.15,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  buttonContainerStyle: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginRight: 30
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Order ===> DetailPage ');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

export default connect(mapStateToProps, actions)(RemovalStatusView);
