import React from 'react';
import {
    StyleSheet,
    View,
    BackHandler,
    Text,
    FlatList,
    ScrollView,
    TouchableOpacity,
    Image,
    Keyboard,
    NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from '../../../components/others';
import strings from '../../../Language/Wom';
import Colors from '../../../config/colors';
import barcodeIcon from '../../../../images/common/barcode.png';
import Checkbox from 'react-native-check-box';
import DragComponent from '../../general/Drag';
import fabIcon from '../../../../images/icons/cfss/fab.png';
class RemovalItemsView extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            isConfirmable: false,
            loadingIndicator: false,
            showText: false,
            RemovalItem: [],
            scannedSerial: '',
            isChecked: false,
            api_error_message: strings.api_error_message,
            locals: {
                screenTitle: strings.inProgressTitle,
                network_error_message: strings.network_error_message,
                backMessage: strings.backMessage,
                scannedSerial: strings.lblScanSerial,
                api_error_message: strings.api_error_message,
                CONFIRM: strings.btnConfirm,
                continue: strings.continueButtonText,
                lblScanSerial: strings.lblScanSerial,
                serialdescription: strings.serialdescription,
                scannerBottomMessage: strings.scannerBottomMessage,
                scannerBottomMessage2: strings.scannerBottomMessage2,
                descriptionText: strings.serialdescription,
                connected: strings.connected,
                disconected: strings.disconected,
                network_conected: strings.network_conected,
                lblSerialNo: strings.lblSerialNo,
                lblSerial: strings.lblSerial,
                lblNoRemovalItem: strings.lblNoRemovalItem
            },
        };
        this.navigatorOb = this.props.navigator;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

        NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
        const newArray = this.props.warranty_details.map(item => {
            let obj = { ...item, isTicked: false };
            return obj;
        })
        this.setState({ RemovalItem: newArray })
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    }
    handleBackButtonClick = () => {
        this.okHandler();
        return true;
    }

    _handleFirstConnectivityChange = isConnected => {
        if (isConnected == false) {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_error_message,
                actionText: this.state.locals.disconected,
                actionId: 'fabClicked',
                actionColor: Colors.colorRed,
                textColor: Colors.white,
                duration: 'indefinite'
            })
        } else {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_conected,
                actionText: this.state.locals.connected,
                actionId: 'fabClicked',
                actionColor: Colors.radioBtn,
                textColor: Colors.white,
            })
        }
    }

    okHandler = () => {
        this.navigatorOb.pop();       
    }
    setRadioButtonActivated(serial) {
        console.log('----------setRadioButtonActivated-----------');

        const newArray = this.state.RemovalItem.map(item => {
            if (item.serial == serial) {
                item.isTicked = true;
                return item;
            } else return item;
        });
        let isConfirmable = true;
        for (let i = 0; i < newArray.length; i++) {
            console.log('value1 ', newArray[i]);

            if (newArray[i].isTicked == false) {
                isConfirmable = false;
            }
        }
        console.log('value2 ', isConfirmable);

        this.setState({ RemovalItem: newArray, isConfirmable });
    }
    checkIsConfirmable(itemArray) {
        console.log("LOG ::")
        for (let i = 0; i < itemArray.length; i++) {
            let item = itemArray[i];
            if ((item.mandatory == '1') && (!item.isTicked)) {
                console.log('can not confirmable ', item);
                return false;
            } else {
                if (i == (itemArray.length - 1)) {
                    console.log('all mandetories are ticked, confirmable');
                    return true
                }
            }
        }
    }

    loadBarodeScanner() {
        this.navigatorOb.push({
            title: this.state.locals.lblScanSerial,
            screen: 'DialogRetailerApp.views.WOMBarcodeScannerScreen',
            passProps: {
                title: this.state.locals.lblSerial,
                isReadOnly: true,
                successCb: (scannedSerial) => {
                    Keyboard.dismiss();
                    this.setState({
                        scannedSerial: scannedSerial
                    }, () => {
                        if (scannedSerial && this.varifySerial(scannedSerial)) {
                            this.setRadioButtonActivated(scannedSerial);
                        } else {
                            this.navigatorOb.showModal({
                                screen: 'DialogRetailerApp.modals.MaterialValidationAlert',
                                title: 'GeneralModalException',
                                passProps: {
                                    isCancelPressed: true,
                                    onRescanPressCb: () => this.loadBarodeScanner(),
                                    description1: this.state.locals.lblSerialNo + ': ' + scannedSerial,
                                    description2: this.state.locals.descriptionText,
                                    descriptionText: this.state.locals.serialdescription,
                                },
                                overrideBackPress: true
                            });
                        }
                    });
                }
            }
        });
    }
    varifySerial(scannedSerial) {
        let varifiedSerial = false;
        try {
            if (scannedSerial) {
                for (item of this.state.RemovalItem) {
                    if (item.serial === scannedSerial) {
                        varifiedSerial = item.serial;
                        break;
                    }
                }
            }
        } finally {
            return varifiedSerial;
        }
    }
    goto_confirmationPage = () => {
        const navigatorOb = this.props.navigator;
        navigatorOb.push({
            title: this.state.locals.screenTitle,
            screen: 'DialogRetailerApp.views.WOMCustomerConfirmation',
            passProps: {
                dataArray: this.state.RemovalItem,
                RemovalItem: this.state.RemovalItem,
                isRemoveList: true
            }
        });
    }

    renderListItem = ({ item }) => {
        console.log("In RenderListItem: ", item);
        console.log("Removal Item: ", this.state.RemovalItem);
        return (
            <View style={styles.itemSeperator}>
                <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                        <Checkbox onClick={() => { }} key={item.isTicked} isChecked={item.isTicked} disabled={true} checkBoxColor={item.isTicked ? Colors.colorYellow : Colors.colorBlack} />
                    </View>
                    <View style={styles.elementMiddle}>
                        <Text style={styles.elementTxtSuccess}>
                            {item.name}</Text>
                    </View>
                    <View style={styles.elementRight}>
                        <Text style={styles.elementTxtSuccess}>
                            {item.serial}</Text>
                    </View>
                </View>
            </View>
        )
    };
    render() {
        let loadingIndicator;
        if (this.state.apiLoading) {
            loadingIndicator = (<ActIndicator animating />);
        } else {
            loadingIndicator = true;
        }
        let screenContent;
        if (!this.state.api_error) {
            screenContent = (
                <View style={styles.container}>
                    <View style={styles.detailsContainer}>
                        <View style={styles.containerMiddle}>
                            <ScrollView>
                                <FlatList
                                    data={this.state.RemovalItem}
                                    ListEmptyComponent={
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'center' }} >
                                            <Text>{this.state.locals.lblNoRemovalItem}...</Text>
                                        </View>
                                    }
                                    renderItem={this.renderListItem}
                                    keyExtractor={(item, index) => item.toString() && index.toString()}
                                    scrollEnabled={true}
                                    refreshing={false}
                                    extraData={this.state}
                                />
                            </ScrollView>
                        </View>
                        <View style={styles.containerBottom}>
                            <View style={styles.containerBottomSection1}>
                                <TouchableOpacity onPress={() => this.loadBarodeScanner()}>
                                    <Image source={barcodeIcon} style={styles.iconStyle} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.containerBottomSection2}>
                                <TouchableOpacity style={styles.buttonCancelContainer}>
                                </TouchableOpacity>
                                {this.state.isConfirmable === true ?
                                    <TouchableOpacity style={styles.buttonContainer} onPress={() => this.goto_confirmationPage()}>
                                        <Text style={{ fontWeight: 'bold', color: Colors.btnLabel }}>
                                            {this.state.locals.CONFIRM}
                                        </Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity disabled={true} style={styles.filterTextDisabled} onPress={() => this.goto_confirmationPage()}>
                                        <Text style={{ fontWeight: 'bold' }}>
                                            {this.state.locals.CONFIRM}
                                        </Text>
                                    </TouchableOpacity>}
                            </View>
                        </View>
                    </View>
                    <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
                        <Image style={styles.helpImage} source={fabIcon} />
                    </DragComponent>
                </View>);
        } else {
            screenContent = (
                <View style={styles.errorDescView}>
                    <Text style={styles.errorDescText}>
                        {this.state.api_error_message}</Text>
                </View>);
        }
        return (
            <View style={styles.container}>
                <Header
                    backButtonPressed={() => this.handleBackButtonClick()}
                    headerText={this.state.locals.screenTitle} />
                {loadingIndicator}
                {screenContent}
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};
const styles = StyleSheet.create({
    container: {
        height: '100%',
        flexDirection: 'column',
        backgroundColor: Colors.backgroundColorWhiteWithAlpa
    },
    containerTop: {
        marginLeft: 7,
        marginRight: 2,
        flex: 0.1,
        justifyContent: 'flex-start',
        flexDirection: 'row'
    }, containerTopText: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
    }, containerTopSection1: {
        flex: 0.5,
        flexDirection: 'column',
        margin: 7,
        alignItems: 'center'
    }, containerTopSection2: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerTopSection3: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerTopSection4: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center'
    }, containerMiddle: {
        marginLeft: 2,
        marginRight: 2,
        height: '80%',
        flexDirection: 'row'
    }, containerMiddleSection1: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection2: {
        flex: 0.4,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection3: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection4: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection5: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerBottom: {
        borderTopWidth: 1,
        borderRightColor: Colors.transparent,
        borderLeftColor: Colors.transparent,
        borderColor: Colors.borderLineColorLightGray,
        height: '15%',
        flexDirection: 'row'
    }, containerBottomSection1: {
        flex: 0.3,
        flexDirection: 'row',
        paddingLeft: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        alignSelf: 'center'
    }, containerBottomSection2: {
        flex: 0.7,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    detailsContainer: {
        borderWidth: 0,
        paddingTop: 15
    },
    imageContainer: {
        alignItems: 'center'
    },
    imageStyle: {
        width: 50,
        height: 50,
        marginTop: 15,
        marginBottom: 5
    },
    bottomContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingRight: 5,
        paddingBottom: 10,
        margin: 10,
        marginRight: 0
    },
    buttonCancelContainer: {
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.colorTransparent,
        height: 45,
        borderRadius: 5,
        alignSelf: 'flex-start'
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.btnActive,
        height: 45,
        borderRadius: 5,
        marginLeft: 5,
    },
    detailElimentContainer: {
        flexDirection: 'row',
        borderWidth: 0,
        marginBottom: 10
    },
    elementLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 10,
        flex: 0.1,
        borderWidth: 0
    },
    elementRight: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        flex: 0.6,
        paddingRight: 10,
        borderWidth: 0,
    },
    elementMiddle: {
        flex: 0.4,
        justifyContent: 'center',
        paddingLeft: 10,
        borderWidth: 0,
    },
    elementTxt: {
        fontSize: 17
    },
    elementTxtSuccess: {
        fontSize: 15,
        color: Colors.black
    },
    elementRightWithIcon: {
        flex: 1,
        borderWidth: 0,
        flexDirection: 'row'
    },
    iconStyle: {
        width: 50,
        height: 45,
        marginTop: 0,
        borderWidth: 1,
        marginBottom: 0,
    },
    itemSeperator: {
        paddingTop: 5,
        paddingBottom: 5,
        borderColor: Colors.borderColorGray,
        borderBottomWidth: 0.5
    },
    helpImage: {
        height: 50,
        width: 50,
        resizeMode: 'stretch',
    },
    filterTextDisabled: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: Colors.btnDisable,
        // color: Colors.black,
        borderRadius: 5,
        // textAlign: 'center'
    },
    errorDescView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorDescText: {
        textAlign: 'center',
        fontSize: 17,
        fontWeight: 'bold',
        color: Colors.black
    },
});
export default connect(mapStateToProps, actions)(RemovalItemsView);