import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions,
  BackHandler,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import ActIndicator from '../ActIndicator';
import strings from '../../../Language/Wom';
import { Header } from '../Header';
import DragComponent from '../../general/Drag';

var { height, width } = Dimensions.get('window');
class DisconnectionStatusView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: true,
      showText: false,
      api_error: false,
      prov_failed: false,
      isReplaced: false,
      flatListRefresh: false,
      disconnectionTimeOut: props.prov_config ? parseInt(props.prov_config.timeout) * 1000 : 600000,
      disconnectionWait: props.prov_config ? parseInt(props.prov_config.time_interval) * 1000 : 120000,
      overallStatus: props.prov_data ? props.prov_data.status : this.props.WOM.work_order_provisioning_status,
      disconnectedStatusData: props.prov_data ? props.prov_data.serial_list : this.props.WOM.work_order_installed_items,
      locals: {
        error: strings.error,
        btnDisconnect: strings.btnDisconnect,
        lblDisconnectInProgress: strings.disconnectInProgress,
        lblDisconnectionComplete: strings.disconnectComplete,
        lblDisconnectionFailed: strings.disconnectFailed,
        lblSerialNo: strings.lblSerialNo,
        btnConfirm: strings.btnConfirm,
        btnAddNew: strings.btnAddNew,
        btnReplace: strings.btnReplace,
        lblReplaceAlertMsg: strings.lblReplaceAlertMsg,
        backMessage: strings.backMessage,
        doManualProv: strings.doManualProv,
        lblToBeDisconnected: strings.lblToBeDisconnected,
        btnConvert: strings.btnConvert,
        lblNicNo: strings.lblNICNo,
        lblConnectionNo: strings.lblConnectionNo,
        api_error_message: strings.api_error_message,
        alertHeader: strings.alertHeader,
        lblDisconnectionMsg: strings.lblDisconnectionMsg,
        inProgressTitle: strings.inProgressTitle,
        lblDisconnectionFailedMsg: strings.disconnectFailed,
        network_error_message: strings.network_error_message
      }
    };
    this.retry_attempt = 0;
    this.navigatorOb = this.props.navigator;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    console.log('DisconnectionStatusView componentWillMount ', this.props.WOM);
    if (this.state.disconnectedStatusData == "") {
      this.props.workOrderSerialList(this.state.disconnectedStatusData); //?
    }

    this.getDisconnectionStatus(); //Get disconnection status
  }

  componentDidMount() {
    this.disconnectionStatusChecking();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    }
  }

  handleRefresh() {
    this.state.isReplaced ?
      this.displayCommonAlert('alertWithOK', this.state.locals.alertHeader, this.state.locals.lblDisconnectionMsg, '')
      :
      this.getDisconnectionStatus();
  }

  disconnectionStatusChecking() {
    let timeCount = 0;
    this.timer = setInterval(() => {
      timeCount += this.state.disconnectionWait;
      if (this.state.overallStatus == 'DISCONNECTION_INPROGRESS') {
        if (timeCount >= this.state.disconnectionTimeOut) {
          clearInterval(this.timer);
        } else {
          this.getDisconnectionStatus()
        }
      } else {
        clearInterval(this.timer);
      }
    }, this.state.disconnectionWait)
  }

  //Get Disconnection Status =======
  getDisconnectionStatus() {
    if (this.state.overallStatus == 'DISCONNECTION_FAILED') {
      this.setState({ prov_failed: true });
    } else {
      this.setState({ prov_failed: false });
    }

    const response = {
      data: {
        data: {
          disconnect_config: {
            timeout: 10,
            time_interval: 10
          },
          disconnect_data: {
            status: 'DISCONNECTION_COMPLETED',
            serial_list: [{ nicNo: '12343143423123', connectionNo: '12312323232', status: 'DISCONNECTION_COMPLETED' }]
          }
        }
      }
    };
    this.getDisconnectionSuccess(response);
  }
  getDisconnectionSuccess = (response) => {
    if (this.state.disconnectionWait != response.data.data.disconnect_config.time_interval && this.state.disconnectionTimeOut != response.data.data.disconnect_config.timeout) {
      this.setState({
        disconnectionWait: parseInt(response.data.data.disconnect_config.time_interval) * 1000,
        disconnectionTimeOut: parseInt(response.data.data.disconnect_config.timeout) * 1000,
      });
    }

    let responseArray = response.data.data;
    // if (this.state.overallStatus != responseArray.prov_data.status) {
    //this.props.workOrderGetDisconnectionStatus(responseArray.disconnect_data.status); //?
    this.setState({
      apiLoading: false,
      overallStatus: responseArray.disconnect_data.status,
      disconnectedStatusData: responseArray.disconnect_data.serial_list,
    });
  }
  getDisconnectionFailed = (response) => {
    this.setState({ apiLoading: false, overallStatus: 'DISCONNECTION_FAILED' }, () => this.displayCommonAlert('error', this.state.locals.error, response.data.error ? response.data.error : this.state.locals.lblDisconnectionFailedMsg, ''));
  }
  getDisconnectionEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }
  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }
  //=================================

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }
  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  //OnPressConvert =============
  onPressConvert = async () => {
    console.log('### Item to be converted', this.state.disconnectedStatusData);
    // Utill.apiRequestPost('convertPostTopre', 'postToPre', 'wom', {
    //   conNo: this.state.conNo,
    //   outstanding: this.state.outstanding,
    //   nic: this.state.nicNo,
    //   signature: this.props.signature,
    // }, this.onPressConvertSuccess, this.onPressConvertFailure, this.onPressConvertEx);
    //Utill.apiRequestPost('convertPostTopre', 'postToPre', 'wom', itemToBeConverted, this.onPressConvertSuccess, this.onPressConvertFailure, this.onPressConvertEx);
  }
  onPressConvertSuccess = (response) => {
    //Show success alert
    const navigatorOb = this.props.navigator;
    try {
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.views.WomModalSuccessScreen',
        title: 'GeneralModalException',
        passProps: {
          message: strings.postToPreConversionSuccess,
          onApproval: () => {
            //Navigate to tile view
            const navigatorOb = this.props.navigator;
            navigatorOb.resetTo({
              title: strings.HomeMenu,
              screen: 'DialogRetailerApp.views.HomeTileScreen'
            });
          }
        },
        overrideBackPress: true
      });
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }
  onPressConvertFailure = (response) => {
    this.setState({ apiLoading: false });
    this.displayCommonAlert('defaultAlert', 'Error', response.data.error ? response.data.error : 'System error when updating user disconnection status.', '')
  }
  onPressConvertEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }
  //=============================

  renderStatus = (status) => {
    switch (status) {
      case 'DISCONNECTION_COMPLETED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtSuccess}>
              {this.state.locals.lblDisconnectionComplete}
            </Text>
          </View>
        );

      case 'DISCONNECTION_INPROGRESS':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblDisconnectInProgress}
            </Text>
          </View>
        );

      case 'DISCONNECTION_FAILED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtFailed}>
              {this.state.locals.lblDisconnectionFailed}
            </Text>
          </View>
        );

      case 'TO_BE_DISCONNECTED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblToBeDisconnected}
            </Text>
          </View>
        );
      default:
        return true
    }
  }

  renderListItem = ({ item }) => {
    return (
      <View style={styles.cardView}>
        <View style={styles.cardTop}>
          <View style={styles.cardTopSection1}>
          </View>
          {this.renderStatus(item.status)}
        </View>
        <View style={styles.cardBottom}>
          <Text style={styles.txtContent}>
            {this.state.locals.lblNicNo} : {item.nicNo}
          </Text>
          <Text style={styles.txtContent}>
            {this.state.locals.lblConnectionNo} : {item.connectionNo}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View>
          <Header
            backButtonPressed={() => this.okHandler()}
            refreshPressed={() => this.handleRefresh()}
            displayRefresh={true}
            headerText={this.props.screenTitle} />
        </View>
        <View style={styles.topContainer} />
        <View style={styles.middleContainer}>
          {this.showIndicator()}
          <FlatList
            data={this.state.disconnectedStatusData}
            keyExtractor={(x, i) => i.toString()}
            renderItem={this.renderListItem}
            scrollEnabled={true}
            extraData={this.state}
          />
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.direbutRow}>
            <View style={styles.dirImageView}>
            </View>
            <View style={styles.filterButView}>
              <View>
                {this.state.overallStatus == 'DISCONNECTION_COMPLETED' ?
                  <TouchableOpacity onPress={() => this.onPressConvert()}>
                    <Text style={styles.filterText}>{this.state.locals.btnConvert}</Text>
                  </TouchableOpacity> :
                  <Text style={styles.filterTextDisabled}>{this.state.locals.btnConvert}</Text>}
              </View>
            </View>
          </View>
        </View>
        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
        </DragComponent>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};


const styles = StyleSheet.create({
  topContainer: {
    flex: 0.08
  }, middleContainer: {
    flex: 0.8,
    flexDirection: 'column',
  }, bottomContainer: {
    flex: 0.12
  },
  labelCol: {
    width: (width / 2) - 10,
    paddingLeft: 15
  },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1,
    paddingBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  }, cardView: {
    flex: 1,
    backgroundColor: Colors.white,
    height: 100,
    margin: 5,
    //marginVertical: 10,
    justifyContent: 'space-around',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
    width: Dimensions.get('window').width - 10,
  },
  cardTop: {
    flex: 1,
    flexDirection: 'row',
  },
  cardBottom: {
    flex: 1.5,
    marginLeft: 7,
    borderColor: Colors.lightGrey,
    justifyContent: 'space-around',
    marginBottom: 10
  }, cardTopSection1: {
    flex: 1,
    marginLeft: 2,
    flexDirection: "column",
  },
  cardTopSection3: {
    flex: 1.5,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 12
  }, txtTitle: {
    fontSize: 18,
    marginLeft: 7,
    color: Colors.black,
  }, txtContent: {
    fontSize: 16,
    color: Colors.black,
  }, txtPending: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorPureYellow,
    fontWeight: 'bold'
  }, txtSuccess: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorGreen,
    fontWeight: 'bold'
  }, txtFailed: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorRed,
    fontWeight: 'bold'
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  }
});

export default connect(mapStateToProps, actions)(DisconnectionStatusView);