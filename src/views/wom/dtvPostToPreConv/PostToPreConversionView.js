import React from 'react';
import {
  Text,
  Dimensions,
  View,
  BackHandler,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import ActIndicator from '../ActIndicator';
import { Header } from '../../wom/Header';
import ButtonComponent from '../components/ButtonComponent';
import MaterialInput from './../MaterialInput';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/Wom';

const Utill = new Utills();
class PostToPreConversionView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      nicNo: '',
      connectionNo: '',
      conNo: '',
      outstanding: undefined,
      outstandingBalance: '',
      signatureObj: '',
      locals: {
        screenTitle: strings.postpre,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        customerSignature: strings.customerSignature,
        lblError: strings.error,
        msgFillForm: strings.msgFillForm,
        msgFillSignature: strings.msgFillSignature,
        inProgressTitle: strings.inProgressTitle,
        lblInvalidNIC: strings.invalidNIC,
        invalidaCon: strings.invalidCon,
        postpre: strings.postpre
      },
      apiLoading: false,
      app_States: '',
      api_error: false,
    };
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  displayAlert(messageType, messageHeader, messageBody, messageFooter) {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter
      }
    });
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }
  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  setIndicater = (loadingState = true) => {
    this.setState({ apiLoading: loadingState });
  }
  showIndicator() {
    if (this.state.apiLoading) { return (<View> <ActIndicator animating /></View>); }
  }

  //Submit Post to Pre conversion form - NEEDS TO BE UPDATED TO CALL DISCONNECT FIRST
  submitDisconnectionData = () => {
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('convertPostTopre', 'postToPre', 'wom', {
        conNo: this.state.conNo,
        outstanding: this.state.outstanding,
        nic: this.state.nicNo,
        signature: this.props.signature,
      }, this.onSuccess, this.onFail, this.onError);
    })
  }
  onSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      const serial_list = [{
        status: 'DISCONNECTION_COMPLETED',
        nicNo: this.state.nicNo,
        connectionNo: this.state.connectionNo
      }];
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: this.state.locals.postpre,
        screen: 'DialogRetailerApp.views.DisconnectionStatusScreen',
        passProps: {
          //provStatus: response.data.data.status,
          provStatus: 'DISCONNECTION_COMPLETED',
          //provisionedStatusData: response.data.data.serial_list,
          provisionedStatusData: serial_list,
          displayRefresh: true,
          screenTitle: this.state.locals.postpre,
        }
      });
    })
  }
  onFail = (response) => {
    console.log('onFail', response);
    this.setState({ apiLoading: false, api_error_message: response.data.error, api_error: true }, () => {
      const additionalInformation = [
        { label: 'NIC no', value: this.state.nicNo },
        { label: 'Connection no', value: this.state.conNo }
      ];
      //Show error alert
      const navigatorOb = this.props.navigator;
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.views.WomModalFailureScreen',
        title: 'GeneralModalException',
        passProps: {
          showAdditionalInformation: true,
          additionalInformation,
          descriptionText: this.state.api_error_message,
        },
        overrideBackPress: true
      });
    });
  }
  onError = (error) => {
    this.setState({ api_error: true, api_error_message: error, apiLoading: false }, () => {
      if (error == 'Network Error') {
        error = strings.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = strings.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  validation() {
    const { nicNo, connectionNo, outstanding, conNo } = this.state;

    let isButtonDisabled = true;
    if (nicNo !== '' && conNo !== '' && outstanding != undefined && this.props.signature !== null && this.props.signature !== undefined) {
      isButtonDisabled = false;
    }
    return isButtonDisabled;
  }

  onPressDisconnect = () => {
    const additionalInformation = [
      { label: 'NIC no', value: this.state.nicNo },
      { label: 'Connection no', value: this.state.conNo }
    ];

    //Show confirmation alert
    const navigatorOb = this.props.navigator;
    try {
      this.displayCommonAlert('alert', strings.disconnectionApproval, '', '' , additionalInformation );
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter, additionalInformation) {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        showAdditionalInformation: true,
        additionalInformation: additionalInformation,
        btnFun:()=>{
          if(messageType == 'alert'){
            this.submitDisconnectionData();
          }
        },
        onPressOK: () => {
          const navigatorOb = this.props.navigator;
          navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        }
      }
    });
  }

  onEnterNIC(value) {
    let nicNo = '';
    if (Utill.nicValidate(value)) {
      nicNo = value;
    } else {
      nicNo = '';
    }
    this.setState({ nicNo, conNo: '', outstanding: undefined }, () => {
      if (this.state.nicNo == '') {
        this.displayCommonAlert('defaultAlert', this.state.locals.lblError, this.state.locals.lblInvalidNIC, '');
      }
    });
  }
  onEnterConnectionNo(value) {
    if (value.length == 8) {
      Utill.apiRequestPost('validatePostTopre', 'postToPre', 'wom', {
        nic: this.state.nic, conNo: value
      },
        (response) => {
          console.log('XXXXXXXXXXXXXXXXXXX', response);
          this.setState({
            conNo: value,
            outstanding: `Rs ${response.data.outstanding}`,
          });
        },
        (error) => {
          console.log(error);
          this.setState({ outstanding: undefined }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, error.data.error, '');
          });
        },
        (error) => {
          console.log(error);
          this.setState({ outstanding: undefined }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, error, '');
          });
        });
    } else {
      this.setState({ conNo: '', outstanding: undefined }, () => {
        this.displayCommonAlert('defaultAlert', this.state.locals.lblError, this.state.locals.invalidaCon, '');
      });
    }
  }

  accessSignatrePad() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.SignaturePadComponentWom',
      passProps: {
        downlodUrl: 'https://www.dialog.lk/tc',
        uploadSignature: (signatureObj) => {
          this.setState({ signatureObj });
        }
      }
    });
  }

  renderFormContainer() {
    const { formContainerStyle, inputContainerStyle, buttonContainerStyle } = styles;
    return (
      <View style={formContainerStyle}>
        <View style={inputContainerStyle}>
          <MaterialInput
            isNeedToFocus
            label={strings.lblNICNo}
            visible={false}
            maxLength={12}
            onEndEditing={(text) => this.onEnterNIC(text.nativeEvent.text)}
            value={this.state.nicNo}
          />
        </View>
        <View style={inputContainerStyle}>
          <MaterialInput
            isNeedToFocus
            label={strings.lblConnectionNo}
            visible={false}
            maxLength={10}
            value={this.state.conNo}
            keyboardType={'numeric'}
            onEndEditing={(text) => this.onEnterConnectionNo(text.nativeEvent.text)}
          />
        </View>
        <View style={inputContainerStyle}>
          <MaterialInput
            isNeedToFocus
            label={strings.lblOutstandingBal}
            visible={false}
            disabled={true}
            value={this.state.outstanding}
          />
        </View>

        <View style={styles.signatureView}>
          <TouchableOpacity
            style={styles.signatureContainer}
            onPress={() => this.accessSignatrePad()}
          >
            {this.props.signature !== null && this.props.signature !== undefined ?
              <Image
                style={styles.signatureImage}
                key={this.props.signature.signatureRand}
                source={{
                  uri: `data:image/jpg;base64,${this.props.signature.signatureBase}`
                }}
                resizeMode="stretch"
              />
              :
              <Text style={styles.signatureTxt}>  {this.state.locals.customerSignature} </Text>}
          </TouchableOpacity>
        </View>
        <View style={buttonContainerStyle}>
          <ButtonComponent
            buttonText={strings.btnDisconnect}
            backgroundColor={'#ffc400'}
            onPress={() => this.onPressDisconnect()}
            isButtonDisabled={this.validation()}
          />
        </View>
      </View>
    );
  }

  render() {
    const { containerStyle } = styles;
    return (
      <View style={containerStyle}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}
        />
        <ScrollView>
          {this.showIndicator()}
          {this.renderFormContainer()}
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  formContainerStyle: {
    flex: 0.85,
    marginTop: 10,
    marginHorizontal: 40
  },
  inputContainerStyle: {
    marginVertical: 6
  },
  bottomContainerStyle: {
    flex: 0.15,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  buttonContainerStyle: {
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginRight: -10
  },
  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
  },
  signatureTxt: {
    color: Colors.black,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  },
  signatureView: {
    marginTop: 40,
    height: 100,
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  },
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Order ===> DetailPage ');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const signature = state.mobile.signature;
  return { Language, WOM, signature };
};

export default connect(mapStateToProps, actions)(PostToPreConversionView);
