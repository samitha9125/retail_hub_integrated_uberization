import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  FlatList,
  Image,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash'

import ButtonComponent from '../../components/ButtonComponent';
import CardComponent from '../../components/CardComponent';
import * as actions from '../../../../actions';
import { Header } from '../../Header';
import strings from '../../../../Language/Wom';
import Colors from '../../../../config/colors';
import DragComponent from '../../../general/Drag';
import fabIcon from '../../../../../images/icons/cfss/fab.png';
import ActIndicator from '../../ActIndicator';
import Utills from '../../../../utills/Utills';
import globalConfig from '../../../../config/globalConfig'

const Utill = new Utills();
class RefixItemsView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      isConfirmable: false,
      isfetching: false,
      api_error_message: strings.api_error_message,
      locals: {
        screenTitle: strings.inProgressTitle,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        CONFIRM: strings.btnConfirm,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        inProgressTitle: strings.inProgressTitle,
        lblError: strings.error
      },
      PmsId: this.props.PmsId, //Only for Relocation Refix WO
      RefixItems: this.props.warrantyDetails,
      isProvisioned: this.props.isProvisioned, //Whether provisioned or not (Initially false)
    };
    this.props.workOrderRefixWarrantyDetails(this.props.warrantyDetails); //Store initial warranty details using redux

    this.debounceOnPressConfirm = _.debounce(() => this.onPressConfirm(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
      this.debounceOnPressAddNew = _.debounce(() => this.onPressAddNew(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      title: this.props.name,
      screen: 'DialogRetailerApp.views.WomDetailView'
    });
  }

  onRefresh() {
    console.log('refreshing');
    this.setState({ isfetching: true });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter
      }
    });
  }

  onPressConfirm() {
    this.setState({ apiLoading: true }, () => {
      const data = {
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        job_id: this.props.WOM.work_order_detail.job_id,
        app_flow: this.props.WOM.work_order_detail.app_flow,
        order_id: this.props.WOM.work_order_detail.order_id,
        cir: this.props.WOM.work_order_detail.cir,
        request_type: this.props.WOM.work_order_detail.wo_type,
        biz_unit: this.props.WOM.work_order_detail.biz_unit,
        breached: this.props.WOM.work_order_detail.breached,
        serial_list: this.state.RefixItems,
        old_serial_list: this.props.WOM.work_order_refix_warranty_details,
        jobType: this.props.WOM.work_order_detail.job_type,
      };
      Utill.apiRequestPost('RefixConfirm', 'DtvRefix', 'wom',
        data, this.confirmSuccess, this.confirmFailed, this.confirmEx);
    });
  }
  confirmSuccess = (response) => {
    this.props.workOrderAppScreen(response.data.data.app_screen);
    this.setState({ apiLoading: false }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
    });
  }
  confirmFailed = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, '')
    });
  }
  confirmEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response, '')
    });
  }

  onPressAddNew() {
    //Redirect to manage items page
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.TRBReplacmentScreen',
      passProps: {
        isRefix: true
      }
    });
  }

  renderListItem = ({ item }) => (
    <CardComponent
      isSimpleCard
      headerText={item.name}
      valueText={item.serial}
    />
  );

  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }

    let screenContent;
    if (!this.state.api_error) {
      screenContent = (
        <View style={styles.container}>
          <View style={styles.detailsContainer}>
            <View style={styles.mainContainerStyle}>
              <FlatList
                data={this.state.RefixItems}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isfetching}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => item.toString() && index.toString()}
                scrollEnabled={true}
                showsVerticalScrollIndicator={false}
              />
            </View>

            <View style={styles.bottomContainerStyle}>
              <View style={styles.buttonContainerStyle}>
                {this.state.isProvisioned !== 'Y' ?
                  <ButtonComponent
                    buttonText={strings.btnAddNew}
                    backgroundColor={'#ffc400'}
                    onPress={() => this.debounceOnPressAddNew()}
                  />
                  : true}
                <ButtonComponent
                  buttonText={strings.btnConfirm}
                  backgroundColor={'#ffc400'}
                  onPress={() => this.debounceOnPressConfirm()}
                />
              </View>
            </View>
          </View>

          <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
            <Image style={styles.helpImage} source={fabIcon} />
          </DragComponent>
        </View>
      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}
          </Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}
        />
        {loadingIndicator}
        {screenContent}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  mainContainerStyle: {
    flex: 0.85
  },
  bottomContainerStyle: {
    flex: 0.15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderTopWidth: 1,
    borderRightColor: Colors.transparent,
    borderLeftColor: Colors.transparent,
    borderColor: Colors.borderLineColorLightGray,
  },
  buttonContainerStyle: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingRight: 18,
    paddingTop: 16
  },
  containerTop: {
    marginLeft: 7,
    marginRight: 2,
    flex: 0.1,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  containerTopText: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    justifyContent: 'center',
  },
  containerTopSection1: {
    flex: 0.5,
    flexDirection: 'column',
    margin: 7,
    alignItems: 'center'
  },
  containerTopSection2: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerTopSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerTopSection4: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerMiddle: {
    marginLeft: 2,
    marginRight: 2,
    height: '80%',
    flexDirection: 'row'
  },
  containerMiddleSection1: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddleSection2: {
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddleSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddleSection4: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddleSection5: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerBottom: {
    borderTopWidth: 1,
    borderRightColor: Colors.transparent,
    borderLeftColor: Colors.transparent,
    borderColor: Colors.borderLineColorLightGray,
    height: '10%',
    flexDirection: 'row'
  },
  containerBottomSection1: {
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-end'
  },
  containerBottomSection2: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'flex-end'
  },
  detailsContainer: {
    flex: 1,
    borderWidth: 0,
    padding: 15
  },
  imageContainer: {
    alignItems: 'center'
  },
  imageStyle: {
    width: 50,
    height: 50,
    marginTop: 15,
    marginBottom: 5
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 5,
    paddingBottom: 10,
    margin: 10,
    marginRight: 0
  },
  dummyView: {
    flex: 0.6
  },
  dummyView3: {
    flex: 1.2
  },
  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
  },
  dummyView2: {
    flex: 0.2
  },
  buttonTxt: {
    color: Colors.colorBlack,
    textAlign: 'center',
    backgroundColor: Colors.colorTransparent
  },
  titleText: {
    paddingTop: 8,
    paddingLeft: 8,
    color: Colors.colorBlack,
    textAlign: 'left',
    backgroundColor: Colors.colorTransparent,
    fontSize: 18,
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },
  elementLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 0,
    flex: 0.5,
    borderWidth: 0
  },
  elementRight: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 0.5,
    borderWidth: 0,
  },
  elementMiddle: {
    flex: 0.4,
    justifyContent: 'center',
    paddingLeft: 10,
    borderWidth: 0,
  },
  elementTxt: {
    fontSize: 17
  },
  elementTxtSuccess: {
    fontSize: 17,
    color: Colors.black
  },
  elementTxtFaulty: {
    fontSize: 17,
    color: Colors.Colors
  },
  elementRightWithIcon: {
    flex: 1,
    borderWidth: 0,
    flexDirection: 'row'
  },
  iconStyle: {
    width: 50,
    height: 45,
    marginTop: 0,
    borderWidth: 1,
    marginBottom: 0,
  },
  itemSeperator: {
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: Colors.borderColorGray,
    borderBottomWidth: 0.5
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: Colors.black,
    borderRadius: 5,
    textAlign: 'center'
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
});

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Oder ===> Visit History');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

export default connect(mapStateToProps, actions)(RefixItemsView);