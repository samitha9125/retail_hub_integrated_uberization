import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Image,
  RefreshControl,
  Keyboard,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import Checkbox from 'react-native-check-box';
import _ from 'lodash'

import * as actions from '../../../../actions';
import { Header } from '../../../wom/Header';
import strings from '../../../../Language/Wom';
import Colors from '../../../../config/colors';
import barcodeIcon from '../../../../../images/common/barcode.png';
import DragComponent from '../../../general/Drag';
import Utills from '../../../../utills/Utills';
import fabIcon from '../../../../../images/icons/cfss/fab.png';
import ActIndicator from '../../ActIndicator';
import globalConfig from '../../../../config/globalConfig'

const Utill = new Utills();
class RecoveryItemsView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      isConfirmable: false,
      isfetching: false,
      loadingIndicator: false,
      showText: true,
      RecoveryItem: this.props.warrantyDetails,
      scannedSerial: '',
      isChecked: false,
      api_error_message: strings.api_error_message,
      locals: {
        screenTitle: strings.inProgressTitle,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        scannedSerial: strings.lblScanSerial,
        api_error_message: strings.api_error_message,
        CONFIRM: strings.btnConfirm,
        continue: strings.continueButtonText,
        lblScanSerial: strings.lblScanSerial,
        serialdescription: strings.serialdescription,
        scannerBottomMessage: strings.scannerBottomMessage,
        scannerBottomMessage2: strings.scannerBottomMessage2,
        lblItemType: strings.lblItemType,
        lblSerial: strings.lblSerial,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        lblSerialNo: strings.lblSerialNo,
        lblSerial: strings.lblSerial,
        lblNoRecoveryItem: strings.lblNoRecoveryItem
      },
    };

    this.debouceGotoConfirmationPage = _.debounce(() => this.goto_confirmationPage(), globalConfig.defaultDelayTime,
    {
      'leading': true,
      'trailing': false
    })
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      title: this.props.name,
      screen: 'DialogRetailerApp.views.WomDetailView',
      passProps: {
        List: this.props.List,
        name: this.props.name
      }
    });
  }

  //===NOT IN USE====
  getItemData() {
    const data = {
      // dtvAccNo: '60823771',
      dtv_acc_no: this.props.WOM.work_order_detail.summary[2].value,
      job_id: this.props.WOM.work_order_detail.job_id,
      app_flow: this.props.WOM.work_order_detail.app_flow,
      order_id: this.props.WOM.work_order_detail.order_id,
      action: "0",
      request_type: "EQU_REMOVE",
      cir: this.props.WOM.work_order_detail.cir
    };

    Utill.apiRequestPost('Remove', 'ConnectionRemoval', 'wom',
      data, this.getItemDataSuccess, this.getItemDataFailure, this.LoadDataExcb);
  }
  getItemDataSuccess = (response) => {
    console.log('#RecoveryItem', response);

    this.setState({ RecoveryItem: response.data.data.warranty_details, apiLoading: false })

    if (response.data.success) { this.setState({ showText: true }); }
  };
  getItemDataFailure = (response) => {
    this.setState({ api_error: true, api_error_message: response.data.error });
  };
  //==================

  setRadioButtonActivated(serial) {

    for (let index = 0; index < this.state.RecoveryItem.length; index++) {

      let item = Object.assign({}, this.state.RecoveryItem[index]); //creating copy of object
      //let item = this.state.RecoveryItem[index]; //Not working

      item.isTicked = item.isTicked ? true : false; //Set isTicked value to false by default
      console.log('HHHHHHHHHHHHHHHHHHHHHHHHHHH', item.isTicked);

      //If serials matches
      if (item.serial === serial) {

        let itemArray = this.state.RecoveryItem.map(itemx => itemx);
        // let itemArray = [this.state.RecoveryItem]; //Not working
        item.isTicked = true; //updating isTicked value
        itemArray[index] = item;
        console.log('XXXXXXXXXXXXXXX', itemArray[index]);

        this.setState({ RecoveryItem: itemArray, isConfirmable: this.checkIsConfirmable(itemArray) });
      }
    }
  }

  checkIsConfirmable(itemArray) {
    let confirmable = false;
    let categories = [];
    let TickedCategories = [];
    try {
      for (let i = 0; i < itemArray.length; i++) {
        let item = itemArray[i];
        if (!categories.includes(item.product_family) && (item.mandatory == 1)) {
          categories.push(item.product_family);
        }
        if (item.mandatory == 1 && (!TickedCategories.includes(item.product_family)) && item.isTicked) {
          TickedCategories.push(item.product_family);
        }
      }
    } finally {
      console.log('@@@@@@@@@@@@ CATEGORIES', categories);
      console.log('########### TICKED CATEGORIES', TickedCategories);
      if (categories.length == TickedCategories.length) {
        return true;
      } else {
        return false;
      }
    }
  }

  loadBarodeScanner() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.lblScanSerial,
      screen: 'DialogRetailerApp.views.WOMBarcodeScannerScreen',
      passProps: {
        title: this.state.locals.lblSerial,
        isReadOnly: true,
        successCb: (scannedSerial) => {
          Keyboard.dismiss();
          // scannedSerial = this.state.RemovalItem[0].serial;
          //scannedSerial = scannedSerial.serial;
          console.log('DSEEEEEEE', scannedSerial)
          this.setState({ scannedSerial: scannedSerial }, () => {
            if (scannedSerial && this.varifySerial(scannedSerial)) {
              this.setRadioButtonActivated(scannedSerial);
            } else {
              const navigatorOb = this.props.navigator;
              try {
                navigatorOb.showModal({
                  screen: 'DialogRetailerApp.models.GeneralExceptionSerialModal',
                  title: 'GeneralModalException',
                  passProps: {
                    lable: this.state.locals.lblSerialNo,
                    item: scannedSerial,
                    descriptionText: this.state.locals.serialdescription,
                    onRescanPress: () => { this.loadBarodeScanner();  }
                  },
                  overrideBackPress: true
                });
              } catch (error) {
                console.log(`Show Modal error: ${error.message}`);
                Keyboard.dismiss();
              }
            }
          });          
        }
      }
    });
  }

  varifySerial(scannedSerial) {
    let varifiedSerial = false;
    try {
      if (scannedSerial) {
        for (item of this.state.RecoveryItem) {
          if (item.serial === scannedSerial) {
            varifiedSerial = item.serial;
            break;
          }
        }
      }
    } finally {
      return varifiedSerial;
    }
  }

  onRefresh() {  this.setState({ refresh: false }); }

  goto_confirmationPage = () => {
    let filterdTickedList = this.state.RecoveryItem.filter((item) => { return (item.isTicked == true) });

    let sampleArray = [];
    let sampleCollection = [];
    filterdTickedList.map((item) => {
      let newArrayObj = [];
      let newObj1 = {
        label: this.state.locals.lblItemType,
        value: item.name
      };
      let newObj2 = {
        label: this.state.locals.lblSerial,
        value: item.serial
      };
      newArrayObj.push(newObj1);
      newArrayObj.push(newObj2);
      sampleCollection.push(newArrayObj);
    });

    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.WOMCustomerConfirmation',
      passProps: {
        dataArray: sampleCollection,
        RecoveryItem: filterdTickedList, //this.state.RecoveryItem,
        isRemoveList: true,
        isSignatureRequired: true,
        isRecoveryFlow: true,
      }
    });
  }

  renderListItem = ({ item }) => {
    // if(item.isTicked)
    return (
      <View style={styles.itemSeperator}>
        <View style={styles.detailElimentContainer}>
          <View style={styles.elementLeft}>
            <Checkbox key={item.isTicked} isChecked={item.isTicked} disabled={true} checkBoxColor={item.isTicked ? Colors.colorYellow : Colors.colorBlack} />
          </View>
          <View style={styles.elementMiddle}>
            <Text style={[styles.elementTxtSuccess, { fontWeight: 'bold' }]}>
              {item.name}
            </Text>
          </View>
          <View style={styles.elementRight}>
            <Text style={styles.elementTxtSuccess}>
              {item.serial}</Text>
          </View>
        </View>
      </View>
    )
  };
  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (

        <View style={styles.container}>

          <View style={styles.detailsContainer}>
            <View style={styles.containerMiddle}>
              <ScrollView
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isfetching}
                    onRefresh={this.onRefresh.bind(this)}
                  /> }
              >
                <FlatList
                  data={this.state.RecoveryItem}
                  onRefresh={this.getItemData}
                  ListEmptyComponent={
                    <View style={{ justifyContent: 'flex-end', alignItems: 'center' }} >
                      <Text>{this.state.locals.lblNoRecoveryItem}...</Text>
                    </View>
                  }
                  renderItem={this.renderListItem}
                  keyExtractor={(item, index) => item.toString() && index.toString()}
                  scrollEnabled={true}
                  refreshing={false}
                  extraData={this.state}
                />
              </ScrollView>
            </View>
            {this.state.showText === true ?
              <View style={styles.containerBottom}>
                <View style={styles.containerBottomSection1}>
                  <TouchableOpacity onPress={() => this.loadBarodeScanner()}>
                    <Image source={barcodeIcon} style={styles.iconStyle} />
                  </TouchableOpacity>
                </View>
                <View style={styles.containerBottomSection2}>
                  <TouchableOpacity style={styles.buttonCancelContainer}>

                  </TouchableOpacity>
                  {this.state.isConfirmable === true ?
                    <TouchableOpacity style={styles.buttonContainer} onPress={() => this.debouceGotoConfirmationPage()}>
                      <Text style={{ fontWeight: 'bold', color: Colors.black }}>
                        {this.state.locals.CONFIRM}
                      </Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity disabled={true} style={styles.filterTextDisabled} onPress={() => this.debouceGotoConfirmationPage()}>
                      <Text style={{ fontWeight: 'bold' }}>
                        {this.state.locals.CONFIRM}
                      </Text>
                    </TouchableOpacity>
                  }
                </View>
              </View>
              : true}
          </View>
          {this.state.showText === true ?
            <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
              <Image style={styles.helpImage} source={fabIcon} />
            </DragComponent>
            : true}
        </View>
      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {loadingIndicator}
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerTop: {
    marginLeft: 7,
    marginRight: 2,
    flex: 0.1,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  }, containerTopText: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    justifyContent: 'center',
  }, containerTopSection1: {
    flex: 0.5,
    flexDirection: 'column',
    margin: 7,
    alignItems: 'center'
  }, containerTopSection2: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerTopSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerTopSection4: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center'
  }, containerMiddle: {
    marginLeft: 2,
    marginRight: 2,
    height: '80%',
    flexDirection: 'row'
  }, containerMiddleSection1: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection2: {
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection4: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection5: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerBottom: {
    borderTopWidth: 1,
    borderRightColor: Colors.transparent,
    borderLeftColor: Colors.transparent,
    borderColor: Colors.borderLineColorLightGray,
    height: '15%',
    flexDirection: 'row'
  }, containerBottomSection1: {
    flex: 0.3,
    flexDirection: 'row',
    paddingLeft: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'center'
  }, containerBottomSection2: {
    flex: 0.7,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'center'
  },
  detailsContainer: {
    borderWidth: 0,
    paddingTop: 15
  },

  imageContainer: {
    alignItems: 'center'
  },

  imageStyle: {
    width: 50,
    height: 50,
    marginTop: 15,
    marginBottom: 5
  },

  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 5,
    paddingBottom: 10,
    margin: 10,
    marginRight: 0
  },

  dummyView: {
    flex: 0.6
  },

  dummyView3: {
    flex: 1.2

  },
  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'

  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
  },

  dummyView2: {
    flex: 0.2
  },

  buttonTxt: {
    color: Colors.colorBlack,
    textAlign: 'center',
    backgroundColor: Colors.colorTransparent
  }, titleText: {
    paddingTop: 8,
    paddingLeft: 8,
    color: Colors.colorBlack,
    textAlign: 'left',
    backgroundColor: Colors.colorTransparent,
    fontSize: 18,
  },

  detailElimentContainer: {
    height: 52,
    paddingTop: 10,
    flexDirection: 'row',
    borderWidth: 0,
    paddingBottom: 10
  },

  elementLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 10,
    flex: 0.1,
    borderWidth: 0
  },
  elementRight: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 0.6,
    borderWidth: 0,
    paddingRight: 20
  },
  elementMiddle: {
    flex: 0.4,
    justifyContent: 'center',
    paddingLeft: 10,
    borderWidth: 0,
  },
  elementTxt: {
    fontSize: 17
  },
  elementTxtSuccess: {
    fontSize: 15,
    color: Colors.black
  },
  elementTxtFaulty: {
    fontSize: 17,
    color: Colors.Colors
  },
  elementRightWithIcon: {
    flex: 1,
    borderWidth: 0,
    flexDirection: 'row'
  },
  iconStyle: {
    width: 50,
    height: 45,
    marginTop: 0,
    borderWidth: 1,
    marginBottom: 0,

  },
  itemSeperator: {
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: Colors.borderColorGray,
    borderBottomWidth: 0.5

  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: Colors.black,
    borderRadius: 5,
    textAlign: 'center'
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },

});
export default connect(mapStateToProps, actions)(RecoveryItemsView);
