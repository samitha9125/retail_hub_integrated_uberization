import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
  RefreshControl,
  Alert,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../../actions';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import strings from '../../Language/Wom';
import { Header } from './Header';
import globalConfig from '../../config/globalConfig';

const { height } = Dimensions.get('screen')
const Utill = new Utills();
class WomLandingView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      dispatchedArray: [],
      workORders: [],
      apiLoading: false,
      api_error: false,
      refresh: false,
      locals: {
        HomeMenu: strings.HomeMenu,
        screenTitle: strings.screenTitle,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        dispatchTitle: strings.dispatchTitle,
        inProgressTitle: strings.inProgressTitle,
        lblNICNo: strings.lblNICNo,
        lblType: strings.lblType,
        lblConnectionNo: strings.lblConnectionNo,
        lblSTBPack: strings.lblSTBPack,
        lblAccessoryPack: strings.lblAccessoryPackDetail,
        lblWarrantyPeriod: strings.lblWarrantyPeriod,
        locationErr: strings.locationErr,
        locationErrHeader: strings.locationErrHeader,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        lblError: strings.error
      }
    };
    this.debounceOnPressHandlerWOname = _.debounce((item) => this.OnPressHandlerWOname(item), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceOnPressHandlerWOnameDispatched = _.debounce((item) => this.OnPressHandlerWOnameDispatched(item), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });

    this.permission = null;
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.getLocationInfo()
    this.LoadData();
    this.props.workOrderRootCauseDataReset();
    this.props.clearTRBWO();
    this.props.clearWOStates();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    this.navigatorOb.resetTo({
      title: strings.HomeMenu,
      screen: 'DialogRetailerApp.views.HomeTileScreen'
    });
  }

  onRefresh() {
    this.setState({ refresh: false }, () => this.LoadData());
  }

  getLocationInfo() {
    Utill.getWOMUserLocation(this.props.Language)
      .then((location) => {
        console.log('location success', location);
      })
      .catch((response) => {

        Alert.alert('', response.message, [
          {
            text: 'OK',
            onPress: () => {
              this.setState({ apiLoading: true }, () => {
                this.timeout = setTimeout(() => {
                  this.setState({ apiLoading: false }, () => this.getLocationInfo())
                }, globalConfig.locationPopUpInterval)
              })
            }
          }
        ], { cancelable: false });
      });
  }

  LoadData = () => {
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('GetSummaryCount', 'WorkOrders', 'wom', {},
        this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
    });
  }

  LoadDataSuccessCb = (response) => {
    this.setState({
      apiLoading: false,
      api_error: false,
      workORders: response.data.data.PendingSummary,
      dispatchedArray: response.data.data.Dispatched
    }, () => this.dissmissNoNetworkModal());
  }

  LoadDataExcb = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.LoadData);
      } else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  LoadDataFailureCb = (response) => {
    this.setState({ apiLoading: false, api_error: true }, () => this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response));
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, response) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType,
        messageHeader,
        messageBody: response.message ? response.message : response.data.error ? response.data.error : response.data.info ? response.data.info : '',
        messageFooter: "",
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  OnPressHandlerWOname = (item) => {
    this.props.workOrderReload('');
    if (item.count == 1) {
      this.setState({ apiLoading: true }, () => {
        const data = { job_status: item.id };

        Utill.apiRequestPost('GetWorkOrders', 'WorkOrders', 'wom', data,
          this.LoadOrderDataSuccess, this.LoadDataFailureCb, this.LoadDataExcb);
      });
    } else {
      this.navigatorOb.push({
        screen: 'DialogRetailerApp.views.PendingWOdersView',
        passProps: {
          woNameParam: item.name,
          woId: item.id,
          load_filer_call: 1,
          dispatch_status: 'un_dispatched',
          count: item.count
        }
      });
    }
  }

  OnPressHandlerWOnameDispatched = (item) => {
    if (item.count == 1) {
      this.setState({ apiLoading: true }, () => {
        const data = { job_status: item.id };

        Utill.apiRequestPost('GetWorkOrders', 'WorkOrders', 'wom', data,
          this.LoadOrderDataSuccess, this.LoadDataFailureCb, this.LoadDataExcb);
      });
    } else {
      this.navigatorOb.push({
        screen: 'DialogRetailerApp.views.PendingWOdersView',
        passProps: {
          woNameParam: item.name,
          woId: item.id,
          load_filer_call: 1,
          dispatch_status: 'dispatched',
          count: item.count
        }
      });
    }
  }

  LoadOrderDataSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.success) {
        let item = response.data.data[0];
        let viewTiltle = "";

        if (item.job_status == "DISPATCHED") {
          viewTiltle = this.state.locals.dispatchTitle;
        } else if (item.job_status == "IN_PROGRESS") {
          viewTiltle = this.state.locals.inProgressTitle;
        }

        this.setWOMDetails(item);

        this.navigatorOb.push({
          title: viewTiltle,
          screen: 'DialogRetailerApp.views.WomDetailView',
          passProps: { woId: item.id, woNameParam: viewTiltle, count: '1' }
        });
      }
    });
  }

  setWOMDetails(entry) {
    this.props.workOrderGetDetail(entry);
    const nic = entry.nic;
    const conn_type = entry.conn_type;
    const conn_no = entry.conn_no;
    const warrenty = entry.warrenty;
    const stbPack = '';
    const accessoryType = '';

    let ProvArray = [
      {
        "label": this.state.locals.lblNICNo,
        value: nic
      },
      {
        "label": this.state.locals.lblType,
        value: conn_type
      },
      {
        "label": this.state.locals.lblConnectionNo,
        value: conn_no
      },
      {
        "label": this.state.locals.lblSTBPack,
        value: stbPack
      },
      {
        "label": this.state.locals.lblAccessoryPack,
        value: accessoryType
      },
      {
        "label": this.state.locals.lblWarrantyPeriod,
        value: warrenty
      }
    ];

    this.props.workOrderGetDTVProvisioning(ProvArray);
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating />
        </View>
      );
    }
  }

  render() {
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (
        <View style={styles.wholeContain}>
          {this.showIndicator()}
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refresh}
                onRefresh={this.onRefresh.bind(this)}
              />
            }>
            <View style={styles.workOrderTilesContainer}>
              <FlatList
                disabled={true}
                data={this.state.workORders}
                keyExtractor={(x, i) => i.toString()}
                renderItem={({ item, index }) => (
                  <View
                    key={index}>
                    {console.log("item", item.count)}
                    {item.count != 0 ?
                      <TouchableOpacity onPress={() => this.debounceOnPressHandlerWOname(item)} >
                        <View style={styles.woTileRow}>
                          <View>
                            <Text style={styles.woNameText}>{item.name}</Text>
                          </View>
                          <View style={styles.countView}>
                            <Text style={styles.countText}>{item.count}</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      :
                      <View style={styles.woTileRowDisabled}>
                        <View>
                          <Text style={styles.woNameText}>{item.name}</Text>
                        </View>
                      </View>}
                  </View>)}
              />
            </View>
            <View style={styles.workOrderTilesContainerDispatched}>
              <FlatList
                disabled={true}
                data={this.state.dispatchedArray}
                keyExtractor={(x, i) => i.toString()}
                renderItem={({ item, index }) => (
                  <View
                    key={index}>
                    {console.log("item", item.count)}
                    {item.count != 0 ?
                      <TouchableOpacity onPress={() => this.debounceOnPressHandlerWOnameDispatched(item)} >
                        <View style={styles.woTileRow}>
                          <View>
                            <Text style={styles.woNameText}>{item.name}</Text>
                          </View>
                          <View style={styles.countView}>
                            <Text style={styles.countText}>
                              {item.count}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      :
                      <View style={styles.woTileRowDisabled}>
                        <View>
                          <Text style={styles.woNameText}>
                            {item.name}
                          </Text>
                        </View>
                      </View>}
                  </View>)}
              />
            </View>
          </ScrollView>
        </View>
      );

    } else {
      screenContent = (
        <View style={styles.wholeContain}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refresh}
                onRefresh={this.onRefresh.bind(this)}
              />
            }>
          </ScrollView>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={strings.screenTitle} />
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  indiView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height
  },
  wholeContain: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  woTileRow: {
    flexDirection: 'row',
    backgroundColor: '#ffc400',
    height: 70,
    alignItems: 'center',
    margin: 5,
    borderRadius: 10
  },
  workOrderTilesContainerDispatched: {
    marginTop: 30
  },
  countView: {
    position: 'absolute',
    right: 0,
    marginRight: 25,
    alignItems: 'center',
  },
  countText: {
    textAlign: 'right',
    alignItems: 'center',
    color: '#000000'
  },
  woNameText: {
    marginLeft: 12,
    fontSize: 18,
    fontWeight: '400',
    color: '#000000'
  },
  woTileRowDisabled: {
    backgroundColor: '#e1e1e1',
    height: 70,
    alignItems: 'center',
    margin: 5,
    borderRadius: 10,
    flexDirection: 'row'
  },
});

export default connect(mapStateToProps, actions)(WomLandingView);


