import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
class InputSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            sectionClick: false,
            deafaultState: true
        };
    }

    render() {
        const { customStyle, label } = this.props;
        let editIcon;
        if (this.props.colorCode) {
            editIcon = (<Ionicons
                name='ios-checkmark-circle-outline'
                size={Styles.statusIconSize}
                color={Colors.statusIconColor.success}/>);
        } else {
            editIcon = (<Ionicons
                name='ios-checkmark-circle-outline'
                size={Styles.statusIconSize}
                color={Colors.statusIconColor.pending}/>);
        }

        return (
            <View style={[styles.container, customStyle]}>
                <View style={[styles.innerTextContainer]}>
                    <Text style={styles.innerText}>{label}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 2,
        marginTop: 3,
        marginLeft: 2,
        marginRight: 2
    },
    innerImageContainer: {
        flex: 1,
        paddingBottom: 8,
        paddingTop: 8,
        backgroundColor: Colors.appBackgroundColor,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    innerTextContainer: {
        flex: 7,
        paddingBottom: 0,
        paddingTop: 0,
        paddingLeft: 1,
        backgroundColor: "transparent",
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    innerText: {
        color: Colors.btnActiveTxtColor,
        fontSize: Styles.defaultBtnFontSize
    }

});

export default InputSection;
