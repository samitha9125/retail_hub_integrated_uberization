import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, BackHandler, Image, ScrollView, NetInfo } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import Colors from './../../../config/colors';
import * as actions from '../../../actions/index';
import strings from './../../../Language/SIMSTBChange';
import strings2 from '../../../Language/Wom';
import TextItemComponent from '../components/TextItemComponent';
import RadioButton from '../components/RadioButtonComponent';
import DragComponent from '../../general/Drag';
import globalConfig from '../../../config/globalConfig';
import SerialListComponent from '../components/SerialListComponent';
import fabIcon from '../../../../images/icons/cfss/fab.png';
class SIMSTBChangeView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    strings2.setLanguage(this.props.Language);
    this.state = {
      gatherings: {
        oldSTB: {
          serial: ''
        },
        newSTB: {
          serial: ''
        },
        STBType: '',
        oldSIM: {
          serial: ''
        },
        newSIM: {
          serial: ''
        },
        isCardless: ''
      },
      refreshOldStbScanner: false,
      refreshOldSIMScanner: false,
      refreshNewSIMScanner: false,
      refreshNewSTBScanner: false,

      selectedCardType: 'N',
      dynemicView: props.propsData.change_type,  // use this to switch between SIM , STB, SIM & STB (BOTH) specific views.
      initData: props.propsData.warranty_details,
      locals: {
        oldSTBSerial: strings.oldSTBSerial,
        newSTBSerial: strings.newSTBSerial,
        oldSIMSerial: strings.oldSIMSerial,
        newSIMSerial: strings.newSIMSerial,
        STBType: strings.STBType,
        continue: strings.continue,
        cardfull: strings.lblCardfull,
        cardless: strings.lblCardless,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        network_error_message: strings.network_error_message,
        screenTitle: strings2.screenTitle
      },
    };
    this.navigatorOb = this.props.navigator;
    this.debouncegotoDetails = _.debounce(() => this.gotoDetails(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    this.navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.WomDetailView',
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: 'IN-PROGRESS WORKORDER',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => {
          this.navigatorOb.pop({
            animated: true,
            animationType: 'fade',
          });
        },
      }
    });
  }

  gotoDetails() {
    let sampleArray = [];
    if (this.state.dynemicView == 'STB') {
      sampleArray = [this.state.gatherings.newSTB];
    } else if (this.state.dynemicView == 'SIM') {
      sampleArray = [this.state.gatherings.newSIM];
    } else {
      this.state.gatherings.newSIM.serial ? this.state.gatherings.newSIM.serial == '' ? sampleArray = [this.state.gatherings.newSTB] : sampleArray = [this.state.gatherings.newSTB, this.state.gatherings.newSIM] : sampleArray = [this.state.gatherings.newSTB];
    }
    const formatArray = sampleArray.map((item) => {
      const newObj = {
        label: item.type,
        value: item.serial
      }
      return newObj
    })

    let newFormattedArray = [];
    newFormattedArray.push(formatArray)

    this.props.workOrderGetDTVProvisioning(newFormattedArray);
    this.navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.DTVInstallationItem',
      passProps: {
        serials: sampleArray,
        simstbChange: true,
        addNewHide: true
      }
    });
  }

  buttonEnable = () => {
    if (this.state.dynemicView == 'BOTH' &&
      (((this.state.gatherings.isCardless == 'N' && this.state.gatherings.oldSIM.serial !== '') ||
        (this.state.gatherings.isCardless == 'Y')) &&
        (this.state.selectedCardType == 'N' &
          (this.state.gatherings.newSTB.serial !== '' && this.state.gatherings.newSTB.serial !== undefined && this.state.gatherings.newSTB.serial !== null) &&
          (this.state.gatherings.newSIM.serial !== '' && this.state.gatherings.newSIM.serial !== undefined && this.state.gatherings.newSIM.serial !== null)) ||
        (this.state.selectedCardType == 'Y' & (this.state.gatherings.newSTB.serial !== '' && this.state.gatherings.newSTB.serial !== undefined && this.state.gatherings.newSTB.serial !== null)))) {
      return true
    }

    if (this.state.dynemicView == 'SIM' && (this.state.gatherings.newSIM.serial !== '' && this.state.gatherings.newSIM.serial !== undefined && this.state.gatherings.newSIM.serial !== null)) {
      return true;
    }

    if (this.state.dynemicView == 'STB' && (this.state.gatherings.newSTB.serial !== '' && this.state.gatherings.newSTB.serial !== undefined && this.state.gatherings.newSTB.serial !== null)) {
      return true
    }
  }

  onSelectCardStatus(value) {
    let gatherAlterObj = this.state.gatherings;
    if (this.state.dynemicView == 'BOTH') {
      gatherAlterObj.newSTB = { serial: '' };
      gatherAlterObj.newSIM = { serial: '' };
      this.setState({
        refreshNewSIMScanner: true,
        refreshNewSTBScanner: true,
        selectedCardType: value,
        gatherings: gatherAlterObj,
      });
    }
  }

  oldStbScannerResponse = (isValidated, response) => {
    let simObj;
    const isSIM = this.state.initData.find(item => {
      if (item.name == 'SIM') return true;
    });

    let gState = this.state.gatherings;

    if (isSIM !== undefined) {
      simObj = {
        action: isSIM.action,
        basic: isSIM.basic,
        product_family: isSIM.product_family,
        serialized: isSIM.serialized,
        serial: isSIM.serial,
        type: isSIM.type,
        name: isSIM.name
      }
      gState.oldSIM = simObj;
    }

    gState.oldSTB = response;
    gState.STBType = response.product_family;
    gState.isCardless = response.old_stb_card_less;
    this.setState({ gatherings: gState, refreshOldStbScanner: false });
  }

  oldSIMScannerResponse = (isValidated, response) => {
    let gState = this.state.gatherings;
    gState.oldSIM = response;
    this.setState({ gatherings: gState, refreshOldSIMScanner: false });
  }

  newSTBScannerResponse = (isValidated, response) => {
    let gState = this.state.gatherings;

    gState.newSTB = response;
    this.setState({ gatherings: gState, refreshNewSTBScanner: false });
  }

  newSIMScannerResponse = (isValidated, response) => {
    let gState = this.state.gatherings;

    gState.newSIM = response;
    this.setState({ gatherings: gState, refreshNewSIMScanner: false });
  }

  renderRadioButtons() {
    const { RaidoView, RadioStyle, radioButtonLabelStyle } = styles;

    return (
      <View style={RaidoView}>
        <View style={RadioStyle}>
          <RadioButton
            currentValue={this.state.selectedCardType}
            value={'Y'}
            outerCircleSize={20}
            innerCircleColor={Colors.radioBtn.innerCircleColor}
            outerCircleColor={Colors.radioBtn.outerCircleColor}
            onPress={(value) => this.onSelectCardStatus(value)}
          >
            <Text style={radioButtonLabelStyle}>{this.state.locals.cardless}</Text>
          </RadioButton>
        </View>
        <View style={RadioStyle}>
          <RadioButton
            currentValue={this.state.selectedCardType}
            value={'N'}
            outerCircleSize={20}
            innerCircleColor={Colors.radioBtn.innerCircleColor}
            outerCircleColor={Colors.radioBtn.outerCircleColor}
            onPress={(value) => this.onSelectCardStatus(value)}
          >
            <Text style={radioButtonLabelStyle}>{this.state.locals.cardfull}</Text>
          </RadioButton>
        </View>
      </View>
    );
  }

  stbTypeViewer() {
    if (this.props.WOM.work_order_detail.new_stb_model !== undefined && this.props.WOM.work_order_detail.new_stb_model !== null && this.props.WOM.work_order_detail.new_stb_model !== '' && this.state.gatherings.STBType !== undefined && this.state.gatherings.STBType !== null) {
      return (
        <View>
          <TextItemComponent label1={strings2.newSTBModel} label2={this.props.WOM.work_order_detail.new_stb_model} />
        </View>
      );
    } else {
      return (
        <View>
          <TextItemComponent label1={strings2.STB_Type} label2={this.props.WOM.work_order_detail.stb_type} />
        </View>
      );
    }
  }

  render() {
    const { formContainerStyle, container } = styles;
    const { gatherings } = this.state;
    return (
      <View style={container}>
        <ScrollView style={formContainerStyle}>
          <View style={{ flex: 1, paddingHorizontal: 20, paddingTop: 20 }} >
            {(this.state.dynemicView == 'BOTH' || this.state.dynemicView == 'STB') ?
              <View>
                <SerialListComponent
                  scanCounter={0}
                  actionName='validateSerialNew'
                  controllerName='serial'
                  moduleName='wom'
                  apiDisable={true}
                  manualLable={this.state.locals.oldSTBSerial}
                  serialData={{ type: 'STB', oldSerial: this.state.initData }}
                  navigatorOb={this.props.navigator}
                  isAllSerialsValidated={this.oldStbScannerResponse}
                  refreshData={this.state.refreshOldStbScanner}
                />
              </View>
              : true}
            {((this.state.dynemicView == 'BOTH' && gatherings.isCardless !== 'Y' && gatherings.isCardless !== '') || this.state.dynemicView == 'SIM') ?
              <View>
                <SerialListComponent
                  scanCounter={0}
                  actionName='validateSerialNew'
                  controllerName='serial'
                  moduleName='wom'
                  apiDisable={true}
                  manualLable={this.state.locals.oldSIMSerial}
                  serialData={{ type: 'SIM', oldSerial: this.state.initData }}
                  navigatorOb={this.props.navigator}
                  isAllSerialsValidated={this.oldSIMScannerResponse}
                  refreshData={this.state.refreshOldSIMScanner}
                />
              </View>
              : true}
            {/* dropdown starts */}
            {(gatherings.STBType.length > 0) ?
              <View style={{ width: '125%', marginLeft: -38 }}>
                {this.stbTypeViewer()}
              </View>
              : true}

            {/* STB cardless/cardfull radio buttons */}
            {(this.state.gatherings.oldSTB.serial !== '' && (this.state.dynemicView == 'STB' || this.state.dynemicView == 'BOTH')) ? //Change to N
              this.renderRadioButtons() : true}

            {/* newSTBSerial */}
            {(this.state.gatherings.oldSTB.serial !== '' && (this.state.selectedCardType == 'N' || this.state.selectedCardType == 'Y') && (this.state.dynemicView == 'BOTH' || this.state.dynemicView == 'STB')) ?
              <View>
                <SerialListComponent
                  actionName='validateSerialNew'
                  controllerName='serial'
                  moduleName='wom'
                  apiParams={{
                    ownership: this.props.WOM.work_order_detail.ownership,
                    order_id: this.props.WOM.work_order_detail.order_id,
                    wo_type: this.props.WOM.work_order_detail.wo_type,
                    app_flow: this.props.WOM.work_order_detail.app_flow
                  }}
                  manualLable={this.state.locals.newSTBSerial}
                  serialData={{ ...this.state.gatherings.oldSTB }}
                  navigatorOb={this.props.navigator}
                  isAllSerialsValidated={this.newSTBScannerResponse}
                  refreshData={this.state.refreshNewSTBScanner}
                />
              </View>
              : true}

            {/* newSIMSerial */}
            {(this.state.gatherings.oldSTB.serial !== '' && this.state.dynemicView == 'BOTH' && this.state.selectedCardType == 'N' || (this.state.dynemicView == 'SIM' && this.state.gatherings.oldSIM.serial !== '')) ?
              <View style={{ flex: 1 }} >
                <SerialListComponent
                  actionName='validateSerialNew'
                  controllerName='serial'
                  moduleName='wom'
                  apiParams={{
                    ownership: this.props.WOM.work_order_detail.ownership,
                    order_id: this.props.WOM.work_order_detail.order_id,
                    wo_type: this.props.WOM.work_order_detail.wo_type,
                    app_flow: this.props.WOM.work_order_detail.app_flow
                  }}
                  manualLable={this.state.locals.newSIMSerial}
                  serialData={{ ...this.state.gatherings.oldSIM }}
                  navigatorOb={this.props.navigator}
                  isAllSerialsValidated={this.newSIMScannerResponse}
                  refreshData={this.state.refreshNewSIMScanner}
                />
              </View>
              : true}
          </View>
        </ScrollView>
        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            {this.buttonEnable() ?
              <TouchableOpacity style={styles.buttonContainer} onPress={() => this.debouncegotoDetails()}>
                <Text style={styles.continueBtn}>{this.state.locals.continue}</Text>
              </TouchableOpacity> :
              <TouchableOpacity disabled={true} style={styles.buttonContainerDisabled}>
                <Text style={styles.acceptbtn}>{this.state.locals.continue}</Text>
              </TouchableOpacity>}
          </View>
        </View>
        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={fabIcon} />
        </DragComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  bottomContainer: { flex: 0.15 },
  formContainerStyle: {
    flex: 0.85,
    paddingHorizontal: 20,
    paddingTop: 20
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
    marginTop: 8,
  },
  buttonContainer: {
    paddingVertical: 10,
    paddingHorizontal: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnActive,
    borderRadius: 5,
  },
  buttonContainerDisabled: {
    paddingVertical: 10,
    paddingHorizontal: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    borderRadius: 5,
  },
  continueBtn: {
    fontWeight: 'bold',
    color: 'black'
  },
  acceptbtn: {
    fontWeight: 'bold'
  },
  RaidoView: {
    marginVertical: 25,
    flexDirection: 'row',
  },
  RadioStyle: { flex: 0.8 },
  radioButtonLabelStyle: {
    marginLeft: 10,
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  }
});

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const SIM = state.sim;
  return { Language, WOM, SIM };
};

export default connect(mapStateToProps, actions)(SIMSTBChangeView);