import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  BackHandler,
  FlatList,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import _ from 'lodash'

import Utills from '../../../utills/Utills';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import strings from '../../../Language/Wom';
import CardTRB from '../dtvInstallation/CardView';
import DragComponent from '../../general/Drag';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import { Header } from '../Header';
import { globalConfig } from '../../../config';

var { width } = Dimensions.get('window');
const Utill = new Utills();
class InitialTRBView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      isEditable: false,
      itemList: [],
      provisionAttempts: 0,
      apiLoading: false,
      locals: {
        btnAddNew: strings.btnAddNew,
        btnConfirm: strings.btnConfirm,
        btnEdit: strings.btnEdit,
        btnDelete: strings.btnDelete,
        cancel: strings.cancel,
        lblNoChangedEqp: strings.lblNoChangedEqp,
        msgError: strings.error,
        closeCIR: strings.closeCIR,
        screenTitle: strings.screenTitle,
        inProgressTitle: strings.inProgressTitle,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        network_error_message: strings.network_error_message,
      },
    };
    this.navigatorOb = this.props.navigator;
    this.debouceForwardConfirmAction = _.debounce(() => this.forwardConfirmAction(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      })
      this.debouceForwardAddNewAction = _.debounce(() => this.forwardAddNewAction(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      })
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    if (this.props.successRefixAddNewItem) {
      let newItemtoAdd = this.props.successRefixAddNewItem;
      let newList = this.state.itemList;

      const isChecked = false;
      const isNewItemArray = Array.isArray(newItemtoAdd);
      if (isNewItemArray) {
        let itemList = this.state.itemList;
        for (let i = 0; i < newItemtoAdd.length; i++) {
          let isReplaced = false;
          newList = itemList.map(item => {
            if (item.type == newItemtoAdd[i].type) {
              isReplaced = true;
              return newItemtoAdd[i];
            } else {
              return item;
            }
          });

          if (!isReplaced) {
            newList.push(newItemtoAdd[i])
          }
          itemList = newList;
        }
      } else {
        if (newItemtoAdd.sale_type == 'REPLACE') {
          let isReplaced = false;
          if (this.props.isRemoveSIM) {
            newList = this.state.itemList.filter(item => {
              if (item.type !== 'SIM') {
                return true;
              }
            });
            const obj = { ...newItemtoAdd, isChecked };
            newList.push(obj);
          } else {
            newList = this.state.itemList.map((item) => {
              if (item.type == newItemtoAdd.type) {
                isReplaced = true;
                const obj = { ...newItemtoAdd, isChecked };
                return obj;
              } else {
                const obj = { ...item, isChecked };
                return obj;
              }
            });
            const obj = { ...newItemtoAdd, isChecked };
            !isReplaced ? newList.push(obj) : true;
          }
        } else {
          let isReplaced = false;
          const obj = { ...newItemtoAdd, isChecked };
          newList = this.state.itemList.map(item => {
            if (item.type == newItemtoAdd.type && item.serial == newItemtoAdd.serial) {
              isReplaced = true;
              return newItemtoAdd;
            } else {
              return item;
            }
          });
          if (!isReplaced) newList.push(obj)
        }
      }
      this.setState({ itemList: newList });
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.props.clearTRBWO();
    this.navigatorOb.pop();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  _editPressed = () => {
    const itemList = this.state.itemList.map(item => {
      let obj = { ...item, isChecked: false }
      return obj;
    });
    this.setState({
      itemList,
      isEditable: !this.state.isEditable
    })
  }

  _cancelPressed = () => {
    const itemList = this.state.itemList.map(item => {
      let obj = { ...item, isChecked: false }
      return obj;
    })
    this.setState({
      itemList,
      isEditable: !this.state.isEditable
    })
  }

  _deletePressed = () => {
    const itemArray = this.state.itemList;

    const itemList = itemArray.filter(item => {
      if (item.isChecked == false) return item;
    })
    this.setState({
      itemList,
      isEditable: !this.state.isEditable
    })
  }

  checkBoxValueChange = (item) => {
    let newArray = [];
    if (item.isChecked === true) {
      const isChecked = false;
      newArray = this.state.itemList.map(value => {
        if (value.root_id == item.root_id) {
          let newObj = { ...value, isChecked };
          return newObj;
        }
        else return value;
      });
    } else {
      const isChecked = true;
      newArray = this.state.itemList.map(value => {
        if (value.root_id == item.root_id) {
          let newObj = { ...value, isChecked };
          return newObj;
        }
        else return value;
      });
    }
    this.setState({ itemList: newArray });
  }

  forwardAddNewAction() { this.addItemforTRBWO(); }

  addItemforTRBWO() {
    let replacedList = this.state.itemList;

    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.TRBReplacmentScreen',
      passProps: {
        screenTitle: this.state.locals.inProgressTitle,
        successAddNewItem: (newItemtoAdd, isRemoveSIM) => {
          let newList = this.state.itemList;

          const isChecked = false;
          const isNewItemArray = Array.isArray(newItemtoAdd);
          if (isNewItemArray) {
            let itemList = this.state.itemList;
            for (let i = 0; i < newItemtoAdd.length; i++) {
              let isReplaced = false;
              newList = itemList.map(item => {
                if (item.type == newItemtoAdd[i].type) {
                  isReplaced = true;
                  const obj = { ...newItemtoAdd[i], isChecked };

                  return obj;
                } else {
                  const obj = { ...item, isChecked };
                  return obj;
                }
              });

              if (!isReplaced) {
                const obj = { ...newItemtoAdd[i], isChecked };
                newList.push(obj)
              }
              itemList = newList;
            }
          } else {
            if (newItemtoAdd.sale_type == 'REPLACE') {
              let isReplaced = false;
              if (isRemoveSIM) {
                newList = this.state.itemList.filter(item => {
                  if (item.type !== 'SIM') {
                    return true;
                  }
                });
                const obj = { ...newItemtoAdd, isChecked };
                newList.push(obj);
              } else {
                newList = this.state.itemList.map((item) => {
                  if (item.type == newItemtoAdd.type) {
                    isReplaced = true;
                    const obj = { ...newItemtoAdd, isChecked };
                    return obj;
                  } else {
                    const obj = { ...item, isChecked };
                    return obj;
                  }
                });
                const obj = { ...newItemtoAdd, isChecked };
                !isReplaced ? newList.push(obj) : true;
              }
            } else {
              let isReplaced = false;
              newList = this.state.itemList.map(item => {
                if (item.type == newItemtoAdd.type && item.serial == newItemtoAdd.serial) {
                  isReplaced = true;
                  const obj = { ...newItemtoAdd, isChecked };
                  return obj;
                } else {
                  const obj = { ...item, isChecked };
                  return obj;
                }
              });

              if (!isReplaced) {
                const obj = { ...newItemtoAdd, isChecked };
                newList.push(obj);
              }
            }
          }
          this.setState({ itemList: newList });
        },
        replacedList: replacedList
      }
    });
  }

  forwardConfirmAction() {
    if (this.state.itemList.length == 0) {
      this.gotoRootCauseView();
    } else {
      const itemArray = this.state.itemList.map((item, index) => {
        return { ...item, index: index }
      });

      this.props.workOrderTRBSales(itemArray);

      let bill = this.calculateBill(itemArray);

      let paymentActive;
      if (bill > 0) { paymentActive = true }
      else { paymentActive = false }

      this.props.workOrderSerialList(itemArray);
      this.props.workOrderGetInstalledItems(itemArray);

      this.navigatorOb.push({
        title: this.state.locals.inProgressTitle,
        screen: 'DialogRetailerApp.views.WOMCustomerConfirmation',
        passProps: {
          dataArray: itemArray,
          isPaymentAvailable: paymentActive,
          isSignatureRequired: false,
          totalGrand: bill,
        }
      });
    }
  }

  gotoRootCauseView() {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: 'alertNoHeader',
        messageHeader: this.state.locals.closeCIR,
        messageBody: '',
        messageFooter: '',
        btnFun: () => { this.doProvision() }
      },
      overrideBackPress: true
    });
  }

  doProvision() {
    let provAttempts = this.state.provisionAttempts

    const data = {
      app_flow: this.props.WOM.work_order_app_flow,
      cir: this.props.WOM.work_order_detail.cir,
      dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
      order_id: this.props.WOM.work_order_detail.order_id,
      job_id: this.props.WOM.work_order_detail.job_id,
      ccbs_order_id: this.props.WOM.work_order_detail.ccbs_order_id,
      request_type: this.props.WOM.work_order_detail.wo_type,
      ownership: this.props.WOM.work_order_detail.ownership,
      retry_attempt: provAttempts,
      pay_amount: '',
      pay_mode: '',
      product_detail: this.props.WOM.work_order_detail.product_detail,
      ezcash_pin: '',
      biz_unit: this.props.WOM.work_order_detail.biz_unit,
      sub_area: this.props.WOM.work_order_detail.sub_area,
      contactNo: this.props.WOM.work_order_detail.contactNo
    };

    this.setState({
      apiLoading: true,
      provisionAttempts: parseInt(provAttempts) + 1
    }, () => {
      Utill.apiRequestPost('ProvisioningInstallation', 'DtvProvision', 'wom', data, this.doProvisionSuccessCB, this.doProvisionFailureCB, this.doProvisionExCB);
    });
  }

  doProvisionSuccessCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.warning_msg == true) {
        this.navigatorOb.push({
          title: this.state.locals.inProgressTitle,
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.msgError,
            messageBody: response.data.info,
            messageFooter: '',
            onPressOK: () => {
              this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
            }
          }
        });
      } else if (response.data.retry == true) {
        this.doProvision();
      } else {
        if (response.data.data.app_screen) {
          this.props.workOrderAppScreen(response.data.data.app_screen);
          this.props.getSignatureMobileAct(null);
          this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
        }
      }
    });
  }

  doProvisionFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('error', this.state.locals.msgError, response.data.error, '');
    });
  }

  doProvisionExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('error', this.state.locals.msgError, response, '');
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: 'IN-PROGRESS WORKORDER',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { this.navigatorOb.pop() }
      }
    });
  }

  calculateBill(dataArray) {
    let total = 0;
    for (const i = 0; i < dataArray.length; i++) {
      let price = dataArray[i].price;
      if (price) {
        total += parseFloat(price);
      }
    }

    return total.toFixed(2);
  }

  renderListItem = ({ item, index }) => {
    console.log('---------------renderListItem ', item);
    return (
      <View
        key={index}
        style={styles.cardComponentView}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {this.state.isEditable === true ?
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 0.1 }}>
                <View>
                  {item.isChecked === true ?
                    <CheckBox
                      isChecked={item.isChecked}
                      style={{ paddingTop: 50, paddingLeft: 10 }}
                      checkBoxColor={Colors.yellow}
                      onClick={() => this.checkBoxValueChange(item)} />
                    :
                    <CheckBox
                      isChecked={item.isChecked}
                      style={{ paddingTop: 50, paddingLeft: 10 }}
                      checkBoxColor={Colors.grey}
                      onClick={() => this.checkBoxValueChange(item)} />}
                </View>
              </View>
              <View style={{ flex: 0.9 }}>
                <CardTRB
                  title={item.name}
                  qtyLable={strings.Qty}
                  lable1={strings.Serial_no}
                  Item1={item.serial}
                  lable2={strings.trbLblWarranty}
                  Item2={item.warrantyPeriod}
                  qty={item.qty}
                  price={item.price == '0' || item.price == '' || item.price == 0 ? < Text > {strings.free_of_change}</Text> : <Text>Rs{'  '}{item.price}</Text>}
                />
              </View>
            </View>
            :
            <View>
              <CardTRB
                title={item.name}
                qtyLable={strings.Qty}
                lable1={strings.Serial_no}
                Item1={item.serial}
                lable2={strings.trbLblWarranty}
                Item2={item.warrantyPeriod}
                qty={item.qty}
                price={item.price == '0' || item.price == '' || item.price == 0 ? < Text > {strings.free_of_change}</Text> : <Text>Rs{'  '}{item.price}</Text>}
              />
            </View>}
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        <View style={styles.topContainer}>
          {this.state.itemList.length > 0 ?
            <View style={{ alignItems: 'flex-end' }}>
              {this.state.isEditable === true ? (
                <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                  <TouchableOpacity onPress={() => this._cancelPressed()}>
                    <Text style={styles.CancelText}>{this.state.locals.cancel}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this._deletePressed()}>
                    <Text style={styles.DeleteText}>{this.state.locals.btnDelete}</Text>
                  </TouchableOpacity>
                </View>
              ) : null}
              <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                <TouchableOpacity onPress={() => this._editPressed()}>
                  {this.state.isEditable === false ? (
                    <Text style={styles.EditText}>{this.state.locals.btnEdit}</Text>
                  ) : null}
                </TouchableOpacity>
              </View>
            </View>
            : true}
        </View>
        <View style={styles.middleContainer}>
          {this.state.itemList.length == 0 ?
            <View style={{ alignSelf: 'center', }}>
              <Text style={{ justifyContent: 'center' }}>{this.state.locals.lblNoChangedEqp}</Text>
            </View>
            :
            <View >
              <FlatList
                data={this.state.itemList}
                keyExtractor={(x, i) => i.toString()}
                renderItem={this.renderListItem}
                scrollEnabled={true}
                extraData={this.state}
              />
            </View>}
        </View>
        <View style={styles.bottomContainer}>
          {this.state.itemList.length > 0 ?
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }} >
              <Text style={styles.grandTotalText}>Total cost</Text>
              <Text style={styles.grandTotalText}>Rs {this.calculateBill(this.state.itemList)}</Text>
            </View>
            : true}
          <View style={styles.direbutRow}>
            <View style={styles.dirImageView}>
            </View>

            <View style={styles.filterButView}>
              <View>
                <TouchableOpacity
                  onPress={() => this.debouceForwardAddNewAction()}
                  disabled={this.state.isEditable == true ? true : false}>
                  <Text style={this.state.isEditable == true ? styles.filterTextDisabled : styles.filterText}>{this.state.locals.btnAddNew}</Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  onPress={() => this.debouceForwardConfirmAction()}
                  disabled={this.state.isEditable == true ? true : false}>
                  <Text style={this.state.isEditable == true ? styles.filterTextDisabled : styles.filterText}>{this.state.locals.btnConfirm}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={fabIcon} />
        </DragComponent>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  grandTotalText: {
    color: Colors.black,
    fontSize: 16,
    lineHeight: 19,
    fontWeight: '500'
  },
  container: {
    height: '100%',
    backgroundColor: Colors.appBackgroundColor
  },
  topContainer: {
    flex: 0.05,
    marginBottom: 10
  },
  middleContainer: {
    flex: 0.8,
    borderBottomColor: Colors.borderLineColorLightGray,
  },
  bottomContainer: {
    flex: 0.15,
    //flexDirection: 'row'
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    marginTop: 12,
  },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 12,
    // paddingTop: 20,
    // borderTopWidth: 1,
    // borderTopColor: '#eeeeee',
    paddingBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  image_c1: {
    width: 20,
    height: 20,
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  EditText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 10
  },
  CancelText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 15
  },
  DeleteText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 10
  },
  helpImage: {
    height: 40,
    width: 40,
    resizeMode: 'stretch',
  },
  cardComponentView: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10
  },
});

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

export default connect(mapStateToProps, actions)(InitialTRBView);