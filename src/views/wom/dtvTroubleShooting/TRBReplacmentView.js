import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  BackHandler,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from './../ActIndicator';
import DragComponent from './../../general/Drag';
import strings from '../../../Language/Wom';
import strings2 from '../../../Language/WarrantyReplacement';
import DropDownInput from '../components/DropDownInput';
import TextFieldComponent from '../components/TextFieldComponent';
import SerialListComponent from '../components/SerialListComponent';
import RadioButton from '../components/RadioButtonComponent';
import globalConfig from '../../../config/globalConfig';

const Utill = new Utills();
var { width, height } = Dimensions.get('window');
class TRBReplacmentView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    strings2.setLanguage(this.props.Language);
    this.state = {
      requestType: [
        {
          value: strings.lblReplacement,
          id: 'REPLACE'
        },
        {
          value: strings.lblAdditionaSale,
          id: 'ADDITIONAL'
        }
      ],

      refreshOldSTBData: false,
      refreshOldSIMData: false,
      refreshNewSIMSerial: false,
      refreshNewSerialData: false,
      refreshSKUData: false,
      refreshNewSerial: false,
      refreshOldpowerSupply: false,
      refreshOldRcuSerial: false,
      main_item: {},
      itemType: [],
      newItemType: [],
      offers: [],
      card_less: '',
      oldSIM: '',
      newSIMSerial: '',
      isViewHidden: true,

      requestTypeValue: [],
      requestTypeIndexs: [],
      selectedRequestType: '',
      selectedRequestTypeID: '',

      itemTypeValues: [],
      itemTypeIndexes: [],
      itemTypeObject: [],
      selectedItemType: '',
      selectedItemTypeID: '',
      selectedItemTypeObject: '',

      newItemTypeValues: [],
      newItemTypeIndexes: [],
      newItemTypeObjct: [],
      selectednewItemType: '',
      selectednewItemTypeID: '',
      selectednewItemObject: '',

      offerValues: [],
      offerCode: [],
      offerObject: [],
      selectedOffer: '',
      selectedofferCode: '',
      selectedofferObject: '',

      contBtnEnable: false,
      isAssarySale: false,
      oldBarcode: '',
      itemPrice: '',
      itemWarranty: '',
      oldRCUBarCode: '',
      oldPowerCode: '',
      validationSerial: '',
      selectedStatus: null,
      newBarcode: '',
      powerSupplyEnable: false,
      rcuEnable: false,
      locals: {
        continue: strings.continue,
        trbRqtype: strings.trbRqtype,
        trbItemType: strings.trbItemType,
        cancel: strings.cancel,
        cardfull: strings.cardfull,
        cardless: strings.cardless,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        lblOldRCUSerial: strings.lbloldRCUSerial,
        lblOldPowerSupplySerial: strings.lblOldPowerSupplySerial,
        lblError: strings.error,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        lblWarrantyAvailable: strings2.warrentyText1,
        lblSerialNo: strings2.Serialnumber,
        lblWarrantyExp: strings2.lblWarrantyPeriodExpired,
        lblWarrantyAvailableMsg: strings2.warrentyText2,
        lblWarrantyExpMsg: strings2.notEligibleWarranty,
        lblNew: strings.lblNew,
        lblType: strings.lblType,
        lblOffers: strings.lblOffer,
        lblPrice: strings.lblPrice,
        lblWarranty: strings.lblWarranty,
        screenTitle: strings.screenTitle,
        optionalOldLabletoReplace: strings.optionalOldLabletoReplace,
        newLabelToReplace: strings.newLabelToReplace,
        oldLabletoReplace: strings.oldLabletoReplace,
      },
    };
    this.navigatorOb = this.props.navigator;
    this.debounceaddNewAccessory = _.debounce(() => this.addNewAccessory(), globalConfig.buttonTapDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.format_RequestItem_Dropdown();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.pop();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  format_RequestItem_Dropdown() {
    let requestTypeValue = [];
    let requestTypeIndexs = [];

    this.state.requestType.map((item, key) => {
      requestTypeValue.push(item.value);
      requestTypeIndexs.push(item.id);
    });

    this.setState({ requestTypeValue, requestTypeIndexs });
  }

  async select_RequestItem(item) {
    await this.setState({
      refreshOldSTBData: true,
      refreshOldSIMData: true,
      refreshNewSerialData: true,
      contBtnEnable: false,
      selectedRequestType: this.state.requestTypeValue[item],
      selectedRequestTypeID: this.state.requestTypeIndexs[item],
      isAssarySale: false,
      selectedItemTypeID: '',
      selectedItemType: '',
      selectednewItemType: '',
      oldBarcode: '',
      selectedOffer: '',
      selectedofferCode: '',
      selectedofferObject: '',
      selectednewItemObject: '',
      card_less: ''
    }, () => {
      this.state.selectedRequestTypeID == 'ADDITIONAL' ?
        this.getCPETypes()
        :
        [this.state.selectedRequestTypeID == 'REPLACE' ? this.getAccessoryTypes() : true]
    });
  }

  getAccessoryTypes() {
    this.setState({ apiLoading: true }, () => {
      const data = {
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id,
        app_flow: this.props.WOM.work_order_app_flow,
        request_type: this.props.WOM.work_order_detail.wo_type
      }
      Utill.apiRequestPost('getAccessoryTypes', 'warrantyReplacement', 'cfss',
        data, this.getAccssryTypeSuccessCB, this.getAccssryTypeFailureCB, this.getAccssryTypeExCB);
    })
  }

  getAccssryTypeSuccessCB = (response) => {
    this.setState({ apiLoading: false, itemType: response.data.data }, () =>
      this.format_itemType_Dropdown());
  }

  getAccssryTypeFailureCB = (response) => {
    this.setState({ apiLoading: false }, () =>
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, ''));
  }

  getAccssryTypeExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  getCPETypes() {
    this.setState({ apiLoading: true }, () => {
      const data = {
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id,
        app_flow: this.props.WOM.work_order_app_flow,
        request_type: this.props.WOM.work_order_detail.wo_type
      };
      Utill.apiRequestPost('GetProduct', 'AccessorySale', 'cfss',
        data, this.getCPETypesSuccess, this.getCPETypesFailed, this.getCPETypesEx);
    });
  }

  getCPETypesSuccess = (response) => {
    this.setState({ apiLoading: false, itemType: response.data.sale_items },
      () => this.format_itemType_Dropdown());
  }

  getCPETypesFailed = (response) => {
    this.setState({ apiLoading: false }, () => this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, ''));
  }

  getCPETypesEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }

  format_itemType_Dropdown() {
    let itemTypeValues = [];
    let itemTypeIndexes = [];
    let itemTypeObject = [];

    this.state.itemType.map((item, key) => {
      itemTypeValues.push(item.name);
      itemTypeIndexes.push(item.type);
      itemTypeObject.push(item)
    });

    this.setState({ itemTypeValues, itemTypeIndexes, itemTypeObject });
  }

  async select_ItemType(item) {
    await this.setState({
      refreshOldSTBData: true,
      refreshOldSIMData: true,
      refreshNewSerialData: true,
      refreshSKUData: true,
      refreshNewSIMSerial: true,
      selectedItemType: this.state.itemTypeValues[item],
      selectedItemTypeID: this.state.itemTypeIndexes[item],
      selectedItemTypeObject: this.state.itemTypeObject[item],
      isAssarySale: false,
      selectednewItemType: '',
      selectedOffer: '',
      selectedofferCode: '',
      contBtnEnable: false,
      selectedofferObject: '',
      selectednewItemObject: '',
      card_less: '',
      oldBarcode: '',
      itemPrice: '',
      itemWarranty: '',
      offerValues: [],
      oldRCUBarCode: '',

      oldPowerCode: '',

      newBarcode: ''

    }, () => this.checkItemType());
  }
  checkItemType() {
    if (this.props.WOM.work_order_detail.ownership == 'DialogOwned' && this.state.selectedItemTypeObject.provisionable == 'N') {
      this.setState({ oldBarcode: 'true' })
    }
  }

  checkForDuplications(item) {
    const replacedList = this.props.isRefix ? [] : this.props.replacedList;
    let selection;
    if (replacedList.length == 0) {
      this.addNewItem(item, false);
    } else {
      replacedList.map(replacedItem => {
        if (replacedItem.type == 'STB' && (replacedItem.card_less && replacedItem.card_less == 'Y') && item.type == 'SIM') {
          selection = 'cannot_add_sim';
          // cannot add sim alert should display
        } else if (replacedItem.type == 'STB' && (replacedItem.card_less && replacedItem.card_less == 'N') && item.type == 'SIM') {
          selection = 'replace_sim';
          // replace old sim display alert
        } else if (replacedItem.type == 'SIM' && item.type == 'STB' && (item.card_less && item.card_less == 'Y')) {
          selection = 'remove_sim';
          // remove old sim with a pop up
        } else if (replacedItem.type == 'SIM' && item.type == 'STB' && (item.card_less && item.card_less == 'N')) {
          selection = 'replace_sim';
          // replace old sim display alert
        } else if (replacedItem.type == item.type && replacedItem.sale_type == 'REPLACE') {
          selection = 'replace_item';
          // display alert to replace item 
        } else if (replacedItem.type == item.type && replacedItem.serial == item.serial) {
          selection = 'already_added';
        } else {
          selection = 'add_new_item';
        }
      });
      this.displayReplaceAlert(selection, item);
    }
  }

  displayReplaceAlert(selection, item) {
    switch (selection) {
      case 'cannot_add_sim':
        this.displayDuplicationAlert('alertWithOK', '', `Cannot replace a SIM. Previously cardless STB replaced.`, '', '');
        break;
      case 'replace_sim':
        this.displayDuplicationAlert('alert', '', `Do you want to replace previously replaced SIM to replace cardfull STB ?`, '', item);
        break;
      case 'remove_sim':
        this.displayDuplicationAlert('alert', '', `Do you want to remove previously replaced SIM to replace cardless STB ?`, '', item, isRemoveSIM = true);
        break;
      case 'replace_item':
        this.displayDuplicationAlert('alert', '', `Do you want to replace previously replaced item ?`, '', item);
        break;
      case 'add_new_item':
        this.addNewItem(item, false);
        break;
      case 'already_added':
        this.displayDuplicationAlert('alertWithOK', '', 'You have already added the item ?', '', '');
        break;
      default:
        console.log('');
    }
  }

  displayDuplicationAlert(messageType, messageHeader, messageBody, messageFooter, item, isRemoveSIM = false) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => {
          this.setState({
            selectedItemType: '',
            selectedItemTypeID: '',
            selectedItemTypeObject: ''
          });
        },
        btnFun: () => {
          this.addNewItem(item, isRemoveSIM);
        },
        cancelBtn: () => {
          this.setState({
            selectedItemType: '',
            selectedItemTypeID: '',
            selectedItemTypeObject: ''
          });
        }
      }
    });
  }

  getSKUDetails() {
    this.setState({ apiLoading: true }, () => {
      const data = {
        app_flow: this.props.WOM.work_order_app_flow,
        serial: this.state.selectedItemTypeObject,
        conn_no: this.props.WOM.work_order_detail.conn_no,
      };
      Utill.apiRequestPost('getTradeMaterial', 'accessorySale', 'cfss',
        data, this.getSKUSuccessCB, this.getSKUFailureCB, this.getSKUExCB);
    });
  }

  getSKUSuccessCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.format_NewItemType_Dropdown(response.data.data, 'SKU');
    });
  }

  getSKUFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, '');
    });
  }

  getSKUExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  select_NewItemType(item, type = 'other') {
    this.setState({
      selectednewItemType: this.state.newItemTypeValues[item],
      selectednewItemObject: this.state.newItemTypeObjct[item],
      selectednewItemTypeID: this.state.newItemTypeIndexes[item],

      selectedOffer: '',
      selectedofferCode: '',
      selectedofferObject: '',
      refreshNewSerialData: true,
      refreshSKUData: true,
      offerValues: [],

      oldRCUBarCode: '',
      contBtnEnable: false,

      oldPowerCode: '',

      newBarcode: '',
      validationSerial: '',
      rcuEnable: false,
      powerSupplyEnable: false,
      refreshOldpowerSupply: true,
      refreshOldRcuSerial: true,
      //remove
      // itemPrice: this.state.newItemTypeObjct[item].price,
      refreshNewSTBData: true
      //remove
      // itemWarranty: this.state.newItemTypeObjct[item].warranty_period
    }, () => {
      if (type == 'SKU') {
        this.getOffers(type, item)
      }
    });
  }

  format_NewItemType_Dropdown(response, type) {
    let newItemTypeValues = [];
    let newItemTypeObjct = [];
    let newItemTypeIndexes = [];

    response.map((item, key) => {
      if (type == 'SKU') {
        newItemTypeValues.push(item.name);
        newItemTypeObjct.push(item);
        newItemTypeIndexes.push(item.material_code);
      } else {
        newItemTypeValues.push(item.display_name);
        newItemTypeObjct.push(item);
        newItemTypeIndexes.push(item.bundle_code);
      }
    });
    this.setState({
      newItemTypeValues,
      newItemTypeObjct,
      newItemTypeIndexes
    });
  }

  format_Offer_Dropdown(responseOffer) {
    let offerValues = [];
    let offerCode = [];
    let offerObject = [];

    responseOffer.map((item, key) => {
      offerValues.push(item.offer_code);
      offerCode.push(item.offer_code);
      offerObject.push(item);
    });

    this.setState({ offerValues, offerCode, offerObject })
  }

  async select_Offer(item) {
    let contBtnEnable;
    let itemWarranty;
    if ((this.state.selectedRequestTypeID == 'ADDITIONAL') || this.state.isAssarySale && this.state.selectedItemTypeID != 'STB' && this.state.selectedItemTypeID != '') {
      contBtnEnable = true;
    } else {
      this.state.offerObject[item].elementwise_details.map(item => {
        if (item.name == 'STB') {
          itemWarranty = item.warranty;
        }
      })

      contBtnEnable = false;
    }

    await this.setState({
      selectedOffer: this.state.offerValues[item],
      selectedofferCode: this.state.offerCode[item],
      selectedofferObject: this.state.offerObject[item],
      itemPrice: this.state.offerObject[item].prod_price_with_tax,
      itemWarranty,
      contBtnEnable
    });
  }

  addNewAccessory() {
    var d = new Date();

    let itemPrice;
    if (this.state.itemPrice == undefined || this.state.itemPrice == null || this.state.itemPrice == '') {
      itemPrice = '0';
    } else {
      itemPrice = this.state.itemPrice;
    }

    let sub_item = [];

    let newItemtoAdd = {
      'root_id': d.getTime(),
      'type': this.state.selectedItemTypeID,
      'name': this.state.selectedItemType,
      'sale_type': this.state.selectedRequestTypeID,
      'serial': this.state.newBarcode,
      ...this.state.validationSerial,
      'main_item': this.state.main_item,
      'qty': '1',
      'price': itemPrice,
      'basic': 'N',
      'warrantyPeriod': this.state.itemWarranty
    }

    if (this.state.selectednewItemObject !== '') {
      newItemtoAdd.replaceble_item = this.state.selectednewItemObject;
    }

    if (this.state.selectednewItemObject.item_condition) {
      if (this.state.rcuEnable == true && this.state.powerSupplyEnable == true) {
        sub_item.push(this.state.oldRCU);
        sub_item.push(this.state.oldPower);
      } else if (this.state.powerSupplyEnable == true) {
        sub_item.push(this.state.oldPower);
      }
      else if (this.state.rcuEnable == true) {
        sub_item.push(this.state.oldRCU);
      }
      newItemtoAdd.sub_items = sub_item;
    }

    if (this.state.selectedStatus !== null) {
      if (this.state.selectedStatus == 'N') {
        newItemtoAdd.sim = this.state.newSIMSerial;
        newItemtoAdd.card_less = 'N';
      } else if (this.state.selectedStatus == 'Y') {
        newItemtoAdd.card_less = 'Y';
      }
    }

    if (this.state.selectedRequestTypeID == 'ADDITIONAL' || (this.state.selectedRequestTypeID == 'REPLACE' && this.state.isAssarySale)) {
      newItemtoAdd.offers = this.state.selectedofferObject;
      newItemtoAdd.warrantyPeriod = this.state.selectedofferObject.warranty;
    }

    if (this.state.selectedRequestTypeID == 'REPLACE') {
      this.checkForDuplications(newItemtoAdd);
    } else {
      this.addNewItem(newItemtoAdd, false);
    }
  }

  addNewItem(newItemtoAdd, isRemoveSIM = false) {
    console.log('newItemtoAdd', newItemtoAdd)
    let propData;
    if (this.state.selectedStatus !== null) {

      if (this.state.selectedStatus == 'N') {
        if (this.state.card_less == 'Y') {
          propData = [];
          const simObj = {
            ...this.state.newSIMSerial,
            card_less: 'N',
            provisionable: 'Y',
            sale_type: 'REPLACE'
          }
          propData.push(newItemtoAdd);
          propData.push(simObj);
          this.props.isRefix ? this.addNewItemForRefix(propData, false) : this.props.successAddNewItem(propData, false);
        } else {
          propData = newItemtoAdd;
          this.props.isRefix ? this.addNewItemForRefix(propData, false) : this.props.successAddNewItem(propData, false);
        }
      } else {
        propData = newItemtoAdd

        this.props.isRefix ? this.addNewItemForRefix(propData, false) : this.props.successAddNewItem(propData, false);
      }
    } else {
      this.props.isRefix ? this.addNewItemForRefix(newItemtoAdd, isRemoveSIM) : this.props.successAddNewItem(newItemtoAdd, isRemoveSIM);
    }
    this.props.isRefix ? true : this.props.navigator.pop();
  }

  addNewItemForRefix(newItemtoAdd, isRemoveSIM) {
    this.navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.InitialTRBView',
      passProps: { successRefixAddNewItem: newItemtoAdd, isRemoveSIM }
    });
  }

  oldScannerResponse = (isValidated, serialList) => {
    let dtvPowerSerial = [];
    let dtvRcuSerial = [];

    let updateValues = {};
    let cardless;

    if (serialList.sub_items) {
      serialList.sub_items.map((item, key) => {
        if (item.warranty_detail.equipment_type == 'DTV_POWER_SUPPLY') {
          dtvPowerSerial.push(item.warranty_detail);
        } else if (item.warranty_detail.equipment_type == 'DTV_RCU') {
          dtvRcuSerial.push(item.warranty_detail)
        }
      })

      updateValues.sub_items = serialList.sub_items;
      updateValues.oldRcu = dtvRcuSerial;
      updateValues.oldPower = dtvPowerSerial;
      updateValues.oldBarcode = serialList.main_item.equipment_serial;
      updateValues.main_item = serialList.main_item;
    } else {
      updateValues.oldBarcode = serialList.main_item.equipment_serial;
      updateValues.main_item = serialList.main_item;
    }

    cardless = serialList.main_item.card_less;
    let isAccesory_sales;
    if (this.props.WOM.work_order_detail.ownership == 'DialogOwned') {
      if (serialList.main_item.warranty_status == "true") {
        isAccesory_sales = false
      } else {
        isAccesory_sales = true
      }

      this.DilaogOwnedReplace(serialList.main_item.warranty_period, serialList, updateValues, cardless, isAccesory_sales);
    }
    else if (this.props.WOM.work_order_detail.ownership !== 'DialogOwned') {
      if (serialList.main_item.warranty_status == "true") {
        this.displayConfirmAlert(serialList.main_item.warranty_status, serialList.main_item.warranty_period, serialList, updateValues, cardless);
      } else {
        this.displayConfirmAlert(serialList.main_item.warranty_status, '', serialList, updateValues, cardless);
      }
    }
  }

  displayConfirmAlert(warrantyStatus, warrantyPeriod, serialList, updateValues, cardless) {
    let serialNo = '';
    let description1 = '';
    let description2 = '';

    if (warrantyStatus) {
      description1 = this.state.locals.lblWarrantyAvailable + ' ';
      description2 = this.state.locals.lblWarrantyAvailableMsg
    } else {
      serialNo = this.state.locals.lblSerialNo + ' ' + this.state.selectedItemTypeObject.name;
      description1 = this.state.locals.lblWarrantyExp;
      description2 = this.state.locals.lblWarrantyExpMsg;
    }

    itemName = this.state.locals.trbItemType + ' : ' + this.state.selectedItemType

    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.WarrantyConfirmAlertScreen',
      passProps: {
        description1, description2, warrantyPeriod, itemName, warrantyStatus, serialNo,
        refreshSerial: () => this.refreshSerial(),
        onConfirmed: (value) => {
          this.DilaogOwnedReplace(warrantyPeriod, serialList, updateValues, cardless, value)
        },
        okPressed: () => {
          this.setState({ card_less: '' })
        }
      }
    })
  }
  refreshSerial = () => {
    this.setState({ refreshOldSTBData: true })
  }

  newScannerResponse = (isValidated, serialList) => {
    console.log('oldItem::::', serialList.inner_items)
    let rcuEnable = false;
    let powerSupplyEnable = false;
    if (this.props.WOM.work_order_detail.ownership == 'CxOwned') {
      if (serialList.inner_items.length > 1) {
        serialList.inner_items.map((item) => {
          if (item.type == 'Remote') {
            rcuEnable = true
          }
          if (item.type == 'PowerSupply') {
            powerSupplyEnable = true
          }
        })
      } else {
        this.setState({ contBtnEnable: true })
      }
    }
    this.setState({
      newBarcode: serialList.serial,
      validationSerial: serialList,
      refreshNewSerialData: false,
      offerCode: '',
      offerObject: '',
      offerValues: '',
      selectedOffer: '',
      selectedofferCode: '',
      selectedofferObject: '',
      rcuEnable,
      powerSupplyEnable,
    }, () => {
      if (this.state.main_item.warranty_status == "true" && this.state.selectedStatus == null) {
        if (this.state.selectedRequestTypeID !== 'ADDITIONAL' && this.state.selectedItemTypeID == 'STB' && this.state.selectednewItemObject.item_condition == 'New') {
          if (this.state.oldRCUBarCode !== '' && this.state.oldPowerCode !== '') {
            this.setState({ contBtnEnable: true });
          }
        } else if (this.state.selectedRequestTypeID !== 'ADDITIONAL' && this.state.selectedItemTypeID !== 'STB') {
          this.setState({ contBtnEnable: true });
        }
      } else if ((this.state.main_item.warranty_status == "true" && (this.state.selectedStatus !== null && this.state.selectedStatus == 'Y')) || (this.state.main_item.warranty_status == "true" && (this.state.card_less == 'N' && this.state.selectedStatus == 'N'))) {
        this.setState({ contBtnEnable: true });
      } else if (this.state.main_item.warranty_status == "false") {
        this.getOffers();
      } else if ((this.state.selectedRequestTypeID == 'ADDITIONAL' || this.state.isAssarySale) && this.state.selectedItemTypeID != 'STB' && this.state.selectedItemTypeID != '') {
        this.getOffers();
      } else if (this.props.WOM.work_order_detail.ownership == 'DialogOwned' && this.state.selectedItemTypeObject.provisionable == 'N' && this.state.selectedRequestTypeID !== 'ADDITIONAL') {
        this.setState({ contBtnEnable: true });
      } else if (this.state.selectedStatus == 'N') {
        this.checkCardfullAvailableSerial()
      }
    });
  }

  getOffers(type = '', index = 0) {
    const data = {
      app_flow: this.props.WOM.work_order_app_flow,
      connType: this.props.WOM.work_order_detail.conn_type,
    }

    if (type == 'SKU') {
      const material_code = this.state.newItemTypeIndexes[index];
      data.material_code = material_code;

    } else {
      const serial = this.state.validationSerial;
      data.serial = serial;
    }

    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('getOffers', 'accessorySale', 'cfss',
        data, this.offerSuccessCB, this.offerFailureCB, this.offerExCB);
    });
  }

  offerSuccessCB = (response) => {
    this.setState({ apiLoading: false, isViewHidden: false }, () => {
      this.format_Offer_Dropdown(response.data.data);
    });
  }

  offerFailureCB = (response) => {
    let contBtnEnable = false;

    this.setState({ apiLoading: false, contBtnEnable }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, '');
    });
  }

  offerExCB = (response) => {
    this.setState({ apiLoading: false, contBtnEnable: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.displayCommonAlert('defaultAlert', this.state.locals.lblError, error, '');
      } else {
        error = this.state.locals.api_error_message;
        this.displayCommonAlert('defaultAlert', this.state.locals.lblError, error, '');
      }
    });
  }

  scannerSKUResponse = (isValidated, serialList) => {
    let contBtnEnable;

    if ((this.state.selectedOffer !== '' && ((this.state.selectedStatus == null) || (this.state.selectedStatus !== null && this.state.selectedStatus == 'Y'))) || (this.state.selectedOffer == '' && ((this.state.selectedStatus == null) || (this.state.selectedStatus !== null && this.state.selectedStatus == 'Y')))) {
      contBtnEnable = true;
    } else {
      contBtnEnable = false;
    }
    this.setState({
      refreshSKUData: false,
      newBarcode: serialList.serial,
      contBtnEnable,
      validationSerial: serialList,
    });
  }

  oldRcuScannerResponse = (isValidated, serialList) => {
    delete serialList.oldSerial;
    delete serialList.name;
    delete serialList.type;



    let contBtnEnable = false;
    if (this.state.rcuEnable == true) {
      this.state.oldRCUBarCode !== '' ? contBtnEnable = false : contBtnEnable = true
      if (this.state.powerSupplyEnable == true) {
        serialList.serial !== '' && this.state.oldPowerCode !== '' ? contBtnEnable = true : contBtnEnable = false
      } else {
        contBtnEnable = true
      }

      this.setState({ oldRCUBarCode: serialList.serial, oldRCU: serialList, contBtnEnable, refreshOldRcuSerial: false });
    }
  }

  oldProwerSupplyResponse = (isValidated, serialList) => {
    delete serialList.oldSerial;
    delete serialList.name;
    delete serialList.type;

    let contBtnEnable = false;
    if (this.state.powerSupplyEnable == true) {
      this.state.oldPowerCode !== '' ? contBtnEnable = false : contBtnEnable = true
    }
    if (this.state.rcuEnable == true) {
      this.state.oldRCUBarCode !== '' && serialList.serial !== '' ? contBtnEnable = true : contBtnEnable = false
    }
    else {
      contBtnEnable = true
    }
    this.setState({ oldPowerCode: serialList.serial, oldPower: serialList, contBtnEnable, refreshOldpowerSupply: false, });
  }

  newSIMScannerResponse = (isValidated, serialList) => {
    this.setState({
      refreshNewSIMSerial: false, newSIMSerial: serialList
    }, () => this.checkCardfullAvailableSerial())
  }

  checkCardfullAvailableSerial() {
    let contBtnEnable = false;

    if (this.state.newBarcode !== '' && this.state.newSIMSerial !== '') {
      let powerSupplySerialAvailable = 'not_available';
      let RCUSerialAvailable = 'not_available';

      if (this.state.powerSupplyEnable == true) {
        if (this.state.oldPowerCode !== '') {
          powerSupplySerialAvailable = 'true'
        } else {
          powerSupplySerialAvailable = 'false'
        }
      }

      if (this.state.rcuEnable == true) {
        if (this.state.oldRCUBarCode !== '') {
          RCUSerialAvailable = 'true'
        } else {
          RCUSerialAvailable = 'false'
        }
      } else {
        contBtnEnable = true;
      }

      if ((powerSupplySerialAvailable == 'true' || powerSupplySerialAvailable == 'not_available')
        && (RCUSerialAvailable == 'true' || RCUSerialAvailable == 'not_available')) {
        contBtnEnable = true;
      }
    }
    this.setState({ contBtnEnable });
  }

  oldSIMScannerResponse = (isValidated, serialList) => {
    this.setState({
      refreshOldSIMData: false,
      oldSIM: serialList
    });
  }

  onSelectRemovalStatus(value) {
    this.setState({
      newBarcode: '',
      newSIMSerial: '',
      selectedStatus: value,
      refreshNewSerialData: true,
      refreshNewSIMSerial: true,
      contBtnEnable: false
    });
  }



  DilaogOwnedReplace = (warrantyPeriod, serialList, updateValues, cardless, value) => {
    this.setState({
      isAssarySale: value,
      ...updateValues,
      card_less: cardless,
      replaceble_items: serialList.replaceble_items,
      itemWarranty: warrantyPeriod,
      refreshOldSTBData: false,
      refreshOldSIMData: false,
      refreshNewSerialData: true,
      newItemTypeIndexes: [],
      newItemTypeObjct: [],
      newItemTypeValues: [],
      selectednewItemType: '',
      selectednewItemObject: '',
      selectednewItemTypeID: '',
      offerCode: '',
      offerObject: '',
      offerValues: '',
      selectedOffer: '',
      selectedofferCode: '',
      selectedofferObject: ''
    }, () => {
      if ((this.state.selectedRequestTypeID == 'ADDITIONAL' || this.state.isAssarySale) && this.state.selectedItemTypeID == 'STB') {
        this.getSKUDetails();
      } else {
        let selectedStatus = null;
        if (this.state.selectedItemType == 'STB') {
          selectedStatus = this.props.WOM.work_order_detail.ownership == 'DialogOwned' ? 'N' : null;
        }
        this.setState({ selectedStatus }, () => {
          this.format_NewItemType_Dropdown(serialList.replaceble_items);
        })
      }
    });
  }

  renderItemType() {
    if (this.state.selectedRequestTypeID !== '') {
      return (
        <View style={{ marginHorizontal: 20 }}>
          <DropDownInput
            alignDropDown={true}
            dropDownTitle={this.state.locals.trbItemType}
            selectedValue={this.state.selectedItemType}
            dropDownData={this.state.itemTypeValues}
            onSelect={item => this.select_ItemType(item)}
          />
        </View>
      )
    } else return true;
  }

  renderOldSerial() {
    if (this.state.selectedItemType !== '' && this.state.selectedRequestTypeID !== 'ADDITIONAL') {
      return (
        <View style={{ marginHorizontal: 20 }}>
          <SerialListComponent
            scanCounter={0}
            refreshData={this.state.refreshOldSTBData}
            apiParams={{ conn_no: this.props.WOM.work_order_detail.conn_no, ownership: this.props.WOM.work_order_detail.ownership }}
            manualLable={this.renderOldSerialLabel(this.state.selectedItemTypeObject.name)}
            serialData={this.state.selectedItemTypeObject}
            isAllSerialsValidated={this.oldScannerResponse}
            navigatorOb={this.props.navigator}
            actionName='warrantyDetails'
            controllerName='warrantyReplacement'
            moduleName='cfss'
          />
          {this.state.card_less == 'N' ?
            <SerialListComponent
              scanCounter={0}
              serialData={{
                name: 'SIM', type: 'SIM', qty: 1, price: '0',
                oldSerial: [{ serial: this.state.main_item.card_sim, name: 'SIM', type: 'SIM' }]
              }}
              manualLable={this.renderOldSerialLabel('SIM')}
              refreshData={this.state.refreshOldSIMData}
              isAllSerialsValidated={this.oldSIMScannerResponse}
              navigatorOb={this.props.navigator}
              actionName='warrantyDetails'
              controllerName='warrantyReplacement'
              moduleName='cfss'
              apiDisable={true}
            />
            :
            true}
        </View>
      );
    } else return true;
  }

  renderTRBOptions() {
    if (this.state.oldBarcode !== '' && this.state.selectedRequestTypeID !== 'ADDITIONAL'
      && this.state.selectedItemTypeID == 'STB' && !this.state.isAssarySale) {
      return this.renderSTBReplacement()
    } else if (this.state.oldBarcode !== '' && this.state.selectedRequestTypeID !== 'ADDITIONAL'
      && !this.state.isAssarySale && this.state.selectedItemTypeID != 'STB' && this.state.selectedItemTypeID != '') {
      return this.renderNonSTBReplacement()
    } else if ((this.state.selectedRequestTypeID == 'ADDITIONAL' || this.state.isAssarySale)
      && this.state.selectedItemTypeID == 'STB') {
      return this.renderSTBAccessorySale()
    } else if ((this.state.selectedRequestTypeID == 'ADDITIONAL' || this.state.isAssarySale)
      && this.state.selectedItemTypeID != 'STB' && this.state.selectedItemTypeID != '') {
      return this.renderNonSTBAccessorySale()
    } else return true;
  }

  renderSTBReplacement() {
    let ps = true;
    let rcu = true;
    let content = true;

    let stb = true;
    let sim = true;

    // stb replacement for dialog owned flow - cardless option selection
    if (this.props.WOM.work_order_detail.ownership == 'DialogOwned') {
      if (this.state.selectedStatus !== null) {
        stb =
          <SerialListComponent
            refreshData={this.state.refreshNewSerialData}
            apiParams={{
              order_id: this.props.WOM.work_order_detail.order_id,
              app_flow: this.props.WOM.work_order_app_flow,
              trbRequestType: this.state.selectedRequestTypeID,
              conn_no: this.props.WOM.work_order_detail.conn_no,
              request_type: this.props.WOM.work_order_detail.wo_type,
              ownership: this.props.WOM.work_order_detail.ownership,
              wo_type: this.props.WOM.work_order_detail.wo_type
            }}
            manualLable={this.renderNewSerialLabel(this.state.selectedItemTypeObject.name)}
            serialData={this.state.selectedItemTypeObject}
            isAllSerialsValidated={this.newScannerResponse}
            navigatorOb={this.props.navigator}
            actionName='validateSerialNew'
            controllerName='serial'
            moduleName='wom'
          />
      }

      // old STB cardless and new cardfull selection render SIM
      if (this.state.card_less !== 'N') {
        if (this.state.selectedStatus == 'N') {
          sim =
            <SerialListComponent
              refreshData={this.state.refreshNewSIMSerial}
              apiParams={{
                order_id: this.props.WOM.work_order_detail.order_id,
                app_flow: this.props.WOM.work_order_app_flow,
                trbRequestType: this.state.selectedRequestTypeID,
                conn_no: this.props.WOM.work_order_detail.conn_no,
                request_type: this.props.WOM.work_order_detail.wo_type,
                ownership: this.props.WOM.work_order_detail.ownership,
                wo_type: this.props.WOM.work_order_detail.wo_type
              }}
              manualLable={this.renderNewSerialLabel('SIM')}
              serialData={{ name: 'SIM', type: 'SIM', qty: 1, price: '0' }}
              isAllSerialsValidated={this.newSIMScannerResponse}
              navigatorOb={this.props.navigator}
              actionName='validateSerialNew'
              controllerName='serial'
              moduleName='wom'
            />
        }
      }

      content =
        <View style={{ marginHorizontal: 20 }}>
          <View style={{ paddingTop: 30 }}>
            {this.renderRadioOption()}
          </View>
          {stb}
          {sim}
        </View>
    }

    // stb replacement for cx owned flow - stb type dropdown
    if (this.props.WOM.work_order_detail.ownership == 'CxOwned') {
      content =
        <View style={{ marginHorizontal: 20 }}>
          <DropDownInput
            alignDropDown={true}
            dropDownTitle={this.state.locals.lblNew + ' ' + this.state.selectedItemType + ' ' + this.state.locals.lblType}
            selectedValue={this.state.selectednewItemType}
            dropDownData={this.state.newItemTypeValues}
            onSelect={item => this.select_NewItemType(item)}
          />
          <SerialListComponent
            apiParams={{
              order_id: this.props.WOM.work_order_detail.order_id,
              app_flow: this.props.WOM.work_order_app_flow,
              trbRequestType: this.state.selectedRequestTypeID,
              conn_no: this.props.WOM.work_order_detail.conn_no,
              request_type: this.props.WOM.work_order_detail.wo_type,
              ownership: this.props.WOM.work_order_detail.ownership,
              replaceble_item: this.state.selectednewItemObject,
              wo_type: this.props.WOM.work_order_detail.wo_type
            }}
            refreshData={this.state.refreshNewSerialData}
            manualLable={this.renderNewSerialLabel(this.state.selectedItemTypeObject.name)}
            serialData={{ ...this.state.selectedItemTypeObject, replaceble_item: this.state.selectednewItemObject }}
            isAllSerialsValidated={this.newScannerResponse}
            navigatorOb={this.props.navigator}
            actionName='validateSerialNew'
            controllerName='serial'
            moduleName='wom'
          />
        </View>
    }

    if (this.state.powerSupplyEnable == true) {
      ps =
        <SerialListComponent
          scanCounter={0}
          apiParams={{ selectedItem: this.state.selectedItemType }}
          serialData={{
            name: this.state.locals.lblOldPowerSupplySerial,
            type: 'PowerSupply',
            oldSerial: this.state.oldPower
          }}
          isAllSerialsValidated={this.oldProwerSupplyResponse}
          navigatorOb={this.props.navigator}
          actionName='warrantyDetails'
          controllerName='warrantyReplacement'
          moduleName='cfss'
          apiDisable={true}
        />
    }

    if (this.state.rcuEnable == true) {
      rcu = <SerialListComponent
        scanCounter={0}
        apiParams={{ selectedItem: this.state.selectedItemType }}
        serialData={{
          name: this.state.locals.lblOldRCUSerial,
          type: 'RCU',
          oldSerial: this.state.oldRcu
        }}
        isAllSerialsValidated={this.oldRcuScannerResponse}
        navigatorOb={this.props.navigator}
        actionName='warrantyDetails'
        controllerName='warrantyReplacement'
        moduleName='cfss'
        apiDisable={true}
      />
    }

    return (
      <View >
        {content}
        <View style={{ marginHorizontal: 20 }}>
          {rcu}
          {ps}
        </View>
      </View>
    )
  }

  renderNonSTBReplacement() {
    return (
      <View style={{ marginHorizontal: 20 }}>
        <SerialListComponent
          apiParams={{
            order_id: this.props.WOM.work_order_detail.order_id,
            app_flow: this.props.WOM.work_order_app_flow,
            trbRequestType: this.state.selectedRequestTypeID,
            conn_no: this.props.WOM.work_order_detail.conn_no,
            request_type: this.props.WOM.work_order_detail.wo_type,
            ownership: this.props.WOM.work_order_detail.ownership,
            wo_type: this.props.WOM.work_order_detail.wo_type
          }}
          manualLable={this.renderNewSerialLabel(this.state.selectedItemTypeObject.name)}
          serialData={this.state.selectedItemTypeObject}
          isAllSerialsValidated={this.newScannerResponse}
          navigatorOb={this.props.navigator}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
        />
      </View>
    )
  }

  renderSTBAccessorySale() {
    let sku = true;
    let itemType = true;
    let offers = true;
    let newSerial = true;
    let simSerial = true;

    if (this.state.newItemTypeValues.length > 0) {
      sku =
        <DropDownInput
          alignDropDown={true}
          dropDownTitle={'SKU'}
          selectedValue={this.state.selectednewItemType}
          dropDownData={this.state.newItemTypeValues}
          onSelect={item => this.select_NewItemType(item, 'SKU')}
        />
    }

    if (this.state.isViewHidden == false) {
      offers =
        <DropDownInput
          alignDropDown={true}
          dropDownTitle={this.state.locals.lblOffers}
          selectedValue={this.state.selectedOffer}
          dropDownData={this.state.offerValues}
          onSelect={item => this.select_Offer(item)}
        />
    }

    if (this.state.selectednewItemType !== '') {
      if (this.state.selectedOffer !== '') {
        itemType =
          <View>            
            <View style={{ width: width - 60, height: 40, marginBottom: 25 }}>
              <TextFieldComponent style={{ marginHorizontal: 20 }}
                label={this.state.locals.lblPrice}
                value={this.state.itemPrice}
                editable={false}
              />
            </View>
            <View >
              <TextFieldComponent
                label={this.state.locals.lblWarranty}
                value={this.state.itemWarranty + ''}
                editable={false}
              />
            </View>
          </View>

        newSerial =
          <SerialListComponent
            refreshSKUData={this.state.refreshSKUData}
            apiParams={{
              order_id: this.props.WOM.work_order_detail.order_id,
              app_flow: this.props.WOM.work_order_app_flow,
              trbRequestType: this.state.selectedRequestTypeID,
              conn_no: this.props.WOM.work_order_detail.conn_no,
              request_type: this.props.WOM.work_order_detail.wo_type,
              ownership: this.props.WOM.work_order_detail.ownership,
              replaceble_item: this.state.selectednewItemObject,
              wo_type: this.props.WOM.work_order_detail.wo_type
            }}
            manualLable={this.renderNewSerialLabel(this.state.selectedItemTypeObject.name)}
            serialData={{ ...this.state.selectedItemTypeObject, replaceble_item: this.state.selectednewItemObject }}
            isAllSerialsValidated={this.scannerSKUResponse}
            navigatorOb={this.props.navigator}
            actionName='validateSerialNew'
            controllerName='serial'
            moduleName='wom'
          />
      }
    }

    return (
      <View style={{ marginHorizontal: 20 }}>
        {sku}
        {offers}
        {itemType}
        {newSerial}
        {simSerial}
      </View>
    )
  }

  renderNonSTBAccessorySale() {
    let content = true;
    if (this.state.isViewHidden == false) {
      content =
        <View>
          <DropDownInput style={{ marginHorizontal: 20 }}
            alignDropDown={true}
            dropDownTitle={this.state.locals.lblOffers}
            selectedValue={this.state.selectedOffer}
            dropDownData={this.state.offerValues}
            onSelect={item => this.select_Offer(item)}
          />
          <TextFieldComponent
            label={this.state.locals.lblPrice}
            value={this.state.itemPrice + ''}
            editable={false}
          />
        </View>
    }

    return (
      <View style={{ marginHorizontal: 20 }}>
        <SerialListComponent
          refreshData={this.state.refreshNewSerialData}
          apiParams={{
            order_id: this.props.WOM.work_order_detail.order_id,
            app_flow: this.props.WOM.work_order_app_flow,
            trbRequestType: this.state.selectedRequestTypeID,
            conn_no: this.props.WOM.work_order_detail.conn_no,
            request_type: this.props.WOM.work_order_detail.wo_type,
            ownership: this.props.WOM.work_order_detail.ownership,
            wo_type: this.props.WOM.work_order_detail.wo_type
          }}
          manualLable={this.renderNewSerialLabel(this.state.selectedItemTypeObject.name)}
          serialData={this.state.selectedItemTypeObject}
          isAllSerialsValidated={this.newScannerResponse}
          navigatorOb={this.props.navigator}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
        />
        {content}
      </View>
    )
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  renderOldSerialLabel(name) {
    if (this.state.selectedItemTypeObject.provisionable == 'N' && this.props.WOM.work_order_detail.ownership == 'DialogOwned') {
      console.log('Selected Item', this.state.selectedItemTypeObject)
      var optionalOldLabletoReplace = this.state.locals.optionalOldLabletoReplace.toString();
      let oldLable = optionalOldLabletoReplace.replace(/<item>/g, name);

      return oldLable
    } else {
      var oldLabletoReplace = this.state.locals.oldLabletoReplace.toString();
      let oldLable = oldLabletoReplace.replace(/<item>/g, name);
      return oldLable
    }
  }

  renderNewSerialLabel(name) {
    var newLabletoReplace = this.state.locals.newLabelToReplace.toString();
    let newLable = newLabletoReplace.replace(/<item>/g, name); //name
    return newLable
  }

  renderRadioOption() {
    const { RaidoView, RadioStyle, radioButtonLabelStyle, contentContainerSerial } = styles;

    return (
      <View style={contentContainerSerial}>
        <View style={RaidoView}>
          <View style={RadioStyle}>
            <RadioButton
              currentValue={this.state.selectedStatus}
              value={'Y'}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={Colors.radioBtn.outerCircleColor}
              onPress={(value) => this.onSelectRemovalStatus(value)}>
              <Text style={radioButtonLabelStyle}>{this.state.locals.cardless}</Text>
            </RadioButton>
          </View>
          <View style={RadioStyle}>
            <RadioButton
              currentValue={this.state.selectedStatus}
              value={'N'}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={Colors.radioBtn.outerCircleColor}
              onPress={(value) => this.onSelectRemovalStatus(value)}>
              <Text style={radioButtonLabelStyle}>{this.state.locals.cardfull}</Text>
            </RadioButton>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        {this.showIndicator()}
        <View style={styles.middleContainer}>
          <ScrollView>
            <View style={styles.detailsContainer}>
              <View style={{ marginHorizontal: 20 }}>
                <DropDownInput
                  alignDropDown={true}
                  dropDownTitle={this.state.locals.trbRqtype}
                  selectedValue={this.state.selectedRequestType}
                  dropDownData={this.state.requestTypeValue}
                  onSelect={item => this.select_RequestItem(item)}
                />
              </View>
              {this.renderItemType()}
              {this.renderOldSerial()}
              {this.renderTRBOptions()}
            </View>
          </ScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.btncontainer}>
            <TouchableOpacity onPress={() => this.props.navigator.pop()}>
              <Text style={styles.filterTextPlain}>{this.state.locals.cancel}</Text>
            </TouchableOpacity>
            {this.state.contBtnEnable ?
              <TouchableOpacity onPress={() => this.debounceaddNewAccessory()}>
                <Text style={styles.filterText}>{this.state.locals.continue}</Text>
              </TouchableOpacity>
              :
              <Text style={styles.filterTextDisabled}>{this.state.locals.continue}</Text>}
          </View>
        </View>
        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
        </DragComponent>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const signature = state.mobile.signature;
  return { Language, WOM, signature };
};

const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1
  },
  topContainer: {
    flex: 0.05
  },
  middleContainer: {
    flex: 0.8,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 15
  },
  bottomContainer: {
    flex: 0.15,
    flexDirection: 'row',
  },
  detailsRow: {
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 8,
  },
  helpImage: {
    height: 40,
    width: 40,
    resizeMode: 'stretch',
  },
  btncontainer: {
    width: width - 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    position: 'absolute',
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextPlain: {
    width: width / 3.5,
    fontSize: 16,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft: 16,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  RaidoView: {
    marginBottom: 25,
    flexDirection: 'row'
  },
  RadioStyle: {
    alignItems: 'flex-start',
    flex: 0.5
  },
  radioButtonLabelStyle: {
    marginLeft: 10
  },
  contentContainerSerial: {
    flexDirection: 'column'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(TRBReplacmentView);

