import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Dimensions,
    Image,
    BackHandler,
    NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import _ from 'lodash';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import strings from '../../../Language/Wom';
import CardComponent from '../components/CardComponent';
import DragComponent from '../../general/Drag';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import Utills from '../../../utills/Utills';
import { Header } from '../Header';
import ActIndicator from './../../wom/ActIndicator';
import globalConfig from '../../../config/globalConfig';

const Utill = new Utills();
var { width, height } = Dimensions.get('window');
class RootCauseList extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            isEditable: false,
            isChecked: false,
            workOrderRootCauses: this.props.WOM.work_order_root_causes,
            isLoading: false,
            locals: {
                lblAction1: strings.lblAction1,
                lblAction2: strings.lblAction2,
                lblRootCause: strings.lblRootCause,
                btnAddNew: strings.btnAddNew,
                btnConfirm: strings.btnConfirm,
                btnEdit: strings.btnEdit,
                btnDelete: strings.btnDelete,
                cancel: strings.cancel,
                lblNoChangedEqp: strings.lblNoChangedEqp,
                msgError: strings.error,
                closeCIR: strings.closeCIR,
                backMessage: strings.backMessage,
                screenTitle: strings.screenTitle,
                inProgressTitle: strings.inProgressTitle,
                connected: strings.connected,
                disconected: strings.disconected,
                network_conected: strings.network_conected,
                network_error_message: strings.network_error_message,
                lblSystemError: strings.system_error
            },
        };
        this.navigatorOb = this.props.navigator;
        this.debounceforwardAddNewAction = _.debounce(() => this.forwardAddNewAction(), globalConfig.defaultDelayTime,
            {
                'leading': true,
                'trailing': false
            });
        this.debounceforwardConfirmAction = _.debounce(() => this.forwardConfirmAction(), globalConfig.defaultDelayTime,
            {
                'leading': true,
                'trailing': false
            });
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    }

    handleBackButtonClick = () => {
        this.navigatorOb.pop();
        return true;
    }

    _handleFirstConnectivityChange = isConnected => {
        if (isConnected == false) {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_error_message,
                actionText: this.state.locals.disconected,
                actionId: 'fabClicked',
                actionColor: Colors.colorRed,
                textColor: Colors.white,
                duration: 'indefinite'
            })
        } else {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_conected,
                actionText: this.state.locals.connected,
                actionId: 'fabClicked',
                actionColor: Colors.radioBtn,
                textColor: Colors.white,
            })
        }
    }

    _editPressed = () => {
        const workOrderRootCauses = this.state.workOrderRootCauses.map(item => {
            item.isChecked = false;
            return item;
        });
        this.setState({
            workOrderRootCauses,
            isEditable: !this.state.isEditable,
        });
    }

    _cancelPressed = () => {
        const workOrderRootCauses = this.state.workOrderRootCauses.map(item => {
            item.isChecked = false;
            return item;
        });
        this.setState({
            workOrderRootCauses,
            isEditable: !this.state.isEditable,
        });
    }

    _deletePressed = () => {
        if (__DEV__) console.log('---------------_deletePressed--------------');

        const workOrderRootCauses = this.state.workOrderRootCauses.filter(item => {
            if (item.isChecked == false) return item;
        });
        this.setState({
            workOrderRootCauses,
            isEditable: !this.state.isEditable
        }, () => {
            if (__DEV__) console.log('values1: ', this.state.workOrderRootCauses);

            this.props.updateRootCauseList(this.state.workOrderRootCauses);
            if (__DEV__) console.log('values2: ', this.props.WOM);

            if (this.state.workOrderRootCauses.length <= 0) {
                const navigatorOb = this.props.navigator;
                navigatorOb.pop();
            }
        });
    }

    checkBoxValueChange = (item) => {
        if (__DEV__) console.log('----------------checkBoxValueChange-------------------');
        if (__DEV__) console.log('values1: ', item)

        let newArray = [];
        if (item.isChecked === true) {
            const isChecked = false;
            newArray = this.state.workOrderRootCauses.map(value => {
                if (value.root_id == item.root_id) {
                    let newObj = { ...value, isChecked };
                    return newObj;
                }
                else return value;
            });
        } else {
            const isChecked = true;
            newArray = this.state.workOrderRootCauses.map(value => {
                if (value.root_id == item.root_id) {
                    let newObj = { ...value, isChecked };
                    return newObj;
                }
                else return value;
            });
        }
        this.setState({ workOrderRootCauses: newArray });
    }

    forwardAddNewAction() { this.navigatorOb.pop(); }

    forwardConfirmAction() {
        this.props.updateRootCauseList(this.state.workOrderRootCauses);
        this.updateRootCauseReasons();
    }

    updateRootCauseReasons() {
        const data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            app_flow: this.props.WOM.work_order_app_flow,
            biz_unit: this.props.WOM.work_order_detail.biz_unit,
            request_type: this.props.WOM.work_order_detail.wo_type,
            root_cause_ans: this.state.workOrderRootCauses,
            cir: this.props.WOM.work_order_detail.cir,
        }
        this.setState({ isLoading: true }, () => {
            Utill.apiRequestPost('updateRootCauseReasons', 'RootCause', 'wom', data, this.updateRCSuccessCb, this.updateRCFailureCb, this.updateRCExcb);
        });
    }

    updateRCSuccessCb = (response) => {
        this.setState({ isLoading: false }, () => {
            this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
        });        
    }

    updateRCFailureCb = (response) => {
        this.setState({ isLoading: false }, () => {
            this.displayAlert('defaultAlert', this.state.locals.msgError, response.data.error ? response.data.error : '', '');
        });
    }

    updateRCExcb = (response) => {
        this.setState({ isLoading: false }, () => {
            this.displayAlert('defaultAlert', this.state.locals.msgError, this.state.locals.lblSystemError, '');
        });
    }

    displayAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                overrideBackPress: true,
                onPressOK: () => this.navigatorOb.pop()
            }
        });
    }

    showIndicator() {
        if (this.state.isLoading) {
            return (
                <View style={styles.indiView}>
                    <ActIndicator animating={true} />
                </View>
            );
        }
    }

    renderListItem = ({ item, index }) => {
        return (
            <View
                key={index}
                style={{ flex: 1, flexDirection: 'column' }}>
                {this.state.isEditable === true ?
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1 }}>
                            {item.isChecked === true ?
                                <CheckBox
                                    isChecked={item.isChecked}
                                    style={{ paddingTop: 50, paddingLeft: 10 }}
                                    checkBoxColor={Colors.yellow}
                                    onClick={() => this.checkBoxValueChange(item)} />
                                : <CheckBox
                                    isChecked={item.isChecked}
                                    style={{ paddingTop: 50, paddingLeft: 10 }}
                                    checkBoxColor={Colors.grey}
                                    onClick={() => this.checkBoxValueChange(item)} />}
                        </View>
                        <View style={{ flex: 0.9 }}>
                            <CardComponent
                                RCTitleNumber={this.state.locals.lblRootCause}
                                RCTitle={item.rootCause}
                                lable1={this.state.locals.lblAction1}
                                lable2={this.state.locals.lblAction2}
                                Item1={item.action1}
                                Item2={item.action2}
                            />
                        </View>
                    </View>
                    : <View>
                        <CardComponent
                            RCTitleNumber={this.state.locals.lblRootCause}
                            RCTitle={item.rootCause}
                            lable1={this.state.locals.lblAction1}
                            lable2={this.state.locals.lblAction2}
                            Item1={item.action1}
                            Item2={item.action2}
                        />
                    </View>}
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Header
                    backButtonPressed={() => this.handleBackButtonClick()}
                    headerText={this.state.locals.inProgressTitle} />
                {this.showIndicator()}
                <View style={styles.topContainer}>
                    {this.state.workOrderRootCauses.length > 0 ?
                        <View style={{ alignItems: 'flex-end' }}>
                            {this.state.isEditable === true ? (
                                <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                                    <TouchableOpacity onPress={() => this._cancelPressed()}>
                                        <Text style={styles.CancelText}>{this.state.locals.cancel}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this._deletePressed()}>
                                        <Text style={styles.DeleteText}>{this.state.locals.btnDelete}</Text>
                                    </TouchableOpacity>
                                </View>
                            ) : true}
                            <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                                <TouchableOpacity
                                    onPress={() => this._editPressed()}>
                                    {this.state.isEditable === false ? (
                                        <Text style={styles.EditText}>{this.state.locals.btnEdit}</Text>
                                    ) : true}
                                </TouchableOpacity>
                            </View>
                        </View> : true  }
                </View>
                <View style={styles.middleContainer}>
                    <View >
                        <FlatList
                            data={this.state.workOrderRootCauses}
                            keyExtractor={(x, i) => i.toString()}
                            renderItem={this.renderListItem}
                            scrollEnabled={true}
                            extraData={this.state}
                        />
                    </View>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.direbutRow}>
                        <View style={styles.dirImageView}>
                        </View>
                        <View style={styles.filterButView}>
                            <View>
                                <TouchableOpacity
                                    onPress={() => this.debounceforwardAddNewAction()}
                                    disabled={this.state.isEditable == true ? true : false}>
                                    <Text style={this.state.isEditable == true ? styles.filterTextDisabled : styles.filterText}>{this.state.locals.btnAddNew}</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity
                                    onPress={() => this.debounceforwardConfirmAction()}
                                    disabled={this.state.workOrderRootCauses.length > 0 ? this.state.isEditable == true ? true : false : true}>
                                    <Text style={this.state.workOrderRootCauses.length > 0 ? this.state.isEditable == true ? styles.filterTextDisabled : styles.filterText : styles.filterTextDisabled}>{this.state.locals.btnConfirm}</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </View>
                <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
                    <Image style={styles.helpImage} source={fabIcon} />
                </DragComponent>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: Colors.appBackgroundColor
    },
    topContainer: {
        flex: 0.05,
        marginBottom: 10
    },
    middleContainer: {
        flex: 0.8,
        borderBottomColor: Colors.borderLineColorLightGray,
    },
    bottomContainer: {
        flex: 0.15,
        flexDirection: 'row'
    },
    direbutRow: {
        flexDirection: 'row',
        marginLeft: 10,
        paddingTop: 20,
        borderTopWidth: 1,
        borderTopColor: '#eeeeee',
        borderTopWidth: 1,
        paddingBottom: 20
    },
    dirImageView: {
        justifyContent: 'flex-start',
        width: (width / 2) - 10,
        paddingTop: 10
    },
    filterButView: {
        width: (width / 2) - 10,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    image_c1: {
        width: 20,
        height: 20,
    },
    filterText: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#ffc400',
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    filterTextDisabled: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: Colors.btnDisable,
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    EditText: {
        color: Colors.rootcauseEditbutton,
        fontSize: 16,
        fontWeight: 'bold',
        marginRight: 10
    },
    CancelText: {
        color: Colors.rootcauseEditbutton,
        fontSize: 16,
        fontWeight: 'bold',
        marginRight: 15
    },
    DeleteText: {
        color: Colors.rootcauseEditbutton,
        fontSize: 16,
        fontWeight: 'bold',
        marginRight: 10
    },
    helpImage: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },
    cardComponentView: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10
    },
    indiView: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
    },
});

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};

export default connect(mapStateToProps, actions)(RootCauseList);