
/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/

import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    Image,
    BackHandler,
    NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import _ from 'lodash';

import Colors from '../../../config/colors';
import strings from '../../../Language/Wom';
import DragComponent from '../../general/Drag';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import DropDownInput from '../components/DropDownInput';
import globalConfig from '../../../config/globalConfig';

var { height, width } = Dimensions.get('window');
class RootCauseForm extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            isConfirmDisabled: true,
            isCancelDisabled: true,
            rootCauseValues: [],
            rootCauseIndexes: [],
            action1Values: [],
            action1Indexes: [],
            action1SubIndexes: [],
            action2Values: [],
            action2Indexes: [],
            selectedRootCause: '',
            selectedRootCauseID: '',
            selectedAction1: '',
            selectedAction1ID: '',
            selectedAction2: '',
            selectedAction2ID: '',
            selectedOption: '',
            rootCauses: this.props.rootCauses,
            isfilledAction1: false,
            isfilledAction2: false,
            locals: {
                btnConfirm: strings.btnConfirm,
                btnCancel: strings.cancel,
                plcRootCause: strings.lblRootCause,
                plcAction1: strings.lblAction1,
                plcAction2: strings.lblAction2,
                msgError: strings.error,
                msgDuplicateRC: strings.duplicateRootCause,
                noRootCause: strings.noRootCause,
                noAction1: strings.noAction1,
                noAction2: strings.noAction2,
                cnfrmProceed: strings.cnfrmProceed,
                screenTitle: strings.inProgressTitle,
                connected: strings.connected,
                disconected: strings.disconected,
                network_conected: strings.network_conected,
                network_error_message: strings.network_error_message,
                lblInProgressTitle: strings.inProgressTitle
            },
        };
        this.navigatorOb = this.props.navigator;
        this.debounceaddNewRootCause = _.debounce(() => this.addNewRootCause(), globalConfig.defaultDelayTime,
            {
                'leading': true,
                'trailing': false
            });
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
        this.setState({
            selectedAction1: '',
            selectedAction2: '',
            selectedRootCause: '',
            action1Indexes: [],
            action1SubIndexes: [],
            action1Values: [],
            action1Values: [],
            action2Indexes: [],
            isfilledAction1: false,
            isfilledAction2: false,
            isConfirmDisabled: true
        }, () => {
            this.formatRootCauses(this.state.rootCauses);
        });
    }

    componentWillReceiveProps(newProps) {
        if (__DEV__) console.log('-----------componentWillReceiveProps--------');
        if (__DEV__) console.log('values1: ', newProps.WOM.work_order_root_causes);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    }

    handleBackButtonClick = () => {
        this.goBack();
        return true;
    }

    _handleFirstConnectivityChange = isConnected => {
        if (isConnected == false) {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_error_message,
                actionText: this.state.locals.disconected,
                actionId: 'fabClicked',
                actionColor: Colors.colorRed,
                textColor: Colors.white,
                duration: 'indefinite'
            })
        } else {
            this.props.navigator.showSnackbar({
                text: this.state.locals.network_conected,
                actionText: this.state.locals.connected,
                actionId: 'fabClicked',
                actionColor: Colors.radioBtn,
                textColor: Colors.white,
            })
        }
    }

    goBack() {
        this.props.workOrderRootCauseDataReset();
        const navigatorOb = this.props.navigator;
        navigatorOb.pop();
    }

    checkDuplications(rootCauseID, action1ID, action2ID) {
        if (__DEV__) console.log('-----------------checkDuplications---------------')

        rootCauseID.toString();;
        action1ID.toString();
        action2ID.toString();

        let rootCauseSelections = this.props.WOM.work_order_root_causes;
        if (__DEV__) console.log('values1: ', rootCauseSelections)
        if (__DEV__) console.log('values2: ', this.props.WOM)

        let isMatch = false;
        for (let i = 0; i < rootCauseSelections.length; i++) {
            if (rootCauseSelections[i].rootCauseID == rootCauseID && rootCauseSelections[i].action1ID == action1ID && rootCauseSelections[i].action2ID == action2ID) {
                isMatch = true;
            }
        }

        if (isMatch) {
            this.setState({
                isfilledAction1: false,
                isfilledAction2: false,
                selectedRootCause: '',
                selectedRootCauseID: '',
                selectedAction1: '',
                selectedAction1ID: '',
                selectedAction2: '',
                selectedAction2ID: '',
                isConfirmDisabled: true
            });
        }

        return isMatch;
    }

    displayAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                overrideBackPress: true,
                onPressOK: () => { }
            }
        });
    }

    addNewRootCause() {
        var d = new Date();

        const root_id = d.getTime();
        const rootCause = this.state.selectedRootCause;
        const rootCauseID = this.state.selectedRootCauseID;
        const action1 = this.state.selectedAction1;
        const action1ID = this.state.selectedAction1ID;
        const action2 = this.state.selectedAction2;
        const action2ID = this.state.selectedAction2ID;

        const newObj = {
            root_id: root_id,
            rootCause: rootCause,
            rootCauseID: rootCauseID,
            action1: action1,
            action1ID: action1ID,
            action2: action2,
            action2ID: action2ID,
            isChecked: false
        }
        console.log('sample123 add123' + JSON.stringify(newObj));
        const isDuplicated = this.checkDuplications(this.state.selectedRootCauseID, this.state.selectedAction1ID, this.state.selectedAction2ID);
        if (isDuplicated) {
            this.displayAlert('defaultAlert', this.state.locals.msgError, this.state.locals.msgDuplicateRC, '')
        } else {
            // this.setState({
            //     isfilledAction1: false,
            //     isfilledAction2: false,
            //     isConfirmDisabled: true
            // }, () => {
            this.props.workOrderRootCauseData(newObj);
            this.gotoRootCauseListScreen();
            // });
        }
    }

    gotoRootCauseListScreen() {
        this.navigatorOb.push({
            title: this.state.locals.lblInProgressTitle,
            screen: 'DialogRetailerApp.views.RtCrsLstScreen'
        });
    }

    formatRootCauses(rootCauses) {
        let rootCauseValues = [];
        let rootCauseIndexes = [];
        rootCauses.rootCause.map((item, key) => {
            const actionID = item.id;
            const actionValue = item.value;
            rootCauseValues.push(actionValue);
            rootCauseIndexes.push(actionID);
        });
        this.setState({
            rootCauseValues,
            rootCauseIndexes
        });
    }

    rootCauseChanges(item) {
        let rootCause = this.state.rootCauseValues[item];
        let rootCauseId = this.state.rootCauseIndexes[item];
        this.setState({
            action1Values: [],
            action1Indexes: [],
            action1SubIndexes: [],
            selectedRootCause: rootCause,
            selectedRootCauseID: rootCauseId,
            selectedAction1: '',
            selectedAction2: '',
        }, () => this.formatAction1(rootCauseId));
    }

    formatAction1(rootCauseID) {
        let action1Values = [];
        let action1SubIndexes = [];
        let action1Indexes = [];

        let rootCauses = this.state.rootCauses;
        rootCauses.sub_option[rootCauseID].map((item, key) => {
            const actionIndexID = item.sub_app_id;
            const actionID = item.id;
            const actionValue = item.value;
            action1Values.push(actionValue);
            action1SubIndexes.push(actionIndexID);
            action1Indexes.push(actionID);
        });
        this.setState({
            action1Values,
            action1SubIndexes,
            action1Indexes,
            isfilledAction1: true,
        })
    }

    action1Changes(item) {
        let action1 = this.state.action1Values[item];
        let action1Id = this.state.action1Indexes[item];
        let action1Index = this.state.action1SubIndexes[item];
        this.setState({
            action2Indexes: [],
            action2Values: [],
            selectedAction2: '',
            selectedAction1: action1,
            selectedAction1ID: action1Id,
        }, () => this.formatAction2(action1Index));
    }

    formatAction2(action2ID) {
        let action2Values = [];
        let action2Indexes = [];

        let rootCauses = this.state.rootCauses;
        rootCauses.sub_sub_option[action2ID].map((item, key) => {
            const actionID = item.id;
            const actionValue = item.value;
            action2Values.push(actionValue);
            action2Indexes.push(actionID);
        });
        this.setState({
            action2Values,
            action2Indexes,
            isfilledAction2: true,
        })
    }

    action2Changes(item) {
        let action2 = this.state.action2Values[item];
        let action2Id = this.state.action2Indexes[item];
        this.setState({
            selectedAction2: action2,
            selectedAction2ID: action2Id,
            isConfirmDisabled: false
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topContainer}>

                </View>
                <View style={styles.middleContainer}>
                    <ScrollView>
                        <View style={{ flex: 1, marginTop: 18, marginBottom: 18 }}>
                            <DropDownInput
                                alignDropDown={true}
                                dropDownTitle={this.state.locals.plcRootCause}
                                hideRightIcon={true}
                                selectedValue={this.state.selectedRootCause}
                                dropDownData={this.state.rootCauseValues}
                                onSelect={(item) => this.rootCauseChanges(item)}
                            />
                        </View>

                        {this.state.isfilledAction1 == false ?
                            <View /> :
                            <View style={{ flex: 1, marginTop: 18, marginBottom: 18 }}>
                                <DropDownInput
                                    alignDropDown={true}
                                    dropDownTitle={this.state.locals.plcAction1}
                                    hideRightIcon={true}
                                    selectedValue={this.state.selectedAction1}
                                    dropDownData={this.state.action1Values}
                                    onSelect={(item) => this.action1Changes(item)}
                                />
                            </View>
                        }

                        {this.state.isfilledAction2 == false ?
                            <View /> :
                            <View style={{ flex: 1, marginTop: 18, marginBottom: 18 }}>
                                <DropDownInput
                                    alignDropDown={true}
                                    dropDownTitle={this.state.locals.plcAction2}
                                    hideRightIcon={true}
                                    selectedValue={this.state.selectedAction2}
                                    dropDownData={this.state.action2Values}
                                    onSelect={(item) => this.action2Changes(item)}
                                />
                            </View>
                        }
                    </ScrollView>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.direbutRow}>
                        <View style={styles.dirImageView}>
                        </View>
                        <View style={styles.filterButView}>
                            {this.props.WOM.work_order_root_causes.length > 0 ?
                                <View>
                                    <TouchableOpacity
                                        onPress={() => this.gotoRootCauseListScreen()}>
                                        <Text style={styles.filterText}>{this.state.locals.btnCancel}</Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View>
                                    <TouchableOpacity
                                        disabled={true}
                                        onPress={() => this.gotoRootCauseListScreen()}>
                                        <Text style={styles.filterTextDisabled}>{this.state.locals.btnCancel}</Text>
                                    </TouchableOpacity>
                                </View>}
                            <View>
                                <TouchableOpacity
                                    disabled={this.state.isConfirmDisabled}
                                    onPress={() => this.debounceaddNewRootCause()}>
                                    <Text style={this.state.isConfirmDisabled ? styles.filterTextDisabled : styles.filterText}>{this.state.locals.btnConfirm}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
                    <Image style={styles.helpImage} source={fabIcon} />
                </DragComponent>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    topContainer: {
        flex: 0.05,
    },
    middleContainer: {
        flex: 0.8,
        marginRight: 36,
        marginLeft: 36,
        borderBottomColor: Colors.borderLineColorLightGray,
    },
    bottomContainer: {
        flex: 0.15,
        flexDirection: 'row'
    },
    direbutRow: {
        flexDirection: 'row',
        marginLeft: 10,
        paddingTop: 20,
        borderTopWidth: 1,
        borderTopColor: '#eeeeee',
        borderTopWidth: 1,
        paddingBottom: 20
    },
    dirImageView: {
        justifyContent: 'flex-start',
        width: (width / 2) - 10,
        paddingTop: 10
    },
    filterButView: {
        width: (width / 2) - 10,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    image_c1: {
        width: 20,
        height: 20,
    },
    filterText: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#ffc400',
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    filterTextDisabled: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: Colors.btnDisable,
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    helpImage: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    }, indiView: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
    },
});

export default connect(mapStateToProps, actions)(RootCauseForm);