import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

import strings from '../../../Language/Wom';
import Colors from '../../../config/colors';
import imageErrorIcon from '../../../../images/common/error_msg.png';
import * as actions from '../../../actions';
class ReplaceConfirmAlert extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        lblCancel: strings.cancel,
        lblOK: strings.btnOk,
        lblReplace: strings.btnReplace,
        lblReplaceWithCost: strings.lblReplaceWithCost
      }
    }
  }
  onCancelPress() {
    this.dismissModal();
    if (this.props.refreshSerial) {
      this.props.refreshSerial();
    }
  }

  dismissModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({
      animated: true, animationType: 'slide-down'
    })
  }

  onOkPressed() {
    this.dismissModal();
    this.props.okPressed ? this.props.okPressed() : true;
    if (this.props.refreshSerial) {
      this.props.refreshSerial();
    }
  }

  onReplacePress(type) {
    this.dismissModal();
    this.props.onConfirmed(type);
  }

  renderItemType() {
    let name;
    if (this.props.warrantyStatus !== 'true') {
      name =  this.props.itemName.substring(0, 30)
    } else {
      name = this.props.itemName.substring(0, 30);
    }
    return <Text style={styles.itemType}>{name}</Text>
  }

  render() {
    const { container, body, alertImageContainer, itemType, bottomContainer, cancelText, replaceText, bodyContainer } = styles;
    return (
      <View style={container}>
        <View style={body}>
          <View style={this.props.warrantyStatus !== 'true' ? alertImageContainer : [alertImageContainer, { alignItems: 'flex-start', justifyContent: 'center' }]} >
            {/* {this.props.warrantyStatus !== 'true' ?
              <Image source={imageErrorIcon} style={{ width: 65, height: 65, resizeMode: 'contain' }} />
              :
              <Text style={itemType}>{this.props.itemName.substring(0, 30)}</Text>
              } */}
            {this.renderItemType()}
          </View>
          <View style={bodyContainer} >
            {/* {this.props.warrantyStatus !== 'true' ?
              <Text style={styles.descText1}>{this.props.serialNo}</Text>
              : true} */}
            <Text style={styles.descText1}>{this.props.description1}</Text>
            {this.props.warrantyStatus == 'true' ?
              <View>
                <Text style={styles.warrantyPeriod}>{this.props.warrantyPeriod}</Text>
                <Text style={styles.descText2}>{this.props.description2}</Text>
              </View>
              :
              // <Text style={styles.descText2}>{this.props.description2}</Text>
              true
            }
          </View>
          {this.props.warrantyStatus == 'true' ?
            <View style={bottomContainer} >
              <TouchableOpacity onPress={() => this.onCancelPress()} >
                <Text style={cancelText} >{this.state.locals.lblCancel}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onReplacePress(false)} >
                <Text style={replaceText} >{this.state.locals.lblReplace}</Text>
              </TouchableOpacity>
            </View>
            :
            [this.props.directSale ?
              <View style={styles.bottomContainerNoWarranty} >
                <View style={{ justifyContent: 'flex-end', marginTop: 10 }}>
                  <TouchableOpacity onPress={() => this.onCancelPress()} >
                    <Text style={styles.cancelText_nw} >{this.state.locals.lblOK}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              :
              <View style={styles.bottomContainerNoWarranty} >
                <View style={{ justifyContent: 'flex-end', marginTop: 10 }}>
                  <TouchableOpacity onPress={() => this.onReplacePress(true)} >
                    <Text style={styles.replaceText_nw} >{this.state.locals.lblReplaceWithCost}</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity onPress={() => this.onOkPressed()} >
                    <Text style={styles.cancelText_nw} >{this.state.locals.lblCancel}</Text>
                  </TouchableOpacity>
                </View>
              </View>]}
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColor
  },
  alertImageContainer: {
    marginBottom: 24,
    alignItems: 'center'
  },
  alertIcon: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  body: {
    backgroundColor: 'white',
    height: 250,
    marginHorizontal: 40,
    paddingHorizontal: 13,
    paddingBottom: 15,
    paddingTop: 15,
  },
  bodyContainer: {
    flex: 1
  },
  descText1: {
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left',
    marginHorizontal: 11,
  },
  descText2: {
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left',
    marginHorizontal: 11,
    marginTop: 15,
    marginBottom: 30
  },
  warrantyPeriod: {
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left',
    marginHorizontal: 11,
  },
  bottomContainer: {
    flex: 1.3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 80
  },
  cancelText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: 'black',
    marginRight: 17,
  },
  replaceText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: '#009688'
  },
  itemType: {
    fontSize: 20,
    marginHorizontal: 11,
    lineHeight: 21,
    color: '#000',
    textAlign: 'left'
  },
  bottomContainerNoWarranty: {
    flex: 1.3,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 80
  },
  replaceText_nw: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'right',
    color: 'red',
  },
  cancelText_nw: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'right',
    color: 'black',
    marginTop: 20,
  },
};

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
}

export default connect(mapStateToProps, actions)(ReplaceConfirmAlert)
