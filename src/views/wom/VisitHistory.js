import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  FlatList,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import { Header } from '../../components/others';
import strings from './../../Language/Wom';
import Color from '../../config/colors';
import chevron from '../../../images/icons/arrow.png';

var { width } = Dimensions.get('window');
class VisitHistory extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      jobHistory:[],      
    };
  }

  componentWillMount() {
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setState({
      jobHistory: this.props.history
    });
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick xxx');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
    return true;
  }

  onClickOrder (item)  { 
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      screen: 'DialogRetailerApp.views.VisitHistoryDetail',
      passProps: {
        historyItem: item
      }
    });
    navigatorOb.pop();
  }

  render() {
    return (

      <View style={styles.container}>
      <Header
        backButtonPressed={() => this.handleBackButtonClick()}
        headerText= {strings.wo_history_title}/>

        <View style={styles.detailsContainer}>
        <View style={styles.topContainer}>
        </View>
          
            <View style={styles.middleContainer}>
              
              <FlatList
                disabled={true}
                scrollEnabled={true}
                data={this.state.jobHistory}
                keyExtractor={(x, i) => i}
                renderItem={({ item }) => (
                  <View>

                      <TouchableOpacity
                        onPress={() => this.onClickOrder(item)}
                      >
                        <View style={styles.detailsRow}>
                          <View style={styles.labelCol}>
                            <Text style={styles.labelText}>{item.id}</Text>
                          </View>
                          <View style={styles.labelCol2}>
                            <Text style={styles.labelText}>{item.request_type}</Text>
                          </View>
                          <View style={styles.labelCol3}>
                            <Text style={styles.labelText}>{item.date_time}</Text>
                          </View>
                          <View>
                            <Image source={chevron} style={styles.iconStyle} />                          
                          </View>
                        </View>
                      </TouchableOpacity>
                      
                  </View>
                )}
              />             
            </View>
        </View>

      </View>
    );
  }

}


const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Oder ===> Visit History');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
}; 


const styles = StyleSheet.create({
  container: {
    height: '100%'
  },
  detailsContainer: {
    flex: 1
  },
  topContainer: {
    flex: 0.01
  },
  middleContainer: {
    flex: 0.99,
    flexDirection: 'column',
  },
  detailsContainer: {
    flex: 1,
    backgroundColor:Color.appBackgroundColor
  },
  detailsRow: {
    flexDirection: 'row',
    borderBottomColor: '#C0C0C0',
    borderBottomWidth: 1,
    height:60,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  labelCol: {
    width: ((width -30) / 4) -10,
    paddingLeft: 8,
    paddingRight: 4,
    // fontWeight: 'bold'
  },
  labelCol2: {
    width: (width -30) / 2 ,
    paddingLeft: 4,
    paddingRight: 4
  },
  labelCol3: {
    width: ((width -30) / 4) +10
  },
  labelText:{
    fontSize: 16
  },
  iconStyle: {
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  }
  

});

export default connect(mapStateToProps, actions)(VisitHistory);


