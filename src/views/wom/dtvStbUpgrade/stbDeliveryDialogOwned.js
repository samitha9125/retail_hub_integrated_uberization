import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, BackHandler, Dimensions, NetInfo } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import * as actions from '../../../actions';
import ActIndicator from '../ActIndicator';
import { Header } from '../Header';
import strings from '../../../Language/Wom';
import TextItemComponent from '../components/TextItemComponent';
import DragComponent from '../../general/Drag';
import _ from 'lodash';
import globalConfig from '../../../config/globalConfig';
import SerialListComponent from '../../wom/components/SerialListComponent';

const { width } = Dimensions.get('window')
class StbDeliveryDialogOwned extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      isContinueBtnDisabled: true,
      scannedSerial: '',
      isHidden: true,
      filteredSerial: '',
      newSerial: '',
      scannedOldSerialObj: {},
      warranty_details: this.props.stbDetails,
      refreshOldSTBData: false,
      locals: {
        screenTitle: strings.inProgressTitle,
        backMessage: strings.backMessage,
        continue: strings.continue,
        cancel: strings.cancel,
        descriptionText: strings.serialdescription,
        oldSerial: strings.oldSerial,
        newSerial: strings.newSerial,
        lblOldSTBSerial: strings.lblOldSTBSerial,
        lblNewSTBSerial: strings.lblNewSTBSerial,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        network_error_message: strings.network_error_message,
        inProgressTitle: strings.inProgressTitle
      }
    };
    this.navigatorOb = this.props.navigator;
    this.debouncegoto_confirmationPage = _.debounce(() => this.goto_confirmationPage(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  loadNewScanner() {
    return (
      <View style={{ marginHorizontal: 20 }}>
        <SerialListComponent
          refreshData={this.state.refreshOldSTBData}
          apiParams={{
            prod_list: this.props.WOM.work_order_detail ? this.props.WOM.work_order_detail.product_detail.product_list ? this.props.WOM.work_order_detail.product_detail.product_list : {} : {},
            ownership: this.props.WOM.work_order_detail.ownership,
            order_id: this.props.WOM.work_order_detail.order_id,
            wo_type: this.props.WOM.work_order_detail.wo_type,
            app_flow: this.props.WOM.work_order_detail.app_flow
          }}
          manualLable={this.state.locals.lblNewSTBSerial}
          serialData={{ ...this.state.scannedOldSerialObj }}
          isAllSerialsValidated={this.newSTBScannerResponse}
          navigatorOb={this.props.navigator}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
        />
      </View>
    )
  }

  oldSTBScannerResponse = (isValidated, response) => {
    this.setState({ scannedSerial: response.serial, scannedOldSerialObj: response, refreshOldSTBData: false, isHidden: false })
  }

  newSTBScannerResponse = (isValidated, res) => {
    this.setState({ isContinueBtnDisabled: false, newSerial: res })
  }

  loadBarodeScanner = () => {
    return (
      <View syle={{ marginLeft: 15, marginRight: 15 }}>
        <SerialListComponent
          scanCounter={0}
          serialData={{
            name: 'STB', type: 'STB',
            oldSerial: this.state.warranty_details
          }}
          manualLable={this.state.locals.lblOldSTBSerial}
          refreshData={this.state.refreshOldSTBData}
          isAllSerialsValidated={this.oldSTBScannerResponse}
          navigatorOb={this.props.navigator}
          apiDisable={true}
        />
      </View>
    )
  }

  goto_confirmationPage = () => {
    let sampleArray = [];
    sampleArray.push(this.state.newSerial);
    const formatArray = sampleArray.map((item) => {
      const newObj = {
        label: item.name,
        value: item.serial
      }
      return newObj
    })

    let newFormattedArray = [];
    newFormattedArray.push(formatArray)

    this.props.workOrderGetDTVProvisioning(newFormattedArray);
    this.navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.DTVInstallationItem',
      passProps: {
        serials: sampleArray,
        hideEditbutton: 'hide',
        hideAddbutton: 'ishide'
      }
    });
  }
  stbTypeViewer() {
    if (this.props.WOM.work_order_detail.new_stb_model !== undefined && this.props.WOM.work_order_detail.new_stb_model !== null && this.props.WOM.work_order_detail.new_stb_model!== '') {
      return (
        <View>
          <TextItemComponent label1={strings.newSTBModel} label2={this.props.WOM.work_order_detail.NEW_STB_MODEL} />
        </View>
      );
    } else {
      return (
        <View>
          <TextItemComponent label1={strings.STB_Type} label2={this.props.WOM.work_order_detail.stb_type} />
        </View>
      );
    }
  }
  render() {
    const { continueBtnContainer, stbTypeContainer, container, contentContainer, metarialInput, imageContainer,
      imageContainer1, helpImage } = styles;
    return (
      <View style={container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        {this.state.isLoading ?
          <ActIndicator validating='Validating...' /> : true}
        <View style={contentContainer}>
          <View style={{ marginHorizontal: 40, marginBottom: 25 }}>
            {this.loadBarodeScanner()}
          </View>
          <View>
            {this.state.isHidden === false ?
              <View style={{ marginTop: 50 }}>
                {this.stbTypeViewer()}
              </View>
              : true}
          </View>
          <View>
            {this.state.isHidden === false ?
              <View>
                <View style={{ marginTop: 20, marginHorizontal: 20 }}>
                  {this.loadNewScanner()}
                </View>
              </View>
              : true}
          </View>
          <View style={continueBtnContainer} >
            <TouchableOpacity
              disabled={this.state.isContinueBtnDisabled}
              style={[styles.barcodeContinueBtn, this.state.isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
              onPress={() => this.debouncegoto_confirmationPage()}
            >
              <Text style={[styles.barcodeContinueBtnTxt, this.state.isContinueBtnDisabled ? { color: Colors.btnDeactiveTxtColor } : { color: Colors.btnActiveTxtColor }]} >{this.state.locals.continue}</Text>
            </TouchableOpacity>
          </View>

          <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
            <Image style={helpImage} source={fabIcon} />
          </DragComponent>
        </View>
      </View >
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: Colors.appBackgroundColor
  },
  contentContainer: {
    flex: 1.0
  },
  barcodeContinueBtn: {
    width: (width / 3),
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: 16,
    color: '#000000',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  stbTypeContainer: {
    marginTop: 24,
    marginBottom: 36,
    marginRight: 10
  },
  continueBtnContainer: {
    alignItems: 'flex-end',
    marginTop: '50%',
    marginRight: 24
  },
  iconStyle: {
    width: 30,
    height: 25,
    resizeMode: 'contain'
  },
  imageContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 25,
    marginTop: -40
  },
  imageContainer1: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 25,
    marginTop: -35
  },
  metarialInput: {
    paddingLeft: 40,
    paddingRight: 30
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
});

export default connect(mapStateToProps, actions)(StbDeliveryDialogOwned);