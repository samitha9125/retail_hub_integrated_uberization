import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, BackHandler, Dimensions, NetInfo } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import Utills from '../../../utills/Utills';
import * as actions from '../../../actions';
import ActIndicator from '../ActIndicator';
import { Header } from '../Header';
import strings from '../../../Language/Wom';
import TextItemComponent from '../components/TextItemComponent';
import DragComponent from '../../general/Drag';
import RadioButton from '../components/RadioButtonComponent';
import SerialListComponent from '../../wom/components/SerialListComponent';

const Utill = new Utills();
const { width, height } = Dimensions.get('window')
class StbDeliveryCustomerOwned extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      isLoading: false,
      isContinueBtnDisabled: true,
      scannedSerial: '',
      scannedOldSTBSerial: {},
      isHidden: true,
      newSerial: '',
      newSIMSerial: '',
      warranty_details: this.props.stbDetails,
      selectedStatus: 'Y',
      cardOption: this.props.card_option,
      simSerialdata: '',
      card_sim: '',
      card_type: '',
      hidebarcodetype: false,
      refreshOldSTBData: false,
      refreshSIMData: false,
      refreshSTBData: false,
      locals: {
        screenTitle: '',
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        error: strings.error,
        oldSerial: strings.oldSerial,
        newSerial: strings.newSerial,
        cardless: strings.cardless,
        newsimSerial: strings.newsimSerial,
        lblOldSTBSerial: strings.lblOldSTBSerial,
        lblNewSTBSerial: strings.lblNewSTBSerial,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        inProgressTitle: strings.inProgressTitle
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => this.navigatorOb.pop()

  loadNewScanner = () => {
    return (
      <View style={{ marginHorizontal: 20 }}>
        <SerialListComponent
          apiParams={{
            // sub_area: this.props.WOM.work_order_detail.sub_area,
            prod_list: this.props.WOM.work_order_detail ? this.props.WOM.work_order_detail.product_detail.product_list ? this.props.WOM.work_order_detail.product_detail.product_list : {} : {},
            ownership: this.props.WOM.work_order_detail.ownership,
            provisionable: 'Y',
            order_id: this.props.WOM.work_order_detail.order_id,
            wo_type: this.props.WOM.work_order_detail.wo_type,
            app_flow: this.props.WOM.work_order_detail.app_flow
          }}
          refreshData={this.state.refreshSTBData}
          manualLable={this.state.locals.lblNewSTBSerial}
          serialData={{ ...this.state.scannedOldSTBSerial }}
          isAllSerialsValidated={this.newSTBScannerResponse}
          navigatorOb={this.props.navigator}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
        />
      </View>
    )
  }

  newSTBScannerResponse = (isValidated, res) => {
    if (this.state.selectedStatus === 'Y') {
      this.setState({ isContinueBtnDisabled: false, newSerial: res, refreshSTBData: false })
    } else {
      this.setState({ newSerial: res, refreshSTBData: false })
    }
  }

  loadBarodeScanner() {
    return (
      <View style={{ marginHorizontal: 30, marginBottom: 25 }}>
        <SerialListComponent
          scanCounter={0}
          serialData={{
            name: 'STB', type: 'STB',
            oldSerial: this.state.warranty_details
          }}
          manualLable={this.state.locals.lblOldSTBSerial}
          refreshData={this.state.refreshOldSTBData}
          isAllSerialsValidated={this.oldSTBScannerResponse}
          navigatorOb={this.props.navigator}
          apiDisable={true}
        />
      </View>
    )
  }
  oldSTBScannerResponse = (isValidated, response) => {
    this.setState({ scannedSerial: response.serial, scannedOldSTBSerial: response, refreshOldSTBData: false, isHidden: false }, () => { this.getindividualserial() })
  }

  loadSIMScanner = () => {
    return (
      <View style={{ marginHorizontal: 20 }}>
        <SerialListComponent
          apiParams={{
            // sub_area: this.props.WOM.work_order_detail.sub_area,
            prod_list: this.props.WOM.work_order_detail ? this.props.WOM.work_order_detail.product_detail.product_list ? this.props.WOM.work_order_detail.product_detail.product_list : {} : {},
            ownership: this.props.WOM.work_order_detail.ownership,
            provisionable: 'Y',
            order_id: this.props.WOM.work_order_detail.order_id,
            wo_type: this.props.WOM.work_order_detail.wo_type,
            app_flow: this.props.WOM.work_order_detail.app_flow
          }}
          refreshData={this.state.refreshSIMData}
          manualLable={this.state.locals.newsimSerial}
          serialData={{ name: 'SIM', type: 'SIM', qty: 1, price: '0', basic: 'Y', card_less: 'N' }}
          isAllSerialsValidated={this.newSIMScannerResponse}
          navigatorOb={this.props.navigator}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
        />
      </View>
    )
  }

  newSIMScannerResponse = (isValidated, res) => {
    this.setState({ isContinueBtnDisabled: false, newSIMSerial: res, refreshSIMData: false })
  }

  getindividualserial = () => {
    this.setState({ isLoading: true }, () => {
      const data = {
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        stb_serial: this.state.scannedSerial
      };
      Utill.apiRequestPost('getCardInfo', 'Serial', 'wom',
        data, this.simSerialSuccess, this.simSerialFailure, this.simSerialEx);
    })
  }

  simSerialSuccess = (response) => {
    this.setState({
      simSerialdata: response.data.data.card_sim,
      card_type: response.data.data.card_type,
      isLoading: false
    });
  }

  simSerialEx = (error) => {
    this.setState({ isLoading: false }, () => {
      let message;
      if (error == 'Network Error') {
        message = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: message });
      } else {
        message = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: message });
      }
    });
  }

  simSerialFailure = (response) => {
    this.setState({ isLoading: false }, () => {
      const navigatorOb = this.props.navigator;

      try {
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: response.data.data ? response.data.data.error : response.data.error ? response.data.error : response.data.info ? response.data.info : ''
          },
          overrideBackPress: true
        });
      } catch (error) {
        console.log('error ', error);
      }
    });
  }

  goto_confirmationPage = () => {
    let sampleArray = [];
    if (this.state.selectedStatus == 'Y') {
      sampleArray.push(this.state.newSerial);
    } else {
      const STB = this.state.newSerial;
      const Data = {
        ...STB,
        card_less: this.state.selectedStatus
      }

      sampleArray.push(Data);

      const SIM = this.state.newSIMSerial;
      const Data1 = {
        ...SIM,
        card_less: this.state.selectedStatus
      }
      sampleArray.push(Data1);

    }

    const formatArray = sampleArray.map((item) => {
      const newObj = {
        label: item.name,
        value: item.serial
      };
      return newObj;
    });

    let newFormattedArray = [];
    newFormattedArray.push(formatArray)

    this.props.workOrderGetDTVProvisioning(newFormattedArray);
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.DTVInstallationItem',
      passProps: {
        serials: sampleArray,
        hideEditbutton: 'hide',
        hideAddbutton: 'ishide',
      }
    });
  }

  onSelectStatus(value) {
    this.setState({ selectedStatus: value, isContinueBtnDisabled: true, refreshSIMData: true, refreshSTBData: true }, () => {
      if (this.state.selectedStatus === 'Y') {
        this.setState({ hidebarcodetype: false })
      } else {
        this.setState({ hidebarcodetype: true })
      }
    });
  }

  renderCardLessItem = (item) => {
    const { RadioStyle, radioButtonLabelStyle } = styles;
    return (
      <View style={RadioStyle}>
        <RadioButton
          currentValue={this.state.selectedStatus}
          value={item.key}
          outerCircleSize={20}
          innerCircleColor={Colors.radioBtn.innerCircleColor}
          outerCircleColor={Colors.radioBtn.outerCircleColor}
          onPress={(value) => this.onSelectStatus(value)}>
          <Text style={radioButtonLabelStyle}>{item.value}</Text>
        </RadioButton>
      </View>
    )
  }

  renderCardList = () => {
    return this.state.cardOption.map(item => {
      return this.renderCardLessItem(item);
    });
  }

  stbTypeViewer() {
    if (this.props.WOM.work_order_detail.new_stb_model !== undefined 
        && this.props.WOM.work_order_detail.new_stb_model !== null 
        && this.props.WOM.work_order_detail.new_stb_model!== '') {
      return (
        <View>
          <TextItemComponent label1={strings.newSTBModel} label2={this.props.WOM.work_order_detail.new_stb_model} />
        </View>
      );
    } else {
      return (
        <View>
          <TextItemComponent label1={strings.STB_Type} label2={this.props.WOM.work_order_detail.stb_type} />
        </View>
      );
    }
  }

  render() {
    const { continueBtnContainer, contentContainerSerial, RaidoView, container, contentContainer,
      helpImage } = styles;
    console.log('');
    return (
      <View style={container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        {this.state.isLoading ?
          <View style={styles.indiView}>
            <ActIndicator animating={true} />
          </View>
          : true}
        <View style={contentContainer}>
          {/* {this.oldbarcodeView()} */}
          <View style={{ marginHorizontal: 10 }}>
            {this.loadBarodeScanner()}
          </View>
          <View>
            {this.state.isHidden === false ?
              <View style={{ marginTop: 50 }}>
                {this.stbTypeViewer()}
              </View> : null}
          </View>
          {this.state.isHidden === false ?
            [this.state.card_type == 'Y' ?
              <View style={{ paddingTop: 30 }}>
                <View style={contentContainerSerial}>
                  <View style={RaidoView}>
                    {this.renderCardList()}
                  </View>
                </View>
              </View>
              :
              true]
            : true}
          <View>
            {this.state.isHidden === false ?
              <View>
                <View style={{ marginHorizontal: 20, marginTop: 20, marginBottom: 10 }}>
                  {this.loadNewScanner()}
                </View>
                {this.state.hidebarcodetype == true ?
                  <View style={{ marginHorizontal: 20, marginTop: 60 }}>
                    {this.loadSIMScanner()}
                  </View>
                  : true}
              </View> : true}
          </View>
          <View style={continueBtnContainer} >
            <TouchableOpacity
              disabled={this.state.isContinueBtnDisabled}
              style={[styles.barcodeContinueBtn, this.state.isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
              onPress={() => this.goto_confirmationPage()}
            >
              <Text style={[styles.barcodeContinueBtnTxt, this.state.isContinueBtnDisabled ? { color: Colors.btnDeactiveTxtColor } : { color: Colors.btnActiveTxtColor }]} >{this.state.locals.continue}</Text>
            </TouchableOpacity>
          </View>
          <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
            <Image style={helpImage} source={fabIcon} />
          </DragComponent>
        </View>
      </View >
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: Colors.appBackgroundColor
  },
  contentContainer: {
    flex: 1.0
  },
  barcodeContinueBtn: {
    width: (width / 3),
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: 16,
    color: '#000000',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  stbTypeContainer: {
    marginTop: 24,
    marginBottom: 36,
    marginRight: 10
  },
  continueBtnContainer: {
    alignItems: 'flex-end',
    marginTop: '25%',
    marginRight: 24
  },
  stbTypeText: {
    fontSize: 12,
    lineHeight: 14,
    textAlign: 'left',
    color: 'rgba(0,0,0,0.54)'
  },
  iconStyle: {
    width: 30,
    height: 25,
    resizeMode: 'contain'
  },
  imageContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 25,
    marginTop: -40
  },

  imageContainer1: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 25,
    marginTop: -35
  },
  metarialInput: {
    paddingLeft: 40,
    paddingRight: 30
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
  dropDownMain: {
    marginTop: 8,
    paddingTop: 8,
    paddingBottom: 8,
    marginBottom: 8,
    backgroundColor: Colors.appBackgroundColor,
    // width: Dimensions.get('window').width * 1,
  },
  dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  dropDownSelectedItemView: {
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    width: '100%'
  },
  selectedItemTextContainer: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderLineColorLightGray
  },
  dropDownStyle: {
    flex: 1,
    marginTop: 0,
    width: '84%',
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropDownIcon: {
    width: '12%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-start'
  }, dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  serialContainer: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 40,
  },
  RaidoView: {
    marginBottom: 10,
    flexDirection: 'row'
  },
  RadioStyle: {
    alignItems: 'center',
    flex: 0.45
  },
  radioButtonLabelStyle: {
    marginLeft: 10
  },
  contentContainerSerial: {
    flexDirection: 'column'
  }
});

export default connect(mapStateToProps, actions)(StbDeliveryCustomerOwned);
