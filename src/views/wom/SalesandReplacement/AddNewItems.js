
/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/

import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    Image,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/Wom';
import SerialListComponent from '../components/SerialListComponent';
import DropDownInput from '../components/DropDownInput';
import DragComponent from '../../general/Drag';
import ActIndicator from './../ActIndicator';
import fabIcon from '../../../../images/icons/cfss/fab.png';
import TextFieldComponent from '../../directSale/components/TextFieldComponent';

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class AddNewItem extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            CPETypes: [],
            CPEObjects: [],
            offerTypes: [],
            offerObjets: [],
            selectedCPEItem: {},
            selectedCPE: '',
            selectedOffer: '',
            selctedOfferObj: '',
            offerPrice: '',
            offerData: false,
            scannedSerial: '',
            isSerializedCPE: false,
            apiLoading: false,
            refreshData: false,
            isHide: true,
            locals: {
                api_error_message: strings.api_error_message,
                network_error_message: strings.network_error_message,
                btnConfirm: strings.btnConfirm,
                btnCancel: strings.cancel,
                msgError: strings.error,
                lblPrice: strings.lblPrice,
                lblOffer: strings.lblOffer,
                lblCPEType: strings.lblCPEType,
                lblAlreadyAdded: strings.lblAlreadyAdded
            },
        };
        this.navigatorOb = this.props.navigator;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonPress);

        this.getCPETypes();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonPress);
        clearTimeout(this.apiTimer)
    }

    handleBackButtonPress = () => {
        this.goBack();
        return true;
    }

    goBack() { this.navigatorOb.pop(); }

    getCPETypes() {
        this.setState({ apiLoading: true }, () => {
            const data = { app_flow: this.props.WOM.work_order_app_flow };
            Utill.apiRequestPost('GetProduct', 'AccessorySale', 'cfss', data,
                this.getCPETypesSuccess, this.getCPETypesFailed, this.getCPETypesEx);
        });
    }

    getCPETypesSuccess = (response) => {
        this.setState({ apiLoading: false }, () => this.formatCPETypes(response.data.sale_items));
    }

    getCPETypesFailed = (response) => {
        this.setState({ apiLoading: false }, () => {
            this.displayAlert('defaultAlert', this.state.locals.msgError, response.data.error ? response.data.error : '', '');
        });
    }

    getCPETypesEx = (response) => {
        this.setState({ apiLoading: false }, () => {
            let error;
            if (response == 'Network Error') {
                error = this.state.locals.network_error_message;
                this.navigatorOb.showSnackbar({ text: error });
            } else {
                error = this.state.locals.api_error_message;
                this.navigatorOb.showSnackbar({ text: error });
            }
        });
    }

    displayAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                overrideBackPress: true,
            }
        });
    }

    formatCPETypes = (CPE_Resp) => {
        let CPETypes = [];
        let CPEObjects = [];

        CPE_Resp.map((item) => {
            CPETypes.push(item.name);
            CPEObjects.push(item);
        });

        this.setState({ CPETypes, CPEObjects });
    }

    getSelectedCPE(selectedIndex) {
        if (this.state.CPEObjects[selectedIndex].serialized == 'Y') {
            this.setState({ isSerializedCPE: true })
        } else {
            this.setState({ isSerializedCPE: false })
        }
        this.setState({
            isHide: true,
            refreshData: true,
            selectedCPE: this.state.CPETypes[selectedIndex],
            selectedCPEItem: { ...this.state.CPEObjects[selectedIndex], index: this.props.itemCount },
            selectedOffer: '',
            offerData: [],
            offerTypes: [],
            selctedOfferObj: '',
            offerPrice: '',
        });
    }

    getSelectedOffer(selectedIndex) {
        this.setState({
            selectedOffer: this.state.offerTypes[selectedIndex],
            selctedOfferObj: this.state.offerObjets[selectedIndex],
            offerPrice: this.state.offerObjets[selectedIndex].prod_price_with_tax
        })
    }

    isValidForm() {
        if (
            this.state.selectedCPE != '' &&
            this.state.scannedSerial != '' &&
            this.state.selectedOffer != '' &&
            this.state.offerPrice != ''
        ) {
            return true;
        } else return false;
    }

    addNewAccessory() {
        let addedItems = this.props.WOM.work_order_serial_list;
        let tempArrayOfItems = [];
        addedItems.map((item) => {
            if (item.serial == this.state.scannedSerial.serial && item.type == this.state.selectedCPE) {
                this.displayAlert('defaultAlert', this.state.locals.msgError, this.state.locals.lblAlreadyAdded, '');
            } else {
                tempArrayOfItems.push(item);
            }
        });
        var d = new Date();
        const newItemtoAdd = {
            root_id: d.getTime(),
            index: this.state.selectedCPEItem.index,
            type: this.state.selectedCPE,
            name: this.state.selectedCPE,
            ...this.state.scannedSerial,
            qty: '1',
            price: this.state.offerPrice,
            basic: 'N',
            sale_type: 'ADDITIONAL',
            offers: this.state.selctedOfferObj,
            offerCode: this.state.selctedOfferObj.offer_code,
        }

        tempArrayOfItems.push(newItemtoAdd);
        this.props.workOrderdtvinstallation(tempArrayOfItems);
        this.props.successAddNewItem(tempArrayOfItems);
        this.navigatorOb.pop();
    }

    isAllSerialsValidated = (isValidated, serialList) => {
        this.setState({
            refreshData: false,
            scannedSerial: serialList ? serialList : '',
            selectedOffer: '',
            offerData: [],
            offerTypes: [],
            offerObjets: [],
            selctedOfferObj: '',
            offerPrice: '',
        }, () => {
            this.apiTimer = setTimeout(() => {
                this.getOfferDetails();
            }, 1000)
        });
    }

    getOfferDetails() {
        this.setState({ apiLoading: true }, () => {
            const bodyData = {
                serial: this.state.scannedSerial,
                conn_type: this.props.WOM.work_order_detail.conn_type,
                sub_area: this.props.WOM.work_order_detail.sub_area
            };
            Utill.apiRequestPost('getOffers', 'accessorySale', 'cfss', bodyData,
                this.getOffersSuccessCb, this.getOffersFailureCb, this.getOffersExCb);
        });
    }

    getOffersSuccessCb = (res) => {
        this.setState({
            apiLoading: false,
            offerData: res.data.data,
            isHide: false
        }, () => this.formatOfferData())
    }

    getOffersFailureCb = (response) => {
        this.setState({ isHide: true, apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.msgError, response.data.error, '');
        });
    }

    getOffersExCb = (response) => {
        this.setState({ apiLoading: false, isHide: true }, () => {
            let error;
            if (response == 'Network Error') {
                error = this.state.locals.network_error_message;
                this.navigatorOb.showSnackbar({ text: error });
            } else {
                error = this.state.locals.api_error_message;
                this.navigatorOb.showSnackbar({ text: error });
            }
        });
    }

    formatOfferData() {
        let offerTypes = [];
        let offerObjets = [];

        this.state.offerData.map((item) => {
            offerTypes.push(item.offer_code);
            offerObjets.push(item);
        });

        this.setState({ offerTypes, offerObjets });
    }

    displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            title: 'IN-PROGRESS WORKORDER',
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                onPressOK: () => this.setState({ isHide: true })
            }
        });
    }

    showIndicator() {
        if (this.state.apiLoading) {
            return (
                <View style={styles.indiView}>
                    <ActIndicator animating={true} />
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topContainer} />
                <View style={styles.middleContainer}>
                    {this.showIndicator()}
                    <ScrollView style={{ marginHorizontal: 20 }}>
                        <DropDownInput
                            alignDropDown={true}
                            dropDownTitle={this.state.locals.lblCPEType}
                            selectedValue={this.state.selectedCPE}
                            dropDownData={this.state.CPETypes}
                            onSelect={selectedIndex => this.getSelectedCPE(selectedIndex)}
                        />
                        {this.state.isSerializedCPE ?
                            <View>
                                <View style={{ marginTop: 36 }}>
                                    <SerialListComponent
                                        refreshData={this.state.refreshData}
                                        apiParams={{
                                            ownership: this.props.WOM.work_order_detail.ownership,
                                            order_id: this.props.WOM.work_order_detail.order_id,
                                            wo_type: this.props.WOM.work_order_detail.wo_type,
                                            app_flow: this.props.WOM.work_order_detail.app_flow
                                        }}
                                        navigatorOb={this.props.navigator}
                                        actionName='validateSerialNew'
                                        controllerName='serial'
                                        moduleName='wom'
                                        serialData={this.state.selectedCPEItem}
                                        isAllSerialsValidated={this.isAllSerialsValidated}
                                    />
                                </View>
                                {this.state.isHide == false ?
                                    <View>
                                        <DropDownInput
                                            dropDownTitle={this.state.locals.lblOffer}
                                            hideRightIcon={true}
                                            selectedValue={this.state.selectedOffer}
                                            onSelect={selectedIndex => {
                                                this.getSelectedOffer(selectedIndex);
                                            }}
                                            dropDownData={this.state.offerTypes} />

                                        <TextFieldComponent
                                            label={this.state.locals.lblPrice}
                                            value={this.state.offerPrice != '' ? 'Rs.' + this.state.offerPrice : ''}
                                            editable={false} />
                                    </View>
                                    :
                                    true}
                            </View> : <View />}
                    </ScrollView>
                </View>
                <View style={styles.bottomContainer}>
                    <View style={styles.direbutRow}>
                        <View style={styles.dirImageView} />
                        <View style={styles.filterButView}>
                            <View>
                                <TouchableOpacity onPress={() => this.goBack()}>
                                    <Text style={styles.btnCancel}>{this.state.locals.btnCancel}</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                {this.isValidForm() ?
                                    <TouchableOpacity onPress={() => this.addNewAccessory()}>
                                        <Text style={styles.filterText}>{this.state.locals.btnConfirm}</Text>
                                    </TouchableOpacity> :
                                    <TouchableOpacity disabled={true}>
                                        <Text style={styles.filterTextDisabled}>{this.state.locals.btnConfirm}</Text>
                                    </TouchableOpacity>}
                            </View>
                        </View>
                    </View>
                </View>
                <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
                    <Image style={styles.helpImage} source={fabIcon} />
                </DragComponent>
            </View >
        );
    }
}

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    topContainer: {
        flex: 0.05,
    },
    middleContainer: {
        flex: 0.8,
        marginRight: 8,
        marginLeft: 8,
        borderBottomColor: Colors.borderLineColorLightGray,
    },
    bottomContainer: {
        flex: 0.15,
        flexDirection: 'row'
    },
    direbutRow: {
        flexDirection: 'row',
        marginLeft: 10,
        paddingTop: 20,
        borderTopWidth: 1,
        borderTopColor: '#eeeeee',
        borderTopWidth: 1,
        paddingBottom: 20
    },
    dirImageView: {
        justifyContent: 'flex-start',
        width: (width / 2) - 10,
        paddingTop: 10
    },
    filterButView: {
        width: (width / 2) - 10,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    filterText: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#ffc400',
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    btnCancel: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    filterTextDisabled: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: Colors.btnDisable,
        color: '#000000',
        borderRadius: 5,
        textAlign: 'center'
    },
    helpImage: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },
    indiView: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
    },
});

export default connect(mapStateToProps, actions)(AddNewItem);