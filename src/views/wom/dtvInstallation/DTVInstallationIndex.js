import React, { Component } from 'react';
import { View, Text } from 'react-native';

import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import DTVInstallationView  from './DTVInstallationView';

class DTVInstallation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### Dtv instaion :: componentDidMount');
    // Analytics.logEvent('Dtv instaion ');
  }

  render() {
    return (
      <View>
        <DTVInstallationView {...this.props}/>
      </View>
    );
  }
}

export default DTVInstallation;