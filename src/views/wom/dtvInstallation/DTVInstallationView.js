import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, BackHandler, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash'

import Colors from '../../../config/colors';
import * as actions from '../../../actions';
import { Header } from '../Header';
import strings from '../../../Language/Wom';
import SerialListComponent from '../components/SerialListComponent';
import TextItemComponent from '../components/TextItemComponent';
import RadioButton from '../components/RadioButtonComponent';
import DragComponent from '../../general/Drag';
import { globalConfig } from '../../../config';

const { width } = Dimensions.get('window')
class DTVInstallationView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      serialData: this.props.serialData ? this.props.serialData ? this.props.serialData : {} : {},
      cardlessArray: [],
      validatedSerials: [],
      isContinueBtnDisabled: true,
      selectedStatus: 'N',
      isRefreshserial: false,
      cardOption: this.props.card_option ? this.props.card_option : false,
      locals: {
        continue: strings.continue,
        cardless: strings.cardless,
        inProgressTitle: strings.inProgressTitle
      }
    };
    this.oldSerialData = this.props.serialData;
    this.navigatorOb = this.props.navigator;
    this.debounceContinueOnPress = _.debounce(() => this.continueOnPress(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      })
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler()
    return true;
  }

  okHandler = () => {
    this.navigatorOb.pop();
  }

  continueOnPress() {
    let validatedSerials;
    let updatedSerialList;

    if (this.state.cardOption) {
      const serialList = this.state.validatedSerials;

      const newSerialArray = serialList.map(item => {
        const obj = {
          ...item,
          card_less: this.state.selectedStatus
        }
        return obj;
      })

      validatedSerials = newSerialArray;
    } else {
      validatedSerials = this.state.validatedSerials;
    }

    const isChecked = false;

    if (this.checkIsArray(validatedSerials)) {
      updatedSerialList = validatedSerials.map(item => {
        let value = { ...item, isChecked };
        return value;
      });
    } else {
      const listArray = [validatedSerials];

      updatedSerialList = listArray.map(item => {
        let value = { ...item, isChecked };
        return value;
      });
    }

    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.DTVInstallationItem',
      passProps: { serials: updatedSerialList }
    });
  }

  checkIsArray(validatedSerials) {
    const value = Array.isArray(validatedSerials);
    return value
  }

  isAllSerialsValidated = (isValidated, serialList) => {
    this.setState({
      isRefreshserial: false,
      isContinueBtnDisabled: isValidated ? false : true,
      validatedSerials: serialList ? serialList : ''
    });
  }

  onSelectStatus(value) {
    this.setState({ selectedStatus: value, isRefreshserial: true, isContinueBtnDisabled: true, }, () => {
      if (this.state.selectedStatus === 'Y') {
        const cardlessArray = this.state.serialData.filter(item => {
          if (item.type === 'STB') {
            return true;
          }
        })

        this.setState({ serialData: cardlessArray })
      } else {
        this.setState({ serialData: this.oldSerialData })
      }
    });
  }

  renderCardLessItem = (item) => {
    const { RadioStyle, radioButtonLabelStyle } = styles;
    return (
      <View style={RadioStyle}>
        <RadioButton
          currentValue={this.state.selectedStatus}
          value={item.key}
          outerCircleSize={20}
          innerCircleColor={Colors.radioBtn.innerCircleColor}
          outerCircleColor={Colors.radioBtn.outerCircleColor}
          onPress={(value) => this.onSelectStatus(value)}>
          <Text style={radioButtonLabelStyle}>{item.value}</Text>
        </RadioButton>
      </View>
    )
  }

  renderCardList = () => {
    return this.state.cardOption.map(item => {
      return this.renderCardLessItem(item);
    });
  }

  renderButtonContainer() {
    return (
      <View style={styles.continueBtnContainer} >
        <TouchableOpacity
          disabled={this.state.isContinueBtnDisabled}
          style={[styles.barcodeContinueBtn, this.state.isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
          onPress={() => this.debounceContinueOnPress()}
        >
          <Text style={[styles.barcodeContinueBtnTxt, this.state.isContinueBtnDisabled ? { color: Colors.btnDeactiveTxtColor } : { color: Colors.btnActiveTxtColor }]} >{this.state.locals.continue}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  stbTypeViewer() {
    if (this.props.WOM.work_order_detail.new_stb_model !== undefined && this.props.WOM.work_order_detail.new_stb_model !== null && this.props.WOM.work_order_detail.new_stb_model !== '') {
      return (
        <View>
          <TextItemComponent label1={strings.newSTBModel} label2={this.props.WOM.work_order_detail.new_stb_model} />
        </View>
      );
    } else {
      return (
        <View>
          <TextItemComponent label1={strings.STB_Type} label2={this.props.WOM.work_order_detail.stb_type} />
        </View>
      );
    }
  }

  SerialListComponent() {
    return (
      <View style={styles.serialListContainer}>
        <SerialListComponent
          scanCounter={0}
          apiParams={{
            conn_no: this.props.WOM.work_order_detail.conn_no,
            sub_area: this.props.WOM.work_order_detail.sub_area,
            prod_list: this.props.WOM.work_order_detail ? this.props.WOM.work_order_detail.product_detail.product_list ? this.props.WOM.work_order_detail.product_detail.product_list : {} : {},
            ownership: this.props.WOM.work_order_detail.ownership,
            order_id: this.props.WOM.work_order_detail.order_id,
            wo_type: this.props.WOM.work_order_detail.wo_type,
            app_flow: this.props.WOM.work_order_detail.app_flow
          }}
          manualLabel={true}
          actionName='validateSerialNew'
          controllerName='serial'
          moduleName='wom'
          serialData={this.state.serialData}
          navigatorOb={this.props.navigator}
          isAllSerialsValidated={this.isAllSerialsValidated}
          refreshData={this.state.isRefreshserial}
        />
      </View>
    );
  }

  Typecomponent() {
    const { stbTypeContainer } = styles;
    return (
      <View style={stbTypeContainer} >
        {this.props.WOM.work_order_detail.wo_type === 'ACCESSORY_DELV' ?
          true :
          <View>{this.stbTypeViewer()}</View>
        }
      </View>
    );
  }

  cardOptionTrue() {
    const { contentContainerSerial, RaidoView, } = styles
    return (
      <View style={{ flex: 1 }}>
        <View style={{ paddingTop: 10 }}></View>
        <View style={contentContainerSerial}>
          <View style={RaidoView}>
            {this.renderCardList()}
          </View>
        </View>
        {this.SerialListComponent()}
        {this.renderButtonContainer()}
      </View>
    );
  }

  cardOptionFalse() {
    return (
      <View style={{ flex: 1 }}>
        {this.SerialListComponent()}
        {this.renderButtonContainer()}
      </View>
    )
  }

  render() {
    const { container, contentContainer } = styles;
    return (
      <View style={container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle} />
        <View style={contentContainer}>
          {this.Typecomponent()}
          {this.state.cardOption ?
            this.cardOptionTrue()
            :
            this.cardOptionFalse()}
          <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
            <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
          </DragComponent>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: Colors.appBackgroundColor
  },
  contentContainer: {
    flex: 1.0,
    flexDirection: 'column'
  },
  barcodeContinueBtn: {
    width: (width / 3),
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: 16,
    color: '#000000',
    textAlign: 'center',
  },
  stbTypeContainer: {
    marginTop: 24
  },
  continueBtnContainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: 30,
    marginRight: 24,
  },
  serialListContainer: {
    flex: 2,
    marginHorizontal: 40
  },
  RaidoView: {
    marginBottom: 25,
    flexDirection: 'row'
  },
  RadioStyle: {
    alignItems: 'center',
    flex: 0.45
  },
  radioButtonLabelStyle: {
    marginLeft: 10
  },
  contentContainerSerial: {
    flexDirection: 'column'
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
});

export default connect(mapStateToProps, actions)(DTVInstallationView);