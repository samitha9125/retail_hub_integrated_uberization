import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  Dimensions,
  TouchableOpacity,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import { Header } from '../Header';
import ActIndicator from '../ActIndicator';
import strings from '../../../Language/Wom';
import globalConfig from '../../../config/globalConfig';

const Utill = new Utills();
const { height } = Dimensions.get('window');
class WomSlaBreach extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      isBtnDisabled: true,
      apiLoading: false,
      delayReason: '',
      selectedId: '',
      selectValue: '',
      dropdownArray: [],
      locals: {
        network_error_message: strings.network_error_message,
        screenTitle: strings.screenTitle,
        slaBreach: strings.slaBreach,
        selectionDescription: strings.selectionDescription,
        submit: strings.submit,
        api_error_message: strings.api_error_message,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        lblError: strings.error,
        inProgressTitle: strings.inProgressTitle,
        alertHeader: strings.alertHeader,
        lblSuccess: strings.lblSuccess
      }
    };
    this.navigatorOb = this.props.navigator;
    this.debounceupdateWorkOrder = _.debounce(() => this.updateWorkOrder(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    let dataArray = this.props.data;
    this.props.data ?
      this.formatDropDownValues(dataArray)
      :
      true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    this.navigatorOb.resetTo({
      title: 'DIALOG SALES APP',
      screen: 'DialogRetailerApp.views.WomDetailView'
    });
  }

  formatDropDownValues(dataArray) {
    const valueArray = dataArray.map(item => { return item.value });
    this.setState({ dropdownArray: valueArray });
  }

  updateWorkOrder() {
    this.setState({ apiLoading: true }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {

          let phone_number_list = '';
          const noArray = this.props.WOM.work_order_detail.phone_number_list

          if (noArray.length > 0) {
            for (const i = 0; i < noArray.length && i < 2; i++) {
              if (noArray[i] !== '') {
                if (i == 1) {
                  phone_number_list += noArray[i];
                } else {
                  phone_number_list += noArray[i] + ',';
                }
              }
            }
          }

          const packageobj = this.props.WOM.work_order_detail.summary.find(item => {
            if (item.name == 'Package') {
              return true;
            }
          });

          const accountObj = this.props.WOM.work_order_detail.summary.find(item => {
            if (item.name == 'Account Number') {
              return true;
            }
          });

          let packageValue;

          if (packageobj !== undefined) {
            packageValue = packageobj.value;
          }

          let accountValue;

          if (accountObj !== undefined) {
            accountValue = accountObj.value;
          }
          let data;
          data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            delay_id: this.state.selectedId,
            status: 'done',
            cir: this.props.WOM.work_order_detail.cir,
            app_flow: this.props.WOM.work_order_app_flow,
            serial_list: this.props.WOM.work_order_serial_list,
            contact_no: this.props.WOM.work_order_detail.contact_no,
            latitude: loc.latitude,
            longitude: loc.longitude,
            contact: phone_number_list,
            biz_unit: this.props.WOM.work_order_detail.biz_unit,
            customer_nic: this.props.WOM.work_order_detail.nic,
            customer_name: this.props.WOM.work_order_detail.cx_name,
            customer_msisdn: this.props.WOM.work_order_detail.contact,
            request_type: this.props.WOM.work_order_detail.wo_type,
            reason_id: this.props.reason_id ? this.props.reason_id : null,
            sub_reason_id: this.props.sub_reason_id ? this.props.sub_reason_id : null,
            ownership: this.props.WOM.work_order_detail.ownership,
            lob: this.props.WOM.work_order_detail.biz_unit,
            account_no: accountValue,
            package: packageValue,
            nic: this.props.WOM.work_order_detail.nic,
            invoiceNo: this.props.WOM.work_order_detail.product_detail.cpos_invoice_no,
          };

          if (this.props.lte_job_id && this.props.lte_order_id) {
            data.lte_job_id = this.props.lte_job_id;
            data.lte_order_id = this.props.lte_order_id;
          }

          Utill.apiRequestPost('updateBreachedReason', 'Feedback', 'wom', data,
            this.submitDelayReasonSuccessCb, this.submitDelayReasonFailureCb, this.submitDelayReasonexCb);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.message, '');
          });
        });
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle
      }
    })
  }

  submitDelayReasonSuccessCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.success && !response.data.data) {
        this.displaySuccessAlert('successWithOk', this.state.locals.lblSuccess, response.data.info, '');
      } else if (response.data.data.app_screen) {
        this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  submitDelayReasonFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: this.state.locals.alertHeader,
          messageBody: response.data.error,
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { this.navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    });
  }

  submitDelayReasonexCb = (error) => {
    this.setState({ apiLoading: false }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.debounceupdateWorkOrder);
      } if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  displaySuccessAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => this.gotoHome()
      },
      overrideBackPress: true,
    });
  }

  gotoHome() {
    this.navigatorOb.resetTo({
      title: 'DIALOG SALES APP',
      screen: 'DialogRetailerApp.views.WomLandingView'
    });
  }

  setSelectedValue = (idx, value) => {
    let dataArray = this.props.data;
    let selectedId = dataArray[idx].key;
    let selectValue = dataArray[idx].value
    this.setState({
      isBtnDisabled: false,
      selectedId: selectedId,
      selectValue: selectValue
    })
  }

  elementFooter = () => (
    <View style={styles.bottomContainer}>
      <View style={styles.dummyView} />
      {this.state.isBtnDisabled ?
        <TouchableOpacity
          disabled={true}
          style={[styles.buttonContainer, { backgroundColor: Colors.btnDisable }]}
          activeOpacity={1}>
          <Text style={styles.buttonDisabledTxt}>{this.state.locals.submit}
          </Text>
        </TouchableOpacity>
        :
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => this.debounceupdateWorkOrder()}
          activeOpacity={1}>
          <Text style={styles.buttonTxt}>{this.state.locals.submit}
          </Text>
        </TouchableOpacity>}
    </View>
  );

  elementSlaTime = () => (
    <View style={styles.containerSlaTime}>
      <Text style={styles.slaTimeText}>
        {this.props.WOM.work_order_detail.sla_lapse}
      </Text>
    </View>
  );

  elementTitle = () => (
    <View style={styles.containerTitle}>
      <Text style={styles.titleText}>
        {this.state.locals.slaBreach}
      </Text>
    </View>
  );

  elementSelctionMenu = () => (
    <ModalDropdown
      options={this.state.dropdownArray}
      onSelect={(idx, value) => this.setSelectedValue(idx, value)}
      defaultIndex={1}
      style={styles.modalDropdownStyles}
      dropdownStyle={styles.dropdownStyle}
      dropdownTextStyle={styles.dropdownTextStyle}
    >
      <View style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElemint}>
          <Text style={styles.dropdownDataElemintTxt}>{this.state.selectValue}</Text>
        </View>
        <View style={styles.dropdownArrow}>
          <Ionicons name='md-arrow-dropdown' size={20} />
        </View>
      </View>
    </ModalDropdown>
  );

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.inProgressTitle}
        />
        {this.state.apiLoading ?
          <View style={styles.indiView} >
            <ActIndicator animating={true} />
          </View>
          :
          true}
        {this.elementTitle()}
        {this.elementSlaTime()}
        <View style={styles.containerDropDown}>
          <View style={styles.dropDownTextContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.selectionDescription}</Text>
          </View>
          {this.elementSelctionMenu()}
        </View>
        <View style={styles.containerBottom}>
          {this.elementFooter()}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;

  return { Language, WOM };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerTitle: {
    flex: 1,
    paddingTop: 18,
  },
  containerSlaTime: {
    flex: 1,
    paddingTop: 0
  },
  containerDropDown: {
    flex: 2,
    paddingTop: 5,
    marginTop: 5,
  },
  containerBottom: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  titleText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 10,
    paddingBottom: 5,
    margin: 5
  },
  slaTimeText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 5,
    color: Colors.colorRed
  },
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },
  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },
  buttonDisabledTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.btnDisable,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },
  dropDownTextContainer: {
    flex: 0.6,
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18
  },
  modalDropdownStyles: {
    width: Dimensions.get('window').width
  },
  dropdownStyle: {
    width: Dimensions.get('window').width - 30,
    marginLeft: 15,
    height: 120,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },
  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(WomSlaBreach);
