import React from 'react';
import {
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box';
import _ from 'lodash';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import { Header } from '../Header';
import CardComponent from './CardView';
import strings from '../../../Language/Wom';
import DragComponent from '../../general/Drag';
import globalConfig from '../../../config/globalConfig';
class DTVInstallationItemView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      dtvInstalltionData: props.serials,
      isEditable: false,
      isChecked: false,
      hideEditbutton: false,
      locals: {
        screenTitle: strings.inProgressTitle,
        continue: strings.continue,
        cancel: strings.cancel,
        free_of_change: strings.free_of_change,
        btnAddNew: strings.btnAddNew,
        btnConfirm: strings.btnConfirm,
        btnEdit: strings.btnEdit,
        btnDelete: strings.btnDelete,
        qty: strings.Qty,
        serialNo: strings.Serial_no,
        totalCost: strings.totalCost
      }
    };
    this.navigatorOb = this.props.navigator;
    this.debouncebtnConfirmAction = _.debounce(() => this.btnConfirmAction(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.formatReceivedSerials();
  }

  checkBasicItem() {
    let basicItem = this.state.dtvInstalltionData.find((element) => {
      if (element.basic == 'N') {
        return element;
      }
    });
    console.log('checkBasicItem basicItem ', basicItem)

    if (basicItem == undefined) {
      console.log('checkBasicItem hideEditbutton : true ')

      this.setState({ hideEditbutton: true })
    } else {
      console.log('checkBasicItem hideEditbutton : false')
      this.setState({ hideEditbutton: false })
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.pop({ animated: true, animationType: 'fade' });
    return true;
  }

  formatReceivedSerials() {
    let serialArray = [];
    let formattedSerials = [];
    serialArray = this.state.dtvInstalltionData;

    for (let i = 0; i < serialArray.length; i++) {
      const productType = serialArray[i].type;
      const productSerial = serialArray[i].serial;
      const productSapCode = serialArray[i].bundle_code;
      const newObj = {
        'accessoryType': productType,
        'sapMaterialCode': productSapCode,
        'serialNo': productSerial,
      }
      formattedSerials.push(newObj);
    }
    //to-do, please check this function nessasory
    this.props.workOrderGetProvisioningStatus('pending');
    this.props.workOrderSerialList(this.state.dtvInstalltionData);
    this.props.workOrderGetInstalledItems(formattedSerials);
    this.props.workOrderdtvinstallation(this.state.dtvInstalltionData);
    this.checkBasicItem();
  }

  btnConfirmAction() {
    this.formatReceivedSerials();
    let isSignatureRequired = true;

    this.props.WOM.work_order_detail.wo_type === 'ACCESSORY_DELV' ? isSignatureRequired = false : true;
    this._onConfirmPressed(isSignatureRequired);
  }

  _onConfirmPressed = (isSignatureRequired) => {
    const bill = this.calculateBill();
    let paymentActive;

    if (bill > 0) {
      paymentActive = true;
    } else {
      paymentActive = false;
    }

    let passProps = {
      totalGrand: bill,
      isPaymentAvailable: paymentActive,
      dataArray: this.state.dtvInstalltionData
    }

    this.props.simstbChange ? passProps.simstbChange = true : true;
    this.props.simstbChange ? passProps.isSignatureRequired = false : passProps.isSignatureRequired = isSignatureRequired;

    this.navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.WOMCustomerConfirmation',
      passProps
    });
  }

  _onAddPressed = () => {
    this.navigatorOb.push({
      title: this.state.locals.screenTitle,
      screen: 'DialogRetailerApp.views.AddNewItem',
      passProps: {
        itemCount: this.state.dtvInstalltionData.length,
        successAddNewItem: (tempArrayOfItems) => {
          const editArray = tempArrayOfItems.map(item => {
            return { ...item, isChecked: false }
          });
          this.setState({ dtvInstalltionData: editArray }, () => this.formatReceivedSerials());
        },
      }
    });
  }

  calculateBill() {
    let total = 0;
    const dataArray = this.state.dtvInstalltionData;
    for (const i = 0; i < dataArray.length; i++) {
      let price = dataArray[i].price;
      if (price) {
        total += parseFloat(price);
      }
    }
    console.log('calculateBill ', total.toFixed(2))

    return total.toFixed(2);
  }

  _editPressed = () => {
    let dtvInstalltionData = this.state.dtvInstalltionData.map(item => {
      let isChecked = false;
      const newObj = { ...item, isChecked }
      return newObj;
    });
    this.setState({ dtvInstalltionData, isEditable: !this.state.isEditable });
  }

  _cancelPressed = () => {
    let dtvInstalltionData = this.state.dtvInstalltionData.map(item => {
      let isChecked = false;
      const newObj = { ...item, isChecked }
      return newObj;
    });
    this.setState({ dtvInstalltionData, isEditable: !this.state.isEditable });
  }

  _deletePressed = () => {
    const dtvInstalltionData = this.state.dtvInstalltionData.filter(item => {
      if (item.isChecked == false) return item;
    })
    this.setState({ dtvInstalltionData, isEditable: !this.state.isEditable }, () => this.formatReceivedSerials());
  }

  checkBoxValueChange = (item) => {
    let newArray = [];
    if (item.isChecked === true) {
      const isChecked = false;
      newArray = this.state.dtvInstalltionData.map(value => {
        if (value.index == item.index) {
          let newObj = { ...value, isChecked };
          return newObj;
        }
        else return value;
      });
    } else {
      const isChecked = true;
      newArray = this.state.dtvInstalltionData.map(value => {
        if (value.index == item.index) {
          let newObj = { ...value, isChecked };
          return newObj;
        }
        else return value;
      });
    }
    this.setState({ dtvInstalltionData: newArray });
  }

  renderListItem = ({ item, index }) => (
    <View style={styles.cardComponentView} key={index} >
      <View style={{ flex: 1, flexDirection: 'column' }}>
        {this.state.isEditable === true ?
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 0.1 }}>
              {item.basic == 'N' ?
                <View>
                  {item.isChecked === true ?
                    <CheckBox
                      isChecked={item.isChecked}
                      style={{ paddingTop: 70, paddingLeft: 5, paddingRight: 5 }}
                      checkBoxColor={Colors.yellow}
                      onClick={() => this.checkBoxValueChange(item)} />
                    :
                    <CheckBox
                      isChecked={item.isChecked}
                      style={{ paddingTop: 70, paddingLeft: 0, paddingRight: 5 }}
                      checkBoxColor={Colors.grey}
                      onClick={() => this.checkBoxValueChange(item)} />}
                </View> :
                true}
            </View>
            <View style={{ flex: 0.9 }}>
              <CardComponent
                title={item.name}
                qtyLable={this.state.locals.qty}
                lable1={this.state.locals.serialNo}
                Item1={item.serial}
                qty={item.qty}
                price={item.price == '0' ? < Text > {this.state.locals.free_of_change}</Text> : <Text>{item.price}</Text>}
              />
            </View>
          </View>
          : <View>
            <CardComponent
              title={item.name}
              qtyLable={this.state.locals.qty}
              lable1={this.state.locals.serialNo}
              Item1={item.serial}
              qty={item.qty}
              price={item.price == '0' ? < Text > {this.state.locals.free_of_change}</Text> : <Text>{item.price}</Text>}
            />
          </View>}
      </View>
    </View>
  );

  render() {
    const { grandTotalText, containerBottom, background, containerBottomSection2 } = styles;
    let screenContent;
    console.log('ARRAY FORMATED', this.state.dtvInstalltionData)
    screenContent = (
      <View style={background}>
        <FlatList
          data={this.state.dtvInstalltionData}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => index.toString()}
          scrollEnabled={true}
          extraData={this.state}
        />
        <View style={containerBottom}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 23,marginHorizontal:10 }} >
            <Text style={grandTotalText}>{this.state.locals.totalCost}</Text>
            <Text style={grandTotalText}>Rs   {this.calculateBill()}</Text>
          </View>

          <View style={containerBottomSection2}>
            {this.state.isEditable === false ?
              <TouchableOpacity style={
                this.props.WOM.work_order_detail.wo_type == ('FeatureHD' || 'ACCESSORY_DELV' || 'STB_CHANGE' || 'SIM_CHANGE' || 'SIM_STB_CHANGE') ? { display: 'none' } // to-do accessory sale available for SIM / BOTH / STB
                  : true}
                onPress={this._onAddPressed}>
                <Text style={styles.filterText}>{strings.btnAddNew}</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity style={
                this.props.WOM.work_order_detail.wo_type == ('FeatureHD' || 'ACCESSORY_DELV' || 'STB_CHANGE' || 'SIM_CHANGE' || 'SIM_STB_CHANGE') ? { display: 'none' } // to-do accessory sale available for SIM / BOTH / STB
                  : true} disabled={true} onPress={this._onAddPressed}>
                <Text style={styles.filterTextDisabled}>{strings.btnAddNew}</Text>
              </TouchableOpacity>}
            <View>
              {this.state.isEditable === false ?
                <TouchableOpacity onPress={() => this.debouncebtnConfirmAction()}>
                  <Text style={styles.filterText}>{this.state.locals.btnConfirm}</Text>
                </TouchableOpacity> :
                <TouchableOpacity disabled={true}>
                  <Text style={styles.filterTextDisabled}>{this.state.locals.btnConfirm}</Text>
                </TouchableOpacity>}
            </View>
          </View>
        </View>
        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
        </DragComponent>
      </View>
    );
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        <View style={{ backgroundColor: Colors.appBackgroundColor }}>
          {this.props.appFlow == "VIEW_EQUIPMENT_NO" ?
            true
            :
            <View style={{ alignItems: 'flex-end' }}>
              {this.state.isEditable === true ?
                (<View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, backgroundColor: Colors.appBackgroundColor }}>
                  <TouchableOpacity onPress={() => this._cancelPressed()}>
                    <Text style={styles.CancelText}>{this.state.locals.cancel}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this._deletePressed()}>
                    <Text style={styles.DeleteText}>{this.state.locals.btnDelete}</Text>
                  </TouchableOpacity>
                </View>)
                :
                <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, backgroundColor: Colors.appBackgroundColor }}>
                  {this.state.hideEditbutton == true ?
                    true
                    :
                    [this.state.isEditable == false ?
                      <TouchableOpacity onPress={() => this._editPressed()}>
                        <Text style={styles.EditText}>{this.state.locals.btnEdit}</Text>
                      </TouchableOpacity>
                      : true]}
                </View>}
            </View>}
        </View>
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = {
  background: {
    backgroundColor: Colors.appBackgroundColor,
    flex: 1
  },
  container: {
    height: '100%'
  },
  orderDetail: {
    flex: 5,
    marginLeft: 10,
    marginBottom: 0,
    marginTop: 0,
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  cardComponentView: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10
  },
  containerBottom: {
    height: '20%',
    borderWidth: 1,
    paddingTop: 13,
    paddingLeft: 7,
    paddingRight: 24,
    paddingBottom: 25,
    borderColor: Colors.borderLineColorLightGray,
  },
  containerBottomSection2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  EditText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 10
  },
  CancelText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 15
  },
  DeleteText: {
    color: Colors.rootcauseEditbutton,
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 10
  },
  grandTotalText: {
    color: Colors.black,
    fontSize: 16,
    lineHeight: 19,
    fontWeight: '500'
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
};

export default connect(mapStateToProps, actions)(DTVInstallationItemView);


