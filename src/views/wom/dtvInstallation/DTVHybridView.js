import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from '../ActIndicator';
import DragComponent from '../../general/Drag';
import strings from '../../../Language/Wom';
import DropDownComponent from '../components/DropdownComponent';
import _ from 'lodash';
import globalConfig from '../../../config/globalConfig';

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class DTVHybridView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      installationTypeValue: [],
      installationTypeIndexs: [],
      selectedRootCause: '',
      selectedRootCauseID: '',

      reasonValues: [],
      reasonIndexes: [],
      selectedReason: '',
      selectedReasonID: '',

      subReason: [],
      subReasonValues: [],
      subReasonIndexes: [],
      selectedSubReason: '',
      selectedSubReasonID: '',

      contBtnEnable: false,

      locals: {
        customerSignature: strings.customerSignature,
        customerConfirmation: strings.customerConfirmation,
        btnProvision: strings.provisionButton,
        dtvHybridDropdown: strings.dtvHybridDropdown,
        inProgressTitle: strings.inProgressTitle,
        continue: strings.continue,
        dtvRejRes: strings.dtvRejRes,
        dtvRejSubRes: strings.dtvRejSubRes,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        lblError: strings.error
      },
    };
    this.navigatorOb = this.props.navigator;
    this.debounceonPressConfirm = _.debounce(() => this.onPressConfirm(), globalConfig.buttonTapDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonPress);
  }

  componentDidMount() {
    this.formatDropdownData();
    this.formatRejectReasonDropdownData();
    this.appendProductList();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonPress)
  }

  handleBackButtonPress = () => {
    this.navigatorOb.resetTo({
      title: 'DIALOG SALES APP',
      screen: 'DialogRetailerApp.views.WomDetailView',
    });
    return true;
  }

  formatDropdownData() {
    let installationTypeValue = [];
    let installationTypeIndexs = [];

    this.props.hybridData.map((item, key) => {
      installationTypeValue.push(item.value);
      installationTypeIndexs.push(item.id);
    });

    let selectedValue = installationTypeValue[0];
    let selectedId = installationTypeIndexs[0];

    this.setState({
      installationTypeValue,
      installationTypeIndexs,
      selectedRootCause: selectedValue,
      selectedRootCauseID: selectedId,
      selectedReason: '',
      selectedSubReason: ''
    }, () => { this.btnEnble(); });
  }

  formatRejectReasonDropdownData() {
    let reasonValues = [];
    let reasonIndexes = [];

    this.props.rejectdata.map((item, key) => {
      reasonValues.push(item.category);
      reasonIndexes.push(item.id);
    });

    this.setState({ reasonValues, reasonIndexes })
  }

  appendProductList() {

    let tempWODetail = this.props.WOM.work_order_detail;
    let existingProductList = tempWODetail.product_detail.product_list;
    let tempProdList = [];
    if (existingProductList) {
      existingProductList.map((item) => {
        tempProdList.push(item);
      });
    }

    let hybridProdList = [{
      "Product": "19084",
      "ProductType": "DTV_ACCESSORIES",
      "ProductDescription": "NewAccessoryPack-TR",
      "SapMaterialCode": "7000005461",
      "CposInvNumber": "DINCDRCFCOL0006219",
      "OrderOfferItemLineNo": "5"
    }, {
      "Product": "19085",
      "ProductType": "DTV_ACCESSORIES",
      "ProductDescription": "NewAccessoryPack-TR",
      "SapMaterialCode": "7000005461",
      "CposInvNumber": "DINCDRCFCOL0006219",
      "OrderOfferItemLineNo": "5"
    }
    ];

    if (hybridProdList) {
      hybridProdList.map((item) => {
        tempProdList.push(item);
      })
    }
    this.props.addAdditionalHybridProducts(tempProdList);
  }

  selectDropdownData(item) {
    let rootCause = this.state.installationTypeValue[item];
    let rootCauseId = this.state.installationTypeIndexs[item];

    this.setState({
      selectedRootCause: rootCause,
      selectedRootCauseID: rootCauseId,
      selectedReason: '',
      selectedSubReason: ''
    }, () => { this.btnEnble(); });
  }

  selectRejectReasonDropdownData(item) {
    let rootCause = this.state.reasonValues[item];
    let rootCauseId = this.state.reasonIndexes[item];

    let subReasonValues = [];
    let subReasonIndexes = [];

    this.props.rejectdata[item].reason.map((item, key) => {
      subReasonValues.push(item.sub_reason);
      subReasonIndexes.push(item.sub_id);
    });

    this.setState({
      subReasonValues,
      subReasonIndexes,
      selectedSubReason: '',
      selectedSubReasonID: '',
      selectedReason: rootCause,
      selectedReasonID: rootCauseId
    }, () => { this.btnEnble(); });
  }

  selectSubReasonDropdownData(item) {
    let rootCause = this.state.subReasonValues[item];
    let rootCauseId = this.state.subReasonIndexes[item];

    this.setState({
      selectedSubReason: rootCause,
      selectedSubReasonID: rootCauseId,
    }, () => { this.btnEnble(); });
  }

  btnEnble() {
    if (this.state.selectedReason != '' && this.state.selectedSubReason != '') {
      this.setState({ contBtnEnable: true });
    } else if (this.state.selectedRootCauseID != "DTV") {
      this.setState({ contBtnEnable: true });
    } else if (this.state.selectedReason != '' && this.state.subReasonValues.length == 0) {

    } else {
      this.setState({ contBtnEnable: false });
    }
  }

  onPressConfirm() {
    this.setState({ apiLoading: true }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {
          const data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            app_flow: this.props.WOM.work_order_app_flow,
            hybrid: this.state.selectedRootCauseID,
            reason_id: this.state.selectedReasonID,
            sub_reason_id: this.state.selectedReasonID,
            request_type: this.props.WOM.work_order_detail.wo_type,
            lte_order_id: this.props.WOM.work_order_detail.lte_order_id,
            lte_job_id: this.props.WOM.work_order_detail.lte_job_id,
            prod_list: this.props.WOM.work_order_detail.product_detail.product_list,
            latitude: loc.latitude,
            longitude: loc.longitude,
            job_status: this.props.WOM.work_order_detail.job_status,
            biz_unit: this.props.WOM.work_order_detail.biz_unit
          }
          Utill.apiRequestPost('updateHybridSelection', 'DtvHybrid', 'wom', data,
            this.updateHybridSelectionSuccessCB, this.updateHybridSelectionFailureCB, this.updateHybridSelectionExCB);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.message, '');
          })
        });
    });
  }

  updateHybridSelectionSuccessCB = (response) => {
    this.setState({ apiLoading: false, api_error: false, }, () => {
      this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
      this.dissmissNoNetworkModal()
    });
  }

  updateHybridSelectionFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, '');
    });
  }

  updateHybridSelectionExCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.onPressConfirm);
      } else {
        error = this.state.locals.api_error_message;
        this.displayCommonAlert('defaultAlert', '', error, '');
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.inProgressTitle
      }
    });
  }


  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1, alignSelf: 'center' }}>
        {this.showIndicator()}
        <View style={styles.topContainer} />
        <View style={styles.middleContainer}>
          <View style={styles.detailsContainer}>
            <DropDownComponent
              options={this.state.installationTypeValue}
              onSelect={(item) => { this.selectDropdownData(item) }}
              selectedOption={this.state.selectedRootCause}
              placeHolder={this.state.locals.dtvHybridDropdown}
            />
            {this.state.selectedRootCauseID == "DTV" ?
              <DropDownComponent
                options={this.state.reasonValues}
                onSelect={(item) => this.selectRejectReasonDropdownData(item)}
                selectedOption={this.state.selectedReason}
                placeHolder={this.state.locals.dtvRejRes}
              />
              : true}

            {this.state.subReasonValues.length > 0 && this.state.selectedRootCauseID == "DTV" ?
              <DropDownComponent
                options={this.state.subReasonValues}
                onSelect={(item) => this.selectSubReasonDropdownData(item)}
                selectedOption={this.state.selectedSubReason}
                placeHolder={this.state.locals.dtvRejSubRes}
              />
              : true}
          </View>

          <View style={styles.btncontainer}>
            <View>
              {this.state.contBtnEnable ?
                <TouchableOpacity style={{ height: 70, width: 140, paddingTop: 30 }} onPress={() => this.debounceonPressConfirm()}>
                  <Text style={styles.filterText}>{this.state.locals.continue}</Text>
                </TouchableOpacity> :
                <TouchableOpacity disabled={true}>
                  <Text style={styles.filterTextDisabled}>{this.state.locals.continue}</Text>
                </TouchableOpacity>}
            </View>
          </View>
        </View>

        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
        </DragComponent>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  const signature = state.mobile.signature;
  return { Language, WOM, signature };
};

const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1,
    marginTop: -5,
  },
  topContainer: {
    flex: 0.05
  }, middleContainer: {
    flex: 0.8
  }, bottomContainer: {
    flex: 0.15
  },
  detailsRow: {
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 8,
    // borderWidth: 1
  },
  labelCol: {
    width: (width / 2) - 10,
    paddingLeft: 15
  },
  labelColRight: {
    width: (width / 2) - 10,
    paddingLeft: 15
  },
  nameValText: {
    color: '#000000',
    fontSize: 16
  },
  ActIndicatorlabelText: {
    fontSize: 16
  },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1,
    paddingBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  image_c1: {
    width: 20,
    height: 20,
  },
  btncontainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  filterText: {
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 15,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {

    fontSize: 15,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  titleText: {
    fontSize: 18,
    color: '#000000',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  unDisBut: {
    marginRight: 20,
    color: '#000000',
    paddingTop: 10,
    fontSize: 16
  },
  containerCover: {
    paddingTop: 10
  },
  indiView: {
    // flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
    // zIndex:1000
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 8,
    marginTop: 8,
    marginLeft: 8,
    marginRight: 8,
  },
  elementLeft: {
    flex: 1,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    borderWidth: 0
  },
  elementTxt: {
    fontSize: 12
  },
  contentContainerTxt: {
    fontSize: 15,
    fontWeight: 'bold',
    color: Colors.btnActiveTxtColor,
  },
  contentContainerLbl: {
    fontSize: 15,
    color: Colors.btnActiveTxtColor,
  }, signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    // margin: 10,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
    //backgroundColor: '#808080'
  },
  signatureTxt: {
    color: Colors.black,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  }, signatureView: {
    marginLeft: 15,
    marginRight: 15,
    height: 100,
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  },
  helpImage: {
    height: 40,
    width: 40,
    resizeMode: 'stretch',
  },

});

export default connect(mapStateToProps, actions)(DTVHybridView);

