import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, BackHandler, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import * as actions from '../../../actions';
import ActIndicator from '../ActIndicator';
import { Header } from '../Header';
import strings from '../../../Language/Wom';
import DropDownComponent from '../components/DropdownComponent';
import SerialListComponent from '../components/SerialListComponent';
import DragComponent from '../../general/Drag';
import RadioButton from '../components/RadioButtonComponent';

const { width } = Dimensions.get('window')
class ReplaceInstalledItems extends Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            isLoading: false,
            selectedSerial: {},
            simSerial: {},
            simValidated: false,
            serialValidated: false,
            isContinueBtnDisabled: true,
            isTypeSelected: false,
            replacedSerial: '',
            apiLoading: false,
            isProvItem: false,
            installedSerials: props.prov_data,
            validatedList: [],
            selectedStatus: '',
            refreshNewSTBSerialData: false,
            refreshNewSIMSerialData: false,
            locals: {
                lblReplaceNotProvAlertMsg: strings.lblReplaceNotProvAlertMsg,
                lblReplaceProvComAlertMsg: strings.lblReplaceProvComAlertMsg,
                lblReplaceProvFailAlertMsg: strings.lblReplaceProvFailAlertMsg,
                network_error_message: strings.network_error_message,
                backMessage: strings.backMessage,
                api_error_message: strings.api_error_message,
                cancel: strings.cancel,
                error: strings.error,
                continue: strings.continue,
                dropDownPlaceHolder: strings.lblItemType,
                cardfull: strings.cardfull,
                cardless: strings.cardless,
                inProgressTitle: strings.inProgressTitle,
                serialLbl: strings.lblSerial,
                newLbl: strings.newlable,
                newLableToReplace: strings.newLabelToReplace
            }
        };
        this.navigatorOb = this.props.navigator;
        this.status = this.props.status;
        this.updateSerial;
        this.basic;
        this.sale_type = '';
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

        const filteredArray = this.state.installedSerials.map((item, index) => {
            return { ...item, index };
        });
        this.setState({ installedSerials: filteredArray });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick = () => {
        this.navigatorOb.pop();
        return true;
    }

    displayConfirmAlert() {
        let description;
        let isProvisionable;

        const status = this.props.status;
        if (this.state.selectedSerial.provisionable == 'Y') {
            isProvisionable = true;
        } else {
            isProvisionable = false;
        }

        if (status == 'PROVISIONING_COMPLETED') {
            description = isProvisionable ? this.state.locals.lblReplaceProvComAlertMsg : this.state.locals.lblReplaceNotProvAlertMsg;
        } else if (status == 'PROVISIONING_FAILED') {
            description = isProvisionable ? this.state.locals.lblReplaceProvFailAlertMsg : this.state.locals.lblReplaceNotProvAlertMsg;
        }
        this.navigatorOb.showModal({
            screen: 'DialogRetailerApp.modals.ReplaceConfirmAlert',
            passProps: {
                description
            }
        });
    }

    getOptions() {
        if (this.props.WOM.work_order_detail.app_flow == 'DTV_NEW_CARRY_CX') {
            const filteredArray = this.state.installedSerials.filter(item => {
                if (item.basic == 'N') {
                    return true;
                }
            });
            if (filteredArray.length > 0) {
                const newArray = filteredArray.map((item) => {
                    return item.type;
                });
                return newArray;
            }
        } else {
            const newArray = this.state.installedSerials.map((item) => {
                return item.type;
            });
            return newArray;
        }
    }

    getSerialData(selectedIndex) {
        let selectedSerial = {};
        let old_serials = [];
        let selectedStatus = '';
        let simSerial = {};
        // find item based on selected index
        let selectedValue = this.state.installedSerials.find((item, index) => {
            if (selectedIndex == index) {
                return true;
            }
        });

        // check whether selected item available
        if (selectedValue !== undefined) {
            // push selected item serial to old_serials array
            old_serials.push({ [selectedValue.serial]: selectedValue.bundle_code });
            //check if old_serials available in selected item and add to old_serials array 

            if (selectedValue.old_serials) {
                if (selectedValue.old_serials.length > 0 && Array.isArray(selectedValue.old_serials)) {
                    selectedValue.old_serials.map(item => {
                        for (let value of Object.keys(item)) {

                            if (value !== selectedValue.serial) {
                                old_serials.push(item);
                            }
                        }
                    });
                } else {
                    console.log('check old array ', selectedValue.old_serials);
                }
            }

            selectedSerial = {
                ...selectedValue,
                old_serials,
                action: 'replace'
            };
        }

        if (this.props.WOM.work_order_detail.ownership == 'DialogOwned') {

            if (selectedValue.type == 'STB') {
                console.log('selectedValue ', selectedValue)

                this.basic = selectedValue.basic ? selectedValue.basic : '';
                this.sale_type = selectedValue.sale_type ? selectedValue.sale_type : '';
                selectedStatus = 'Y';
                if (selectedValue.card_less) {
                    if (selectedValue.card_less == 'N') {

                        const oldSim = this.state.installedSerials.find(item => {
                            if (item.type == 'SIM') {

                                old_serials.push({ [item.serial]: item.bundle_code });

                                if (item.old_serials) {
                                    if (item.old_serials.length > 0 && Array.isArray(item.old_serials)) {
                                        item.old_serials.map(value => {
                                            for (let serialKey of Object.keys(value)) {
                                                if (serialKey !== item.serial) {
                                                    old_serials.push(value);
                                                }
                                            }
                                        });
                                    } else {
                                        console.log('check old sim array ', item.old_serials);
                                    }
                                }
                                return true;
                            }
                        });

                        selectedSerial.old_serials = old_serials;

                        if (oldSim !== undefined) {
                            simSerial = { ...oldSim, card_less: selectedValue.card_less }
                        } else {
                            simSerial = { name: 'SIM', type: 'SIM', card_less: selectedValue.card_less };
                        }

                    } else if (selectedValue.card_less == 'Y') {
                        simSerial = { name: 'SIM', type: 'SIM', card_less: selectedValue.card_less };
                    }
                }
            }
        }

        console.log('simSerial ', simSerial);

        this.setState({
            selectedSerial, isTypeSelected: true, selectedStatus, simSerial,
            refreshNewSIMSerialData: true, refreshNewSTBSerialData: true, serialValidated: false, simValidated: false
        }, () => this.displayConfirmAlert())
    }

    continueOnPress() {
        let validatedList = this.state.validatedList;
        if (this.state.selectedSerial.type == 'STB' && this.state.selectedStatus == 'Y') {
            const filteredArray = this.state.validatedList.filter(item => {
                if (item.type !== 'SIM') {
                    return item;
                }
            });

            validatedList = filteredArray;
        }

        this.props.successCb(validatedList, this.state.isProvItem);
        this.navigatorOb.pop({ animated: true, animationType: 'fade' });
    }

    isSerialsValidated = (isValidated, serialData) => {
        const replacedSerial = serialData;
        let isProvItem = false;
        let SIMAvailable = false;
        const newArray = this.state.installedSerials.map((item) => {

            if (item.index == replacedSerial.index) {

                isProvItem = true;
                if (replacedSerial.status) {
                    return { ...replacedSerial, status: 'TO_BE_PROVISIONED' };
                } else {
                    return replacedSerial;
                }
            } else {
                if (this.state.simValidated) {
                    if (item.type == 'SIM') {
                        return this.state.simSerial;
                    }
                }

                if (item.provisionable == 'Y') {
                    return { ...item, status: 'TO_BE_PROVISIONED' }
                } else {
                    return item;
                }
            }
        });

        if (this.state.simValidated) {
            if (this.state.selectedStatus == 'N') {
                for (let value of newArray) {
                    if (value.type == 'SIM') {
                        SIMAvailable = true;
                        break;
                    }
                }

                if (!SIMAvailable) {
                    const simSerial = { ...this.state.simSerial, status: 'TO_BE_PROVISIONED' }

                    newArray.push(simSerial);
                    SIMAvailable = false;
                }
            }
        }

        this.setState({
            validatedList: newArray, isProvItem,
            refreshNewSIMSerialData: false, refreshNewSTBSerialData: false,
            serialValidated: true, selectedSerial: replacedSerial
        });
    }

    simResponse = (isAllValidated, response) => {
        let replacedSerial = response;
        let isSIMAvailable = false;
        let newArray;
        if (this.state.isProvItem) {
            newArray = this.state.validatedList.map((item) => {
                if (item.type == replacedSerial.type) {
                    isSIMAvailable = true;

                    if (replacedSerial.status) {
                        return { ...replacedSerial, status: 'TO_BE_PROVISIONED' }
                    } else {
                        return replacedSerial;
                    }
                } else {
                    if (item.provisionable == 'Y') {
                        return { ...item, status: 'TO_BE_PROVISIONED' }
                    } else {
                        return item;
                    }
                }
            });
        } else {
            isSIMAvailable = true;

            if (replacedSerial.status) {
                replacedSerial = { ...replacedSerial, status: 'TO_BE_PROVISIONED' }
            }
        }

        const obj = { ...response, status: 'TO_BE_PROVISIONED' }

        isSIMAvailable == false ? newArray.push(obj) : true;

        this.setState({ validatedList: newArray, refreshNewSIMSerialData: false, simSerial: replacedSerial, simValidated: true });
    }

    onButtonSelect(value) {
        this.setState({ selectedStatus: value, serialValidated: false, simValidated: false, refreshNewSIMSerialData: true, refreshNewSTBSerialData: true });
    }

    renderNewManualLabel() {
        var newLabletoReplace = this.state.locals.newLableToReplace.toString();
        let newLable = newLabletoReplace.replace(/<item>/g, this.state.selectedSerial.name);
        return newLable;
    }

    renderNewSIMManualLabel() {
        var newLabletoReplace = this.state.locals.newLableToReplace.toString();
        let newLable = newLabletoReplace.replace(/<item>/g, this.state.simSerial.name);
        return newLable;
    }

    renderContinueButton() {
        let isContinueBtnDisabled = true;
        console.log('renderContinueButton1 ', this.state.selectedSerial.type == 'STB' && this.props.WOM.work_order_detail.ownership == 'DialogOwned');
        console.log('renderContinueButton2 ', this.state.selectedStatus == 'N');

        if (this.state.selectedSerial.type == 'STB' && this.props.WOM.work_order_detail.ownership == 'DialogOwned') {
            if (this.state.selectedStatus == 'N') {
                console.log('renderContinueButton3 stb ', this.state.serialValidated, ' sim ', this.state.simValidated);

                if (this.state.serialValidated && this.state.simValidated) {
                    console.log('renderContinueButton4 enable');

                    isContinueBtnDisabled = false;
                }
            } else if (this.state.selectedStatus == 'Y') {
                if (this.state.serialValidated) {
                    isContinueBtnDisabled = false;
                }
            }
        } else {
            if (this.state.serialValidated) {
                isContinueBtnDisabled = false;
            }
        }

        const { barcodeContinueBtn, barcodeContinueBtnTxt, continueBtnContainer } = styles;

        return (
            <View style={continueBtnContainer} >
                <TouchableOpacity
                    disabled={isContinueBtnDisabled}
                    style={[barcodeContinueBtn, isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
                    onPress={() => this.continueOnPress()}
                >
                    <Text style={[barcodeContinueBtnTxt, isContinueBtnDisabled ? { color: Colors.btnDeactiveTxtColor } : { color: Colors.btnActiveTxtColor }]} >{this.state.locals.continue}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderRadioOption() {
        const { RadioStyle, radioButtonLabelStyle, contentContainerSerial } = styles;

        if (this.state.selectedSerial.type == 'STB' && this.props.WOM.work_order_detail.ownership == 'DialogOwned') {
            return (
                <View style={contentContainerSerial}>
                    <View style={RadioStyle}>
                        <RadioButton
                            currentValue={this.state.selectedStatus}
                            value={'Y'}
                            outerCircleSize={20}
                            innerCircleColor={Colors.radioBtn.innerCircleColor}
                            outerCircleColor={Colors.radioBtn.outerCircleColor}
                            onPress={(value) => this.onButtonSelect(value)}>
                            <Text style={radioButtonLabelStyle}>{this.state.locals.cardless}</Text>
                        </RadioButton>
                    </View>
                    <View style={RadioStyle}>
                        <RadioButton
                            currentValue={this.state.selectedStatus}
                            value={'N'}
                            outerCircleSize={20}
                            innerCircleColor={Colors.radioBtn.innerCircleColor}
                            outerCircleColor={Colors.radioBtn.outerCircleColor}
                            onPress={(value) => this.onButtonSelect(value)}>
                            <Text style={radioButtonLabelStyle}>{this.state.locals.cardfull}</Text>
                        </RadioButton>
                    </View>
                </View>
            );
        } else return true;
    }

    renderSIMSerial() {
        const { serialListContainer } = styles;
        if (this.state.selectedStatus == 'N') {
            return (
                <View style={serialListContainer} >
                    <SerialListComponent
                        refreshData={this.state.refreshNewSIMSerialData}
                        apiParams={{
                            sub_area: this.props.WOM.work_order_detail.sub_area,
                            prod_list: this.props.WOM.work_order_detail.product_detail.product_list,
                            ownership: this.props.WOM.work_order_detail.ownership,
                            order_id: this.props.WOM.work_order_detail.order_id,
                            wo_type: this.props.WOM.work_order_detail.wo_type,
                            app_flow: this.props.WOM.work_order_detail.app_flow
                        }}
                        manualLable={this.renderNewSIMManualLabel()}
                        serialData={{ ...this.state.simSerial, basic: this.basic, card_less: this.state.selectedStatus, sale_type: this.sale_type }}
                        isAllSerialsValidated={this.simResponse}
                        navigatorOb={this.props.navigator}
                        actionName='validateSerialNew'
                        controllerName='serial'
                        moduleName='wom'
                    />
                </View>
            )
        } else return true;
    }

    render() {
        const { container, itemTypeContainer, contentContainer, helpImage, serialContainer, serialListContainer } = styles;
        return (
            <View style={container}>
                <Header
                    backButtonPressed={() => this.handleBackButtonClick()}
                    headerText={this.state.locals.inProgressTitle} />
                {this.state.isLoading ? <ActIndicator /> : true}
                <View style={contentContainer}>
                    <View style={itemTypeContainer} >
                        <DropDownComponent
                            placeHolder={this.state.locals.dropDownPlaceHolder}
                            onSelect={selectedIndex => this.getSerialData(selectedIndex)}
                            selectedOption={this.state.selectedSerial.type}
                            options={this.getOptions()}
                        />
                    </View>
                    {this.renderRadioOption()}
                    {this.state.isTypeSelected ?
                        <View style={serialContainer} >
                            <View style={serialListContainer} >
                                <SerialListComponent
                                    refreshData={this.state.refreshNewSTBSerialData}
                                    apiParams={{
                                        sub_area: this.props.WOM.work_order_detail.sub_area,
                                        prod_list: this.props.WOM.work_order_detail.product_detail.product_list,
                                        ownership: this.props.WOM.work_order_detail.ownership,
                                        order_id: this.props.WOM.work_order_detail.order_id,
                                        wo_type: this.props.WOM.work_order_detail.wo_type,
                                        app_flow: this.props.WOM.work_order_detail.app_flow
                                    }}
                                    manualLable={this.renderNewManualLabel()}
                                    serialData={this.state.selectedSerial}
                                    isAllSerialsValidated={this.isSerialsValidated}
                                    navigatorOb={this.props.navigator}
                                    actionName='validateSerialNew'
                                    controllerName='serial'
                                    moduleName='wom'
                                />
                            </View>
                            {this.renderSIMSerial()}
                            {this.renderContinueButton()}
                        </View>
                        :
                        true}
                </View>
                <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
                    <Image style={helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
                </DragComponent>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};

const styles = StyleSheet.create({
    container: {
        height: '100%',
        flexDirection: 'column',
        marginLeft: 0,
        marginRight: 0,
        backgroundColor: Colors.appBackgroundColor
    },
    serialContainer: {
        flex: 1,
    },
    contentContainer: {
        flex: 1
    },
    contentContainerSerial: {
        flexDirection: 'row',
        marginHorizontal: 32,
        marginBottom: 36
    },
    itemTypeContainer: {
        marginTop: 34,
        marginBottom: 36,
        alignItems: 'center',
    },
    helpImage: {
        height: 40,
        width: 40,
        resizeMode: 'stretch',
    },
    barcodeContinueBtn: {
        width: (width / 3),
        height: 45,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.btnActive
    },
    barcodeContinueBtnTxt: {
        fontSize: 16,
        color: '#000000',
        textAlign: 'center',
    },
    continueBtnContainer: {
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginRight: 24,
        marginTop: 36
    },
    serialListContainer: {
        height: 48,
        marginHorizontal: 32,
        marginTop: 15,
        marginBottom: 15
    },
    RadioStyle: {
        alignItems: 'flex-start',
        flex: 0.5
    },
    radioButtonLabelStyle: {
        marginLeft: 10
    },
});

export default connect(mapStateToProps, actions)(ReplaceInstalledItems);