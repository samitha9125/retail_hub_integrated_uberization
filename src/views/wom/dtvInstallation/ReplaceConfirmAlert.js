import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import strings from '../../../Language/Wom';
import Colors from '../../../config/colors';
import imageAtentionIcon from '../../../../images/common/alert.png';
class ReplaceConfirmAlert extends React.Component {
  constructor(props){
    super(props);
    strings.setLanguage(this.props.Language);
    this.state ={ 
      locals: {
        lblCancel: strings.cancel,
        lblReplace: strings.btnReplace
      }
    }
  }
  onCancelPress() {
    const navigatorOb = this.props.navigator;
    this.dismissModal();
    // navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  dismissModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({
      animated: true, animationType: 'slide-down'
    })
  }

  onReplacePress() {
    this.dismissModal();
  }

  render() {
    const { container, alertIcon, body, alertImageContainer, descText, bottomContainer, cancelText, replaceText } = styles;
    return (
      <View style={container}>
        <View style={body}>
          <View style={alertImageContainer} >
            <Image style={alertIcon} source={imageAtentionIcon} />
          </View>
          <Text style={descText}>{this.props.description}</Text>
          <View style={bottomContainer} >
            <TouchableOpacity
              onPress={() => this.onCancelPress()}
            >
              <Text style={cancelText} >{this.state.locals.lblCancel}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.onReplacePress()}
            >
              <Text style={replaceText} >{this.state.locals.lblReplace}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColor
  },
  alertImageContainer: {
    marginBottom: 24,
    alignItems: 'center',
  },
  alertIcon: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  body: {
    backgroundColor: 'white',
    height: 200,
    marginHorizontal: 40,
    paddingHorizontal: 13,
    paddingBottom: 17,
    paddingTop: 21,
  },
  descText: {
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left',
    marginHorizontal: 11,
    marginBottom: 34,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: 'black',
    marginRight: 17,
  },
  replaceText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: '#009688'
  }
};

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
}

export default connect(mapStateToProps)(ReplaceConfirmAlert);
