import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions,
  BackHandler,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from '../ActIndicator';
import strings from '../../../Language/Wom';
import { Header } from '../Header';
import DragComponent from '../../general/Drag';
import globalConfig from '../../../config/globalConfig';

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class DTVProvisionStatus extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      pay_mode: '',
      pay_amount: '',
      apiLoading: true,
      isReplaced: false,
      enable_reject: props.enable_reject,
      provisionTimeOut: props.prov_config ? parseInt(props.prov_config.timeout) * 1000 : 600000,
      provisionWait: props.prov_config ? parseInt(props.prov_config.time_interval) * 1000 : 120000,
      overallStatus: props.prov_data ? props.prov_data.status : this.props.WOM.work_order_provisioning_status,
      provisionedStatusData: props.prov_data ? props.prov_data.serial_list : this.props.WOM.work_order_installed_items,
      locals: {
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        error: strings.error,
        btnProvision: strings.btnProvision,
        lblProvInprogress: strings.provisionInprogress,
        lblProvComplete: strings.provisionComplete,
        lblProvFailed: strings.provisionFailed,
        lblSerialNo: strings.lblSerialNo,
        btnConfirm: strings.btnConfirm,
        btnReplace: strings.btnReplace,
        lblToBeProv: strings.lblToBeProv,
        lblreject: strings.reject,
        inProgressTitle: strings.inProgressTitle,
        lblAlert: strings.alertHeader,
        checkStatusMsg: strings.checkStatusMsg
      }
    };
    this.retry_attempt = 1;
    this.provisionAttempts = 1;

    this.navigatorOb = this.props.navigator;
    this.debounceonPressConfirm = _.debounce(() => this.onPressConfirm(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceonPressProvision = _.debounce(() => this.onPressProvision(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentWillMount() {
    this.getworkOrderProvisioningStatus();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.provisionStatusChecking();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.navigatorOb.resetTo({
      title: 'DIALOG SALES APP',
      screen: 'DialogRetailerApp.views.WomDetailView',
    });
  }

  provisionStatusChecking() {
    let timeCount = 0;
    this.timer = setInterval(() => {
      timeCount += this.state.provisionWait;
      if (this.state.overallStatus == 'PROVISIONING_INPROGRESS') {
        if (timeCount >= this.state.provisionTimeOut) {
          clearInterval(this.timer);
        } else {
          this.getworkOrderProvisioningStatus()
        }
      } else {
        clearInterval(this.timer);
      }
    }, this.state.provisionWait)
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else {
      return true;
    }
  }

  onPressProvision() {
    this.setState({ apiLoading: true }, () => {
      let retry_attempt = this.retry_attempt;

      const data = {
        app_flow: this.props.WOM.work_order_app_flow,
        cir: this.props.WOM.work_order_detail.cir,
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id,
        ccbs_order_id: this.props.WOM.work_order_detail.ccbs_order_id,
        request_type: this.props.WOM.work_order_detail.wo_type,
        ownership: this.props.WOM.work_order_detail.ownership,
        serial_list: this.state.provisionedStatusData,
        retry_attempt: retry_attempt,
        prov_action: 'replace',
        pay_amount: this.props.WOM.payment_details.pay_amount ? this.props.WOM.payment_details.pay_amount : '',
        pay_mode: this.props.WOM.payment_details.pay_mode ? this.props.WOM.payment_details.pay_mode : '',
        product_detail: this.props.WOM.work_order_detail.product_detail,
        biz_unit: this.props.WOM.work_order_detail.biz_unit,
        sub_area: this.props.WOM.work_order_detail.sub_area,
        contactNo: this.props.WOM.work_order_detail.contact,
        conn_type: this.props.WOM.work_order_detail.conn_type,
      };

      if (this.props.WOM.work_order_detail.job_type == 'REFIX' && this.props.WOM.work_order_refix_warranty_details) {
        data.warranty_list = this.props.WOM.work_order_refix_warranty_details;
      }

      retry_attempt += 1;
      this.retry_attempt = retry_attempt;
      Utill.apiRequestPost('ProvisioningInstallation', 'DtvProvision', 'wom',
        data, this.doProvisionSuccessCB, this.doProvisionFailureCB, this.doProvisionExCB);
    });

  }

  doProvisionSuccessCB = (response) => {
    this.checkProvisionStatus(response)
    // this.setState({
    //   apiLoading: false,
    //   overallStatus: response.data.data.prov_data.status,
    //   provisionTimeOut: response.data.data.prov_config ? parseInt(response.data.data.prov_config.timeout) * 1000 : 600000,
    //   provisionWait: response.data.data.prov_config ? parseInt(response.data.data.prov_config.time_interval) * 1000 : 120000,
    //   provisionedStatusData: response.data.data.prov_data.serial_list,
    //   isReplaced: false
    // }, () => {
    //   if (response.data.warning_msg == true) {
    //     this.navigatorOb.push({
    //       title: this.state.locals.inProgressTitle,
    //       screen: 'DialogRetailerApp.views.CommonAlertModel',
    //       passProps: {
    //         messageType: 'defaultAlert',
    //         messageHeader: this.state.locals.error,
    //         messageBody: response.data.info,
    //         messageFooter: '',
    //         onPressOK: () => {
    //           this.checkAppFlow(response.data.data.app_screen, response.data.data)
    //         }
    //       }
    //     });
    //   }
    // });
  }

  checkProvisionStatus(response) {
    if (response.data.data.prov_data) {
      if (response.data.data.prov_data.status) {
        this.changeStates(response);
      } else if (response.data.warning_msg == true) {
        this.displayWarningMsg(response);
      } else {
        this.setState({ apiLoading: false }, () => {
          this.checkAppFlow(response.data.data.app_screen, response.data.data)
        })
      }
    } else if (response.data.warning_msg == true) {
      this.displayWarningMsg(response);
    } else {
      this.checkAppFlow(response.data.data.app_screen, response.data.data)
    }
  }

  changeStates(response) {
    this.setState({
      apiLoading: false,
      overallStatus: response.data.data.prov_data.status,
      provisionTimeOut: response.data.data.prov_config ? parseInt(response.data.data.prov_config.timeout) * 1000 : 600000,
      provisionWait: response.data.data.prov_config ? parseInt(response.data.data.prov_config.time_interval) * 1000 : 120000,
      provisionedStatusData: response.data.data.prov_data.serial_list,
      isReplaced: false
    })
  }

  displayWarningMsg(response) {
    this.setState({ apiLoading: false, isReplaced: false },
      () => {
        this.navigatorOb.push({
          title: this.state.locals.inProgressTitle,
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.error,
            messageBody: response.data.info,
            messageFooter: '',
            onPressOK: () => {
              this.checkAppFlow(response.data.data.app_screen, response.data.data);
            }
          }
        });
      })
  }

  checkAppFlow(app_screen, data) {
    this.navigatorOb.push(Utill.appFlowManegement(app_screen, data, this.state.locals));
  }

  doProvisionFailureCB = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.retry == true) {
        //display alert with cancel and retry option
        this.displayRetryProvisionAlert('failureWithRetry', this.state.locals.error, response.data.error, '');
      } else {
        this.displayCommonAlert('provisionFailed', response.data.error, response.data.serials, '', '');
      }
    });
  }

  displayRetryProvisionAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressRetry: () => { this.debounceonPressProvision(); }
      },
      overrideBackPress: true
    });
  }

  displayAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      },
      overrideBackPress: true
    });
  }

  doProvisionExCb = (res) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  handleRefresh() {
    this.state.isReplaced ?
      this.displayCommonAlert('alertWithOK', this.state.locals.lblAlert, this.state.locals.checkStatusMsg, '')
      :
      this.getworkOrderProvisioningStatus();
  }

  getworkOrderProvisioningStatus() {
    this.setState({ apiLoading: true }, () => {
      let prov_failed;
      if (this.state.overallStatus == 'PROVISIONING_FAILED') {
        prov_failed = true;
      } else {
        prov_failed = false;
      }
      const data = {
        app_flow: this.props.WOM.work_order_app_flow,
        cir: this.props.WOM.work_order_detail.cir,
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id,
        request_type: this.props.WOM.work_order_detail.wo_type,
        biz_unit: this.props.WOM.work_order_detail.biz_unit,
        prov_failed: prov_failed,
        contactNo: this.props.WOM.work_order_detail.contact
      };
      Utill.apiRequestPost('GetProvisioningStatus', 'DtvProvision', 'wom',
        data, this.getProvisioningStatusSuccess, this.getProvisioningStatusFailed, this.getProvisioningStatusEx);
    });
  }

  getProvisioningStatusSuccess = (response) => {
    let responseArray = response.data.data;
    this.setState({
      apiLoading: false,
      overallStatus: responseArray.prov_data.status,
      provisionedStatusData: responseArray.prov_data.serial_list,
      pay_mode: responseArray.prov_data.pay_mode,
      pay_amount: responseArray.prov_data.pay_amount
    }, () => {
      this.props.workOrderGetProvisioningStatus(responseArray.prov_data.status);
      if (this.state.provisionWait != response.data.data.prov_config.time_interval && this.state.provisionTimeOut != response.data.data.prov_config.timeout) {
        this.setState({
          provisionWait: parseInt(response.data.data.prov_config.time_interval) * 1000,
          provisionTimeOut: parseInt(response.data.data.prov_config.timeout) * 1000,
        });
      }
    });
  }

  getProvisioningStatusFailed = (response) => {
    this.setState({ apiLoading: false, overallStatus: 'PROVISIONING_FAILED' },
      () => this.displayCommonAlert('error', this.state.locals.error, response.data.error ? response.data.error : 'Provisionning failed', ''));
  }

  getProvisioningStatusEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }

  onPressConfirm() {
    if (this.state.overallStatus == "PROVISIONING_COMPLETED") {
      this.confirmInstallation();
    } else {
      // this.displayCommonAlert('defaultAlert', 'Error!', 'Provisioning has failed!', '');
      this.displayCommonAlert('provisionFailed', response.data.error, response.data.serials, '', '');
    }
  }

  confirmInstallation() {
    console.log('-----------confirmInstallation--------------');
    let provAttempts = this.provisionAttempts;

    let bodyData;
    const data = {
      order_id: this.props.WOM.work_order_detail.order_id,
      job_id: this.props.WOM.work_order_detail.job_id,
      serial_list: this.state.provisionedStatusData,
      product_detail: this.props.WOM.work_order_detail.product_detail,
      biz_unit: this.props.WOM.work_order_detail.biz_unit,
      request_type: this.props.WOM.work_order_detail.wo_type,
      app_flow: this.props.WOM.work_order_app_flow,
      sub_area: this.props.WOM.work_order_detail.sub_area,
      ownership: this.props.WOM.work_order_detail.ownership,
      dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
      is_oaf: this.props.WOM.work_order_detail.is_oaf,
      conn_type: this.props.WOM.work_order_detail.conn_type,
      contact: this.props.WOM.work_order_detail.contact,
      retry_attempt: provAttempts,
      contactNo: this.props.WOM.work_order_detail.contact,
      biz_unit: this.props.WOM.work_order_detail.biz_unit,
      cir: this.props.WOM.work_order_detail.cir
    };

    let ezcash_pin;
    let pay_mode;

    if (this.state.pay_mode == 'ez_cash' && this.state.pay_amount > 0) {
      pay_mode = this.state.pay_mode;
      if (this.props.WOM.payment_details.ezcash_pin) {
        if (this.props.WOM.payment_details.ezcash_pin !== '') {
          ezcash_pin = this.props.WOM.payment_details.ezcash_pin;

          bodyData = {
            ...data,
            ezcash_pin: ezcash_pin,
            pay_mode: pay_mode
          };
          this.updateUserConfirmation(bodyData, provAttempts);
        } else {
          this.props.navigator.showModal({
            screen: 'DialogRetailerApp.views.EZCashPINMissingAlert',
            title: 'GeneralModalException',
            passProps: {
              ezCashPIN: (pin) => {
                ezcash_pin = pin;
                bodyData = {
                  ...data,
                  ezcash_pin: ezcash_pin,
                  pay_mode: pay_mode
                };
                this.updateUserConfirmation(bodyData, provAttempts);
              }
            }
          });
        }
      } else {
        this.props.navigator.showModal({
          screen: 'DialogRetailerApp.views.EZCashPINMissingAlert',
          title: 'GeneralModalException',
          passProps: {
            ezCashPIN: (pin) => {
              ezcash_pin = pin;
              bodyData = {
                ...data,
                ezcash_pin: ezcash_pin,
                pay_mode: pay_mode
              };
              this.updateUserConfirmation(bodyData, provAttempts);
            }
          }
        });
      }
    } else {
      bodyData = {
        ...data,
        ezcash_pin: '',
        pay_mode: ''
      };
      this.updateUserConfirmation(bodyData, provAttempts);
    }
  }

  updateUserConfirmation(bodyData, provAttempts) {
    provAttempts = provAttempts + 1;
    this.provisionAttempts = provAttempts;

    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('updateUserConfirm', 'DtvProvision', 'wom',
        bodyData, this.confirmSuccess, this.confirmFailed, this.confirmEx);
    });
  }

  confirmSuccess = (response) => {
    // this.setState({ apiLoading: false }, () => {
    if (response.data.warning_msg == true) {
      this.navigatorOb.push({
        title: this.state.locals.inProgressTitle,
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: this.state.locals.error,
          messageBody: response.data.info,
          messageFooter: '',
          onPressOK: () => {
            this.setState({ apiLoading: false }, () => {
              this.checkAppFlow(response.data.data.app_screen, response.data.data)
            });
          }
        }
      });
    } else {
      this.setState({ apiLoading: false }, () => {
        this.checkAppFlow(response.data.data.app_screen, response.data.data)
      });
    }
    // });
  }

  confirmFailed = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.retry == true) {
        // display alert with cancel and retry
        this.displayRetryUserConfirmAlert('failureWithRetry', this.state.locals.error, response.data.error, '');
      } else {
        // this.displayCommonAlert('defaultAlert', 'Error', response.data.error ? response.data.error : 'System error when updating user provision status.', '')
        this.displayCommonAlert('provisionFailed', response.data.error, response.data.serials, '', '');
      }
    });
  }

  displayRetryUserConfirmAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressRetry: () => { this.confirmInstallation(); }
      },
      overrideBackPress: true
    });
  }

  confirmEx = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(() => this.confirmInstallation());
      } else if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  onPressReplace() {
    const status = this.props.WOM.work_order_provisioning_status;
    this.navigatorOb.push({
      title: this.state.locals.inProgressTitle,
      screen: 'DialogRetailerApp.views.ReplaceInstalledItems',
      passProps: {
        status,
        prov_data: this.state.provisionedStatusData,
        successCb: (data, isReplaced) => {
          console.log('onPressReplace ', isReplaced, '@@@ ', data);
          this.formatReceivedSerials(data);
          this.setState({
            isReplaced: isReplaced,
            overallStatus: isReplaced ? 'TO_BE_PROVISIONED' : this.state.overallStatus,
            provisionedStatusData: data
          });
        }
      }
    });
  }

  formatReceivedSerials(validatedSerials) {
    let serialArray = [];
    let formattedSerials = [];
    serialArray = validatedSerials;
    for (let i = 0; i < serialArray.length; i++) {
      const productType = serialArray[i].type;
      const productSerial = serialArray[i].serial;
      const productSapCode = serialArray[i].bundle_code;
      const newObj = {
        'accessoryType': productType,
        'sapMaterialCode': productSapCode,
        'serialNo': productSerial,
      }
      formattedSerials.push(newObj);
    }
    this.props.workOrderSerialList(validatedSerials);
    this.props.workOrderGetInstalledItems(formattedSerials);
    this.props.workOrderdtvinstallation(validatedSerials);
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.props.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    this.navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  renderReplaceButton() {
    if (!(this.props.WOM.work_order_detail.wo_type == "SIM_CHANGE" || this.props.WOM.work_order_detail.wo_type == "STB_CHANGE" || this.props.WOM.work_order_detail.wo_type == "SIM_STB_CHANGE" || this.props.WOM.work_order_detail.wo_type == 'FeatureHD' || this.props.WOM.work_order_detail.wo_type == 'ACCESSORY_DELV' || this.props.WOM.work_order_detail.app_flow == 'DTV_NEW_CARRY_CX')) {
      if (this.state.overallStatus != 'PROVISIONING_INPROGRESS') {
        if (this.props.WOM.work_order_detail.app_flow == 'DTV_NEW_CARRY_CX') {
          const filteredArray = this.state.provisionedStatusData.filter(item => {
            if (item.basic == 'N') {
              return true;
            }
          });
          if (filteredArray.length > 0) {
            return (
              <TouchableOpacity style={{ paddingBottom: 50 }} onPress={() => this.onPressReplace()} >
                <Text style={styles.filterText}>{this.state.locals.btnReplace}</Text>
              </TouchableOpacity>
            )
          } else {
            return <Text style={styles.filterTextDisabled}>{this.state.locals.btnReplace}</Text>
          }
        } else {
          return (
            <TouchableOpacity style={{ paddingBottom: 50 }} onPress={() => this.onPressReplace()} >
              <Text style={styles.filterText}>{this.state.locals.btnReplace}</Text>
            </TouchableOpacity>
          )
        }
      } else {
        if (this.state.overallStatus != 'PROVISIONING_INPROGRESS') {
          return true;
        } else {
          return (
            <Text style={styles.filterTextDisabled}>{this.state.locals.btnReplace}</Text>
          )
        }
      }
    } else {
      if (this.state.enable_reject) {
        if (this.state.overallStatus == 'PROVISIONING_FAILED') {
          if (this.props.WOM.work_order_detail.wo_type == "SIM_CHANGE" || this.props.WOM.work_order_detail.wo_type == "STB_CHANGE" || this.props.WOM.work_order_detail.wo_type == "SIM_STB_CHANGE" || this.props.WOM.work_order_detail.wo_type == 'FeatureHD' || this.props.WOM.work_order_detail.wo_type == 'ACCESSORY_DELV' || this.props.WOM.work_order_detail.app_flow == 'DTV_NEW_CARRY_CX') {
            return (
              <TouchableOpacity style={{ paddingBottom: 50 }} onPress={() => this.onPressReject()} >
                <Text style={styles.filterText}>{this.state.locals.lblreject}</Text>
              </TouchableOpacity>
            )
          }
        }
      }
      return true
    }
  }

  onPressReject() {
    this.setState({ apiLoading: true }, () => {
      const data = {
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id,
        serial_list: this.state.provisionedStatusData,
        product_detail: this.props.WOM.work_order_detail.product_detail,
        biz_unit: this.props.WOM.work_order_detail.biz_unit,
        request_type: this.props.WOM.work_order_detail.wo_type,
        app_flow: this.props.WOM.work_order_app_flow,
        sub_area: this.props.WOM.work_order_detail.sub_area,
        ownership: this.props.WOM.work_order_detail.ownership,
        dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
        is_oaf: this.props.WOM.work_order_detail.is_oaf,
        conn_type: this.props.WOM.work_order_detail.conn_type,
        contact: this.props.WOM.work_order_detail.contact,
        contactNo: this.props.WOM.work_order_detail.contact,
        cir: this.props.WOM.work_order_detail.cir,
        job_status: this.props.WOM.work_order_detail.job_status
      };
      Utill.apiRequestPost('rejectProvisionedFailedWo', 'JobClosure', 'wom',
        data, this.getprovisionSuccess, this.getprovisionFailure, this.getprovisionEx);
    });
  }


  getprovisionSuccess = (response) => {
    this.setState({ apiLoading: false, api_error: false }, () => {
      if (response.data.data.app_screen) {
        this.checkAppFlow(response.data.data.app_screen, response.data.data)
      }
    });

    getprovisionFailure = (response) => {
      this.setState({ apiLoading: false }, () => {
        this.navigatorOb.push({
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.error,
            messageBody: response.data.error,
            messageFooter: "",
            overrideBackPress: true,
            onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
          }
        });
      });
    }

    getprovisionEx = (error) => {
      this.setState({ apiLoading: false }, () => {
        if (error == 'No_Network') {
          this.props.navigator.showSnackbar({ text: error });
        } if (error == 'Network Error') {
          error = this.state.locals.network_error_message;
          this.props.navigator.showSnackbar({ text: error });
        } else {
          error = this.state.locals.api_error_message;
          this.props.navigator.showSnackbar({ text: error });
        }
      });
    }
  }

  renderStatus = (status) => {
    switch (status) {
      case 'PROVISIONING_COMPLETED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtSuccess}>
              {this.state.locals.lblProvComplete}
            </Text>
          </View>
        );

      case 'PROVISIONING_INPROGRESS':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblProvInprogress}
            </Text>
          </View>
        );

      case 'PROVISIONING_FAILED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtFailed}>
              {this.state.locals.lblProvFailed}
            </Text>
          </View>
        );

      case 'TO_BE_PROVISIONED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblToBeProv}
            </Text>
          </View>
        );

      default:
        return (
          <View />
        );
    }
  }

  renderListItem = ({ item }) => {
    return (
      <View style={styles.cardView}>
        <View style={styles.cardTop}>
          <View style={styles.cardTopSection1}>
            <Text style={styles.txtTitle}>
              {item.name}
            </Text>
          </View>
          <View style={{ justifyContent: 'flex-end', paddingRight: 10 }}>
            {this.renderStatus(item.status)}
          </View>
        </View>

        <View style={styles.cardBottom}>
          <Text style={styles.txtContent}>
            {this.state.locals.lblSerialNo} : {item.serial}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View>
          <Header
            backButtonPressed={() => this.okHandler()}
            refreshPressed={() => this.handleRefresh()}
            displayRefresh={true}
            headerText={this.props.screenTitle} />
        </View>
        {this.showIndicator()}
        <View style={styles.topContainer} /> 
        <View style={styles.middleContainer}>
          <FlatList
            data={this.state.provisionedStatusData}
            keyExtractor={(x, i) => i}
            renderItem={this.renderListItem}
            scrollEnabled={true}
            extraData={this.state}
          />
        </View>

        <View style={styles.bottomContainer}>
          <View style={styles.direbutRow}>
            <View style={styles.dirImageView}>
            </View>
            <View style={styles.filterButView}>
              {this.renderReplaceButton()}
              <View>
                {this.state.overallStatus == 'PROVISIONING_COMPLETED' || this.state.overallStatus == 'TO_BE_PROVISIONED' ?
                  <TouchableOpacity onPress={() => this.state.isReplaced ? this.debounceonPressProvision() : this.debounceonPressConfirm()}>
                    <Text style={styles.filterText}>{this.state.isReplaced ? this.state.locals.btnProvision : this.state.locals.btnConfirm}</Text>
                  </TouchableOpacity> :
                  <Text style={styles.filterTextDisabled}>{this.state.locals.btnConfirm}</Text> }
              </View>
            </View>
          </View>
        </View>

        <DragComponent style={{ position: 'relative' }} navigation={this.props.navigator} screen={'DialogRetailerApp.views.DetailDisplayView'}>
          <Image style={styles.helpImage} source={require('../../../../images/icons/cfss/fab.png')} />
        </DragComponent>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  topContainer: {
    flex: 0.08,
    backgroundColor: Colors.appBackgroundColor
  },
  middleContainer: {
    flex: 0.8,
    flexDirection: 'column',
    backgroundColor: Colors.appBackgroundColor
  },
  bottomContainer: {
    flex: 0.15,
    backgroundColor: Colors.appBackgroundColor
  },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1,
    paddingBottom: 20
  },
  dirImageView: {
    justifyContent: 'flex-start',
    width: (width / 2) - 10,
    paddingTop: 10
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: Colors.btnDisable,
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  cardView: {
    flex: 1,
    backgroundColor: Colors.white,
    height: 100,
    margin: 5,
    marginVertical: 10,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
    width: Dimensions.get('window').width - 10,
  },
  cardTop: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  cardBottom: {
    flex: 0.7,
    flexDirection: 'row',
    marginLeft: 7,
    borderColor: Colors.lightGrey
  },
  cardTopSection1: {
    flex: 1,
    marginTop: 12,
    marginLeft: 2,
    flexDirection: "column",
  },
  cardTopSection3: {
    flex: 1.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  txtTitle: {
    fontSize: 18,
    marginLeft: 7,
    color: Colors.black,
  },
  txtContent: {
    fontSize: 16,
    color: Colors.black,
  },
  txtPending: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorDarkOrange,
  },
  txtSuccess: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorGreen,
  },
  txtFailed: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorRed,
  },
  helpImage: {
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  }
});

export default connect(mapStateToProps, actions)(DTVProvisionStatus);