import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  Dimensions,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import Communications from 'react-native-communications';
import _ from 'lodash';

import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import { Header } from './Header';
import strings from './../../Language/Wom';
import Color from '../../config/colors';
import { colors } from '../../config/stylescfss';
import globalConfig from '../../config/globalConfig';

const Utill = new Utills();
var { height, width } = Dimensions.get('window');
class WomDetailView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      detailArrRec: [],
      workORders: [],
      jobStatus: '',
      woType: '',
      visitCount: '',
      appFlow: '',
      jobHistory: [],
      apiLoading: false,
      api_error: false,
      isLoad: false,
      app_States: '',
      locals: {
        alertHeader: strings.alertHeader,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        googleMapsMessage: strings.googleMapsMessage,
        dispatchConfirm: strings.dispatchConfirm,
        pendingTitle: strings.pendingTitle,
        dispatchTitle: strings.dispatchTitle,
        inProgressTitle: strings.inProgressTitle,
        error: strings.error,
        closeCIR: strings.closeCIR,
        lblBtnOk: strings.btnOk,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };
    this.count = this.props.count;
    this.navigatorOb = this.props.navigator;
    this.debouncedoChangeWO = _.debounce((item, command) => this.doChangeWO(item, command), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceRejectWO = _.debounce(() => this.RejectWO(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceCheckAppflow = _.debounce(() => this.CheckAppflow(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debouncedoResolveWO = _.debounce((value) => this.doResolveWO(value), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debouncedoChangeDispatchWO = _.debounce((item, command) => this.doChangeDispatchWO(item, command), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceHandleBackButtonClick = _.debounce(() => this.handleBackButtonClick(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentWillMount() {
    var screenTitle = this.getTitle(this.props.WOM.work_order_detail.job_status);
    if (this.props.WOM.work_order_detail.wo_type == "TRB") {
      this.getPreviousVisit();
    }
    this.setState({
      detailArrRec: this.props.WOM.work_order_detail.summary,
      jobStatus: this.props.WOM.work_order_detail.job_status,
      screenTitle: screenTitle,
      woType: this.props.WOM.work_order_detail.wo_type
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.props.workOrderSignatureData('');
    this.props.workOrderRootCauseDataReset();
    this.props.setJobHistoryDetails({})
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Color.colorRed,
        textColor: Color.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Color.radioBtn,
        textColor: Color.white,
      })
    }
  }

  okHandler() {
    const count = parseInt(this.count);
    this.props.clearWOStates();
    if (count > 1) {
      let loadVal = 0;
      if (this.state.isLoad) {
        loadVal = 1;
      }

      this.navigatorOb.resetTo({
        title: "WORK ORDER",
        screen: 'DialogRetailerApp.views.PendingWOdersView',
        passProps: {
          woNameParam: this.props.woNameParam,
          woId: this.props.woId,
          load_filer_call: loadVal,
          woList: this.props.woList,
          searchEnabled: this.props.searchEnabled ? this.props.searchEnabled : false,
          initialWoList: this.props.initialWoList ? this.props.initialWoList : [],
          searchedText: this.props.searchedText ? this.props.searchedText : ''
        }
      });
    } else {
      this.navigatorOb.resetTo({
        title: "WORK ORDER",
        screen: 'DialogRetailerApp.views.WomLandingView'
      });
    }
  }

  getTitle(status) {
    let viewTiltle = this.state.locals.pendingTitle;
    if (status == "DISPATCHED") {
      viewTiltle = this.state.locals.dispatchTitle;
    } else if (status == "IN_PROGRESS") {
      viewTiltle = this.state.locals.inProgressTitle;
    }
    return viewTiltle;
  }

  getPreviousVisit = () => {
    const data = {
      order_id: this.props.WOM.work_order_detail.order_id,
      job_id: this.props.WOM.work_order_detail.job_id,
      conn_no: this.props.WOM.work_order_detail.conn_no,
      wo_type: this.props.WOM.work_order_detail.wo_type
    };
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('getJobVisitHistory', 'WorkOrders', 'wom', data,
        this.getPreviousVisitSuccessCb, this.getPreviousVisitFailureCb, this.exCb);
    });
  }

  getPreviousVisitSuccessCb = (response) => {
    if (response.data.success) {
      let trbCount = response.data.data.count + ' visits';
      let jobHistoryDetails = {
        visitCount: trbCount,
        jobHistory: response.data.data.history
      }
      this.props.setJobHistoryDetails(jobHistoryDetails)
      this.setState({
        apiLoading: false,
        visitCount: trbCount,
        jobHistory: response.data.data.history
      });
    }
  }

  getPreviousVisitFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => this.displayAlert('defaultAlert', 'Error', response.data.error ? response.data.error : '', ''));
  }

  toggleModalHideDispatch(item) {
    this.setState({ isModalVisibleDispatch: false });
  }

  doChangeWO(item, command, isLongPopup) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: strings.messageType,
        messageHeader: strings.headerTitle + item.order_id,
        messageBody: strings.dispatchConfirm,
        messageFooter: "",
        isLongPopup,
        btnFun: () => this.doChangeDispatchWO(item, command),
      },
      overrideBackPress: true,
    });
  }

  doChangeDispatchWO(item, command) {
    this.changeWOStatus(item, command);
  }

  changeWOStatus(item, command) {
    this.setState({ apiLoading: true }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {
          const data = {
            order_id: item.order_id,
            job_id: item.job_id,
            job_status: item.job_status,
            command: command,
            latitude: loc.latitude,
            longitude: loc.longitude
          };
          Utill.apiRequestPost('ChangeOrderStatus', 'WorkOrders', 'wom', data,
            this.changeWOStatusCb, this.changeWOStatusFailureCb, this.exCb);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', 'Error', response.message, '');
          });
        });
    });
  }

  changeWOStatusCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.props.setWOJobStatus(response.data.job_status);
      this.props.workOrderReload('yes');
      if (response.data.flag !== 'already_dispatched') {
        var screenTitle = this.getTitle(response.data.job_status);
        this.setState({ jobStatus: response.data.job_status, isLoad: true, screenTitle });
      } else {
        this.navigatorOb.push({
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: this.state.locals.alertHeader,
            messageBody: response.data.info,
            messageFooter: "",
            overrideBackPress: true,
            onPressOK: () => { this.navigatorOb.pop() }
          }
        });
      }
    });
  }
  changeWOStatusFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: this.state.locals.error,
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { this.navigatorOb.pop() }
        }
      });
    });
  }

  exCb = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.debounceCheckAppflow);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  RejectWO() {
    this.navigatorOb.push({
      title: strings.rejectitle,
      screen: 'DialogRetailerApp.views.RejectIndex'
    });
  }

  CheckAppflow = async () => {
    if (this.props.WOM.work_order_app_screen == '') {
      await this.props.workOrderAppFlow(this.props.WOM.work_order_detail.app_flow);
    }
    this.setState({
      apiLoading: true,
      app_States: this.props.WOM.work_order_detail.app_flow
    }, () => {
      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {
          const data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            app_flow: this.props.WOM.work_order_app_flow,
            cir: this.props.WOM.work_order_detail.cir,
            biz_unit: this.props.WOM.work_order_detail.biz_unit,
            request_type: this.props.WOM.work_order_detail.wo_type,
            dtv_acc_no: this.props.WOM.work_order_detail.conn_no,
            breached: this.props.WOM.work_order_detail.breached,
            job_type: this.props.WOM.work_order_detail.job_type,
            product_list: this.props.WOM.work_order_detail.product_detail.product_list,
            lte_order_id: this.props.WOM.work_order_detail.lte_order_id,
            lte_job_id: this.props.WOM.work_order_detail.lte_job_id,
            ownership: this.props.WOM.work_order_detail.ownership,
            conn_type: this.props.WOM.work_order_detail.conn_type,
            latitude: loc.latitude,
            longitude: loc.longitude,
            account_type: this.props.WOM.work_order_detail.account_type
          };
          Utill.apiRequestPost('GetAppWoInfo', 'AppFlow', 'wom', data,
            this.FlowSuccessCb, this.FlowFailureCb, this.exCb);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', 'Error', response.message, '');
          });
        });
    });
  }

  FlowSuccessCb = (response) => {
    this.setState({ apiLoading: false, api_error: false }, () => {
      this.props.workOrderAppScreen(response.data.data.app_screen);
      this.dissmissNoNetworkModal()
      this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
    });
  }

  FlowFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayAlert('defaultAlert', this.state.locals.error, response.data.error, '');
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => { }
      }
    });
  }

  doResolveWO = async (value) => {
    await this.props.workOrderAppFlow('DTV_TRB_RESOLVE');
    await this.props.workOrderAppScreen('DTV_TRB_RESOLVE');
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: 'alertNoHeader',
        messageHeader: this.state.locals.closeCIR,
        messageBody: '',
        messageFooter: '',
        btnFun: () => this.debounceCheckAppflow(),
        onPressOK: () => { this.navigatorOb.pop() }
      },
      overrideBackPress: true
    });
  }

  showHistory = () => {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.VisitHistory',
      passProps: {
        history: this.state.jobHistory
      }
    });
  }

  numberclicked = (value) => {
    Communications.phonecall(value, true);
  }

  displayAlert(messageType, messageHeader, messageBody, messageFooter, isLongPopup = false) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        isLongPopup,
        btnFun: () => { },
        onPressOK: () => { this.navigatorOb.pop() }
      },
      overrideBackPress: true
    });
  }

  contactItem = (item, index) => {
    return (
      <TouchableOpacity key={index} onPress={() => this.numberclicked(item)}>
        <Text style={{ color: '#0000EE', textDecorationLine: 'underline', fontSize: 16 }}>{item}</Text>
      </TouchableOpacity>
    );
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    this.navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Header
          backButtonPressed={() => this.debounceHandleBackButtonClick()}
          headerText={this.state.screenTitle} />
        {this.state.apiLoading ?
          <View style={styles.indiView}>
            <ActIndicator animating={true} />
          </View>
          :
          true}
        <View style={styles.middleContainer}>
          <ScrollView style={{ backgroundColor: Color.appBackgroundColor }}>
            <View style={styles.detailsContainer}>
              <FlatList
                disabled={true}
                data={this.state.detailArrRec}
                keyExtractor={(x, i) => i.toString()}
                renderItem={({ item, index }) => (
                  <View key={index}>
                    {item.value != "" || (item.fieldType === 'visit' && this.state.visitCount != "") ?
                      <View style={styles.detailsRow}>
                        <View style={styles.labelCol}>
                          <Text >{item.name}</Text>
                        </View>{item.name && item.value !== '' || item.fieldType === 'visit' ?
                          <View >
                            <Text > : </Text>
                          </View> : null}
                        <View style={styles.labelColRight}>
                          {item.fieldType === 'address' ?
                            <View><Text style={styles.nameValText}>{item.value[0]}</Text>
                              <Text style={styles.nameValText}>{item.value[1]}</Text>
                              <Text style={styles.nameValText}>{item.value[2]}</Text></View>
                            : [item.fieldType === 'contact' && this.state.woType === "TRB" && this.state.jobStatus === "DISPATCHED" ?
                              <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 0.5 }}>
                                  {item.value.map((value, index) => {
                                    return this.contactItem(value, index)
                                  })}
                                  <View style={{ flex: 0.5, position: 'absolute', left: 100, zIndex: -1 }}>
                                    <TouchableOpacity style={styles.resolveArea} onPress={() => this.debouncedoResolveWO()}>
                                      <Text style={styles.resolveBut}>{strings.btnResolve}</Text>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </View>
                              : [item.fieldType === 'contact' ?
                                <View>
                                  {item.value.map((value, index) => {
                                    return this.contactItem(value, index)
                                  })}
                                </View> :
                                [item.fieldType === 'visit' ?
                                  <TouchableOpacity onPress={() => this.showHistory()}>
                                    <Text style={{ color: '#0000EE', textDecorationLine: 'underline', fontSize: 16 }}>{this.state.visitCount}</Text>
                                  </TouchableOpacity>
                                  : <Text style={styles.nameValText}>{item.value}</Text>]]]}
                        </View>
                      </View>
                      : true}
                  </View>
                )}
                extraData={this.state}
              />
            </View>
          </ScrollView>
        </View>
        <View style={styles.bottomContainer}>
          {this.state.jobStatus === "INITIAL" ?
            <View style={styles.direbutRow}>
              <View style={styles.dirImageView}>
                <TouchableOpacity onPress={() => Utill.onMapsPress(this.props.WOM.work_order_detail, this.state.locals.googleMapsMessage)} >
                  <Image source={require('../../../images/wom/direction.png')} style={styles.image_c1} />
                </TouchableOpacity>
              </View>
              <View style={styles.filterButView}>
                <TouchableOpacity
                  onPress={() => this.debouncedoChangeWO(this.props.WOM.work_order_detail, 'DISPATCH')} >
                  <Text style={styles.filterText}>{strings.DISPATCH}</Text>
                </TouchableOpacity>
              </View>
            </View>
            : true}
          {this.state.jobStatus === "DISPATCHED" ?
            <View style={styles.direbutRow}>
              <View style={styles.dirImageView}>
                <TouchableOpacity onPress={() => Utill.onMapsPress(this.props.WOM.work_order_detail, this.state.locals.googleMapsMessage)} >
                  <Image source={require('../../../images/wom/direction.png')} style={styles.image_c1} />
                </TouchableOpacity>
              </View>
              <View style={styles.filterButView}>
                <View>
                  <TouchableOpacity
                    onPress={() => this.debouncedoChangeDispatchWO(this.props.WOM.work_order_detail, 'UNDISPATCH')} >
                    <Text style={styles.unDisBut}>{strings.UNDISPATCH}</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => this.debouncedoChangeDispatchWO(this.props.WOM.work_order_detail, 'START')} >
                    <Text style={styles.filterText}>{strings.START}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View> : true}
          {this.state.jobStatus === "IN_PROGRESS" ?
            <View style={styles.direbutRow}>
              <View style={styles.dirImageView}>
              </View>
              <View style={styles.filterButView}>
                <View>
                  <TouchableOpacity onPress={() => this.debounceRejectWO()} >
                    <Text style={styles.unDisBut}>{strings.reject}</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity onPress={() => this.debounceCheckAppflow()} >
                    <Text style={styles.proceedBtn}>{strings.proceed}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View> : true}
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1,
    backgroundColor: Color.appBackgroundColor
  },
  middleContainer: {
    flex: 1.2
  },
  bottomContainer: {
    flex: 0.15
  },
  detailsRow: {
    flexDirection: 'row',
    marginTop: 10
  },
  labelCol: {
    width: (width / 2) - 40,
    paddingLeft: 15
  },
  labelColRight: {
    width: (width / 2) + 30,
    paddingLeft: 15
  },
  nameValText: {
    color: '#000000',
    fontSize: 16
  },
  direbutRow: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 10,
    borderTopColor: '#eeeeee',
    borderTopWidth: 1,
    backgroundColor: colors.appBackgroundColor
  },
  dirImageView: {
    justifyContent: 'flex-start',
    marginLeft: 8,
    width: (width / 2) - 10,
    paddingTop: 6
  },
  filterButView: {
    width: (width / 2) - 10,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  image_c1: {
    width: 25,
    height: 25,
  },
  filterText: {
    width: 130,
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center',
    marginRight: 10
  },
  unDisBut: {
    marginRight: 20,
    color: '#000000',
    paddingTop: 10,
    fontSize: 16
  },
  resolveBut: {
    color: Color.colorGreen,
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    borderWidth: 1,
    borderRadius: 5,
  },
  proceedBtn: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center',
    marginRight: 10
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(WomDetailView);


