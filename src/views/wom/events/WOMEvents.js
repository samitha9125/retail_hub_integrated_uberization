import React from 'react';
import {
    StyleSheet,
    View,
    BackHandler,
    Alert,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView,
    DatePickerAndroid,
    Dimensions

} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
const Utill = new Utills();

class WOMEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        // console.log('propsDTV' + JSON.stringify(this.props.WOM.work_order_dtv_provisioning));
    }

    componentWillMount() {
    }

    getworkOrderProvisioningStatus() {
        const data = {

        };
        // Utill.apiRequestPost('getProvisioningStatus', 'dtvInstallation', 'cfss', data, this.getProvisioningStatusSuccess, this.getProvisioningStatusFailed, this.getProvisioningStatusEx);
        Utill.apiRequestPost('getProvisioningStatus', 'DtvProvision', 'cfss', data, this.getProvisioningStatusSuccess, this.getProvisioningStatusFailed, this.getProvisioningStatusEx);
    }

    getProvisioningStatusSuccess = (response) => {
        responseArray = response.data.data;
        this.props.workOrderGetProvisioningStatus(responseArray.GetProvisioningStatus.return.prov_status);
        this.setState({ flatListRefresh: true });
    }

    getProvisioningStatusFailed = (response) => {
        console.log('PROVSTATS' + JSON.stringify(response.data.data));
    }

    getProvisioningStatusEx = (response) => {
        console.log('PROVSTATS' + JSON.stringify(response.data.data));
    }

}

const mapStateToProps = (state) => {
    console.log('****** REDUX STATE :: Work Oder ===> MainPage ');
    console.log('****** REDUX STATE :: Work Oder => MainPage ');
    const Language = state.lang.current_lang;
    const WOM = state.wom;
    return { Language, WOM };
};
export default connect(mapStateToProps, actions)(WOMEvents);