import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import { Header } from '../../components/others';
import strings from './../../Language/Wom';
import Color from '../../config/colors';

var { width } = Dimensions.get('window');
class VisitHistoryDetail extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      jobHistoryItem:[]
    };
  }

  componentWillMount() {
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setState({
      jobHistoryItem: this.props.historyItem
    });   
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick xxx');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
    return true;
  }

  onClickOrder = (item) => {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      screen: 'DialogRetailerApp.views.VisitHistoryDetail',
      passProps: {
        historyItem: item
      }
    });
  }

  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
      <Header
        backButtonPressed={() => this.handleBackButtonClick()}
        headerText= {strings.wo_history_title}/>

        <View style={styles.wholeContain}>
          <ScrollView style={{backgroundColor:Color.appBackgroundColor}}>
            <View style={styles.detailsContainer}>
              
              <FlatList
                disabled={true}
                data={this.state.jobHistoryItem.summary}
                keyExtractor={(x, i) => i}
                renderItem={({ item }) => (
                  <View>

                    {console.log("item", item)}
                    {item.value != ""  ?
                      <View>
                        {item.type == "root"  ?
                        <View style={styles.detailsBlankRow}></View>
                        :<View></View>}
                        <View style={styles.detailsRow}>
                          <View style={styles.labelCol}>
                            <Text style={styles.labelRightText}>{item.key}</Text>
                          </View>
                          <View >
                            <Text style={styles.labelText}> : </Text>
                          </View>
                          <View style={styles.labelCol}>
                            <Text style={styles.labelText}>{item.value}</Text>
                          </View>
                          </View>
                      </View> 
                      :
                      <View></View>}
                  </View>
                )}
              /> 
                                    
            </View>
          </ScrollView>
        </View>
        <View style={styles.bottomContainer}>
        <View style={styles.footerButView}>
                <View>
                  <TouchableOpacity
                    onPress={() => this.handleBackButtonClick()}
                  >
                    <Text style={styles.okBut}>{strings.btnOk}</Text>
                  </TouchableOpacity>
                </View>
              </View> 
        </View>
      </View>
    );
  }

}


const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Work Oder ===> Visit HistoryDetail ');
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};


const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1,
    backgroundColor:Color.appBackgroundColor,
    paddingTop: 40
  },
  detailsBlankRow: {
    height:20,
  },
  detailsRow: {
    flexDirection: 'row',
    paddingTop: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  labelCol: {
    width: (width -30) / 2 ,
    paddingLeft: 20,
    paddingRight: 20
  },
  labelText:{
    fontSize: 16
  },
  labelRightText:{
    fontSize: 16,
    fontWeight: 'bold'
  },
  footerButView: {
    justifyContent: 'flex-end',
    flexDirection: 'row'    
  },
  okBut: {
    width: 130,
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center',
    marginRight: 10
  },
  wholeContain: {
    flex: 1.2
  },
  bottomContainer: {   
    flex: 0.15
  }
  
});

export default connect(mapStateToProps, actions)(VisitHistoryDetail);


