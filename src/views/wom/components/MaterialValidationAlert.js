import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation'
import * as actions from '../../../actions';
import strings from '../../../Language/Wom';
import Colors from '../../../config/colors';
import imageAtentionIcon from '../../../../images/common/error_msg.png';
class MaterialValidationAlert extends React.Component {
  constructor(props){
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        lblReject: strings.reject,
        lblRescan: strings.lblRescan,
        lblCancel: strings.cancel
      }
    }
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentWillUnmount(){
    Orientation.lockToPortrait()
  }

  onCancelPress() {
    const navigatorOb = this.props.navigator;

    if (this.props.isCancelPressed) {
      navigatorOb.dismissModal({
        animated: true, animationType: 'slide-down'
      })
    } else {
      this.dismissModal();
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    }
  }

  dismissModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({
      animated: true, animationType: 'slide-down'
    })
  }

  onRescanPress() {
    this.props.onRescanPressCb();
    this.dismissModal();
  }

  render() {
    const { container, alertIcon, body, alertImageContainer, descText, descText2, bottomContainer, cancelText, replaceText } = styles;
    return (
      <View style={container}>
        <View style={body}>
          <View style={alertImageContainer} >
            <Image style={alertIcon} source={imageAtentionIcon} />
          </View>
          <Text style={descText}>{this.props.description1}</Text>
          <Text style={descText2}>{this.props.description2}</Text>
          <View style={bottomContainer} >
            {this.props.scanCounter >= 3 ?
              <TouchableOpacity
                onPress={() => {
                  this.dismissModal();
                  const navigatorOb = this.props.navigator;
                  navigatorOb.push({
                    screen: 'DialogRetailerApp.views.RejectIndex'
                  });
                }}
              >
                <Text style={cancelText} >{this.state.locals.lblReject}</Text>
              </TouchableOpacity> :
              <TouchableOpacity onPress={() => this.onCancelPress()} >
                <Text style={cancelText} >{this.state.locals.lblCancel}</Text>
              </TouchableOpacity>}

            <TouchableOpacity onPress={() => this.onRescanPress()} >
              <Text style={replaceText} >{this.state.locals.lblRescan}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColor
  },
  alertImageContainer: {
    marginBottom: 24,
    alignItems: 'center',
  },
  alertIcon: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },
  body: {
    backgroundColor: 'white',
    height: 241,
    marginHorizontal: 40,
    paddingHorizontal: 17,
    paddingBottom: 17,
    paddingTop: 21,
  },
  descText: {
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left',
    fontWeight: 'bold',
    color: Colors.btnActiveTxtColor,
    marginHorizontal: 11,
    marginBottom: 16,
  },
  descText2: {
    fontSize: 16,
    lineHeight: 19,
    fontWeight: 'bold',
    color: Colors.btnDisableTxtColor,
    textAlign: 'left',
    marginHorizontal: 11,
    marginBottom: 34,
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  cancelText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: 'black',
    marginRight: 17,
  },
  replaceText: {
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'center',
    color: '#009688'
  }
};

export default connect(mapStateToProps, actions)(MaterialValidationAlert);