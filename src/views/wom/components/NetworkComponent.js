import { Component } from 'react';
import { NetInfo } from 'react-native';
export default class NetworkConnectivity extends Component {

  componentWillMount(){
    NetInfo.addEventListener('connectivity_check',this.handleConnectivityChange );
  }

  componentWillUnmount(){
    NetInfo.removeEventListener('connectivity_check',this.handleConnectivityChange);
  }
  
  handleConnectivityChange = isConnected => {
    if (__DEV__) console.log('---------------NetworkConnectivity-----------------');
    if (__DEV__) console.log('handleConnectivityChange :: value: ', isConnected);
  };

  async checkNetworkConnectivityFunc() { // check network connectivity status and return value
    return await NetInfo.isConnected.fetch()
      .then(isConnected => {
        if (__DEV__) { console.log('NetworkConnectivity checkNetworkConnectivity IsConnected ? ', isConnected); }
        return isConnected;
      })
      .catch(error => {
        if (__DEV__) { console.log('NetworkConnectivity checkNetworkConnectivity isConnect::catch ', error); }
        return error;
      });
  }

    
  async getConnnectionType() {
    return await NetInfo.getConnectionInfo()
      .then(type => {
        if (__DEV__) { console.log('NetworkConnectivity getConnectionType type :  ', type); }
        return type;
      })
      .catch(error => {
        if (__DEV__) { console.log('NetworkConnectivity getConnectionType error : ', error); }
        return error;
      });
  }
}
