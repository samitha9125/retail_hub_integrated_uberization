import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';

export default class ButtonComponent extends Component {
  render() {
    const { buttonStyle } = styles;
    const { backgroundColor, buttonText, onPress, isButtonDisabled } = this.props;
    return (
      <TouchableOpacity onPress={onPress} disabled={isButtonDisabled}>
        <Text style={[
          buttonStyle, { backgroundColor },
          isButtonDisabled ? { backgroundColor: '#999999' } : null
        ]}>
          {buttonText}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  buttonStyle: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    marginLeft: 8,
    marginRight: 8,
    backgroundColor: '#ffc400',
    color: '#000000',
    borderRadius: 5,
    textAlign: 'center'
  }
};