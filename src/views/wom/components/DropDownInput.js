import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions
} from 'react-native';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import { TextField } from 'react-native-material-textfield';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../config/colors';
class DropDownInput extends React.Component {
  render() {
    const {
      onSelect,
      dropDownTitle,
      selectedValue,
      dropDownData = [],
      defaultIndex,
      disabled,
      disableDropdown,
      isTextInput = false,
      onTextInputChange,
      modalRef,
      onBlur,
      onDropdownWillHide,
      error = '',
      title = ''
    } = this.props;

    let dropdownOptionStyle;
    let dropdownStyle;
    if (this.props.alignDropDown) {
      dropdownOptionStyle = [styles.insideDropdownStyle, { marginBottom: 24 }];
      dropDownData.length < 5 ? dropdownStyle = [styles.dropdownStyle, { marginTop: 24, height: 'auto' }] : dropdownStyle = [styles.dropdownStyle, { marginTop: 24 }];

    }else if(this.props.alignDropDownnormal){
      dropdownOptionStyle = [styles.insideDropdownStyle, { marginBottom: 0 }];
      dropDownData.length < 5 ? dropdownStyle = [styles.dropdownStyle, { marginTop: 0, height: 'auto' }] : dropdownStyle = [styles.dropdownStyle, { marginTop: 0 }];
    }
     else {
      dropDownData.length < 5 ? dropdownStyle = [styles.dropdownStyle, { height: 'auto' }] : dropdownStyle = styles.dropdownStyle
      dropdownOptionStyle = styles.insideDropdownStyle;
    }

    return (
      <View style={styles.innerContainer}>
        <View style={styles.dropDownViewContainer} key={defaultIndex + selectedValue.length + error.length}>
          <ModalDropdown
            ref={modalRef}
            options={dropDownData}
            disabled={disableDropdown || dropDownData.length < 1}
            defaultIndex={defaultIndex}
            onSelect={onSelect}
            dropdownStyle={dropdownStyle}
            dropdownTextStyle={styles.dropdownTextStyle}
            dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
            onDropdownWillHide={onDropdownWillHide}>
            <View style={dropdownOptionStyle}>
              <View style={styles.leftViewStyle}>
                <TextField
                  editable={isTextInput == true && !disabled}
                  label={dropDownTitle}
                  multiline={true}
                  numberOfLines={2}
                  inputContainerStyle={{}}
                  onChangeText={onTextInputChange}
                  style={styles.dropTextStyle}
                  value={selectedValue}
                  onBlur={onBlur}
                  error={error}
                  title={title}
                />
              </View>
              {isTextInput == true ?
                <View style={styles.rightViewStyle1} />
                :
                <View style={styles.rightViewStyle}>
                  {dropDownData.length < 1 ?
                    true
                    :
                    <Ionicons
                      name='md-arrow-dropdown'
                      size={24}
                      color={Colors.defaultIconColorBlack} />}
                </View>}
            </View>
          </ModalDropdown>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1
  },
  dropDownViewContainer: {
    flex: 1
  },
  dropdownStyle: {
    marginTop: 24,
    justifyContent: 'flex-end',
    width: Dimensions.get('window').width * 0.81
  },
  dropdownTextStyle: {
    color: Colors.black,
    marginLeft: 5,
    fontSize: 15
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  insideDropdownStyle: {
    flex: 0.4,
    flexDirection: 'row',
    height: '100%',
  },
  leftViewStyle: {
    flex: 9
  },
  dropTextStyle: {
    color: Colors.black,
    fontSize: 15,
    paddingRight: 15,
  },
  rightViewStyle: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 0,
    marginTop: 12,
    height: '100%',
    position: 'absolute',
    right: -4,
    marginRight: 15
  },
  rightViewStyle1: {
    flex: 0,
    alignItems: 'flex-end',
    paddingRight: 0,
    height: '100%'
  }
});

export default DropDownInput;
