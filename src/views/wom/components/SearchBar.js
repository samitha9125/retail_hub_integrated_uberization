import React from 'react';
import { TextInput, View, TouchableOpacity, Animated, Keyboard } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../../config/colors';

const INITIAL_TOP = -60;

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      top: new Animated.Value(
        INITIAL_TOP
      ),
      text: ''
    };
    this.navigator = this.props.navigator;
  }

  setSearchedValue = (text) => {
    this.setState({ text })
  }

  show = () => {
    const { animate = true, animationDuration = 0 } = this.props;

    if (animate) {
      Animated.timing(this.state.top, {
        toValue: 0,
        duration: animationDuration
      }).start();
    } else {
      this.setState({ top: new Animated.Value(0) });
    }
    this.textInput.focus();
  };

  hide = () => {
    this.setState({ text: '' });
    const { animate = true, animationDuration = 0 } = this.props;

    if (animate) {
      Animated.timing(this.state.top, {
        toValue: INITIAL_TOP,
        duration: animationDuration
      }).start();
    }
    Keyboard.dismiss();
  };

  _handleBlur = () => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur();
    }
  };

  render() {

    return (

      <Animated.View style={[styles.viewStyle, { top: this.state.top }]}>
        <View style={styles.searchBarContainer}>
          <View style={styles.searchBarIcon}>
            <MaterialIcons
              name={"search"}
              size={styles.inputFieldIconSize}
              color={Colors.grey}
            />
          </View>
          <TextInput
            ref={ref => (this.textInput = ref)}
            style={styles.textInputStyleClass}
            onChangeText={(text) => {
              this.setState({ text: text }, () => {
                this.props.onChangeText(text);
              });
            }}
            onSubmitEditing={Keyboard.dismiss}
            onBlur={this._handleBlur}
            value={this.state.text}
            underlineColorAndroid='transparent'
            placeholder={this.props.placeholder}
            keyboardType={'numeric'}
          />

          <TouchableOpacity style={styles.clearIcon} onPress={() => {
            this.props.onPressClear();
            this.hide();
          }}>
            <MaterialIcons
              name={"clear"}
              size={styles.inputFieldIconSize}
              color={Colors.grey}
            />
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  }
}

const styles = {
  inputFieldIconSize: 30,
  viewStyle: {
    borderWidth: 3,
    flexDirection: 'row',
    backgroundColor: Colors.navBarBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 9999
  },
  filterSearchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  filterButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  searchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  textStyle: {
    fontSize: 18,
    color: '#FFF',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  headerTitleView: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  backButtonView: {
    flex: 1,
    alignItems: 'center',
    marginLeft: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  rightButtonsView: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  textInputStyleClass: {
    flex: 9,
    textAlign: 'left',
  },
  searchBarContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    marginVertical: 10
  },
  searchBarIcon: {
    flex: 1,
    margin: 5
  },
  clearIcon: {
    flex: 1,
    margin: 5
  }
};

export { SearchBar };
