import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, Image } from 'react-native';

import strings from '../../../Language/Wom';
import * as actions from '../../../actions';
import barcodeIcon from '../../../../images/icons/barcode.png';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

const lowercase = (s) => {
    if (typeof s !== 'string') return ''
    else if (s.charAt(1) == s.charAt(1).toUpperCase() && s.charAt(2) == s.charAt(2).toUpperCase()) return s
    return s.charAt(0).toLowerCase() + s.slice(1)
}

const SerialItemComponent = (props) => {
    const { container, labelText, barcodeLarge, barcodeSmall,
        smallContainer, largeContainer, label1Text, line1, line2,
        label2Text, valueContainer } = itemStyles;
    const { label, value, onPress } = props;
    console.log('SerialItemComponent ', props);
    if (value) {
        return (
            <View style={largeContainer}>
                <Text style={label1Text}>{capitalize(label)} {props.locals.lblNo}</Text>
                <View style={valueContainer}>
                    <Text style={label2Text}>{value}</Text>
                    <TouchableOpacity onPress={onPress} >
                        <Image source={barcodeIcon} style={barcodeSmall} />
                    </TouchableOpacity>
                </View>
                <View style={line1} />
            </View>
        );
    } else {
        return (
            <View style={smallContainer}>
                <View style={container} >
                    <Text style={labelText} >{props.locals.lblScan} {label}</Text>
                    <TouchableOpacity onPress={onPress} >
                        <Image source={barcodeIcon} style={barcodeLarge} />
                    </TouchableOpacity>
                </View>
                <View style={line2} />
            </View>
        );
    }
};
class SerialListComponent extends React.Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            serialData: [],
            locals: {
                lblScan: strings.lblscan,
                lblNo: strings.lblNo,
                lblSerialLower: strings.lblSerialLower
            }
        };
        this.isArray = Array.isArray(this.props.serialData);
    }

    componentWillReceiveProps(newProps) {
        newProps.refreshData ? this.setState({ serialData: [] }) : true
    }
    componentWillReceiveProps(newProps) {
        newProps.refreshData ? this.setState({ serialData: [] }) : true
    }
    gotoScanner(item, index) {
        const navigatorOb = this.props.navigatorOb;
        navigatorOb.push({
            screen: 'DialogRetailerApp.views.WOMBarcodeScannerScreen',
            passProps: {
                isReadOnly: this.props.isReadOnly ? this.props.isReadOnly : null,
                scanCounter: this.props.scanCounter == 0 ? this.props.scanCounter : undefined,
                title: this.props.manualLable ? this.props.manualLable : lowercase(item.name) + ' serial',
                item: item,
                apiParams: this.props.apiParams,
                actionName: this.props.actionName,
                controllerName: this.props.controllerName,
                moduleName: this.props.moduleName,
                apiDisable: this.props.apiDisable,
                successCb: (data) => {
                    let itemData = { ...item, ...data, index };
                    this.updateSerialList(itemData);
                },
                failureCb: () => {
                    this.deleteSerialItem(index);
                }
            }
        });
    }

    deleteSerialItem(index) {
        let oldArray = this.state.serialData;
        let serialData = [];
        if (index) {
            if (this.isArray) {
                const filteredArray = oldArray.filter((item) => item.index !== index);
                serialData = filteredArray;
            }
        }

        this.setState({ serialData });
    }

    updateSerialList(item) {
        console.log('updateSerialList ', item);
        if (this.isArray) {
            console.log('updateSerialList isArray', this.isArray);

            let oldArray = this.state.serialData;
            let newArray;

            if (oldArray.length < 1) {
                newArray = [item]
            } else {
                let updated = false;
                newArray = oldArray.map(array => {

                    if (array.index == item.index) {
                        const newValue = item
                        updated = true;

                        return newValue;
                    } else {
                        return array;
                    }
                });

                if (updated == false) {
                    newArray.push(item);
                }
            }
            this.setState({ serialData: newArray }, () => { this.isAllSerialsValidated(); });
        } else {
            this.setState({ serialData: item }, () => this.props.isAllSerialsValidated(true, this.state.serialData))
        }
    }

    getValue(index) {
        let value = undefined;
        console.log('getValue ', index);

        if (this.isArray) {
            console.log('getValue isArray ', this.isArray)
            for (const i = 0; i < this.state.serialData.length; i++) {
                if (this.state.serialData[i].index == index) {
                    value = this.state.serialData[i].serial;
                }
            }
            return value;
        } else {
            value = this.state.serialData.serial;
            console.log('getValue2 ', value, this.state.serialData);

            return value;
        }
    }

    isAllSerialsValidated() {
        const newArrayLength = this.state.serialData.length;
        const oldArraylength = this.props.serialData.length;

        if (oldArraylength == newArrayLength) {
            this.props.isAllSerialsValidated(true, this.state.serialData);
        } else {
            this.props.isAllSerialsValidated(false, false);
        }
    }

    renderSerialList() {
        if (this.props.serialData) {
            return this.props.serialData.map((item, index) => {
                return (
                    <View key={index} style={styles.scannerView} >
                        <SerialItemComponent
                            locals={this.state.locals}
                            label={this.props.manualLable ? this.props.manualLable : lowercase(item.name) + ' ' + this.state.locals.lblSerialLower}
                            onPress={() => this.gotoScanner(item, index)}
                            value={this.getValue(index)} />
                    </View>
                );
            });
        } else return true;
    }

    renderSerialItem = () => {
        const serialData = this.props.serialData;
        const nan = isNaN(serialData.index);
        const index = nan ? 0 : serialData.index;
        if (serialData.type) {
            return (
                <View style={styles.serialItemContainer} >
                    <SerialItemComponent
                        locals={this.state.locals}
                        label={this.props.manualLable ? this.props.manualLable : lowercase(serialData.name) + ' ' + this.state.locals.lblSerialLower}
                        onPress={() => this.gotoScanner(serialData, index)}
                        value={this.getValue(serialData.type)} />
                </View>
            )
        } else return true;
    }

    render() {
        const { container } = styles;
        return (
            <View style={container} >
                {this.isArray ?
                    this.renderSerialList()
                    :
                    this.renderSerialItem()}
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
        paddingTop: 12
    },
    scannerView: {
        flex: 1
    },
    textcontainer: {
        flex: 0.85,
        paddingLeft: 10,
        justifyContent: 'center',
    },
    iconContainer: {
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    scanLable: {
        fontSize: 17,
    },
    iconStyle: {
        width: 35,
        height: 30,
        marginTop: 0,
        marginBottom: 0,
    },
    serialItemContainer: {
        flex: 1,
        marginBottom: 0
    }
};

const itemStyles = {
    smallContainer: {
        alignItems: 'stretch',
        height: 48,
        marginBottom: 30,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 0,
        alignItems: 'center'
    },
    labelText: {
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'left',
        color: 'rgba(0,0,0,0.38)',
        marginBottom: 9,
    },
    barcodeLarge: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    line1: {
        height: 2,
        backgroundColor: 'rgba(0,0,0,0.42)',
    },
    line2: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.42)',
        marginHorizontal: 0,
    },
    label1Text: {
        fontSize: 12,
        lineHeight: 14,
        textAlign: 'left',
        color: 'rgba(0,0,0,0.54)',
        marginBottom: 6,
    },
    label2Text: {
        fontSize: 16,
        lineHeight: 19,
        textAlign: 'left',
        color: 'rgba(0,0,0,0.87)',
    },
    barcodeSmall: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    largeContainer: {
        alignItems: 'stretch',
        height: 72,
        marginBottom: 30,
        marginHorizontal: 0,
        paddingBottom: 6,
        paddingTop: 17,
    },
    valueContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 9,
    }
}

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    return { Language };
}

export default connect(mapStateToProps, actions)(SerialListComponent);
