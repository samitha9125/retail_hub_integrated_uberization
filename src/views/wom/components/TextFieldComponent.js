
/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/

import React from 'react';
import { StyleSheet, View } from 'react-native';
import { TextField } from 'react-native-material-textfield';
export default class TextFieldComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
        this.refVal = null
    }

    componentWillReceiveProps(newProps) {
        this.setState({ value: newProps.value })
    }

    clearFieldValue(){
        this.refVal.clear()
    }

    render() {
        const {
            customStyle,
            label,
            onChangeText,
            editable,
            keyboardType,
            maxLength,
            secureTextEntry,
            isFocused,
            lineWidth,
            onSubmitEditing,
            onBlur
        } = this.props;

        return (
            <View style={[styles.container, customStyle]}>
                <TextField
                    title={this.props.title}
                    label={label}
                    value={this.state.value}
                    editable={editable}
                    ref={(refVal) => {
                        this.refVal = refVal
                        isFocused && refVal !== null ?
                            refVal.focus()
                            :
                            true;
                    }}
                    onSubmitEditing={onSubmitEditing}
                    onBlur={onBlur}
                    secureTextEntry={secureTextEntry}
                    onChangeText={onChangeText}
                    keyboardType={keyboardType}
                    maxLength={maxLength}
                    lineWidth={lineWidth}
                    {...this.props}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
