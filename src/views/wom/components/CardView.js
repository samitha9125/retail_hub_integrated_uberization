import React, { Component } from 'react';
import { View, Text, Platform, StyleSheet } from 'react-native';
import Colors from '../../../config/colors';
class CardComponent extends Component {
  render() {
    return (
      <View style={[styles.shadow, this.props.outerStyle]}>
        <View style={[styles.card, this.props.innerStyle, this.props.borderRadius ? { borderRadius: this.props.borderRadius } : {}]}>
          <View style={styles.headerView}>
            <View style={styles.titleView}>
              <Text style={styles.titleText}>{this.props.title}</Text>
            </View>
            <View style={styles.qtyView}>
              <Text style={styles.lable1}>{this.props.qtyLable}</Text>
            </View>
          </View>

          <View style={styles.container}>
            <View style={styles.titleView1}>
              <View style={styles.serialView}>
                <Text style={styles.lable}>{this.props.lable1}</Text>
                <Text style={styles.lable1}>{this.props.Item1}</Text>
              </View>
              <View style={styles.warnteyView}>
                <Text style={styles.lable2}>{this.props.lable2}</Text>
                <Text style={styles.lable1}>{this.props.Item2}</Text>
              </View>
            </View>
            <View style={styles.qtyView1} >
              <View style={styles.qtyViewbackground}>
              
                <View style={styles.qtytextView}><Text style={styles.lable1}>{this.props.qty}</Text></View>
              </View>
            </View>
          </View>
          <View style={styles.bottomViewbackground}>
            <View style={styles.bottomview}>
            
            </View>
            <View style={styles.chargeView}>
            <View style={styles.chargeTextView}><Text>{this.props.price}</Text></View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const shadow = Platform.select({
  android: {
    elevation: 3
  },
  ios: {
    shadowRadius: 2,
    shadowColor: 'rgba(0, 0, 0, 1.0)',
    shadowOpacity: 0.54,
    shadowOffset: { width: 0, height: 2 },
  },
});

const styles = StyleSheet.create({
  shadow: {
    borderRadius: 3,
    ...shadow
  },
  card: {
    backgroundColor: Colors.appBackgroundColor,
    borderRadius: 3,
    zIndex: 50
  },
  headerView: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 20
  },
  container: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 15
  },
  titleView: {
    flex: 3,

  },
  qtyView: {
    flex: 1,
    alignItems: 'center',
  },
  titleText: {
    color: Colors.colorBlack,
    fontWeight: 'bold',
    fontSize: 20
  },
  serialView: {
    borderWidth: 1,
    borderColor: 'transparent',
    flexDirection: 'row',
  },
  warnteyView: {
    borderWidth: 1,
    borderColor: 'transparent',
    flexDirection: 'row',
    marginTop: 10
  },
  lable1: {
    flex: 1,
    flexWrap: 'wrap',
    color: Colors.colorBlack
  },
  lable: {
    marginRight: 10,
    color: Colors.colorBlack
  },
  lable2: {
    marginRight: 10,
    color: Colors.colorBlack
  },
  titleView1: {
    flex: 3,

  },
  qtyView1: {
    flex: 1,
    alignItems: 'center',
  },
  qtyViewbackground: {
    backgroundColor: Colors.borderLineColorLightGray,
    width: 50,
    height: 50
  },
  qtytextView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 15

  },
  containerBottom: {
    flex: 0.2,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: Colors.borderLineColorLightGray,
  },
  bottomViewbackground: {
    marginTop: 10,
    backgroundColor: Colors.borderLineColorLightGray,
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  bottomview:{
    flex: 2,
    flexDirection: 'row',
    alignItems:'flex-start',
    justifyContent:'center',
    marginLeft: 10
  },
  chargeView:{
    flex: 1,
    alignItems:'flex-end',
    justifyContent:'center',
    marginRight:10
    
  },
  

});

export default CardComponent;
