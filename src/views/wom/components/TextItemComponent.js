import React from 'react';
import { View, Text } from 'react-native';

export default TextItemComponent = (props) => {
    const { container, line, label1Text, label2Text } = styles;
    const { label1, label2 } = props;
    return (
        <View style={container}>
            <Text style={label1Text}>{label1}</Text>
            <Text style={label2Text}>{label2}</Text>
            <View style={line} />
        </View>
    );
}

const styles = {
    container: {
        height: 72,
        marginHorizontal: 40,
        alignItems: 'stretch',
        paddingTop: 17,
        paddingBottom: 5,
    },
    line: {
        height: 1,
        backgroundColor: 'rgba(0,0,0,0.42)'
    },
    label1Text: {
        fontSize: 12,
        lineHeight: 14,
        textAlign: 'left',
        color: 'rgba(0,0,0,0.54)',
        marginBottom: 6,
    },
    label2Text: {
        fontSize: 14,
        lineHeight: 16,
        textAlign: 'left',
        color: 'rgba(0,0,0,0.87)',
        marginBottom: 9,
    }
}