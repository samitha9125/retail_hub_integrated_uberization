import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
  Alert,
  Keyboard,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation'
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import { Header } from '../Header';
import strings from '../../../Language/Wom';
import ActIndicator from '../../home/ActIndicator';
import Utills from '../../../utills/Utills';

const { width } = Dimensions.get('window');
const maskColWidth = (width - 300) / 2;
const VIBRATION_DURATION = 200;
const Utill = new Utills();
class BarcodeScanner extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      simBackMsg: strings.backMessage,
      isLoading: false,
      locals: {
        api_error_message: strings.api_error_message,
        network_error_message: strings.network_error_message,
        scannerBottomMessage: strings.scannerBottomMessage,
        scannerBottomMessage2: strings.scannerBottomMessage2,
        permission_to_use_camera: strings.permission_to_use_camera,
        permission_to_use_camera_message: strings.permission_to_use_camera_message,
        successfulMessage: strings.successfulMessage,
        failedMessage: strings.failedMessage,
        scanInvalid: strings.scanInvalid,
        lblOk: strings.btnOk,
        emptySerialWarning: strings.emptySerialWarning,
        lblSerialNo: strings.lblSerialNo,
        lblSystemError: strings.system_error,
        scanSerialCaps: strings.ScanSerialCaps,
        lblEnterSerial: strings.lblEnterSerial
      },
      uniqueValue: 0,
      barcodeValue: null,
      continue: strings.continue,
    };
    this.camera = null;
    this.navigatorOb = this.props.navigator;
    this.scanCounter = 0;
  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    Orientation.lockToPortrait();
  }

  onHandleBackButton = () => {
    this.goBack();
    return true;
  }

  goBack = () => {
    this.navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  forceRemount = () => {
    this.setState(({ uniqueValue }) => ({ uniqueValue: uniqueValue + 1 }));
  }

  onPressContinue() {
    Keyboard.dismiss();
    const serial = this.state.barcodeValue;

    if (serial && serial !== '') {
      if (this.props.isReadOnly) {
        this.navigatorOb.pop({
          animated: true, // does the pop have transition animation or does it happen immediately (optional)
          animationType: 'fade',
        });

        let serialData;
        

        if(this.props.item){
          const item = this.props.item;
          serialData = { ...item, serial }
        } else {
          serialData= serial;
        }
        this.props.successCb(serialData);
      } else {
        const { actionName, controllerName, moduleName, apiParams, item } = this.props;
        const serialData = { ...item, serial };
        let bodyData;
        if (apiParams) {
          bodyData = { ...apiParams, serial: serialData };
        } else {
          bodyData = { serial: serialData };
        }

        if (this.props.apiDisable && item.oldSerial.length > 0) {
          let showError = true;
          let oldItems = {};
          item.oldSerial.map((item, key) => {
            if (item.serial == serial) {
              showError = false;
              oldItems = item;
            }
          });
          if (!showError) {
            this.navigatorOb.showSnackbar({ text: this.state.locals.successfulMessage });
            this.timeOut = setTimeout(() => {
              this.navigatorOb.pop();
              this.props.successCb(oldItems);
            }, 2000);
          } else {
            if (this.props.scanCounter >= 0) {
              this.scanCounter++;
            }
            
            this.displayOldAlert(this.scanCounter);
          }

        } else {
          this.setState({ isLoading: true },
            () => Utill.apiRequestPost(actionName, controllerName, moduleName, bodyData,
              this.serialValidateSuccessCb, this.serialValidateFailureCb, this.serialValidateExCb));
        }
      }
    } else {
      Alert.alert('', this.state.locals.emptySerialWarning, [
        {
          text: this.state.locals.lblOk,
          onPress: () => { if (__DEV__) console.log('OK pressed'); }
        }
      ], { cancelable: true });
    }
  }

  serialValidateSuccessCb = (response) => {
    try {
      this.setState({ isLoading: false }, () => {
        this.navigatorOb.showSnackbar({ text: this.state.locals.successfulMessage });
        this.timeOut = setTimeout(() => {
          this.navigatorOb.pop();
          this.props.successCb(response.data.data ? response.data.data : response.data, this.state.barcodeValue);
        }, 1000);
      });
    } catch (error) {
      console.log('error ', error);
    }
  }

  serialValidateFailureCb = (response) => {
    try {
      if (this.props.scanCounter >= 0) {
        this.scanCounter++;
      }

      this.forceRemount();

      this.setState({ isLoading: false }, () => {
        this.displayFailureAlert(response.data, this.scanCounter);
        this.props.failureCb(this.props.item);
      });
    } catch (error) {
      this.displayFailureAlert({}, this.scanCounter);

      console.log('error', error);
    }
  }

  displayFailureAlert(data, scanCounter) {
    this.navigatorOb.showModal({
      screen: 'DialogRetailerApp.modals.MaterialValidationAlert',
      passProps: {
        scanCounter: scanCounter,
        description1: `${this.state.locals.lblSerialNo} : ${this.state.barcodeValue}`,
        description2: data.error ? data.error : data.info ? data.info : this.state.locals.lblSystemError,
        onRescanPressCb: () => {
          this.setState({ barcodeValue: '' })
          // this.onPressContinue();
        }
      }
    });
  }

  displayOldAlert(scanCounter) {
    this.navigatorOb.showModal({
      screen: 'DialogRetailerApp.modals.MaterialValidationAlert',
      passProps: {
        scanCounter: scanCounter,
        description1: `${this.state.locals.lblSerialNo} : ${this.state.barcodeValue}`,
        description2: this.state.locals.scanInvalid,
        onRescanPressCb: () => {
          this.setState({ barcodeValue: '' })
          // this.onPressContinue();
        }
      }
    });
  }

  serialValidateExCb = (error) => {
    try {
      this.setState({ isLoading: false }, () => {
        this.forceRemount();
        if (error == 'Network Error') {
          error = this.state.locals.network_error_message;
          this.navigatorOb.showSnackbar({ text: error });
        } else {
          error = this.state.locals.api_error_message;
          this.navigatorOb.showSnackbar({ text: error });
        }
        this.timeOut = setTimeout(() => {
          this.props.failureCb(this.props.item);
        }, 1000)
      });
    } catch (erro) {
      console.log('error', error);
    }
  }

  onBarCodeReadCb = (e) => {
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e.data.toString();

    this.setState({ barcodeValue: last8Digit });
    return;
  };

  onChangeText = (value) => {
    this.setState({ barcodeValue: value });
  };

  render() {
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.onHandleBackButton()}
          headerText={this.props.scannerHeader ? this.props.scannerHeader : this.state.locals.scanSerialCaps} />
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.cameraView}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => this.onBarCodeReadCb(e)}
        >
        </RNCamera>
        <View style={styles.maskOutter}>
          <View style={{ flex: 1 }}>
            <View style={[{ flex: 2.1 }, styles.maskRow, styles.maskFrame]}
            />
            <View style={[{ flex: 0.75 }, styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]}
              />
            </View>
            <View style={[{ flex: 2.26 }, styles.maskRow, styles.maskFrame]} />
          </View>
        </View>
        <View style={styles.overlayView}>
          <View style={styles.overlayContainer} >
            <Text style={styles.titleText}>Scan {this.props.title}</Text>
            <View>
              <View style={styles.descContainer}>
                <Text style={styles.descText}>{this.state.locals.scannerBottomMessage}</Text>
                <Text style={styles.descText}> {this.state.locals.scannerBottomMessage2}</Text>
              </View>

              <View style={styles.bottomContainer}>
                <TextInput
                  style={styles.barcodeTxtInput}
                  underlineColorAndroid='transparent'
                  keyboardType={'numeric'}
                  value={this.state.barcodeValue}
                  placeholder={this.state.locals.lblEnterSerial}
                  autoFocus={false}
                  returnKeyType={'done'}
                  selectionColor={'yellow'}
                  onChangeText={(value) => this.setState({ barcodeValue: value })} />
                <TouchableOpacity
                  style={styles.barcodeContinueBtn}
                  onPress={() => this.onPressContinue()}>
                  <Text style={styles.barcodeContinueBtnTxt}>
                    {this.state.continue}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {this.state.isLoading ?
          <ActIndicator isTextWhite /> : true}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  const sim_validation = state.sim.sim_validation;

  return { Language, simApiFail: state.sim.sim_api_fail, sim_number: state.mobile.sim_number, sim_validation };
};
const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  containerBarcodeScan: {
    flex: 1
  },
  cameraView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  maskOutter: {
    position: 'absolute',
    top: 55,
    left: 0,
    bottom: 0,
    right: 0
  },
  maskInner: {
    width: 312,
    backgroundColor: Colors.colorTransparent,
    borderColor: '#FFC400', //#FFFFC400
    borderWidth: 3,
  },
  maskFrame: {
    backgroundColor: Colors.colorBlack,
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
  barcodeTxtInput: {
    fontSize: Styles.otpModalFontSize,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'left',
    flex: 0.7
  },
  barcodeContinueBtn: {
    height: 36,
    width: 108,
    flex: 0.3,
    borderRadius: 2,
    justifyContent: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: Styles.thumbnailTxtFontSize,
    fontWeight: 'bold',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  overlayView: { position: 'absolute', top: 55, left: 0, right: 0, bottom: 0 },
  overlayContainer: { flex: 1, justifyContent: 'space-between' },
  titleText: { fontSize: 16, lineHeight: 19, color: Colors.appBackgroundColor, marginTop: 105, textAlign: 'center' },
  descContainer: { marginBottom: 79, alignItems: 'center' },
  descText: { fontSize: 16, lineHeight: 19, color: Colors.appBackgroundColor },
  bottomContainer: { height: 72, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 24, alignItems: 'center', backgroundColor: Colors.barcodeBottomBackColor },

});

export default connect(mapStateToProps, actions)(BarcodeScanner);