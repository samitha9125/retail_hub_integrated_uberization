
/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/

import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Colors from '../../../config/colors';
var { width } = Dimensions.get('window');
class DropDownComponent extends Component {
    render() {
        const { onSelect } = this.props;
        return (
            <View style={styles.container}>
                {this.props.selectedOption == "" ?
                    true
                    :
                    <Text style={{ marginLeft: 2 }}> {this.props.placeHolder} </Text>}
                <ModalDropdown
                    options={this.props.options}
                    style={styles.dropDownMain}
                    dropdownStyle={styles.dropDownStyle}
                    dropdownTextStyle={styles.dropDownTextStyle}
                    dropdownTextHighlightStyle={styles.dropDownTextHighlightStyle}
                    defaultIndex={0}
                    onSelect={onSelect}>
                    <View style={styles.dropDownSelectedItemView}>
                        <View style={styles.selectedItemTextContainer}>
                            {this.props.selectedOption == "" ?
                                <Text style={styles.sortTypeTextPlaceHolder}>
                                    {this.props.placeHolder}
                                </Text>
                                :
                                <Text style={styles.sortTypeText}>
                                    {this.props.selectedOption}
                                </Text>}
                        </View>
                        <View style={styles.dropDownIcon}>
                            <Ionicons name='md-arrow-dropdown' size={20} />
                        </View>
                    </View>
                </ModalDropdown>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        marginBottom: 20
    },
    dropDownMain: {
        marginTop: 5,
        width: width - 60,
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderLineColorLightGray
    },
    dropDownStyle: {
        flex: 1,
        width: Dimensions.get('window').width * 0.9,
        height: '12%'
    },
    dropDownSelectedItemView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    }, 
    selectedItemTextContainer: {
        width: width - 70,
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 30,
        backgroundColor: Colors.colorTransparent,
        borderBottomColor: Colors.borderLineColorLightGray
    }, 
    sortTypeText: {
        flex: 1,
        color: Colors.black,
        fontSize: 15,
        marginTop: 5,
        marginLeft: 8,
    }, 
    sortTypeTextPlaceHolder: {
        flex: 1,
        marginLeft: 10,
        color: Colors.btnDeactiveTxtColor,
        fontSize: 16,
        marginTop: 5,
    }, 
    dropDownTextStyle: {
        color: Colors.black,
        fontSize: 15,
    },
    dropDownIcon: {
        width: 20
    }
});

export default DropDownComponent;