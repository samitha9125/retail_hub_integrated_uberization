import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity, BackHandler, ScrollView, RefreshControl, Dimensions, NetInfo
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';

import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import * as actions from '../../actions';
import ActIndicator from './ActIndicator';
import InputSection from './InputSection';
import DropDownInput from './components/DropDownInput';
import { Header } from './Header';
import strings from '../../Language/Wom';
import globalConfig from '../../config/globalConfig';

const Utill = new Utills();
const { height } = Dimensions.get('window')
class RejectView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      PickerValueHolder: '',
      refresh: false,
      apiLoading: false,
      RejectReson: [],
      selectedReasonValue: [],
      selectedReasonItem: [],
      selectedSubReasonItem: [],
      selectedSubReason: [],
      selectedReasonID: '',
      rejectResonArray: [],
      selectedSubReasonId: '',
      rejectId: [],
      rejectCategory: [],
      rejectSubReason: [],
      rejectSubId: [],
      rejectSubReasonValues: [],
      selectedSubReasonArray: [],
      locals: {
        screenTitle: this.props.woNameParam,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        error: strings.error,
        in_Progress_title: strings.inProgressTitle,
        workorder_reject: strings.workorder_reject,
        reason: strings.reason,
        subreason: strings.subreason,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        woID: strings.WOID,
        lblError: strings.error
      }
    };
    this.navigatorOb = this.props.navigator;
    this.debounceRejectData = _.debounce(() => this.RejectData(), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
  }

  componentWillMount() {
    if (this.props.RejectResonprop) {
      this.setState({ RejectReson: this.props.rejectArray }, () => { this.formatReason(); });
    } else {
      this.LoadData();
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  LoadData = () => {
    this.setState({ apiLoading: true }, () => {
      const data = {
        biz_unit: this.props.WOM.work_order_detail.biz_unit,
        request_type: this.props.WOM.work_order_detail.wo_type,
        wo_type: this.props.WOM.work_order_detail.wo_type,
        job_status: this.props.WOM.work_order_detail.job_status,
        order_id: this.props.WOM.work_order_detail.order_id,
        job_id: this.props.WOM.work_order_detail.job_id
      };
      Utill.apiRequestPost('GetRejectInfo', 'WorkOrders', 'wom',
        data, this.getReasonSuccess, this.getReasonFailure, this.getReasonEx);
    });
  }

  getReasonSuccess = (response) => {
    this.setState({ apiLoading: false, RejectReson: response.data.data, }, () => {
      this.dissmissNoNetworkModal();
      this.formatReason();
    });
  }

  getReasonFailure = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayFailureAlert('defaultAlert', this.state.locals.error, response.data.error, '');
    });
  }

  getReasonEx = (error) => {
    this.setState({ apiLoading: false }, () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.LoadData);
      } else {
        error = this.state.locals.api_error_message;
        this.displayCommonAlert('defaultAlert', this.state.locals.error, api_error_message, '');
      }
    });
  }


  formatReason() {
    let rejectId = [];
    let rejectCategory = [];
    let rejectSubReason = [];

    this.state.RejectReson.map((item, key) => {
      rejectId.push(item.id);
      rejectCategory.push(item.category);
      rejectSubReason.push(item.reason);
    });
    this.setState({
      rejectId,
      rejectCategory,
      rejectSubReason
    });
  }

  displayFailureAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => {
          const navigatorOb = this.props.navigator;
          navigatorOb.pop({ animated: true, animationType: 'fade' });
        }
      }
    });
  }

  RejectData = () => {
    this.setState({ apiLoading: true }, () => {

      Utill.getWOMUserLocation(this.props.Language)
        .then((loc) => {
          const data = {
            order_id: this.props.WOM.work_order_detail.order_id,
            job_id: this.props.WOM.work_order_detail.job_id,
            job_status: this.props.WOM.work_order_detail.job_status,
            command: "REJECT",
            reason_id: this.state.selectedReasonID,
            sub_reason_id: this.state.selectedSubReasonId,
            latitude: loc.latitude,
            longitude: loc.longitude,
            app_flow: this.props.WOM.work_order_detail.app_flow,
            request_type: this.props.WOM.work_order_detail.wo_type,
            biz_unit: this.props.WOM.work_order_detail.biz_unit
          };

          Utill.apiRequestPost('ChangeOrderStatus', 'WorkOrders', 'wom', data, this.getRejectSuccess, this.getRejectFailure, this.getRejectEx);
        })
        .catch((response) => {
          this.setState({ apiLoading: false }, () => {
            this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.message, '');
          })
        });
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        overrideBackPress: true,
        onPressOK: () => { }
      }
    });
  }

  getRejectSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      if (response.data.data.app_screen) {
        this.navigatorOb.push({
          title: 'IN-PROGRESS WORK ORDER',
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'defaultAlert',
            messageHeader: 'Workorder id:' + this.props.WOM.work_order_detail.order_id,
            messageBody: response.data.info,
            messageFooter: '',
            onPressOK: () => {
              this.navigatorOb.push(Utill.appFlowManegement(response.data.data.app_screen, response.data.data, this.state.locals));
            }
          }
        });
      } else {
        this.navigatorOb.push({
          title: 'IN-PROGRESS WORK ORDER',
          screen: 'DialogRetailerApp.views.CommonAlertModel',
          passProps: {
            messageType: 'successWithOk',
            messageHeader: this.state.locals.woID + this.props.WOM.work_order_detail.order_id,
            messageBody: this.state.locals.workorder_reject,
            messageFooter: '',
            onPressOK: () => {
              this.navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.WomLandingView' });
            }
          }
        });
      }
    });
  }

  getRejectFailure = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayFailureAlert('defaultAlert', this.state.locals.error, response.data.error, '');
    });
  }

  getRejectEx = (error) => {
    this.setState({ apiLoading: false }, () => {
      let message;
      if (error == 'Network Error') {
        message = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: message })
      } else {
        message = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: message });
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.in_Progress_title
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  GetPickerSelectedItemValue = (item) => {
    let selectedReasonValue = this.state.rejectCategory[item];
    let selectedReasonID = this.state.rejectId[item];
    let selectedSubReasonArray = this.state.rejectSubReason[item];
    this.setState({ selectedReasonValue, selectedReasonID, selectedSubReasonArray, selectedSubReasonItem: '' }, () => {
      this.formatSubReason();
    });
  }

  formatSubReason() {
    let rejectSubId = [];
    let rejectSubReasonValues = [];

    this.state.selectedSubReasonArray.map((item, key) => {
      rejectSubId.push(item.sub_id);
      rejectSubReasonValues.push(item.sub_reason);
    });
    this.setState({ rejectSubId, rejectSubReasonValues });
  }

  subReasonSelected = (item) => {
    let selectedSubReasonId = this.state.rejectSubId[item];
    let selectedSubReasonItem = this.state.rejectSubReasonValues[item];
    this.setState({ selectedSubReasonId, selectedSubReasonItem });
  }

  onRefresh() {
    this.setState({ refresh: false }, () => {
      this.LoadData();
    });
  }

  render() {
    let screenContent;
    screenContent = (
      <ScrollView style={styles.background}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refresh}
            onRefresh={this.onRefresh.bind(this)}
          />
        }
      >
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <View style={styles.inputsection}>
              <InputSection
                key={2}
                label={strings.reasonLine + ' '+ strings.reasonLine2}
                colorCode
              />
            </View>
            <View style={{ marginHorizontal: 30, marginVertical: 10 }}>
              <DropDownInput
                alignDropDownnormal={true}
                dropDownTitle={this.state.locals.reason}
                hideRightIcon={true}
                selectedValue={this.state.selectedReasonValue}
                dropDownData={this.state.rejectCategory}
                onSelect={(item) => this.GetPickerSelectedItemValue(item)}
              />
            </View>

            <View style={{ marginHorizontal: 30 }}>
              <DropDownInput
                alignDropDownnormal={true}
                dropDownTitle={this.state.locals.subreason}
                hideRightIcon={true}
                selectedValue={this.state.selectedSubReasonItem}
                dropDownData={this.state.rejectSubReasonValues}
                onSelect={(item) => this.subReasonSelected(item)}
              />
            </View>
          </View>

          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.selectedSubReasonItem == '') ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.selectedSubReasonItem == '')}
              onPress={() => this.debounceRejectData()}>
              <Text style={styles.btnActive}>{strings.submit}</Text>
            </TouchableOpacity>
          </View>
        </View >

      </ScrollView>

    );

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.in_Progress_title} />
        {this.state.apiLoading ?
          <View style={styles.indiView}>
            <ActIndicator animating={true} />
          </View>
          :
          true}
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const WOM = state.wom;
  return { Language, WOM };
};

const styles = StyleSheet.create({

  background: {
    backgroundColor: Colors.appBackgroundColor,
    flex: 1
  },
  container: {
    height: '100%'
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer: {
    flex: 1.0,
    paddingRight: '1%',
    paddingLeft: '1%'
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  pickerStyle: {
    flex: 1,
    margin: 0,
    padding: 0,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    marginBottom: 0,
    marginTop: 30,
    borderColor: Colors.borderLineColorLightGray,
  },
  buttonContainer: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnActive,
  },

  buttonContainereDisable: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnDeactive,
  },
  viewButton: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: '15%',
    marginRight: 30
  },
  inputsection: {
    marginLeft: 30,
    marginRight: 10,
    marginBottom: 10,
    marginTop: '15%'
  }, btnActive: {
    color: Colors.btnActiveTxtColor,
    fontSize: 15,
    fontWeight: 'bold',
  }
});

export default connect(mapStateToProps, actions)(RejectView);
