import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import strings from '../../Language/Wom';
import { Header } from './Header';
import ActIndicator from './ActIndicator';

const Utill = new Utills();
const { height } = Dimensions.get('screen');
class EtendedSortingView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    this.state = {
      selectedScheduleDate: '',
      selectedWoType: '',
      selBizType: '',
      selectedBizObj: '',
      woData: '',
      apiLoading: false,
      disableDropdown: false,
      lobArrayShow: [],
      woArraShow: [],
      api_error: false,
      selectedLobValue: '',
      DBN: [],
      DBN_wo_type: [],
      DTV: [],
      DTV_wo_type: [],
      ENTERPRISE: [],
      ENTERPRISE_wo_type: [],
      GSM: [],
      GSM_wo_type: [],
      PONY: [],
      PONY_wo_type: [],
      SPREAD: [],
      SPREAD_wo_type: [],
      WOW: [],
      WOW_wo_type: [],
      schedule: [],
      biz: [],
      locals: {
        screenTitle: this.props.woNameParam,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        invalidForm: strings.invalidForm,
        error: strings.error,
        Schedule: strings.Schedule,
        BussinessUnit: strings.BussinessUnit,
        LOB: strings.LOB,
        Work_order_type: strings.Work_order_type,
        FILTER: strings.FILTER
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.LoadData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.pop();
    return true;
  }

  clearAllButtonHandler() {
    this.setState({ selBizType: '', selectedScheduleDate: '', selectedLobValue: '', selectedWoType: '', selectedWoName: '' });
  }

  LoadData = () => {
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('GetBizInfo', 'WorkOrders', 'wom', {}, this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
    });
  }

  LoadDataSuccessCb = (response) => {
    this.setState({
      woData: response.data.data,
      DBN: response.data.data[0].lob,
      DBN_wo_type: response.data.data[0].wo_type,
      DTV: response.data.data[1].lob,
      DTV_wo_type: response.data.data[1].wo_type,
      ENTERPRISE: response.data.data[2].lob,
      ENTERPRISE_wo_type: response.data.data[2].wo_type,
      GSM: response.data.data[3].lob,
      SM_wo_type: response.data.data[4].wo_type,
      PONY: response.data.data[4].lob,
      PONY_wo_type: response.data.data[4].wo_type,
      SPREAD: response.data.data[5].lob,
      SPREAD_wo_type: response.data.data[5].wo_type,
      WOW: response.data.data[6].lob,
      WOW_wo_type: response.data.data[6].wo_type,
      schedule: response.data.schedule,
      biz: response.data.biz,
      apiLoading: false,
      api_error: false,
    }, () => { this.dissmissNoNetworkModal() })
  }

  LoadDataFailureCb = (response) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: this.state.locals.error,
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : '',
          messageFooter: "",
        }
      });
    });
  }

  LoadDataExcb = (error) => {
    this.setState({ apiLoading: false, api_error: true }, () => {
      if (error == 'No_Network') {
        this.showNoNetworkModal(this.LoadData);
      } else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  woTypeSelected(idx, val) {
    const value = this.state.selectedBizObj.wo_type[idx].key;
    this.setState({ selectedWoType: value, selectedWoName: val, selectedWoTypeIndex: idx });
  }

  selectedLob(idx, value) {
    this.setState({ selectedLobValue: value });
  }

  scheduleSellected(idx, value) {
    this.setState({ selectedScheduleDate: value });
  }

  async selectedBizType(idx, value) {
    await this.setState({
      selBizType: value,
      selectedLobValue: '',
      selectedWoName: '',
      selectedBizObj: this.state.woData[idx],
      woTypeArray: this.state.woData[idx].wo_type.map((val) => { return val.value })
    }, async () => {
      if (this.state.selBizType === "DBN") {
        let lobArray = this.state.DBN;
        let woArray = this.state.DBN_wo_type;
        await this.setState({ lobArrayShow: lobArray, woArraShow: woArray, disableDropdown: false });
      } else if (this.state.selBizType === "DTV") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.DTV_wo_type;
        await this.setState({ woArraShow: woArray });
      } else if (this.state.selBizType === "PONY") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.PONY_wo_type;
        await this.setState({ woArraShow: woArray });
      } else if (this.state.selBizType === "GSM") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.GSM_wo_type;
        await this.setState({ woArraShow: woArray });
      } else if (this.state.selBizType === "SPREAD") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.SPREAD_wo_type;
        await this.setState({ woArraShow: woArray });
      } else if (this.state.selBizType === "ENTERPRISE") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.ENTERPRISE_wo_type;
        await this.setState({ woArraShow: woArray });
      } else if (this.state.selBizType === "WOW") {
        await this.setState({ disableDropdown: true });
        let woArray = this.state.WOW_wo_type;
        await this.setState({ woArraShow: woArray, disableDropdown: false });
      }
    });
  }

  naviToPendingOrders() {
    if (this.isValidForm()) {
      this.navigatorOb.pop();
      this.props.filterWOCb({
        biz_unit: this.state.selBizType,
        lob: this.state.selectedLobValue,
        wo_type: this.state.selectedWoType,
        schedule: this.state.selectedScheduleDate,
        woId: this.props.woId,
        load_filer_call: 2
      });
    } else {
      this.displayAlert('error', this.state.locals.error, this.state.locals.invalidForm);
    }
  }

  isValidForm() {
    if (this.state.selBizType == "" &&
      this.state.selectedLobValue == "" &&
      this.state.selectedScheduleDate == "" &&
      this.state.selectedWoType == "") {
      return false;
    }
    return true;
  }

  displayAlert(messageType, messageHeader, messageBody) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        biz_unit: this.state.selBizType,
        lob: this.state.selectedLobValue,
        wo_type: this.state.selectedWoType,
        schedule: this.state.selectedScheduleDate,
        woId: this.props.woId,
        woNameParam: this.props.woNameParam,
        load_filer_call: 2,
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        onPressOK: () => { this.navigatorOb.pop() }
      }
    });
  }

  /******************************************************************/
  render() {
    /**************Shedule start*************************************/
    const SheduleSelctionMenu = () => (
      <ModalDropdown
        options={this.state.schedule}
        onSelect={(idx, value) => this.scheduleSellected(idx, value)}
        style={styles.modalDropdownStyles}
        dropdownStyle={styles.dropdownStyle}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElemint}>
            <Text style={styles.dropdownDataElemintTxt}>{this.state.selectedScheduleDate}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20} />
          </View>
        </View>
      </ModalDropdown>
    );
    /**************Shedule end*************************************/

    /**************BisUnit start*************************************/
    const BizUnitSelctionMenu = () => (
      <ModalDropdown
        options={this.state.biz}
        onSelect={(idx, value) => this.selectedBizType(idx, value)}
        style={styles.modalDropdownStyles}
        dropdownStyle={styles.dropdownStyle}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElemint}>
            <Text style={styles.dropdownDataElemintTxt}>{this.state.selBizType}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20} />
          </View>
        </View>
      </ModalDropdown>
    );
    /**************BisUnit end*************************************/

    /**************Lob start*************************************/
    const LobSelctionMenu = () => (
      <ModalDropdown
        options={this.state.lobArrayShow}
        onSelect={(idx, value) => this.selectedLob(idx, value)}
        style={styles.modalDropdownStyles}
        disabled={this.state.disableDropdown}
        dropdownStyle={styles.dropdownStyle}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElemint}>
            <Text style={styles.dropdownDataElemintTxt}> {this.state.selectedLobValue}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20} />
          </View>
        </View>
      </ModalDropdown>
    );
    /**************Lob end*************************************/

    /**************Work Order type*************************************/
    const WorkOrdertypeSelctionMenu = () => (
      <ModalDropdown
        options={this.state.woTypeArray}
        onSelect={(idx, value) => this.woTypeSelected(idx, value)}
        style={styles.modalDropdownStyles}
        dropdownStyle={styles.dropdownStyle}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElemint}>
            <Text style={styles.dropdownDataElemintTxt}>{this.state.selectedWoName}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20} />
          </View>
        </View>
      </ModalDropdown>
    );
    /**************Work Order type*************************************/

    const ElementFooter = ({ okButtontext, onClickOk }) => (
      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={okButtontext == 'CLEAR ALL' ? styles.buttonClearContainer : styles.buttonContainer}
          onPress={onClickOk}
        >
          <Text style={styles.buttonTxt}>{okButtontext}</Text>
        </TouchableOpacity>
      </View>
    );

    return (
      <View style={styles.mainContainer}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.props.woNameParam} />
        {this.state.apiLoading ?
          <View style={styles.indiView}>
            <ActIndicator animating={true} />
          </View>
          : true}
        <View style={{ height: '100%' }} >
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            <View style={{ flex: 3, backgroundColor: Colors.appBackgroundColor }}>
              <View style={styles.filterSection}>
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.dropDownText}>{this.state.locals.Schedule}</Text>
                </View>
                <SheduleSelctionMenu
                />
              </View>
              {/* shedule */}

              {/* BizUnit */}
              <View style={styles.filterSection}>
                <View style={styles.dropDownTextContainer}>
                  <Text style={styles.dropDownText}>{this.state.locals.BussinessUnit}</Text>
                </View>
                <BizUnitSelctionMenu style={{ marginTop: -20 }}
                />
              </View>
              {/* BizUnit */}

              {/* Lob */}
              {this.state.selBizType === 'DBN' ?
                <View style={styles.filterSection}>

                  <View style={styles.dropDownTextContainer}>
                    <Text style={styles.dropDownText}>{this.state.locals.LOB}</Text>
                  </View>

                  <LobSelctionMenu style={{ marginTop: -20 }}
                  />
                </View> : null}
              {/* Lob */}

              {/* Work Order type */}
              {this.state.selBizType != '' ?
                <View style={styles.filterSection}>
                  <View style={styles.dropDownTextContainer}>
                    <Text style={styles.dropDownText}>{this.state.locals.Work_order_type}</Text>
                  </View>
                  <WorkOrdertypeSelctionMenu style={{ marginTop: -20 }}
                  />
                </View> : null}
              {/*  Work Order type */}
            </View>
          </KeyboardAwareScrollView >
          <View style={{ flex: 1, backgroundColor: Colors.appBackgroundColor }}>
            <View style={styles.footerSection}>
              <View style={{ flex: 0.8, flexDirection: 'row', justifyContent: 'flex-end' }}>
                <ElementFooter
                  okButtontext={"CLEAR ALL"}
                  onClickOk={() => this.clearAllButtonHandler()} />

                <ElementFooter
                  okButtontext={"FILTER"}
                  onClickOk={() => this.naviToPendingOrders()}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const pendingWorkOrders = state.delivery.pendingWorkOrders;
  const lang = state.lang.current_lang;
  return { pendingWorkOrders, lang };
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  container: {
    flex: 0.8,
    backgroundColor: Colors.appBackgroundColor,
    paddingBottom: 20
  },
  filterSection: {
    flex: 1,
    flexDirection: 'column'
  },
  footerSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  dropDownTextContainer: {
    paddingTop: 10
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 15,
  },
  //button styles
  bottomContainer: {
    height: 55,
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    padding: 5,
    marginRight: 0
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
  },
  buttonClearContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },
  modalDropdownStyles: {
    width: Dimensions.get('window').width
  },
  dropdownStyle: {
    width: Dimensions.get('window').width - 20,
    height: '20%',
    marginLeft: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderLineColorLightGray
  },
  dropdownDataElemint: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(EtendedSortingView);



