import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import TopItems from './TopItems';
import BottomItemsRoot from './BottomItemsRoot';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import * as actions from '../../actions';
import strings from '../../Language/MobileActivaton.js';
import screenTitles from '../../Language/Home.js';
import Orientation from 'react-native-orientation';
import ActIndicator from './ActIndicator';
import { Header } from '../../components/others';

const Utill = new Utills();

class MobileActMain extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      apiLoading: false,
      backMessage: strings.backMessage,
      pre_post: 'pre',
      MobActivate: screenTitles.MobActivate
    };
    this.handleBackButtonClick = this
      .handleBackButtonClick
      .bind(this);

    Orientation.lockToPortrait();
  }

  componentWillMount() {
    if (this.props.mobile_act_mobile_number !== '') {
      const num_list = (this.props.pre_post == 'pre')
        ? `${this.props.mobile_act_mobile_number}|P;`
        : `${this.props.mobile_act_mobile_number}|O;`;

      const releaseData = {
        flag: 'second',
        blocked_id_second: '',
        num_list,
        blocked_id_first: this.props.blocked_id_first,
        selected_number: ''
      };

      if (this.props.pre_post == 'pre') {
        Utill.apiRequestPost('knownRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail, this.releaseEx);
      } else {
        Utill.apiRequestPost('knownRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail, this.releaseEx);
      }

    } else {
      this
        .props
        .resetMobileActivationState();
      this
        .props
        .resetSimChangeState();
      this
        .props
        .getApiLoading(false);
      this
        .props
        .getUniqueTxIdMobileAct(Date.now());
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.showAlertMsg();
    return true;
  }

  goBackToHome = () => {
    console.log('xxx goBackToHome');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this.props;
    me.resetMobileActivationState();
  }

  showAlertMsg = () => {
    console.log('xxx showAlertMsg');
    Alert.alert('', this.state.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.goBackToHome()
      }
    ], { cancelable: true });
  }

  releaseNumberSuccess = (response) => {
    this
      .props
      .resetMobileActivationState();
    this
      .props
      .getApiLoading(false);
    this
      .props
      .getUniqueTxIdMobileAct(Date.now());
  }

  releaseNumberFail = (response) => {
    this
      .props
      .resetMobileActivationState();
    this
      .props
      .getApiLoading(false);
    this
      .props
      .getUniqueTxIdMobileAct(Date.now());
  }

  releaseEx = (error) => {
    this
      .props
      .resetMobileActivationState();
    this
      .props
      .getApiLoading(false);
    this
      .props
      .getUniqueTxIdMobileAct(Date.now());
  }

  handleClick() {
    console.log("===> Button Tapped \n ");
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    this.handleBackButtonClick();
  }

  render() {
    console.log('MobileActMain Printed');
    const { navigator } = this.props;
    return (
      <View style={styles.mainContainer}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleClick()}
          headerText={this.state.MobActivate}/>
        <KeyboardAwareScrollView
          style={[styles.keyboardAwareScrollViewContainer, styles.additionalStyle]}
          keyboardShouldPersistTaps="always">
          <TouchableWithoutFeedback
            onPress={Keyboard.dismiss}
            accessible={false}
            style={styles.container}>
            <View>
              <View style={styles.containerTop}>
                <TopItems navigator={navigator}/>
              </View>
              <View style={styles.containerBottom}>
                <BottomItemsRoot navigator={navigator} defaultEzNumber={this.props.defaultEzNumber}/>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>
        {/* <ActIndicator animating /> */}
        {this.props.mobileActLoading
          ? <ActIndicator animating/>
          : true}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const apiLoading = state.auth.api_loading;
  const mobile_act_mobile_number = state.mobile.mobile_number;
  const blocked_id_first = state.mobile.blocked_id_first;
  const Language = state.lang.current_lang;
  const pre_post = state.mobile.pre_post;
  const mobileActLoading = state.mobile.mobileActLoading || state.mobile.mActApiLoading ;

  console.log('PRE_POST STATE: ', pre_post);

  return {
    apiLoading,
    mobile_act_mobile_number,
    blocked_id_first,
    Language,
    pre_post,
    mobileActLoading
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },

  additionalStyle: {
    flex: 1
  },
  containerTop: {
    flex: 0.6
  },
  containerBottom: {
    flex: 4.4
  },
  header: {
    flex: 1
  },
  mainContainer: {
    flex: 1
  }
});

export default connect(mapStateToProps, actions)(MobileActMain);
