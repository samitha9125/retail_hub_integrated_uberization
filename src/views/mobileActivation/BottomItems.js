import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image,
  Keyboard
} from 'react-native';
import CheckBox from 'react-native-check-box';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import MaterialInput from './MaterialInput';
import InputSection from './InputSection';
import SectionContainer from './SectionContainer';
import IdRadioButtons from './IdRadioButtons';
import CaptureId from './CaptureId';
import CaptureThumbnails from './CaptureThumbnails';
import CaptureTwoThumbnails from './CaptureTwoThumbnails';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import  { Analytics, Screen }  from '../../utills/';
import  { Colors, Styles, Images, Constants }  from '../../config/';
import constants from '../../config/constants';
import strings from '../../Language/MobileActivaton';
import _ from 'lodash';

const Utill = new Utills();
const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

class BottomItems extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      phone: '',
      sectionClick1: true,
      sectionClick2: false,
      sectionClick3: false,
      sectionClick4: false,
      otpVerified: false,
      idNotClearChecked: false,
      addressDifferentChecked: false,
      captureContex: 'NIC',
      activateBtnTxtColor: Colors.btnDeactiveTxtColor,
      activateBtnColor: Colors.btnDeactive,
      promptVisible: false,
      otpCount: 1,
      imageIsCaptured: false,
      isLoading: false,
      loginBtnTap: false,
      nicValidate: strings.nicValidate,
      passValidate: strings.passValidate,
      productInfo: strings.productInfo,
      custInfo: strings.custoInfo,
      ButtonTxt: strings.buttonTxt,
      simNo: strings.simNo,
      scanSim: strings.scanSim,
      mobileNo: strings.mobileNo,
      firstRel: strings.firstRel,
      alternateContact: strings.alternateContact,
      BtnTxt: strings.buttonTxt,
      submit_valid_nic: strings.submit_valid_nic,
      submit_valid_passport: strings.submit_valid_passport,

      idLabelInputNic: strings.idLabelInputNic,
      idLabelInputPp: strings.idLabelInputPp,

      captureNicLabelTxt: strings.captureNicLabelTxt,
      captureDlLabelTxt: strings.captureDlLabelTxt,
      capturePpLabelTxt: strings.capturePpLabelTxt,
      captureForeignAddress: strings.captureForeignAddress,

      capturedNicLabelTxt: strings.capturedNicLabelTxt,
      capturedDlLabelTxt: strings.capturedDlLabelTxt,
      capturedPpLabelTxt: strings.capturedPpLabelTxt,
      capturedForeignAddress: strings.capturedForeignAddress,

      notClearNicLabelTxt: strings.notClearNicLabelTxt,
      notClearDlLabelTxt: strings.notClearDlLabelTxt,
      notClearPpLabelTxt: strings.notClearPpLabelTxt,

      captureCustomerPhotoSubTxt: strings.captureCustomerPhotoSubTxt,
      captureProofOfDeliverySubTxt: strings.captureProofOfDeliverySubTxt,
      capturedCustomerPhotoSubTxt: strings.capturedCustomerPhotoSubTxt,
      capturedProofOfDeliverySubTxt: strings.capturedProofOfDeliverySubTxt,

      pobNicLabelTxt: strings.pobNicLabelTxt,
      pobDlLabelTxt: strings.pobDlLabelTxt,
      pobPpLabelTxt: strings.pobPpLabelTxt,

      customerSignature: strings.customerSignature,
      reloadAmount:  strings.reloadAmount,

      activation_success: strings.activation_success,
      invalid_sim_serieal: strings.invalid_sim_serieal,
      customer_information_image_not_captured: strings.customer_information_image_not_captured,
      please_enter_nic: strings.please_enter_nic,
      passport_image_not_captured: strings.passport_image_not_captured,
      address_image_not_captured: strings.address_image_not_captured,
      pob_image_not_captured: strings.pob_image_not_captured,
      customer_image_not_captured: strings.customer_image_not_captured,
      invalid_altenate_contact_no: strings.invalid_altenate_contact_no,
      signature_not_captured: strings.signature_not_captured,
      start_ts: '',
      btnContinue: strings.continue,
      btnOk: strings.btnOk
    };

    this.debouncedFinalActivation = _.debounce(() => this.onPressActivate(), 150, DEBOUNCE_OPTIONS);
    this.navigator = this.props.navigator;
  }

  componentWillMount(){
    this.props.getUniqueTxIdMobileAct(Date.now());
    Orientation.lockToPortrait();
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillReceiveProps(nextProps) {
    console.log('xxxxxx componentWillReceiveProps', nextProps);
    const regex = /^[^abcdefghijklmnopqrstuwyzABCDEFGHIJKLMNOPQRSTUWYZ]+$/;

    if (nextProps.local_foreign == 'local' && ((((nextProps.id_number.length > 10 && (nextProps.id_number.substr(10, 1) == 'v' || nextProps.id_number.substr(10, 1) == 'V' || nextProps.id_number.substr(10, 1) == 'x' || nextProps.id_number.substr(10, 1) == 'X' || nextProps.id_number.substr(11, 1) == 'v' || nextProps.id_number.substr(11, 1) == 'V' || nextProps.id_number.substr(11, 1) == 'x' || nextProps.id_number.substr(11, 1) == 'X')) || (nextProps.id_number.length == 10 && (((nextProps.id_number.substr(2, 3) > 365 && nextProps.id_number.substr(2, 3) < 500) || (nextProps.id_number.substr(2, 3) > 865 && nextProps.id_number.substr(2, 3) < 1000)) && (nextProps.id_number.substr(9, 1) == 'v' || nextProps.id_number.substr(9, 1) == 'V' || nextProps.id_number.substr(9, 1) == 'x' || nextProps.id_number.substr(9, 1) == 'X')))) || (nextProps.id_number.length > 12 || (nextProps.id_number.length == 12 && ((nextProps.id_number.substr(4, 3) > 365 && nextProps.id_number.substr(4, 3) < 500) || ((nextProps.id_number.substr(10, 1) != 'v' || nextProps.id_number.substr(10, 1) != 'x') && (nextProps.id_number.substr(4, 3) > 865 && nextProps.id_number.substr(4, 3) < 1000)))))) || (!regex.test(nextProps.id_number) && nextProps.id_number.length != 0) || (nextProps.id_number.length == 12 && (nextProps.id_number.substr(0, 1) != 1 || nextProps.id_number.substr(0, 1) != 2))) && !nextProps.firstValidated) {
      this.showAlert(this.state.submit_valid_nic);
    }
    const reg = /^[a-zA-Z0-9]+$/;

    if (nextProps.local_foreign === 'foreign' && !reg.test(nextProps.id_number) && nextProps.id_number.length > 0) {
      this.showAlert(this.state.submit_valid_passport);
    }

    if (nextProps.information_id_type !== this.props.information_id_type) {
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      this.setState({
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
    }

    if (nextProps.local_foreign == 'foreign' && nextProps.id_number !== this.props.id_number) {
      nextProps.getApiLoading(false);
      nextProps.getSimNumberMobileAct('');
      nextProps.resetValidateSimNumber();
      nextProps.getMobileNumberMobileAct('');
      nextProps.getFirstReloadMobileAct('');
      nextProps.resetMobileActNicValidation();
      nextProps.resetMobileActModelPopped();
      nextProps.resetMobileActOTPValidation();
      nextProps.getSkipValidationMobileAct(false);
      nextProps.mobileActOtpPopped(false);
      nextProps.mobileActMConnectPopped(false);
      nextProps.getOtpStatusMobileAct(false);
      nextProps.getResetImagesMobileAct();
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      nextProps.local_foreign == 'local' ? nextProps.getInformationIdTypeMobileAct('NIC') : nextProps.getInformationIdTypeMobileAct('PP');
      this.setState({
        sectionClick2: false,
        otpCount: 1,
        isLoading: false,
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.nicApiFail) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.simApiFail) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      nextProps.getSimNumberMobileAct('');
    }

    if (!nextProps.firstValidated) {
      nextProps.getApiLoading(false);
      nextProps.getSimNumberMobileAct('');
      nextProps.resetValidateSimNumber();
      nextProps.getMobileNumberMobileAct('');
      nextProps.getFirstReloadMobileAct('');
      nextProps.resetMobileActNicValidation();
      nextProps.resetMobileActModelPopped();
      nextProps.resetMobileActOTPValidation();
      nextProps.getSkipValidationMobileAct(false);
      nextProps.mobileActOtpPopped(false);
      nextProps.mobileActMConnectPopped(false);
      nextProps.getOtpStatusMobileAct(false);
      nextProps.getResetImagesMobileAct();
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      nextProps.local_foreign == 'local' ? nextProps.getInformationIdTypeMobileAct('NIC') : nextProps.getInformationIdTypeMobileAct('PP');
      this.setState({
        sectionClick2: false,
        otpCount: 1,
        isLoading: false,
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.local_foreign === 'local' && nextProps.firstValidated && nextProps.nic_validation === null && nextProps.id_number !== this.props.id_number) {
      const data = {
        id_number: nextProps.id_number,
        id_type: nextProps.local_foreign === 'local'
          ? 'NIC'
          : 'PP'
      };
      Keyboard.dismiss();
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      let start_ts = Math.round((new Date()).getTime() / 1000);
      this.setState({ start_ts: start_ts });
      nextProps.mobileActNicValidation(data);
      nextProps.resetMobileActModelPopped();
    }

    if (nextProps.nic_validation !== null && nextProps.nic_validation.success === true && nextProps.firstValidated && !nextProps.skip_validation) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);

      //TODO : New Change
      if (nextProps.nic_validation.cxExistStatus === 'Y' 
                && nextProps.notificationNumberListArray
                && nextProps.nic_validation.documnetExistInDataScan === 'Y' 
                && !nextProps.model_popped) {
        this.setState({ sectionClick2: true });
        nextProps.mobileActModelPopped(true);
        this.showValidationModel();
      } else if (nextProps.nic_validation.cxExistStatus === 'N') {
        this.setState({ sectionClick2: true });
        nextProps.mobileActModelPopped(true);
      } else if (nextProps.nic_validation.cxExistStatus === 'Y'  
              && (!nextProps.notificationNumberListArray
              || nextProps.nic_validation.documnetExistInDataScan === 'N' )) {
        this.setState({ sectionClick2: true });
        nextProps.mobileActModelPopped(true);
      }
    }

    if (!nextProps.simValidated) {
      nextProps.resetValidateSimNumber();
    }

    if (nextProps.otp_status === 1) {
      this.setState({ otpVerified: true });
    }

    if (nextProps.sim_number == ''){
      if (nextProps.mobile_number){
        nextProps
          .getMobileNumberMobileAct('');
      }
    }

    if (nextProps.simValidated && nextProps.sim_validation == null && nextProps.firstValidated && nextProps.sim_number !== this.props.sim_number) {
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      const data = {
        sim: nextProps.sim_number
      };

      nextProps.validateSimNumber(data);
    }

    if (nextProps.sim_validation !== null) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.mobile_number == '') {
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }

    if (nextProps.local_foreign === 'foreign') {
      nextProps.kyc_pob_capture !== null ? nextProps.getDifferentAddressMobileAct(true) : true;
      nextProps.kyc_customer_capture !== null ? nextProps.getInformationNotClearMobileAct(true) : true;
    }

    if (nextProps.local_foreign === 'local') {
      nextProps.kyc_pob_capture !== null ? nextProps.getDifferentAddressMobileAct(true) : true;
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated) {
      this.setState({ sectionClick3: true });
    } else {
      this.setState({ sectionClick3: false });
    }

    if (nextProps.nic_validation !==undefined && nextProps.nic_validation !==null) {
      if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && nextProps.signatureCaptured) {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && (nextProps.otp_status == 1 || nextProps.otp_status == 3) && nextProps.nic_validation.documnetExistInDataScan === 'Y') {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && (nextProps.otp_status == 1 || nextProps.otp_status == 3) && nextProps.nic_validation.documnetExistInDataScan === 'N' && nextProps.signatureCaptured) {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else {
        this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
      }
    } else {
      this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
    }


    if (nextProps.firstValidated && nextProps.otp_popped && this.state.otpCount === 1) {
      this.setState({
        promptVisible: true,
        otpCount: 2
      });
    }
  }

  onPressNIC = () => {
    console.log('xxx onPressNIC');
    let capture_type_no;
    let titleName;
    if (this.props.information_id_type === 'DR') {
      capture_type_no = 4;
      titleName = 'Capture Driving License';
    } else if (this.props.information_id_type === 'NIC') {
      capture_type_no = 1;
      titleName = 'Capture NIC';
    } else {
      capture_type_no = 3;
      titleName = 'Capture Passport';
    }

    this
      .props
      .getCaptureIdTypeNoMobileAct(capture_type_no);
    this
      .props
      .navigator
      .push({
        title: titleName,
        screen: 'DialogRetailerApp.views.CameraScreen',
        passProps: {
          onReturnView: 'some text',
          captureType: this.props.information_id_type
        }
      });
  };

  onPressReCapture = (capNo = null) => {
    console.log('xxx onPressReCapture');
    let captureTypeNo;
    let titleName;
    if (this.props.information_id_type === 'DR') {
      captureTypeNo = 4;
      titleName = 'Capture Driving License';
    } else if (this.props.information_id_type === 'NIC') {
      titleName = 'Capture NIC';
      if (capNo !== null && capNo === 2) {
        captureTypeNo = 2;
      } else {
        captureTypeNo = 1;
      }
    } else {
      captureTypeNo = 3;
      titleName = 'Capture Passport';
    }

    this.props.getCaptureIdTypeNoMobileAct(captureTypeNo);
    this.props.navigator.push({
      title: titleName,
      screen: 'DialogRetailerApp.views.CameraReCaptureScreen',
    });
  };


  onPressReCaptureForeign = () => {
    console.log('xxx onPressReCaptureForeign');
    let captureTypeNo;
    let titleName;

    if (this.props.information_id_type === 'NIC') {
      captureTypeNo = 1;
      titleName = 'Capture NIC';
    } else {
      captureTypeNo = 3;
      titleName = 'Capture Passport';
    }

    this
      .props
      .getCaptureIdTypeNoMobileAct(captureTypeNo);
    this
      .props
      .navigator
      .push({
        title: titleName,
        screen: 'DialogRetailerApp.views.CameraReCaptureScreen',
      });
  };


  onPressPassport = () => {
    if (this.props.local_foreign === 'foreign'){
      this.props.resetMobileActNicValidation();
    }
    if (this.props.local_foreign === 'foreign' && this.props.firstValidated) {
      const data = {
        id_number: this.props.id_number,
        id_type: this.props.local_foreign === 'local'
          ? 'NIC'
          : 'PP'
      };
      let start_ts = Math.round((new Date()).getTime() / 1000);
      this.setState({ start_ts: start_ts });
      this.props.mobileActNicValidation(data);
      this.props.resetMobileActModelPopped();
    }
  }

  onPressSIM = () => {
    console.log('xxx onPressSIM');
    this
      .props
      .navigator
      .push({
        title: this.state.scanSim,
        screen: 'DialogRetailerApp.views.BarcodeScannerScreen',
        passProps: {
          onReturnView: 'this.onPressNIC'
        }
      });
  };

  onPressMobile = () => {
    console.log('xxxx onPressMobile');
    this.props.navigator.showLightBox({
      screen: 'DialogRetailerApp.models.NumberSelectionModel',
      passProps: {
        title: 'titleN',
        content: 'contentN',
        onClose: null
      },
      overrideBackPress: true,
      style: modelStyleNoSelection,
      adjustSoftInput: 'resize',
      tapBackgroundToDismiss: false
    });
  };

  onPressReload = () => {
    console.log('xxxx onPressReload');
    this.setState({ isLoading: true });
    const data = {};
    Utill.apiRequestPost('dialogFirstReloadOffers', 'gsmConnection', 'ccapp', data, this.showReloadOfferModal, this.onGetOffersFailure, this.onGetOffersException);
  };

  onGetOffersFailure = (response) => {
    this.setState({ isLoading: false });
    Utill.showAlert(response.data.error);
  }

  onGetOffersException = (error) => {
    this.setState({ isLoading: false });
    Utill.showAlert(error);
  }

  showReloadOfferModal = (response) => {
    console.log('xxxx showReloadOfferModal');
    this.setState({ isLoading: false });
    this.props.navigator.showLightBox({
      screen: 'DialogRetailerApp.models.OfferSelectionModel',
      passProps: {
        title: 'titleN',
        content: 'contentN',
        onClose: null,
        offerData: response.data.data,
        response: response
      },
      style: modelStyle,
      adjustSoftInput: 'resize'
    });
  };

  onPressSucessModel = () => {
    console.log('xxxx onPressSucessModel');
    this.props.navigator.showLightBox({
      screen: 'DialogRetailerApp.models.GeneralModelSuccessMobileAct',
      passProps: {
        title: 'titleN',
        content: 'contentN',
        onClose: null
      },
      style: modelStyle,
      adjustSoftInput: 'resize',
      tapBackgroundToDismiss: false
    });
  };

  getImageTypeList = () => {
    console.log('xxxx getImageTypeList');
    let imageTypeList = []; 
    if (this.props.information_id_type === 'NIC') {
      this.props.customer_information_image_nic_1 !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.NIC_FRONT) : true;
      this.props.customer_information_image_nic_2 !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.NIC_BACK) : true;
    } else if (this.props.information_id_type === 'DR') {
      this.props.customer_information_image !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.DRIVING_LICENCE) : true;     
    } else {
      this.props.customer_information_image !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.PASSPORT) : true;
    }

    this.props.kyc_customer_capture !== null ? 
      imageTypeList.push(constants.imageCaptureTypes.CUSTOMER_IMAGE) : true;
    this.props.kyc_pob_capture !== null ? 
      imageTypeList.push(constants.imageCaptureTypes.PROOF_OF_BILLING) : true;
    this.props.signature !== null ? 
      imageTypeList.push(constants.imageCaptureTypes.SIGNATURE) : true;
    
    console.log('xxxx getImageTypeList', imageTypeList);
    return imageTypeList;
  }

  onPressActivate = () => {

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated && !this.props.fourthValidated) {
      this.showAlert(this.state.customer_information_image_not_captured);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated && !this.props.fourthValidated && this.props.nic_validation.documnetExistInDataScan === 'N') {
      this.showAlert(this.state.customer_information_image_not_captured);
      return;
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'local' && this.props.information_id_type === 'NIC' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && (this.props.customer_information_image_nic_1 == null || this.props.customer_information_image_nic_2 == null)) {
      this.showAlert(this.state.please_enter_nic);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'local' && this.props.information_id_type === 'NIC' && this.props.nic_validation.documnetExistInDataScan === 'N' && (this.props.customer_information_image_nic_1 == null || this.props.customer_information_image_nic_2 == null)) {
      this.showAlert(this.state.please_enter_nic);
      return;
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y')) {
      if (this.props.customer_information_image == null) {
        this.showAlert(this.state.passport_image_not_captured);
        return;
      } else if (this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.address_image_not_captured);
        return;
      }
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && this.props.nic_validation.documnetExistInDataScan === 'Y') {
      if (this.props.customer_information_image == null) {
        this.showAlert(this.state.passport_image_not_captured);
        return;
      }
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'foreign' && this.props.nic_validation.documnetExistInDataScan === 'N') {
      if (this.props.customer_information_image == null) {
        this.showAlert(this.state.passport_image_not_captured);
        return;
      } else if (this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.address_image_not_captured);
        return;
      }
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && this.props.nic_validation.cxExistStatus === 'Y' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.address_image_not_captured);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'foreign' && this.props.nic_validation.cxExistStatus === 'Y' && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.address_image_not_captured);
      return;
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.information_id_type === 'PP' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.pob_image_not_captured);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.information_id_type === 'PP' && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.pob_image_not_captured);
      return;
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.information_not_clear && this.props.kyc_customer_capture == null) {
      this.showAlert(this.state.customer_image_not_captured);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.information_not_clear && this.props.kyc_customer_capture == null) {
      this.showAlert(this.state.customer_image_not_captured);
      return;
    }

    if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.address_different && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.pob_image_not_captured);
      return;
    }


    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.address_different && this.props.kyc_pob_capture == null) {
      this.showAlert(this.state.pob_image_not_captured);
      return;
    }

    if (!this.props.alternateContactValidated) {
      this.showAlert(this.state.invalid_altenate_contact_no);
      return;
    }

    if ((this.props.otp_status !== 1 && this.props.otp_status !== 3) && this.props.signature == null) {
      this.showAlert(this.state.signature_not_captured);
      return;
    }

    if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.signature == null) {
      this.showAlert(this.state.signature_not_captured);
      return;
    }

    let end_ts = Math.round((new Date()).getTime() / 1000);

    const data = {
      material_code: this.props.material_code,
      sim_serial_number: this.props.sim_serial_number,
      otp_status: this.props.otp_status,
      mobile_number: this.props.mobile_number,
      blocked_id: this.props.blocked_id_first,
      first_reload: this.props.first_reload,
      pre_post: this.props.pre_post,
      alternate_contact_number: this.props.alternate_contact_number,
      information_id_type: this.props.information_id_type,
      id_not_clear: this.props.information_not_clear,
      address_different: this.props.address_different,
      otp_number_list: this.props.otp_number_list,
      otp_selected_number: this.props.otp_number,
      reload_offer_code: this.props.reload_offer_code,
      start_ts: this.state.start_ts,
      end_ts: end_ts,
      id_reference: this.props.information_id_type === 'DR'
        ? 'DL'
        : (this.props.information_id_type === 'NIC'
          ? 'NIC'
          : 'PP'),
      cus_id: this.props.id_number,
      cus_type: this.props.local_foreign,
      document_exist: this.props.documentExist,
      imageTypeList: this.getImageTypeList()
    };

    // this.props.getApiLoading(true);
    this.setState({ isLoading: true });
    this.props.getMobileActLoading(true);
    const payLoad = {
      connection: this.props.mobile_number,
      simSerialNumber: this.props.sim_serial_number,
      customerType: this.props.local_foreign
    };
    Analytics.logEvent('dsa_m_act_tap_prepaid', payLoad);
    Utill.apiRequestPost('getO2AOfferDetails', 'gsmConnection', 'ccapp', data, this.onActivationSuccess, this.onActivationFailure, this.onActivationEx);
  };

  showActivateSuccessModal = (response) => {
    console.log('xxx showActivateSuccessModal :: response ', response);
    let data = response.data;
    let description = data.message;
    let description_title = data.mobile;

    let passProps = {
      primaryText: this.state.btnOk,
      primaryPress: this.goToHomePage,
      primaryPressPayload: data,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.SuccessAlert,
      description: Utill.getStringifyText(description),
      descriptionTitle : description_title,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorGreen },
      
    };

    console.log('showGeneralModal :: passProps ', passProps);
    Screen.showGeneralModal(passProps);
  };

  goToHomePage = () => {
    console.log('xxx goToHomePage');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const _this = this;
    setTimeout(() => {
      _this.props.resetMobileActivationState();
    }, 50);
  }

  //TODO : FINAL API
  onActivationSuccess = (response) => {
    console.log('xxxx onActivationSuccess');
    this.setState({ isLoading: false });
    this.props.getMobileActLoading(false);
    this.props.getApiResponseMobileAct(response);
    Analytics.logEvent('dsa_mobile_activation_success', {
      txId: response.data.tx_id,
      orderId: response.data.order_id,
      connection: response.data.mobile,
      activationType: this.props.information_id_type,
    });

    this.showActivateSuccessModal(response);

    if (this.props.information_id_type === 'NIC' && this.props.local_foreign !== 'foreign') {
      if (this.props.customer_information_image_nic_1 !== null || this.props.customer_information_image_nic_2 !== null) {
        this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image_nic_1.captureType, this.props.customer_information_image_nic_1.imageUri);
        this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image_nic_2.captureType, this.props.customer_information_image_nic_2.imageUri);
      }
    } else if (this.props.customer_information_image !== null) {
      this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image.captureType, this.props.customer_information_image.imageUri);
    }

    this.props.kyc_customer_capture !== null
      ? this.addToCaptureDb(response.data.tx_id, this.props.kyc_customer_capture.captureType, this.props.kyc_customer_capture.imageUri)
      : true;
    this.props.kyc_pob_capture !== null
      ? this.addToCaptureDb(response.data.tx_id, this.props.kyc_pob_capture.captureType, this.props.kyc_pob_capture.imageUri)
      : true;
    this.props.signature !== null
      ? this.addToCaptureDb(response.data.tx_id, 7, this.props.signature.signatureUri)
      : true;
  }

  onActivationFailure = (response) => {
    this.setState({ isLoading: false });
    this.props.getMobileActLoading(false);
    this.props.getApiResponseMobileAct(response);
    const payLoad = {
      connection: this.props.mobile_number,
      simSerialNumber: this.props.sim_serial_number,
      customerType: this.props.local_foreign
    };
    Analytics.logEvent('dsa_mobile_activation_failure', payLoad);
    this.navigator.showModal({
      screen: 'DialogRetailerApp.models.MobileActModel',
      overrideBackPress: true
    });
  }

  onActivationEx = (error) => {
    this.setState({ isLoading: false });
    this.props.getMobileActLoading(false);
    const payLoad = {
      connection: this.props.mobile_number,
      simSerialNumber: this.props.sim_serial_number,
      customerType: this.props.local_foreign
    };
    Analytics.logEvent('dsa_mobile_activation_exception', payLoad);

    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.models.GeneralModalException',
      title: 'GeneralModalException',
      passProps: { message: error },
      overrideBackPress: true
    });
  }

  onPressCaptureNIC = () => {
    console.log('xxxx onPressCaptureNIC');
    this
      .props
      .navigator
      .showLightBox({
        screen: 'DialogRetailerApp.models.NicCaptureModel',
        passProps: {
          title: 'titleN',
          content: 'contentN',
          onClose: null
        },
        style: modelStyle,
        adjustSoftInput: 'resize'
      });
  };

  onPress = (key) => {
    console.log(key);
    // this.setState({ sectionClick1: false, sectionClick2: false, sectionClick3: false });
    // switch (key) {
    //     case 1:
    //         this.setState({
    //             sectionClick1: !this.state.sectionClick1
    //         });
    //         break;
    //     case 2:
    //         this.setState({
    //             sectionClick2: !this.state.sectionClick2
    //         });
    //         break;
    //     case 3:
    //         this.setState({
    //             sectionClick3: !this.state.sectionClick3
    //         });
    //         break;
    //     default:
    //         this.setState({ sectionClick1: true });
    //         break;
    // }
  };

  onClickInformationNotClear = () => {
    this.setState({
      idNotClearChecked: !this.state.idNotClearChecked
    });
    this
      .props
      .getInformationNotClearMobileAct(!this.props.information_not_clear);

    if (!this.state.idNotClearChecked) {
      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.CUSTOMER_IMAGE);

    } else {
      this.props.getKycCustomerCaptureMobileAct(null);
    }
  }

  onClickInformationNotClearImage = () => {
    this
      .props
      .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.CUSTOMER_IMAGE);
    this
      .props
      .navigator
      .push({
        title: 'Customer Image Capture',
        screen: 'DialogRetailerApp.views.CameraScreen',
        passProps: {
          onReturnView: 'some text',
          captureType: this.props.information_id_type
        }
      });

  }

  onClickPassportCaptureForeign = () => {
    this
      .props
      .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PASSPORT);
    this
      .props
      .navigator
      .push({
        title: 'Customer Image Capture',
        screen: 'DialogRetailerApp.views.CameraScreen',
        passProps: {
          onReturnView: 'some text',
          captureType: this.props.information_id_type
        }
      });
  }

  onClickAddressDifferent = () => {

    this.setState({
      addressDifferentChecked: !this.state.addressDifferentChecked
    });

    this
      .props
      .getDifferentAddressMobileAct(!this.props.address_different);

    if (!this.state.addressDifferentChecked) {
      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);
    } else {
      this.props.getKycPobCaptureMobileAct(null);
    }
  }

  onClickAddressDifferentImage = () => {
    this
      .props
      .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);
    this
      .props
      .navigator
      .push({
        title: 'POB Capture',
        screen: 'DialogRetailerApp.views.CameraScreen',
        passProps: {
          onReturnView: 'some text',
          captureType: this.props.information_id_type
        }
      });

  }

  onClickAddressCaptureForeign = () => {
    this
      .props
      .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);
    this
      .props
      .navigator
      .push({
        title: 'Address Capture',
        screen: 'DialogRetailerApp.views.CameraScreen',
        passProps: {
          onReturnView: 'some text',
          captureType: this.props.information_id_type
        }
      });
  }

  onCaptureSignature = () => {

    let screen = {
      title: 'SignatuerConfirmModal',
      id: 'DialogRetailerApp.models.SignatuerConfirmModal',
    };

    Utill.showModalView(screen, null, false);
  };

  otpContinue = (value) => {
    this.props.otp_validation.otp_pin == value ? this.setState({ promptVisible: false }) : Alert.alert('OTP Validation Failed!');
  }

  otpCancel = () => {
    this.setState({ promptVisible: false });
    this.props.getOtpStatusMobileAct(2);
    //this.props.resetMobileActivationState();  
  }


  releaseNumberSuccess = () => {
    console.log('releaseNumberSuccess');
  }

  releaseNumberFail = () => {
    this
      .props
      .getSimNumberMobileAct('');
    this
      .props
      .getMobileNumberMobileAct('');
    this
      .props
      .getFirstReloadMobileAct('');
    this
      .props
      .resetMobileActNicValidation();
    this
      .props
      .resetMobileActModelPopped();
    this
      .props
      .resetValidateSimNumber();
    this
      .props
      .getSkipValidationMobileAct(false);

    //this.setState({ sectionClick2: false });
  }

  reserveNumberFailure = (response) => {
    this.showAlert(response.data.error);
    this.setState({ isLoading: false });
    this
      .props
      .getMobileActLoading(false);
  }

  reserveNumberEx = (error) => {
    this.setState({ isLoading: false });
    this
      .props
      .getMobileActLoading(false);
    this.props.navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: { message: error },
        overrideBackPress: true
      });    
  }

  reserveNumberSuccess = (response) => {
    this.setState({ isLoading: false });
    this
      .props
      .getMobileActLoading(false);
    this.props.getApiLoading(false); //should change this to true
    this
      .props
      .getMobileNumberMobileAct(response.data.data[0].value);
    const num_list = `${response.data.data[0].value}|P;`;

    this
      .props
      .getBlockedIdMobileAct(response.data.blockedId);

    const releaseData = {
      flag: 'first',
      blocked_id_first: response.data.blockedId,
      num_list
    };

    Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail);
  }

  showValidationModel = () => {
    console.log('xxxx showValidationModel');
    this
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModelMobileActPostPaid',
        title: 'Success',
        
        overrideBackPress: true
      });
    // this
    //   .navigator
    //   .showLightBox({
    //     screen: 'DialogRetailerApp.models.GeneralModelMobileActPostPaid',
    //     passProps: {
    //       title: 'Success',
    //       onClose: 'this.dismissLightBox'
    //     },
    //     overrideBackPress: true,
    //     style: {
    //       backgroundBlur: 'dark',
    //       backgroundColor: 'rgba(0, 0, 0, 0.7)',
    //     },
    //     adjustSoftInput: 'resize',
    //     tapBackgroundToDismiss: false
    //   });
  }

  checkBoxClick = (key) => {
    switch (key) {
      case 1:
        this.setState({
          sectionClick1: !this.state.sectionClick1
        });
        break;
      case 2:
        this.setState({
          sectionClick2: !this.state.sectionClick2
        });
        break;
      default:
        this.setState({ sectionClick1: true });
        break;
    }
  };

  idSelcetionRadioBtn = (key) => {
    console.log('xxx idSelcetionRadioBtn');
    switch (key) {
      case 1:
        this.setState({ captureContex: 'Capture NIC' });
        break;
      case 2:
        this.setState({ captureContex: 'Capture Passport' });
        break;
      case 3:
        this.setState({ captureContex: 'Capture Driving License' });
        break;
      default:
        this.setState({ captureContex: 'Capture NIC' });
        break;
    }
  };

  openModel = (screenId, titleN = null, contentN = null, onClickCb = null) => {
    console.log(`xxxx openModel${screenId}`);
    this
      .props
      .navigator
      .showLightBox({
        screen: screenId,
        passProps: {
          title: titleN,
          content: contentN,
          onClose: onClickCb
        },
        style: modelStyle,
        adjustSoftInput: 'resize'
      });
  };

  showAlert = (msg) => {
    Utill.showAlert(msg);
    Analytics.logEvent('dsa_mobile_act_show_alert');
  };

  addToCaptureDb = async (txnId, imageId, imageUri) => {
    console.log('xxx addToCaptureDb');
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id1 = `${txnId}_${imageId}`;
      const url1 = Utill.createApiUrl('imageUploadEnc', 'initAct', 'ccapp');
      const imageFile1 = imageUri.replace('file://', '');
      const uploadSelector1 = 'encrypt';

      const options = {
        id: id1,
        url: url1,
        imageFile: imageFile1,
        uploadSelector: uploadSelector1,
        deviceData: JSON.stringify(deviceData)
      };

      const errorCb = (msg) => {
        console.log('xxxx addToCaptureDb errorCb ');
        Analytics.logEvent('dsa_m_act_sqlite_error');
        console.log(msg);
      };
      const successCb = (msg) => {
        console.log('xxxx addToCaptureDb successCb');
        Analytics.logEvent('dsa_m_act_sqlite_success');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, successCb);
      
    } catch (e) {
      console.log('addToCaptureDb Image Uploading Failed', e.message);
      Analytics.logEvent('dsa_m_act_sqlite_ex');
      this.showAlert(e.message);
    }
  }

  render() {
    //capture lable
    const captureNicLabelTxt = this.state.captureNicLabelTxt;
    const captureDlLabelTxt = this.state.captureDlLabelTxt;
    const capturePpLabelTxt = this.state.capturePpLabelTxt;
    const captureForeignAddress = this.state.captureForeignAddress;

    //captured lable
    const capturedNicLabelTxt = this.state.capturedNicLabelTxt;
    const capturedDlLabelTxt = this.state.capturedDlLabelTxt;
    const capturedPpLabelTxt = this.state.capturedPpLabelTxt;
    const capturedForeignAddress = this.state.capturedForeignAddress;

    //customer image not clear lable
    const notClearNicLabelTxt = this.state.notClearNicLabelTxt;
    const notClearDlLabelTxt = this.state.notClearDlLabelTxt;
    const notClearPpLabelTxt = this.state.notClearPpLabelTxt;

    //customer image not clear sub text
    const captureCustomerPhotoSubTxt = this.state.captureCustomerPhotoSubTxt;
    const captureProofOfDeliverySubTxt = this.state.captureProofOfDeliverySubTxt; 
    const capturedCustomerPhotoSubTxt = this.state.capturedCustomerPhotoSubTxt;
    const capturedProofOfDeliverySubTxt = this.state.capturedProofOfDeliverySubTxt; 

    //capture pob lable
    const pobNicLabelTxt = this.state.pobNicLabelTxt;
    const pobDlLabelTxt = this.state.pobDlLabelTxt;
    const pobPpLabelTxt = this.state.pobPpLabelTxt;

    let idLabel;
    let idLabelInput;
    let captureLabelTxt;
    let capturedLabelTxt;
    let cusImageNotClearTxt;
    let pobLabelTxt;
    let noOfImages;
    let isImageCaptured;
    let isImageCapturedForeignPp;
    let isImageCapturedForeignAddress;
    let isImageCapturedLocalAddress;
    let isImageCapturedCustomer;
    let iconValidate;
    let idIconSize;

    if (this.props.local_foreign === 'local') {
      idLabel = this.state.nicValidate;
      idLabelInput = this.state.idLabelInputNic;
      iconValidate = 'mode-edit';
      idIconSize = 26;
    } else {
      idLabel = this.state.passValidate;
      idLabelInput = this.state.idLabelInputPp;
      iconValidate = 'chevron-circle-right';
      idIconSize = Constants.icon.next.defaultFontSize;

    }

    if (this.props.information_id_type === 'NIC') {
      console.log('Triggering NIC Section');
      captureLabelTxt = captureNicLabelTxt;
      capturedLabelTxt = capturedNicLabelTxt;
      cusImageNotClearTxt = notClearNicLabelTxt;
      
      pobLabelTxt = pobNicLabelTxt;
      isImageCaptured = (this.props.customer_information_image_nic_1 === null || this.props.customer_information_image_nic_2 === null);
      isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
      isImageCapturedCustomer = this.props.kyc_customer_capture === null;
      noOfImages = 2;
    } else if (this.props.information_id_type === 'DR') {
      console.log('Triggering DR Section');
      captureLabelTxt = captureDlLabelTxt;
      capturedLabelTxt = capturedDlLabelTxt;
      cusImageNotClearTxt = notClearDlLabelTxt;
      pobLabelTxt = pobDlLabelTxt;
      isImageCaptured = this.props.customer_information_image === null;
      isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
      isImageCapturedCustomer = this.props.kyc_customer_capture === null;
      noOfImages = 1;
    } else {
      console.log('Triggering PP Section');
      captureLabelTxt = capturePpLabelTxt;
      capturedLabelTxt = capturedPpLabelTxt;
      cusImageNotClearTxt = notClearPpLabelTxt;
      pobLabelTxt = pobPpLabelTxt;
      isImageCaptured = this.props.customer_information_image === null;
      isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
      isImageCapturedCustomer = this.props.kyc_customer_capture === null;
      noOfImages = 1;
    }
    //doom

    if (this.props.local_foreign === 'foreign') {
      noOfImages = 1;
      isImageCapturedForeignPp = this.props.customer_information_image === null;
      isImageCapturedForeignAddress = this.props.kyc_pob_capture === null;
    }

    console.log('captureLabelTxt', captureLabelTxt);
    console.log('information_id_type', this.props.information_id_type);

    return (

      <View style={styles.container}>
        <InputSection
          key={1}
          label={idLabel}
          colorCode={this.props.firstValidated}
          onPress={()=>{ 
            Keyboard.dismiss;
            // if (this.props.firstValidated)
            //   this.setState({ sectionClick1: !this.state.sectionClick1, sectionClick2: false, sectionClick3: false });
          }}
        />{this.state.sectionClick1
          ? <SectionContainer >
            <MaterialInput
              label={idLabelInput}
              index={'NIC'}
              iconIndex={this.props.local_foreign}
              onPress={() => this.onPressPassport()}
              icon={iconValidate}
              iconSize={idIconSize}
              iconTap
            />
          </SectionContainer>
          : <View />
        }
        {/* {this.state.isLoading ? <ActIndicator animating /> : <View />} */}
        <InputSection
          key={2}
          label={this.state.productInfo}
          colorCode={this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated}
          onPress={()=>{ 
            Keyboard.dismiss;
            // if (this.props.firstValidated)
            //   this.setState({ sectionClick2: !this.state.sectionClick2, sectionClick1: false, sectionClick3: false  });
          }}
        />{this.state.sectionClick2
          ? <View>
            <SectionContainer >
              <MaterialInput
                label={this.state.simNo}
                index={'SIM No'}
                icon={'mode-edit'}
                onPress={() => this.onPressSIM()}
                editable={false}
                iconTap={this.props.firstValidated}
              />
            </SectionContainer>
            <SectionContainer >
              <MaterialInput
                label={this.state.mobileNo}
                index={'Mobile No'}
                icon={'mode-edit'}
                onPress={() => this.onPressMobile()}
                editable={false}
                iconTap={this.props.firstValidated && this.props.secondValidated}
              />
            </SectionContainer>
            {this.props.enable_reload ? 
              (this.props.getConfigurationRapidEZ.rapid_ez_availability)?
                <SectionContainer >
                  <MaterialInput
                    label={this.state.firstRel}
                    index={'First Reload Offer'}
                    icon={'mode-edit'}
                    onPress={() => this.onPressReload()}
                    editable={false}
                    iconTap={this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated}
                  />
                </SectionContainer>
                :
                null
              :  
              <View />
            }
          </View>
          : <View />
        }
        <InputSection
          key={3}
          label={this.state.custInfo}
          colorCode={this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated && this.props.signatureCaptured}
          onPress={()=>{ 
            Keyboard.dismiss;
            // if (this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated)
            //   this.setState({ sectionClick3: !this.state.sectionClick3, sectionClick1: false, sectionClick2: false });
          }}
        />{this.state.sectionClick3
          ? <View>
            {(this.props.local_foreign === 'local' && this.props.otp_status !== 1 && this.props.otp_status !== 3)
              || ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'local' && this.props.nic_validation.documnetExistInDataScan === 'N')
              ? <View>
                <SectionContainer >
                  <IdRadioButtons />
                </SectionContainer>
                {isImageCaptured ?
                  <CaptureId
                    label={captureLabelTxt}
                    onPress={() => {
                      Keyboard.dismiss();
                      this.onPressNIC();
                    }}
                  />
                  : <View>{noOfImages === 2 ?
                    <CaptureTwoThumbnails
                      imageCount={noOfImages}
                      label={capturedLabelTxt}
                      onPress1={() => {
                        Keyboard.dismiss();
                        this.onPressReCapture(1);
                      }}
                      onPress2={() => {
                        Keyboard.dismiss();
                        this.onPressReCapture(2);
                      }}
                    />
                    :
                    <CaptureThumbnails
                      imageCount={1}
                      label={capturedLabelTxt}
                      onPress={() => {
                        Keyboard.dismiss();
                        this.onPressReCapture();
                      }}
                    />
                  }
                  </View>
                }
                <SectionContainer />

                {
                  this.props.information_id_type !== 'PP' ?
                    <View>
                      <View style={styles.checkViewOne}>
                        <View>
                          <CheckBox
                            style={styles.checkBox1}
                            onClick={this.onClickInformationNotClear}
                            isChecked={this.state.idNotClearChecked}
                          />
                        </View>

                        <View style={styles.checkViewFour}>
                          <Text
                            style={styles.checkBox2}
                          >
                            {cusImageNotClearTxt}
                          </Text>
                        </View>
                      </View>

                      {/* First camera view */}
                      {this.state.idNotClearChecked ? <View style={ styles.faceNotClearTextLabel }>
                        {isImageCapturedCustomer ?
                          <CaptureId
                            label={captureCustomerPhotoSubTxt}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickInformationNotClearImage();
                            }}
                          />
                          :
                          <CaptureThumbnails
                            imageCount={1}
                            label={capturedCustomerPhotoSubTxt }
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickInformationNotClearImage();
                            }}
                          />
                        }
                      </View> : true}
                      {/* First camera view */}


                      <View style={styles.checkViewOne} >
                        <View>

                          <CheckBox
                            style={styles.checkBox3}
                            onClick={this.onClickAddressDifferent}
                            isChecked={this.state.addressDifferentChecked}
                          />

                        </View>

                        <View style={styles.checkViewFour}>
                          <Text
                            style={styles.checkBox4}
                          >
                            {pobLabelTxt}
                          </Text>
                        </View>
                      </View>

                      {/* Second camera view */}
                      {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                        {isImageCapturedLocalAddress ?
                          <CaptureId
                            label={captureProofOfDeliverySubTxt}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressDifferentImage();
                            }}
                          />
                          :
                          <CaptureThumbnails
                            imageCount={1}
                            label={capturedProofOfDeliverySubTxt}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressDifferentImage();
                            }}
                          />
                        }
                      </View> : true}
                      {/* Second camera view */}

                    </View> :
                    <View>
                      {isImageCapturedLocalAddress ?
                        <CaptureId
                          label={captureForeignAddress}
                          onPress={() => {
                            Keyboard.dismiss();
                            this.onClickAddressCaptureForeign();
                          }}
                        />
                        :
                        <CaptureThumbnails
                          imageCount={1}
                          label={capturedForeignAddress}
                          onPress={() => {
                            Keyboard.dismiss();
                            this.onClickAddressCaptureForeign();
                          }}
                        />
                      }
                    </View>
                }
              </View>
              :
              //Document Scan Y Logic Start (For OTP and MConnect Not verified)
              <View>
                {
                  this.props.local_foreign == 'local' && this.props.nic_validation.documnetExistInDataScan === 'Y' ?
                    <View>
                      {/* POB capture - checkbox (NIC or Driving License)*/}
                      <View style={styles.checkViewOne} >
                        <View>
                          <CheckBox
                            style={styles.checkBox3}
                            onClick={this.onClickAddressDifferent}
                            isChecked={this.state.addressDifferentChecked}
                          />
                        </View>
                        <View style={styles.checkViewFour}>
                          <Text
                            style={styles.checkBox4}
                          >
                            {pobLabelTxt}
                          </Text>
                        </View>
                      </View>
                      {/* POB capture - camera view (NIC or Driving License) */}
                      {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                        {isImageCapturedLocalAddress ?
                          <CaptureId
                            label={captureProofOfDeliverySubTxt}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressDifferentImage();
                            }}
                          />
                          :
                          <CaptureThumbnails
                            imageCount={1}
                            label={capturedProofOfDeliverySubTxt}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressDifferentImage();
                            }}
                          />
                        }
                      </View> : true}

                    </View> :
                    //Else Should Have the Passport Logic
                    //Foreigner Logic Start
                    <View>
                      {
                        (this.props.otp_status !== 1 && this.props.otp_status !== 3)
                          || ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.nic_validation !== null && this.props.nic_validation.documnetExistInDataScan === 'N') ?
                          <View>
                            {isImageCapturedForeignPp ?
                              <CaptureId
                                label={capturePpLabelTxt}
                                onPress={() => {
                                  Keyboard.dismiss();
                                  this.onClickPassportCaptureForeign();
                                }}
                              />
                              :
                              <CaptureThumbnails
                                imageCount={1}
                                label={capturedPpLabelTxt}
                                onPress={() => {
                                  Keyboard.dismiss();
                                  this.onClickPassportCaptureForeign();
                                }}
                              />
                            }

                            {isImageCapturedForeignAddress ?
                              <CaptureId
                                label={captureForeignAddress}
                                onPress={() => {
                                  Keyboard.dismiss();
                                  this.onClickAddressCaptureForeign();
                                }}
                              />
                              :
                              <CaptureThumbnails
                                imageCount={1}
                                label={capturedForeignAddress}
                                onPress={() => {
                                  Keyboard.dismiss();
                                  this.onClickAddressCaptureForeign();
                                }}
                              />
                            }
                          </View>
                          :
                          //true
                          //Drop2 New Change Below
                          <View>
                            {
                              ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.nic_validation !== null && this.props.nic_validation.documnetExistInDataScan === 'Y') ?
                                <View>
                                  {/* POB capture - checkbox (For Passport)*/}
                                  <View style={styles.checkViewOne} >
                                    <View>
                                      <CheckBox
                                        style={styles.checkBox3}
                                        onClick={this.onClickAddressDifferent}
                                        isChecked={this.state.addressDifferentChecked}
                                      />
                                    </View>
                                    <View style={styles.checkViewFour}>
                                      <Text
                                        style={styles.checkBox4}
                                      >
                                        {pobLabelTxt}
                                      </Text>
                                    </View>
                                  </View>
                                  {/* POB capture - camera view (For Passport) */}
                                  {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                                    {isImageCapturedLocalAddress ?
                                      <CaptureId
                                        label={captureProofOfDeliverySubTxt}
                                        onPress={() => {
                                          Keyboard.dismiss();
                                          this.onClickAddressDifferentImage();
                                        }}
                                      />
                                      :
                                      <CaptureThumbnails
                                        label={capturedProofOfDeliverySubTxt}
                                        onPress={() => {
                                          Keyboard.dismiss();
                                          this.onClickAddressDifferentImage();
                                        }}
                                      />
                                    }
                                  </View> : true}
                                </View>
                                : true
                            }
                          </View>
                      }
                    </View>
                  //Foreigner Logic End
                }
              </View>
              //Document Scan Y Logic End
            }
            <SectionContainer >
              <MaterialInput
                label={this.state.alternateContact}
                ref={'alternateContact'}
                isNeedToFocus
                iconTap
                index={'Alternative Contact No (Optional)'}
                icon={'mode-edit'}
                onPress={() => console.log('Alternative')}
                maxLength={10}
                keyboardType={'numeric'}
              />
            </SectionContainer>
            {/* {(this.props.otp_status !== 1 && this.props.otp_status !== 3)
              || ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.nic_validation !== null && this.props.nic_validation.documnetExistInDataScan == 'N') ? */}
            <View style={styles.bottomContainer}>
              <View style={styles.bottomInnerContainerSignature}>
                <TouchableOpacity
                  style={styles.signatureContainer}
                  onPress={() => this.onCaptureSignature()}
                >

                  {this.props.signature !== null
                    ? <Image
                      style={styles.signatureImage}
                      key={this.props.signature.signatureRand}
                      source={{
                        uri: `data:image/jpg;base64,${this.props.signature.signatureBase}`
                      }}
                      resizeMode="stretch"
                    />
                    : <Text style={styles.signatureTxt}>
                      {this.state.customerSignature}
                    </Text>
                  }
                </TouchableOpacity>
              </View>
              <View style={styles.reloadAmountContainer}>
                <View style={styles.totalTextViewLeft}>
                  <Text style={styles.totalTextLeft}> {this.state.reloadAmount}</Text>
                </View>
                <View style={styles.totalTextViewRight}>
                  <Text style={styles.totalTextRight}> Rs {(this.props.first_reload =='')? 0 :  this.props.first_reload}</Text>
                </View>            
              </View>
            </View> 


          </View>
          : <View />}
        <View style={styles.activateBtnContainer}>
          <View style={styles.activatDummy} />
          <TouchableOpacity
            style={[
              styles.activateBtn, {
                backgroundColor: this.state.activateBtnColor
              }
            ]}
            onPress={() => this.debouncedFinalActivation()}
            disabled={!(this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated)}
          >
            <Text
              style={[
                styles.activateBtnTxt, {
                  color: this.state.activateBtnTxtColor
                }
              ]}
            >
              {this.state.BtnTxt}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: BottomItems => MOBILE ', state.mobile);
  const idLabel = '';
  const idLabelInput = '';
  let nicValidation = null;
  let firstValidated = false;
  let simValidated = false;
  let secondValidated = false;
  let thirdValidated = false;
  let fourthValidated = false;
  let signatureCaptured = false;
  let alternateContactValidated = true;
  let nicApiFail = false;
  let simApiFail = false;
  let documentExist = '';

  const id_number = state.mobile.id_number;
  const sim_number = state.mobile.sim_number;
  const mobile_number = state.mobile.mobile_number;
  const local_foreign = state.mobile.local_foreign;
  const nic_validation = state.mobile.nic_validation;
  const nic_api_fail = state.mobile.nic_api_fail;
  const sim_api_fail = state.sim.sim_api_fail;
  const sim_validation = state.sim.sim_validation;
  const skip_validation = state.mobile.skip_validation;
  const model_popped = state.mobile.model_popped;
  const otp_popped = state.mobile.otp_popped;
  const otp_validation = state.mobile.otp_validation;
  const information_id_type = state.mobile.information_id_type;
  const address_different = state.mobile.address_different;
  const information_not_clear = state.mobile.information_not_clear;
  const customer_information_image = state.mobile.kyc_capture;
  const customer_information_image_nic_1 = state.mobile.kyc_capture_nic_1;
  const customer_information_image_nic_2 = state.mobile.kyc_capture_nic_2;
  const alternate_contact_number = state.mobile.alternate_contact_number;
  const signature = state.mobile.signature;
  const kyc_pob_capture = state.mobile.kyc_pob_capture;
  const kyc_customer_capture = state.mobile.kyc_customer_capture;
  const otp_number = state.mobile.otp_number;
  //const notificationNumberList = state.mobile.nic_validation.notificationNumberList;

  nic_validation !== null ? documentExist = nic_validation.documnetExistInDataScan : documentExist = '';

  if (state.mobile.local_foreign === 'local' && (Utill.nicValidate(state.mobile.id_number).length === 10 || Utill.nicValidate(state.mobile.id_number).length == 12)) {
    firstValidated = true;
  } else if (state.mobile.local_foreign === 'foreign' && state.mobile.id_number.length > 4) {
    !state.mobile.id_number.match(/^[a-zA-Z0-9]+$/) ? firstValidated = false : firstValidated = true;
  } else {
    firstValidated = false;
  }

  if (nic_validation == null && nic_api_fail) {
    nicApiFail = true;
  }

  if (state.mobile.sim_number.length === 8) {
    simValidated = true;
  } else {
    simValidated = false;
  }

  if (sim_validation == null && sim_api_fail) {
    simApiFail = true;
    simValidated = false;
  }

  if (state.sim.sim_validation !== null && state.sim.sim_validation.success) {
    secondValidated = true;
  } else {
    secondValidated = false;
  }

  if (state.mobile.mobile_number.length === 9 || state.mobile.mobile_number.length === 10 || state.mobile.mobile_number.length === 11) {
    thirdValidated = true;
  } else {
    thirdValidated = false;
  }

  if (state.mobile.nic_validation !== null) {
    nicValidation = state.mobile.nic_validation;
  } else {
    nicValidation = null;
  }

  // if (state.mobile.local_foreign === 'local') {
  //     idLabel = this.state.nicValidate;
  //     idLabelInput = 'NIC';
  // } else {
  //     idLabel = this.state.passValidate;
  //     idLabelInput = 'Passport';
  // }


  if (firstValidated && secondValidated && thirdValidated && local_foreign == 'local' && nic_validation !== null && nic_validation.documnetExistInDataScan === 'N' && information_id_type !== 'NIC' && customer_information_image === null) {
    fourthValidated = false;
  } else {
    fourthValidated = true;
  }

  if (firstValidated && secondValidated && thirdValidated && alternate_contact_number !== '') {
    Number.isInteger(Number(alternate_contact_number))
      ? alternateContactValidated = true
      : alternateContactValidated = false;
  } else {
    alternateContactValidated = true;
  }

  if (state.mobile.signature !== null) {
    signatureCaptured = true;
  }

  const material_code = state.sim.sim_validation !== null
    ? state.sim.sim_validation.data.material_code
    : null;
  const sim_serial_number = state.sim.sim_validation !== null
    ? state.sim.sim_validation.data.sim_serial_number
    : null;
  const otp_status = state.mobile.otp_status;
  const blocked_id_first = state.mobile.blocked_id_first;
  const pre_post = state.mobile.pre_post;
  const first_reload = state.mobile.first_reload;
  const otp_number_list = (state.mobile.otp_status && state.mobile.nic_validation !== null)
    ? state.mobile.nic_validation.notificationNumberList
    : null;
  const reload_offer_code = state.mobile.reload_offer_code;
  const getConfigurationRapidEZ  = state.auth.getConfiguration.rapid_ez_account; 
  const getConfiguration  = state.auth.getConfiguration; 
  let { enable_reload = '' } = getConfiguration.config;
  console.log('simApiFail', simApiFail);

  if (otp_status !== 1 && otp_status !== 3 && firstValidated && secondValidated && thirdValidated && local_foreign == 'local' && nic_validation !== null && (nic_validation.documnetExistInDataScan === 'N' || nic_validation.documnetExistInDataScan === 'Y') && information_id_type !== 'NIC' && customer_information_image === null) {
    fourthValidated = false;
  } else if ((otp_status == 1 || otp_status == 3) && firstValidated && secondValidated && thirdValidated && local_foreign == 'local' && nic_validation !== null && (nic_validation.documnetExistInDataScan === 'N') && information_id_type !== 'NIC' && customer_information_image === null) {
    fourthValidated = false;
  } else {
    fourthValidated = true;
  }
  
  let notificationNumberListArray = false;

  if (state.mobile.nic_validation !==  null 
          && state.mobile.nic_validation.notificationNumberList != undefined
          && state.mobile.nic_validation.notificationNumberList !== null) {
    console.log('xxx notificationNumberListArray');
    console.log('notificationNumberListArray', state.mobile.nic_validation.notificationNumberList.length !== 0);
    notificationNumberListArray = state.mobile.nic_validation.notificationNumberList.length !== 0;
  } 

  return {
    Language: state.lang.current_lang,
    idLabel,
    idLabelInput,
    id_number,
    local_foreign,
    firstValidated,
    nic_validation,
    nicValidation,
    sim_validation,
    nicApiFail,
    simApiFail,
    sim_number,
    mobile_number,
    simValidated,
    skip_validation,
    model_popped,
    otp_popped,
    secondValidated,
    thirdValidated,
    fourthValidated,
    information_id_type,
    information_not_clear,
    address_different,
    customer_information_image,
    customer_information_image_nic_1,
    customer_information_image_nic_2,
    alternateContactValidated,
    alternate_contact_number,
    material_code,
    sim_serial_number,
    otp_status,
    otp_number,
    otp_validation,
    blocked_id_first,
    pre_post,
    first_reload,
    otp_number_list,
    reload_offer_code,
    signature,
    signatureCaptured,
    kyc_customer_capture,
    kyc_pob_capture,
    documentExist,
    notificationNumberListArray,
    getConfiguration,
    enable_reload,
    getConfigurationRapidEZ
  };
};

const modelStyle = {
  backgroundBlur: 'dark',
  backgroundColor: Colors.modalOverlayColor,
  tapBackgroundToDismiss: true
};
const modelStyleNoSelection = {
  backgroundBlur: 'dark',
  backgroundColor: Colors.modalOverlayColor,
  tapBackgroundToDismiss: false
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor,
  },
  bottomContainer: {
    flex: 1,
    marginTop: 10,
    height: 170,
    marginLeft: 18,
    marginRight: 18,
  },
  
  bottomInnerContainerSignature: {
    flex: 1,
    height: 50,
    marginLeft:20,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },

  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    margin: 10,
    borderWidth: 1,
    borderColor: Colors.btnDeactiveTxtColor,
    borderRadius: 3,
  },
  reloadAmountContainer: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 20,
    marginRight: 10,
  },

  totalTextViewLeft: { 
    flex: 1
  },
  totalTextViewRight: { 
    flex: 1,
  },
  totalTextLeft: {
    padding: 5,
    textAlign: 'left' ,
    fontWeight: '500',
    fontSize: Styles.defaultFontSize
  },
  totalTextRight: { 
    padding: 5,
    textAlign: 'right',
    fontWeight: '500',
    fontSize: Styles.defaultFontSize 
  },

  activateBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 10,
  },
  activatDummy: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activateBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderRadius: 5,

  },
  activateBtnTxt: {
    fontSize: Styles.defaultBtnFontSize
  },
  signatureTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  checkViewOne: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10,
    marginLeft: 12
  },

  checkViewFour: {
    flex: 1,
    borderWidth: 0,
    flexDirection: 'row'
  },
  signatureImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined,
  },
  checkBox1: {
    padding: 0,
    paddingBottom: 3,
    paddingRight: 5,
    marginLeft: 30

  },

  checkBox2: {
    fontSize: 14,
    paddingBottom: 3,
    marginRight: 10

  },
  checkBox3: {
    padding: 0,
    paddingRight: 5,
    marginLeft: 30

  },
  checkBox4: {
    fontSize: 14,
    marginRight: 10

  },
  faceNotClearTextLabel: { 
    marginTop: -10, 
    top: -10, 
    marginLeft: 28,
    bottom: -20,
    backgroundColor: Colors.transparent
  }

});

export default connect(mapStateToProps, actions)(BottomItems);