import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

class CaptureTwoThumbnails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sectionClick: false,
      deafaultState: true
    };
  }

  render() {
    const { label, onPress1, onPress2 } = this.props;

    return (
      <View style={styles.container}>
        <View style={[styles.innerTextContainer]}>
          <Text style={styles.innerText}>{label}
          </Text>
        </View>
        <View style={styles.image2}>
          <TouchableOpacity style={[styles.innerImageContainer1]} onPress={onPress1}>
            <FontAwesome name='picture-o' size={26} color={Colors.colorBlack}/>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.innerImageContainer2]} onPress={onPress2}>
            <FontAwesome name='picture-o' size={26} color={Colors.colorBlack}/>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 2,
    marginTop: 2,
    marginLeft: 35,
    marginRight: 15,
    backgroundColor: Colors.appBackgroundColor
  },
  image2: {
    flex: 4,
    flexDirection: 'row'
  },

  innerImageContainer1: {
    flex: 2,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  innerImageContainer2: {
    flex: 2,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  innerTextContainer: {
    flex: 4,
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 8,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  innerText: {
    color: Colors.thumbnailTxtColor,
    fontSize: Styles.thumbnailTxtFontSize
  }

});

export default CaptureTwoThumbnails;
