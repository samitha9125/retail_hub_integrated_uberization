import React from 'react';
import { StyleSheet, View, Text, Picker } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import strings from '../../Language/MobileActivaton';
import Utills from '../../utills/Utills';
import Images from '../../config/images';
import { Screen } from '@Utils';

const Utill = new Utills();

class TopItems extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      user: '',
      value: 1,
      prepaid: strings.prepaid,
      postpaid: strings.postpaid,
      srilankan: strings.srilankan,
      foreigner: strings.foreigner,
      buttonContinue: strings.continue,
      buttonOk: strings.btnOk
    };
  }

  componentDidMount() {
    this.checkRapidEzcashModal();
  }

  componentWillReceiveProps(nextProps) {
    nextProps.local_foreign == 'local'
      ? this.setState({ user: 'local' })
      : this.setState({ user: 'foreign' });
  }

     updateUser = (userId) => {
       this.setState({ user: userId } , async()=> {
         await this.props.getLocalForeignMobileAct(userId);
         await this.props.getIdNumberMobileAct('');
         await this.props.getOtpStatusMobileAct('');//NEW
         await this.props.resetMobileActivationContactData(); //New
       }); 
     }

    handleOnPressPre = (value1) => {
      this.setState({ value: value1 } , async()=> {
        await this.props.resetMobileActivationState();
        await this.props.getOtpStatusMobileAct('');//NEW
        await this.props.getPrePostMobileAct('pre');
      }); 
      this.checkRapidEzcashModal();
    }

    handleOnPressPost = (value1) => {
      this.setState({ value: value1 } , async()=> {
        await this.props.resetMobileActivationState();
        await this.props.getOtpStatusMobileAct('');//NEW
        await this.props.getPrePostMobileAct('post');
      }); 
      this.checkEzcashAccountModal();
    }

    // this will check rapidEzCash availability
    checkRapidEzcashModal = () => {
      if  (!this.props.getConfiguration.rapid_ez_account.rapid_ez_availability && this.props.getConfiguration.config.enable_reload ){
        let passProps = {
          primaryText: this.state.buttonContinue,
          primaryPress: () => this.onPressContinue(),
          disabled: false,
          hideTopImageView: false,
          icon: Images.icons.AttentionAlert,
          description: Utill.getStringifyText(this.props.rapid_ez_error),
          primaryButtonStyle: { color: Colors.colorYellow },
        };
    
        Screen.showGeneralModal(passProps);
      }
    }

    // this will check ezcash availability
    checkEzcashAccountModal = () => {
      if ( this.props.getConfiguration.ez_cash_account.ez_cash_account_availability == false ){
        let passProps = {
          primaryText: this.state.buttonOk,
          primaryPress: () => this.onPressOk(),
          disabled: false,
          hideTopImageView: false,
          icon: Images.icons.FailureAlert,
          description: Utill.getStringifyText(this.props.ezCash_error),
          primaryButtonStyle: { color: Colors.colorRed },
        };
    
        Screen.showGeneralModal(passProps);
      }
    }
  
    /**
     * @description dismiss modal
     * @memberof BottomItem
     */
    onPressContinue = () => {
      console.log('xxx onPressContinue');
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animationType: 'slide-down' });
    };
  
    /**
     * @description Back to homeScreen
     * @memberof BottomItem
     */
    onPressOk = () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animationType: 'slide-down' });
      navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    }

    render() {
      return (
        <View style={styles.container}>
          <View style={styles.containerRadioWrapper}>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.state.value}
                value={1}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.state.value == 1 ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={this.handleOnPressPre}>
                <Text style={styles.radioBtnTxt}>{this.state.prepaid}</Text>
              </RadioButton>
            </View>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.state.value}
                value={2}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.state.value == 2 ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={this.handleOnPressPost}>
                <Text style={styles.radioBtnTxt}>{this.state.postpaid}</Text>
              </RadioButton>
            </View>
          </View>
          <View style={styles.containerPicker}>
            <Picker
              style={styles.picker}
              itemStyle={styles.pickerItem}
              mode={'dropdown'}
              selectedValue={this.state.user}
              onValueChange={this.updateUser}>
              <Picker.Item label={this.state.srilankan} value="local"/>
              <Picker.Item label={this.state.foreigner} value="foreign"/>
            </Picker>
          </View>
        </View>
      );
    }
}

const mapStateToProps = state => {
  const local_foreign = state.mobile.local_foreign;
  const getConfiguration = state.auth.getConfiguration;
  const { rapid_ez_error = '' } = getConfiguration.rapid_ez_account;
  const { ezCash_error = '' } = getConfiguration.ez_cash_account;
 
  return { local_foreign, language: state.lang.current_lang,getConfiguration,rapid_ez_error, ezCash_error };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    marginLeft: 7,
    marginRight: 7
  },
  containerRadioWrapper: {
    flex: 2,
    marginRight: 2,
    flexDirection: 'row',
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 0,
    padding: 5,
    borderColor: Colors.topSelctionBorderColor,
    borderWidth: StyleSheet.hairlineWidth
  },
  containerRadioBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appBackgroundColor,
    margin: 2
  },
  containerPicker: {
    flex: 1.2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorTransparent,
    paddingLeft: 20,
    marginLeft: -2,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 1,
    borderColor: Colors.topSelctionBorderColor,
    borderWidth: StyleSheet.hairlineWidth
  },
  picker: {
    width: 140,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 5,
    marginLeft: 2,
    marginRight: 15,
    borderWidth: 1
  },
  radioBtnTxt: {
    paddingLeft: 5
  },
  pickerItem: {
    color: Colors.colorRed
  }
});

export default connect(mapStateToProps, actions)(TopItems);
