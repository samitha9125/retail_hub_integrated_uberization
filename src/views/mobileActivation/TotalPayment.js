import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import strings from '../../Language/MobileActivaton.js';

class TotalPayments extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      expanSwich: false,
      needToShowoutstanding: false,
      totalPayValue: '',
      conFeeValue: '',
      outstandingFeeValue: '',
      localCallValue: '',
      arrowDown: 'md-arrow-dropdown',
      connection_fee: '',
      depositAmount: '',
      locals: {
        totalPay: strings.totalPay,
        showData: strings.showData,
        localCall: strings.localCall,
        conFee: strings.conFee,
        outstandingFee: strings.outstandingFee
      }
    };
  }

  componentDidMount(){
    console.log("Totalpayments did mount.");
    var outstanding_fee = this.checkNumber(this.props.outstanding_fee);
    var connection_fee = this.checkNumber(this.props.connection_fee);
    var depositAmount = this.checkNumber(this.props.depositAmount);

    console.log('outstanding_fee', outstanding_fee);
    console.log('connection_fee', connection_fee);
    console.log('depositAmount', depositAmount);

    var total = (parseFloat(Number(outstanding_fee)) + parseFloat(Number(connection_fee)) + parseFloat(Number(depositAmount))).toFixed(2);

    this.setState({
      totalPayValue: "Rs. " + parseFloat(total).toFixed(2),
      conFeeValue: "Rs. " + parseFloat(connection_fee).toFixed(2),
      outstandingFeeValue: "Rs. " + parseFloat(outstanding_fee).toFixed(2),
      localCallValue: "Rs. " + parseFloat(depositAmount).toFixed(2)
    }, () =>{
      this.props.setTotalPaymentData({
        totalPayValue: "Rs. " + parseFloat(total).toFixed(2),
        conFeeValue: "Rs. " + parseFloat(connection_fee).toFixed(2),
        outstandingFeeValue: "Rs. " + parseFloat(outstanding_fee).toFixed(2),
        localCallValue: "Rs. " + parseFloat(depositAmount).toFixed(2),
        outstanding_fee: outstanding_fee
      });
    });

    this.props.setPaymentInfo({
      totalPayValue: parseFloat(total).toFixed(2),
      conFeeValue: parseFloat(connection_fee).toFixed(2),
      outstandingFeeValue: parseFloat(outstanding_fee).toFixed(2),
      localCallValue: parseFloat(depositAmount).toFixed(2)
    });

    this
      .props
      .getTotalPayment(total);

    if (this.props.outstanding_fee > 0) {
      this.setState({ needToShowoutstanding: true });
    }
  }

  componentWillMount(){
    console.log("Totalpayments will mount.");
  }

  componentWillReceiveProps(nextProps) {
    console.log("TOTAL PAYMENT NEXTPROPS: ", nextProps);
    console.log("TOTAL PAYMENT NEXTPROPS: ", JSON.stringify(nextProps));
    console.log("outstanding_fee XX", nextProps.props.outstanding_fee);
    console.log("connection_fee XX", nextProps.connection_fee);
    console.log("depositAmount XX", nextProps.depositAmount);

    var outstanding_fee = this.checkNumber(nextProps.props.outstanding_fee);
    var connection_fee = this.checkNumber(nextProps.connection_fee);
    var depositAmount = this.checkNumber(nextProps.depositAmount);

    console.log('outstanding_fee', outstanding_fee);
    console.log('connection_fee', connection_fee);
    console.log('depositAmount', depositAmount);

    var total = (parseFloat(Number(outstanding_fee)) + parseFloat(Number(connection_fee)) + parseFloat(Number(depositAmount))).toFixed(2);

    this.setState({
      totalPayValue: "Rs. " + parseFloat(total).toFixed(2),
      conFeeValue: "Rs. " + parseFloat(connection_fee).toFixed(2),
      outstandingFeeValue: "Rs. " + parseFloat(outstanding_fee).toFixed(2),
      localCallValue: "Rs. " + parseFloat(depositAmount).toFixed(2)
    }, () =>{
      if (nextProps.totalPaymentData.totalPayValue !== this.state.totalPayValue ||
          nextProps.totalPaymentData.conFeeValue !== this.state.conFeeValue ||
          nextProps.totalPaymentData.outstandingFeeValue !== this.state.outstandingFeeValue || 
          nextProps.totalPaymentData.localCallValue !== this.state.localCallValue ){
        this.props.setTotalPaymentData({
          totalPayValue: "Rs. " + parseFloat(total).toFixed(2),
          conFeeValue: "Rs. " + parseFloat(connection_fee).toFixed(2),
          outstandingFeeValue: "Rs. " + parseFloat(outstanding_fee).toFixed(2),
          localCallValue: "Rs. " + parseFloat(depositAmount).toFixed(2),
          outstanding_fee: outstanding_fee
        });
      }
    });

    this.props.setPaymentInfo({
      totalPayValue: parseFloat(total).toFixed(2),
      conFeeValue: parseFloat(connection_fee).toFixed(2),
      outstandingFeeValue: parseFloat(outstanding_fee).toFixed(2),
      localCallValue: parseFloat(depositAmount).toFixed(2)
    });

    this
      .props
      .getTotalPayment(total);

    if (nextProps.props.outstanding_fee > 0) {
      this.setState({ needToShowoutstanding: true });
    }

    console.log("totalPayValue: ", total);
    console.log("connection_fee: ", connection_fee);
    console.log("depositAmount: ", depositAmount);
  }

  checkNumber = (number) => {
    console.log('xxx checkNumber');
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }

  }

  expanDetails() {
    if (this.state.expanSwich) {
      this.setState({ expanSwich: false, arrowDown: 'md-arrow-dropdown' });
    } else {
      this.setState({ expanSwich: true, arrowDown: 'md-arrow-dropup' });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRadioWrapper}>
          <View style={styles.rowmain}>
            <Text style={styles.cl2}>{this.state.locals.totalPay}</Text>
            <Text
              style={styles.showDetails}
              onPress={this
                .expanDetails
                .bind(this)}>{this.state.locals.showData}</Text>
            <Ionicons name={this.state.arrowDown} size={20} color={Colors.linkColor}/>
            <Text style={styles.cl3}>{this.props.totalPaymentData.totalPayValue}</Text>
          </View>
          {this.state.expanSwich
            ? <View style={styles.moreDataStyle}>
              {this.props.totalPaymentData.outstandingFeeValue !== '' && this.props.totalPaymentData.outstanding_fee > 0? <View style={styles.rowsub}>
                <Text style={styles.cl2}>{this.state.locals.outstandingFee}</Text>
                <Text style={styles.cl3}>{this.props.totalPaymentData.outstandingFeeValue}</Text>
              </View>
                : <View/>
              }
              <View style={styles.rowsub}>
                <Text style={styles.cl2}>{this.state.locals.localCall}</Text>
                <Text style={styles.cl3}>{this.props.totalPaymentData.localCallValue}</Text>
              </View>
              <View style={styles.rowsub}>
                <Text style={styles.cl2}>{this.state.locals.conFee}</Text>
                <Text style={styles.cl3}>{this.props.totalPaymentData.conFeeValue}</Text>
              </View>
            </View>
            : <View/>
          }
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TOTAL PAYMENT => MOBILE ACTIVATION', state.mobile);
  console.log('****** REDUX STATE :: TOTAL PAYMENT => MOBILE ACTIVATION', JSON.stringify(state.mobile));
  const connection_fee = state.mobile.selectedPackage.connection_fee;
  const depositAmount = (state.mobile.packageDeposits !==null) ? state.mobile.packageDeposits.depositAmount : 0;
  const totalPaymentData = state.mobile.totalPaymentData;

  const language = state.lang.current_lang;
  return { connection_fee, depositAmount, language, totalPaymentData };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    marginLeft: 43,
    marginRight: 7
  },
  containerRadioWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    padding: 5,
    borderColor: Colors.borderLineColor
  },

  moreDataStyle :{ 
    marginTop: 8,
    paddingTop: 5
  },
  rowmain: {
    flexDirection: 'row',
    flex: 1
  },
  rowsub: {
    flexDirection: 'row',
    marginTop: 5,
    flex: 1
  },
  showDetails: {
    color: Colors.urlLinkColor,
    textDecorationLine: 'underline',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: Styles.otpEnterModalFontSize
  },
  cl2: {
    flexDirection: 'row',
    flex: 1,
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  cl3: {
    textAlign: 'right',
    flex: 1,
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  }

});

export default connect(mapStateToProps, actions)(TotalPayments);