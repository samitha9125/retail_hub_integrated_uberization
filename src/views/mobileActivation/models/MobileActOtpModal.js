import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Analytics from '../../../utills/Analytics';
import Utills from '../../../utills/Utills';
import Orientation from 'react-native-orientation';
import strings from '../../../Language/MobileActivaton';

const Utill = new Utills();

/**
 * Curently using for both prepaid and postpaid
 */

class MobileActOtpModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      openModel: false,
      enteredOtp: '',
      resendTapped: 0,
      otpCount: 0,
      locals: {
        five_pin_number_sent_to: strings.five_pin_number_sent_to,
        enter_pin: strings.enter_pin,
        recive_five_digit_pin: strings.recive_five_digit_pin,
        resend_pin: strings.resend_pin,
        validate_pin: strings.validate_pin,
        skip_validation: strings.skip_validation,
        otp_validation_success: strings.otp_validation_success,
        otp_validation_failed: strings.otp_validation_failed,
        otp_resent: strings.otp_resent
      }
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  onResendPin = () => {
    this.setState({
      resendTapped: this.state.resendTapped + 1
    });

    const data = {
      customer_msisdn: this.props.otp_number
    };

    this.props.mobileActOTPValidation(data);
    Utill.showAlertMsg(this.state.locals.otp_resent);
    Analytics.logEvent('dsa_mobile_activation_otp_resend', { customerNumber: this.props.otp_number });
  }

  valueChanged = (number) => {
    this.setState({ enteredOtp: number });
  }
  otpContinue = () => {
    this.setState({
      otpCount: this.state.otpCount + 1
    });

    if (this.props.state.mobile.otp_validation !== null && (this.props.state.mobile.otp_validation.otp_pin == this.state.enteredOtp)) {
      //Utill.showAlertMsg(this.state.locals.otp_validation_success);
      //TODO: UAT concern
      this
        .props
        .resetMobileActOTPValidation();
      this
        .props
        .getOtpStatusMobileAct(1);
      Analytics.logEvent('dsa_mobile_activation_otp_success', { customerNumber: this.props.otp_number });
      this.dismissModal();

    } else if (this.state.otpCount === 2) {
      Utill.showAlertMsg(this.state.locals.otp_validation_failed);
      this.dismissModal();
      this
        .props
        .getOtpStatusMobileAct(2);
      this
        .props
        .resetMobileActOTPValidation();
      Analytics.logEvent('dsa_mobile_activation_otp_failed', { customerNumber: this.props.otp_number });
    } else {
      Utill.showAlertMsg(this.state.locals.otp_validation_failed);
      this.setState({ enteredOtp: '' });
      Analytics.logEvent('dsa_mobile_activation_otp_failed', { customerNumber: this.props.otp_number });
    }
  }

  dismissModal = () => {
    console.log('@@@@@@@@@@@@@@@@ dismissModal @@@@@@@@@@@@@@@@@@');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  otpCancel = () => {
    Analytics.logEvent('dsa_mobile_activation_otp_cancel', { customerNumber: this.props.otp_number });
    this.dismissModal();
    this
      .props
      .resetMobileActOTPValidation();
    this
      .props
      .getOtpStatusMobileAct(2);
  }

  render() {
    let resendRightView;
    if (this.state.resendTapped !== 3) {
      resendRightView = (
        <View style={styles.resendRightView}>
          <TouchableOpacity onPress={this.onResendPin} style={styles.resendRightViewBtn}>
            <Text style={styles.resendRightViewText}>{this.state.locals.resend_pin}
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      resendRightView = true;
    }
    return (
      <View style={styles.container}>
        <View style={styles.containerOverlay}>
          <View style={styles.topContainer}/>
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.textViewContainer}>
                {/* Top text view */}
                <Text style={styles.text_1}>
                  {this.state.locals.five_pin_number_sent_to}
                  {' '}
                  {Utill.numberMask(this.props.otp_number)}
                </Text>
                <Text style={styles.text_2}>
                  {this.state.locals.enter_pin}
                </Text>
              </View>
              {/* OTP enter text input*/}
              <View style={styles.otpInputContainer}>
                <TextInput
                  style={styles.inputTextField}
                  underlineColorAndroid={Colors.colorTransparent}
                  onChangeText={(value) => this.valueChanged(value)}
                  keyboardType={'numeric'}
                  value={this.state.enteredOtp}/>
              </View>
              {/* OTP resent view*/}
              <View style={styles.resendViewContainer}>
                <View style={styles.resendLeftView}>
                  <Text style={styles.resendLeftViewText}>
                    {this.state.locals.recive_five_digit_pin}
                  </Text>
                </View>
                {resendRightView}
              </View>
              {/* Validate OTP button */}
              <View style={styles.validationOtpContainer}>
                <TouchableOpacity
                  style={styles.validationBtn}
                  onPress={this.otpContinue}>
                  <Text style={styles.validationBtnText}>
                    {this.state.locals.validate_pin}</Text>
                </TouchableOpacity>
              </View>
              {/* Skip validation button */}
              <View style={styles.skipViewContainer}>
                <TouchableOpacity onPress={this.otpCancel} style={styles.skipBtn}>
                  <Text style={styles.skipBtnTxt}>
                    {this.state.locals.skip_validation}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.bottomContainer}/>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const otp_number = state.mobile.otp_number;
  const language = state.lang.current_lang;

  return { state, language, otp_number };
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  containerOverlay: {
    height: Dimensions
      .get('window')
      .height
  },

  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    flex: 1,
    width: Dimensions
      .get('window')
      .width * 0.95,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    flex: 0.7
  },

  innerContainer: {
    // flex: 1,
    height: 300,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    //backgroundColor: Colors.colorGreen, //TOSO
    padding: 5,
    paddingTop: 10,
    paddingHorizontal: 10,
    marginLeft: 10,
    marginRight: 10
  },

  textViewContainer: {
    //flex: 0.7,
    height: 90,
    marginLeft: 5,
    marginRight: 5,
    ///backgroundColor: Colors.colorPureYellow
  },

  otpInputContainer: {
    //flex: 0.5,
    height: 50,
    marginLeft: 5,
    marginRight: 5,
    // backgroundColor: Colors.colorGreen
  },

  resendViewContainer: {
    //flex: 0.5,
    height: 50,
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    //backgroundColor: Colors.colorPureYellow
  },

  validationOtpContainer: {
    //flex: 0.6,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: Colors.colorGreen
  },

  skipViewContainer: {
    //flex: 0.2,
    height: 26,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: Colors.colorGreen //TODO
  },

  skipBtn: {
    flex: 1,
    padding: 10,
    paddingHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: Colors.colorGreen //TODO
  },
  skipBtnTxt: {
    textDecorationLine: 'underline',
    color: Colors.colorBlue,
    textAlign: 'center'
  },
  validationBtn: {
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Colors.colorYellow,
    height: 45,
    borderRadius: 5
  },

  validationBtnText: {
    color: Colors.colorBlack,
    margin: 5,
    backgroundColor: Colors.colorBackground
  },

  resendRightView: {
    flex: 1
  },

  resendLeftView: {
    flex: 1.8
  },

  resendRightViewBtn: {
    flex: 1
  },

  resendLeftViewText: {
    fontSize: Styles.otpEnterModalFontSize,
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 5,
    textAlign: 'left',
    fontWeight: '100'
  },

  resendRightViewText: {
    fontSize: Styles.otpEnterModalFontSize,
    margin: 5,
    textAlign: 'right',
    fontWeight: '100',
    color: Colors.yellow
  },
  text_1: {
    fontSize: Styles.otpEnterModalFontSize,
    marginBottom: 5,
    marginLeft: 5,
    marginTop: 5,
    fontWeight: '100'
  },

  text_2: {
    fontSize: Styles.otpEnterModalFontSize,
    marginLeft: 5,
    marginTop: 5,
    fontWeight: '100'
  },

  inputTextField: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    margin: 5,
    marginTop: 1
  }
});

export default connect(mapStateToProps, actions)(MobileActOtpModal);
