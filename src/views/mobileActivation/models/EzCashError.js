import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/MobileActivaton';


import imageErrorIcon from '../../../../images/common/cross.png';

class EzCashError extends Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);

    this.state = {
      openModel: false,
      locals:{
        ezCashAccountLabel: strings.ezCashAccountLabel,
        cancelButtonTxt: strings.cancelButtonTxt,
        retryButtonTxt: strings.retryButtonText,
      }
    };
  }

  onBnPressRetry = () => {
    console.log("Retry pressed");
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this
      .props
      .getEzCashPinMobileAct('');
    this
      .props
      .getEzCashPinBill('');
    this.props.focusEzCashPin(true);

  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  onBnPressRevisit = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    // navigatorOb.push({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
    console.log("Revisit pressed.");
  }

  render() {
    const  { mobile_number, error_message }  = this.props;
    return (
      <View style={styles.containerOverlay}>
        {/* <View style={styles.topContainer}/> */}
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageErrorIcon} style={styles.alertImageStyle}/>
            </View>
            <Text style={styles.workOrderNo}>{this.state.locals.ezCashAccountLabel} : {mobile_number}</Text>
            <Text style={styles.descriptionText}>{error_message}</Text>
            <View style={styles.bottomCantainerBtn}>
              {/* <View style={styles.dummyView}/> */}
              <TouchableOpacity
                onPress={() => this.onBnPressRevisit()}
                style={styles.bottomBtnCancel}>
                <Text style={styles.bottomCantainerCancelBtnTxt}>
                  {this.state.locals.cancelButtonTxt}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onBnPressRetry()}
                style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {this.state.locals.retryButtonTxt}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* <View style={styles.bottomContainer}/> */}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.modalOverlayColor,
    alignItems: 'center',
    justifyContent: 'center'
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },

  // bottomContainer: {
  //   flex: 0.5
  // },

  innerContainer: {
    flex: 0.95,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 5,
    paddingBottom: 2,
    marginTop: 5,
    // marginBottom: 5,
    marginLeft: 15,
    marginRight: 15,
    // width:350,
    // height:400
  },

  alertImageContainer: {
    alignItems: 'center',
    padding: 5
  },

  alertImageStyle: {
    width: 80,
    height: 80,
    marginTop: 5,
    marginBottom: 5
  },
  descriptionText: {
    fontSize: 16,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    fontWeight: '100'

  },
  workOrderNo: {
    alignItems: 'flex-start',
    fontSize: 18,
    marginLeft: 10,
    textAlign: 'left',
    color: Colors.colorBlack,
    marginBottom: 20,
    marginRight: 10,
    marginTop: 5,
    fontWeight: 'bold'
  },

  bottomCantainerBtn: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingRight: 5,
    paddingBottom: 10,
    marginTop: 10
  },

  bottomBtnCancel: {
    flex: 0.25,
    alignSelf: 'flex-end',

    justifyContent: 'flex-end'
  },
  bottomBtn: {
    flex: 0.25,
    marginRight: 5,
    alignSelf: 'flex-end',

    justifyContent: 'flex-end'
  },
  bottomCantainerBtnTxt: {
    fontSize: 16,
    alignSelf: 'center',
    color: Colors.colorRed
  },
  bottomCantainerCancelBtnTxt: {
    fontSize: 16,
    alignSelf: 'center',
    color: Colors.colorBlack
  }

});

export default connect(mapStateToProps, actions)(EzCashError);
