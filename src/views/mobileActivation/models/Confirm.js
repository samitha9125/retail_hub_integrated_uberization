/*
 * File: HbbActivation.js
 * Project: Dialog Sales App
 * File Created: Thursday, 16th August 2018 5:40:57 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: Friday, 17th August 2018 8:34:43 pm
 * Modified By: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */


import React from 'react';
import { View, Text, FlatList, BackHandler, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton';
import Orientation from 'react-native-orientation';
import { Header } from '../components/Header';
import Utills from '../../../utills/Utills';
import FuncUtils from '../../../utills/FuncUtils';

const Utill = new Utills();
class Confirm extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      locals: {
        confirmButtonTxt: strings.confirmButtonTxt,
        cancelButtonTxt: strings.cancelButtonTxt,
        modalTitleNotVerified: strings.modalTitle,
        modalTitleOtpVerified: strings.modalTitleOtpVerified,
        lcdValue: strings.lcdValue,
        totalPayment: strings.totalPayment,
        byclicking: strings.byclicking,
        tns: strings.tns,
        localCall: strings.localCall,
        conFee: strings.conFee,
        outstandingFee: strings.outstandingFee,
        totalPay: strings.totalPay,
        lblPrePaid: strings.prepaid,
        lblPostPaid: strings.postpaid,
        ModalHeaderTitle: strings.mobileActivationTitle
      }
    };
  }


  componentWillMount() {
    Orientation.lockToPortrait();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.dismissModal();
  
  }

  dismissModal = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  getTermAndConditionUrl = () => {
    const { tcUrls = { PREPAID: '', POSTPAID: '' } } = this.props;
    let returnUrl;
    if (this.props.pre_post == 'pre') {
      returnUrl = tcUrls.PREPAID;
    } else {
      returnUrl = tcUrls.POSTPAID;
    }
    return returnUrl;
  }

  openSignature = () => {

    if ( FuncUtils.isNullOrUndefined(this.props.tcUrls)){
      return;
    }
    console.log('Open Signature');
    this.dismissModal();
    Orientation.lockToLandscape();
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.state.customerSignature,
      screen: 'DialogRetailerApp.views.SignaturePadScreen',
      passProps: {
        tncUrl: this.getTermAndConditionUrl(),
        confirmModal: ()=>this.dismissModal()
      }
    });
  }

  checkNumber = (number) => {
    console.log('xxx checkNumber :: checkNumber : ', number);
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00");
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => {
            this.dismissModal();
            Orientation.lockToPortrait();
          }}>
          <Text style={styles.ConfirmAndCancelTextStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => {
            this.openSignature();
          }}>
          <Text style={styles.ConfirmAndCancelTextStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  getLocalizedPaymentPlanString(paymentPlan){
    if (paymentPlan == 'pre'){
      return this.state.locals.lblPrePaid;
    }
    return this.state.locals.lblPostPaid;
  }

  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value} />)
  renderPaymentListItem = ({ item }) => (<ElementPaymentItem data={item.data} value={item.value} />)
  renderTotalPaymentItem = ({ item }) => (<ElementTotalPaymentItem data={item.data} value={item.value} />)

  render() {

    //array for Hbb confirmation Left, right Columns data
    let finalPakageDetails = [
      {
        data: this.props.local_foreign == 'local'
          ? strings.nicNo
          : strings.ppNo,
        value: this.props.id_number
      }, {
        data: strings.conn_Type,
        value: this.getLocalizedPaymentPlanString(this.props.pre_post)
      }, {
        data: strings.connectionNo,
        value: this.props.mobile_number
      }, {
        data: strings.simNo,
        value: this.props.sim_number
      }
    ];
  
    let paymentDataArray = [];
    let totalPaymentData = [];
    if (this.props.pre_post == 'pre') {
  
      if (this.props.alternate_contact_number){
        finalPakageDetails.push({
          data: strings.alternateContact,
          value: this.props.alternate_contact_number
        });
      }
      if (this.props.first_reload){
        paymentDataArray.push({
          data: strings.firstRel,
          value: Utill.numberWithCommas(this.checkNumber(this.props.first_reload))
        });
      } 
  
    } else if (this.props.pre_post == 'post') {
      finalPakageDetails.push(
        {
          data: strings.offer,
          value: this.props.selectedPackage.pkg_CODE
        },{
          data: strings.package,
          value: this.props.selectedPackage.pkg_NAME
        }, {
          data: strings.rental,
          value: 'Rs ' + Utill.numberWithCommas(this.checkNumber(this.props.selectedPackage.pkg_RENTAL))
        }
      );
      if (this.props.alternate_contact_number){
        finalPakageDetails.push({
          data: strings.otherContactNo,
          value: this.props.alternate_contact_number
        });
      } 
  
      paymentDataArray.push({
        data: this.state.locals.localCall,
        value: Utill.numberWithCommas(this.checkNumber(this.props.paymentInfo.localCallValue))
      }, {
        data: this.state.locals.conFee,
        value: Utill.numberWithCommas(this.checkNumber(this.props.paymentInfo.conFeeValue))
      } );

      if (this.props.needToShowoutstanding) {
        paymentDataArray.push({
          data: this.state.locals.outstandingFee,
          value: Utill.numberWithCommas(this.checkNumber(this.props.paymentInfo.outstandingFeeValue))
        });
      }
    
      totalPaymentData.push({
        data: this.state.locals.totalPay,
        value: Utill.numberWithCommas(this.checkNumber(this.props.paymentInfo.totalPayValue))
      });
    }

    return (
      <View style={styles.containerOverlay}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.ModalHeaderTitle} />
        <ScrollView
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode='on-drag'>
          <View style={styles.containerStyle}>
            <View style={styles.section_1_Container}>
              <Text style={styles.titleStyle}>{strings.modalTitle}</Text>
            </View>
            <View style={styles.section_2_Container}>
              <FlatList
                data={finalPakageDetails}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false}
              />
            </View>
            <View style={styles.section_2_1_Container} />
            <View style={styles.section_3_Container}>
              <FlatList
                data={paymentDataArray}
                renderItem={this.renderPaymentListItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false} />
              <FlatList
                data={totalPaymentData}
                renderItem={this.renderTotalPaymentItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false} />

            </View>
            <View style={styles.section_4_Container}>
              {this.buttonSet()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftItem}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightItem}>
      <Text style={styles.textStyleRight}>{value}</Text>
    </View>
  </View>
);

const ElementPaymentItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftView}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>

      <View style={styles.elementRightInnerView1}>
        <Text style={styles.textStyleRight}>Rs </Text>
      </View>
      <View style={styles.elementRightInnerView2}>
        <Text style={styles.textStyleRight}>{value}</Text>
      </View>
      <View style={styles.elementRightInnerView3}></View>
    </View>
  </View>
);

const ElementTotalPaymentItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftView}>
      <Text style={[styles.textStyleForTotalPayment, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>

      <View style={styles.elementRightInnerView1}>
        <Text style={styles.textStyleForTotalPaymentRight}>Rs </Text>
      </View>
      <View style={styles.elementRightInnerView2}>
        <Text style={styles.textStyleForTotalPaymentRight}>{value}</Text>
      </View>
      <View style={styles.elementRightInnerView3}></View>
    </View>
  </View>
);

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.white
  },
  elementRightInnerView: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row'

  },
  elementRightInnerView1: {
    flex: 0.2,
    alignItems: 'flex-start',
  },
  elementRightInnerView2: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  elementRightInnerView3: {
    flex: 0.3,
    alignItems: 'flex-start',
  },
  elementRightInnerViewsub: {
    flex: 0.75,
    flexDirection: 'row'
  },
  elementRightInnerViewsub1: {
    flex: 0.25,
    flexDirection: 'row'
  },

  elementLeftView: {

    flex: 1
  },
  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'

  },
  elementRightView: {
    flex: 1,
    flexDirection: 'row'

  },
  elementRightView1: {
    flex: 1,
    alignItems: 'flex-start'

  },

  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    flex: 1.5
  },
  section_2_Container: {
    flex: 5,
    marginTop: 20,
  },
  elementLeftItem: {
    flex: 1,

  },
  elementRightItem: {
    flex: 1,
  },
  section_2_1_Container: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.colorTransparent,
    marginBottom: 10,
    marginTop: 10
  },
  section_3_Container: {
    flex: 3,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1,
    marginTop: 48,
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },
  textStyleForTotalPayment:{
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    fontWeight: 'bold'
  },
  textStyleForTotalPaymentRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'

  },
  ConfirmAndCancelTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },

  textStyleRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },
  titleStyle: {
    fontSize: 18,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  confirmButtonStyle: {
    backgroundColor: '#FFC400',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginRight: 20,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10

  },
  cancelButtonStyle: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginLeft: 5,
    marginRight: 10,
    padding: 10

  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION', state.mobile);
  // console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION',
  // JSON.stringify(state.mobile));
  
  const id_number = state.mobile.id_number;
  const sim_number = state.mobile.sim_number;
  const mobile_number = state.mobile.mobile_number;
  const information_id_type = state.mobile.information_id_type;
  const alternate_contact_number = state.mobile.alternate_contact_number;
  const local_foreign = state.mobile.local_foreign;
  const language = state.lang.current_lang;
  const selectedPackage = state.mobile.selectedPackage;
  const packageDeposits = state.mobile.packageDeposits;
  const total_payment = state.mobile.total_payment;
  const connection_fee = state.mobile.selectedPackage.connection_fee;
  const depositAmount = state.mobile.packageDeposits.depositAmount;
  const paymentInfo = state.mobile.paymentInfo;
  const pre_post = state.mobile.pre_post;
  const first_reload = state.mobile.first_reload;
  const tcUrls = FuncUtils.getValueSafe(state.auth.getConfiguration).agreement_document;
  const needToShowoutstanding = paymentInfo.outstandingFeeValue !== undefined
  && paymentInfo.outstandingFeeValue !== null
  && !isNaN(paymentInfo.outstandingFeeValue)
  && paymentInfo.outstandingFeeValue !== ""
  && paymentInfo.outstandingFeeValue !== "0.00";
  
  return {
    language,
    id_number,
    local_foreign,
    mobile_number,
    information_id_type,
    alternate_contact_number,
    sim_number,
    total_payment,
    selectedPackage,
    packageDeposits,
    connection_fee,
    depositAmount,
    paymentInfo,
    needToShowoutstanding,
    pre_post,
    first_reload,
    tcUrls
  };
};

export default connect(mapStateToProps, actions)(Confirm);