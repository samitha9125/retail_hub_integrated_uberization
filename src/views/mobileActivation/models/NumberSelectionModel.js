import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
  Picker,
} from 'react-native';
import { connect } from 'react-redux';
import RadioForm from 'react-native-simple-radio-button';
import * as actions from '../../../actions';

import refreshIcon from '../../../../images/common/refresh_icon.png';
import strings from '../../../Language/MobileActivaton.js';
import Utills from '../../../utills/Utills';
import Orientation from 'react-native-orientation';

const Utill = new Utills();

class NumberSelectionModel extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      num_selection_option: '',
      numSelectionData: [],
      selectedNumber: '',
      blocked_id_second: '',
      maxLength: 10,
      numberSelection: strings.numberSelection,
      numberRandom: strings.numberRandom,
      numberSearch: strings.numberSearch,
      numberInclude: strings.numberInclude,
      numberStart: strings.numberStart,
      numberEnd: strings.numberEnd,
      numberCancel: strings.numberCancel,
      pre_post: 'pre'
    };

    this.searchValue = '';
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();

    const num_list = `${this.props.mobile_number}|P;`;

    const releaseData = {
      flag: 'second',
      blocked_id_second: '',
      num_list,
      blocked_id_first: this.props.blocked_id_first,
      selected_number: ''
    };

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('knownRelease', 'gsmConnection', 'ccapp', releaseData, this.firstReleaseNumberSuccess, this.firstReleaseNumberFail, this.firstReleaseNumberEx);
    } else {
      Utill.apiRequestPost('knownRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.firstReleaseNumberSuccess, this.firstReleaseNumberFail, this.firstReleaseNumberEx);
    }
  }

  onPressNumber = (number) => {
    console.log('onPressNumber', number);
    this
      .props
      .getMobileNumberMobileAct(number);

    let num_list = '';

    // for (let i = 0; i < this.state.numSelectionData.length; i++) {   if
    // (this.state.numSelectionData[i].value !== number) {     num_list =
    // `${num_list + this.state.numSelectionData[i].value}|P;`;   } }
    num_list = `${number}|P;`;

    const releaseData = {
      // flag: 'second',
      blocked_id_second: this.state.blocked_id_second,
      num_list,
      blocked_id_first: this.props.blocked_id_first,
      selected_number: number
    };


    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail, this.releaseEx);
    } else {
      Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail, this.releaseEx);
    }  
  }

  onBtnPressCancel = () => {
    
    this
      .props
      .navigator
      .dismissLightBox();
    
    let num_list = '';

    num_list = `${this.props.mobile_number}|P;`;

    const releaseData = {
      // flag: 'second',
      blocked_id_second: this.state.blocked_id_second,
      num_list,
      blocked_id_first: this.props.blocked_id_first,
      selected_number: ''
    };


    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseCancelSuccess, this.releaseCancelFail, this.releaseCancelEx);
    } else {
      Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseCancelSuccess, this.releaseCancelFail, this.releaseCancelEx);
    }  


    // num_list = `${this.props.mobile_number}|P;`; const releaseData = {   // flag:
    // 'second',   blocked_id_second: this.state.blocked_id_second,   num_list,
    // blocked_id_first: this.props.blocked_id_first,   selected_number: '' };
    // Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp',
    // releaseData, this.releaseNumberSuccess, this.releaseNumberFail,
    // this.releaseEx);
  }

  releaseCancelSuccess = (response) => {
    console.log('N_P releaseCancelSuccess', response);
    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }
  }

  releaseCancelFail = (response) => {
    console.log('N_P releaseCancelFail', response);
    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }
  }

  releaseCancelEx = (error) => {
    console.log('N_P releaseCancelEx', error);
    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }
  }

  reserveNumberFailure = (response) => {
    console.log('N_P reserveNumberFailure', response);
    this
      .props
      .navigator
      .dismissLightBox();
  }

  reserveNumberEx = (error) => {
    console.log('N_P reserveNumberEx', error);
    this
      .props
      .navigator
      .dismissLightBox();
  }

  reserveNumberSuccess = (response) => {
    console.log('N_P reserveNumberSuccess', response);
    this
      .props
      .getApiLoading(false); //should change this to true
    this
      .props
      .getMobileNumberMobileAct(response.data.data[0].value);
    let num_list = '';

    // for (let i = 1; i < response.data.data.length; i++) {     num_list =
    // `${num_list + response.data.data[i].value}|P;`; }

    num_list = `${response.data.data[0].value}|P;`;

    this
      .props
      .getBlockedIdMobileAct(response.data.blockedId);

    const releaseData = {
      flag: 'first',
      blocked_id_first: response.data.blockedId,
      num_list
    };


    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail);
    } else {
      Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail);
    }
  }

  onBtnPressContinue = () => {
    this
      .props
      .navigator
      .dismissLightBox();
  }

  releaseNumberSuccess = (response) => {
    console.log('N_P releaseNumberSuccess', response);
    this
      .props
      .getBlockedIdMobileAct(response.data.blocked_id);
    this
      .props
      .navigator
      .dismissLightBox();
  }

  releaseNumberFail = (response) => {
    console.log('N_P releaseNumberFail', response);
    this.props.getMobileNumberMobileAct('');
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });


    // if (this.props.pre_post == 'pre'){
    //   Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    // } else {
    //   Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    // } // TODO

     
    // this
    //   .props
    //   .navigator
    //   .dismissLightBox(); // TODO by Manoj


    // this   .props   .getSimNumberMobileAct(''); this   .props
    // .getMobileNumberMobileAct(''); this   .props   .getFirstReloadMobileAct('');
    // this   .props   .resetMobileActNicValidation(); this   .props
    // .resetMobileActModelPopped(); this   .props   .resetValidateSimNumber(); this
    //   .props   .getSkipValidationMobileAct(false);
  }

  releaseEx = (error) => {
    console.log('N_P releaseEx', error);
    this.props.getMobileNumberMobileAct('');
    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }
    
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
  }

  searchTextChange = (value) => {
    console.log('N_P searchTextChange', value);
    this.searchValue = value;

    //

    if (value.length == 0) {
      this.searchValueChange('random', 0);
    }
    if (this.state.num_selection_option == 'search' && (value.length == 9)) {

      let num_list = '';

      num_list = `${this.props.mobile_number}|P;`;
  
      const releaseData = {
        // flag: 'second',
        blocked_id_second: this.state.blocked_id_second,
        num_list,
        blocked_id_first: this.props.blocked_id_first,
        selected_number: ''
      };
  

      if (this.props.pre_post == 'pre'){
        Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.searchReleaseSuccess, this.searchReleaseFailure, this.searchReleaseEx);
      } else {
        Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.searchReleaseSuccess, this.searchReleaseFailure, this.searchReleaseEx);
      }

  
    } else if (this.state.num_selection_option !== 'search' && value.length == 4) {
     
      let num_list = '';

      num_list = `${this.props.mobile_number}|P;`;
  
      const releaseData = {
        // flag: 'second',
        blocked_id_second: this.state.blocked_id_second,
        num_list,
        blocked_id_first: this.props.blocked_id_first,
        selected_number: ''
      };
  

      if (this.props.pre_post == 'pre'){
        Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.searchReleaseSuccess, this.searchReleaseFailure, this.searchReleaseEx);
      } else {
        Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.searchReleaseSuccess, this.searchReleaseFailure, this.searchReleaseEx);
      }
    }
  }

  searchReleaseSuccess = () => {
    console.log('N_P searchReleaseSuccess');
    console.log('COMMING TO SUCCES CALLBACK');
    const data = {
      search_type: this.state.num_selection_option,
      search_val: this.searchValue
    };


    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    } else {
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmPostpaidConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    }  
  }

  searchReleaseFailure = (value) => {
    console.log('N_P searchReleaseFailure', value);
    const data = {
      search_type: this.state.num_selection_option,
      search_val: this.searchValue
    };

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    } else {
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmPostpaidConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    }
  }

  searchReleaseEx = (value) => {
    console.log('N_P searchReleaseEx', value);
    const data = {
      search_type: this.state.num_selection_option,
      search_val: this.searchValue
    };

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    } else {
      Utill.apiRequestPost('reserveMobileNoSearch', 'gsmPostpaidConnection', 'ccapp', data, this.addData, this.searchFailure, this.searchEx);
    }
  }

  searchValueChange = (itemValue, index) => {
    console.log('N_P searchValueChange', index, itemValue);
    this.setState({ num_selection_option: itemValue });

    if (itemValue == 'random') {

      if (this.props.pre_post == 'pre'){
        Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.addData, this.reserveFailure, this.reserveEx);
      } else {
        Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.addData, this.reserveFailure, this.reserveEx);
      }

    } else if (itemValue == 'search') {
      this.setState({ maxLength: 9 });
    } else if (itemValue == 'include' || itemValue == 'startswith' || itemValue == 'endswith') {
      this.setState({ maxLength: 4 });
    }
  }

  onBtnPressRefresh = () => {
    console.log('N_P onBtnPressRefresh');
    this.setState({ num_selection_option: 'random' });
    let num_list = '';

    // for (let i = 0; i < this.state.numSelectionData.length; i++) {     num_list =
    // `${num_list + this.state.numSelectionData[i].value}|P;`; }

    num_list = `${this.props.mobile_number}|P;`;

    const releaseData = {
      flag: 'second',
      blocked_id_second: this.state.blocked_id_second,
      num_list,
      blocked_id_first: this.props.blocked_id_first,
      selected_number: ''
    };

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('numberPoolRelease', 'gsmConnection', 'ccapp', releaseData, this.releaseNumberSuccessRefresh, this.releaseNumberFailRefresh, this.releaseExRefresh);
    } else {
      Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseNumberSuccessRefresh, this.releaseNumberFailRefresh, this.releaseExRefresh);
    }
  }

  releaseNumberSuccessRefresh = (response) => {
    console.log('N_P releaseNumberSuccessRefresh', response);
    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', {}, this.addData, this.reserveFailure, this.reserveEx);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.addData, this.reserveFailure, this.reserveEx);
    }
  }

  releaseNumberFailRefresh = (response) => {
    console.log('N_P releaseNumberFailRefresh', response);
    this.setState({ numSelectionData: [] });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
  }

  releaseExRefresh = (error) => {
    console.log('N_P releaseExRefresh', error);
    this.setState({ numSelectionData: [] });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
  }

  addData = (response) => {
    this.setState({ numSelectionData: response.data.data });
    this.setState({ blocked_id_second: response.data.blockedId });
  }

  reserveFailure = (response) => {
    console.log('N_P reserveFailure', response);
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
  }

  reserveEx = (error) => {
    console.log('N_P reserveEx', error);
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
  }

  searchFailure = (response) => {
    console.log('N_P searchFailure', response);
    this.setState({ numSelectionData: [] });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
  }

  searchEx = (error) => {
    console.log('N_P searchEx', error);
    this.setState({ numSelectionData: [] });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
  }

  firstReleaseNumberSuccess = (response) => {
    console.log('N_P firstReleaseNumberSuccess', response);
    const data = {};

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', data, this.addData);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', data, this.addData);
    }
  }

  firstReleaseNumberFail = (response) => {
    console.log('N_P firstReleaseNumberFail', response);
    const data = {};

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', data, this.addData);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', data, this.addData);
    }
  }

  firstReleaseNumberEx = (error) => {
    console.log('N_P firstReleaseNumberEx', error);
    const data = {};

    if (this.props.pre_post == 'pre'){
      Utill.apiRequestPost('reserveMobileNo', 'gsmConnection', 'ccapp', data, this.addData);
    } else {
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', data, this.addData);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.modelContainer}>
          <View>
            <Text style={styles.modelTitleTxt}>{ this.state.numberSelection }</Text>
          </View>
          <View style={styles.refreshIconContainer}>
            <TouchableOpacity
              style={styles.refreshIconInnerContainer}
              onPress={this.onBtnPressRefresh}
              activeOpacity={1}
            >
              <Image source={refreshIcon} style={styles.refreshIconStyle} />
            </TouchableOpacity>
          </View>
          <Picker
            selectedValue={this.state.num_selection_option}
            mode={'dropdown'}
            style={styles.pickerStyle}
            onValueChange={(itemValue, index) => this.searchValueChange(itemValue, index)}
          >
            <Picker.Item label={ this.state.numberRandom } value="random" />
            <Picker.Item label={ this.state.numberSearch } value="search" />
            <Picker.Item label={ this.state.numberInclude } value="include" />
            <Picker.Item label={ this.state.numberStart } value="startswith" />
            <Picker.Item label={ this.state.numberEnd } value="endswith" />
          </Picker>

        </View>
        {this.state.num_selection_option === 'search' || this.state.num_selection_option === 'include' || this.state.num_selection_option === 'startswith' || this.state.num_selection_option === 'endswith'
          ? <View>
            <TextInput
              style={styles.numberSearchInput}
              placeholder={this.state.num_selection_option === 'search'
                ? 'Enter Mobile Number'
                : 'Enter 4 digits'}
              onChangeText={(value) => this.searchTextChange(value)}
              maxLength={this.state.maxLength}
              keyboardType={'numeric'} 
            />
          </View>
          : <View />
        }

        <View style={styles.radioFormContainer}>
          <View style={styles.radioFormInnerContainer}>

            <RadioForm
              radio_props={this.state.numSelectionData}
              initial={-1}
              onPress={this.onPressNumber}
              buttonColor={'#939393'}
              selectedButtonColor={'#FFC400'}
              animation
              buttonSize={10}
              buttonOuterSize={20}
              buttonStyle={{
                color: 'red'
              }}
              labelStyle={styles.radioFormLabelStyle} 
            />

          </View>
        </View>
        <View>
          <View style={styles.bottomContainer}>
            <TouchableOpacity
              style={styles.cancelBtnContainer}
              onPress={this
                .onBtnPressCancel
                .bind(this)}
              activeOpacity={1}
            >
              <Text style={styles.cancelBtnTxt}>{ this.state.numberCancel }</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={styles.continueBtnContainer}
              onPress={this.onBtnPressContinue.bind(this)}
              activeOpacity={1}
            >
              <Text style={styles.continueBtnTxt}>CONTINUE</Text>
            </TouchableOpacity> */}

          </View>
        </View>

      </View>

    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: NUMBER SELECTION => MOBILE ', state.mobile);
  const blocked_id_first = state.mobile.blocked_id_first;
  const mobile_number = state.mobile.mobile_number;
  const Language = state.lang.current_lang;

  const pre_post = state.mobile.pre_post;

  console.log('NumberSelection PRE_POST STATE: ',pre_post);

  return { blocked_id_first, mobile_number, Language , pre_post };
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions
      .get('window')
      .width * 0.94,
    height: Dimensions
      .get('window')
      .height * 0.8,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 16,
    marginTop: 25
  },
  modelContainer: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    borderWidth: 0
  },

  modelTitleTxt: {
    fontSize: 16,
    marginTop: 10,
    fontWeight: '100',
    borderWidth: 0
  },

  refreshIconContainer: {
    width: 37,
    height: 37,
    borderWidth: 0,
    marginTop: 5,
    marginLeft: 5
  },

  refreshIconInnerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: '#ffc400', height: '40%',
    borderRadius: 5,
    // width: '15%',
    marginLeft: 10,
    borderWidth: 0,
    flex: 1

  },
  refreshIconStyle: {
    width: 37,
    height: 37
  },

  pickerStyle: {
    width: '100%',
    margin: 0,
    padding: 0,
    marginLeft: 3,
    marginRight: 0,
    flex: 1
  },

  numberSearchInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#DDD'
  },

  radioFormContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    height: 310
  },

  radioFormInnerContainer: {
    paddingTop: 10,
    borderWidth: 0,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    alignItems: 'flex-start',
    paddingLeft: 10
  },

  radioFormLabelStyle: {
    fontSize: 17,
    color: '#1B1B1B',
    lineHeight: 25,
    marginLeft: 0
  },
  bottomContainer: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    paddingRight: 5,
    paddingBottom: 30,
    flexDirection: 'row'
  },
  cancelBtnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    width: '40%',
    marginLeft: 10
  },

  continueBtnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffc400',
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    width: '40%',
    marginLeft: 10
  },
  continueBtnTxt: {
    color: '#000',
    backgroundColor: 'transparent'
  },
  cancelBtnTxt: {
    color: '#000',
    backgroundColor: 'transparent'
  }
});

export default connect(mapStateToProps, actions)(NumberSelectionModel);
