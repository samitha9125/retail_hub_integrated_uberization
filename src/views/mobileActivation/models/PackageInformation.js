import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import * as actions from '../../../actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton.js';
import Orientation from 'react-native-orientation';

class PackageInformation extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      dropDownData: [],
      information_id_type: '',
      pack: { 
        pkg_CODE: '', 
        pkg_NAME: '', 
        pkg_RENTAL: '0',
        connection_fee: '',
        deposit_AMOUNT: '',
        package_info: []
      },
      package: [],
      packageDeposits: { depositType: "LCD", depositAmount: "Local Call Deposit" },
      userSelectedPackageIndex: -1,
      locals: {
        titlePackageInfo: strings.packageInfo,
        continue: strings.continue
      },
      selectedPackage:{ 
        pkg_CODE: '', 
        pkg_NAME: '', 
        pkg_RENTAL: '0',
        connection_fee: '',
        deposit_AMOUNT: '',
        package_info: []
      }
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait();
    // var elegiblePackageNames = [];
    // response.data.info.forEach((item) => {
    //   elegiblePackageNames.push(item.pkg_NAME);
    // });

    // this.setState({ dropDownData : elegiblePackageNames });
    // this.setState({ eligiblePackages : response.data.info });
    console.log("Package information Props: ", this.props.packageData.message);
    console.log("Currently selected package: ", this.props.packageData.selectedPackage);
    
    const currentSelectedPackage = this.props.packageData.selectedPackage;


    this.setPackageInfo(currentSelectedPackage);

    var elegiblePackageNames = [];

    this.props.packageData.message.forEach((item) => {
      elegiblePackageNames.push(item.pkg_NAME);
    });

    this.setState({ dropDownData : elegiblePackageNames });


  }

  outstandingContinue = () => {
    this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  
  
  setPackageInfo(selectedPackage){
    var selectedPackageInfo = this.props.pack.package_info;

    this.setState({ package: selectedPackageInfo });

    this.setState({ pack: selectedPackage });

    console.log('%c\npackage: '+ selectedPackageInfo + '\npack: ' + JSON.stringify(selectedPackage), 'background: green; color: white; display: block;');

  }

  updatePackage = (idx, value) => {
    this
      .props
      .getSelectedPackageIndex(idx);

    const selectedPackage = this.props.packageData.message[idx];

    console.log("selected package index: ", idx);
    console.log("selected package index: ", value);
    console.log("selected package: ", selectedPackage);

    const depositParams = {
      package_code : selectedPackage.pkg_CODE,
      customer_type : this.props.local_foreign,
      identification_type : this.props.information_id_type
    };

    this
      .props
      .getPackageDeposits(depositParams);

    this
      .props
      .getSelectedPostpaidPackage(selectedPackage);

    this.setPackageInfo(selectedPackage);
  }

  buttonSet = () => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
        <TouchableOpacity onPress={() => this.outstandingContinue()} style={{ backgroundColor: '#edc92c', alignSelf: 'stretch', borderRadius: 5, marginLeft: 5, padding: 10 }}>
          <Text style={styles.textStyle}>
            {this.state.locals.continue}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderListItem = ({ item }) => (
    <ElementItem data={item} />
  )

  render() {
    const finalPakageDetails = this.props.pack.package_info[0];
    console.log("finalPakageDetails: ", JSON.stringify(finalPakageDetails));
    return (

      <View style={styles.containerOverlay}>
        <View style={styles.containerStyle}>
          <View style={styles.titleViewStyle}>
            <Text style={styles.titleStyle}>{this.state.locals.titlePackageInfo}</Text>
          </View>
          <View style={styles.modalDropdownView}>
            <ModalDropdown 
              options={this.state.dropDownData} 
              defaultIndex={ this.props.userSelectedPackageIndex } 
              onSelect={(idx, value) => this.updatePackage(idx, value)} 
              style={{ width: Dimensions
                .get('window')
                .width * 0.80 }} 
              dropdownStyle={{ width: Dimensions.get('window').width * 0.80-2, marginRight: 50, position: 'absolute' }} 
              dropdownTextStyle={{ color: '#000', fontSize: 15 }} 
              dropdownTextHighlightStyle={{ fontWeight: 'bold' }} >

              <View style={{ flexDirection: 'row', height: '100%', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex:5, alignSelf: 'flex-start', justifyContent: 'center' }}>
                  <Text style={{ flex:1, paddingLeft:7, color: '#000', fontSize: 15 }}>{this.props.pack.pkg_NAME}</Text>
                </View>
                <View style={{ flex:1, alignItems: 'center', justifyContent: 'center'  }}>
                  <Ionicons name='md-arrow-dropdown' size={20} />
                </View>
              </View>
            </ModalDropdown>

          </View>
          {/* Package Info List */}
          <View style={styles.packageInfoContainer}>
            <FlatList
              data={finalPakageDetails}
              renderItem={this.renderListItem}
              extraData={this.state.package[0]}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={{ height: 100 }}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ data }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elimentViewStyle}>
      <Icon name="circle" style={styles.bulletIcon} />
    </View>
    <View style={styles.elimentTextStyle}>
      <Text style={styles.textStyle}>{data}</Text>
    </View>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: PACKAGE SELECTION => MOBILE ACTIVATION', state.mobile);
  console.log("Selected package Info: \n", state.mobile.selectedPackage);

  const language = state.lang.current_lang;
  const pack = state.mobile.selectedPackage; 
  const information_id_type = state.mobile.information_id_type;
  const local_foreign = state.mobile.local_foreign;
  const apiResponse = state.mobile.api_response;
  const packageDeposits = state.mobile.packageDeposits;
  const userSelectedPackageIndex = state.mobile.userSelectedPackageIndex;

  return { language, apiResponse, pack, information_id_type, packageDeposits, userSelectedPackageIndex, local_foreign };
};

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingRight: 20,
    paddingLeft: 20,
    marginLeft: 10,
    width: Dimensions
      .get('window')
      .width * 0.90,
    height: Dimensions
      .get('window')
      .height * 0.80,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    borderRadius: 5,
    padding: 16,
    marginTop: 25,
    // backgroundColor: 'blue'
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%'
  },
  textStyle: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
  },
  titleViewStyle: {
    flex: 1
  },
  titleStyle: {
    fontSize: 24,
    color: Colors.colorBlack,
  },
  modalDropdownView: {
    marginTop:10,
    paddingTop:5,
    flex:2,
    borderColor: '#E9E8E8',
    borderWidth: 1,
    // backgroundColor: 'red',
    flexDirection: 'row',
    width: Dimensions
      .get('window')
      .width * 0.80,
  },
  pickerItem: {
    fontSize: 15
  },
  picker: {
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    height: '100%'
  },

  elimentViewStyle: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  bulletIcon: {
    color: Colors.colorBlack,
  },
  elimentTextStyle: {
    flex:10,
    justifyContent: 'flex-start',
  },
  packageInfoContainer:{ 
    flex:10,
    paddingTop: 10 
  }
};

export default connect(mapStateToProps, actions)(PackageInformation);