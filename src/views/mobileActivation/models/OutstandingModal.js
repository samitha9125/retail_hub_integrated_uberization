import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton.js';
import imageInfoAlert from '../../../../images/icons/alert.png';
import Utills from '../../../utills/Utills';
import Orientation from 'react-native-orientation';
import icon_mobile_src from '../../../../images/common/icons/icon_mobile_black.png';
import icon_dtv_src from '../../../../images/common/icons/icon_dtv_black.png';
import icon_lte_src from '../../../../images/common/icons/icon_lte_black.png';

/**
 * Curently using for both prepaid and postpaid
 *
 * 'DialogRetailerApp.models.OutstandingModal'
 */
const HEIGHT = 40;
const Utill = new Utills();

class OutstandingModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      nic: this.props.id_number,
      para: this.props.outstandingData[this.props.outstandingData.length - 1].totalOsAmount,
      package: [],
      information_id_type: '',
      locals: {
        passportUC: strings.passportUC,
        nicUC: strings.nicUC,
        last: strings.last,
        confirmText: strings.confirmText,
        continueButtonText: strings.continueButtonText,
        cancelButtonTxt: strings.cancelButtonTxt,
        outstandingLeftText: strings.outstandingLeftText,
        outstandingRightText: strings.outstandingRightText
      }
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }
  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = icon_mobile_src;
        break;
      case 'DTV':
        icon = icon_dtv_src;
        break;
      case 'LTE':
        icon = icon_lte_src;
        break;       
      
    }

    return icon;
  }

  outstandingContinue = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    if (this.props.notificationNumberListArray && this.props.isDocumnetExistInDataScan) {
      setTimeout(function () {
        navigatorOb.showModal({ screen: 'DialogRetailerApp.models.GeneralModelMobileActPostPaid', title: 'GeneralModelMobileActPostPaid', overrideBackPress: true });
        // navigatorOb.showLightBox({   screen:
        // 'DialogRetailerApp.models.GeneralModelMobileActPostPaid',   passProps: {
        // title: 'Success',     onClose: 'this.dismissLightBox'   }, overrideBackPress:
        // true,   style: {     backgroundBlur: 'dark', backgroundColor: 'rgba(0, 0, 0,
        // 0.7)'   },   adjustSoftInput: 'resize', tapBackgroundToDismiss: false });
      }, 100);
    }

  }

  outstandingCancel = () => {
    Alert.alert('Confirm', this.state.locals.confirmText, [
      {
        text: 'Yes',
        onPress: () => this.onConfirmYes()
      }, {
        text: 'No',
        onPress: () => console.log('No Pressed'),
        style: 'cancel'
      }
    ], { cancelable: false });

  }

  onConfirmYes = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this;
    setTimeout(() => {
      me
        .props
        .resetMobileActivationState();
    }, 50);

  }

  buttonSet = () => {
    return (
      <View style={styles.buttonInnerContainer}>
        <TouchableOpacity
          onPress={() => this.outstandingCancel()}
          style={styles.buttonCancelStyle}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.outstandingContinue()}
          style={styles.buttonContinueStyle}>
          <Text style={styles.textStyle}>
            {this.state.locals.continueButtonText}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderListItem = ({ item }) => (<ElementItem
    data={(item.display_name)}
    value={item.outstandingAmount}
    lob={item.lob}
    screen={this}/>)

  render() {
    console.log("OUTSTANDING DATA FROM MODAL", this.props.outstandingData);
    const information_id_type = (this.props.information_id_type == 'PP'
      ? this.state.locals.passportUC
      : this.state.locals.nicUC);

    let outstandingData = [];
    outstandingData = [...this.props.outstandingData];
    outstandingData.splice(-1, 1); // This will remove the last element of the outstandingData array because the last element contains the total outstanding amount and has no use to the FlatList.

    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer} >
          <View style={styles.innerContainer}>
            <View style={styles.imageContainerStyle}>
              <Image source={imageInfoAlert} style={styles.imageStyle}/>
            </View>
            <View style={styles.nicContainer}>
              <Text style={[styles.textStyle, styles.nixText]}>{information_id_type}
                : {this.state.nic}</Text>
            </View>
            <View style={styles.outstandingTextContainer}>
              <Text style={styles.textStyle}>
                {this.state.locals.outstandingLeftText}
              </Text>
              <Text style={styles.textStyle}>
                <Text style={styles.priceStyle}>{' Rs ' + this.state.para + ' '}</Text>
                {this.state.locals.outstandingRightText}
              </Text>
            </View>
            <View style={[ styles.outstandingListContainer,  outstandingData.length < 4 ? { height: HEIGHT * outstandingData.length } : {flex: 2}]}>
              <ScrollView>
                <FlatList
                  data={outstandingData}
                  renderItem={this.renderListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={this.state.scrollEnabled}/>
              </ScrollView>
            </View>
            <View style={styles.bottomTextContainer}>
              <Text style={styles.textStyle}>{this.state.locals.last}</Text>
            </View>
            <View style={styles.buttonContainer}>
              {this.buttonSet()}
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const ElementItem = ({ data, value,screen,lob }) => (
  <View style={styles.elementContainor}>
   <View style={styles.elementLeftLobIcon}>
          <Image source={screen.getLobIconSrc(lob)} style={styles.imageIconStyle}/>
          {/* <Image source={icon_mobile_src} style={styles.imageIconStyle}/> */}
        </View>
    <View style={styles.elementLeftView}>
      <Text style={styles.elementLeftText}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>
      <View style={styles.elementRightInnerView}>
        <Text style={styles.elementRightText}>Rs. {value}</Text>
      </View>
    </View>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: SAMPAL_MODAL => MOBILE ACTIVATION', state.mobile);
  console.log('****** REDUX STATE :: SAMPAL_MODAL => MOBILE ACTIVATION', JSON.stringify(state.mobile));

  const language = state.lang.current_lang;
  const apiResponse = state.mobile.api_response;
  const id_number = state.mobile.id_number;
  const information_id_type = state.mobile.information_id_type;
  let notificationNumberListArray = false;
  let isDocumnetExistInDataScan = false;

  if (state.mobile.nic_validation !== null && state.mobile.nic_validation.notificationNumberList != undefined && state.mobile.nic_validation.documnetExistInDataScan !== null && state.mobile.nic_validation.notificationNumberList !== null) {
    console.log('xxx OTP NUMBERS TEST');
    notificationNumberListArray = state.mobile.nic_validation.notificationNumberList.length !== 0;
    isDocumnetExistInDataScan = state.mobile.nic_validation.documnetExistInDataScan === 'Y';
  }

  return {
    language,
    notificationNumberListArray,
    isDocumnetExistInDataScan,
    id_number,
    apiResponse,
    information_id_type
  };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.5
  },
  modalContainer: {
    flex: 7,
    // flexGrow: 6
  },
  bottomContainer: {
    flex: 0.5
  },
  innerContainer: {
    flex: 1,
    padding: 10,
    paddingRight: 15,
    paddingLeft: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  nicContainer: {
    flex: 1,
    marginTop: 5,
    padding: 5
  },
  nixText: {
    fontWeight: 'bold',
    fontSize: 19
  },
  elementLeftLobIcon: {
    flex: 1

  },

  imageIconStyle: {
    width: 22,
    height: 22,
    margin: 5
  },


  outstandingTextContainer: {
    // flex: 1.5,
    padding: 15,
    marginBottom: 10
  },
  outstandingListContainer: {
    // flex: 2,
    // marginTop: 20,
    // padding: 5
   
  },

  bottomTextContainer: {
    // flex: 1,
    // marginTop: 20,
    padding: 5
  },

  buttonContainer: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
    padding: 5
  },

  imageContainerStyle: {
    flex: 1.5,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5
  },
  imageStyle: {
    width: 80,
    height: 80
  },
  textStyle: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  priceStyle: {
    color: Colors.colorRed
  },

  buttonInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },

  buttonContinueStyle: {
    backgroundColor: Colors.btnActive,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  buttonCancelStyle: {
    backgroundColor: Colors.colorWhite,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10

  },

  elementContainor: {
    flex: HEIGHT,
    flexDirection: 'row'
  },

  elementLeftView: {

    flex: 1
  },

  elementRightView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start'

  },

  elementRightInnerView: {
    flex: 1,
    alignItems: 'flex-end',
    marginLeft: 5

  },
  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'

  }
});

export default connect(mapStateToProps, actions)(OutstandingModal);
