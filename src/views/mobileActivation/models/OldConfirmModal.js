import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton.js';
import Orientation from 'react-native-orientation';

class Confirms extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      package: [
        {
          data: this.props.local_foreign == 'local'
            ? strings.nicNo
            : strings.ppNo,
          value: this.props.id_number
        }, {
          data: strings.type,
          value: strings.connetionType
        }, {
          data: strings.connectionNo,
          value: this.props.mobile_number
        }, {
          data: strings.simNo,
          value: this.props.sim_number
        }, {
          data: strings.offer,
          value: this.props.selectedPackage.pkg_CODE
        }, {
          data: strings.package,
          value: this.props.selectedPackage.pkg_NAME
        }, {
          data: strings.rental,
          value: 'Rs. ' + parseFloat(this.props.selectedPackage.pkg_RENTAL).toFixed(2)
        }, {
          data: strings.otherContactNo,
          value: this.props.alternate_contact_number
        }
      ],

      tncDownloadUrl: 'https://www.dialog.lk/dialogdocroot/content/pdf/tc/postpaid-application-form-mob' +
          'ile.pdf',

      locals: {
        confirmButtonTxt: strings.confirmButtonTxt,
        cancelButtonTxt: strings.cancelButtonTxt,
        modalTitleNotVerified: strings.modalTitle,
        modalTitleOtpVerified: strings.modalTitleOtpVerified,
        lcdValue: strings.lcdValue,
        totalPayment: strings.totalPayment,
        byclicking: strings.byclicking,
        tns: strings.tns,
        localCall: strings.localCall,
        conFee: strings.conFee,
        outstandingFee: strings.outstandingFee,
        totalPay: strings.totalPay
      }
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();

  }
  dismissModal = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animated: true, animationType: 'slide-down' });
    Orientation.lockToPortrait();
  }

  openSignature = () => {
    console.log('Open Signature');
    this.dismissModal();
    if (this.props.isExsistingCustomer) {
      console.log('finalActivationMethod');
      Orientation.lockToPortrait();
      this
        .props
        .finalActivationMethod();

    } else {
      Orientation.lockToLandscape();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: this.state.customerSignature,
        screen: 'DialogRetailerApp.views.SignaturePadScreen',
        passProps: {
          downlodUrl: this.state.tncDownloadUrl
        }
      });

    }
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => {
            this.dismissModal();
            Orientation.lockToPortrait();
          }}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => {
            this.openSignature();
          }}>
          <Text style={styles.textStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  setTnCtext = () => {
    if (this.props.isExsistingCustomer) {
      return (
        <View style={styles.tncView}>
          <Text style={styles.desTncTxt}>{this.state.locals.byclicking}
          </Text>
          <Text
            style={styles.desTncTxtLink}
            onPress={() => Linking.openURL(this.state.tncDownloadUrl)}>
            {this.state.locals.tns}
          </Text>
        </View>
      );

    } else {
      return (<View/>);
    }
  }

  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value}/>)

  render() {
    const finalPakageDetails = this.state.package;
    let paymentDataArray = [
      {
        data: this.state.locals.localCall,
        value: 'Rs ' + this.props.paymentInfo.localCallValue
      }, {
        data: this.state.locals.conFee,
        value: 'Rs ' + this.props.paymentInfo.conFeeValue
      }
    ];

    if (this.props.needToShowoutstanding) {
      paymentDataArray.push({
        data: this.state.locals.outstandingFee,
        value: 'Rs ' + this.props.paymentInfo.outstandingFeeValue
      });
    }

    paymentDataArray.push({
      data: this.state.locals.totalPay,
      value: 'Rs ' + this.props.paymentInfo.totalPayValue
    });

    let modalTitle;
    if (this.props.isExsistingCustomer) {
      modalTitle = this.state.locals.modalTitleOtpVerified;
    } else {
      modalTitle = this.state.locals.modalTitleNotVerified;
    }
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.containerStyle}>
          <View style={styles.section_1_Container}>
            <Text style={styles.titleStyle}>{modalTitle}</Text>
          </View>
          <View style={styles.section_2_Container}>
            <FlatList
              data={finalPakageDetails}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={true}/>
          </View>
          <View style={styles.section_2_1_Container}/>
          <View style={styles.section_3_Container}>
            <FlatList
              data={paymentDataArray}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={false}/>
          </View>
          {this.setTnCtext()}
          <View style={styles.section_4_Container}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftItem}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementrightItem}>
      <Text style={styles.textStyleRight}>{value}</Text>
    </View>
  </View>
);

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    flex: 1.5
  },
  section_2_Container: {
    flex: 5
  },
  section_2_1_Container: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderLineColorLightGray,
    marginBottom: 10,
    marginTop: 10
  },
  section_3_Container: {
    flex: 4,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1
  },

  elementLeftItem: {
    width: '48%'

  },
  elementrightItem: {
    width: '52%'
  },

  innerContainerTODO: {
    backgroundColor: Colors.colorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },

  textStyleRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },
  textStyleLeft: {
    fontWeight: 'bold'

  },
  titleStyle: {
    fontSize: 24,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  pickerItem: {
    fontSize: 15
  },
  picker: {
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    height: '100%'
  },
  confirmButtonStyle: {
    backgroundColor: '#edc92c',
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  cancelButtonStyle: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  tncView: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5
  },
  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: Styles.otpEnterModalFontSize
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: Styles.otpEnterModalFontSize,
    textDecorationLine: 'underline'
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION', state.mobile);
  // console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION',
  // JSON.stringify(state.mobile));

  const id_number = state.mobile.id_number;
  const sim_number = state.mobile.sim_number;
  const mobile_number = state.mobile.mobile_number;
  const information_id_type = state.mobile.information_id_type;
  const alternate_contact_number = state.mobile.alternate_contact_number;
  const local_foreign = state.mobile.local_foreign;
  const language = state.lang.current_lang;
  const selectedPackage = state.mobile.selectedPackage;
  const packageDeposits = state.mobile.packageDeposits;
  const total_payment = state.mobile.total_payment;
  const connection_fee = state.mobile.selectedPackage.connection_fee;
  const depositAmount = state.mobile.packageDeposits.depositAmount;
  const paymentInfo = state.mobile.paymentInfo;
  const needToShowoutstanding = paymentInfo.outstandingFeeValue != 0;

  return {
    language,
    id_number,
    local_foreign,
    mobile_number,
    information_id_type,
    alternate_contact_number,
    sim_number,
    total_payment,
    selectedPackage,
    packageDeposits,
    connection_fee,
    depositAmount,
    paymentInfo,
    needToShowoutstanding
  };
};

export default connect(mapStateToProps, actions)(Confirms);