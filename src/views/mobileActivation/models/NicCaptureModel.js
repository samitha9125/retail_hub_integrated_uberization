import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Alert,
    Dimensions
} from 'react-native';
import * as actions from '../../../actions';
import { connect } from 'react-redux';

import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/Ionicons';

class EnterNicInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name:''

        };
    }

    componentWillMount(){

        if(this.props.customer_name !== ''){

            this.setState({name : this.props.customer_name});
        }
    }

    onBtnPressCancel = () => {
        this.props.navigator.dismissLightBox();
        this.props.getNameMobileAct(this.state.name);
        this.props.name?this.props.getInformationNotClearMobileAct(false):this.props.getDifferentAddressMobileAct(false);
    }

    onBtnPressContinue = () => {
        this.props.getNameMobileAct(this.state.name);
        this.props.navigator.dismissLightBox();
    }

    render() {
        return (

            <View style={styles.container}>
              <View style={styles.titleViewStyle}>
                    <Text style={styles.titleTextStyle}>{this.props.title}</Text>
                </View>

                <ScrollView style={styles.scrollViewStyle}>

                     {this.props.name?
                     <View style={styles.textFieldStyle}>
                        <TextField
                            label="Name"
                            value={this.state.name}
                            editable = {true}
                            onChangeText={(value) => this.setState({name: value})} 
                        />
                    </View>:<View/>}

                     {this.props.name?
                    <View style={styles.labelViewStyle1}>
                        <View style={styles.labelStyle1}>
                            <Text style={styles.textStyle1}>
                            Customer Picture{'\n'}(when {this.props.id_type} picture is not clear)
                            </Text>
                        </View>
                        <View style={styles.iconStyle1}>
                            <Icon name="md-camera" size={30} />
                        </View>
                    </View>:<View/>}

                    {!this.props.name?
                    <View style={styles.labelViewStyle2}>
                        <View style={styles.labelStyle2}>
                            <Text style={styles.textStyle2}>
                            Scan Proof of Billing (POB){'\n'}(when {this.props.id_type} address is not clear)
                            </Text>
                        </View>
                        <View style={styles.iconStyle2}>
                            <Icon name="md-camera" size={30} />
                        </View>
                    </View>:<View/>}

                    <View style={styles.buttonStyle}>
                        <TouchableOpacity
                            style={styles.cancelButtonStyle}
                            onPress={this.onBtnPressCancel}
                            activeOpacity={1}
                        >
                            <Text style={styles.cancelButtonTextStyle}>CANCEL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.continueButtonStyle}
                            onPress={this.onBtnPressContinue}
                            activeOpacity={1}
                        >
                            <Text style={styles.continueButtonTextStyle}>CONTINUE</Text>
                        </TouchableOpacity>

                    </View>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        width: Dimensions
          .get('window')
          .width * 0.94,
        height: Dimensions
          .get('window')
          .height * 0.8,
        backgroundColor: '#ffffff',
        borderRadius: 5,
        padding: 16,
        marginTop: 25
    },
    container1: {
        flex: 1,
        backgroundColor: '#ffffff'
    },

    titleTextStyle: {
        fontSize: 23,
        marginBottom: 20,
        marginLeft: 10,
        marginTop: 10,
        fontWeight: 'bold'
    },

    scrollViewStyle: {
        backgroundColor: '#FFF'
    },

    titleViewStyle: {
        backgroundColor: '#FFF'
    },

    viewStyle: {
        margin: 10,
        padding: 5,
        backgroundColor: '#F5F5F5',
        height: '85%'
    },

    hoshiStyle: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginRight: 20,
        marginLeft: 20
    },

    labelViewStyle1: {
        flex: 4,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 30,
        marginTop: 25,
        marginRight: 30,
        marginLeft: 30
    },

    labelViewStyle2: {
        flex: 4,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 50,
        marginTop: 20,
        marginRight: 30,
        marginLeft: 30
    },

    labelStyle1: {
        flex: 3,
        flexDirection: 'row'
    },

    labelStyle2: {
        flex: 3,
        flexDirection: 'row'
    },

    iconStyle1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    iconStyle2: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    textStyle1: {
        color: '#000',
        backgroundColor: 'transparent'
    },

    cancelButtonStyle: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: '#FFF',
        height: 45,
        borderRadius: 5,
        zIndex: 100,
        width: '40%',
        marginLeft: 10
    },

    continueButtonStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffc400',
        height: 45,
        borderRadius: 5,
        zIndex: 100,
        width: '50%',
        marginLeft: 10
    },

    cancelButtonTextStyle: {
        color: '#000',
        backgroundColor: 'transparent'
    },

    continueButtonTextStyle: {
        color: '#000',
        backgroundColor: 'transparent'
    },

    textStyle2: {
        color: '#000',
        backgroundColor: 'transparent'
    },

    buttonStyle: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 10,
        paddingTop: 10,
        marginRight: 20,
        marginLeft: 20,
        borderTopWidth: 1,
        borderColor: '#DDD'
    },
    textFieldStyle: {
        flex: 7,
        justifyContent: 'flex-start',
    }
});

const mapStateToProps = (state) => {

    let customer_name = state.mobile.name;

    return {customer_name};
};

export default connect(mapStateToProps, actions)(EnterNicInformation);
