import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';

import imageSuccessAlert from '../../../../images/common/success_msg.png';
import imageErrorsAlert from '../../../../images/common/error_msg.png';
import Orientation from 'react-native-orientation';


class MobileActModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      data: this.props.apiResponse.data
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  onBnPress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    if (this.state.data.success) {
      navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
      const me = this;
      setTimeout(() => {
        me.props.resetMobileActivationState();
      }, 50);
    }
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              {this.state.data.success
                ? <Image source={imageSuccessAlert} style={styles.alertImageStyle}/>
                : <Image source={imageErrorsAlert} style={styles.alertImageStyle}/>
              }
            </View>

            {this.state.data.success
              ? <View>
                <Text style={styles.descriptionText}>{this.state.data.message}</Text>
                <Text style={styles.mobileNumber}>{this.state.data.mobile}</Text>
              </View>
              : <View>
                <Text style={styles.errorDescriptionText}>{this.state.data.error}</Text>
              </View>
            }
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity onPress={() => this.onBnPress()} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'OK'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const apiResponse = state.mobile.api_response;

  return { apiResponse };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    backgroundColor: Colors.backgroundColorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },
  descriptionText: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: '100'

  },
  errorDescriptionText: {
    alignItems: 'flex-start',
    fontSize: 25,
    color: Colors.colorBlack,
    marginBottom: 20,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  mobileNumber: {
    alignItems: 'flex-start',
    fontSize: 28,
    color: Colors.colorBlack,
    marginBottom: 20,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtn: {
    width: 45

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen,
  }

});

export default connect(mapStateToProps, actions)(MobileActModel);
