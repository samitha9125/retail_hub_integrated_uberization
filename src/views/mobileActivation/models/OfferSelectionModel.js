import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
  KeyboardAvoidingView
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import CloseIcon from '../../../../images/common/close-icon.png';
import Utills from '../../../utills/Utills';
import Images from '../../../config/images';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton';
import _ from 'lodash';

const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };
const MIN_RELOAD_AMOUNT = 0;
const MAX_RELOAD_AMOUNT = 25000; //TODO: configure

const Utill = new Utills();

class OfferSelectionModel extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      selectedKey: 0,
      reloadValue: '',
      offerData1: null,
      more: false,
      offerData: this.props.offerData,
      local: {
        enter_valid_amount: strings.enter_valid_amount,
        reload_cant_empty: strings.reload_cant_empty,
        select_first_reload: strings.select_first_reload,
        continue: strings.continue,
        ok: strings.btnOk,
        more_offers: strings.more_offers
      }
    };

    this.onBtnPressContinueDebounced = _.debounce(() => this.onBtnPressContinue(), 300, DEBOUNCE_OPTIONS);
  }

  componentDidMount() {
    this.resetSelection(0);
    //TODO : Sample response
  // response.data.data = [{ "offer_key": "DATACARD_120", "offer_val": { "CODE":
  // "10", "RANK": 0, "OFFER_TYPE": "TOPUP", "CHARGE": "120", "RESOURCES": [{
  // "LABEL": "DATA_DAY", "VALUE": 1024, "TYPE": "MB" }, { "LABEL": "DATA_NIGHT",
  // "VALUE": 2048, "TYPE": "MB" }] } }, { "offer_key": "DATACARD_151",
  // "offer_val": { "CODE": "11", "RANK": 1, "OFFER_TYPE": "TOPUP", "CHARGE":
  // "151", "RESOURCES": [{ "LABEL": "SMS_ANY_LOCAL", "VALUE": 100, "TYPE":
  // "Items" }, { "LABEL": "RUPEES_CALL_LOCAL", "VALUE": 51, "TYPE": "Rupees" }, {
  // "LABEL": "DATA_ANY", "VALUE": 400, "TYPE": "MB" }] } }, { "offer_key":
  // "OFFER_101", "offer_val": { "CODE": "11", "RANK": 2, "OFFER_TYPE": "TOPUP",
  // "CHARGE": "101", "RESOURCES": [{ "LABEL": "SMS_D2D", "VALUE": 200, "TYPE":
  // "Items" }, { "LABEL": "SMS_D2ND", "VALUE": 200, "TYPE": "Items" }, { "LABEL":
  // "DATA_ANY", "VALUE": 75, "TYPE": "MB" }] } }, { "offer_key": "OFFER_102",
  // "offer_val": { "CODE": "11", "RANK": 3, "OFFER_TYPE": "TOPUP", "CHARGE":
  // "102", "RESOURCES": [{ "LABEL": "RUPEES_IDD", "VALUE": 40, "TYPE": "Rupees"
  // }, { "LABEL": "CALL_D2D", "VALUE": 60, "TYPE": "Minutes" }, { "LABEL":
  // "SMS_D2D", "VALUE": 100, "TYPE": "Items" }, { "LABEL": "DATA_ANY", "VALUE":
  // 100, "TYPE": "MB" }] } }, { "offer_key": "OFFER_103", "offer_val": { "CODE":
  // "11", "RANK": 4, "OFFER_TYPE": "TOPUP", "CHARGE": "125", "RESOURCES": [{
  // "LABEL": "SMS_D2D", "VALUE": 250, "TYPE": "Items" }] } }];
  }

  onBtnPressContinue = () => {
    console.log('onBtnPressContinue');
    const regex = /^\d+$/;

    if (this.state.reloadValue == '') {
      Utill.showAlertMsg(this.state.local.reload_cant_empty);
      return;
    }

    if (!(regex.test(this.state.reloadValue) && this.state.reloadValue > MIN_RELOAD_AMOUNT && this.state.reloadValue <= MAX_RELOAD_AMOUNT)) {
      Utill.showAlertMsg(this.state.local.enter_valid_amount);
      return;
    }

    this.props.getOfferCodeMobileAct(this.state.offerCode);
    this.props.getFirstReloadMobileAct(this.state.reloadValue);
    const requestParams = {
      amount: this.state.reloadValue
    };

    if (this.state.reloadValue !== 0 || this.state.reloadValue !== '') {
      this.props.commonRapidEzAccountCheck(requestParams, this.successBalance, this);
    } else {
      this.props.navigator.dismissLightBox();
    }
  }

  onBtnPressCancel = () => {
    this.props.navigator.dismissLightBox();
  }

  showRapidEzErrorModal = (response) => {
    console.log('xxx showRapidEzErrorModal');
    this.props.getFirstReloadMobileAct('');
    let passProps = {
      primaryText: this.state.local.ok,
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      icon: Images.icons.FailureAlert,
      descriptionTitle: response.title,
      description: Utill.getStringifyText(response.error),
      descriptionBottomText: response.balance,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryPress: () => this.dismissModal()
    };

    Navigation.showModal({
      title: 'CommonModalAlert',
      screen: 'DialogRetailerApp.modals.CommonModalAlert',
      passProps: passProps
    });
  }

  dismissModal = () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  resetSelection = (key) => {
    console.log(key);
    this.setState({ selectedKey: key, reloadValue: this.state.offerData[key].offer_val.CHARGE, offerCode: this.state.offerData[key].offer_key });
  };

  successBalance = (response) => {
    console.log('xxx successBalance :: ', response);
    this.props.navigator.dismissLightBox();
  }

  valueChanged = (value) => {
    // this.props.getFirstReloadMobileAct(value);
    this.props.getOfferCodeMobileAct('');
    this.setState({ reloadValue: value, offerCode: '' });
  }

  viewMore = () => {
    this.setState({ more: true });
  }

  render() {
    const offerItems = [];
    let offerDataCount = this.state.offerData.length;
    offerDataCount = Math.min(3, offerDataCount);
    for (let i = 0; i < offerDataCount; i++) {
      offerItems.push(<OfferItem
        item={this.state.offerData[i].offer_val}
        key={i}
        index={i}
        borColor={this.state.selectedKey === i
          ? '#FFC400'
          : '#DDDDDD'}
        onPress={() => this.resetSelection(i)} 
      />);
    }
    let offerList = (
      <View style={styles.fatListContainer}>
        {offerItems}
        {this.state.offerData.length > 3
          ? (
            <View>
              <Text style={styles.viewMoreContainer} onPress={() => this.viewMore()}>
                {this.state.local.more_offers}
              </Text>
            </View>
          )
          : null
        }
      </View>
    );

    if (this.state.more) {
      const offerItems = [];
      const offerDataCount = this.state.offerData.length;
      for (let i = 0; i < offerDataCount; i++) {
        offerItems.push(<OfferItem
          item={this.state.offerData[i].offer_val}
          key={i}
          index={i}
          borColor={this.state.selectedKey === i
            ? '#FFC400'
            : '#DDDDDD'}
          onPress={() => this.resetSelection(i)} 
        />);
      }
      offerList = (
        <ScrollView style={styles.fatListContainerScrollable}>
          {offerItems}
        </ScrollView>
      );
    }

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View style={styles.bottomContainer}>
          <TouchableOpacity
            style={styles.cancelBtnContainer}
            onPress={this.onBtnPressCancel}
          >
            <Image source={CloseIcon} style={styles.closeIconStyle} />
          </TouchableOpacity>
        </View>
        <View style={styles.modelTitleContainer}>
          <Text style={styles.modelTitleTxt}>{this.state.local.select_first_reload}</Text>
        </View>
        {offerList}
        <View style={styles.bottomCantainer}>
          <TextInput
            style={styles.bottomCantainerTextInput}
            keyboardType="numeric"
            value={this.state.reloadValue}
            underlineColorAndroid="#FFF"
            onChangeText={this.valueChanged} 
          />

          <TouchableOpacity
            style={styles.bottomCantainerBtn}
            onPress={this.onBtnPressContinueDebounced}
          >
            <Text style={styles.bottomCantainerBtnTxt}>{this.state.local.continue}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bottomViewStyle} />
      </KeyboardAvoidingView>

    );
  }
}

const OfferItem = ({ item, index, borColor, onPress }) => {
  const dynamicItems = [];
  const resourceCount = item.RESOURCES.length;
  for (let i = 0; i < resourceCount; i++) {
    const resource = item.RESOURCES[i];
    dynamicItems.push(
      <View style={styles.fatListItemResources} key={i}>
        <Text style={styles.fatListItemResourceLabelText}>{resource.LABEL}</Text>
        <Text style={styles.fatListItemResourceValueText}>{resource.VALUE}</Text>
        <Text style={styles.fatListItemResourceTypeText}>{resource.TYPE}</Text>
      </View>
    );
  }
  return (
    <TouchableOpacity
      style={[
        styles.fatListInnerContainer, {
          borderColor: borColor
        }
      ]}
      onPress={onPress}
      index={index}
    >
      <View style={styles.fatListItemAmount}>
        <Text style={styles.fatListItemAmountText}>Rs</Text>
        <Text style={styles.fatListItemAmountText}>{item.CHARGE}</Text>
      </View>
      <View style={styles.fatListItemContainerResources}>
        {dynamicItems}
      </View>

    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  const first_reload = state.mobile.first_reload;
  const Language = state.lang.current_lang;
  return { first_reload, Language };
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions
      .get('window')
      .width * 0.94,
    height: Dimensions
      .get('window')
      .height * 0.92,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 16,
    marginTop: 25
  },
  modelTitleContainer: {
    backgroundColor: '#FFF'
  },
  closeIconStyle: {
    width: 30,
    height: 30
  },
  modelTitleTxt: {
    fontSize: 23,
    marginBottom: 20,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold'
  },
  fatListContainer: {
    flex: 5,
    // height: 500,
    backgroundColor: '#ffffff'
  },

  fatListContainerScrollable: {
    height: Dimensions
      .get('window')
      .height * 0.35,
    backgroundColor: '#ffffff'
  },

  viewMoreContainer: {
    //flex: 5,
    color: '#2E3192',
    textAlign: 'center',
    fontSize: 15,
    marginTop: 5,
    marginBottom: 8,
    textDecorationLine: 'underline'

  },
  bottomCantainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
    paddingTop: 10,
    marginRight: 0,
    borderTopWidth: 1,
    borderColor: '#DDD'
  },
  fatListInnerContainer: {
    height: 85,
    flexDirection: 'row',
    padding: 0,
    margin: 5,
    borderWidth: 2,
    borderRadius: 5
  },

  fatListItemAmount: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F9F3F3'
  },
  fatListItemAmountText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20
  },

  fatListItemContainerResources: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 4
  },
  fatListItemResources: {
    flex: 1
  },
  fatListItemResourceLabelText: {
    textAlign: 'center',
    fontSize: 11,
    marginBottom: 5
  },
  fatListItemResourceValueText: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 5
  },
  fatListItemResourceTypeText: {
    textAlign: 'center',
    fontSize: 10
  },

  bottomCantainerTextInput: {
    backgroundColor: '#FFF',
    width: '55%',
    height: 45,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd'
  },

  bottomCantainerBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffc400',
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    width: '40%',
    marginLeft: 10
  },
  bottomCantainerBtnTxt: {
    color: '#000',
    backgroundColor: 'transparent'
  },
  bottomContainer: {
    alignSelf: 'flex-end',
    //justifyContent: 'center', paddingRight: 5, marginBottom: 30,
    flexDirection: 'row'
  },
  cancelBtnContainer: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    height: 20,
    //borderRadius: 5,
    zIndex: 100,
    width: '40%',
    marginLeft: 10
  },
  cancelBtnTxt: {
    color: '#000',
    backgroundColor: 'transparent'
  },
  bottomViewStyle: {
    height: 68
  }

});

export default connect(mapStateToProps, actions)(OfferSelectionModel);
