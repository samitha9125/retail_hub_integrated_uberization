import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import { connect } from 'react-redux';
import * as actions from '../../actions';
import BottomItems from './BottomItems';
import BottomItemsPost from './BottomItemsPost';

class BottomItemsRoot extends Component {
  constructor(props) {
    super(props);

    this.navigator = this.props.navigator;
  }

  render() {
    const { navigator } = this.props;
    return (
      <View>
        {this.props.pre_post == 'pre'
          ? <View style={styles.container}>
            <BottomItems navigator={navigator}/>
          </View>
          : <View style={styles.container}>
            <BottomItemsPost navigator={navigator} defaultEzNumber={this.props.defaultEzNumber}/>
          </View>
        }
      </View>

    );
  }
}

const mapStateToProps = state => {

  const pre_post = state.mobile.pre_post;

  return { pre_post };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7
  }
});

export default connect(mapStateToProps, actions)(BottomItemsRoot);
