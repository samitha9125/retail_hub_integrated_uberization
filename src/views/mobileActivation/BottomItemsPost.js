import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image,
  Keyboard
} from 'react-native';
import CheckBox from 'react-native-check-box';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Orientation from 'react-native-orientation';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import MaterialInput from './MaterialInput';
import InputSection from './InputSection';
import SectionContainer from './SectionContainer';
import IdRadioButtons from './IdRadioButtons';
import CaptureId from './CaptureId';
import CaptureThumbnails from './CaptureThumbnails';
import CaptureTwoThumbnails from './CaptureTwoThumbnails';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import  { Analytics, Screen }  from '../../utills/';
import  { Colors, Styles, Images, Constants }  from '../../config/';
import constants from '../../config/constants';
import strings from '../../Language/MobileActivaton';
import TotalPayments from './TotalPayment';

import _ from 'lodash';


const Utill = new Utills();
const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

class BottomItemsPost extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      finalActivationData: {},
      dropDownData: [],
      userLogged: true,
      userData: '',
      phone: '',
      agentMssdn: global.agentMssdn,
      // isEnablePackageInfo: this.props.didPackageLoad,
      enableSerchButton: this.props.nic_api_fail,
      selectedPackage: { 
        pkg_CODE: '', 
        pkg_NAME: '', 
        pkg_RENTAL: "0",
        connection_fee: "",
        deposit_AMOUNT: "",
      },
      d_connections: [],
      packageDeposits: { depositType: "LCD", depositAmount: "Local Call Deposit" },
      eligiblePackages: {},
      totalOutstanding: '',
      ezCashPIN: '',
      sectionClick1: true,
      sectionClick2: false,
      sectionClick3: false,
      sectionClick4: false,
      otpVerified: false,
      material_code: '',
      idNotClearChecked: false,
      addressDifferentChecked: false,
      captureContex: 'NIC',
      activateBtnTxtColor: Colors.btnDeactiveTxtColor,
      activateBtnColor: Colors.btnDeactive,
      promptVisible: false,
      otpCount: 1,
      userSelectedPackageIndex: -1,
      imageIsCaptured: false,
      isLoading: false,
      loginBtnTap: false,
      nicValidate: strings.nicValidate,
      passValidate: strings.passValidate,
      productInfo: strings.productInfo,
      custInfo: strings.custoInfo,
      ButtonTxt: strings.buttonTxt,
      simNo: strings.simNo,
      scanSim: strings.scanSim,
      mobileNo: strings.mobileNo,
      firstRel: strings.firstRel,
      alternateContact: strings.otherContact,
      BtnTxt: strings.buttonTxt,
      submit_valid_nic: strings.submit_valid_nic,
      submit_valid_passport: strings.submit_valid_passport,

      idLabelInputNic: strings.idLabelInputNic,
      idLabelInputPp: strings.idLabelInputPp,

      captureNicLabelTxt: strings.captureNicLabelTxt,
      captureDlLabelTxt: strings.captureDlLabelTxt,
      capturePpLabelTxt: strings.capturePpLabelTxt,
      captureForeignAddress: strings.captureForeignAddress,

      capturedNicLabelTxt: strings.capturedNicLabelTxt,
      capturedDlLabelTxt: strings.capturedDlLabelTxt,
      capturedPpLabelTxt: strings.capturedPpLabelTxt,
      capturedForeignAddress: strings.capturedForeignAddress,

      captureCustomerPhotoSubTxt: strings.captureCustomerPhotoSubTxt,
      captureProofOfDeliverySubTxt: strings.captureProofOfDeliverySubTxt,
      capturedCustomerPhotoSubTxt: strings.capturedCustomerPhotoSubTxt,
      capturedProofOfDeliverySubTxt: strings.capturedProofOfDeliverySubTxt,

      notClearNicLabelTxt: strings.notClearNicLabelTxt,
      notClearDlLabelTxt: strings.notClearDlLabelTxt,
      notClearPpLabelTxt: strings.notClearPpLabelTxt,

      pobNicLabelTxt: strings.pobNicLabelTxtPost,
      pobDlLabelTxt: strings.pobDlLabelTxtPost,
      pobPpLabelTxt: strings.pobPpLabelTxtPost,

      customerSignature: strings.customerSignature,

      activation_success: strings.activation_success,
      invalid_sim_serieal: strings.invalid_sim_serieal,
      customer_information_image_not_captured: strings.customer_information_image_not_captured,
      please_enter_nic: strings.please_enter_nic,
      passport_image_not_captured: strings.passport_image_not_captured,
      address_image_not_captured: strings.address_image_not_captured,
      pob_image_not_captured: strings.pob_image_not_captured,
      customer_image_not_captured: strings.customer_image_not_captured,
      invalid_altenate_contact_no: strings.invalid_altenate_contact_no,
      signature_not_captured: strings.signature_not_captured,
      locals: {
        please_enter_ezcash_code: strings.please_enter_ezcash_code,
        ezcash_pin_must_only_numbers: strings.ezcash_pin_must_only_numbers,
        ezcash_pin_must_contain_4_numbers: strings.ezcash_pin_must_contain_4_numbers,
        emailValidation : strings.please_enter_valid_email
      },

      package: strings.package,
      lcd: strings.lcd,
      email: strings.email,
      ezcash: strings.ezcash,
      ezCashPin: strings.ezCashPin,
      email_address: '',
      system_error: strings.system_error,
      start_ts: '',
      btnOk: strings.btnOk
    };

    this.debouncedFinalActivation = _.debounce(() => this.onPressActivate(), 150, DEBOUNCE_OPTIONS);

    this.navigator = this.props.navigator;
  }

  componentWillMount(){
    console.log("In component will mount");
    this.props.getUniqueTxIdMobileAct(Date.now());
    Orientation.lockToPortrait();
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  packageSuccess = (response) => {
    console.log('xxx packageSuccess', response);
    this.props.getMobileActLoading(false);
    // this.setState({ sectionClick2: true, isLoading: false });
    console.log('ELIGIBLE PACKAGES RESPONSE', JSON.stringify(response.data));
    console.log('ELIGIBLE PACKAGES RESPONSE', response.data.info);
    console.log('ELIGIBLE PACKAGES RESPONSE', response.data.info);

    var elegiblePackageNames = [];
    response.data.info.forEach((item) => {
      elegiblePackageNames.push(item.pkg_NAME);
    });

    this.props.isPackageLoaded(true);
    this.setState({ dropDownData : elegiblePackageNames });
    // this.setState({ isEnablePackageInfo : true });
    this.setState({ eligiblePackages : response.data.info });
  }

  packageFailure = (response) => {
    console.log('xxx packageFailure', response);
    this.props.getMobileActLoading(false);
    // this.setState({ sectionClick2: true, isLoading: false });
    this.props.isPackageLoaded(false);
    // this.setState({ isEnablePackageInfo : false });
    console.log('ELIGIBLE PACKAGES FAIL RESPONSE', JSON.stringify(response.data));
    console.log('ELIGIBLE PACKAGES FAIL RESPONSE', response.data.error);
    console.log('ELIGIBLE PACKAGES FAIL RESPONSE', response.data.error);
    this.showAlert(response.data.error);
    //SIM is not assgined to a default offer
    if (response.data.error == 'SIM is not assgined to a default offer'){
      this.props.resetSimApiFail(true);
      this
        .props
        .getSimNumberMobileAct('');
    }
  }


  componentWillReceiveProps(nextProps) {
    console.log('xxxxxx componentWillReceiveProps', nextProps);
    // console.log('xxxx section 2', this.state.sectionClick2);
    const regex = /^[^abcdefghijklmnopqrstuwyzABCDEFGHIJKLMNOPQRSTUWYZ]+$/;

    const elegiblePackages = this.state.eligiblePackages;

    if (nextProps.local_foreign == 'local' && ((((nextProps.id_number.length > 10 && (nextProps.id_number.substr(10, 1) == 'v' || nextProps.id_number.substr(10, 1) == 'V' || nextProps.id_number.substr(10, 1) == 'x' || nextProps.id_number.substr(10, 1) == 'X' || nextProps.id_number.substr(11, 1) == 'v' || nextProps.id_number.substr(11, 1) == 'V' || nextProps.id_number.substr(11, 1) == 'x' || nextProps.id_number.substr(11, 1) == 'X')) || (nextProps.id_number.length == 10 && (((nextProps.id_number.substr(2, 3) > 365 && nextProps.id_number.substr(2, 3) < 500) || (nextProps.id_number.substr(2, 3) > 865 && nextProps.id_number.substr(2, 3) < 1000)) && (nextProps.id_number.substr(9, 1) == 'v' || nextProps.id_number.substr(9, 1) == 'V' || nextProps.id_number.substr(9, 1) == 'x' || nextProps.id_number.substr(9, 1) == 'X')))) || (nextProps.id_number.length > 12 || (nextProps.id_number.length == 12 && ((nextProps.id_number.substr(4, 3) > 365 && nextProps.id_number.substr(4, 3) < 500) || ((nextProps.id_number.substr(10, 1) != 'v' || nextProps.id_number.substr(10, 1) != 'x') && (nextProps.id_number.substr(4, 3) > 865 && nextProps.id_number.substr(4, 3) < 1000)))))) || (!regex.test(nextProps.id_number) && nextProps.id_number.length != 0) || (nextProps.id_number.length == 12 && (nextProps.id_number.substr(0, 1) != 1 || nextProps.id_number.substr(0, 1) != 2))) && !nextProps.firstValidated) {
      this.showAlert(this.state.submit_valid_nic);
    }
    const reg = /^[a-zA-Z0-9]+$/;

    if (nextProps.local_foreign === 'foreign' && !reg.test(nextProps.id_number) && nextProps.id_number.length > 0) {
      this.showAlert(this.state.submit_valid_passport);
    }

    if (nextProps.information_id_type !== this.props.information_id_type) {
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      nextProps.resetEzCashPin();
      this.setState({
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
    }

    if (nextProps.local_foreign == 'foreign' && nextProps.id_number !== this.props.id_number) {
      nextProps.getApiLoading(false);
      nextProps.getSimNumberMobileAct('');
      nextProps.resetValidateSimNumber();
      nextProps.getMobileNumberMobileAct('');
      nextProps.getFirstReloadMobileAct('');
      nextProps.resetMobileActNicValidation();
      nextProps.resetMobileActModelPopped();
      nextProps.resetMobileActOTPValidation();
      if (nextProps.selectedPackage.pkg_CODE !== ''){
        nextProps.resetSelectedPackage();
      }
      if (nextProps.packageDeposits.depositAmount !== ''){
        nextProps.resetLocalCallDeposit();
      }
      if (nextProps.userSelectedPackageIndex !== -1){
        nextProps.resetSelectedPackageIndex();
      }
      nextProps.getSkipValidationMobileAct(false);
      nextProps.mobileActOtpPopped(false);
      nextProps.mobileActMConnectPopped(false);
      nextProps.getOtpStatusMobileAct(false);
      nextProps.getResetImagesMobileAct();
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      nextProps.local_foreign == 'local' ? nextProps.getInformationIdTypeMobileAct('NIC') : nextProps.getInformationIdTypeMobileAct('PP');
      this.setState({
        sectionClick2: false,
        otpCount: 1,
        isLoading: false,
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
      this
        .props
        .getMobileActLoading(false);
    }

    // this.setState({ isEnablePackageInfo : false });

    if (nextProps.nicApiFail) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.simApiFail) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      nextProps.getSimNumberMobileAct('');
    }

    if (!nextProps.firstValidated) {
      nextProps.getApiLoading(false);
      nextProps.getSimNumberMobileAct('');
      nextProps.resetValidateSimNumber();
      nextProps.getMobileNumberMobileAct('');
      nextProps.getFirstReloadMobileAct('');
      nextProps.resetMobileActNicValidation();
      nextProps.resetMobileActModelPopped();
      nextProps.resetMobileActOTPValidation();
      nextProps.resetEzCashPin();
      if (nextProps.selectedPackage.pkg_CODE !== ''){
        nextProps.resetSelectedPackage();
      }
      if (nextProps.packageDeposits.depositAmount !== ''){
        nextProps.resetLocalCallDeposit();
      }
      if (nextProps.userSelectedPackageIndex !== -1){
        nextProps.resetSelectedPackageIndex();
      }
      nextProps.getSkipValidationMobileAct(false);
      nextProps.mobileActOtpPopped(false);
      nextProps.mobileActMConnectPopped(false);
      nextProps.getOtpStatusMobileAct(false);
      nextProps.getResetImagesMobileAct();
      nextProps.getInformationNotClearMobileAct(false);
      nextProps.getDifferentAddressMobileAct(false);
      nextProps.local_foreign == 'local' ? nextProps.getInformationIdTypeMobileAct('NIC') : nextProps.getInformationIdTypeMobileAct('PP');
      this.setState({
        sectionClick2: false,
        otpCount: 1,
        isLoading: false,
        addressDifferentChecked: false,
        idNotClearChecked: false
      });
      // this.setState({ isEnablePackageInfo : false });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.local_foreign === 'local' && nextProps.firstValidated && nextProps.nic_validation === null && nextProps.id_number !== this.props.id_number) {
      const data = {
        id_number: nextProps.id_number,
        id_type: nextProps.local_foreign === 'local'
          ? 'NIC'
          : 'PP'
      };
      Keyboard.dismiss();
      this.setState({ isLoading: true, totalOutstanding: 0 , d_connections: [] }, ()=>{
        this.props.setTotalPaymentData({ outstandingFeeValue: '' });
      });
      nextProps.resetPaymentInfo();
      let start_ts = Math.round((new Date()).getTime() / 1000);
      this.setState({ start_ts: start_ts });
      nextProps.mobileActNicValidation(data,'gsmPostpaidConnection');
      nextProps.resetMobileActModelPopped();
      this
        .props
        .getMobileActLoading(true);
    }
    //TODO : New Change
    if (nextProps.nic_validation !== null && nextProps.nic_validation.success === true && nextProps.firstValidated && !nextProps.skip_validation) {
      this.setState({ isLoading: false });
      console.log('xxxxxx NIC VALIDATION SUCCESS xxxxxxxxxxxxxx');
      this
        .props
        .getMobileActLoading(false);

      if (nextProps.nic_validation.cxExistStatus === 'Y' && !nextProps.model_popped) {
        console.log('xxxxxx cxExistStatus :: YES');
        nextProps.mobileActModelPopped(true);
        this.showValidationModel();
      } else if (nextProps.nic_validation.cxExistStatus === 'N') {
        console.log('xxxxxx cxExistStatus :: NO');
        this.setState({ sectionClick2: true }); // Section2 true
        nextProps.mobileActModelPopped(true);
      }
    }

    if (!nextProps.simValidated) {
      nextProps.resetValidateSimNumber();
    }

    if (nextProps.otp_status === 1) {
      this.setState({ otpVerified: true });
    }

    if (nextProps.sim_number == ''){
      if (nextProps.mobile_number){
        nextProps
          .getMobileNumberMobileAct('');
      }
      if (nextProps.selectedPackage.pkg_CODE !== ''){
        nextProps.resetSelectedPackage();
      }
      if (nextProps.packageDeposits.depositAmount !== ''){
        nextProps.resetLocalCallDeposit();
      }
      if (nextProps.userSelectedPackageIndex !== -1){
        nextProps.resetSelectedPackageIndex();
      }
      this.setState({ dropDownData : [] });

      // if (nextProps.selectedPackage.pkg_NAME){
      //   nextProps
      //     .resetSelectedPackage();
      // }
    }

    if (nextProps.simValidated && nextProps.sim_validation == null && nextProps.firstValidated && nextProps.sim_number !== this.props.sim_number) {
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      const data = {
        sim: nextProps.sim_number
      };

      // console.log("VALIDATE SIM NO FROM POSTPAID: ",nextProps);
      nextProps.validateSimNumber(data);
    }

    if (nextProps.sim_validation !== null) {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.mobile_number == '') {
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      Utill.apiRequestPost('reserveMobileNo', 'gsmPostpaidConnection', 'ccapp', {}, this.reserveNumberSuccess, this.reserveNumberFailure, this.reserveNumberEx);
    }

    if (nextProps.local_foreign === 'foreign') {
      nextProps.kyc_pob_capture !== null ? nextProps.getDifferentAddressMobileAct(true) : true;
      nextProps.kyc_customer_capture !== null ? nextProps.getInformationNotClearMobileAct(true) : true;
    }

    if (nextProps.local_foreign === 'local') {
      nextProps.kyc_pob_capture !== null ? nextProps.getDifferentAddressMobileAct(true) : true;
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && nextProps.selectedPackage.pkg_CODE !== "") {
      this.setState({ sectionClick3: true });
    } else {
      this.setState({ sectionClick3: false });
    }
    if (nextProps.nic_validation !== undefined && nextProps.nic_validation!== null) {
      if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && nextProps.signatureCaptured && nextProps.ezCashPIN !== "") {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && (nextProps.otp_status == 1 || nextProps.otp_status == 3) && nextProps.nic_validation.documnetExistInDataScan === 'Y' && nextProps.ezCashPIN !== "") {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && (nextProps.otp_status == 1 || nextProps.otp_status == 3) && nextProps.nic_validation.documnetExistInDataScan === 'N' && nextProps.signatureCaptured && nextProps.ezCashPIN !== "") {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated && (nextProps.otp_status == 1 || nextProps.otp_status == 3) && nextProps.nic_validation.documnetExistInDataScan === 'N' && nextProps.signatureCaptured && nextProps.ezCashPIN !== "") {
        this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
      } else {
        this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
      }
    } else {
      this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
    }

    console.log("%%%%%-> nextProps.ezcashpin: ", nextProps.ezCashPIN );

    if (nextProps.material_code !== null 
      && nextProps.material_code.length >= 1 
      && nextProps.mobile_number !== '' 
      && nextProps.sim_number !== ''
      && nextProps.didPackageLoad == false
      && Object.keys(elegiblePackages).length === 0 && elegiblePackages.constructor === Object
    ) {
      console.log("Received material_code: ", nextProps.material_code);
      nextProps.getMobileActLoading(true);
      Utill.apiRequestPost('salesAppDialogEligiblePackage', 'gsmPostpaidConnection', 'ccapp', { material_code: nextProps.material_code }, this.packageSuccess, this.packageFailure);
    }

    if (nextProps.firstValidated && nextProps.otp_popped && this.state.otpCount === 1) {
      this.setState({
        promptVisible: true,
        otpCount: 2
      });
    }
  }

    onPressNIC = () => {
      console.log('xxx onPressNIC');
      let capture_type_no;
      let titleName;
      if (this.props.information_id_type === 'DR') {
        capture_type_no = 4;
        titleName = 'Capture Driving License';
      } else if (this.props.information_id_type === 'NIC') {
        capture_type_no = 1;
        titleName = 'Capture NIC';
      } else {
        capture_type_no = 3;
        titleName = 'Capture Passport';
      }

      this
        .props
        .getCaptureIdTypeNoMobileAct(capture_type_no);
      this
        .props
        .navigator
        .push({
          title: titleName,
          screen: 'DialogRetailerApp.views.CameraScreen',
          passProps: {
            onReturnView: 'some text',
            captureType: this.props.information_id_type
          }
        });
    };

    onPressReCapture = (capNo = null) => {
      console.log('xxx onPressReCapture');
      let captureTypeNo;
      let titleName;
      if (this.props.information_id_type === 'DR') {
        captureTypeNo = 4;
        titleName = 'Capture Driving License';
      } else if (this.props.information_id_type === 'NIC') {
        titleName = 'Capture NIC';
        if (capNo !== null && capNo === 2) {
          captureTypeNo = 2;
        } else {
          captureTypeNo = 1;
        }
      } else {
        captureTypeNo = 3;
        titleName = 'Capture Passport';
      }

      this
        .props
        .getCaptureIdTypeNoMobileAct(captureTypeNo);
      this
        .props
        .navigator
        .push({
          title: titleName,
          screen: 'DialogRetailerApp.views.CameraReCaptureScreen',
        });
    };

    updatePackage = (idx, value) => {
      console.log('xxx updatePackage');
      const selectedPackage = this.state.eligiblePackages[idx];
      console.log("selected package index: ", idx);
      console.log("selected package value: ", value);
      console.log("selected package: ", selectedPackage);

      this
        .props
        .getSelectedPackageIndex(idx);

      const depositParams = {
        package_code : selectedPackage.pkg_CODE,
        customer_type : this.props.local_foreign,
        identification_type : this.props.information_id_type === 'DR'
          ? 'DL'
          : (this.props.information_id_type === 'NIC'
            ? 'NIC'
            : 'PP')

      };

      this
        .props
        .getPackageDeposits(depositParams); //API_CALL
    
      this
        .props
        .getSelectedPostpaidPackage(selectedPackage);

      this.setState({ selectedPackage: selectedPackage });
    }


    onPressReCaptureForeign = () => {
      console.log('xxx onPressReCaptureForeign');
      let captureTypeNo;
      let titleName;

      if (this.props.information_id_type === 'NIC') {
        captureTypeNo = 1;
        titleName = 'Capture NIC';
      } else {
        captureTypeNo = 3;
        titleName = 'Capture Passport';
      }

      this
        .props
        .getCaptureIdTypeNoMobileAct(captureTypeNo);
      this
        .props
        .navigator
        .push({
          title: titleName,
          screen: 'DialogRetailerApp.views.CameraReCaptureScreen',
        });
    };


    onPressPassport = () => {
      console.log('xxxx onPressPassport');
      if (this.props.local_foreign === 'foreign'){
        this.props.resetMobileActNicValidation();
      }
      if (this.props.local_foreign === 'foreign' && this.props.firstValidated) {
        const data = {
          id_number: this.props.id_number,
          id_type: this.props.local_foreign === 'local'
            ? 'NIC'
            : 'PP'
        };
        this.setState({ totalOutstanding: 0, d_connections: [] }, ()=>{
          this.props.setTotalPaymentData({ outstandingFeeValue: '' });
        });
        let start_ts = Math.round((new Date()).getTime() / 1000);
        this.setState({ start_ts: start_ts });
        this.props.mobileActNicValidation(data, 'gsmPostpaidConnection');
        this.props.resetMobileActModelPopped();
      }
    }

    onPressSIM = () => {
      this.setState({ eligiblePackages: {} });
      this.props.isPackageLoaded(false);

      if (this.props.selectedPackage.pkg_CODE !== ''){
        this.props.resetSelectedPackage();
      }
      if (this.props.packageDeposits.depositAmount !== ''){
        this.props.resetLocalCallDeposit();
      }
      if (this.props.userSelectedPackageIndex !== -1){
        this.props.resetSelectedPackageIndex();
      }


      console.log('xxx onPressSIM');
      this
        .props
        .navigator
        .push({
          title: this.state.scanSim,
          screen: 'DialogRetailerApp.views.BarcodeScannerScreen',
          passProps: {
            onReturnView: 'this.onPressNIC'
          }
        });
    };

    onPressMobile = () => {
      console.log('xxxx onPressMobile');
      this
        .props
        .navigator
        .showLightBox({
          screen: 'DialogRetailerApp.models.NumberSelectionModel',
          passProps: {
            title: 'titleN',
            content: 'contentN',
            onClose: null
          },
          overrideBackPress: true,
          style: modelStyleNoSelection,
          adjustSoftInput: 'resize',
          tapBackgroundToDismiss: false
        });
    };

    onPressReload = () => {
      console.log('xxxx onPressReload');
      this
        .props
        .navigator
        .showLightBox({
          screen: 'DialogRetailerApp.models.OfferSelectionModel',
          passProps: {
            title: 'titleN',
            content: 'contentN',
            onClose: null
          },
          style: modelStyle,
          adjustSoftInput: 'resize'
        });
    };

    onPressSucessModel = () => {
      console.log('xxxx onPressSucessModel');
      this
        .props
        .navigator
        .showLightBox({
          screen: 'DialogRetailerApp.models.GeneralModelSuccessMobileAct',
          passProps: {
            title: 'titleN',
            content: 'contentN',
            onClose: null
          },
          style: modelStyle,
          adjustSoftInput: 'resize',
          tapBackgroundToDismiss: false
        });
    };

    getImageTypeList = () => {
      console.log('xxxx getImageTypeList');
      let imageTypeList = []; 
      if (this.props.information_id_type === 'NIC') {
        this.props.customer_information_image_nic_1 !== null ? 
          imageTypeList.push(constants.imageCaptureTypes.NIC_FRONT) : true;
        this.props.customer_information_image_nic_2 !== null ? 
          imageTypeList.push(constants.imageCaptureTypes.NIC_BACK) : true;
      } else if (this.props.information_id_type === 'DR') {
        this.props.customer_information_image !== null ? 
          imageTypeList.push(constants.imageCaptureTypes.DRIVING_LICENCE) : true;     
      } else {
        this.props.customer_information_image !== null ? 
          imageTypeList.push(constants.imageCaptureTypes.PASSPORT) : true;
      }
  
      this.props.kyc_customer_capture !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.CUSTOMER_IMAGE) : true;
      this.props.kyc_pob_capture !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.PROOF_OF_BILLING) : true;
      this.props.signature !== null ? 
        imageTypeList.push(constants.imageCaptureTypes.SIGNATURE) : true;
      console.log('xxxx getImageTypeList', imageTypeList);
      return imageTypeList;
    }

    onPressActivate = () => {
      console.log('xxxxxxxx onPressActivate');
      console.log("outstandingAmount: ", this.state.d_connections);
      let outstandingAmount = 0;

      if (this.props.email_address){
        if (!Utill.emailValidation(this.props.email_address)) {
          console.log('xxxxxxxx onPressActivate - emailCheck', this.props.email_address);
          this.showAlert(this.state.locals.emailValidation);
          return;
        } 
      }
      
      if (this.state.d_connections.length !== 0){
        outstandingAmount = this.state.d_connections[this.state.d_connections.length-1].totalOsAmount;
      }

      const outstanding_fee = parseFloat((outstandingAmount !== "" || outstandingAmount !== undefined) ? outstandingAmount : 0.00).toFixed(2);
      const connection_fee = parseFloat((this.props.selectedPackage.connection_fee !== "" || this.props.selectedPackage.connection_fee !== undefined) ? this.props.selectedPackage.connection_fee : 0.00).toFixed(2);
      const depositAmount = parseFloat((this.props.packageDeposits.depositAmount !== "" || this.props.packageDeposits.depositAmount !== undefined ) ? this.props.packageDeposits.depositAmount : 0.00).toFixed(2);
      const total = (parseFloat(isNaN(outstanding_fee)? 0 : outstanding_fee) + parseFloat(isNaN(connection_fee)? 0 : connection_fee) + parseFloat(isNaN(depositAmount)? 0 : depositAmount)).toFixed(2);
     
      console.log("totalPayValue: ", total);
      console.log("connection_fee: ", connection_fee);
      console.log("depositAmount: ", depositAmount);
      console.log("outstandingAmount: ", outstanding_fee);
      //.d_connections[1].totalOsAmount
      const postPaidActivationParams = {
        "d_connections": this.state.d_connections,
        "lcd_amount": this.props.packageDeposits.depositAmount,
        "deposit_type": this.props.packageDeposits.depositType,
        "total_amount": total,
        "con_fee": this.props.selectedPackage.connection_fee, 
        "package_code": this.props.selectedPackage.pkg_CODE,
        "eZ_pin": this.props.ezCashPIN,

      };

      console.log("postPaidActivationParams: ", JSON.stringify(postPaidActivationParams));

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated && !this.props.fourthValidated) {
        this.showAlert(this.state.customer_information_image_not_captured);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated && !this.props.fourthValidated && this.props.nic_validation.documnetExistInDataScan === 'N') {
        this.showAlert(this.state.customer_information_image_not_captured);
        return;
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'local' && this.props.information_id_type === 'NIC' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && (this.props.customer_information_image_nic_1 == null || this.props.customer_information_image_nic_2 == null)) {
        this.showAlert(this.state.please_enter_nic);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'local' && this.props.information_id_type === 'NIC' && this.props.nic_validation.documnetExistInDataScan === 'N' && (this.props.customer_information_image_nic_1 == null || this.props.customer_information_image_nic_2 == null)) {
        this.showAlert(this.state.please_enter_nic);
        return;
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y')) {
        if (this.props.customer_information_image == null) {
          this.showAlert(this.state.passport_image_not_captured);
          return;
        } else if (this.props.kyc_pob_capture == null) {
          this.showAlert(this.state.address_image_not_captured);
          return;
        }
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && this.props.nic_validation.documnetExistInDataScan === 'Y') {
        if (this.props.customer_information_image == null) {
          this.showAlert(this.state.passport_image_not_captured);
          return;
        }
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'foreign' && this.props.nic_validation.documnetExistInDataScan === 'N') {
        if (this.props.customer_information_image == null) {
          this.showAlert(this.state.passport_image_not_captured);
          return;
        } else if (this.props.kyc_pob_capture == null) {
          this.showAlert(this.state.address_image_not_captured);
          return;
        }
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.local_foreign === 'foreign' && this.props.nic_validation.cxExistStatus === 'Y' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.address_image_not_captured);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.local_foreign === 'foreign' && this.props.nic_validation.cxExistStatus === 'Y' && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.address_image_not_captured);
        return;
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.information_id_type === 'PP' && (this.props.nic_validation.documnetExistInDataScan === 'N' || this.props.nic_validation.documnetExistInDataScan === 'Y') && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.pob_image_not_captured);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.information_id_type === 'PP' && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.pob_image_not_captured);
        return;
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.information_not_clear && this.props.kyc_customer_capture == null) {
        this.showAlert(this.state.customer_image_not_captured);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.information_not_clear && this.props.kyc_customer_capture == null) {
        this.showAlert(this.state.customer_image_not_captured);
        return;
      }

      if (this.props.otp_status !== 1 && this.props.otp_status !== 3 && this.props.address_different && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.pob_image_not_captured);
        return;
      }


      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.address_different && this.props.kyc_pob_capture == null) {
        this.showAlert(this.state.pob_image_not_captured);
        return;
      }

      if (!this.props.alternateContactValidated) {
        this.showAlert(this.state.invalid_altenate_contact_no);
        return;
      }

      if ((this.props.otp_status !== 1 && this.props.otp_status !== 3) && this.props.signature == null) {
        this.showAlert(this.state.signature_not_captured);
        return;
      }

      if ((this.props.otp_status == 1 || this.props.otp_status == 3) && this.props.nic_validation.documnetExistInDataScan === 'N' && this.props.signature == null) {
        this.showAlert(this.state.signature_not_captured);
        return;
      }

      if (this.props.ezCashPIN.replace(/\s/g, '') == '') {
        this.showAlert(this.state.locals.please_enter_ezcash_code);
        return;
      } else if (isNaN(this.props.ezCashPIN.replace(/\s/g, ''))){
        this.showAlert(this.state.locals.ezcash_pin_must_only_numbers);
        return;
      } else if ((this.props.ezCashPIN.replace(/\s/g, '')).length !== 4){
        this.showAlert(this.state.locals.ezcash_pin_must_contain_4_numbers);
        return;
      }

      this.props.focusEzCashPin(false);
      let end_ts = Math.round((new Date()).getTime() / 1000);


      const data = {
        material_code: this.props.material_code,
        sim_serial_number: this.props.sim_serial_number,
        otp_status: this.props.otp_status,
        mobile_number: this.props.mobile_number,
        blocked_id: this.props.blocked_id_first,
        first_reload: this.props.first_reload,
        pre_post: this.props.pre_post,
        alternate_contact_number: this.props.alternate_contact_number,
        information_id_type: this.props.information_id_type,
        id_not_clear: this.props.information_not_clear,
        address_different: this.props.address_different,
        otp_number_list: this.props.otp_number_list,
        otp_selected_number: this.props.otp_number,
        reload_offer_code: this.props.reload_offer_code,
        start_ts: this.state.start_ts,
        end_ts: end_ts,
        id_reference: this.props.information_id_type === 'DR'
          ? 'DL'
          : (this.props.information_id_type === 'NIC'
            ? 'NIC'
            : 'PP'),
        cus_id: this.props.id_number,
        cus_type: this.props.local_foreign,
        document_exist: this.props.documentExist,
        imageTypeList: this.getImageTypeList(),
        email_address: this.props.email_address
        //packageDeposits
      };
      Object.assign(data,postPaidActivationParams);
      // data.merge(postPaidActivationParams);
      // this.props.getApiLoading(true);

      // if ((this.props.otp_status == 1 || this.props.otp_status == 3) 
      //       && this.props.nic_validation !== null 
      //       && this.props.nic_validation.documnetExistInDataScan === 'Y') {
      //   console.log('METHOD 1 -  PART FINAL API');
      //   this.onShowConfirm(data);
      // } else {
      console.log('METHOD 2 -  PART FINAL API');
      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      const payLoad = {
        connection: this.props.mobile_number,
        simSerialNumber: this.props.sim_serial_number,
        customerType: this.props.local_foreign
      };
      Analytics.logEvent('dsa_mobile_activation_tap_activate_postpaid', payLoad);
      Utill.apiRequestPost('getO2AOfferDetails', 'gsmPostpaidConnection', 'ccapp', data, this.onActivationSuccess, this.onActivationFailure, this.onActivationEx);

      // } 
    };

    showActivateSuccessModal = (response) => {
      console.log('xxx showActivateSuccessModal :: response', response);
      let data = response.data;
      let description = data.message;
      let description_title = data.mobile;
      
      let passProps = {
        primaryText: this.state.btnOk,
        primaryPress: this.goToHomePage,
        primaryPressPayload: data,
        disabled: false,
        hideTopImageView: false,
        icon: Images.icons.SuccessAlert,
        description: Utill.getStringifyText(description),
        descriptionTitle : description_title,
        descriptionTitleTextStyle: { alignSelf: 'flex-start' },
        primaryButtonStyle: { color: Colors.colorGreen },
        
      };
  
      console.log('showGeneralModal :: passProps ', passProps);
      Screen.showGeneralModal(passProps);
    };
  
  goToHomePage = () => {
    console.log('xxx goToHomePage');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const _this = this;
    setTimeout(() => {
      _this.props.resetMobileActivationState();
    }, 50);
  }

  
    //TODO : FINAL API
    onActivationSuccess = (response) => {
      console.log('xxxx onActivationSuccess');
      this.setState({ isLoading: false });
      this.props.getMobileActLoading(false);
      this.props.getApiResponseMobileAct(response);
      Analytics.logEvent('dsa_mobile_activation_success', {
        txId: response.data.tx_id,
        orderId: response.data.order_id,
        connection: response.data.mobile,
        activationType: this.props.information_id_type,
      });

      this.showActivateSuccessModal(response);


      if (this.props.information_id_type === 'NIC' && this.props.local_foreign !== 'foreign') {
        if (this.props.customer_information_image_nic_1 !== null || this.props.customer_information_image_nic_2 !== null) {
          this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image_nic_1.captureType, this.props.customer_information_image_nic_1.imageUri);
          this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image_nic_2.captureType, this.props.customer_information_image_nic_2.imageUri);
        }
      } else if (this.props.customer_information_image !== null) {
        this.addToCaptureDb(response.data.tx_id, this.props.customer_information_image.captureType, this.props.customer_information_image.imageUri);
      }

      this.props.kyc_customer_capture !== null
        ? this.addToCaptureDb(response.data.tx_id, this.props.kyc_customer_capture.captureType, this.props.kyc_customer_capture.imageUri)
        : true;
      this.props.kyc_pob_capture !== null
        ? this.addToCaptureDb(response.data.tx_id, this.props.kyc_pob_capture.captureType, this.props.kyc_pob_capture.imageUri)
        : true;
      this.props.signature !== null
        ? this.addToCaptureDb(response.data.tx_id, 7, this.props.signature.signatureUri)
        : true;
    }

    onActivationFailure = (response) => {
      this.setState({ isLoading: false });
      this.props.getMobileActLoading(false);
      if ( response.data.ez_error === 1 ){
        this.navigator.showModal({
          title: '',
          screen: 'DialogRetailerApp.models.EzCashErrorPopup',
          overrideBackPress: true,
          passProps: {
            mobile_number: this.props.defaultEzNumber,
            error_message: response.data.error
          }
        });
      } else {
        this.props.getApiResponseMobileAct(response);
        const payLoad = {
          connection: this.props.mobile_number,
          simSerialNumber: this.props.sim_serial_number,
          customerType: this.props.local_foreign
        };
        Analytics.logEvent('dsa_mobile_activation_failure', payLoad);
        this.navigator.showModal({
          screen: 'DialogRetailerApp.models.MobileActModel',
          overrideBackPress: true
        });
      }
    }

    onActivationEx = (error) => {
      this.setState({ isLoading: false });
      this.props.getMobileActLoading(false);
      const payLoad = {
        connection: this.props.mobile_number,
        simSerialNumber: this.props.sim_serial_number,
        customerType: this.props.local_foreign
      };
      Analytics.logEvent('dsa_mobile_activation_exception', payLoad);

      this.props.navigator.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: { message: error },
        overrideBackPress: true
      });
    }

    onPressCaptureNIC = () => {
      console.log('xxxx onPressCaptureNIC');
      this.props.navigator.showLightBox({
        screen: 'DialogRetailerApp.models.NicCaptureModel',
        passProps: {
          title: 'titleN',
          content: 'contentN',
          onClose: null
        },
        style: modelStyle,
        adjustSoftInput: 'resize'
      });
    };

    onPress = (key) => {
      console.log('onPress', key);
      // this.setState({ sectionClick1: false, sectionClick2: false, sectionClick3: false });
      // switch (key) {
      //     case 1:
      //         this.setState({
      //             sectionClick1: !this.state.sectionClick1
      //         });
      //         break;
      //     case 2:
      //         this.setState({
      //             sectionClick2: !this.state.sectionClick2
      //         });
      //         break;
      //     case 3:
      //         this.setState({
      //             sectionClick3: !this.state.sectionClick3
      //         });
      //         break;
      //     default:
      //         this.setState({ sectionClick1: true });
      //         break;
      // }
    };

    onClickInformationNotClear = () => {
      this.setState({
        idNotClearChecked: !this.state.idNotClearChecked
      });
      this
        .props
        .getInformationNotClearMobileAct(!this.props.information_not_clear);

      if (!this.state.idNotClearChecked) {
        this
          .props
          .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.CUSTOMER_IMAGE);

      } else {
        this.props.getKycCustomerCaptureMobileAct(null);
      }
    }

    onClickInformationNotClearImage = () => {

      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.CUSTOMER_IMAGE);

      this
        .props
        .navigator
        .push({
          title: 'Customer Image Capture',
          screen: 'DialogRetailerApp.views.CameraScreen',
          passProps: {
            onReturnView: 'some text',
            captureType: this.props.information_id_type
          }
        });

    }

    onClickPassportCaptureForeign = () => {
      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PASSPORT);
      this
        .props
        .navigator
        .push({
          title: 'Customer Image Capture',
          screen: 'DialogRetailerApp.views.CameraScreen',
          passProps: {
            onReturnView: 'some text',
            captureType: this.props.information_id_type
          }
        });
    }

    onClickAddressDifferent = () => {

      this.setState({
        addressDifferentChecked: !this.state.addressDifferentChecked
      });

      this
        .props
        .getDifferentAddressMobileAct(!this.props.address_different);

      if (!this.state.addressDifferentChecked) {
        this
          .props
          .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);
      } else {
        this.props.getKycPobCaptureMobileAct(null);
      }
    }

    onClickAddressDifferentImage = () => {

      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);

      this
        .props
        .navigator
        .push({
          title: 'POB Capture',
          screen: 'DialogRetailerApp.views.CameraScreen',
          passProps: {
            onReturnView: 'some text',
            captureType: this.props.information_id_type
          }
        });

    }

    onClickAddressCaptureForeign = () => {
      this
        .props
        .getCaptureIdTypeNoMobileAct(constants.imageCaptureTypes.PROOF_OF_BILLING);
      this
        .props
        .navigator
        .push({
          title: 'Address Capture',
          screen: 'DialogRetailerApp.views.CameraScreen',
          passProps: {
            onReturnView: 'some text',
            captureType: this.props.information_id_type
          }
        });
    }

    onCaptureSignature = () => {

      let screen = {
        title: 'SignatuerConfirmModal',
        id: 'DialogRetailerApp.models.SignatuerConfirmModal',
      };
  
      Utill.showModalView(screen, null, false);
    };


    // confimActivate = () => {
    //   console.log('xxxx confimActivate');
    //   const data = this.state.finalActivationData;
    //   this
    //     .props
    //     .getMobileActLoading(true);
    //   const payLoad = {
    //     connection: this.props.mobile_number,
    //     simSerialNumber: this.props.sim_serial_number,
    //     customerType: this.props.local_foreign
    //   };
    //   Analytics.logEvent('dsa_m_act_tap_postpaid', payLoad);
    //   Utill.apiRequestPost('getO2AOfferDetails', 'gsmPostpaidConnection', 'ccapp', data, this.onActivationSuccess, this.onActivationFailure, this.onActivationEx);
    // };

    // onShowConfirm = (data) => {
    //   console.log('xxxx onShowConfirm');
    //   this.setState({ finalActivationData: data });
    //   this.props.navigator
    //     .showModal({
    //       screen: 'DialogRetailerApp.models.SignatuerConfirmModal',
    //       title: 'SignatuerConfirmModal',
    //       passProps: { 
    //         message: 'sucess',
    //         isExsistingCustomer: true,
    //         finalActivationMethod : this.confimActivate
    //       },
    //       overrideBackPress: true
    //     });
    // };

    otpContinue = (value) => {
      this.props.otp_validation.otp_pin == value ? this.setState({ promptVisible: false }) : Alert.alert(JSON.stringify('OTP Validation Failed!'));
    }

    otpCancel = () => {
      this.setState({ promptVisible: false });
      this.props.getOtpStatusMobileAct(2);
      //this.props.resetMobileActivationState();  
    }

    onPressPackage = () => {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      this.props.navigator
        .showModal({
          screen: 'DialogRetailerApp.models.PackageInformation',
          title: 'GeneralModalException',
          passProps: { message: this.state.eligiblePackages , selectedPackage: this.state.selectedPackage },
          overrideBackPress: true
        });
    }


    releaseNumberSuccess = () => {
      console.log('releaseNumberSuccess');
    }

    releaseNumberFail = () => {
      console.log('releaseNumberFail');
      this
        .props
        .getSimNumberMobileAct('');
      this
        .props
        .getMobileNumberMobileAct('');
      this
        .props
        .getFirstReloadMobileAct('');
      this
        .props
        .resetMobileActNicValidation();
      this
        .props
        .resetMobileActModelPopped();
      this
        .props
        .resetValidateSimNumber();
      this
        .props
        .getSkipValidationMobileAct(false);

      //this.setState({ sectionClick2: false });
    }

    reserveNumberFailure = (response) => {
      this.showAlert(response.data.error);
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
    }

    reserveNumberEx = (error) => {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      this.props.navigator
        .showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: { message: error },
          overrideBackPress: true
        });
    }

    reserveNumberSuccess = (response) => {
      this.setState({ isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      this.props.getApiLoading(false); //should change this to true
      this
        .props
        .getMobileNumberMobileAct(response.data.data[0].value);
      const num_list = `${response.data.data[0].value}|O;`;

      this
        .props
        .getBlockedIdMobileAct(response.data.blockedId);

      const releaseData = {
        flag: 'first',
        blocked_id_first: response.data.blockedId,
        num_list
      };

      Utill.apiRequestPost('numberPoolRelease', 'gsmPostpaidConnection', 'ccapp', releaseData, this.releaseNumberSuccess, this.releaseNumberFail);
    }

    showValidationModel = () => {
      console.log('getO2AConnectionOsDetails Props: ', this.props.id_number);
      const osDetailsParams = { 
        identification_no: this.props.id_number,
        identification_type:  this.props.local_foreign === 'local'
          ? 'NIC'
          : 'PP'      
      };

      this.setState({ isLoading: true });
      this
        .props
        .getMobileActLoading(true);
      Utill.apiRequestPost('getO2AConnectionOsDetails', 'gsmPostpaidConnection', 'ccapp', osDetailsParams, this.outstandingSuccess, this.outstandingFail, this.outstandingEx);

    }

    outstandingSuccess = (response) => {
      console.log('xxx outstandingSuccess');
      this.setState({ sectionClick2: true, isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      console.log('OUTSTANDING RESPONSE :: data', JSON.stringify(response.data));
      console.log('OUTSTANDING RESPONSE :: info', response.data.info);
      console.log('OUTSTANDING RESPONSE :: is_empty', response.data.is_empty);

      if (!response.data.is_empty) {
        console.log('xxx outstandingSuccess :: NOT EMPTY');
        this.setState({ totalOutstanding : response.data.info[response.data.info.length - 1].totalOsAmount });
        this.setState({ d_connections : response.data.info });
        this.props.navigator.showModal({
          title: 'MobileActModel',
          screen: 'DialogRetailerApp.models.OutstandingModal',
          passProps: { outstandingData: response.data.info },
          overrideBackPress: true
        });
      } else {
        console.log('xxx outstandingSuccess :: EMPTY');
        if (this.props.notificationNumberListArray && this.props.isDocumnetExistInDataScan) {
          this
            .navigator
            .showModal({
              screen: 'DialogRetailerApp.models.GeneralModelMobileActPostPaid',
              title: 'Success',    
              overrideBackPress: true
            });
        }
      }   
    }

    outstandingFail = (response) => {
      console.log('xxxx outstandingFail');
      //Outstanding check failed. User will proceed with the normal mConnect OTP flow
      // this.setState({ sectionClick2: true, sectionClick1: false, isLoading: false }); // Section2 true
      this.setState({ sectionClick2: true, isLoading: false });
      this
        .props
        .getMobileActLoading(false);
      console.log('xxx outstandingFail', response);
      this.setState({ totalOutstanding : 0 }, ()=>{
        this.props.setTotalPaymentData({ outstandingFeeValue: '' });
      });
      Utill.showAlertMsg(JSON.stringify(response.data.error));
      
    }

    outstandingEx = (error) => {
      console.log('xxxx outstandingEx');
      this.setState({ isLoading: false, totalOutstanding : 0 }, ()=>{
        this.props.setTotalPaymentData({ outstandingFeeValue: '' });
      });
      this
        .props
        .getMobileActLoading(false);
      console.log('xxx outstandingEx', error);
      Utill.showAlertMsg(this.state.system_error);
    }

    checkBoxClick = (key) => {
      switch (key) {
        case 1:
          this.setState({
            sectionClick1: !this.state.sectionClick1
          });
          break;
        case 2:
          this.setState({
            sectionClick2: !this.state.sectionClick2
          });
          break;
        default:
          this.setState({ sectionClick1: true });
          break;
      }
    };

    idSelcetionRadioBtn = (key) => {
      console.log('xxx idSelcetionRadioBtn');
      switch (key) {
        case 1:
          this.setState({ captureContex: 'Capture NIC' });
          break;
        case 2:
          this.setState({ captureContex: 'Capture Passport' });
          break;
        case 3:
          this.setState({ captureContex: 'Capture Driving License' });
          break;
        default:
          this.setState({ captureContex: 'Capture NIC' });
          break;
      }
    };

    openModel = (screenId, titleN = null, contentN = null, onClickCb = null) => {
      console.log(`xxxx openModel${screenId}`);
      this
        .props
        .navigator
        .showLightBox({
          screen: screenId,
          passProps: {
            title: titleN,
            content: contentN,
            onClose: onClickCb
          },
          style: modelStyle,
          adjustSoftInput: 'resize'
        });
    };

    showAlert = (msg) => {
      Utill.showAlert(msg);
      Analytics.logEvent('dsa_mobile_act_show_alert');
    };

    addToCaptureDb = async (txnId, imageId, imageUri) => {
      console.log('xxx addToCaptureDb');
      let deviceData = await Utill.getDeviceAndUserData();
      try {
        const id1 = `${txnId}_${imageId}`;
        const url1 = Utill.createApiUrl('imageUploadEnc', 'initAct', 'ccapp');
        const imageFile1 = imageUri.replace('file://', '');
        const uploadSelector1 = 'encrypt';

        const options = {
          id: id1,
          url: url1,
          imageFile: imageFile1,
          uploadSelector: uploadSelector1,
          deviceData: JSON.stringify(deviceData)
        };
        const errorCb = (msg) => {
          console.log('xxxx addToCaptureDb errorCb ');
          Analytics.logEvent('dsa_m_act_sqlite_error');
          console.log(msg);
        };
        
        const successCb = (msg) => {
          console.log('xxxx addToCaptureDb successCb');
          Analytics.logEvent('dsa_m_act_sqlite_success');
          console.log(msg);
        };
  
        RNReactNativeImageuploader.addToDb(options, errorCb, successCb);

      } catch (e) {
        console.log('addToCaptureDb Image Uploading Failed', e.message);
        Analytics.logEvent('dsa_m_act_sqlite_ex');
        this.showAlert(e.message);
      }
    }

    packagesDropDown_adjustFrame(style) {
      style.height = 250;
      return style;
    }

    render() {
      //capture lable
      const captureNicLabelTxt = this.state.captureNicLabelTxt;
      const captureDlLabelTxt = this.state.captureDlLabelTxt;
      const capturePpLabelTxt = this.state.capturePpLabelTxt;
      const captureForeignAddress = this.state.captureForeignAddress;

      //captured lable
      const capturedNicLabelTxt = this.state.capturedNicLabelTxt;
      const capturedDlLabelTxt = this.state.capturedDlLabelTxt;
      const capturedPpLabelTxt = this.state.capturedPpLabelTxt;
      const capturedForeignAddress = this.state.capturedForeignAddress;

      //customer image not clear sub text
      const captureCustomerPhotoSubTxt = this.state.captureCustomerPhotoSubTxt;
      const captureProofOfDeliverySubTxt = this.state.captureProofOfDeliverySubTxt; 
      const capturedCustomerPhotoSubTxt = this.state.capturedCustomerPhotoSubTxt;
      const capturedProofOfDeliverySubTxt = this.state.capturedProofOfDeliverySubTxt;

      //customer image not clear lable
      const notClearNicLabelTxt = this.state.notClearNicLabelTxt;
      const notClearDlLabelTxt = this.state.notClearDlLabelTxt;
      const notClearPpLabelTxt = this.state.notClearPpLabelTxt;

      //capture pob lable
      const pobNicLabelTxt = this.state.pobNicLabelTxt;
      const pobDlLabelTxt = this.state.pobDlLabelTxt;
      const pobPpLabelTxt = this.state.pobPpLabelTxt;

      let idLabel;
      let idLabelInput;
      let captureLabelTxt;
      let capturedLabelTxt;
      let cusImageNotClearTxt;
      let pobLabelTxt;
      let noOfImages;
      let isImageCaptured;
      let isImageCapturedForeignPp;
      let isImageCapturedForeignAddress;
      let isImageCapturedLocalAddress;
      let isImageCapturedCustomer;
      let iconValidate;
      let idIconSize;

      if (this.props.local_foreign === 'local') {
        idLabel = this.state.nicValidate;
        idLabelInput = this.state.idLabelInputNic;
        iconValidate = 'mode-edit';
        idIconSize = 26;
      } else {
        idLabel = this.state.passValidate;
        idLabelInput = this.state.idLabelInputPp;
        iconValidate = 'chevron-circle-right';
        idIconSize = Constants.icon.next.defaultFontSize;
      }

      if (this.props.information_id_type === 'NIC') {
        console.log('Triggering NIC Section');
        captureLabelTxt = captureNicLabelTxt;
        capturedLabelTxt = capturedNicLabelTxt;
        cusImageNotClearTxt = notClearNicLabelTxt;
        pobLabelTxt = pobNicLabelTxt;
        isImageCaptured = (this.props.customer_information_image_nic_1 === null 
          || this.props.customer_information_image_nic_2 === null);
        isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
        isImageCapturedCustomer = this.props.kyc_customer_capture === null;
        noOfImages = 2;
      } else if (this.props.information_id_type === 'DR') {
        console.log('Triggering DR Section');
        captureLabelTxt = captureDlLabelTxt;
        capturedLabelTxt = capturedDlLabelTxt;
        cusImageNotClearTxt = notClearDlLabelTxt;
        pobLabelTxt = pobDlLabelTxt;
        isImageCaptured = this.props.customer_information_image === null;
        isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
        isImageCapturedCustomer = this.props.kyc_customer_capture === null;
        noOfImages = 1;
      } else {
        console.log('Triggering PP Section');
        captureLabelTxt = capturePpLabelTxt;
        capturedLabelTxt = capturedPpLabelTxt;
        cusImageNotClearTxt = notClearPpLabelTxt;
        pobLabelTxt = pobPpLabelTxt;
        isImageCaptured = this.props.customer_information_image === null;
        isImageCapturedLocalAddress = this.props.kyc_pob_capture === null;
        noOfImages = 1;
      }
      //doom

      if (this.props.local_foreign === 'foreign') {
        noOfImages = 1;
        isImageCapturedForeignPp = this.props.customer_information_image === null;
        isImageCapturedForeignAddress = this.props.kyc_pob_capture === null;
      }

      console.log('captureLabelTxt', captureLabelTxt);
      console.log('information_id_type', this.props.information_id_type);
      let outstanding_fee_value = Utill.checkNumber(this.state.totalOutstanding);

      const totalPaymentProps = { outstanding_fee: outstanding_fee_value };

      let activeStyle = styles.containerPicker;
      if (this.props.userSelectedPackageIndex == -1) {
        activeStyle = styles.containerPicker;
      } else {
        activeStyle = styles.containerPickerActive;
      }
      return (
        <View style={styles.container}>
          <InputSection
            key={1}
            label={idLabel}
            colorCode={this.props.firstValidated}
            onPress={()=>{ 
              Keyboard.dismiss;
              // if (this.props.firstValidated)
              //   this.setState({ sectionClick1: !this.state.sectionClick1, sectionClick2: false, sectionClick3: false });
            }}
          />{this.state.sectionClick1
            ? <SectionContainer >
              <MaterialInput
                label={idLabelInput}
                index={'NIC'}
                ref={'idNumber'}
                iconIndex={this.props.local_foreign}
                onPress={() => this.onPressPassport()}
                icon={iconValidate}
                iconSize={idIconSize}
                iconTap
              />
            </SectionContainer>
            : <View />
          }
          {/* {this.state.isLoading ? <ActIndicator animating /> : <View />} */}
          <InputSection
            key={2}
            label={this.state.productInfo}
            colorCode={this.props.firstValidated && this.props.secondValidated 
              && this.props.thirdValidated}
            onPress={()=>{ 
              Keyboard.dismiss;
              // if (this.props.firstValidated)
              //   this.setState({ sectionClick2: !this.state.sectionClick2, sectionClick1: false, sectionClick3: false  });
            }}
          />{this.state.sectionClick2
            ? <View>
              <SectionContainer >
                <MaterialInput
                  label={this.state.simNo}
                  index={'SIM No'}
                  icon={'mode-edit'}
                  onPress={() => this.onPressSIM()}
                  editable={false}
                  iconTap={this.props.firstValidated}
                />
              </SectionContainer>
              <SectionContainer >
                <MaterialInput
                  label={this.state.mobileNo}
                  index={'Mobile No'}
                  icon={'mode-edit'}
                  onPress={() => this.onPressMobile()}
                  editable={false}
                  iconTap={this.props.firstValidated && this.props.secondValidated}
                />
              </SectionContainer>
              <SectionContainer>                
                <View style={activeStyle}>  
                  <View style={styles.dropdownContainerTextStyle}><Text style={styles.packageStyle}>{this.state.package}</Text></View>      
                  <View style={styles.dropdownContainerStyle}>
                    <View style={styles.viewStyle} key={ this.props.userSelectedPackageIndex == -1 ? 0 : this.props.userSelectedPackageIndex } >
                      <ModalDropdown 
                        disabled={!this.props.didPackageLoad} 
                        options={this.state.dropDownData} 
                        defaultIndex={ this.props.userSelectedPackageIndex } 
                        adjustFrame={style => this.packagesDropDown_adjustFrame(style)} 
                        // renderRow={style => this.packagesDropDown_adjustRowFrame(style)} 
                        onSelect={(idx, value) => this.updatePackage(idx, value)} 
                        style={styles.dropdownButtonStyle} 
                        dropdownStyle={styles.dropdownFallStyle} 
                        dropdownTextStyle={styles.dropdownTextStyle} 
                        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle} >
                        <View style={styles.insideDropdownStyle}>
                          <View style={styles.leftViewStyle}>
                            <Text style={styles.dropTextStyle}>{this.props.selectedPackage.pkg_NAME}</Text>
                          </View>
                          <View style={styles.rightViewStyle}>
                            <Ionicons name='md-arrow-dropdown' size={20} />
                          </View>
                        </View>
                      </ModalDropdown>
                    </View>
                    <View style={styles.infoBtnContainer}>
                      <TouchableOpacity onPress={() => this.onPressPackage()} disabled={(this.props.userSelectedPackageIndex == -1)}>
                        <Image
                          source={require('../../../images/icons/iIcon.png')}
                          style={styles.dropdownIconStyle}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </SectionContainer>
              <SectionContainer>
                <MaterialInput
                  label={this.state.lcd}
                  index={'lcd'}
                  onPress={() => { console.log('lcd'); }}
                  editable={false}
                  iconTap={false}
                />
              </SectionContainer>
            </View>
            : <View />
          }
          <InputSection
            key={3}
            label={this.state.custInfo}
            colorCode={this.props.firstValidated && this.props.secondValidated 
              && this.props.thirdValidated && this.props.signatureCaptured}
            onPress={()=>{ 
              Keyboard.dismiss;
              // if (this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated)
              //   this.setState({ sectionClick3: !this.state.sectionClick3, sectionClick1: false, sectionClick2: false });
            }}
          />{this.state.sectionClick3 
            ? 
            <View>
              {(this.props.local_foreign === 'local' && this.props.otp_status !== 1 
                            && this.props.otp_status !== 3)
                            || ((this.props.otp_status == 1 
                            || this.props.otp_status == 3) 
                            && this.props.local_foreign === 'local' 
                            && this.props.nic_validation.documnetExistInDataScan === 'N')
                ? 
                <View>
                  <SectionContainer >
                    <IdRadioButtons />
                  </SectionContainer>
                  {isImageCaptured ?
                    <CaptureId
                      label={captureLabelTxt}
                      onPress={() => {
                        Keyboard.dismiss();
                        this.onPressNIC();
                      }}
                    />
                    : <View>{noOfImages === 2 ?
                      <CaptureTwoThumbnails
                        imageCount={noOfImages}
                        label={capturedLabelTxt}
                        onPress1={() => {
                          Keyboard.dismiss();
                          this.onPressReCapture(1);
                        }}
                        onPress2={() => {
                          Keyboard.dismiss();
                          this.onPressReCapture(2);
                        }}
                      />
                      :
                      <CaptureThumbnails
                        imageCount={1}
                        label={capturedLabelTxt}
                        onPress={() => {
                          Keyboard.dismiss();
                          this.onPressReCapture();
                        }}
                      />
                    }
                    </View>
                  }
                  <SectionContainer />
                  {
                    this.props.information_id_type !== 'PP' ?
                      <View>
                        {/* ImageCapturelogic start - loacl and NIC or DL*/}
                        {/* Customer Image Capture - checkbox (NIC or Driving Licence) */}
                        <View style={styles.checkViewOne}>
                          <View>
                            <CheckBox
                              style={styles.checkBox1}
                              onClick={this.onClickInformationNotClear}
                              isChecked={this.state.idNotClearChecked}
                            />
                          </View>
                          <View style={styles.checkViewFour}>
                            <Text
                              style={styles.checkBox2}
                            >
                              {cusImageNotClearTxt}
                            </Text>
                          </View>
                        </View>
                        {/* Customer Image Capture - camera view (NIC or Driving Licence) */}
                        {this.state.idNotClearChecked ? <View style={ styles.faceNotClearTextLabel }>
                          {isImageCapturedCustomer ?
                            <CaptureId
                              label={captureCustomerPhotoSubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickInformationNotClearImage();
                              }}
                            />
                            :
                            <CaptureThumbnails
                              imageCount={1}
                              label={capturedCustomerPhotoSubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickInformationNotClearImage();
                              }}
                            />
                          }
                        </View> : true}
                        {/* POB capture - chekbox (NIC or Driving Licence)*/}
                        <View style={styles.checkViewOne} >
                          <View>
                            <CheckBox
                              style={styles.checkBox3}
                              onClick={this.onClickAddressDifferent}
                              isChecked={this.state.addressDifferentChecked}
                            />
                          </View>
                          <View style={styles.checkViewFour}>
                            <Text
                              style={styles.checkBox4}
                            >
                              {pobLabelTxt}
                            </Text>
                          </View>
                        </View>
                        {/* POB capture - camera view (NIC or Driving Licence) */}
                        {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                          {isImageCapturedLocalAddress ?
                            <CaptureId
                              label={captureProofOfDeliverySubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickAddressDifferentImage();
                              }}
                            />
                            :
                            <CaptureThumbnails
                              imageCount={1}
                              label={capturedProofOfDeliverySubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickAddressDifferentImage();
                              }}
                            />
                          }
                        </View> : true}
                        {/* ImageCapturelogic end - local and NIC or DL*/}
                        {/* ImageCapturelogic start  - local PP*/}
                      </View> :
                      <View>
                        {isImageCapturedLocalAddress ?
                          <CaptureId
                            label={captureForeignAddress}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressCaptureForeign();
                            }}
                          />
                          :
                          <CaptureThumbnails
                            imageCount={1}
                            label={capturedForeignAddress}
                            onPress={() => {
                              Keyboard.dismiss();
                              this.onClickAddressCaptureForeign();
                            }}
                          />
                        }
                      </View>
                  }
                </View>
                :
                //Document Scan Y Logic Start (For OTP and MConnect verified)
                <View>
                  {
                    this.props.local_foreign == 'local' 
                    && this.props.nic_validation.documnetExistInDataScan === 'Y' ?
                      <View>
                        {/* POB capture - checkbox (NIC or Driving License)*/}
                        <View style={styles.checkViewOne} >
                          <View>
                            <CheckBox
                              style={styles.checkBox3}
                              onClick={this.onClickAddressDifferent}
                              isChecked={this.state.addressDifferentChecked}
                            />
                          </View>
                          <View style={styles.checkViewFour}>
                            <Text
                              style={styles.checkBox4}
                            >
                              {pobLabelTxt}
                            </Text>
                          </View>
                        </View>
                        {/* POB capture - camera view (NIC or Driving License) */}
                        {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                          {isImageCapturedLocalAddress ?
                            <CaptureId
                              label={captureProofOfDeliverySubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickAddressDifferentImage();
                              }}
                            />
                            :
                            <CaptureThumbnails
                              imageCount={1}
                              label={capturedProofOfDeliverySubTxt}
                              onPress={() => {
                                Keyboard.dismiss();
                                this.onClickAddressDifferentImage();
                              }}
                            />
                          }
                        </View> : true}
                      </View> :
                      //Else Should Have the Passport Logic
                      //Foreigner Logic Start
                      <View>
                        {
                          (this.props.otp_status !== 1 && this.props.otp_status !== 3)
                              || ((this.props.otp_status == 1 
                              || this.props.otp_status == 3) 
                              && this.props.nic_validation !== null 
                              && this.props.nic_validation.documnetExistInDataScan === 'N') ?
                            <View>
                              {isImageCapturedForeignPp ?
                                <CaptureId
                                  label={capturePpLabelTxt}
                                  onPress={() => {
                                    Keyboard.dismiss();
                                    this.onClickPassportCaptureForeign();
                                  }}
                                />
                                :
                                <CaptureThumbnails
                                  imageCount={1}
                                  label={capturedPpLabelTxt}
                                  onPress={() => {
                                    Keyboard.dismiss();
                                    this.onClickPassportCaptureForeign();
                                  }}
                                />
                              }

                              {isImageCapturedForeignAddress ?
                                <CaptureId
                                  label={captureForeignAddress}
                                  onPress={() => {
                                    Keyboard.dismiss();
                                    this.onClickAddressCaptureForeign();
                                  }}
                                />
                                :
                                <CaptureThumbnails
                                  imageCount={1}
                                  label={capturedForeignAddress}
                                  onPress={() => {
                                    Keyboard.dismiss();
                                    this.onClickAddressCaptureForeign();
                                  }}
                                />
                              }
                            </View>
                            :
                            //true
                            //Drop2 New Change Below
                            <View>
                              {
                                ((this.props.otp_status == 1 || this.props.otp_status == 3) 
                                && this.props.nic_validation !== null 
                                && this.props.nic_validation.documnetExistInDataScan === 'Y') ?
                                  <View>
                                    {/* POB capture - checkbox (For Passport)*/}
                                    <View style={styles.checkViewOne} >
                                      <View>
                                        <CheckBox
                                          style={styles.checkBox3}
                                          onClick={this.onClickAddressDifferent}
                                          isChecked={this.state.addressDifferentChecked}
                                        />
                                      </View>
                                      <View style={styles.checkViewFour}>
                                        <Text
                                          style={styles.checkBox4}
                                        >
                                          {pobLabelTxt}
                                        </Text>
                                      </View>
                                    </View>
                                    {/* POB capture - camera view (For Passport) */}
                                    {this.state.addressDifferentChecked ? <View style={ styles.faceNotClearTextLabel }>
                                      {isImageCapturedLocalAddress ?
                                        <CaptureId
                                          label={captureProofOfDeliverySubTxt}
                                          onPress={() => {
                                            Keyboard.dismiss();
                                            this.onClickAddressDifferentImage();
                                          }}
                                        />
                                        :
                                        <CaptureThumbnails
                                          label={capturedProofOfDeliverySubTxt}
                                          onPress={() => {
                                            Keyboard.dismiss();
                                            this.onClickAddressDifferentImage();
                                          }}
                                        />
                                      }
                                    </View> : true}
                                  </View>
                                  : true
                              }
                            </View>
                        }
                      </View>
                    //Foreigner Logic End
                  }
                </View>
                //Document Scan Y Logic End
              }
              <SectionContainer >
                <MaterialInput
                  label={this.state.alternateContact}
                  ref={'alternateContact'}
                  isNeedToFocus
                  iconTap
                  index={'Alternative Contact No (Optional)'}
                  icon={'mode-edit'}
                  onPress={() => console.log('Alternate')}
                  maxLength={10}
                  keyboardType={'numeric'}
                />
              </SectionContainer>
              <SectionContainer >
                <MaterialInput
                  label={this.state.email}
                  ref={'email'}
                  isNeedToFocus
                  iconTap
                  index={'Email (Optional)'}
                  icon={'mode-edit'}
                  onPress={() => console.log('Email')}
                  keyboardType={'email-address'}
                />
              </SectionContainer>
              {/* {((this.props.otp_status !== 1 && this.props.otp_status !== 3)
                            || ((this.props.otp_status == 1 
                            || this.props.otp_status == 3) 
                            && this.props.nic_validation !== null 
                            && this.props.nic_validation.documnetExistInDataScan == 'N')) 
                            && (this.props.selectedPackage.pkg_CODE !== "")? */}
              <View style={styles.bottomContainer}>
                <View style={styles.bottomInnerContainerSignature}>
                  <TouchableOpacity
                    style={styles.signatureContainer}
                    onPress={() => this.onCaptureSignature()}
                  >

                    {this.props.signature !== null
                      ? <Image
                        style={styles.signatureImage}
                        key={this.props.signature.signatureRand}
                        source={{
                          uri: `data:image/jpg;base64,${this.props.signature.signatureBase}`
                        }}
                        resizeMode="stretch"
                      />
                      : <Text style={styles.signatureTxt}>
                        {this.state.customerSignature}
                      </Text>
                    }
                  </TouchableOpacity>
                </View>
              </View> 
       
              <TotalPayments props={ totalPaymentProps }/>
              {this.props.signature ||  ((this.props.otp_status == 1 || this.props.otp_status == 3) 
                && this.props.nic_validation !== null 
                && this.props.nic_validation.documnetExistInDataScan === 'Y')?
                <View style={styles.bottomInnerContainer}>
                  <View style={styles.ezCashViewStyle}>
                    <Text style={styles.textStyle}>{this.state.ezCashPin}</Text>
                  </View>
                  <MaterialInput
                    label=''
                    ref={'ezcashPin'}
                    secureTextEntry
                    title={this.props.defaultEzNumber == "" ? `eZ Cash Account PIN` : `(${this.props.defaultEzNumber}) eZ Cash Account PIN`}
                    index={'eZ cash PIN'}
                    maxLength={4}
                    manualFocus={this.props.focusEzCashInput}
                    keyboardType={'numeric'}
                  />
                </View>
                : true }
            </View>
            : <View />}
          <View style={styles.activateBtnContainer}>
            <View style={styles.activatDummy} />
            <TouchableOpacity
              style={[
                styles.activateBtn, {
                  backgroundColor: this.state.activateBtnColor
                }
              ]}
              onPress={() => this.debouncedFinalActivation()}
              disabled={!(this.props.firstValidated 
                && this.props.secondValidated 
                && this.props.thirdValidated) 
                || 
                (this.props.selectedPackage.pkg_CODE == '' 
                && this.props.packageDeposits.depositAmount == '')
              }
            >
              <Text
                style={[
                  styles.activateBtnTxt, {
                    color: this.state.activateBtnTxtColor
                  }
                ]}
              >
                {this.state.BtnTxt}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
}

const mapStateToProps = state => {
  Orientation.lockToPortrait();
  console.log('****** REDUX STATE :: BottomItemsPost SAME => MOBILE ', state.mobile);
  const idLabel = '';
  const idLabelInput = '';
  let nicValidation = null;
  let firstValidated = false;
  let simValidated = false;
  let secondValidated = false;
  let thirdValidated = false;
  let fourthValidated = false;
  let signatureCaptured = false;
  let alternateContactValidated = true;
  let nicApiFail = false;
  let simApiFail = false;
  let documentExist = '';
  let ezCashPIN = state.mobile.ezCashPIN;

  const id_number = state.mobile.id_number;
  const sim_number = state.mobile.sim_number;
  const mobile_number = state.mobile.mobile_number;
  const local_foreign = state.mobile.local_foreign;
  const nic_validation = state.mobile.nic_validation;
  const nic_api_fail = state.mobile.nic_api_fail;
  const sim_api_fail = state.sim.sim_api_fail;
  const sim_validation = state.sim.sim_validation;
  const skip_validation = state.mobile.skip_validation;
  const model_popped = state.mobile.model_popped;
  const otp_popped = state.mobile.otp_popped;
  const otp_validation = state.mobile.otp_validation;
  const information_id_type = state.mobile.information_id_type;
  const address_different = state.mobile.address_different;
  const information_not_clear = state.mobile.information_not_clear;
  const customer_information_image = state.mobile.kyc_capture;
  const customer_information_image_nic_1 = state.mobile.kyc_capture_nic_1;
  const customer_information_image_nic_2 = state.mobile.kyc_capture_nic_2;
  const alternate_contact_number = state.mobile.alternate_contact_number;
  const signature = state.mobile.signature;
  const kyc_pob_capture = state.mobile.kyc_pob_capture;
  const kyc_customer_capture = state.mobile.kyc_customer_capture;
  const otp_number = state.mobile.otp_number;
  const selectedPackage = state.mobile.selectedPackage; 
  const packageDeposits = state.mobile.packageDeposits;
  const userSelectedPackageIndex = state.mobile.userSelectedPackageIndex;
  // const material_code = state.mobile.material_code.material_code;
  // const isEnablePackageInfo = (userSelectedPackageIndex==-1);
  const didPackageLoad =  state.mobile.didPackageLoad;
  const email_address = state.mobile.email_address;

  // console.log('xxxxxxxxxxxx isEnablePackageInfo xxxxxxxxxxxxxxx', isEnablePackageInfo);
  console.log('xxxxxxxxxxxx isPackageLoaded xxxxxxxxxxxxxxx', didPackageLoad);

  nic_validation !== null ? documentExist = nic_validation.documnetExistInDataScan : documentExist = '';

  if (state.mobile.local_foreign === 'local' && (Utill.nicValidate(state.mobile.id_number).length === 10 || 
  Utill.nicValidate(state.mobile.id_number).length == 12)) {
    firstValidated = true;
  } else if (state.mobile.local_foreign === 'foreign' && state.mobile.id_number.length > 4) {
    !state.mobile.id_number.match(/^[a-zA-Z0-9]+$/) ? firstValidated = false : firstValidated = true;
  } else {
    firstValidated = false;
  }

  if (nic_validation == null && nic_api_fail) {
    nicApiFail = true;
  }

  if (state.mobile.sim_number.length === 8) {
    simValidated = true;
  } else {
    simValidated = false;
  }

  if (sim_validation == null && sim_api_fail) {
    simApiFail = true;
    simValidated = false;
  }

  if (state.sim.sim_validation !== null && state.sim.sim_validation.success) {
    secondValidated = true;
  } else {
    secondValidated = false;
  }

  if (state.mobile.mobile_number.length === 9 || state.mobile.mobile_number.length === 10 || state.mobile.mobile_number.length === 11) {
    thirdValidated = true;
  } else {
    thirdValidated = false;
  }

  if (state.mobile.packageDeposits.depositAmount !==''){
    thirdValidated = true;
  } else {
    thirdValidated = false;
  }

  // if (state.mobile.ezCashPIN !== "") {
  //   thirdValidated = true;
  // } else {
  //   thirdValidated = false;
  // }

  if (state.mobile.nic_validation !== null) {
    nicValidation = state.mobile.nic_validation;
  } else {
    nicValidation = null;
  }

  // if (state.mobile.local_foreign === 'local') {
  //     idLabel = this.state.nicValidate;
  //     idLabelInput = 'NIC';
  // } else {
  //     idLabel = this.state.passValidate;
  //     idLabelInput = 'Passport';
  // }


  if (firstValidated && secondValidated 
    && thirdValidated 
    && local_foreign == 'local' 
    && nic_validation !== null 
    && nic_validation.documnetExistInDataScan === 'N' 
    && information_id_type !== 'NIC' 
    && customer_information_image === null) {
    fourthValidated = false;
  } else {
    fourthValidated = true;
  }

  if (firstValidated && secondValidated && thirdValidated && alternate_contact_number !== '') {
    Number.isInteger(Number(alternate_contact_number))
      ? alternateContactValidated = true
      : alternateContactValidated = false;
  } else {
    alternateContactValidated = true;
  }

  if (state.mobile.signature !== null) {
    signatureCaptured = true;
  }

  const material_code = state.sim.sim_validation !== null && state.sim.sim_validation.data.material_code !== null && state.sim.sim_validation.data.material_code.length >= 1
    ? state.sim.sim_validation.data.material_code
    : null;

  const sim_serial_number = state.sim.sim_validation !== null
    ? state.sim.sim_validation.data.sim_serial_number
    : null;
  const otp_status = state.mobile.otp_status;
  const blocked_id_first = state.mobile.blocked_id_first;
  const pre_post = state.mobile.pre_post;
  const first_reload = state.mobile.first_reload;
  const otp_number_list = (state.mobile.otp_status && state.mobile.nic_validation !== null)
    ? state.mobile.nic_validation.notificationNumberList
    : null;
  const reload_offer_code = state.mobile.reload_offer_code;
  console.log('simApiFail', simApiFail);

  if (otp_status !== 1 && otp_status !== 3 && firstValidated && secondValidated && thirdValidated && local_foreign == 'local' && nic_validation !== null && (nic_validation.documnetExistInDataScan === 'N' || nic_validation.documnetExistInDataScan === 'Y') && information_id_type !== 'NIC' && customer_information_image === null) {
    fourthValidated = false;
  } else if ((otp_status == 1 || otp_status == 3) && firstValidated && secondValidated && thirdValidated && local_foreign == 'local' && nic_validation !== null && (nic_validation.documnetExistInDataScan === 'N') && information_id_type !== 'NIC' && customer_information_image === null) {
    fourthValidated = false;
  } else {
    fourthValidated = true;
  }

  let notificationNumberListArray = false;
  let isDocumnetExistInDataScan =  false;

  if (state.mobile.nic_validation !==  null 
    && state.mobile.nic_validation.notificationNumberList != undefined
    && state.mobile.nic_validation.documnetExistInDataScan !== null
    && state.mobile.nic_validation.notificationNumberList !== null) {
    console.log('xxx OTP NUMBERS TEST');
    notificationNumberListArray = state.mobile.nic_validation.notificationNumberList.length !== 0;
    isDocumnetExistInDataScan = state.mobile.nic_validation.documnetExistInDataScan === 'Y';
  }
  const focusEzCashInput = state.mobile.focusEzCashInput;

  return {
    Language: state.lang.current_lang,
    notificationNumberListArray,
    isDocumnetExistInDataScan,
    idLabel,
    idLabelInput,
    nicValidation,
    id_number,
    local_foreign,
    firstValidated,
    nic_validation,
    nic_api_fail,
    sim_validation,
    nicApiFail,
    simApiFail,
    sim_number,
    mobile_number,
    simValidated,
    skip_validation,
    model_popped,
    otp_popped,
    secondValidated,
    thirdValidated,
    fourthValidated,
    information_id_type,
    information_not_clear,
    address_different,
    customer_information_image,
    customer_information_image_nic_1,
    customer_information_image_nic_2,
    alternateContactValidated,
    alternate_contact_number,
    material_code,
    sim_serial_number,
    otp_status,
    otp_number,
    otp_validation,
    blocked_id_first,
    pre_post,
    first_reload,
    otp_number_list,
    reload_offer_code,
    signature,
    signatureCaptured,
    kyc_customer_capture,
    kyc_pob_capture,
    documentExist,
    selectedPackage,
    packageDeposits,
    ezCashPIN,
    userSelectedPackageIndex,
    // isEnablePackageInfo,
    didPackageLoad,
    email_address,
    focusEzCashInput
  };
};

const modelStyle = {
  backgroundBlur: 'dark',
  backgroundColor: 'rgba(0, 0, 0, 0.7)',
  tapBackgroundToDismiss: true
};
const modelStyleNoSelection = {
  backgroundBlur: 'dark',
  backgroundColor: 'rgba(0, 0, 0, 0.7)',
  tapBackgroundToDismiss: false
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  bottomContainer: {
    flex: 1,
    marginTop: 10,
    height: 170,
    marginLeft: 18,
    marginRight: 18,
    // backgroundColor: 'green'
  },
  bottomInnerContainer: {
    flex: 1,
    height: 50,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingTop: 20,
    marginLeft: 35,
    // alignItems: 'center', backgroundColor: 'yellow'
  },

  bottomInnerContainerSignature:{
    flex: 1,
    height: 50,
    margin: 10,
    paddingTop: 20,
    marginLeft: 20,
    flexDirection: 'column',
    justifyContent: 'flex-end',
   
  },

  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    margin: 10,
    borderWidth: 1,
    borderColor: Colors.btnDeactiveTxtColor,
    borderRadius: 3,

  },
  activateBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 10,
    marginTop: 70,
    height: 50,
    //backgroundColor: '#155'
  },
  activatDummy: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
    //backgroundColor: '#185',
  },
  activateBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // padding: 10,
    borderRadius: 5,
    //backgroundColor: Colors.btnDeactive, borderWidth: 1, borderColor: '#808080',

  },
  activateBtnTxt: {
    fontSize: Styles.defaultBtnFontSize
  },
  signatureTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  checkViewOne: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10,
    marginLeft: 12
  },

  checkViewFour: {
    flex: 1,
    borderWidth: 0,
    flexDirection: 'row'
  },
  signatureImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: 70 
  },
  checkBox1: {
    padding: 0,
    paddingBottom: 3,
    paddingRight: 5,
    marginLeft: 30

  },

  checkBox2: {
    fontSize: 14,
    paddingBottom: 3,
    marginRight: 10

  },
  checkBox3: {
    padding: 0,
    paddingRight: 5,
    marginLeft: 30

  },
  containerPickerActive:{
    //flex:1.5,
    height: 100,
    marginLeft: 15,
    marginRight: 0,
    //backgroundColor: 'blue'
  },
  containerPicker:{
    flex: 1,
    marginLeft: 15,
    marginRight: 0,
  },
  infoBtnContainer:{
    width: '13%',
    // backgroundColor: 'yellow',
    justifyContent: 'flex-end'
  },
  checkBox4: {
    fontSize: 14,
    marginRight: 10

  },
  textStyle: {
    fontSize: 17,
    //color: '#000'
  },
  packageStyle: {
    fontSize: 17,
    color: Colors.black
  },

  dropdownContainerTextStyle: {
    flex:0.2,
    marginRight: Styles.matirialInput.marginRight

  },
  dropdownContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-between',
    flex:1,
    marginRight: Styles.matirialInput.marginRight
    // backgroundColor: 'green'
  },
  viewStyle: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
    width: '87%'
  },
  dropdownButtonStyle: {
    width: '100%'
  },
  dropdownFallStyle: {
    width: '70%',
    height: 180
  },
  dropdownTextStyle: {
    color: Colors.black,
    fontSize: 15
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  insideDropdownStyle: {
    flexDirection: 'row',
    // width: '100%'
  },
  leftViewStyle: {
    width: '70%'
  },
  dropTextStyle: {
    color: Colors.black,
    fontSize: 15,
    // width: '70%'
  },
  rightViewStyle: {
    width: '30%',
    alignItems: 'flex-end',
    paddingRight: 10
  },
  dropdownIconStyle: {
    height: 22,
    width: 22,
    // marginLeft: 5,
    right:0,
    alignSelf: 'flex-end'
  },
  ezCashViewStyle: {
    paddingLeft: 15,
    marginBottom: -17
  },
  faceNotClearTextLabel: { 
    marginTop: -10, 
    top: -10, 
    marginLeft: 28,
    bottom: -20,
    backgroundColor: Colors.transparent
  }
});

export default connect(mapStateToProps, actions)(BottomItemsPost);