import React from 'react';
import { StyleSheet, View, TouchableOpacity, Keyboard } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

class MaterialInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  inputChanged = (text) => {
    console.log("BottomItemsPost MaterialInput Changed", text);
    if (this.props.index === 'NIC' || this.props.index === 'Passport') {
      this
        .props
        .getIdNumberMobileAct(text);
    } else if (this.props.index === 'SIM No') {
      this
        .props
        .getSimNumberMobileAct(text);
    } else if (this.props.index === 'Mobile No') {
      this
        .props
        .getMobileNumberMobileAct(text);
    } else if (this.props.index === 'First Reload Offer') {
      this
        .props
        .getFirstReloadMobileAct(text);
    } else if (this.props.index === 'Alternative Contact No (Optional)') {
      this
        .props
        .getAlternateContactNumberMobileAct(text);
    } else if (this.props.index === 'Email (Optional)') {
      this
        .props
        .getEmailAdressMobileAct(text);
    } else if (this.props.index === 'eZ cash PIN') {
      this
        .props
        .getEzCashPinMobileAct(text);
      console.log('eZ Cash Account PIN', text);
    }
  }

  onFocus = () => {
    console.log('xxxxxxxxxxxxxx onFocus xxxxxxxxxxxxxx');
  }

  onBlur = () => {
    console.log('xxxxxxxxxxxxxx onBlur xxxxxxxxxxxxxxx');
  }

  onSelectionChange = () => {
    console.log('xxxxxxxxxxxx onSelectionChange xxxxxxxx');
  }

  onSubmitEditing = () => {
    console.log('xxxxxxxxxxxx onSubmitEditing xxxxxxxxx');
    this.onBlur();
  }

  render() {
    const {
      customStyle,
      label,
      icon,
      iconSize,
      iconIndex,
      onPress,
      editable,
      iconTap,
      keyboardType,
      maxLength,
      secureTextEntry,
      ref,
      isNeedToFocus,
      manualFocus,
      manualBlur
    } = this.props;

    let refTextInput = ref;

    const keyboardAwareOnPress = function () {
      if (isNeedToFocus) {
        refTextInput.focus();
      } else if (iconIndex !== 'foreign') {
        refTextInput.focus();
      } else {
        Keyboard.dismiss();
      }

      onPress.apply(this, arguments);
    };

    let rightIcon;
    if (this.props.index === 'SIM No') {
      rightIcon = (<MaterialCommunityIcons
        name="barcode"
        size={30}
        color={Colors.defaultIconColorBlack}/>);
    } else if (this.props.index === 'NIC' && iconIndex == 'foreign') {
      rightIcon = (<FontAwesome name={icon} size={iconSize ? iconSize : 26} color={Colors.defaultIconColorBlack}/>);

    } else {
      rightIcon = (<MaterialIcons name={icon} size={25} color={Colors.defaultIconColorBlack}/>);
    }

    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldStyle}>
            <TextField
              title={this.props.title}
              label={label}
              value={this.props.value}
              editable={editable}
              selectTextOnFocus
              onFocus= {this.onFocus}
              onBlur={this.onBlur}
              selectTextOnFocus={false}
              onSelectionChange={this.onSelectionChange}
              ref={(ref) => {
                refTextInput = ref;
                manualFocus && ref !== null
                  ? ref.focus()
                  : true;
                manualBlur && ref !== null
                  ? ref.blur()
                  : true;
              }}
              onSubmitEditing={this.onSubmitEditing}
              secureTextEntry={secureTextEntry}
              onChangeText={this.inputChanged}
              keyboardType={keyboardType}
              maxLength={maxLength}/>
          </View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={keyboardAwareOnPress}
            disabled={!iconTap}>
            {rightIcon}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log('****** REDUX STATE :: MaterialInput => MOBILE ', state.mobile);
  let value = '';
  if (ownProps.index === 'NIC' || ownProps.index === 'Passport') {
    value = state.mobile.id_number;
  } else if (ownProps.index === 'SIM No') {
    value = state.mobile.sim_number;
  } else if (ownProps.index === 'Mobile No') {
    value = state.mobile.mobile_number;
  } else if (ownProps.index === 'First Reload Offer') {
    value = state.mobile.first_reload;
  } else if (ownProps.index === 'Alternative Contact No (Optional)') {
    value = state.mobile.alternate_contact_number;
  } else if (ownProps.index === 'Email (Optional)') {
    value = state.mobile.email_address;
  } else if (ownProps.index === 'eZ cash PIN') {
    value = state.mobile.ezCashPIN;
  } else if (ownProps.index === 'lcd') {
    value = (state.mobile.packageDeposits !==null) ? 
      state.mobile.packageDeposits.depositAmount : '';
  }
  //for reset ezCash PIN
  if (state.mobile.ezCashPIN == '' && ownProps.index === 'eZ cash PIN') {
    value = '';
  }

  if (state.mobile.ezCashPIN == '' && ownProps.index === 'eZ cash PIN') {
    value = '';
  }

  return { value };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 2,
    marginTop: -15,
    marginLeft: Styles.matirialInput.marginLeft,
    marginRight: Styles.matirialInput.marginRight
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  textFieldStyle: {
    flex: 7,
    justifyContent: 'flex-start'
  },
  imageStyle: Styles.matirialInput.imageStyle
});

export default connect(mapStateToProps, actions)(MaterialInput);