import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../../config/colors';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.navigator = this.props.navigator;
  }

  render() {

    return (
      <View style={styles.viewStyle}>
        <View style={styles.backButtonView}>
          <TouchableOpacity 
            underlayColor={Colors.transparent} 
            style={styles.backButton} 
            accessibilityLabel={`button_back`} 
            testID={`button_back`}
            onPress={this.props.backButtonPressed.bind(this)}>
            <MaterialIcons
              name={"arrow-back"}
              size={styles.inputFieldIconSize}
              color={Colors.white}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.headerTitleView}>
          <Text style={styles.textStyle}>{this.props.headerText}</Text>
        </View>
        <View style={styles.leftButtonsView}>
        </View>
      </View>
    );
  }
}

const styles = {
  inputFieldIconSize: 30,
  viewStyle: {
    // flex:1,
    flexDirection: 'row',
    backgroundColor: Colors.navBarBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 55,
    // paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 2,
    position: 'relative'
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  textStyle: {
    fontSize: 18,
    color: '#FFF',
    fontWeight: 'bold',
  },
  headerTitleView:{
    flex:4,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  backButtonView:{
    flex:1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  leftButtonsView:{
    flex:1
  }
};

export { Header };
