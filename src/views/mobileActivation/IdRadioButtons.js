import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import strings from '../../Language/MobileActivaton';
import Colors from '../../config/colors';

class IdRadioButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      value: 0,
      idradioTxt: {
        nic: strings.idLabelInptNicEng,
        passsport: strings.capturedPpLabelTxt,
        driving_license: strings.capturedDlLabelTxt
      }
    };
  }

  handleOnPress(value) {
    this
      .props
      .getResetImagesMobileAct();
    if (value === 0) {
      this
        .props
        .getInformationIdTypeMobileAct('NIC');
    } else if (value === 1) {
      this
        .props
        .getInformationIdTypeMobileAct('PP');
    } else if (value === 2) {
      this
        .props
        .getInformationIdTypeMobileAct('DR');
    }

    this.setState({ value });
  }

  componentDidMount(){
    const information_id_type = this.props.information_id_type;
    console.log("information_id_type: ", this.props.information_id_type);
    if (information_id_type === 'NIC') {
      this.setState({ value: 0 });
    } else if (information_id_type === 'PP') {
      this.setState({ value: 1 });
    } else if (information_id_type === 'DR') {
      this.setState({ value: 2 });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRadioWrapper}>
          {this.props.local_foreign === 'local'
            ? <View style={styles.containerRadioBtnNic}>
              <RadioButton
                currentValue={this.state.value}
                value={0}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.state.value == 0 ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={this
                  .handleOnPress
                  .bind(this)}>
                <Text style={styles.radioBtnTxt}>{this.state.idradioTxt.nic}</Text>
              </RadioButton>
            </View>
            : <View/>}

          <View style={styles.containerRadioBtnPp}>
            <RadioButton
              currentValue={this.state.value}
              value={1}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={this.state.value == 1 ? 
                Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
              }
              onPress={this
                .handleOnPress
                .bind(this)}>
              <Text style={styles.radioBtnTxt}>{this.state.idradioTxt.passsport}</Text>
            </RadioButton>
          </View>
          <View style={styles.containerRadioBtnDl}>
            <RadioButton
              currentValue={this.state.value}
              value={2}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={this.state.value == 2 ? 
                Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
              }
              onPress={this
                .handleOnPress
                .bind(this)}>
              <Text style={styles.radioBtnTxt}>{this.state.idradioTxt.driving_license}</Text>
            </RadioButton>
          </View>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => { 
  return { 
    local_foreign: state.mobile.local_foreign,
    information_id_type: state.mobile.information_id_type 
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5
  },
  containerRadioWrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 2
  },
  containerRadioBtnNic: {
    flex: 0.7,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.appBackgroundColor,
    margin: 2
  },
  containerRadioBtnPp: {
    flex: 1.2,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.appBackgroundColor,
    margin: 2
  },
  containerRadioBtnDl: {
    flex: 1.7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appBackgroundColor,
    margin: 2
  },
  radioBtnTxt: {
    paddingLeft: 5
  }
});

export default connect(mapStateToProps, actions)(IdRadioButtons);
