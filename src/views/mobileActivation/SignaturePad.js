import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Linking,
  BackHandler,
  Alert
} from 'react-native';
import SignatureCapture from 'react-native-signature-capture';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import strings from '../../Language/MobileActivaton';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import { FuncUtils, Screen } from '../../utills';


const Utill = new Utills();

let navigatorOb;
let isUserStartSigined;
let ImagePath;

class SignaturePad extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      isUserStartSigin: false,
      placeYourSignature: strings.placeYourSignature,
      bySigining: strings.bySigining,
      tns: strings.tns,
      enterCustomerSignature: strings.enterCustomerSignature,
      sigReset: strings.sigReset,
      sigOk: strings.sigOk,
      SignaturePadTitle: strings.customerSignatureUC,
      backMessage: strings.backMessage,
      didUserSign: false,
      tnsUc: strings.tnsUc
    };
    navigatorOb = props.navigator;
    isUserStartSigined = false;
    this.imageId = `SIG_${this.props.unique_tx_id}`;
    // ImagePathOld = '/storage/emulated/0/DCIM'; ImagePath =
    // `${Environment.getExternalStorageDirectory().toString()}/DCIM`;
    ImagePath = 'temp';
  }

  componentWillMount(){
    setTimeout(()=>{
      Orientation.lockToLandscape();
    },10);
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  onSaveEvent(result) {
    const signatureId = result.imageId;
    const signatureUri = result.pathName;
    const signatureRand = Math.floor(Math.random() * 1000);
    const signatureBase = result.encoded;
    console.log(result);
    const signatureOb = {
      signatureRand,
      signatureId,
      signatureUri,
      signatureBase
    };
    if (isUserStartSigined) {
      this
        .props
        .getSignatureMobileAct(signatureOb);
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    } else {
      Utill.showAlertMsg(this.state.enterCustomerSignature);
    }
  }
  onDragEvent() {
    // This callback will be called when the user enters signature
    console.log('onDragEvent');
    isUserStartSigined = true;
    this.setState({ didUserSign: true });
  }

  handleTndCPress  = () => {
    const { tncUrl } = this.props;
    let extensionType = FuncUtils.getFileExtensionType(tncUrl);
    if (extensionType == 'pdf') {
      let screen = {
        title: this.state.tnsUc,
        id: 'DialogRetailerApp.modals.PdfReaderModal',
      };
      let passProps = {
        tncUrl: this.props.tncUrl,
        title: this.state.tnsUc
      };
      Screen.showModalView(screen, passProps, false);
    } else {
      Linking.openURL(tncUrl);
    }  
  }
  
  handleBackButton = ()=> {
    console.log('xxxx SignatureCapture :: handleBackButton');
    console.log('xxxx onHandleBackButton');
    Alert.alert('', this.state.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.goBack()
      }
    ], { cancelable: true });
    return true;
  }

  goBack = () => {
    console.log('xxx goBack');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

    saveSign = () => {
      this
        .refs
        .sign
        .saveImage();
    }

    resetSign = () => {
      isUserStartSigined = false;
      this.setState({ didUserSign: false });
      this
        .refs
        .sign
        .resetImage();
    }

    handleClick() {
      console.log("===> Button Tapped \n ") ;
      console.log(JSON.stringify(this.props));
      console.log(JSON.stringify(this.state));
      this.handleBackButton();
    }

    render() {
      return (
        <View style={styles.container}>
          <Header backButtonPressed={() => this.handleClick()} headerText={this.state.SignaturePadTitle}/>
          <View style={styles.signatureContainer}>
            <View style={styles.topContainer}>
              <View stye={{ flex:1 }}>
                { this.state.didUserSign == true ?
                  <TouchableHighlight
                    style={styles.buttonStyle}
                    underlayColor={Colors.transparent}
                    onPress={() => {
                      this.resetSign();
                    }}
                  >
                    <Ionicons
                      name='md-close'
                      size={30}
                      color={Colors.colorBlack}/>
                  </TouchableHighlight>
                  :
                  true
                }
              </View>
              <View style={{ flex:8, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.desTxt}>{this.state.placeYourSignature}</Text>
              </View>
              <View stye={{ flex:1 }}>
                { this.state.didUserSign == true?  
                  <TouchableHighlight
                    style={styles.buttonStyle}
                    underlayColor={Colors.transparent}
                    onPress={() => {
                      this.saveSign();
                    }}
                  >
                    <Ionicons
                      name='md-checkmark'
                      size={35}
                      color={Colors.colorBlack}/>
                  </TouchableHighlight>
                  :
                  true
                }
              </View>
            </View>
            <SignatureCapture
              style={styles.signature}
              ref="sign"
              onSaveEvent={this
                .onSaveEvent
                .bind(this)}
              onDragEvent={this
                .onDragEvent
                .bind(this)}
              saveImageFileInExtStorage
              showNativeButtons={false}
              showTitleLabel={false}
              viewMode={'landscape'}
              savePath={ImagePath}
              imageId={this.imageId}/>
          </View>
          <View style={styles.buttonContainer}>
            <Text style={styles.desTncTxt}>{this.state.bySigining}
            </Text>
            <Text 
              style={styles.desTncTxtLink} 
              onPress={() => this.handleTndCPress()}>
              {this.state.tns}
            </Text>
          </View>
        </View>
      );
    }
}

const mapStateToProps = (state) => {
  const unique_tx_id = state.configuration.activity_start_time;
  const Language = state.lang.current_lang;

  return { unique_tx_id, Language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    backgroundColor: Colors.white
  },

  topContainer: {
    flex: 0.8,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0
  },
  signature: {
    flex: 7,
  },
  buttonContainer: {
    flex: 0.8,
    margin: 5,
    marginTop:0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width:40,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.transparent,
  },
  desTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
    fontWeight: 'bold'
  },

  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold'
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: 15,
    marginLeft: 5,
    textDecorationLine: 'underline',
    fontWeight: 'bold'
  },
  signatureContainer:{ 
    flex:7, 
    flexDirection: 'column', 
    margin: 10, 
    elevation: 3, 
    borderColor: Colors.signatureBorderColor
  }
});

export default connect(mapStateToProps, actions)(SignaturePad);