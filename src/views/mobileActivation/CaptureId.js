import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Ionicons from 'react-native-vector-icons/Ionicons';

class CaptureId extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sectionClick: false
    };
  }

  render() {
    const { label, onPress } = this.props;

    return (
      <View style={styles.container}>
        <View style={[styles.innerTextContainer]}>
          <Text style={styles.innerText}>{label}
          </Text>
        </View>
        <TouchableOpacity style={[styles.innerImageContainer]} onPress={onPress}>
          <Ionicons
            name='ios-camera'
            size={Styles.captureIconFontSize}
            color={Colors.colorBlack}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 2,
    marginTop: 0,
    paddingTop: 0,
    marginLeft: 35,
    marginRight: 15,
    backgroundColor: Colors.transparent
  },
  innerImageContainer: {
    flex: 2,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  innerTextContainer: {
    flex: 4,
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 8,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  innerText: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.thumbnailTxtFontSize
  }

});

export default CaptureId;
