/*
 * File: EzCashPayment.js
 * Project: Dialog Sales App
 * File Created: Thursday, 16th August 2018 5:40:57 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: Friday, 17th August 2018 8:34:43 pm
 * Modified By: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */



import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';

const Utill = new Utills();
class EzCashPayment extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>   
        <View style={styles.containerRadioWrapper}>
          <View style={styles.rowmain}>
            <View style={styles.elementLeftView}>
              <Text style={styles.totalPayment}>{this.props.title}</Text>
            </View>
            <View style={styles.elementRightView1}>
              <View style={styles.elementRightInnerView3}></View>
              <View style={styles.elementRightInnerView1}>
                <Text style={styles.totalPaymentValue}>{this.props.rs_label} </Text>
              </View>
              <View style={styles.elementRightInnerView2}>
                <Text style={styles.totalPaymentValue}>{Utill.numberWithCommas(Utill.getCorrectedNumber(this.props.ezCashAccountBalance))}</Text>
              </View>
            </View>
          </View>         
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
  },
  containerRadioWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    borderColor: Colors.borderLineColor
  },
  rowmain: {
    flexDirection: 'row',
    flex: 1
  },
  totalPayment: {
    fontWeight: '400',
    color: Colors.colorGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPaymentValue: {
    textAlign: 'right',
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  elementLeftView: {
    flex: 0.6
  },
  elementRightView1: {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  }, elementRightInnerView1: {
    flex: 0.2,
    alignItems: 'flex-start',
  },
  elementRightInnerView2: {
    flex: 0.7,
    alignItems: 'flex-end',
  },
  elementRightInnerView3: {
    flex: 0.1,
    alignItems: 'flex-start',
  }
});

export default EzCashPayment;