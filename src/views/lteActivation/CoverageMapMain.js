import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Header } from '../common/components/Header';
import ActivityIndicator from './components/ActivityIndicator';
import Color from '../../config/colors';
import CustomWebView from '../../components/CustomWebView';
import strings from '../../Language/LteActivation';
import _ from 'lodash';
import Images from '@Config/images';
import Utills from '../../utills/Utills';
import { Screen } from '@Utils';

const Utill = new Utills();

class CoverageMapMain extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      mapUrl:'',
      refId:'',
      locals: {
        title: strings.title,
        loading: strings.loading,
        backMessage: strings.backMessage,
        ok: strings.btnOk,
        cancel: strings.btnCancel,
        buttonContinue: strings.btnContinue
      }
    };
    this.deBouncedCheckStatus = _.debounce(()=>this._checkStatus(),500,{ 
      'leading': true,
      'trailing': false 
    });
    this.checkRapidEzcashModal = _.debounce(()=>this._checkRapidEzcashModalWithDebounce(),500,{ 
      'leading': true,
      'trailing': false 
    });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    navigator.geolocation.getCurrentPosition(position => {
      console.log('position: ',position);
      const { latitude, longitude } = position.coords;
      let initialPosition = { latitude,longitude };
      this.props.requestLocationPermission((info)=>this.requestLocationResponse(info),initialPosition);
    }, err => {
      console.log(err);
      console.log('Fetching the position failed');
      let coords = { latitude: '',longitude: '' };
      this.props.requestLocationPermission((info)=>this.requestLocationResponse(info),coords);
    });
  }

  requestLocationResponse = (info) =>{
    if (info !== undefined){
      const { refId, url } = info;
      this.setState({ mapUrl: url, refId: refId });
      console.log("requestLocationResponse Info: ", JSON.stringify(info));
    } else {
      console.log("requestLocationResponse Info object undefined");
    }
  };

  componentWillMount(){
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    // Alert.alert('', this.state.locals.backMessage, [
    //   {
    //     text: this.state.locals.cancel,
    //     onPress: () => console.log('Cancel Pressed'),
    //     style: 'cancel'
    //   }, {
    //     text: this.state.locals.ok,
    //     onPress: () => this.okHandler()
    //   }
    // ], { cancelable: true });
    this.okHandler(); // Removed confirmation alert due to bug RETMAPP-5549
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }

  _checkRapidEzcashModalWithDebounce = () => {

    if (!this.props.getConfiguration.rapid_ez_account.rapid_ez_availability && this.props.getConfiguration.config.enable_reload ){
      this.topViewGeneralModal();
    } else {
      this.deBouncedCheckStatus();
    }
  }

  _navEventHandler(event) {
    console.log(event);
    if (event.url.match('/mobile/close')){
      this.checkRapidEzcashModal();
    }
  }

  /**
   * @description Top View General modal
   * @param {Object} passProps
   * @memberof BottomItemsPre
   */
  topViewGeneralModal = () => {
  
    let passProps = {
      primaryText: this.state.locals.buttonContinue,
      primaryPress: () => this.onPressContinue(),
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.AttentionAlert,
      description: Utill.getStringifyText(this.props.rapid_ez_error),
      primaryButtonStyle: { color: Color.colorYellow },
    };

    Screen.showGeneralModal(passProps);
  };

  onPressContinue = () => {
    console.log('xxx modalDismiss');
    const { navigator } = this.props;
    navigator.dismissModal({ animationType: 'slide-down' });
    this.deBouncedCheckStatus();
  };

  async _checkStatus(){
    const { navigator } = this.props;
    
    const data = {
      ref_id: this.state.refId
    };

    this.props.coverageMapStatusCheck((successRes)=>{
      console.log('Check Status success: ', JSON.stringify(successRes));
      navigator.push({
        title: this.state.hbb,
        screen: 'DialogRetailerApp.views.LteActScreen',
        passProps: {
          ref_id: this.state.refId
        }
      });
    },data);
    console.log("Done Tapped");
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title}
          testID={`lte_coverage_map`} />
        {/*loading indicator*/}
        <ActivityIndicator animating={this.props.apiLoading} text={this.state.locals.loading}/>
        <View 
          style={styles.webViewStyle}
          accessibilityLabel={`lte_coverage_map`} 
          testID={`lte_coverage_map`} >
          <CustomWebView
            automaticallyAdjustContentInsets={false}
            source={{ uri: this.state.mapUrl }}
            onNavigationStateChange={(navEvent)=> this._navEventHandler(navEvent)}
            onMessage={(event)=> console.log(event.nativeEvent.data)}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            decelerationRate="normal"
            startInLoadingState={true}
            scalesPageToFit={true}
            accessibilityLabel={`lte_coverage_map`} 
            testID={`lte_coverage_map`} 
            //initialScale= "100%"
          />
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("#####====== LTE ACTIVATION INDEX =====######\n", state.lteActivation);
  const Language = state.lang.current_lang;

  const { apiLoading } = state.lteActivation;
  const getConfiguration = state.auth.getConfiguration;
  const { rapid_ez_error = '' } = getConfiguration.rapid_ez_account;
  
  return { apiLoading, Language, rapid_ez_error, getConfiguration };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.appBackgroundColor
  },
  webViewStyle:{
    flex:1
  }
});

export default connect(mapStateToProps, actions)(CoverageMapMain);
