import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Analytics } from '../../utills';
import { Constants, Colors } from '../../config';
import { Header } from '../common/components/Header';
import ActivityIndicator from '../common/components/ActivityIndicator';
import strings from '../../Language/LteActivation';
import TopItems from './TopItems';
import LteMainViewContainer from './LteMainViewContainer';

class lteActivationMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userLogged: true,
      locals: {
        title: strings.title,
        loading: strings.loading,
        validating: strings.validating,
        backMessage: strings.backMessage,
        ok: strings.btnOk,
        cancel: strings.btnCancel,
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.LteGetConfiguration();
    Analytics.logFirebaseEvent('action_start', {
      action_name: 'lte_tile_click',
      action_type: 'button_click',
      additional_details: 'LTE Tile click',
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }

  /**
   * @description Cancel voucher code reservation 
   * @memberof LteMainViewContainer
   */
  cancelVoucherCodeReservation = (previousVoucherCode = '') => {
    console.log('xxx cancelVoucherCodeReservationLTE');
    let me = this;
    let endpoint = {
      action: 'cancelVoucherCode',
      controller: 'lte',
      module: 'ccapp'
    };
    let releaseOnly = false;
    let voucherCode = previousVoucherCode == '' ? me.props.enteredVoucherCode : previousVoucherCode;

    if (previousVoucherCode !== ''){
      releaseOnly = true;
    }


    if (this.props.is_voucher_cancelled == false && this.props.validatedVoucherCode !== '') {
      console.log('xxx cancelVoucherCodeReservationLTE', me.props.voucherCodeAvailable, me.props.isValidVoucherCode, me.props.validatedVoucherCode !== '');
      if (me.props.voucherCodeAvailable && me.props.isValidVoucherCode && me.props.validatedVoucherCode !== '') {
        console.log('xxx cancelVoucherCodeReservation :: CANCEL_VOUCHER_CODE');
        let requestParams = {
          "conn_type": me.props.selected_connection_type,
          "lob": Constants.LOB_LTE,
          "cx_identity_no": me.props.idNumber_input_value,
          "om_campaign_record_no": me.props.om_campaign_record_no,
          "reservation_no": me.props.reservation_no,
          "reference_no": me.props.reference_no,
          "voucher_code": voucherCode,
        };
        Analytics.logEvent('lte_cancel_voucher_code_reservation');
        me.props.voucherCodeCancellationApi(requestParams, me, endpoint, releaseOnly );

        console.log("xxx cancelVoucherCodeReservationLTE previousVoucherCode", previousVoucherCode);
        if (previousVoucherCode == '') {
          me.props.resetValidateVoucherCodeStatus(Constants.LOB_LTE);
        }
       
      } else {
        console.log('xxx cancelVoucherCodeReservation :: DO NOTHING');
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title}
          testID={`lte_main`} />
        {/*loading indicator*/}
        <ActivityIndicator animating={this.props.apiLoading} text={this.props.api_call_indicator_msg}/>
        <View style={styles.container}>
          <KeyboardAwareScrollView
            style={[styles.keyboardAwareScrollViewContainer, styles.additionalStyle]}
            keyboardShouldPersistTaps="always">
            <TouchableWithoutFeedback
              onPress={Keyboard.dismiss}
              accessible={false}
              style={styles.container}>
              <View>
                <View style={styles.containerTop}>
                  <TopItems cancelVoucherCodeReservation={this.cancelVoucherCodeReservation}/>
                </View>
                <View style={styles.bottomItemsContainer}>
                  {/* LTE activation view */}
                  <LteMainViewContainer 
                    cancelVoucherCodeReservation={this.cancelVoucherCodeReservation} 
                    {...this.props}/>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log("#####====== LTE ACTIVATION INDEX =====######\n", JSON.stringify(state.lteActivation));
  console.log("#####====== LTE ACTIVATION INDEX =====######\n", state.lteActivation);
  const apiLoading = state.lteActivation.apiLoading;
  const api_call_indicator_msg = state.lteActivation.api_call_indicator_msg;
  const is_voucher_cancelled = state.lteActivation.is_voucher_cancelled;
  const validatedVoucherCode = state.lteActivation.validatedVoucherCode;
  const voucherCodeAvailable = state.lteActivation.voucherCodeAvailable;
  const isValidVoucherCode = state.lteActivation.isValidVoucherCode;
  const selected_connection_type = state.lteActivation.selected_connection_type;
  const idNumber_input_value = state.lteActivation.idNumber_input_value;
  const om_campaign_record_no = state.lteActivation.om_campaign_record_no;
  const reservation_no = state.lteActivation.reservation_no;
  const reference_no = state.lteActivation.reference_no;
  const enteredVoucherCode = state.lteActivation.enteredVoucherCode;

  return { 
    apiLoading, 
    api_call_indicator_msg, 
    is_voucher_cancelled,
    validatedVoucherCode,
    voucherCodeAvailable,
    isValidVoucherCode,
    selected_connection_type,
    idNumber_input_value,
    om_campaign_record_no,
    reservation_no,
    reference_no,
    enteredVoucherCode };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  bottomItemsContainer: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7
  }
});

export default connect(mapStateToProps, actions)(lteActivationMain);
