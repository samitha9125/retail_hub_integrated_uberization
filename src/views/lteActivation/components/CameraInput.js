import React from 'react';
import { StyleSheet, View, TouchableOpacity,Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';

class CameraInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  render() {
    const {
      customStyle,
      icon,
      titleLabel,
      frontAndBack,
      captureFront,
      captureBack
    } = this.props;

    let rightIcon;
 
    if (this.props.icon.iconType === 'MaterialCommunityIcons') {
      rightIcon = (<MaterialCommunityIcons
        name={icon.id}
        size={26}
        color={Colors.defaultIconColorBlack}/>);
    } else if (icon.iconType === 'Ionicons') {
      rightIcon = (<Ionicons name={icon.id} size={35} color={Colors.defaultIconColorBlack}/>);
    } else if (icon.iconType === 'FontAwesome') {
      rightIcon = (<FontAwesome name={icon.id} size={26} color={Colors.defaultIconColorBlack}/>);
    } else if (icon.iconType === 'Entypo') {
      rightIcon = (<Entypo name={icon.id} size={24} color={Colors.defaultIconColorBlack}/>);
    } else {
      rightIcon = (<MaterialIcons name={icon.id} size={26} color={Colors.defaultIconColorBlack}/>);
    }

    const getAdditionalCaptureView = () => {

      if (frontAndBack) {
        return (<TouchableOpacity
          style={styles.imageStyle}
          onPress={captureBack}>
          {rightIcon}
        </TouchableOpacity>);
      } else {
        return true;
      }
    };

    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldContainerStyle}>
            <Text style={styles.textFieldStyle}>{titleLabel}</Text>
          </View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={captureFront}>
            {rightIcon}
          </TouchableOpacity>
          {getAdditionalCaptureView()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 2,
    //backgroundColor: 'green'
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems:'center'
    //backgroundColor: 'yellow'
  },
  textFieldStyle: {
    fontSize: 15,
    color: Colors.black,
  },
  textFieldContainerStyle: {
    flex: 7,
    justifyContent: 'flex-start'
  },
  imageStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: 8,
  }
});

export default connect(null, actions)(CameraInput);