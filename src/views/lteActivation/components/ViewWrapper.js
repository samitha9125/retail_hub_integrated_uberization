import React from 'react';
import { PropTypes } from 'prop-types';

class ViewWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opacity:this.props.visible ? 1 : 0
    };
  }

  shouldComponentUpdate(nextProps) {
    return this.props.visible !== nextProps.visible || this.props.formData !== nextProps.formData;
  }

  render() {
    if (this.props.removeWhenHidden && !this.props.visible) {
      return null;
    }

    return (
      <View style={[this.props.style, { opacity: this.state.opacity }]}>
        {this.props.children}
      </View>
    );
  }
}

ViewWrapper.propTypes = {
  visible: PropTypes.bool.isRequired,
  removeWhenHidden: PropTypes.bool,

};

export default ViewWrapper;