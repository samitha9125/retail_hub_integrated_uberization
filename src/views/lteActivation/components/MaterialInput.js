import React from 'react';
import { StyleSheet, View, TouchableOpacity, Keyboard } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';

class MaterialInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('xxx MaterialInput :: componentWillReceiveProps', nextProps);
  }

  render() {
    const {
      customStyle,
      ref,
      label,
      value,
      error ='',
      icon,
      iconSize,
      onIconPress,
      onChangeText,
      editable,
      disableIconTap,
      keyboardType,
      maxLength,
      secureTextEntry,
      isFocused,
      hideRightIcon,
      tapToFocus = true
    } = this.props;

    let refTextInput = ref;

    const keyboardAwareOnPress = function () {
      console.log('refTextInput', refTextInput);
      if (tapToFocus == false) {
        console.log('TAP_TO_DISMISS');
        Keyboard.dismiss();
      } else {  
        console.log('TAP_TO_FOCUS');
        refTextInput.focus();
      }
      onIconPress.apply(this, arguments);
    };

    let rightIcon;
 
    if (!hideRightIcon) {
      if (icon.iconType === 'MaterialCommunityIcons') {
        rightIcon = (<MaterialCommunityIcons
          name={icon.id}
          size={24}
          color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'Ionicons') {
        rightIcon = (<Ionicons name={icon.id} size={22} color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'FontAwesome') {
        rightIcon = (<FontAwesome name={icon.id} size={iconSize ? iconSize : 26} color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'Entypo') {
        rightIcon = (<Entypo name={icon.id} size={22} color={Colors.defaultIconColorBlack}/>);
      } else {
        rightIcon = (<MaterialIcons name={icon.id} size={18} color={Colors.defaultIconColorBlack}/>);
      }
    } else {
      rightIcon = (true);
    }

    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldStyle}>
            <TextField
              style={styles.TextFieldInputStyle}
              title={this.props.title}
              label={label}
              error={error}
              editable={editable}
              value={value}
              ref={(refVal) => {
                refTextInput = refVal;
                isFocused && refVal !== null
                  ? refVal.focus()
                  : true;
              }}  
              secureTextEntry={secureTextEntry}
              onChangeText={onChangeText}
              keyboardType={keyboardType}
              maxLength={maxLength}/>
          </View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={keyboardAwareOnPress}
            disabled={disableIconTap}>
            {rightIcon}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 2,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    //backgroundColor: 'green'
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
    //backgroundColor: 'yellow'
  },
  textFieldStyle: {
    flex: 9,
    justifyContent: 'flex-start'
  },
  TextFieldInputStyle: { 
    paddingRight: 30
  },
  imageStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 30,
    // marginBottom:5,
    alignItems: 'flex-end',
    left: -30,
    backgroundColor: Colors.appBackgroundColor,
    height:26

  }
});

export default connect(null, actions)(MaterialInput);