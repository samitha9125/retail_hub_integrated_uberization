import React from 'react';
import { ActivityIndicator, View, StyleSheet, Text } from 'react-native';
import Orientation from 'react-native-orientation';
import Colors from '../../../config/colors';
import strings from '../../../Language/LteActivation';

class ActIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: true,
      locals: {
        validatingText: strings.validatingText

      }
    };
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
  }

  render() {
    const animating = this.props.animating;
    let validatingText = this.props.text ? this.props.text : this.state.locals.validatingText;
    const setLoadingIndicator = () => {
      if (animating){
        return (
          <View style={styles.container}>
            <View style={styles.indicatorStyle}>
              <Text style={styles.textStyle}>{validatingText}</Text>
              <ActivityIndicator
                animating={animating}
                color={Colors.activityIndicatorColor}
                size="large"
                style={styles.activityIndicator}/>
            </View>
          </View>
        );
      } else {
        return (<View/>);
      }

    };
    return (
      setLoadingIndicator()
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    paddingVertical: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1000,
    opacity: 0.5
  },
  textStyle: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: "500",
    color: Colors.colorBlack,
    paddingVertical: 100,
    zIndex: 1000,
    paddingTop: 100,
    marginTop: 100
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    marginBottom: 45

  },
  indicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});

export default ActIndicator;
