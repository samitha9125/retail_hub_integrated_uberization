import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';

class InputSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sectionClick: false
    };
  }

  render() {
    const {
      customStyle,
      label,
      sectionId,
      validationStatus,
      isFocused = false,
      onPress = Keyboard.dismiss
    } = this.props;
    let labelIcon;
    if (validationStatus) {
      labelIcon = <View
        style={[
          styles.circleIcon, { backgroundColor: Colors.statusIconColor.success }]}>
        <Text style={styles.sectionCircleText}>
          {'✓'}
        </Text>
      </View>;

    } else {
      labelIcon = <View
        style={[
          styles.circleIcon, isFocused
            ? { backgroundColor: Colors.statusIconColor.success }
            : {} ]}>
        <Text style={styles.sectionIdText}>
          {sectionId}
        </Text>
      </View>;
    }

    return (
      <View>
        <TouchableOpacity style={[styles.container, customStyle]} onPress={onPress}>
          <View style={[styles.innerImageContainer]}>
            {labelIcon}
          </View>
          <View style={[styles.innerTextContainer]}>
            <Text style={styles.innerText}>{label}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[styles.innerComponentWrapper]}>
          {this.props.children}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginRight: 2
  },
  innerImageContainer: {
    width: 50,
    paddingBottom: 8,
    paddingTop: 8,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  innerTextContainer: {
    flex: 1,
    paddingBottom: 8,
    paddingTop: 8,
    paddingLeft: 1,
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  innerText: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.mobileAct.defaultBtnFontSize,
    fontWeight: '400'
  },

  ////////////
  circleIcon: {
    width: 23,
    height: 23,
    alignItems: 'center',
    backgroundColor: Colors.statusIconColor.pending,
    padding: 2,
    borderRadius: 48
  },

  sectionIdText: {
    color: Colors.colorWhite,
    fontWeight: '500'
  },
  sectionCircleText: {
    color: Colors.colorWhite
  },
  innerComponentWrapper: {
    borderLeftColor: Colors.statusIconColor.pending,
    borderLeftWidth: 1,
    marginLeft: 23
  }

});

export default InputSection;
