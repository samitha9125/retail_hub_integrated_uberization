/*
 * File: EzCashPayment.js
 * Project: Dialog Sales App
 * File Created: Thursday, 16th August 2018 5:40:57 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: Friday, 17th August 2018 8:34:43 pm
 * Modified By: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/LteActivation';
import Utills from '../../utills/Utills';

const Utill = new Utills();
class EzCashPayment extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      expanSwich: false,
      needToShowoutstanding: false,
      totalPayValue: '',
      outstandingFeeValue: '',
      depositChargeValue: '',
      arrowDown: 'md-arrow-dropdown',
      connection_fee: '',
      depositAmount: '',
      unitPriceValue: '',
      datalanSachetValue: '',
      locals: {
        eZCashtotalPay: strings.eZCashtotalPay,
        showData: strings.showData,
        depositCharge: strings.depositCharge,
        outstandingFee: strings.outstandingFee,
        unitPriceAdjustment: strings.unitPriceAdjustment,
        Rs : strings.rs_label,
        purchaseunit : strings.purchaseUnit,
        purchaseSim : strings.purchaseSim,
        sellUnit: strings.sellUnit,
        sellSim: strings.sellSim
      }
    };
  }

  componentDidMount() {
    console.log('xxx TotalPayments :: componentDidMount');
    console.log(JSON.stringify(this.props));
  }

  checkNumber = (number) => {
    console.log('xxx TotalPayments :: checkNumber');
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00" && value !== 0.00);
  }

  expanDetails() {
    if (this.state.expanSwich) {
      this.setState({ expanSwich: false, arrowDown: 'md-arrow-dropdown' });
    } else {
      this.setState({ expanSwich: true, arrowDown: 'md-arrow-dropup' });
    }
  }


  render() {
    console.log('====================================');
    console.log('expandContent :: state\n', JSON.stringify(this.state));
    console.log('expandContent :: props\n', JSON.stringify(this.props));
    console.log('====================================');

    return (

      <View style={styles.container}>
        {this.needToShowValue(this.props.eZCashTotalPaymentProps.eZCashtotalPay)
          ?
          <View style={styles.containerRadioWrapper}>
            <View style={styles.rowmain}>
              <View style={styles.elementLeftView}>
                <Text style={styles.totalPayment}>{this.state.locals.eZCashtotalPay}</Text>
              </View>
              <View style={styles.elementRightView1}>
                <View style={styles.elementRightInnerView3}></View>
                <View style={styles.elementRightInnerView1}>
                  <Text style={styles.totalPaymentValue}>{this.state.locals.Rs} </Text>
                </View>
                <View style={styles.elementRightInnerView2}>
                  <Text style={styles.totalPaymentValue}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.eZCashtotalPay))}</Text>
                </View>
              </View>
            </View>
            <View style={styles.rowmain}>
              <Text
                style={styles.showDetails}
                onPress={this
                  .expanDetails
                  .bind(this)}>{this.state.locals.showData}</Text>
              <Ionicons name={this.state.arrowDown} size={20} color={Colors.linkColor} />
            </View>
            {this.state.expanSwich
              ? <View>
                {this.props.selected_connection_type === Constants.PREPAID ?
                  <View>
                    <View style={styles.rowsub}>
                      <View style={styles.elementLeftView}>
                        <Text style={styles.cl2}>{this.state.locals.unitPriceAdjustment}</Text>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.unitPriceAdjustment))}</Text>
                        </View>

                      </View>
                    </View>
                    <View style={styles.rowsub}>
                      <View style={styles.elementLeftView}>
                        <View style={styles.elementLeftViewforAdjustment}>
                          <Text style={styles.adjustmentFontStyle}>{this.state.locals.purchaseunit}</Text>
                        </View>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.serialPackPrice))}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.rowsub}>
                      <View style={styles.elementLeftView}>
                        <View style={styles.elementLeftViewforAdjustment}>
                          <Text style={styles.adjustmentFontStyle}>{this.state.locals.purchaseSim}</Text>
                        </View>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.simSerialPrice))}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.rowsub}>
                      <View style={styles.elementLeftView}>
                        <View style={styles.elementLeftViewforAdjustment}>
                          <Text style={styles.adjustmentFontStyle}>{this.state.locals.sellUnit}</Text>
                        </View>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.unitPriceValue))}</Text>
                        </View>
                      </View>
                    </View>
                
                  </View>

                  :
                  <View>
                    {this.needToShowValue(this.props.eZCashTotalPaymentProps.outstandingFeeValue)
                      ?

                      <View style={styles.rowsub}>
                        <View style={styles.elementLeftView}>
                          <Text style={styles.cl2}>{this.state.locals.outstandingFee}</Text>
                        </View>
                        <View style={styles.elementRightView1}>
                          <View style={styles.elementRightInnerView3}></View>
                          <View style={styles.elementRightInnerView1}>
                            <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                          </View>
                          <View style={styles.elementRightInnerView2}>
                            <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.outstandingFeeValue))}</Text>
                          </View>
                        </View>
                      </View>
                      :
                      <View />
                    }

                    <View>
                      <View style={styles.rowsub}>
                        <View style={styles.elementLeftView}>
                          <Text style={styles.cl2}>{this.state.locals.unitPriceAdjustment}</Text>
                        </View>
                        <View style={styles.elementRightView1}>
                          <View style={styles.elementRightInnerView3}></View>
                          <View style={styles.elementRightInnerView1}>
                            <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                          </View>
                          <View style={styles.elementRightInnerView2}>
                            <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.unitPriceAdjustment))}</Text>
                          </View>
                        </View>
                      </View>
                      <View style={styles.rowsub}>
                        <View style={styles.elementLeftView}>
                          <View style={styles.elementLeftViewforAdjustment}>
                            <Text style={styles.adjustmentFontStyle}>{this.state.locals.purchaseunit}</Text>
                          </View>
                        </View>
                        <View style={styles.elementRightView1}>
                          <View style={styles.elementRightInnerView3}></View>
                          <View style={styles.elementRightInnerView1}>
                            <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                          </View>
                          <View style={styles.elementRightInnerView2}>
                            <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.serialPackPrice))}</Text>
                          </View>
                        </View>
                      </View>
                      <View style={styles.rowsub}>
                        <View style={styles.elementLeftView}>
                          <View style={styles.elementLeftViewforAdjustment}>
                            <Text style={styles.adjustmentFontStyle}>{this.state.locals.purchaseSim}</Text>
                          </View>
                        </View>
                        <View style={styles.elementRightView1}>
                          <View style={styles.elementRightInnerView3}></View>
                          <View style={styles.elementRightInnerView1}>
                            <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                          </View>
                          <View style={styles.elementRightInnerView2}>
                            <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.simSerialPrice))}</Text>
                          </View>
                        </View>
                      </View>
                      <View style={styles.rowsub}>
                        <View style={styles.elementLeftView}>
                          <View style={styles.elementLeftViewforAdjustment}>
                            <Text style={styles.adjustmentFontStyle}>{this.state.locals.sellUnit}</Text>
                          </View>
                        </View>
                        <View style={styles.elementRightView1}>
                          <View style={styles.elementRightInnerView3}></View>
                          <View style={styles.elementRightInnerView1}>
                            <Text style={styles.textStyleRightforAdjustment}>{this.state.locals.Rs} </Text>
                          </View>
                          <View style={styles.elementRightInnerView2}>
                            <Text style={styles.textStyleRightforAdjustment}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.unitPriceValue))}</Text>
                          </View>
                        </View>
                      </View>
                    </View>


                    <View style={styles.rowsub}>
                      <View style={styles.elementLeftView}>
                        <Text style={styles.cl2}>{this.state.locals.depositCharge}</Text>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.eZCashTotalPaymentProps.depositAmount))}</Text>
                        </View>
                      </View>
                    </View>

                  </View>
                }
              </View>
              : <View />
            }
          </View>
          :
          <View />
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: EZ CASH PAYMENT => LTE ACTIVATION', state.lteActivation);
  const language = state.lang.current_lang;
  const selected_connection_type = state.lteActivation.selected_connection_type;

  return {
    language,
    selected_connection_type
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
  },
  containerRadioWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    borderColor: Colors.borderLineColor
  },
  rowmain: {
    flexDirection: 'row',
    flex: 1
  },
  rowsub: {
    flexDirection: 'row',
    marginTop: 5,
    flex: 1
  },
  showDetails: {
    color: Colors.urlLinkColor,
    paddingRight: 9,
    fontSize: Styles.otpEnterModalFontSize,
    fontWeight: 'normal'
  },
  adjustmentFontStyle: {
    flexDirection: 'row',
    flex: 1.5,
    color: Colors.colorUnitAdjustment,
    fontSize: Styles.otpEnterModalFontSize
  },
  cl2: {
    flexDirection: 'row',
    flex: 1.5,
    color: Colors.colorGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPayment: {
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPaymentValue: {
    textAlign: 'right',
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  elementLeftView: {
    flex: 0.6
  },
  elementLeftViewforAdjustment: {
    marginLeft: 10,
    flex: 0.6
  },
  elementRightView1: {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  }, elementRightInnerView1: {
    flex: 0.2,
    alignItems: 'flex-start',
  },
  elementRightInnerView2: {
    flex: 0.7,
    alignItems: 'flex-end',
  },
  elementRightInnerView3: {
    flex: 0.1,
    alignItems: 'flex-start',
  },
  textStyleRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },
  textStyleRightforAdjustment: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorUnitAdjustment
  },

});

export default connect(mapStateToProps, actions)(EzCashPayment);