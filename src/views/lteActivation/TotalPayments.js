/*
 * File: TotalPayments.js
 * Project: Dialog Sales App
 * File Created: Thursday, 16th August 2018 5:40:57 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: Friday, 17th August 2018 8:34:43 pm
 * Modified By: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/LteActivation';
import Utills from '../../utills/Utills';

const Utill = new Utills();
class TotalPayments extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      expandSwitch: false,
      arrowDown: 'md-arrow-dropdown',
      locals: {
        totalPay: strings.totalPayment,
        showData: strings.showData,
        depositCharge: strings.depositAmount,
        outstandingFee: strings.outstandingFee,
        unitPrice: strings.unitPrice,
        dataPlanSachet: strings.dataSachetReload,
        Rs : strings.rs_label
      }
    };
  }

  componentDidMount() {
    console.log('xxx TotalPayments :: componentDidMount');
    console.log(JSON.stringify(this.props));
  }

  checkNumber = (number) => {
    console.log('xxx TotalPayments :: checkNumber');
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00" && value !== 0.00);
  }

  expandDetails = () => {
    console.log('xxx expandDetails');
    if (this.state.expandSwitch) {
      this.setState({ expandSwitch: false, arrowDown: 'md-arrow-dropdown' });
    } else {
      this.setState({ expandSwitch: true, arrowDown: 'md-arrow-dropup' });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRadioWrapper}>
          <View style={styles.mainRow}>
            <View style={styles.elementLeftView}>
              <Text style={styles.totalPayment}>{this.state.locals.totalPay}</Text>
            </View>
            <View style={styles.elementRightView1}>
              <View style={styles.elementRightInnerView3}></View>
              <View style={styles.elementRightInnerView1}>
                <Text style={styles.totalPaymentValue}>{this.state.locals.Rs} </Text>
              </View>
              <View style={styles.elementRightInnerView2}>
                <Text style={styles.totalPaymentValue}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.totalPayValue))}</Text>
              </View>
            </View>
          </View>
          <View style={styles.mainRow}>
            <Text
              style={styles.showDetails}
              onPress={() =>this.expandDetails()}>{this.state.locals.showData}</Text>
            <Ionicons name={this.state.arrowDown} size={20} color={Colors.linkColor} />
          </View>
          {this.state.expandSwitch
            ? <View>
              {this.props.selected_connection_type === Constants.PREPAID ?
                <View>
                  <View style={styles.subRow}>
                    <View style={styles.elementLeftView}>
                      <Text style={styles.cl2}>{this.state.locals.unitPrice}</Text>
                    </View>
                    <View style={styles.elementRightView1}>
                      <View style={styles.elementRightInnerView3}></View>
                      <View style={styles.elementRightInnerView1}>
                        <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                      </View>
                      <View style={styles.elementRightInnerView2}>
                        <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.unitPriceValue))}</Text>
                      </View>

                    </View>
                  </View>
                  {this.needToShowValue(this.props.totalPaymentProps.dataplanSachetValue) ?
                    <View style={styles.subRow}>
                      <View style={styles.elementLeftView}>
                        <Text style={styles.cl2}>{this.state.locals.dataPlanSachet}</Text>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.dataplanSachetValue))}</Text>
                        </View>
                      </View>
                    </View>
                    :
                    <View />
                  }
                </View>
                :
                <View>
                  {this.needToShowValue(this.props.totalPaymentProps.outstandingFeeValue)
                    ?
                    <View style={styles.subRow}>
                      <View style={styles.elementLeftView}>
                        <Text style={styles.cl2}>{this.state.locals.outstandingFee}</Text>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.outstandingFeeValue))}</Text>
                        </View>
                      </View>
                    </View>
                    :
                    <View />
                  }
                  <View style={styles.subRow}>
                    <View style={styles.elementLeftView}>
                      <Text style={styles.cl2}>{this.state.locals.unitPrice}</Text>
                    </View>
                    <View style={styles.elementRightView1}>
                      <View style={styles.elementRightInnerView3}></View>
                      <View style={styles.elementRightInnerView1}>
                        <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                      </View>
                      <View style={styles.elementRightInnerView2}>
                        <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.unitPriceValue))}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.subRow}>
                    <View style={styles.elementLeftView}>
                      <Text style={styles.cl2}>{this.state.locals.depositCharge}</Text>
                    </View>
                    <View style={styles.elementRightView1}>
                      <View style={styles.elementRightInnerView3}></View>
                      <View style={styles.elementRightInnerView1}>
                        <Text style={styles.textStyleRight}>{this.state.locals.Rs} </Text>
                      </View>
                      <View style={styles.elementRightInnerView2}>
                        <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.depositAmount))}</Text>
                      </View>
                    </View>
                  </View>

                </View>
              }
            </View>
            : <View />
          }
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TOTAL PAYMENT => LTE ACTIVATION', state.lteActivation);
  const language = state.lang.current_lang;
  const selected_connection_type = state.lteActivation.selected_connection_type;

  return {
    language,
    selected_connection_type
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    // marginLeft: 20,
    // marginRight: 7
  },
  containerRadioWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    // padding: 5,
    borderColor: Colors.borderLineColor
  },
  mainRow: {
    flexDirection: 'row',
    flex: 1
  },
  subRow: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,   
  },
  showDetails: {
    color: Colors.urlLinkColor,
    // textDecorationLine: 'underline',
    paddingRight: 9,
    // paddingLeft: 5,
    fontSize: Styles.otpEnterModalFontSize,
    fontWeight: 'normal'
  },
  cl2: {
    flexDirection: 'row',
    flex: 1.5,
    color: Colors.colorGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPayment : {
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPaymentValue : {
    textAlign: 'right',
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  elementLeftView : {

    flex: 0.6
  },
  elementRightView1 : {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  },
  elementRightInnerView1 : {
    flex: 0.2,
    alignItems: 'flex-start'
  },
  elementRightInnerView2 : {
    flex: 0.7,
    alignItems: 'flex-end'
  },
  elementRightInnerView3 : {
    flex: 0.1,
    alignItems: 'flex-start'
  },
  textStyleRight : {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },

});

export default connect(mapStateToProps, actions)(TotalPayments);