import React from 'react';
import { StyleSheet, View, Text, Picker } from 'react-native';
import RadioButton from 'radio-button-react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/LteActivation';
import Images from '@Config/images';
import Utills from '../../utills/Utills';
import { Navigation } from 'react-native-navigation';
import { Screen } from '@Utils';

const Utill = new Utills();
class TopItems extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      user: '',
      connType: Constants.PREPAID,
      value: 1,
      local: {
        prepaidRadioBtnTitle: strings.prepaidRadioBtnTitle,
        postpaidRadioBtnTitle:  strings.postpaidRadioBtnTitle,
        lblSriLankan: strings.lblSriLankan,
        lblForeigner: strings.lblForeigner,
        buttonContinue: strings.btnContinue
      }
    };
  }

  
  updateUser = (userType) => {
    console.log('xxx updateUser', userType);
    this.setState({ user: userType });
    this.fullResetSelectedConnectionNumber();
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.resetLTEData(true);
    this.props.setLTEConnectionType(this.state.connType);
    this.props.lteSetCustomerType(userType);
  }

  setConnectionType = (mode, value) => {
    console.log("xxx setConnectionType: ", mode);
    this.fullResetSelectedConnectionNumber();
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.resetLTEData(true);
    this.props.setLTEConnectionType(mode);
    this.props.lteSetCustomerType(Constants.CX_TYPE_LOCAL);
    this.setState({ value: value, connType: mode, user: Constants.CX_TYPE_LOCAL }); 
    this.checkRapidEzcashModal(mode);

  }

  fullResetSelectedConnectionNumber = () =>{
    let { selected_number = '' } = this.props.selected_connection_number;
    if (selected_number !== '') {
      this.props.resetSelectedConnectionNumber();
      this.props.numberPoolRelease({ type: Constants.RELEASE_TYPES.RELEASE_ALL }, null, this);
    }
  }

  checkRapidEzcashModal = (mode) => {
    console.log("xxx setConnectionType: ", mode);
    if ( mode == Constants.PREPAID && !this.props.getConfiguration.rapid_ez_account.rapid_ez_availability && this.props.getConfiguration.config.enable_reload ){
      this.topViewGeneralModal();
    }
    
  }

  
  /**
   * @description Top View General modal
   * @param {Object} passProps
   * @memberof BottomItemsPre
   */
  topViewGeneralModal = () => {
  
    let passProps = {
      primaryText: this.state.local.buttonContinue,
      primaryPress: () => this.onPressContinue(),
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.AttentionAlert,
      description: Utill.getStringifyText(this.props.rapid_ez_error),
      primaryButtonStyle: { color: Colors.colorYellow },
    };

    Screen.showGeneralModal(passProps);
  };

  /**
   * @description dismiss modal
   * @memberof BottomItemsPre
   */
  onPressContinue = () => {
    console.log('xxx modalDismiss');
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRadioWrapper}>
          <View style={styles.containerRadioBtn}>
            <RadioButton
              currentValue={this.state.value}
              value={1}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={this.state.value == 1 ? 
                Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
              }
              onPress={() => this.setConnectionType(Constants.PREPAID, 1)}>
              <Text style={styles.radioBtnTxt}>{this.state.local.prepaidRadioBtnTitle}</Text>
            </RadioButton>
          </View>
          <View style={styles.containerRadioBtn}>
            <RadioButton
              currentValue={this.state.value}
              value={2}
              outerCircleSize={20}
              innerCircleColor={Colors.radioBtn.innerCircleColor}
              outerCircleColor={this.state.value == 2 ? 
                Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
              }
              onPress={() => this.setConnectionType(Constants.POSTPAID, 2)}>
              <Text style={styles.radioBtnTxt}>{this.state.local.postpaidRadioBtnTitle}</Text>
            </RadioButton>
          </View>
        </View>
        <View style={styles.containerPicker}>
          <Picker
            style={styles.picker}
            itemStyle={styles.pickerItem}
            mode={'dropdown'}
            selectedValue={this.state.user}
            onValueChange={this.updateUser}>
            <Picker.Item label={this.state.local.lblSriLankan} value={Constants.CX_TYPE_LOCAL}/>
            <Picker.Item label={this.state.local.lblForeigner} value={Constants.CX_TYPE_FOREIGNER}/>
          </Picker>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: TopItems :: lteActivation\n', state.lteActivation);
  const Language = state.lang.current_lang;
  const selected_connection_type = state.lteActivation.selected_connection_type;  
  const lteSelectedArea = state.lteActivation.lteSelectedArea;
  const reserved_number_list = state.lteActivation.reserved_number_list;
  const selected_connection_number = state.lteActivation.selected_connection_number;
  const getConfiguration = state.auth.getConfiguration;
  const { rapid_ez_error = '' } = getConfiguration.rapid_ez_account;
  return { 
    Language, 
    selected_connection_type, 
    lteSelectedArea, 
    reserved_number_list, 
    selected_connection_number,
    getConfiguration,
    rapid_ez_error
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    marginLeft: 7,
    marginRight: 7
  },
  containerRadioWrapper: {
    flex: 2,
    marginRight: 2,
    flexDirection: 'row',
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 0,
    padding: 5,
    borderColor: Colors.topSelctionBorderColor,
    borderWidth: StyleSheet.hairlineWidth
  },
  containerRadioBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appBackgroundColor,
    margin: 0
  },
  containerPicker: {
    flex: 1.2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorTransparent,
    paddingLeft: 20,
    marginLeft: -2,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 1,
    borderColor: Colors.topSelctionBorderColor,
    borderWidth: StyleSheet.hairlineWidth
  },
  picker: {
    width: 140,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 5,
    marginLeft: 2,
    marginRight: 15,
    borderWidth: 1
  },
  radioBtnTxt: {
    paddingLeft: 8
  },
  pickerItem: {
    color: Colors.colorRed
  }
});

export default connect(mapStateToProps, actions)(TopItems);
