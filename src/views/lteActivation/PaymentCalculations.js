import Constants from '../../config/constants';

let totalPaymentValue;
let unitPriceValue;
let dataPlanSachetValue;
let outstandingFeeValue;
let depositAmount;
let eZCashtotalPay;
let unitPriceAdjustment;
let deviceSellingPrice;
let voucher_value;
let serialPackPrice;
let simSerialPrice;
let bundleSerialPrice;

const getPaymentDataFromRedux = (lteActState) => {
  console.log('### getPaymentDataFromRedux :: getPaymentData', lteActState);
  let outstandingFeeValueRedux = lteActState.customerOutstandingData !== null
        && lteActState.customerOutstandingData !== undefined ? 
    lteActState.customerOutstandingData.totalOsAmount : 0.00;

  unitPriceValue = checkNumber(lteActState.availableLTEOffersList.offerPrice);
  dataPlanSachetValue = checkNumber(lteActState.reloadAmount);
  outstandingFeeValue = checkNumber(outstandingFeeValueRedux);
  depositAmount = checkNumber(lteActState.lte_deposit_value.depositAmount);
  voucher_value = checkNumber(lteActState.voucherValue);

  console.log('### =============================================================== ##');
  console.log('### getPaymentDataFromRedux :: outstandingFeeValue : ', outstandingFeeValue);
  console.log('### getPaymentDataFromRedux :: unitPriceValue : ', unitPriceValue);
  console.log('### getPaymentDataFromRedux :: dataPlanSachetValue : ', dataPlanSachetValue);
  console.log('### getPaymentDataFromRedux :: depositAmount : ', depositAmount);
  console.log('### getPaymentDataFromRedux :: voucher_value : ', voucher_value);

  //Calculating Unit Price Value
  if (lteActState.selected_connection_type == Constants.PREPAID) {
    unitPriceValue = parseFloat(unitPriceValue) - parseFloat(voucher_value);  
  }
  // calculating Buying Price According To Serial Type
  if (lteActState.serial_scan_mode == Constants.BUNDLE_SERIAL) {
    if (lteActState.bundleSerialData !== undefined 
    && lteActState.bundleSerialData  !== null) {
      console.log('### getPaymentDataFromRedux :: bundleSerialPriceRedux : ', lteActState.bundleSerialData.sapMaterialItems.price);
      bundleSerialPrice =  checkNumber(lteActState.bundleSerialData.sapMaterialItems.price);  
    }
    deviceSellingPrice = bundleSerialPrice;

  } else {


    if (lteActState.serialPackData !== undefined 
      && lteActState.serialPackData !== null) {
      console.log('### getPaymentDataFromRedux :: serialPackPriceRedux : ', lteActState.serialPackData.sapMaterialItems.price);
      serialPackPrice =  checkNumber(lteActState.serialPackData.sapMaterialItems.price);
    } else {
      serialPackPrice =  0;
    }

    if (lteActState.simSerialData !== undefined 
      && lteActState.simSerialData !== null) {
      console.log('### getPaymentDataFromRedux :: simSerialPriceRedux : ', lteActState.simSerialData.sapMaterialItems.price);
      simSerialPrice =  checkNumber(lteActState.simSerialData.sapMaterialItems.price);
    } else {
      simSerialPrice =  0;
    }

    console.log('### getPaymentDataFromRedux :: serialPackPrice : ', serialPackPrice);
    console.log('### getPaymentDataFromRedux :: simSerialPrice : ', simSerialPrice);

    deviceSellingPrice = parseFloat(serialPackPrice) + parseFloat(simSerialPrice);  
  }

  //Calculating unitPriceAdjustment Field
  unitPriceAdjustment = (deviceSellingPrice - unitPriceValue) * (-1);

  
  // Calculating Total Amount & eZ cash Total Amount According To Serial Type
  if (lteActState.selected_connection_type == Constants.PREPAID) {
    totalPaymentValue = parseFloat(unitPriceValue) + parseFloat(dataPlanSachetValue);
    eZCashtotalPay = unitPriceAdjustment;
  } else {
    totalPaymentValue = parseFloat(unitPriceValue) + parseFloat(outstandingFeeValue) + parseFloat(depositAmount); 
    eZCashtotalPay = parseFloat(unitPriceAdjustment) + parseFloat(outstandingFeeValue) + parseFloat(depositAmount); 
  }

  unitPriceValue = parseFloat(unitPriceValue).toFixed(2);
  deviceSellingPrice = parseFloat(deviceSellingPrice).toFixed(2);
  unitPriceAdjustment = parseFloat(unitPriceAdjustment).toFixed(2);
  totalPaymentValue = parseFloat(totalPaymentValue).toFixed(2);
  eZCashtotalPay = parseFloat(eZCashtotalPay).toFixed(2);

  console.log('### getPaymentDataFromRedux :: unitPriceValue : ', unitPriceValue);
  console.log('### getPaymentDataFromRedux :: deviceSellingPrice : ', deviceSellingPrice);
  console.log('### getPaymentDataFromRedux :: unitPriceAdjustment : ', unitPriceAdjustment);
  console.log('### getPaymentDataFromRedux :: totalPaymentValue : ', totalPaymentValue);
  console.log('### getPaymentDataFromRedux :: eZCashtotalPay : ', eZCashtotalPay);
  console.log('### getPaymentDataFromRedux :: serialPackPrice : ', serialPackPrice);
  console.log('### getPaymentDataFromRedux :: simSerialPrice : ', simSerialPrice);
  console.log('### getPaymentDataFromRedux :: bundleSerialPrice : ', bundleSerialPrice);
  console.log('### =============================================================== ##');


  let totalPaymentProps = {
    unitPriceValue: unitPriceValue,
    dataplanSachetValue: dataPlanSachetValue,
    outstandingFeeValue: outstandingFeeValue,
    depositAmount: depositAmount,
    totalPayValue: totalPaymentValue
  };

  let eZCashTotalPaymentProps = {
    eZCashtotalPay: eZCashtotalPay,
    depositAmount: depositAmount,
    outstandingFeeValue: outstandingFeeValue,
    unitPriceAdjustment: unitPriceAdjustment,
    deviceSellingPrice : deviceSellingPrice,
    serialPackPrice: serialPackPrice,
    simSerialPrice : simSerialPrice,
    bundleSerialPrice : bundleSerialPrice,
    unitPriceValue: unitPriceValue,

  };

  console.log('### getPaymentDataFromRedux :: totalPaymentProps', totalPaymentProps);
  console.log('### getPaymentDataFromRedux :: eZCashTotalPaymentProps', eZCashTotalPaymentProps);

  return { totalPaymentProps, eZCashTotalPaymentProps };

};

const checkNumber = (number) => {
  console.log('xxx checkNumber :: number', number);
  if (number !== undefined && number !== null) {
    let numberString = number.toString().replace(/,/g, '');
    if (numberString !== "" && !isNaN(numberString)) {
      return parseFloat(numberString).toFixed(2);
    } else {
      return 0.00;
    }
   
  } else {
    return 0.00;
  }
};

export default {
  getPaymentDataFromRedux,
  checkNumber
};

