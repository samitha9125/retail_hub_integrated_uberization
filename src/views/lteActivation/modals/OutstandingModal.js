import React from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton';
import Images from '@Config/images';
import icon_mobile_src from '../../../../images/common/icons/icon_mobile_black.png';
import icon_dtv_src from '../../../../images/common/icons/icon_dtv_black.png';
import icon_lte_src from '../../../../images/common/icons/icon_lte_black.png';

//Not using for LTE

const HEIGHT = 40;
class OutstandingModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      nic: this.props.customerData.id_number,
      para: this.props.outstandingData.totalOsAmount,
      locals: {
        passportUC: strings.passportUC,
        nicUC: strings.nicUC,
        last: strings.last,
        rs_with_dot: 'Rs',
        confirmText: strings.confirmText,
        continueButtonText: strings.continueButtonText,
        cancelButtonTxt: strings.cancelButtonTxt,
        outstandingLeftText: strings.outstandingLeftText,
        outstandingRightText: strings.outstandingRightText
      }
    };
  }
  componentDidMount() {
    console.log('xxx customerData', this.props.customerData);
    console.log('xxx outstandingData', this.props.outstandingData);
  }

  outstandingConfirmCancel = () => {
    Alert.alert('Confirm', this.state.locals.confirmText, [
      {
        text: 'Yes',
        onPress: () => this.onConfirmYes()
      }, {
        text: 'No',
        onPress: () => console.log('No Pressed'),
        style: 'cancel'
      }
    ], { cancelable: false });

  }

  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = icon_mobile_src;
        break;
      case 'DTV':
        icon = icon_dtv_src;
        break;
      case 'LTE':
        icon = icon_lte_src;
        break;
    }
    return icon;
  }


  onConfirmYes = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this;
    setTimeout(() => {
      me.props.resetMobileActivationState();
    }, 50);

  }

  buttonSet = () => {
    return (
      <View style={styles.buttonInnerContainer}>
        <TouchableOpacity
          onPress={() => this.outstandingConfirmCancel()}
          style={styles.buttonCancelStyle}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.outstandingConfirmContinue(this.props.outstandingData, this.props.customerData)}
          style={styles.buttonContinueStyle}>
          <Text style={styles.textStyle}>
            {this.state.locals.continueButtonText}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderListItem = ({ item }) => (
    <ElementItem
      data={(item.display_name)}
      value={item.outstandingAmount}
      lob={item.lob}
      screen={this}
    />
  );

  render() {

    const customerData = this.props.customerData;
    const outstandingDataOb = this.props.outstandingData;
    console.log("OUTSTANDING_DATA :", outstandingDataOb);
    const id_type = (customerData.id_type == 'PP'
      ? this.state.locals.passportUC
      : this.state.locals.nicUC);

    let outstandingData = [];
    outstandingData = [...outstandingDataOb.info];

    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.imageContainerStyle}>
              <Image source={Images.icons.AttentionAlert} style={styles.imageStyle} />
            </View>
            <View style={styles.nicContainer}>
              <Text style={[styles.textStyle, styles.nicText]}>{id_type}
                : {this.state.nic}</Text>
            </View>
            <View style={styles.outstandingTextContainer}>
              <Text style={styles.textStyle}>
                {this.state.locals.outstandingLeftText}
              </Text>
              <Text style={styles.textStyle}>
                <Text style={styles.priceStyle}>{'Rs ' + this.state.para + ' '}</Text>
                {this.state.locals.outstandingRightText}
              </Text>
            </View>
            <View style={[styles.outstandingListContainer, outstandingData.length < 5 ? { height: HEIGHT * outstandingData.length } : { flex: 2 }]}>
              <ScrollView>
                <FlatList
                  data={outstandingData}
                  renderItem={this.renderListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={this.state.scrollEnabled}
                />
              </ScrollView>
            </View>
            <View style={styles.bottomTextContainer}>
              <Text style={styles.textStyle}>{this.state.locals.last}</Text>
            </View>
            <View style={styles.buttonContainer}>
              {this.buttonSet()}
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const ElementItem = ({ data, value, lob, screen }) => (
  <View style={styles.elementContainer}>
    <View style={styles.elementLeftLobIcon}>
      <Image source={screen.getLobIconSrc(lob)} style={styles.imageIconStyle} />
    </View>
    <View style={styles.elementLeftView}>
      <Text style={styles.elementLeftText}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>
      <View style={styles.elementRightInnerView}>
        <Text style={styles.elementRightText}>{screen.state.locals.rs_with_dot}</Text>
        <Text style={styles.elementRightText}>{value}</Text>
      </View>
    </View>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: LTE => OutstandingModal\n', state.lteActivation);
  const language = state.lang.current_lang;
  return {
    language,
  };
};


const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.5
  },

  modalContainer: {
    // flex: 5.5,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
    flexGrow: 7
  },
  bottomContainer: {
    flex: 0.5
  },
  innerContainer: {
    flex: 1,
    paddingTop: 5,
    padding: 10,
    paddingRight: 15,
    paddingLeft: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  nicContainer: {
    marginTop: 5,
    padding: 5
  },

  nicText: {
    fontWeight: '500',
    fontSize: 20
  },

  outstandingTextContainer: {
    // flex: 1.5,
    padding: 15,
    marginBottom: 10
  },

  outstandingListContainer: {
    // flex: 2,
    // marginTop: 5,
    // padding: 5
  },

  bottomTextContainer: {
    // flex: 1,
    // marginTop: 5,
    padding: 5
  },

  buttonContainer: {
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
    padding: 5
  },

  imageContainerStyle: {
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    width: 62,
    height: 62
  },
  textStyle: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  priceStyle: {
    color: Colors.colorRed
  },

  buttonInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },

  buttonContinueStyle: {
    backgroundColor: Colors.btnActive,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  buttonCancelStyle: {
    backgroundColor: Colors.colorWhite,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },

  elementContainer: {
    flex: HEIGHT,
    flexDirection: 'row'
  },

  elementLeftLobIcon: {
    flex: 0.3
  },

  imageIconStyle: {
    width: 22,
    height: 22,
    marginBottom: 10
  },

  elementLeftView: {
    flex: 1.4,
  },

  elementRightView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    //backgroundColor: Colors.colorGreen

  },

  elementRightInnerView: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginLeft: 5

  },

  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },

  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'
  }
});

export default connect(mapStateToProps, actions)(OutstandingModal);
