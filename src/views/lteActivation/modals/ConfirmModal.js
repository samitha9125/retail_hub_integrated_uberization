import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton.js';
import Orientation from 'react-native-orientation';

class ConfirmModal extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      package: [{
        data: this.props.local_foreign == 'local' ?
          strings.nicNo : strings.ppNo,
        value: this.props.idNumber
      }, {
        data: strings.type,
        value: strings.connetionType
      }, {
        data: strings.connectionNo,
        value: this.props.connectionNo
      }, {
        data: 'Bundle Serial',
        value: this.props.simNumber
      }, {
        data: strings.offer,
        value: this.props.offer
      }, {
        data: 'Data Packages',
        value: this.props.dataPacks
      }, {
        data: 'Monthly Rental',
        value: this.props.pkgRental
      }, {
        data: 'Contact No',
        value: this.props.contactNumber
      },
      {
        data: 'Email',
        value: this.props.email
      }
      ],
      paymentData: [{
        data: 'Connection Fee',
        value: this.props.connectionFee
      }, {
        data: 'Deposit amount',
        value: this.props.depositAmount
      },
      {
        data: strings.totalPay,
        value: this.props.totalPayment
      }
      ],

      tncDownloadUrl: 'https://www.dialog.lk/dialogdocroot/content/pdf/tc/postpaid-application-form-mob' +
        'ile.pdf',

      locals: {
        confirmButtonTxt: strings.confirmButtonTxt,
        cancelButtonTxt: strings.cancelButtonTxt,
        modalTitle: strings.modalTitle,
        lcdValue: strings.lcdValue,
        totalPayment: strings.totalPayment,
        byclicking: strings.byclicking,
        tns: strings.tns
      }
    };
  }

  
  componentWillMount(){
    Orientation.lockToPortrait();
  }

  dismissModal = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  openSignature = () => {
    console.log('Open Signature');
    this.dismissModal();
    if (this.props.isExsistingCustomer) {
      console.log('finalActivationMethod');
      this.props.finalActivationMethod();

    } else {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: this.state.customerSignature,
        screen: 'DialogRetailerApp.views.SignaturePadScreen',
        passProps: {
          downlodUrl: this.state.tncDownloadUrl
        }
      });

    }
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => this.dismissModal()}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => this.openSignature()}>
          <Text style={styles.textStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  setTnCtext = () => {
    if (this.props.isExsistingCustomer) {
      return (
        <View style={styles.tncView}>
          <Text style={styles.desTncTxt}>{this.state.locals.byclicking}
          </Text>
          <Text
            style={styles.desTncTxtLink}
            onPress={() => Linking.openURL(this.state.tncDownloadUrl)}>
            {this.state.locals.tns}
          </Text>
        </View>
      );
    } else {
      return (<View/>);
    }
  }

  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value}/>)

  render() {
    const finalPakageDetails = this.state.package;
    const paymentData = this.state.paymentData;
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.containerStyle}>
          <View style={styles.section_1_Container}>
            <Text style={styles.titleStyle}>{this.state.locals.modalTitle}</Text>
          </View>
          <View style={styles.section_2_Container}>
            <FlatList
              data={finalPakageDetails}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index}
              scrollEnabled={true}/>
          </View>
          <View style={styles.section_2_1_Container}/>
          <View style={styles.section_3_Container}>
            <FlatList
              data={paymentData}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index}
              scrollEnabled={false}/>
          </View>
          {this.setTnCtext()}
          <View style={styles.section_4_Container}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftItem}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementrightItem}>
      <Text style={styles.textStyleRight}>{value}</Text>
    </View>
  </View>
);

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor,
    padding: 5,
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 20,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    flex: 1.5
  },
  section_2_Container: {
    flex: 6
  },
  section_2_1_Container: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderLineColorLightGray,
    marginBottom: 10,
    marginTop: 10
  },
  section_3_Container: {
    flex: 3,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1
  },

  elementLeftItem: {
    flex: 0.9

  },
  elementrightItem: {
    flex: 1
  },
  viewStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },

  textStyleRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },
  textStyleLeft: {
    fontWeight: 'bold'

  },
  titleStyle: {
    fontSize: 24,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  confirmButtonStyle: {
    backgroundColor: '#edc92c',
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  cancelButtonStyle: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  tncView: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5
  },
  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: Styles.otpEnterModalFontSize
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: Styles.otpEnterModalFontSize,
    textDecorationLine: 'underline'
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION', state.mobile);
  const language = state.lang.current_lang;
  const local_foreign = 'local';
  const idNumber= '9135585555v';
  const connectionNo = '033123456';
  const simNumber = '5855255855555';
  const offer = 'Standard';
  const dataPacks = 'Lite Plus';
  const pkgRental = 'Rs 599';
  const contactNumber = '0777123456';
  const email = 'email';
  const connectionFee = 'Rs 5000';
  const depositAmount = 'Rs 300';
  const totalPayment = 'RS 5300';


  return {
    language,
    local_foreign,
    idNumber,
    connectionNo,
    simNumber,
    offer,
    dataPacks,
    pkgRental,
    contactNumber,
    email,
    connectionFee,
    depositAmount,
    totalPayment
  };
};

export default connect(mapStateToProps, actions)(ConfirmModal);