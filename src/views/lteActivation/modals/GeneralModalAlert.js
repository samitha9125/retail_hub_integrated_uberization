/*
 * File: GeneralModalAlert.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 25th July 2018 6:22:48 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 26th July 2018 11:34:31 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import Button from '@Components/others/Button';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
const { width, height } = Dimensions.get('window');

export default class GeneralModalAlert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { 
      description = '', 
      modalHeightRatio,
      disabled= false,
      hideTopImageView = false,  
      showTitle = false,
      title = '',
      descriptionTitle,
      customDescriptionTextView = false,   
      primaryPress,
      primaryPressPayload ={} ,
      primaryText,
      secondaryPress,
      secondaryPressPayload = {},
      secondaryText,
      additionalProps,
      descriptionTextStyle = {}, 
      descriptionTitleTextStyle = {},
      primaryButtonStyle = {}, 
      secondaryButtonStyle = {} ,
    } = this.props;

    let modalContainerCustomStyle = { height: height * modalHeightRatio };

    const TopImageView = () => {
      if (!hideTopImageView) {
        return (<View style={styles.topContainer} >
          <View style={styles.imageContainerStyle}>
            <Image
              resizeMode="contain"
              resizeMethod="scale"
              style={styles.titleImageStyle}
              source={this.props.icon}
            />
          </View>
        </View>);
      } else if (showTitle) {
        return (<View style={styles.topTitleContainer} >
          <Text style={styles.titleTextStyle}> {title} </Text>
        </View>);
      }  else {
        return true;
      }  
    };


    const DescriptionTextView = () => {
      if (customDescriptionTextView) {
        return true;
      } else {
        return (<Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{description}</Text>);
      }   
    };

    const DescriptionTitleView = () => {
      if (descriptionTitle) {
        return ( <View style={styles.descriptionTitleContainer}>
          <Text textAlign="center" style={[styles.descriptionTitleTextStyle, descriptionTitleTextStyle]}>{descriptionTitle}</Text>
        </View>);
      } else { 
        return true;
      }
    };

    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          {/*Top image view*/}
          {TopImageView()}
          {/*Description Title view*/}
          {DescriptionTitleView()}
          <View style={styles.descriptionContainer}>
            {/*Description view*/}
            {DescriptionTextView()}
          </View>
          <View style={styles.buttonContainer}>
            {secondaryText ? <Button children={secondaryText}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={() =>secondaryPress(secondaryPressPayload)}
            /> : null}
            <Button children={primaryText}
              childStyle={[styles.rightButtonStyle, primaryButtonStyle]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={disabled}
              onPress={() =>primaryPress(primaryPressPayload)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
   
  },
  modalContainer: {
    height: height * 0.5,
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
    paddingHorizontal: 10,
  },

  topContainer: {
    //flex: 1.2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
  },

  topTitleContainer: {
    //flex: 0.6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 5,  
  },

  descriptionContainer: {
    flex: 1.2,
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 20,
  },

  descriptionTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 6
  },

  buttonContainer: {
    flex: 0.6,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 18,
    paddingBottom: 10,
    paddingRight: 10, 
  },

  imageContainerStyle: {
    //flex: 1,
    marginBottom : 5,
    padding: 5,
    alignItems: 'center'
  },
  
  titleImageStyle: {
    width: 54,
    height: 54
  },

  titleTextStyle: {
    fontSize: Styles.generalModalTitleTextSize,
    fontWeight:'500',
    color: Colors.colorBlack,
  },

  descriptionTextStyle: {
    fontSize: Styles.generalModalTextSize,
    color: Colors.generalModalTextColor,
  },

  descriptionTitleTextStyle: {
    fontSize: Styles.generalAlertTitleTextSize,
    color: Colors.colorBlack,
  },

  leftButtonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingVertical: 5
  },
  rightButtonStyle: {
    color: Colors.colorGreen,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  }
});
