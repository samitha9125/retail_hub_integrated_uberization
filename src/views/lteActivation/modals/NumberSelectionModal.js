/*
 * File: NumberSelectionModal.js
 * Project: Dialog Sales App
 * File Created: Monday, 13th August 2018 9:52:44 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Tuesday, 14th August 2018 4:41:49 pm
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
  Keyboard,
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import refreshIcon from '../../../../images/common/refresh_icon.png';
import Color from '../../../config/colors';
import Styles from '../../../config/styles';
import Constants from '../../../config/constants';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ActivityIndicator from '../components/ActivityIndicator';
import strings from '../../../Language/LteActivation';

import _ from 'lodash';

class NumberSelectionModel extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      num_selection_option: '',
      numSelectionData: [],
      selectedNumber: '',
      filterOptionId: 0,
      filterOption: 'Random',
      blocked_id_second: '',
      maxLength: 4,
      searchString:'',
      locals:{
        numberSelection: strings.numberSelection,
        numberRandom: strings.numberRandom,
        numberSearch: strings.numberSearch,
        numberInclude: strings.numberInclude,
        numberStart: strings.numberStart,
        numberEnd: strings.numberEnd,
        btnCancel: strings.btnCancel,
        btnContinue: strings.btnContinue,
        btnConfirm: strings.btnConfirm,
        dropDownOptions:[strings.numberRandom, strings.numberSearch, strings.numberInclude, strings.numberStart, strings.numberEnd],
        textInputHelperTexts:[
          '',
          strings.next_7_digit,
          strings.next_4_digit_without_area_code,
          strings.next_4_digit,
          strings.last_4_digit
        ]
      },
      pre_post: 'pre'
    };
    this.searchValue = '';
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  /**
   * Initially reserve 10 numbers when the number selection popup appears
   *
   * @memberof NumberSelectionModel
   */
  componentDidMount(){
    this.releaseNumberPool({ type: Constants.RELEASE_TYPES.REFRESH });
  }

  /**
   * This function will be invoked when  
   * @param {Number} number - Value of the selected connection number
   * @memberof NumberSelectionModel
   */
  onPressNumber = (number) => {
    this.setState({ selectedNumber: number });
    console.log("On press Number", number);
  }

  /**
   * Cancel button Action
   *
   * @memberof NumberSelectionModel
   */
  onBtnPressCancel = () => {
    console.log('Cancel Pressed.');
    this.setState({ selectedNumber: '' }, ()=>{
      this.releaseNumberPool({ type: Constants.RELEASE_TYPES.RELEASE });

      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    });
  }

  /**
   *Confirm button Action
   *
   * @memberof NumberSelectionModel
   */
  onBtnPressContinue = () => {
    console.log('Continue Pressed.');
    // If the user has selected any connection number, update redux store with selected connection number. 
    // Else, just release the number pool
    if (this.state.selectedNumber !== ''){
      let data = {
        selected_number: this.state.selectedNumber,
        blocked_id: ""
      };
      // this.props.setSelectedConnectionNumber(data);
    }

    this.releaseNumberPool({ type: Constants.RELEASE_TYPES.RELEASE });
    
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  /**
   * This function is called when the top bar drop down item is selected.
   * 
   * @param {Number} idx - id of the selected dropdown item
   * @param {String} value - text value of the selected drop down item
   * @memberof NumberSelectionModel
   */
  didSelectFilterOption = (idx, value) =>{
    console.log('onDataPackageInfoDropdownSelect', idx, value);
    if (idx == 0){
      this.releaseNumberPool({ type: Constants.RELEASE_TYPES.REFRESH });
    } else {
      this.releaseNumberPool({ type: Constants.RELEASE_TYPES.RELEASE });
    }
    this.setState({ filterOptionId: parseInt(idx), filterOption: value, searchString: '' });
  }

  /**
   * This function is called when a search string is entered into the search textfield
   * 
   * This function will call releaseNumberPool with type: 'search' and request params for search. This will release the 
   * current reserved number set and reserve the search result.
   * 
   * filterOptionId: 1 = 'includes', 2 = 'start_with', 3 = 'end_with'
   * 
   * @param {String} text - input value of the search textfield
   * @memberof NumberSelectionModel
   */
  onSearchTextChange = (text) => {
    console.log('onTextInputChange\n','text: ', text, '\n', 'selectedOption: ',this.state.filterOptionId);
    let requestParams = {};

    this.setState({ searchString: text },()=>{
      switch (this.state.filterOptionId) {
        case 1:
          requestParams.search = text;
          break;
        case 2:
          requestParams.includes = text;
          break;
        case 3:
          requestParams.start_with = text;
          break;
        case 4:
          requestParams.end_with = text;
          break;
      }
      if (this.state.filterOptionId !== 1 && text.length == 4){
        Keyboard.dismiss();
        this.releaseNumberPool({ type: Constants.RELEASE_TYPES.SEARCH }, requestParams);
        return;
      }

      if (this.state.filterOptionId == 1 && text.length == 7){
        Keyboard.dismiss();
        this.releaseNumberPool({ type: Constants.RELEASE_TYPES.SEARCH }, requestParams);
      }
    });
  };

  /**
   * This function will call releaseNumberPool with type: 'refresh'.
   * This will release the current reserved number set and reserve a new batch of numbers from number pool.
   *
   * @memberof NumberSelectionModel
   */
  onBtnPressRefresh = () => {
    console.log('Refresh button clicked');
    this.dropDown.select(0);
    this.setState({ filterOptionId: 0, filterOption: 'Random', searchString: '' });
    this.releaseNumberPool({ type: Constants.RELEASE_TYPES.REFRESH });
  }

  /**
   * This common function will be called for all the number pool release related tasks.
   * 
   * @param {Object} type - Type of number pool release that is needed to be done. 
   *                      - Expected parameter=> { type: 'release'/ 'refresh'/ 'search' }
   *                 
   * @param {Object} searchParams - This parameter is mandatory when type = 'search'
   *                              - Expected parameter=> {includes: '4242'}/ {start_with:'4242'}/ {end_with:'4242'}
   * @memberof NumberSelectionModel
   */
  releaseNumberPool = (type={}, searchParams={}) =>{ // Types: 'refresh', 'release'
    this.props.numberPoolRelease(type,searchParams,this);
  }

  render() {
    let hasPrefix = this.state.filterOptionId == 1 || this.state.filterOptionId == 3;
    let { reserved_number_list = {} } = this.props;
    let { number_list = [] } = reserved_number_list;
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <ActivityIndicator animating={this.props.apiLoading} text={this.props.api_call_indicator_msg}/>
          <View style={styles.modelContainerTop}>
            <View style={styles.modalTitleTxtContainer}>
              <Text style={styles.modalTitleTxt}>{ this.state.locals.numberSelection }</Text>
            </View>
            
            <View style={styles.refreshIconContainer}>
              <TouchableOpacity
                style={styles.refreshIconInnerContainer}
                onPress={this.onBtnPressRefresh}
                activeOpacity={1}
                disabled={false}
              >
                <Image source={refreshIcon} resizeMode={'contain'} style={styles.refreshIconStyle} />
              </TouchableOpacity>
            </View>
            <View style={styles.pickerContainer}>
              <ModalDropdown
                options={this.state.locals.dropDownOptions}
                ref={(ref) => this.dropDown = ref}
                onSelect={this.didSelectFilterOption}
                defaultIndex={this.state.filterOptionId}
                style={styles.modalDropdownStyles}
                dropdownStyle={styles.dropdownStyle}
                dropdownTextStyle={styles.dropdownTextStyle}
                dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
                <View style={styles.dropdownElementContainer}>
                  <View style={styles.dropdownDataElemint}>
                    <Text ellipsizeMode ='tail' numberOfLines={2} style={styles.dropdownDataElemintTxt}>{ this.state.filterOption }</Text>
                  </View>
                  <View style={styles.dropdownArrow}>
                    <Ionicons name='md-arrow-dropdown' size={20}/>
                  </View>
                </View>
              </ModalDropdown>
            </View>
          </View>
          { this.state.filterOptionId !== 0 ? 
            <View>
              { hasPrefix ?
                <Text style={ styles.numberPrefixText }>
                  { this.props.lteSelectedArea.area_code }
                </Text>
                : true }
              <TextInput
                style={ hasPrefix ? 
                  styles.numberSearchInputWithPrefix: styles.numberSearchInputNoPrefix }
                value={this.state.searchString}
                onChangeText={this.onSearchTextChange}
                maxLength={this.state.filterOptionId === 1 ? 7 : this.state.maxLength}
                keyboardType={'numeric'} 
                title={'Enter the next 7 digits to search'}
              /> 
              <Text style={ styles.numberHelperText }>
                { this.state.locals.textInputHelperTexts[this.state.filterOptionId] }
              </Text>
            </View>: true }

          <View style={styles.radioFormContainer}>
            <View style={styles.radioFormInnerContainer}>
              {_.isObject(this.props.reserved_number_list) && number_list.length > 0 && this.props.apiLoading == false ?
                <RadioForm
                  radio_props={number_list}
                  initial={-1}
                  onPress={this.onPressNumber}
                  buttonColor={'#939393'}
                  selectedButtonColor={'#FFC400'}
                  buttonSize={10}
                  buttonOuterSize={20}
                  labelStyle={styles.radioFormLabelStyle} 
                /> 
                :
                true 
              }
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.cancelBtnContainer}>
              <TouchableOpacity       
                onPress={()=>this.onBtnPressCancel()}
                activeOpacity={1}
              >
                <Text style={styles.cancelBtnTxt}>{ this.state.locals.btnCancel }</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.continueBtnContainer}>
              <TouchableOpacity           
                onPress={()=>this.onBtnPressContinue()}
                activeOpacity={1}
              >
                <Text style={styles.cancelBtnTxt}>{ this.state.locals.btnConfirm }</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>

    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: NUMBER SELECTION => LTE ', state.lteActivation);
  const Language = state.lang.current_lang;
  const apiLoading = state.lteActivation.apiLoading;
  const api_call_indicator_msg = state.lteActivation.api_call_indicator_msg;
  const lteSelectedArea = state.lteActivation.lteSelectedArea;
  const selected_connection_type = state.lteActivation.selected_connection_type;
  const reserved_number_list = state.lteActivation.reserved_number_list;
  const selected_connection_number = state.lteActivation.selected_connection_number;


  return { 
    apiLoading,
    Language,
    api_call_indicator_msg,
    lteSelectedArea,
    selected_connection_type,
    reserved_number_list,
    selected_connection_number
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Color.modalOverlayColorLow
  },
  innerContainer:{
    flex:0.85,
    width: Dimensions
      .get('window')
      .width - 40,
    height: Dimensions
      .get('window')
      .height - 120,
    backgroundColor: Color.appBackgroundColor,
    flexDirection:'column',
    padding:10
  },
  modelContainerTop: {
    height:50,
    flexDirection: 'row'
  },
  radioFormContainer: {
    flex: 5
  },
  bottomContainer: {
    height:45,
    flexDirection: 'row',
    paddingLeft:50
  },
  cancelBtnContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight:5
  },
  continueBtnContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Color.btnActive,
    paddingLeft:5,
    borderRadius: 3
  },
  cancelBtnTxt: {
    flex: 1,
    color: Color.btnActiveTxtColor,
    fontWeight: '500',
    fontSize: Styles.defaultBtnFontSize,
    textAlign: 'center',
    alignSelf: 'center',
    textAlignVertical: 'center'
  },

  modalTitleTxt: {
    flex: 1,
    fontSize: 17,
    fontWeight: '500',
    textAlign: 'left',
    textAlignVertical: 'center',
    color: Color.black,
    margin:0,
    paddingLeft:10
  },

  refreshIconContainer: {
    flex: 0.5
  },
  modalTitleTxtContainer:{
    flex:2.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 0,

  },
  pickerContainer:{
    width: Dimensions
      .get('window')
      .width * 0.3,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding:0
  },
  refreshIconInnerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  refreshIconStyle: {
    flex: 1,
    width: 30,
    height: 30
  },
  modalDropdownStyles: {
    flex:1,
  },
  dropdownStyle: {
    width: Dimensions.get('window').width * 0.3
  },
  dropdownDataElemintTxt: {
    flex:1,
    color: Color.colorBlack,
    fontSize: 17,
    fontWeight: '500'
  },
  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 40,
    padding: 5,
    paddingTop:10
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5    
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownTextStyle: {
    color: Color.colorBlack,
    fontSize: 17,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },

  radioFormInnerContainer: {
    flex: 1,
    paddingTop: 10,
    borderWidth: 0,
    backgroundColor: Color.appBackgroundColor,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    alignItems: 'flex-start',
    paddingLeft: 10
  },

  radioFormLabelStyle: {
    flex: 1,
    fontSize: 17,
    color: Color.black,
    lineHeight: 25,
    marginLeft: 0
  },

  numberSearchInputWithPrefix: {
    height: 40,
    borderWidth: 1,
    borderColor: Color.borderColor,
    paddingLeft: 45
  },
  numberSearchInputNoPrefix:{
    height: 40,
    borderWidth: 1,
    borderColor: Color.borderColor
  },
  dropdownTextHighlightStyle: {
    fontWeight: '500'
  },
  numberPrefixText:{ 
    position:'absolute', 
    left: 15, 
    top: 10, 
    height: 40, 
    fontWeight: '500', 
  },
  numberHelperText:{
    fontSize:14,
    fontWeight: '500',
    color: Color.helperTextFontColor
  }
});

export default connect(mapStateToProps, actions)(NumberSelectionModel);
