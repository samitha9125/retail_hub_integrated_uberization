import React from 'react';
import {
  StyleSheet, View, ScrollView, Text, TextInput, 
  TouchableOpacity,Dimensions, Image, KeyboardAvoidingView
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Images from '../../../config/images';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/MobileActivaton';
import string_dtv from '../../../Language/LteActivation';
import CloseIcon from '../../../../images/common/close-icon.png';
import _ from 'lodash';

const MIN_RELOAD_AMOUNT = 50;
const MAX_RELOAD_AMOUNT = 25000; //TODO: configure
const FAT_LIST_ITEM_HIGHT = 85;
const CONTAINER_MARGIN = 60;
const { width, height } = Dimensions.get('window');

const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

const Utill = new Utills();

class DataSachetSelectionModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    string_dtv.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      selectedKey: 0,
      reloadValue: '',
      more: false,
      offerData: [],
      selectedOfferData: {   
        offer_key: null,
        offer_val : null
      } ,
      local: {
		ok: strings.btnOk,
        enter_valid_amount: strings.enter_valid_amount,
        reload_cant_empty: strings.reload_cant_empty,
        select_sachet_offer: string_dtv.select_sachet_offer,
        continue: string_dtv.btnContinue,
        more_offers: string_dtv.more_offers,
        ok: strings.btnOk,
      }
    };
    this.debouncedOnBtnPressContinue = _.debounce(() => this.onBtnPressContinue(), 300, DEBOUNCE_OPTIONS);
  }

  componentDidMount () {
    console.log('xxx  DataSachetSelectionModal :: componentDidMount');
    this.setState({ offerData: this.props.offerDataList } ,  () => this.resetSelection(0));  
  }

  onBtnPressContinue =()=> {
    let me = this;
    console.log('xxx onBtnPressContinue');
    const regex = /^\d+$/;
    if (this.state.reloadValue == '') {
      Utill.showAlertMsg(this.state.local.reload_cant_empty);
      return;
    }

    if (!(regex.test(this.state.reloadValue) 
      && this.state.reloadValue >= MIN_RELOAD_AMOUNT 
      && this.state.reloadValue <= MAX_RELOAD_AMOUNT)) {
      Utill.showAlertMsg(this.state.local.enter_valid_amount);
      return;
    }

    if (this.state.reloadValue !== 0 || this.state.reloadValue !== '') {
      const requestParams = {
        amount: this.state.reloadValue
      };
      me.props.commonRapidEzAccountCheck(requestParams, this.agentAccountCheckSuccess, this);

    } else {
      this.props.navigator.dismissModal({ animationType: 'slide-down' });
    }
  }

  onBtnPressCancel = () => {
    console.log('xxx onBtnPressCancel');
    this.props.lteSetReload({
      offer_key: null,
      offer_val: null,
    });
    this.props.navigator.dismissModal({ animationType: 'slide-down' });
  }

  showRapidEzErrorModal = (response) => {
    console.log('xxx showRapidEzErrorModal');
    let passProps = {
      primaryText: this.state.local.ok,
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      icon: Images.icons.FailureAlert,
      descriptionTitle: response.title,
      description: Utill.getStringifyText(response.error),
      descriptionBottomText: response.balance,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryPress: () => this.dismissModal()
    };

    Navigation.showModal({
      title: 'CommonModalAlert',
      screen: 'DialogRetailerApp.modals.CommonModalAlert',
      passProps: passProps
    });
  }

  dismissModal = () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  agentAccountCheckSuccess = () => {
    console.log('xxx agentAccountCheckSuccess');
    this.props.lteSetReload(this.state.selectedOfferData);
    this.props.navigator.dismissModal({ animationType: 'slide-down' });
  }

  resetSelection = (key) => {
    console.log('xxx resetSelection : ', key);
    try {
      console.log('xxx resetSelection :: TRY');
      if (key !== -1){
        let offer_val = (this.state.offerData[key].offer_val !== undefined
          && this.state.offerData[key].offer_val !== null) ? this.state.offerData[key].offer_val : null;
    
        let offer_key = (this.state.offerData[key].offer_key !== undefined
          && this.state.offerData[key].offer_key !== null) ? this.state.offerData[key].offer_key : null;
    
        let charge = (this.state.offerData[key].offer_val.CHARGE !== undefined
          && this.state.offerData[key].offer_val.CHARGE !== null) ? this.state.offerData[key].offer_val.CHARGE : '';
        
        let reload_offer_code = (this.state.offerData[key].offer_val.CODE !== undefined
          && this.state.offerData[key].offer_val.CODE !== null) ? this.state.offerData[key].offer_val.CODE : '';
  
        console.log('xxx resetSelection :: reload_offer_code', reload_offer_code);
        this.setState({ 
          selectedKey: key,
          reloadValue: charge, 
          reload_offer_code: reload_offer_code,
          selectedOfferData: {   
            offer_key: offer_key,
            offer_val : offer_val
          } 
        });
      } else {
        this.setState({ 
          selectedKey: key,
          reloadValue: '', 
          reload_offer_code: '',
          selectedOfferData: {   
            offer_key: null,
            offer_val : null
          } 
        });
      }

          
    } catch (error) {
      console.log('xxx resetSelection :: CATCH', error);
      this.setState({ 
        selectedKey: key,
        reloadValue: '', 
        selectedOfferData: {   
          offer_key: null,
          offer_val : null
        } 
      });
    }  
  };

  valueChanged = (value, code) => {
    console.log('xxx valueChanged : ',code);
    this.resetSelection(-1);
    this.setState({ 
      reloadValue: value, 
      selectedOfferData: {   
        offer_key: null,
        offer_val : {
          CHARGE: value,
          CODE : code
        }
      } 
    });
  }

  viewMore = () => {
    console.log('xxx viewMore');
    this.setState({ more: true });
  }

  render() {
    const offerItems = [];
    let offerDataCount = this.state.offerData.length;
    offerDataCount = Math.min(3, offerDataCount);
    for (let i = 0; i < offerDataCount; i++) {
      offerItems.push(<OfferItem
        item={this.state.offerData[i].offer_val}
        key={i}
        index={i}
        borColor={this.state.selectedKey === i
          ? '#FFC400'
          : '#DDDDDD'}
        onPress={() => this.resetSelection(i)} 
      />);
    }
    let offerList = (
      <View style={styles.fatListContainer}>
        {offerItems}
        {this.state.offerData.length > 3
          ? (
            <View>
              <Text style={styles.viewMoreContainer} onPress={() => this.viewMore()}>
                {this.state.local.more_offers}
              </Text>
            </View>
          )
          : null
        }
      </View>
    );

    if (this.state.more) {
      const offerItems = [];
      const offerDataCount = this.state.offerData.length;
      for (let i = 0; i < offerDataCount; i++) {
        offerItems.push(<OfferItem
          item={this.state.offerData[i].offer_val}
          key={i}
          index={i}
          borColor={this.state.selectedKey === i
            ? '#FFC400'
            : '#DDDDDD'}
          onPress={() => this.resetSelection(i)} 
        />);
      }
      offerList = (
        <View style={styles.fatListContainer}>
          <ScrollView style={styles.fatListContainerScrollable}>
            {offerItems}
          </ScrollView>
        </View>
      );
    }

    return (
      <KeyboardAvoidingView style={styles.containerOverlay} behavior="padding" enabled >
        <View style={styles.modalContainer}>
          <View style={styles.topContainer}>
            <View style={styles.modelTitleContainer}>
              <Text style={styles.modelTitleTxt}>{this.state.local.select_sachet_offer}</Text>
            </View>
            <TouchableOpacity
              style={styles.closeIconContainer}
              onPress={this.onBtnPressCancel}
            >
              <Image source={CloseIcon} style={styles.closeIconStyle} />
            </TouchableOpacity>
          </View>
          {offerList}
          <View style={styles.bottomContainer}>
            <View style={styles.textInputContainer}>
              <TextInput
                style={styles.bottomContainerTextInput}
                keyboardType="numeric"
                value={`${this.state.reloadValue}`}
                underlineColorAndroid={Colors.colorWhite}
                onChangeText={this.valueChanged} 
              />
            </View>
            <TouchableOpacity
              style={styles.bottomContainerBtn}
              onPress={this.debouncedOnBtnPressContinue}
            >
              <Text style={styles.bottomContainerBtnTxt}>{this.state.local.continue}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>

    );
  }
}

const OfferItem = ({ item, index, borColor, onPress }) => {
  const dynamicItems = [];
  const resourceCount = item.RESOURCES.length;
  for (let i = 0; i < resourceCount; i++) {
    const resource = item.RESOURCES[i];
    dynamicItems.push(
      <View style={styles.fatListItemResources} key={i}>
        <Text style={styles.fatListItemResourceLabelText}>{resource.LABEL}</Text>
        <Text style={styles.fatListItemResourceValueText}>{resource.VALUE}</Text>
        <Text style={styles.fatListItemResourceTypeText}>{resource.TYPE}</Text>
      </View>
    );
  }


  return (
    <TouchableOpacity
      style={[
        styles.fatListInnerContainer, {
          borderColor: borColor
        }
      ]}
      onPress={onPress}
      index={index}
    >
      <View style={styles.fatListItemAmount}>
        <Text style={styles.fatListItemAmountText}>Rs</Text>
        <Text style={styles.fatListItemAmountText}>{item.CHARGE}</Text>
      </View>
      <View style={styles.fatListItemContainerResources}>
        {dynamicItems}
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
  },

  modalContainer: {
    width: width * 0.92,
    height:( height * 0.75) > (FAT_LIST_ITEM_HIGHT * 3 + 200) ? 
      (height * 0.75 ) : (FAT_LIST_ITEM_HIGHT * 3 + 200) ,
    backgroundColor: Colors.colorWhiteAlpha,
    borderRadius: 2,
    padding: 10,
    marginTop: 10,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
  },


  topContainer: {
    flexDirection: 'row',
    padding: 5,
    backgroundColor: Colors.colorWhiteAlpha
   
  },

  fatListContainer: {
    height: FAT_LIST_ITEM_HIGHT * 3 + CONTAINER_MARGIN,
    backgroundColor: Colors.colorWhiteAlpha,
  },

  bottomContainer: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    paddingTop: 10,
  },

  modelTitleContainer: {
    backgroundColor: Colors.colorWhiteAlpha,
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: 5,
    paddingTop: 5,
  },

  closeIconContainer: {
    alignSelf: 'flex-end',
    padding: 5
  },

  closeIconStyle: {
    width: 30,
    height: 30
  },

  modelTitleTxt: {
    fontSize: 22,
    color: Colors.colorLightBlack,
    fontWeight: '500'
  },

  fatListContainerScrollable: {
    height: FAT_LIST_ITEM_HIGHT * 3 + CONTAINER_MARGIN,
    backgroundColor: Colors.colorWhiteAlpha,
  },

  viewMoreContainer: {
    color: Colors.urlLinkColor,
    textAlign: 'center',
    fontSize: 15,
    marginTop: 5,
    marginBottom: 8,
    textDecorationLine: 'underline'

  },

  fatListInnerContainer: {
    height: FAT_LIST_ITEM_HIGHT,
    flexDirection: 'row',
    padding: 0,
    margin: 5,
    borderWidth: 2,
    borderRadius: 5
  },

  fatListItemAmount: {
    flex: 1.6,
    justifyContent: 'center',
    backgroundColor: Colors.offerBackgroundColor
  },

  fatListItemAmountText: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 20
  },

  fatListItemContainerResources: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },

  fatListItemResources: {
    flex: 1
  },

  fatListItemResourceLabelText: {
    textAlign: 'center',
    fontSize: 11,
    marginBottom: 5
  },

  fatListItemResourceValueText: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 5
  },

  fatListItemResourceTypeText: {
    textAlign: 'center',
    fontSize: 10
  },

  textInputContainer: {
    backgroundColor:  Colors.colorWhite,
    flex: 5,
    marginRight: 10,
    marginLeft: 10,
    height: 45,
  },

  bottomContainerBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 2,
    flex: 4,
    marginLeft: 10
  },

  bottomContainerTextInput: {
    backgroundColor:  Colors.colorWhite,
    borderRadius: 2,
    height: 45,
    borderWidth: 1,
    borderColor: Colors.borderLineColorLightGray
  },

  bottomContainerBtnTxt: {
    color: Colors.colorBlack,
    fontSize: 18,
    fontWeight: '500',
    backgroundColor: Colors.colorTransparent,
  },

  
});

export default connect(mapStateToProps, actions)(DataSachetSelectionModal);
