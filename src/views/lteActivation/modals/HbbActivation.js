/*
 * File: HbbActivation.js
 * Project: Dialog Sales App
 * File Created: Thursday, 16th August 2018 5:40:57 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: Friday, 17th August 2018 8:34:43 pm
 * Modified By: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */


import React from 'react';
import { View, Text, FlatList, BackHandler, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/LteActivation';
import Orientation from 'react-native-orientation';
import Constants from '../../../../src/config/constants';
import { Header } from '../components/Header';
import Utills from '../../../utills/Utills';

import _ from "lodash";

const Utill = new Utills();
class HbbActivation extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      locals: {
        confirmButtonTxt: strings.confirmButtonTxt,
        cancelButtonTxt: strings.cancelButtonTxt,
        modalTitleNotVerified: strings.modalTitle,
        dataSachetReload: strings.dataSachetReload,
        unitPrice: strings.unitPrice,
        depositAmount: strings.depositAmount,
        totalPay: strings.totalPaymentText,
        outstandingFee: strings.outstandingFee,
        modalTitleOtpVerified: strings.modalTitleOtpVerified,
        title: strings.title,
        backMessage: strings.backMessage,
        dataRental: strings.dataRental,
        voicePackageRental: strings.voicePackageRental,
        taxMonthly : strings.taxMonthly

      }
    };
  }


  componentWillMount() {
    Orientation.lockToPortrait();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.dismissModal();
  
  }

  dismissModal = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  openSignature = () => {
    this.dismissModal();
    Orientation.lockToLandscape();
    this.props.showSignature();
  }

  checkNumber = (number) => {
    console.log('xxx checkNumber :: checkNumber : ', number);
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00");
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => {
            this.dismissModal();
            Orientation.lockToPortrait();
          }}>
          <Text style={styles.ConfirmAndCancelTextStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => {
            this.openSignature();
          }}>
          <Text style={styles.ConfirmAndCancelTextStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }


  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value} />)
  renderPaymentListItem = ({ item }) => (<ElementPaymentItem data={item.data} value={item.value} />)
  renderTotalPaymentItem = ({ item }) => (<ElementTotalPaymentItem data={item.data} value={item.value} />)

  render() {

    //array for Hbb confirmation Left, right Columns data
    let mainItemsArray = [
      {
        data: this.props.customerType == Constants.CX_TYPE_LOCAL
          ? strings.nicNo
          : strings.ppNo,
        value: this.props.idNumber_input_value
      }, {
        data: strings.paymentPlan,
        value: this.props.selected_connection_type
      }, {
        data: strings.connectionNumber,
        value: this.props.connection_Number.selected_number
      }, {
        data: strings.offer,
        value: this.props.offer_value.selectedValue
      }, {
        data: strings.dataPackages,
        value: this.props.data_pakages_value.selectedValue
      }, {
        data: strings.mobileContactNo,
        value: this.props.mobile_contact_no
      }
    ];


    let dataPackagesIndex = _.findIndex(mainItemsArray, { data: strings.dataPackages });
    if (this.props.voice_Package.selectedValue.length != 0) {
      mainItemsArray.splice(dataPackagesIndex + 1, 0, {
        data: strings.voicePackage,
        value: this.props.voice_Package.selectedValue
      });
    }

    let offerIndex = _.findIndex(mainItemsArray, { data: strings.offer });

    const { warrantyType = [] } = this.props;

    warrantyType.forEach(function (warrantyItem, index) {
      console.log('warrantyType', index);
      console.log('warrantyItem', warrantyItem);   
      if (warrantyItem.warrantyValue) {
        const warrantyUnitValidation = warrantyItem.warrantyUnit !== null
              && warrantyItem.warrantyUnit !== undefined ? warrantyItem.warrantyUnit : '';

        mainItemsArray.splice(offerIndex + 1, 0, {
          data: warrantyItem.warrantyType,
          value: warrantyItem.warrantyValue + ' ' + warrantyUnitValidation
        });
      }
    });

    if (this.props.selected_connection_type === Constants.PREPAID) {
      let mobileIndex = _.findIndex(mainItemsArray, { data: strings.mobileContactNo });
      if (this.props.Landline_contact_no.length != 0) {
        mainItemsArray.splice(mobileIndex + 1, 0, {
          data: strings.LandlineContactNo,
          value: this.props.Landline_contact_no
        });
      }

      if (this.props.email.length != 0) {
        if (this.props.Landline_contact_no.length != 0) {
          mainItemsArray.splice(mobileIndex + 2, 0, {
            data: strings.email,
            value: this.props.email
          });

        } else {
          mainItemsArray.splice(mobileIndex + 1, 0, {
            data: strings.email,
            value: this.props.email
          });
        }
      }
    }


    if (this.props.selected_connection_type === Constants.POSTPAID) {

      let dataPackageIndex = _.findIndex(mainItemsArray, { data: strings.dataPackages });

      mainItemsArray.splice(dataPackageIndex + 1, 0, {
        data: strings.dataRental,
        value: 'Rs ' + this.checkNumber(this.props.data_package_rental) + ' + ' + strings.taxMonthly
      });

      if (this.props.voice_Package.selectedValue.length != 0) {
        mainItemsArray.splice(dataPackageIndex + 3, 0, {
          data: strings.voicePackageRental,
          value: 'Rs ' + this.checkNumber(this.props.voice_PackageValue) + ' + ' + strings.taxMonthly
        });

        mainItemsArray.splice(dataPackageIndex + 4, 0, {
          data: strings.billCycle,
          value: this.props.bill_cycle.selectedValue
        });
      } else {

        mainItemsArray.splice(dataPackageIndex + 3, 0, {
          data: strings.billCycle,
          value: this.props.bill_cycle.selectedValue
        });

      }

      let PostpaidMobileIndex = _.findIndex(mainItemsArray, { data: strings.mobileContactNo });

      if (this.props.Landline_contact_no.length != 0) {
        mainItemsArray.splice(PostpaidMobileIndex + 1, 0, {
          data: strings.LandlineContactNo,
          value: this.props.Landline_contact_no
        });
      }

      if (this.props.email.length != 0) {
        if (this.props.Landline_contact_no.length != 0) {
          mainItemsArray.splice(PostpaidMobileIndex + 2, 0, {
            data: strings.email,
            value: this.props.email
          });

        } else {

          mainItemsArray.splice(PostpaidMobileIndex + 1, 0, {
            data: strings.email,
            value: this.props.email
          });
        }
      }
    }

    let paymentDataArray = [
      {
        data: this.state.locals.unitPrice,
        value: Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.unitPriceValue))
      }
    ];

    if (this.props.selected_connection_type == Constants.POSTPAID) {
      paymentDataArray.push({
        data: this.state.locals.depositAmount,
        value: Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.depositAmount))
      });
    }


    if (this.props.selected_connection_type == Constants.PREPAID) {
      paymentDataArray.push({
        data: this.state.locals.dataSachetReload,
        value: Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.dataplanSachetValue))
      });
    }

    if (this.props.needToShowOutstanding) {
      paymentDataArray.push({
        data: this.state.locals.outstandingFee,
        value: Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.outstandingFeeValue))
      });
    }

    let TotalPaymentData = [{
      data: this.state.locals.totalPay,
      value: Utill.numberWithCommas(this.checkNumber(this.props.totalPaymentProps.totalPayValue))
    }];


    return (
      <View style={styles.containerOverlay}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title} />
        <ScrollView
          scrollEnabled={true}
          showsVerticalScrollIndicator={true}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode='on-drag'>
          <View style={styles.containerStyle}>
            <View style={styles.section_1_Container}>
              <Text style={styles.titleStyle}>{strings.modalTitle}</Text>
            </View>
            <View style={styles.section_2_Container}>
              <FlatList
                data={mainItemsArray}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false}
              />
            </View>
            <View style={styles.section_2_1_Container} />
            <View style={styles.section_3_Container}>
              <FlatList
                data={paymentDataArray}
                renderItem={this.renderPaymentListItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false} />
              <FlatList
                data={TotalPaymentData}
                renderItem={this.renderTotalPaymentItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={false} />

            </View>
            <View style={styles.section_4_Container}>
              {this.buttonSet()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftItem}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightItem}>
      <Text style={styles.textStyleRight}>{value}</Text>
    </View>
  </View>
);

const ElementPaymentItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftView}>
      <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>

      <View style={styles.elementRightInnerView1}>
        <Text style={styles.textStyleRight}>Rs </Text>
      </View>
      <View style={styles.elementRightInnerView2}>
        <Text style={styles.textStyleRight}>{value}</Text>
      </View>
      <View style={styles.elementRightInnerView3}></View>
    </View>
  </View>
);

const ElementTotalPaymentItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftView}>
      <Text style={[styles.textStyleForTotalPayment, styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementRightView}>

      <View style={styles.elementRightInnerView1}>
        <Text style={styles.textStyleForTotalPaymentRight}>Rs </Text>
      </View>
      <View style={styles.elementRightInnerView2}>
        <Text style={styles.textStyleForTotalPaymentRight}>{value}</Text>
      </View>
      <View style={styles.elementRightInnerView3}></View>
    </View>
  </View>
);

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.white
  },
  elementRightInnerView: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row'

  },
  elementRightInnerView1: {
    flex: 0.2,
    alignItems: 'flex-start',
  },
  elementRightInnerView2: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  elementRightInnerView3: {
    flex: 0.3,
    alignItems: 'flex-start',
  },
  elementRightInnerViewsub: {
    flex: 0.75,
    flexDirection: 'row'
  },
  elementRightInnerViewsub1: {
    flex: 0.25,
    flexDirection: 'row'
  },

  elementLeftView: {

    flex: 1
  },
  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'

  },
  elementRightView: {
    flex: 1,
    flexDirection: 'row'

  },
  elementRightView1: {
    flex: 1,
    alignItems: 'flex-start'

  },

  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    flex: 1.5
  },
  section_2_Container: {
    flex: 5,
    marginTop: 20,
  },
  elementLeftItem: {
    flex: 1,

  },
  elementRightItem: {
    flex: 1,
  },
  section_2_1_Container: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.colorTransparent,
    marginBottom: 10,
    marginTop: 10
  },
  section_3_Container: {
    flex: 3,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1,
    marginTop: 48,
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },
  textStyleForTotalPayment:{
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    fontWeight: 'bold'
  },
  textStyleForTotalPaymentRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'

  },
  ConfirmAndCancelTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },

  textStyleRight: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },
  titleStyle: {
    fontSize: 18,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  confirmButtonStyle: {
    backgroundColor: '#FFC400',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginRight: 20,
    paddingLeft: 15,
    paddingRight: 15,
    padding: 10

  },
  cancelButtonStyle: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginLeft: 5,
    marginRight: 10,
    padding: 10

  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM MODAL => LTE ACTIVATION\n', state.lteActivation);
  const language = state.lang.current_lang;
  const connection_Number = state.lteActivation.selected_connection_number;
  const selected_connection_type = state.lteActivation.selected_connection_type;
  const voice_Package = state.lteActivation.availableLTEVoicePackagesList;
  const bill_cycle = state.lteActivation.lte_billing_cycle_number;
  const mobile_contact_no = state.lteActivation.lte_set_input_MobileNumber;
  const Landline_contact_no = state.lteActivation.lte_set_input_landLineNumber;
  const email = state.lteActivation.email_input_value;
  const customerType = state.lteActivation.customerType;
  const paymentInfo = state.mobile.paymentInfo;
  const idNumber_input_value = state.lteActivation.idNumber_input_value;
  const data_pakages_value = state.lteActivation.availableLTEDataPackagesList;
  const data_package_rental =  data_pakages_value.dataRental;
  const offer_value = state.lteActivation.availableLTEOffersList;
  const voice_PackageValue = voice_PackageValue !== null
    && voice_PackageValue !== undefined ? voice_Package.packageList.package_rental : 0;
  const totalPaymentProps = state.lteActivation.lteSetTotalPayment;

  const needToShowOutstanding = totalPaymentProps.outstandingFeeValue !== undefined
    && totalPaymentProps.outstandingFeeValue !== null
    && !isNaN(totalPaymentProps.outstandingFeeValue)
    && totalPaymentProps.outstandingFeeValue !== ""
    && totalPaymentProps.outstandingFeeValue !== "0.00";

  const warrantyType = state.lteActivation.selectedLTEOfferDetailsObject;


  return {
    language,
    selected_connection_type,
    voice_Package,
    bill_cycle,
    data_pakages_value,
    connection_Number,
    mobile_contact_no,
    Landline_contact_no,
    email,
    offer_value,
    customerType,
    paymentInfo,
    idNumber_input_value,
    voice_PackageValue,
    totalPaymentProps,
    needToShowOutstanding,
    warrantyType,
    data_package_rental
  };
};

export default connect(mapStateToProps, actions)(HbbActivation);