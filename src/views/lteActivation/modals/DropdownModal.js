/*
 * File: DropdownModal.js
 * Project: Dialog Sales App
 * File Created: Monday, 23rd July 2018 3:19:37 pm
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Thursday, 26th July 2018 1:56:56 pm
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 * 
 * 
 * HOW TO USE THIS MODAL:
 * finalOfferDetails variable could be an Array like ["Some Text", "Some Text", "Some Text", "Some Text" ] or 
 * an Object like { error: "Error Text" } if the data retrieved for details section returned an error.
 * 
 * MANDATORY PROPS:
 * {
      modalTitle = '',            // Title of the modal
      dropDownData = [],          // Data array for the dropdown in the form of ["Some Text", "Some Text", "Some Text"]
      defaultIndex = -1,          // Pass this value to initially highlight a menu item. -1 resets the highlighted item
      onSelect=this.onSelect,     // Callback to be fired when a menu item is pressed
      selectedValue = '',         // Text value of the selected item ("Some Text")
      modaltype                   // Type of the data that will be displayed. This will be used to determine which 
                                     prop to be used. For reusability purposes
    }
 * 
 */

import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import strings from '../../../Language/LteActivation';

class DropdownModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      locals: {
        Continue: strings.btnContinue
      }
    };
  }

  onBnPressContinue = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  onSelect = (idx, value) => {
    console.log('onSelect', idx, value);
  }

  renderListItem = ({ item }) => (
    <ElementItem data={item} />
  )

  render() {
    const {
      modalTitle = '',
      dropDownData = [],
      defaultIndex = -1,
      onSelect=this.onSelect,
      selectedValue = '',
      modalType
    } = this.props;
    
    let finalOfferDetails;
    let selectedVal = selectedValue;
    let error = undefined;
    switch (modalType) {
      case 'offers':
        finalOfferDetails = this.props.selectedLTEOfferDetails.details;
        selectedVal = this.props.availableLTEOffersList.selectedValue;
        break;
      case 'datapackages':
        finalOfferDetails = this.props.selectedLTEDataPackageDetails;
        selectedVal = this.props.availableLTEDataPackagesList.selectedValue;
        break;
      case 'voicepackages':
        console.log("In dropdownmodal: ", this.props.availableLTEVoicePackagesList);
        finalOfferDetails = this.props.selectedLTEVoicePackageDetails;
        selectedVal = this.props.availableLTEVoicePackagesList.selectedValue;
        break;
      default:
        finalOfferDetails = this.props.selectedLTEOfferDetails;
        selectedVal = this.props.selectedLTEOfferDetails.selectedValue;
    }

    if (finalOfferDetails !== undefined){
      console.log('finalOfferDetails',finalOfferDetails);
      console.log('modalType',modalType);

      error = finalOfferDetails.error !== undefined ? finalOfferDetails.error : undefined;
    }
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            {/* Modal Title */}
            <View style={styles.titleViewStyle}>
              <Text style={styles.titleTextStyle}>{modalTitle}</Text>
            </View>
            {/* Dropdown selection */}
            <View style={styles.modalDropdownView}>
              <ModalDropdown
                options={dropDownData}
                onSelect={onSelect}
                defaultIndex={parseInt(defaultIndex)}
                style={styles.modalDropdownStyles}
                dropdownStyle={dropDownData.length<5 ? [styles.dropdownStyle, { height: 'auto' }] : styles.dropdownStyle}
                dropdownTextStyle={styles.dropdownTextStyle}
                dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
                <View style={styles.dropdownElementContainer}>
                  <View style={styles.dropdownDataElemint}>
                    <Text ellipsizeMode ='tail' numberOfLines={3} style={styles.dropdownDataElemintTxt}>{selectedVal}</Text>
                  </View>
                  <View style={styles.dropdownArrow}>
                    <Ionicons name='md-arrow-dropdown' size={20}/>
                  </View>
                </View>
              </ModalDropdown>
            </View>
            {/* Details View */}
            <View style={ error !== undefined? styles.errorViewContainer :  styles.detailViewContainer}>
              { error ? 
                //Error View
                <View style={ styles.errorView }>
                  <Text style={ styles.errorText }>{ error}</Text>
                </View> :
                // If error is undefined, show details list
                <FlatList
                  data={finalOfferDetails}
                  renderItem={this.renderListItem}
                  // extraData={this.state.package[0]}
                  keyExtractor={(item, index) => index.toString()}
                />
              }
            </View>
            {/* Button */}
            <View style={styles.bottomCantainerBtn}>
              <View style={styles.dummyView}/>
              <TouchableOpacity
                onPress={() => this.onBnPressContinue()}
                style={styles.buttonContainer}>
                <Text style={styles.buttonTxt}>
                  {this.state.locals.Continue}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const ElementItem = ({ data }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elimentViewStyle}>
      <Icon name="circle" style={styles.bulletIcon} />
    </View>
    <View style={styles.elimentTextStyle}>
      <Text style={styles.textStyle}>{data}</Text>
    </View>
  </View>
);

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const selectedLTEOfferDetails = state.lteActivation.selectedLTEOfferDetails;
  const selectedLTEDataPackageDetails = state.lteActivation.selectedLTEDataPackageDetails;
  const selectedLTEVoicePackageDetails = state.lteActivation.selectedLTEVoicePackageDetails;
  const availableLTEOffersList = state.lteActivation.availableLTEOffersList;
  const availableLTEDataPackagesList = state.lteActivation.availableLTEDataPackagesList;
  const availableLTEVoicePackagesList = state.lteActivation.availableLTEVoicePackagesList;
  return { Language, 
    selectedLTEOfferDetails,
    selectedLTEDataPackageDetails,
    selectedLTEVoicePackageDetails,
    availableLTEOffersList,
    availableLTEDataPackagesList,
    availableLTEVoicePackagesList
  };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.3,
    flexDirection: 'row'
  },
  modalContainer: {
    flex: 1.4,
    flexDirection: 'column'
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginLeft: 10,
    marginRight: 10
  },

  titleViewStyle: {
    flex: 2,
    flexDirection: 'row',
  },

  bottomCantainerBtn: {
    flex: 0.5,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 10
  },

  //button styles
  dummyView: {
    flex: 1.5,
    padding: 5
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 2,
    marginLeft: 5,
    padding: 5,
    marginRight: 5,
    marginBottom: 5,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.dropDownModal.defaultBtnFontSize,
    fontWeight: '400'
  },

  titleTextStyle: {
    fontSize: Styles.dropDownModal.titleFontSize,
    alignSelf: 'flex-start',
    flex:1,
    fontWeight: '500',
    margin: 5,
    color: Colors.colorBlack
  },

  modalDropdownView: {
    marginTop: 5,
    flexDirection: 'row',
  },

  modalDropdownStyles: {
    width: Dimensions
      .get('window')
      .width - 30
  },

  dropdownStyle: {
    width: Dimensions
      .get('window')
      .width - 60,
    marginLeft: 15,
    marginRight: 15,
    height: 160,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },

  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    marginLeft: 15,
    marginRight: 15,
    padding: 5,
    borderWidth: 1,
    borderColor: Colors.borderColor
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
   // marginLeft: 4,
    marginLeft: 15
   // backgroundColor: 'red'
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 7
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%'
  },
  elimentViewStyle: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bulletIcon: {
    color: Colors.colorBlack,
  },
  textStyle: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
  },
  elimentTextStyle: {
    flex:10,
    justifyContent: 'flex-start'
  },
  detailViewContainer:  { 
    flex:10,
    paddingTop: 10,
    flexDirection: 'row',
    marginTop: 5
  },
  errorViewContainer:  { 
    flex:10,
    paddingTop: 10,
    flexDirection: 'column',
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center'
  },
  errorView:{ 
    flex:1, 
    justifyContent:'center', 
    alignItems:'center' 
  },
  errorText:{ 
    fontSize: 15, 
    fontWeight: '500',
    marginBottom: 15 
  }
});

export default connect(mapStateToProps, actions)(DropdownModal);
