import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Analytics from '../../../utills/Analytics';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/MobileActivaton';

const Utill = new Utills();

/**
 * OTP entering modal for both prepaid and postpaid
 */
class OtpEnterModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      openModel: false,
      enteredOtp: '',
      otpResendCount: 0,
      otpVerifyCount: 0,
      locals: {
        five_pin_number_sent_to: strings.five_pin_number_sent_to,
        enter_pin: strings.enter_pin,
        receive_five_digit_pin: strings.receive_five_digit_pin,
        resend_pin: strings.resend_pin,
        validate_pin: strings.validate_pin,
        skip_validation: strings.skip_validation,
        otp_validation_success: strings.otp_validation_success,
        otp_validation_failed: strings.otp_validation_failed,
        otp_resent: strings.otp_resent
      }
    };
  }

  dismissModal = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  onChangeText = (number) => {
    this.setState({ enteredOtp: number });
  }

  otpCancel = () => {
    Analytics.logEvent('dsa_lte_activation_otp_cancel', { customerNumber: this.props.otpInfo.sender_msisdn });
    this.dismissModal();
  }

  otpVerify = () => {
    console.log('xxx otpVerify');
    this.setState({
      otpVerifyCount: this.state.otpVerifyCount + 1
    });

    if (this.state.otpVerifyCount <= 3) {
      this.props.verifyOtp(this.props.otpInfo.sender_msisdn, this.state.enteredOtp, this.props.otpInfo.reference_id); 
    } else {
      Utill.showAlertMsg(this.state.locals.otp_validation_failed);
      this.dismissModal();
      Analytics.logEvent('dsa_lte_activation_otp_failed', { customerNumber: this.props.otpInfo.sender_msisdn });
    } 

  }

  otpResend = () => {
    console.log('xxx otpResend');
    this.setState({
      otpResendCount: this.state.otpResendCount + 1
    });

    this.props.reSendOtpMessage(this.props.otpInfo.sender_msisdn, this.props.otpInfo.reference_id); 
  }
  
  render() {
    let resendRightView;
    if (this.state.otpResendCount !== 3) {
      resendRightView = (
        <View style={styles.resendRightView}>
          <TouchableOpacity 
            onPress={() =>this.otpResend()} 
            style={styles.resendRightViewBtn}>
            <Text style={styles.resendRightViewText}>{this.state.locals.resend_pin}
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      resendRightView = true;
    }
    return (
      <View style={styles.container}>
        <View style={styles.containerOverlay}>
          <View style={styles.topContainer}/>
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <View style={styles.textViewContainer}>
                {/* Top text view */}
                <Text style={styles.text_1}>
                  {this.props.otpInfo.message}
                </Text>
                <Text style={styles.text_2}>
                  {this.state.locals.enter_pin}
                </Text>
              </View>
              {/* OTP enter text input*/}
              <View style={styles.otpInputContainer}>
                <TextInput
                  style={styles.inputTextField}
                  underlineColorAndroid={Colors.colorTransparent}
                  onChangeText={(value) => this.onChangeText(value)}
                  keyboardType={'numeric'}
                  maxLength={5}
                  value={this.state.enteredOtp}/>
              </View>
              {/* OTP resent view*/}
              <View style={styles.resendViewContainer}>
                <View style={styles.resendLeftView}>
                  <Text style={styles.resendLeftViewText}>
                    {this.state.locals.receive_five_digit_pin}
                  </Text>
                </View>
                {resendRightView}
              </View>
              {/* Validate OTP button */}
              <View style={styles.validationOtpContainer}>
                <TouchableOpacity
                  style={styles.validationBtn}
                  onPress={() => this.otpVerify()}>
                  <Text style={styles.validationBtnText}>
                    {this.state.locals.validate_pin}</Text>
                </TouchableOpacity>
              </View>
              {/* Skip validation button */}
              <View style={styles.skipViewContainer}>
                <TouchableOpacity onPress={this.otpCancel} style={styles.skipBtn}>
                  <Text style={styles.skipBtnTxt}>
                    {this.state.locals.skip_validation}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.bottomContainer}/>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const language = state.lang.current_lang;

  return { language };
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  containerOverlay: {
    height: Dimensions
      .get('window')
      .height
  },

  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    flex: 1,
    width: Dimensions
      .get('window')
      .width * 0.95,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    flex: 0.7
  },

  innerContainer: {
    // flex: 1,
    height: 300,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginLeft: 10,
    marginRight: 10
  },

  textViewContainer: {
    //flex: 0.7,
    height: 90,
    marginLeft: 5,
    marginRight: 5,
  },

  otpInputContainer: {
    //flex: 0.5,
    height: 50,
    marginLeft: 5,
    marginRight: 5,
  },

  resendViewContainer: {
    //flex: 0.5,
    height: 50,
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'flex-start',
  },

  validationOtpContainer: {
    //flex: 0.6,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  skipViewContainer: {
    //flex: 0.2,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  skipBtn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  skipBtnTxt: {
    textDecorationLine: 'underline',
    color: Colors.colorBlue,
    textAlign: 'center'
  },
  validationBtn: {
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Colors.colorYellow,
    height: 45,
    borderRadius: 5
  },

  validationBtnText: {
    backgroundColor: Colors.colorBackground,
    color: Colors.colorBlack,
    margin: 5,
    fontWeight: '500',
   
  },

  resendLeftView: {
    flex: 2.2
  },

  resendRightView: {
    flex: 1
  },
  resendRightViewBtn: {
    flex: 1,
    //justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  resendLeftViewText: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 5,
    textAlign: 'left',
    fontWeight: '400'
  },

  resendRightViewText: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.yellow,
    margin: 5,
    marginLeft: 0,
    fontWeight: '500',
    
  },

  text_1: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    marginBottom: 5,
    marginLeft: 5,
    marginTop: 5,   
    fontWeight: '100'
  },

  text_2: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    marginLeft: 5,
    marginTop: 25,
    fontWeight: '100'
  },

  inputTextField: {
    flex: 1,
    height: 30,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    marginBottom:10
  }
});

export default connect(mapStateToProps, actions)(OtpEnterModal);
