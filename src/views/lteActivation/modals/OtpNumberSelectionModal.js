import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Picker,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/MobileActivaton';
import FuncUtils from '../../../utills/FuncUtils';

const Utill = new Utills();

/**
 * Otp Number Selection Modal for both prepaid and postpaid
 */

class OtpNumberSelectionModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      openModel: false,
      otpMobileNumber: '',
      pendingState: 0,
      locals: {
        customer_already_with_dialog: strings.customer_already_with_dialog,
        select_mobile_number: strings.select_mobile_number,
        send_validation: strings.send_validation,
        skip_validation: strings.skip_validation,
        msg_please_select_no: strings.msg_please_select_no,
        otp_select_a_number: strings.otp_select_a_number
      }
    };
  }

  onSkipValidation = () => {
    console.log('xxx onSkipValidation');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  onSendValidation = () => {
    console.log('xxx onSendValidation', this.props.customerData.notificationNumberList[0]);
    this.props.sendOtpMessage(this.state.otpMobileNumber === '' ? this.props.customerData.notificationNumberList[0] : this.state.otpMobileNumber);
    this.props.lteSetOtpNumber(FuncUtils.getValueSafe(this.state.lte_cx_otp_number) == '' ? this.props.customerData.notificationNumberList[0] : this.state.lte_cx_otp_number);
    this.props.dtvSetOtpNumber(FuncUtils.getValueSafe(this.state.dtv_cx_otp_number) == '' ? this.props.customerData.notificationNumberList[0] : this.state.dtv_cx_otp_number);
  }

  valueChanged = (number) => {
    console.log(number);
    this.props.lteSetOtpNumber(number);
    this.props.dtvSetOtpNumber(number);
    this.setState({ otpMobileNumber: number });
  }

  render() {
    const setPicker = () => {
      if (this.props.customerData.notificationNumberList !== undefined || this.props.customerData.notificationNumberList !== null) {
        const numberListArray = this.props.customerData.notificationNumberList;
        return (
          <Picker
            style={numberListArray.length > 1 ? styles.picker :styles.pickerone}
            selectedValue={this.state.otpMobileNumber}
            mode={'dropdown'}
            onValueChange={this.valueChanged}>
            {/* <Picker.Item
              label={this.state.locals.otp_select_a_number}
              value={numberListArray}/>  */}
            {
              numberListArray.map(number => (<Picker.Item key={number} label={Utill.numberMask(number)} value={number} />))
            }
          </Picker>
        );
      } else {
        return (<View />);
      }
    };

    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.textViewContainer}>
              {/* Top text view */}
              <Text style={styles.mainText}>
                {this.state.locals.customer_already_with_dialog}
              </Text>
              <Text style={styles.mainText}>
                {this.state.locals.select_mobile_number}
              </Text>
            </View>
            {/* Number selection Dropdown*/}
            <View style={styles.pickerViewContainer}>
              {setPicker()}
            </View>
            {/* Send validation button */}
            <View style={styles.validationButtonContainer}>
              <TouchableOpacity
                style={styles.validationBtn}
                onPress={() => this.onSendValidation()}>
                <Text style={styles.validationBtnText}>
                  {this.state.locals.send_validation}</Text>
              </TouchableOpacity>
            </View>
            {/* Skip validation button */}
            <View style={styles.skipViewContainer}>
              <TouchableOpacity onPress={this.onSkipValidation} style={styles.skipBtn}>
                <Text style={styles.skipBtnTxt}>
                  {this.state.locals.skip_validation}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const language = state.lang.current_lang;
  const local_foreign = state.mobile.local_foreign;
  const otpNumber = state.lteActivation.otpNumber;
  const lte_cx_otp_number = state.lteActivation.cx_otp_number;
  const dtv_cx_otp_number = state.dtvActivation.cx_otp_number;

    

  return {
    state,
    language,
    otpNumber,
    local_foreign,
    lte_cx_otp_number,
    dtv_cx_otp_number
  };
};

const styles = StyleSheet.create({
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.5
  },
  modalContainer: {
    flex: 1,
    //width: 450,
    width: Dimensions
      .get('window')
      .width * 0.95,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  bottomContainer: {
    flex: 0.6
  },

  innerContainer: {
    //flex: 1,
    height: 280,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginLeft: 10,
    marginRight: 10
  },

  textViewContainer: {
    //flex: 1,
    // height: 100,
    marginLeft: 5,
    marginRight: 5
  },
  pickerViewContainer: {
    //flex: 0.5,
    height: 60,
    marginTop: -5
  },
  validationButtonContainer: {
    //flex: 0.7,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  skipViewContainer: {
    //flex: 0.2,
    marginTop:5,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainText: {
    textAlign: 'left',
    fontSize: Styles.otpModalFontSize,
    marginTop: 10,
    fontWeight: '100',
    color: Colors.colorBlack,
  },
  picker: {
    flex: 1,
    margin: 0,
    marginTop: 5,
    padding: 0,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1
  },

  pickerone:{
    flex: 1,
    margin: 0,
    marginTop: 5,
    padding: 0,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    backgroundColor:Colors.white
  },

  skipBtn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },  

  skipBtnTxt: {
    textDecorationLine: 'underline',
    color: Colors.colorBlue,
    textAlign: 'center'
  },

  validationBtn: {
    //flex: 1,
    margin: 5,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Colors.colorYellow,
    height: 45,
    borderRadius: 5
  },

  validationBtnText: {
    color: Colors.colorBlack,
    backgroundColor: Colors.colorBackground,
    margin: 5,
    fontWeight: '500'
  }

});

export default connect(mapStateToProps, actions)(OtpNumberSelectionModal);
