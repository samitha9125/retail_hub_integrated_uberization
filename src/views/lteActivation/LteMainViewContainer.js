import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Keyboard } from 'react-native';
import Orientation from 'react-native-orientation';
import { Navigation } from 'react-native-navigation';
import RadioButton from 'radio-button-react-native';
import CheckBox from 'react-native-check-box';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import { Analytics, Screen, FuncUtils } from '../../utills';
import { globalConfig, Constants, Images, Colors, Styles } from '../../config';
import Utills from '../../utills/Utills';
import MainView from './components/MainView';
import SectionContainer from './components/SectionContainer';
import MaterialInput from './components/MaterialInput';
import CameraInput from './components/CameraInput';
import DropDownInput from './components/DropDownInput';
import InputSection from './components/InputSection';
import strings from '../../Language/LteActivation';
import TotalPayments from './TotalPayments';
import EzCashPayment from './EzCashPayment';
import EzCashBalance from './EzCashBalance';
import PaymentCalculations from './PaymentCalculations';
import _ from 'lodash';

const Utill = new Utills();

const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

class LteMainViewContainer extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        sectionNICValidation: strings.sectionNICValidation,
        sectionPPValidation: strings.sectionPPValidation,
        sectionCustomerInformation: strings.sectionCustomerInformation,
        sectionProductInformation: strings.sectionProductInformation,
        id_nic_mc: strings.id_nic_mc,
        id_passport_mc: strings.id_passport_mc,
        labelNIc: strings.labelNIc,
        labelPP: strings.labelPP,
        labelVoucherCode: strings.labelVoucherCode,
        labelScanSerialPack: strings.labelScanSerialPack,
        labelScanSimSerial: strings.labelScanSimSerial,
        labelBundleSerial: strings.labelBundleSerial,
        labelIndividualSerial: strings.labelIndividualSerial,
        labelAnalogPhoneSerial: strings.labelAnalogPhoneSerial,
        serialType: strings.serialType,
        bundleSerial: strings.bundleSerial,
        individualSerial: strings.individualSerial,
        offer: strings.offer,
        voicePackages: strings.voicePackages,
        default_offer: strings.default_offer,
        radioBtnField_NIC: strings.radioBtnField_NIC,
        radioBtnField_Passport: strings.radioBtnField_Passport,
        radioBtnField_Driving_Licence: strings.radioBtnField_Driving_Licence,
        dataPackages: strings.dataPackages,
        area: strings.area,
        dataSachets: strings.dataSachets,
        depositAmount: strings.depositAmount,
        billingCycle: strings.billingCycle,
        captureNIC: strings.captureNIC,
        addressDifferentFromNIC: strings.addressDifferentFromNIC,
        addressDifferentFromNICPost: strings.addressDifferentFromNICPost,
        customerFaceNotClearNIC: strings.customerFaceNotClearNIC,
        customerFaceNotClearDL: strings.customerFaceNotClearDL,
        addressDifferentFromDL: strings.addressDifferentFromDL,
        addressDifferentFromDLPost: strings.addressDifferentFromDLPost,
        installationAddressDifferentFromBillingProof: strings.installationAddressDifferentFromBillingProof,
        installationAddressDifferentFromBillingProofPost: strings.installationAddressDifferentFromBillingProofPost,
        addressDifferentFromPrevious: strings.addressDifferentFromPrevious,
        addressDifferentFromOtpVerifiedConnectionPre: strings.addressDifferentFromOtpVerifiedConnectionPre,
        addressDifferentFromOtpVerifiedConnectionPost: strings.addressDifferentFromOtpVerifiedConnectionPost,
        billingAddressDifferentFromPrevious: strings.billingAddressDifferentFromPrevious,
        capturePOB: strings.capturePOB,
        capturePOBPost: strings.capturePOBPost,
        captureAdditionalPOB: strings.captureAdditionalPOB,
        captureCustomerPhoto: strings.captureCustomerPhoto,
        capturePassport: strings.capturePassport,
        captureAddress: strings.captureAddress,
        captureDrivingLicense: strings.captureDrivingLicense,
        labelCustomerEmail: strings.labelCustomerEmail,
        labelCustomerMobile: strings.labelCustomerMobile,
        labelCustomerLandline: strings.labelCustomerLandline,
        checkBoxTextAddVoice: strings.checkBoxTextAddVoice,
        checkBoxTextAnalogPhone: strings.checkBoxTextAnalogPhone,
        labelArea: strings.labelArea,
        labelConnectionNo: strings.labelConnectionNo,
        labelDataSachetReload: strings.labelDataSachetReload,
        rs_label: strings.rs_label,
        rs_label_with_dot: strings.rs_label_with_dot,
        ez_cash_account_balance: strings.ez_cash_account_balance,
        system_error_please_try_again: strings.system_error_please_try_again,
        do_uou_want_to_cancel_serial_scan_and_go_back: strings.do_uou_want_to_cancel_serial_scan_and_go_back,
        do_uou_want_to_cancel_image_capture_and_go_back: strings.do_uou_want_to_cancel_image_capture_and_go_back,
        enterSerial: strings.enterSerial,
        customerSignature: strings.customerSignature,
        ezCashPin: strings.ezCashPin,
        ez_cash_pin_title: strings.ez_cash_pin_title,
        txtActivate: strings.txtActivate,
        btnOk: strings.btnOk,
        btnCancel: strings.btnCancel,
        btnYes: strings.btnYes,
        btnNo: strings.btnNo,
        btnRetry: strings.btnRetry,
        please_enter_valid_nic: strings.please_enter_valid_nic,
        please_enter_valid_pp: strings.please_enter_valid_pp,
        offerInfo: strings.offerInfo,
        packageInfo: strings.packageInfo,
        title_scan_serial: strings.title_scan_serial,
        please_enter_analog_phone_serial: strings.please_enter_analog_phone_serial,
        please_enter_valid_email: strings.please_enter_valid_email,
        please_enter_valid_mobile: strings.please_enter_valid_mobile,
        please_enter_valid_laneline: strings.please_enter_valid_laneline,
        please_enter_valid_voucher_code: strings.please_enter_valid_voucher_code,
        please_select_mobile_number: strings.please_select_mobile_number,
        please_capture_nic: strings.please_capture_nic,
        please_capture_passport: strings.please_capture_passport,
        please_capture_driving_licence: strings.please_capture_driving_licence,
        please_capture_pob: strings.please_capture_pob,
        please_enter_customer_face: strings.please_enter_customer_face,
        please_enter_poi: strings.please_enter_poi,
        please_enter_signature: strings.please_enter_signature,
        please_enter_ez_cash_pin: strings.please_enter_ez_cash_pin,
        offerPricePrefix: strings.offerPricePrefix,
        dataRentalPrefix: strings.dataRentalPrefix,
        tax: strings.tax,
      },

      emailError: '',
      mobileError: '',
      landlineError: '',
      section_1_visible: true,
      section_2_visible: false,
      section_3_visible: false,
      previousVoucherCode:'',
      searchResults: [],
      selectedArea: '',
      currentTime: ''
    };
    // this.debouncedAreaSearch = _.debounce((requestParams) => this.loadArea(requestParams), 500, DEBOUNCE_OPTIONS);
    this.debouncedCheckboxSelect = _.debounce((checkBoxType) => this.checkAddVoiceField(checkBoxType), 5, DEBOUNCE_OPTIONS);
    this.debouncedFinalActivation = _.debounce(() => this.onPressActivate(), 150, DEBOUNCE_OPTIONS);
  }

  //-----------------------------LIFE CYCLE EVENTS------------------------//

  componentWillUnmount() {
    console.log('componentWillUnmount');
    Orientation.lockToPortrait();
    this.fullResetSelectedConnectionNumber();
    if (this.props.cancelVoucherCodeReservation && this.props.lte_activation_status == false) {
      this.props.cancelVoucherCodeReservation();
    }     
  }

  componentDidMount() {
    console.log('componentDidMount');
    Orientation.lockToPortrait();
    this.props.configSetLteActivationStatus(false);
    this.props.resetLTEData(true);

  }

  /*
  static getDerivedStateFromProps(nextProps, prevState) {   
    console.log('###### >>>>>> getDerivedStateFromProps ##### nextProps :', nextProps);
    console.log('###### >>>>>> getDerivedStateFromProps ##### prevState :', prevState);

    return null;
  } 
  */

  //-----------------------------LIFE CYCLE EVENTS END------------------------//

  goToHomePage = () => {
    console.log('xxx goToHomePage');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this;
    if (me.props.cancelVoucherCodeReservation && this.props.lte_activation_status == false)
      me.props.cancelVoucherCodeReservation();
    setTimeout(() => {
      me.props.resetLTEData(true);
    }, 50);
  }

  onPressOtp = () => {
    console.log('xxx onPressOtp');
    let screen = {
      title: 'OtpNumberSelectionModal',
      id: 'DialogRetailerApp.modals.OtpNumberSelectionModal',
    };
    let passProps = {
      messageType: 'success'
    };
    Utill.showModalView(screen, passProps);
  }

  onConfirmModal = () => {
    console.log('xxx onConfirmModal');
    let screen = {
      title: 'Confirm Modal Lte',
      id: 'DialogRetailerApp.modals.ConfirmModalLte',
    };
    let passProps = {
      messageType: 'success'
    };
    Utill.showModalView(screen, passProps);
  }

  onPressInfo = (availableDataList, modalType) => {
    if (availableDataList == undefined) {
      return;
    }
    console.log('xxx onPressInfo');
    let screen = {
      title: 'Confirm Modal Lte',
      id: 'DialogRetailerApp.modals.DropdownModalLte',
    };

    let passProps = {};

    switch (modalType) {
      case 'offers':
        passProps = {
          modalTitle: this.state.locals.offerInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onOfferInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType
        };
        break;
      case 'datapackages':
        passProps = {
          modalTitle: this.state.locals.packageInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onDataPkgInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType
        };
        break;
      case 'voicepackages':
        passProps = {
          modalTitle: this.state.locals.packageInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onPressVoicePkgInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType
        };
        console.log(JSON.stringify(passProps));
        break;
      default:
        Utill.showAlert(this.state.locals.system_error_please_try_again);
    }
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Serial scanning functions
   * @param {String} serialType
   * @memberof LteMainViewContainer
   */
  onPressSerialScan = (serialType) => {
    
    console.log('xxx onPressSerialScan', serialType);
    let me = this;
    const validateUrl = {
      action: 'validateMaterialSerial',
      controller: 'lte',
      module: 'ccapp'
    };

    let screen = {
      title: 'SCAN SERIALS',
      id: 'DialogRetailerApp.views.BarcodeScannerModule',
    };

    let serialMaxLength = Constants.DEFAULT_SERIAL_MAX_LENGTH;
    let screenTitle = me.state.locals.title_scan_serial;
    let displayMessage = '';
    let materialType = 'INDIVIDUAL';
    let inputValidateType = 'NUMERIC';
    let keyboardType = 'numeric';
    let setSerialValueRedux;
    let productFamily;
    let autoValidation = false;
    let sliceNumber = false;
    let sliceLength = 0;
    let cancelVoucherCodeReservation = undefined;


    switch (serialType) {
      case 'bundleSerial':
        console.log('xxx onPressSerialScan bundleSerial');
        materialType = 'BUNDLE';
        serialMaxLength = Constants.BUNDLE_SERIAL_MAX_LENGTH;
        displayMessage = strings.scan_bundle_serial;
        setSerialValueRedux = me.props.lteSetBundleSerial;
        productFamily = Constants.PRODUCT_FAMILY_LTE_CPE;
        cancelVoucherCodeReservation = this.props.cancelVoucherCodeReservation;
        break;
      case 'serialPack':
        console.log('xxx onPressSerialScan serialPack');
        displayMessage = strings.scan_router_IMEI_serial;
        serialMaxLength = Constants.PACK_SERIAL_MAX_LENGTH;
        setSerialValueRedux = me.props.lteSetSerialPack;
        productFamily = Constants.PRODUCT_FAMILY_LTE_CPE;
        cancelVoucherCodeReservation = this.props.cancelVoucherCodeReservation;
        break;
      case 'simSerial':
        console.log('xxx onPressSerialScan simSerial');
        displayMessage = strings.scan_SIM_serial;
        serialMaxLength = Constants.SIM_SERIAL_MAX_LENGTH;
        autoValidation = true;
        sliceNumber = true;
        sliceLength = Constants.SIM_SERIAL_MAX_LENGTH;
        setSerialValueRedux = me.props.lteSetSimSerial;
        productFamily = Constants.PRODUCT_FAMILY_LTE_SIM;
        cancelVoucherCodeReservation = this.props.cancelVoucherCodeReservation;
        break;
      case 'analogPhoneSerial':
        console.log('xxx onPressSerialScan analogPhoneSerial');
        displayMessage = strings.scan_analog_phone_serial;
        serialMaxLength = Constants.ANALOG_PHONE_SERIAL_MAX_LENGTH;
        setSerialValueRedux = me.props.lteSetAnalogPhoneSerial;
        productFamily = Constants.PRODUCT_FAMILY_LTE_ANALOG_PHONE;
        keyboardType = 'default';
        inputValidateType = 'ALPHA_NUMERIC';
        break;
      default:
        console.log('xxx onPressSerialScan default');
        break;
    }

    let passProps = {
      screenTitle: screenTitle,
      backMessage: me.state.locals.do_uou_want_to_cancel_serial_scan_and_go_back,
      displayMessage: displayMessage,
      textInputLabel: me.state.locals.enterSerial,
      serialMaxLength: serialMaxLength,
      validateUrl: validateUrl,
      autoValidation: autoValidation,
      sliceNumber: sliceNumber,
      sliceLength: sliceLength,
      materialType: materialType,
      serialType: serialType,
      inputValidateType: inputValidateType,
      keyboardType: keyboardType,
      productFamily: productFamily,
      cancelVoucherCodeReservation: cancelVoucherCodeReservation,
      lob: Constants.LOB_LTE,
      setSerialValueRedux: setSerialValueRedux,
      resetSerialValidationStatus: () => this.resetSerialValidationStatus(),
      setSerialInputValue: (serialValue) => this.setSerialInputValue(serialValue),
      resetSerialFieldValues: (serialType) => this.resetSerialFieldValues(serialType),
      validateSerialNumberAPI: (requestParam, url, barcodeScreen) => this.props.lteValidateSerialNumber(requestParam, url, barcodeScreen),
      hasNextScreen: false,
      additionalProps: {
        bundleSerialData: me.props.bundleSerialData,
        serialPackData: me.props.serialPackData,
        simSerialData: me.props.simSerialData,
        conn_type: me.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        id_number: me.props.idNumber_input_value,
        id_type: me.props.customerInfoSectionIdType,
        customer_status: me.props.customerExistence == 'N' ? 'NEW' : 'EXISTING',
        customerType: me.props.customerType,
        selectedArea: _.isObject(this.props.lteSelectedArea) ? this.props.lteSelectedArea.city_name : this.state.selectedArea
      }
    };

    Screen.pushView(screen, me, passProps);
  }

  /**
   * @description Reset serial validation status
   * @memberof LteMainViewContainer
   */
  resetSerialValidationStatus = () => {
    console.log('xxx resetSerialValidationStatus :: LTE');
    let me = this;
    me.props.lteResetSerialValidationStatus();
  };

  /**
   * @description Set serial input serial value
   * @memberof LteMainViewContainer
   */
  setSerialInputValue = (value) => {
    console.log('xxx setSerialInputValue :: LTE', value);
    let me = this;
    me.props.lteSetSerialTextValue(value);
  };

  /**
   * @description Reset serial input fields
   * @param {String} serialType
   * @memberof LteMainViewContainer
   */
  resetSerialFieldValues = (serialType) => {
    console.log('xxx resetSerialFieldValues :: LTE', serialType);
    let me = this;
    if (serialType === 'analogPhoneSerial') {
      console.log('xxx resetSerialFieldValues :: analogPhoneSerial');
      me.props.lteResetAnalogPhoneSerialData();
      me.props.commonResetKycImages();
    } else {
      console.log('xxx resetSerialFieldValues :: Other type');
      if (me.props.cancelVoucherCodeReservation)
        me.props.cancelVoucherCodeReservation();
      me.props.lteResetIndividualSerialFieldValues(serialType);
      me.props.commonResetKycImages();
    }
  };


  /**
   * @description dismiss modal
   * @memberof LteMainViewContainer
   */
  modalDismiss = () => {
    console.log('xxx modalDismiss');
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  /**
   * @description Show inline validation error message in idNumber field
   * @param {Object} response
   * @param {String} description
   * @memberof LteMainViewContainer
   */
  showInlineErrorMessageIdNumber = (response, description) => {
    console.log('xxx showInlineErrorMessageIdNumber :: response', response);
    //let description = 'You must be older than 18 years to get connection.';
    let me = this;
    me.props.lteSetIdNumberError(description);
  };

  /**
   * @description Show Maximum Number Exceeded Modal or Blacklisted Customer Modal
   * @param {Object} response
   * @param {String} description
   * @memberof LteMainViewContainer
   */
  showCustomModalTypeMessage = (response, description) => {
    console.log('xxx showCustomModalTypeMessage :: response', response);
    //let description_1 = 'Entered NIC has exceeded the maximum number (5) of connections. \n\nInform the customer to visit the nearest Dialog service centre to disconnect any connections not in use.';
    //let description_2 = 'Entered NIC is not eligible to get a new connection. \n\nInform the customer to visit the nearest Dialog service centre to disconnect any connections not in use.';
    let passProps = {
      primaryText: this.state.locals.btnOk,
      disabled: false,
      hideTopImageView: true,
      showTitle: true,
      title: this.getCustomerIdTypeText() + ' : ' + this.props.idNumber_input_value,
      modalHeightRatio: 0.5,
      icon: Images.icons.SuccessAlert,
      description: Utill.getStringifyText(description),
      additionalProps: response
    };

    this.LteGeneralModal(passProps);

  };

  /**
   * @description Show Maximum Number Exceeded Modal or Blacklisted Customer Modal
   * @param {Object} response
   * @param {String} description
   * @memberof LteMainViewContainer
   */
  showModalTypeMessage = (response, description) => {
    console.log('xxx showModalTypeMessage :: response', response);
    let okFn = null;
    let okText = this.state.locals.btnOk;
    let title = this.getCustomerIdTypeText() + ' : ' + this.props.idNumber_input_value;
    let message = description;

    Screen.showDialogBox(message, title, okFn, okText);

  };

  /**
   * @description Show General error modal
   * @param {Object} error
   * @memberof LteMainViewContainer
   */
  showGeneralErrorModal = (error) => {
    console.log('xxx showGeneralErrorModal :: error', error);
    let description = error === undefined ? this.state.locals.system_error_please_try_again : error;
    let passProps = {
      primaryText: this.state.locals.btnOk,
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      //descriptionTitle: 'Voucher Code : 1234',
      //modalHeightRatio: 0.45, 
      modalHeightRatio: 0.35,
      icon: Images.icons.FailureAlert,
      description: Utill.getStringifyText(description),
    };

    this.LteGeneralModal(passProps);
  };

  /**
   * @description LTE General modal
   * @param {Object} passProps
   * @memberof LteMainViewContainer
   */
  LteGeneralModal = (passProps) => {
    console.log('xxx showGeneralModal :: passProps', passProps);
    let screen = {
      title: 'LteGeneralModal',
      id: 'DialogRetailerApp.modals.LteGeneralModal',
    };

    if (!passProps.primaryPress) {
      passProps.primaryPress = () => this.modalDismiss();
    }

    Utill.showModalView(screen, passProps);
  }

  setCusInfoMode = (infoType) => {
    console.log('xxx setCusInfoMode :: infoType :', infoType);
    this.props.lteSetCustomerInfoSectionIdType(infoType);
    this.props.commonResetKycImages();
    this.props.lteSetKycCheckBoxField('RESET');
  }

  /**
  *This function will be invoked when an offer is selected from the dropdown or the Offer selection popup and set
  *the selected offer details into the redux state. 
  *Also this will call the backend and retrieve the selected offer details and update the redux state.
  * @param {Number} idx - id of the selected dropdown item
  * @param {String} value - text value of the selected drop down item
  * @returns 
  * @memberof LteMainViewContainer
  */
  onOfferInfoDropdownSelect = (idx, value) => {
    console.log('onOfferInfoDropdownSelect', idx, value);
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.resetVoucerValue();

    let offerList = this.props.availableLTEOffersList.offerList;
    let offerAdditionalInfo = this.props.availableLTEOffersList.additionalInfo;
    let multiplayOfferData = this.props.multiplayOfferData;
    let selectedOfferCode = offerList[idx].offer_Code;
    let offer_type = offerList[idx].offer_type;

    this.props.setVoucherCodeValidityOfferCode(selectedOfferCode);

    console.log('xxx onOfferInfoDropdownSelect::offer_type', offer_type);
    console.log('xxx onOfferInfoDropdownSelect::selectedOfferCode', selectedOfferCode);
    console.log('xxx onOfferInfoDropdownSelect::multiplayOfferData', multiplayOfferData);
    console.log('xxx onOfferInfoDropdownSelect::multiplayOfferData - reservation_no', multiplayOfferData.reservation_no);

    let selectedData = {
      dropDownData: _.toArray(_.mapValues(offerList, 'offer_Name')),
      selectedValue: _.filter(offerList, { 'offer_Code': selectedOfferCode })[0].offer_Name,
      offer_Code: selectedOfferCode,
      offerPrice: _.filter(offerList, { 'offer_Code': selectedOfferCode })[0].offer_Price,
      defaultIndex: _.findIndex(offerList, ['offer_Code', selectedOfferCode]),
      offerList: offerList,
      additionalInfo: offerAdditionalInfo
    };

    let requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      cx_identity_no: this.props.idNumber_input_value,
      offer_type: offer_type,
      offer_code: selectedOfferCode,
      sap_material_items: this.props.allSerialData,
      audience_group_name: multiplayOfferData.audience_group_name,
      om_campaign_record_no: multiplayOfferData.om_campaign_record_no,
      reservation_no: multiplayOfferData.reservation_no,
    };
    console.log('onOfferInfoDropdownSelect :: requestParams', requestParams);
    console.log('onOfferInfoDropdownSelect :: selectedData', selectedData);

    if (this.props.availableLTEOffersList.offer_Code !== selectedOfferCode) {
      console.log('onOfferInfoDropdownSelect :: API call - getLTEOfferDetails');

      let additionalProps = {
        bundleSerialData: this.props.bundleSerialData,
        serialPackData: this.props.serialPackData,
        simSerialData: this.props.simSerialData,
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        id_number: this.props.idNumber_input_value,
        id_type: this.props.customerInfoSectionIdType,
        customer_status: this.props.customerExistence == 'N' ? 'NEW' : 'EXISTING',
        customerType: this.props.customerType,
        selectedArea: _.isObject(this.props.lteSelectedArea) ? this.props.lteSelectedArea.city_name : this.state.selectedArea
      };

      let requestParamsDataPackage = {
        conn_type: additionalProps.conn_type,
        lob: additionalProps.lob,
        cx_identity_no: additionalProps.id_number,
        cx_identity_type: additionalProps.id_type,
        package_type: 'DATA',
        offer_code: selectedOfferCode
      };

      let billingCycleParams = {
        conn_type: additionalProps.conn_type,
        lob: Constants.LOB_LTE,
        cx_identity_no: additionalProps.id_number,
        cx_identity_type: additionalProps.customerType,
        customer_status: additionalProps.customer_status
      };

      let dataPackageParams = {
        requestParamsDataPackage,
        billingCycleParams,
        additionalProps
      };

      this.props.getLTEOfferDetails(requestParams, selectedData, dataPackageParams);
    } else {
      console.log('onOfferInfoDropdownSelect :: Ignore API call - getLTEOfferDetails');
    }
    
  }

  onDataPkgInfoDropdownSelect = (idx, value) => {
    console.log('onDataPackageInfoDropdownSelect', idx, value);
    this.props.resetDataPackageRental();
    let packageList = this.props.availableLTEDataPackagesList.packageList;
    let selectedPackageCode = packageList[idx].package_code;
    console.log('Selected Package Code', selectedPackageCode);

    let selectedData = {
      dropDownData: _.toArray(_.mapValues(packageList, 'package_name')),
      selectedValue: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name,
      data_package_code: selectedPackageCode,
      defaultIndex: _.findIndex(packageList, ['package_code', selectedPackageCode]),
      dataRental: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_rental,
      packageList: packageList
    };

    let requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      package_code: selectedPackageCode,
      package_type: 'DATA' // This is to get details of DATA or VOICE
    };

    console.log('onDataPackageInfoDropdownSelect :: Selected Data Ob', selectedData);
    this.props.getLTEDataPakageDetails(requestParams, selectedData);

    if (this.props.selected_connection_type === Constants.POSTPAID) {
      let depositParams = {
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        package_code: selectedPackageCode,
        cx_identity_type: this.props.customerType,
      };

      this.props.getLtePackageDeposits(depositParams);

      let billingCycleParams = {
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        cx_identity_no: this.props.idNumber_input_value,
        cx_identity_type: this.props.customerType,
        customer_status: this.props.customerExistence == 'N' ? 'NEW' : 'EXISTING',
        package_code: selectedPackageCode,
      };

      console.log('Selected Data Ob billingCycleParams', billingCycleParams);
      this.props.getLTEbillingCycleDropdownDetails(billingCycleParams);
    }

  }

  onPressBillingCycleDropdownSelect = (idx, value) => {
    console.log('onPressBillingCycleDropdownSelect', idx, value);
    let billingCycleList = this.props.lte_billing_cycle_number.dates;
    let selectedBillingCycleDate = billingCycleList[idx].key;
    console.log('Selected selectedValue', selectedBillingCycleDate);

    const selectedData = {
      dropDownData: _.toArray(_.mapValues(billingCycleList, 'value')),
      selectedValue: _.filter(billingCycleList, { 'key': selectedBillingCycleDate })[0].value,
      defaultIndex: _.findIndex(billingCycleList, ['key', selectedBillingCycleDate]),
      dates: billingCycleList,
      selectedBillingCycleCode: selectedBillingCycleDate

    };

    this.props.setBillingCycle(selectedData);

  }

  onPressVoicePkgInfoDropdownSelect = (idx, value) => {
    console.log('onDataPackageInfoDropdownSelect', idx, value);
    let packageList = this.props.availableLTEVoicePackagesList.packageList;
    let selectedPackageCode = packageList[idx].package_code;
    console.log('Selected Package Code', selectedPackageCode);

    let selectedData = {
      dropDownData: _.toArray(_.mapValues(packageList, 'package_name')),
      selectedValue: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name,
      voice_package_code: selectedPackageCode,
      defaultIndex: _.findIndex(packageList, ['package_code', selectedPackageCode]),
      packageList: packageList
    };

    let requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: 'lte',
      package_code: selectedPackageCode,
      package_type: 'VOICE' // This is to get details of DATA or VOICE,
    };

    console.log('Selected Data Ob', selectedData);

    this.props.getLTEVoicePakageDetails(requestParams, selectedData);
  }

  onSerialTypeDropdownSelect = (idx, value) => {
    console.log('onSerialTypeDropdownSelect', idx, value);
    this.fullResetSelectedConnectionNumber();
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.lteResetSerialValues();
    this.props.setSerialScanMode(idx);
    this.props.commonResetKycImages();
  }

  /**
   * @description NIC and Passport validation on text change event
   * @param {Number} nicNumber
   * @memberof LteMainViewContainer
   */
  onIdNumberTextChange = (nicNumber) => {
    console.log("onIdNumberTextChange", nicNumber);
    this.fullResetSelectedConnectionNumber();
    this.props.lteSetInputIdNumber(nicNumber);
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();    
    this.clearReduxValues('idValidation');
    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      this.props.lteSetCustomerInfoSectionIdType(Constants.CX_INFO_TYPE_NIC);
      //Check wether message need to show or not
      if (Utill.nicNumberRealTimeValidate(nicNumber).showMessage) {
        console.log('onIdNumberTextChange :: Invalid ID number');
        //Utill.showAlertMsg('Please enter valid NIC');
        this.props.lteSetIdNumberError(this.state.locals.please_enter_valid_nic);
      } else {
        console.log('onIdNumberTextChange :: Valid ID number');
        this.props.lteSetIdNumberError('');
        //Check NIC validity
        if (Utill.nicNumberRealTimeValidate(nicNumber).status) {
          //Call the NIC validation API
          console.log('onIdNumberTextChange :: Call the NIC validation API');
          const requestParams = {
            conn_type: this.props.selected_connection_type,
            lob: Constants.LOB_LTE,
            cx_identity_no: nicNumber,
            cx_identity_type: 'NIC'

          };

          Keyboard.dismiss();
          this.props.lteCustomerValidation(requestParams, this.props.selected_connection_type, this);
          this.state.currentTime = new Date().toLocaleString();
          console.log('current time', this.state.currentTime);
        }
      }
    } else {
      this.props.lteSetCustomerInfoSectionIdType(Constants.CX_INFO_TYPE_PASSPORT);
      if (Utill.passportNumberRealTimeValidate(nicNumber).showMessage) {
        console.log('onIdNumberTextChange :: Invalid ID number');
        //Utill.showAlertMsg('Please enter valid Passport');
        this.props.lteSetIdNumberError(this.state.locals.please_enter_valid_pp);
      } else {
        console.log('onIdNumberTextChange :: Valid ID number');
        this.props.lteSetIdNumberError('');
      }
    }
  }

  /**
   * @description Passport validation backend API call
   * @memberof LteMainViewContainer
   */
  onPressValidatePassport = () => {
    console.log('onPressValidatePassport');
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      console.log('onPressValidatePassport :: PASSPORT - SKIP_API_CALL');
      return;
    }
    if (Utill.passportNumberRealTimeValidate(this.props.idNumber_input_value).status) {
      console.log('onPressValidatePassport :: PASSPORT - ALLOW_API_CALL');
      const requestParams = {
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        cx_identity_no: this.props.idNumber_input_value,
        cx_identity_type: 'PP'

      };
      this.props.lteSetIdNumberError('');
      Keyboard.dismiss();
      this.props.lteCustomerValidation(requestParams, this.props.selected_connection_type, this);
    } else {
      this.props.lteSetIdNumberError(this.state.locals.please_enter_valid_pp);
    }
  }

  /**
   * @description Handle eZ Cash PIN input tet field change
   * @param {Number} ezCashPin
   * @memberof LteMainViewContainer
   */
  onExCashPinTextChange = (ezCashPin) => {
    console.log('xxx onExCashPinTextChange :', ezCashPin);
    let me = this;
    me.props.lteSetEzCashPin(ezCashPin);
    if (Utill.validateNumberFormat(ezCashPin) && ezCashPin.length === 4) {
      console.log('xxx onExCashPinTextChange VALID EZ_CASH_PIN ', ezCashPin);
      const requestParams = {
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_LTE,
        subscriber_pin: ezCashPin,
        payment_amount: !_.isNil(this.props.data_eZCashTotalPaymentProps) ? this.props.data_eZCashTotalPaymentProps.eZCashtotalPay : 0
      };
      me.props.lteEzCashAccountCheck(requestParams, me);
    } else {
      this.props.lteResetEzcashBalance();
    }
  }

  onPressEzCashCancel = () => {
    console.log('xxx onPressEzCashCancel');
    this.goToHomePage();
  }

  onPressEzCashReTry = () => {
    console.log('xxx onPressEzCashReTry');
    this.props.lteSetEzCashPin('');
    this.modalDismiss();
  }

  /**
   * @description Check agent's eZ Cash account and show error message
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showEzCashErrorModal = (response) => {
    console.log('xxx showEzCashErrorModal ::  response', response);
    let description = response.error;
    let description_title = response.title;
    let passProps = {
      secondaryButtonStyle: { color: Colors.colorGrey },
      primaryButtonStyle: { color: Colors.colorRed },
      disabled: false,
      hideTopImageView: false,
      modalHeightRatio: 0.45,
      icon: Images.icons.FailureAlert, 
      descriptionTitle: description_title,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      description: Utill.getStringifyText(description),
    };

    if (response.ez_cash_error_type=='BLOCKED') {   
      passProps.primaryText = this.state.locals.btnOk;
      passProps.primaryPress = () => this.onPressEzCashCancel();
     
    } else if (response.ez_cash_error_type=='BALANCE_INSUFFICIENT') {
      passProps.primaryText = this.state.locals.btnOk;
      passProps.primaryPress = () => this.onPressEzCashReTry();

    } else {
      passProps.primaryText = this.state.locals.btnRetry;
      passProps.secondaryText = this.state.locals.btnCancel;
      passProps.primaryPress = () => this.onPressEzCashReTry();
      passProps.secondaryPress = () => this.onPressEzCashCancel();
    }

    this.LteGeneralModal(passProps);
  }

  /**
   * @description Check agent's eZ Cash account and show error message
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showEzCashBalanceInfo = (response) => {
    console.log('xxx showEzCashBalanceInfo ::  response', response);

  }

  onVoucherCodeTextChange = async (text) => {
    console.log('xxx onVoucherCodeTextChange');
    let me = this;
    await me.props.setVoucherCodeText(text);
    if (!Utill.alphaNumericValidation(text) && text !== '') {
      this.setState({ voucherCodeError: this.state.locals.please_enter_valid_voucher_code });
    } else {
      this.setState({ voucherCodeError: '' });
    }
  };

  onPressVoucherCodeValidate = () => {
    console.log('onPressVoucherCodeValidate');
    console.log("Voucher Code: ", this.props.enteredVoucherCode);
    console.log("ID No: ", this.props.idNumber_input_value);

    let me = this;
    let previousVoucherCode = this.props.validatedVoucherCode;

    this.setState({ previousVoucherCode: previousVoucherCode });

    let successCb = (res) =>{
      console.log("previous vCode: ", this.state.previousVoucherCode);
      console.log("validated vCode: ", res.voucher_code);
      if (me.state.previousVoucherCode !== '' && (me.state.previousVoucherCode !== res.voucher_code)){
        me.props.cancelVoucherCodeReservation(this.state.previousVoucherCode);
        // me.setState({ previousVoucherCode: res.voucher_code });
      }
    };

    let requestParams = {
      "conn_type": this.props.selected_connection_type,
      "lob": Constants.LOB_LTE,
      "cx_identity_no": this.props.idNumber_input_value,
      "voucher_code": this.props.enteredVoucherCode,
      "group_name": this.props.voucher_group_name,
      "offer_code": this.props.voucher_code_validity_offer_code
    };
    console.log("requestParams: ", requestParams);
    this.props.checkVoucherCodeValidity(requestParams, successCb,this.props.enteredVoucherCode);


  }

  onAreaTextChange = (text) => {
    console.log("onAreaTextChange", text);
  }

  onConnectionNoTextChange = (text) => {
    console.log("onConnectionNoTextChange", text);
  }

  onPressSection = (key) => {
    console.log("xxxxx onPressSection :", key);
    let { selected_number = '' } = this.props.selected_connection_number;
    if (selected_number !== '') {
      this.props.lteSetProductInfoValidationStatus(true);
    }

    let validation_status = {
      section_1: this.props.sectionId_validation_status,
      section_2: this.props.sectionProductInfo_validation_status,
      section_3: this.props.sectionCustomerInfo_validation_status
    };

    let current_View_status = {
      section_1: this.props.section_1_visible,
      section_2: this.props.section_2_visible,
      section_3: this.props.section_3_visible
    };

    this.props.lteSetSectionVisibility(key, validation_status, current_View_status);
  }

  /**
   * @description Show OTP number selection modal 
   * @param {Object} customerData
   * @memberof LteMainViewContainer
   */
  showOtpNumberSelectionModal = (customerData) => {
    console.log('xxx showOtpNumberSelectionModal :: customerData', customerData);
    let screen = {
      title: 'OtpNumberSelectionModal',
      id: 'DialogRetailerApp.modals.OtpNumberSelectionModal',
    };
    let passProps = {
      modalTitle: 'OtpNumberSelectionModal',
      customerData: customerData,
      sendOtpMessage: this.sendOtpMessage,

    };
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Show OTP entering modal (Send OTP success callback)
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showOtpEnteringModal = (response) => {
    console.log('xxx showOtpEnteringModal :: response', response);
    this.modalDismiss();
    let screen = {
      title: 'OtpEnterModal',
      id: 'DialogRetailerApp.modals.OtpEnterModal',
    };
    let passProps = {
      modalTitle: 'OtpEnterModal',
      otpInfo: response.info,
      response: response,
      reSendOtpMessage: this.reSendOtpMessage,
      verifyOtp: this.verifyOtp

    };
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Show data sachet selection modal
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showDataSachetSelectionModal = (response) => {
    console.log('xxx showDataSachetSelectionModal :: response', response);
    let screen = {
      title: 'DataSachetSelectionModal',
      id: 'DialogRetailerApp.modals.DataSachetSelectionModal',
    };
    let passProps = {
      modalTitle: 'DataSachetSelectionModal',
      offerDataList: response.sachet_list

    };
    Utill.showModalView(screen, passProps);
  }


  /**
   * @description Load Data Sachet Offers from backend and show Data Sachet selection modal
   * @memberof LteMainViewContainer
   */
  loadDataSachetOffers = () => {
    console.log('xxx loadDataSachetOffers');
    let me = this;
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE

    };
    this.props.lteLoadDataSachetList(requestParams, me);
  };


  /**
   * @description Send OTP SMS to existing Prepaid and Postpaid customer 
   * @param {Number} otpNumber
   * @memberof LteMainViewContainer
   */
  sendOtpMessage = (otpNumber) => {
    console.log('xxx sendOtpMessage :: otpNumber', otpNumber);
    if (otpNumber == '' || otpNumber == undefined) {
      Utill.showAlert(this.state.locals.please_select_mobile_number);
      return;
    }
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      otp_msisdn: otpNumber
    };

    this.props.lteSendOtpMessage(requestParams, this);
  };

  /**
   * @description Re Send OTP SMS to existing Prepaid and Postpaid customer 
   * @param {Number} otpNumber
   * @param {Number} refId
   * @memberof LteMainViewContainer
   */
  reSendOtpMessage = (otpNumber, refId) => {
    console.log('xxx checkCustomerOutstanding :: otpNumber, refId', otpNumber, refId);
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      otp_msisdn: otpNumber,
      ref_id: refId,
      reference_id: refId
    };

    this.props.lteSendOtpMessage(requestParams, this);
  };

  otpResendSuccessfulMessage = () => {
    console.log('xxx otpResendSuccessfulMessage');
    Utill.showAlertMsg('Successfully resend OTP');
  }

  /**
   * @description Validate OTP SMS from backend
   * @param {Number} otpNumber
   * @param {Number} otpPin
   * @param {Number} refId
   * @memberof LteMainViewContainer
   */
  verifyOtp = (otpNumber, otpPin, refId) => {
    console.log('xxx verifyOtp');
    let me = this;
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      otp_msisdn: otpNumber,
      otp_pin: otpPin,
      ref_id: refId,
      reference_id: refId

    };

    this.props.lteVerifyOtp(requestParams, me);
  };

  /**
   * @description Check Existing postpaid customer Outstanding amount and Show Outstanding confirm modal
   * @param {Object} customerData
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showOutstandingConfirmModal = (customerData, response) => {
    console.log('xxx showOutstandingConfirmModal :: customerData, response', customerData, response);
    let screen = {
      title: 'CustomerOutstandingModal',
      id: 'DialogRetailerApp.modals.CustomerOutstandingModal',
    };
    let passProps = {
      modalTitle: 'OutstandingModalDtv',
      outstandingData: response,
      customerData: customerData,
      outstandingConfirmContinue: this.outstandingConfirmContinue,
      outstandingConfirmCancel: this.outstandingConfirmCancel
    };
    Utill.showModalView(screen, passProps);
  }

  onPressNumberSelection = () => {
    let screen = {
      title: 'LteNumberSelectionModal',
      id: 'DialogRetailerApp.modals.LteNumberSelectionModal',
    };
    let passProps = {
      modalTitle: 'LteNumberSelectionModal',
    };
    
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Click Customer Outstanding continue
   * @param {Object} outstandingData
   * @param {Object} customerData
   * @memberof LteMainViewContainer
   */
  outstandingConfirmContinue = (outstandingData, customerData) => {
    console.log('xxx outstandingConfirmContinue :: outstandingData, customerData :', outstandingData, customerData);
    this.modalDismiss();
    if (customerData.cxExistStatus == Constants.CX_EXIST_YES
      && customerData.documnetExistInDataScan == Constants.DOCUMENT_EXIST_YES
      && customerData.notificationNumberList.length > 0) {
      console.log('POSTPAID_OTP_FLOW');
      this.showOtpNumberSelectionModal(customerData);
    } else {
      console.log('POSTPAID_NEW_CUSTOMER_NORMAL_FLOW');
    }
  };

  /**
   * @description Click Customer Outstanding no
   * @param {Object} response
   * param {Object} customerData
   * @memberof LteMainViewContainer
   */
  outstandingConfirmCancel = () => {
    console.log('xxx outstandingConfirmCancel :: response, customerData :');
    Utill.showAlert('Do you want to cancel the operation');
  };

  /**
   * @description Existing postpaid customer without Outstanding amount
   * @param {Object} customerData
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  customerWithoutOutstanding = (customerData, response) => {
    console.log('xxx customerWithoutOutstanding :: customerData, response :', response, customerData);
    if (customerData.cxExistStatus == Constants.CX_EXIST_YES
      && customerData.documnetExistInDataScan == Constants.DOCUMENT_EXIST_YES
      && customerData.notificationNumberList.length > 0) {
      console.log('POSTPAID_OTP_FLOW');
      this.showOtpNumberSelectionModal(customerData);
    } else {
      console.log('POSTPAID_NEW_CUSTOMER_NORMAL_FLOW');
    }
  };

  /**
   * @description Check existing postpaid customer Outstanding amount
   * @param {Object} customerData
   * @memberof LteMainViewContainer
   */
  checkCustomerOutstanding = (customerData) => {
    console.log('xxx checkCustomerOutstanding :: response', customerData);
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      cx_identity_no: customerData.id_number,
      cx_identity_type: customerData.id_type,
      identification_no: customerData.id_number,
      identification_type: customerData.id_type
    };

    this.props.lteCustomerOutstandingCheck(requestParams, customerData, this);
  };


  checkAddVoiceField = (checkBoxType) => {
    console.log('xxx checkAddVoiceField :: checkBoxType ', checkBoxType);
    const { addVoiceChecked, analogPhoneChecked } = this.props.lteVoiceCheckboxData;

    let data = {
      addVoiceChecked: addVoiceChecked,
      analogPhoneChecked: analogPhoneChecked
    };

    switch (checkBoxType) {
      case 'voice':
        data.addVoiceChecked = !data.addVoiceChecked;
        if (data.addVoiceChecked == false) {
          data.analogPhoneChecked = false;
          this.props.resetLTEVoicePackageData();
          this.props.lteResetAnalogPhoneSerialData();
        } else {
          let requestParams = {
            conn_type: this.props.selected_connection_type,
            lob: Constants.LOB_LTE,
            package_type: 'VOICE'
          };
          this.props.getLTEVoicePackagesList(requestParams);
        }
        break;
      case 'phone':
        data.analogPhoneChecked = !data.analogPhoneChecked;
        if (!data.analogPhoneChecked) {
          this.props.lteResetAnalogPhoneSerialData();
        }
        break;
      default:
        data = data;
    }
    this.props.setLTEVoiceCheckboxData(data);
  }

  fullResetSelectedConnectionNumber = () =>{
    let { selected_number = '' } = this.props.selected_connection_number;
    if (selected_number !== '') {
      this.props.resetSelectedConnectionNumber();
      this.props.numberPoolRelease({ type: Constants.RELEASE_TYPES.RELEASE_ALL }, null, this);
    }
  }

  onTelcoAreaDropDownSelect = (idx, value) => {
    console.log('onTelcoAreaDropDownSelect', idx, value);
    console.log('selected Data Obj', this.props.telcoAreaData.searchResult[idx]);
    console.log('in reserved_number_list', this.props.reserved_number_list.reserved_number_list);
    console.log('in this.props.reserved_number_list', this.props.reserved_number_list);

    this.fullResetSelectedConnectionNumber();

    let requestParams = {
      "conn_type": this.props.selected_connection_type,
      "lob": Constants.LOB_LTE,
      "area_code": this.props.telcoAreaData.searchResult[idx].area_code,
      "number_count": 1,
    };

    this.props.setLTESelectedArea(this.props.telcoAreaData.searchResult[idx], requestParams);
  }

  /**
   * @description Open image capture camera module
   * @param {Number} captureType
   * @memberof LteMainViewContainer
   */
  onPressImageCapture = (captureType) => {
    console.log('#### onPressImageCapture : captureType => ', captureType);
    let me = this;
    let screen = {
      title: 'Image Capture Module',
      id: 'DialogRetailerApp.views.ImageCaptureModule',
    };

    let passProps = {
      screenTitle: 'Image Capture Module',
      backMessage: me.state.locals.do_uou_want_to_cancel_image_capture_and_go_back,
      imageCaptureTypeNo: captureType,
      lob: Constants.LOB_LTE,
      reCapture: false
    };

    Screen.pushView(screen, me, passProps);
  }


  /**
   * @description Common method to hide individual items in both Prepaid and Postpaid activation
   * @param {String} refName
   * @memberof LteMainViewContainer
   */
  getIndividualItemHideStatus = (refName) => {
    console.log('#### getIndividualItemHideStatus : refName => ', refName);
    console.log('#### getIndividualItemHideStatus : CONNECTION_TYPE ::' + this.props.selected_connection_type);
    if (this.props.selected_connection_type === Constants.PREPAID) {
      switch (refName) {
        case 'idNumber':
          console.log('## idNumber');
          return false;
        case 'serialType':
          console.log('# serialType');
          return false;
        case 'voucherCode':
          console.log('# voucherCode');
          return !this.props.voucherCodeAvailable;
        case 'voicepackages':
          console.log('# voicepackages');
          return !this.props.lteVoiceCheckboxData.addVoiceChecked;
        case 'analogPhoneSerial':
          console.log('# analogPhoneSerial');
          return !this.props.lteVoiceCheckboxData.analogPhoneChecked;
        case 'area':
          console.log('# area');
          return false;
        case 'connectionNo':
          console.log('# connectionNo');
          return false;
        case 'depositamount':
          console.log('# depositamount');
          return true;
        case 'billingcycle':
          console.log('# billingcycle');
          return true;
        case 'dataSachetReload':
          console.log('# dataSachetReload');
          return !this.props.enable_reload;
        case 'email':
          console.log('# email');
          return false;
        case 'customerMobile':
          console.log('# customerMobile');
          return false;
        case 'customerLandline':
          console.log('# customerLandline');
          return false;
        case 'ezCashPin':
          console.log('# ezCashPin data_eZCashTotalPaymentProps', this.props.data_eZCashTotalPaymentProps.eZCashtotalPay);
          return !(this.props.common.kyc_signature !== null
            && (this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== "0.00"
              && this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== ""));
        case 'ezCashBalance':
          console.log('# ezCashBalance');
          return !(this.props.common.kyc_signature !== null
            && (this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== "0.00"
              && this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== ""));
        case 'totalPayment':
          console.log('# totalPayment');
          return !this.props.sectionId_validation_status;
        case 'ezCashtotalPayment':
          console.log('# ezCashtotalPayment');
          return (!((this.props.sectionId_validation_status)? this.props.enable_dealer_collection : false ));
        default:
          console.log('# default :: ' + refName);
          return false;
      }
    } else {

      switch (refName) {
        case 'idNumber':
          console.log('## idNumber');
          return false;
        case 'serialType':
          console.log('# serialType');
          return false;
        case 'voucherCode':
          console.log('# voucherCode');
          return !this.props.voucherCodeAvailable;
        case 'voicepackages':
          console.log('# voicepackages');
          return !this.props.lteVoiceCheckboxData.addVoiceChecked;
        case 'analogPhoneSerial':
          console.log('# analogPhoneSerial');
          return !this.props.lteVoiceCheckboxData.analogPhoneChecked;
        case 'area':
          console.log('# area');
          return false;
        case 'connectionNo':
          console.log('# connectionNo');
          return false;
        case 'depositamount':
          console.log('# depositamount');
          return false;
        case 'billingcycle':
          console.log('# billingcycle');
          return false;
        case 'dataSachetReload':
          console.log('# dataSachetReload');
          return true;
        case 'email':
          console.log('# email');
          return false;
        case 'customerMobile':
          console.log('# customerMobile');
          return false;
        case 'customerLandline':
          console.log('# customerLandline');
          return false;
        case 'ezCashPin':
          console.log('# ezCashPin data_eZCashTotalPaymentProps', this.props.data_eZCashTotalPaymentProps.eZCashtotalPay);
          return !(this.props.common.kyc_signature !== null
            && (this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== "0.00"
              && this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== ""));
        case 'ezCashBalance':
          console.log('# ezCashBalance');
          return !(this.props.common.kyc_signature !== null
            && (this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== "0.00"
              && this.props.data_eZCashTotalPaymentProps.eZCashtotalPay !== ""));
        case 'totalPayment':
          console.log('# totalPayment');
          return !this.props.sectionId_validation_status;
        case 'ezCashtotalPayment':
          console.log('# ezCashtotalPayment');
          return (!((this.props.sectionId_validation_status)? this.props.enable_dealer_collection : false ));
        default:
          console.log('# default :: ' + refName);
          return false;
      }
    }
  }

  /**
   * @description Common method to clear redux values
   * @param {String} clearLevel
   * @memberof LteMainViewContainer
   */
  clearReduxValues = async (clearLevel) => {
    console.log('#### clearReduxValues :: clearLevel :: ### ', clearLevel);
    switch (clearLevel) {
      case 'idValidation':
        await this.props.lteResetSectionValidationStatus();
        break;
      case 'serialValidation':
        break;
      default:
        //Clear all data related to LTE
        await this.props.resetLTEData(true);
        break;
    }
  }


  isPrepaid = () => {
    console.log('#### isPrepaid');
    return this.props.selected_connection_type === Constants.PREPAID;
  }

  getCustomerIdTypeText = () => {
    console.log('#### getCustomerIdTypeText');
    return this.props.customerIdType == Constants.CX_ID_TYPE_NIC ?
      this.state.locals.id_nic_mc : this.state.locals.id_passport_mc;
  }

  getTermAndConditionUrl = () => {
    const { tcUrls } = this.props;
    let returnUrl;
    if (this.isPrepaid()) {
      returnUrl = tcUrls.PREPAID;
    } else {
      returnUrl = tcUrls.POSTPAID;
    }
    return returnUrl;
  }

  onCaptureSignature = (totalPaymentProps, eZCashTotalPaymentProps) => {
    
    if ( FuncUtils.isNullOrUndefined(this.props.tcUrls)){
      return;
    }
    console.log('#### onCaptureSignature', this.props.customerMobile);
    if (this.props.validateMobileNumber != '' && Utill.mobileNumberValidateAny(this.props.validateMobileNumber)) {
      const navigatorOb = this.props.navigator;
      this.props.lteSetTotalPayment(totalPaymentProps);
      this.props.lteSetEzCashTotalPayment(eZCashTotalPaymentProps);

      Orientation.lockToPortrait();
      navigatorOb.showModal({
        title: '',
        screen: 'DialogRetailerApp.modals.HbbActivationModal',
        overrideBackPress: true,
        passProps: {
          onUnmount: () => {
          },
          showSignature: () => {
            this.modalDismiss();
            Orientation.lockToLandscape();
            navigatorOb.push({
              title: '',
              screen: 'DialogRetailerApp.views.SignaturePadModule',
              passProps: {
                onReturnView: 'DialogRetailerApp.views.LteActScreen',
                tncUrl: this.getTermAndConditionUrl(),
                confirmModal: ()=>this.modalDismiss()
              },
              overrideBackPress: true
            });
          },

        }
      });
    } else {
      Utill.showAlert(this.state.locals.please_enter_valid_mobile);
    }
  }

  /**
   * @description Final activation API call implementation - TODO
   * @memberof LteMainViewContainer
   */
  onPressActivate = () => {
    console.log('##### onPressActivate #####');
    let me = this;
    let activity_start_time = this.props.activity_start_time;
    let activity_end_time = Math.round((new Date()).getTime() / 1000);

    console.log('activity_start_time', activity_start_time);
    console.log('activity_end_time', activity_end_time);

    if (!me.getActivateButtonStatus().status) {
      console.log('##### onPressActivate :: DISPLAY_ALERT');
      Utill.showAlert(me.getActivateButtonStatus().reason);
      return;
    }

    let os_details = '';

    if (this.props.customerOutstandingData !==undefined && 
      this.props.customerOutstandingData !== null) {
      os_details = this.props.customerOutstandingData.info;
    }

    const requestParams = {
      start_ts: activity_start_time,
      end_ts: activity_end_time,
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_LTE,
      cx_type: this.props.customerType,
      cx_identity_type: this.props.customerIdType,
      cx_id_reference_type: this.props.customerInfoSectionIdType,
      cx_identity_no: this.props.idNumber_input_value,
      conn_number: this.props.selected_connection_number.selected_number,
      conn_number_data: this.props.selected_connection_number,
      otp_verified_status: this.props.customerOtpStatus,
      otp_verified_number: this.props.cx_otp_number,
      id_not_clear: this.props.opt_customerFaceNotClear,
      address_different: this.props.opt_addressDifferent,
      install_address_different: this.props.opt_installationAddressDeferent,
      voice_checked: this.props.lteVoiceCheckboxData.addVoiceChecked,
      analog_phone_checked: this.props.lteVoiceCheckboxData.analogPhoneChecked,
      sap_material_items: this.props.allSerialData, //OPTIONAL - NO NEED
      sap_material_code: this.props.allSerialData, //OPTIONAL  - NO NEED
      data_package_code: this.props.availableLTEDataPackagesList.data_package_code,
      voice_package_code: this.props.availableLTEVoicePackagesList.voice_package_code,
      offer_code: this.props.availableLTEOffersList.offer_Code,
      voucher_code: this.props.enteredVoucherCode, //INPUT
      voucher_value: this.props.availableVaoucherCodeValue.voucher_value, //TODO - PRE - Voucher cancellation API
      alt_contact_number: this.props.customerMobile,
      contact_number_land: this.props.customerLandline,
      contact_email: this.props.email_input_value,
      om_campaign_record_no: this.props.om_campaign_record_no == '' ? 
        this.props.multiplayOfferData.om_campaign_record_no :  this.props.om_campaign_record_no, //OPTIONAL - POST for voucher code
      reservation_no: this.props.reservation_no == '' ? 
        this.props.multiplayOfferData.reservation_no :  this.props.reservation_no, //PRE voucher reservation number 
      voucher_group_name: this.props.voucher_group_name,
      reference_no: this.props.reference_no, //voucher reference number 
      serial_type: this.props.serial_scan_mode == Constants.BUNDLE_SERIAL ? 'bundle' : 'individual',
      bundle_serial: this.props.bundleSerialValue,
      sim_serial: this.props.simSerialValue,
      pack_serial: this.props.serialPackValue,
      analog_phone_serial: this.props.serialPackValue,
      image_type_list: this.getImageTypeList(),
      product_items: this.props.allSerialData,
      ezcash_pin: this.props.ez_cash_input_value,
      total_amount: this.props.data_totalPaymentProps.totalPayValue, //OPTIONAL
      reload_amount: this.props.reloadAmount,
      adjustment_amount: this.props.enable_dealer_collection == true ? this.props.data_eZCashTotalPaymentProps.unitPriceAdjustment : 0,
      deposit_amount: this.props.data_totalPaymentProps.depositAmount,
      deposit_type: this.props.lte_deposit_value.depositType,
      os_amount: this.props.data_totalPaymentProps.outstandingFeeValue,
      connection_os_details: os_details,
      connection_fee: this.props.availableLTEOffersList.offerPrice,
      device_selling_price: this.props.data_eZCashTotalPaymentProps.deviceSellingPrice,
      referenceId: this.props.ref_id,
      reload_offer_code: this.props.reloadOfferCode,
      bill_run_code: this.props.lte_billing_cycle_number.selectedBillingCycleCode
    };

    console.log("Final activation data: ", requestParams);

    me.props.lteFinalActivationApi(requestParams, me);
  }

  getImageTypeList = () => {
    console.log('xxxx getImageTypeList');
    let imageTypeList = [];
    this.props.common.kyc_nic_front !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT) : true;
    this.props.common.kyc_nic_back !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.NIC_BACK) : true;

    this.props.common.kyc_driving_licence !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE) : true;

    this.props.common.kyc_passport !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.PASSPORT) : true;
    this.props.common.kyc_proof_of_billing !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING) : true;

    this.props.common.kyc_customer_image !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE) : true;

    this.props.common.kyc_signature !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.SIGNATURE) : true;

    this.props.common.kyc_additional_pob !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB) : true;

    console.log('xxxx imageTypeList', imageTypeList);
    return imageTypeList;
  };

  /**
   * @description Add to image upload SQLite db
   * @param {Number} txnId
   * @param {Number} imageId
   * @param {String} imageOb
   * @memberof LteMainViewContainer
   */
  addToImageUploadQueue = async (txnId, imageId, imageOb) => {
    console.log('xxx LTE :: addToImageUploadQueue :: txnId, imageId, imageUri : ', txnId, imageId, imageOb);
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id = `${txnId}_${imageId}`;
      const url = Utill.createApiUrl('imageUploadEnc', 'initAct', 'ccapp');
      const imageFile = imageOb.imageUri.replace('file://', '');
      const uploadSelector = 'encrypt';

      const options = {
        id: id,
        url: url,
        imageFile: imageFile,
        uploadSelector: uploadSelector,
        deviceData: JSON.stringify(deviceData)
      };

      console.log('xxx LTE :: addToImageUploadQueue :: options : ', options);

      const successCb = (msg) => {
        console.log('xxxx addToImageUploadQueue successCb');
        Analytics.logEvent('dsa_lte_activation_sqlite_add_success');
        console.log(msg);
      };

      const errorCb = (msg) => {
        console.log('xxxx addToImageUploadQueue errorCb ');
        Analytics.logEvent('dsa_lte_activation_sqlite_add_error');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, successCb);

    } catch (e) {
      console.log('addToImageUploadQueue Image Uploading Failed :', e.message);
      Analytics.logEvent('dsa_lte_activation_sqlite_exception');
      this.showAlert(e.message);
    }
  }

  /**
   * @description Get activate button status
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  getActivateButtonStatus = () => {
    console.log('xxx getActivateButtonStatus');
    let me = this;
    //Check analog phone serial if analog phone checked
    if (me.props.lteVoiceCheckboxData.analogPhoneChecked && me.props.analogPhoneSerialValue === '') {
      return {
        status: false,
        reason: this.state.locals.please_enter_analog_phone_serial
      };
    }
    //Check image capture
    if (me.props.customerType === Constants.CX_TYPE_LOCAL
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {
      if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_NIC
        && (me.props.common.kyc_nic_front === null
          || me.props.common.kyc_nic_back === null)) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> NIC');
        return {
          status: false,
          reason: this.state.locals.please_capture_nic
        };
      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_PASSPORT) {
        if (me.props.common.kyc_passport === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - PASSPORT');
          return {
            status: false,
            reason: this.state.locals.please_capture_passport
          };
        }
        if (me.props.common.kyc_proof_of_billing === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - POB');
          return {
            status: false,
            reason: this.state.locals.please_capture_pob
          };
        }

      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_DRIVING_LICENCE
        && me.props.common.kyc_driving_licence === null) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> DRIVING_LICENCE');
        return {
          status: false,
          reason: this.state.locals.please_capture_driving_licence
        };
      }
    } else if (me.props.customerType === Constants.CX_TYPE_FOREIGNER
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {

      if (me.props.common.kyc_passport === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PASSPORT');
        return {
          status: false,
          reason: this.state.locals.please_capture_passport
        };
      }

      if (me.props.common.kyc_proof_of_billing === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PROOF_OF_BILLING');
        return {
          status: false,
          reason: this.state.locals.please_capture_pob
        };
      }
    }

    //Check optional images
    if (me.props.opt_addressDifferent && me.props.common.kyc_proof_of_billing === null) {
      console.log('ANY >> CHECK_BOX >> PROOF_OF_BILLING');
      return {
        status: false,
        reason: this.state.locals.please_capture_pob
      };
    }

    if (me.props.opt_customerFaceNotClear && me.props.common.kyc_customer_image === null) {
      console.log('ANY >> CHECK_BOX >> CUSTOMER_IMAGE');
      return {
        status: false,
        reason: this.state.locals.please_enter_customer_face
      };
    }

    if (me.props.opt_installationAddressDeferent && me.props.common.kyc_additional_pob === null) {
      console.log('ANY >> CHECK_BOX >> ADDITIONAL_POB');
      return {
        status: false,
        reason: this.state.locals.please_enter_poi
      };
    }

    //Check signature image
    if (me.props.common.kyc_signature === null) {
      console.log('ANY >> SIGNATURE');
      return {
        status: false,
        reason: this.state.locals.please_enter_signature
      };
    }
    console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay ', me.props.data_eZCashTotalPaymentProps.eZCashtotalPay);
    console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay type ', typeof me.props.data_eZCashTotalPaymentProps.eZCashtotalPay);
    console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay 1 ', parseInt(me.props.data_eZCashTotalPaymentProps.eZCashtotalPay) !== 0);
    console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay 2 ', toString(me.props.data_eZCashTotalPaymentProps.eZCashtotalPay) != "");
    console.log('#### ez_cash_input_value 3 ', me.props.ez_cash_input_value.length != 4);

    //Check eZ cash PIN
    if ((parseInt(me.props.data_eZCashTotalPaymentProps.eZCashtotalPay) != 0
      && toString(me.props.data_eZCashTotalPaymentProps.eZCashtotalPay) != "")
      && me.props.ez_cash_input_value.length != 4) {
      console.log('ANY >> EZ_CASH_PIN');
      return {
        status: false,
        reason: this.state.locals.please_enter_ez_cash_pin
      };
    }

    return {
      status: true,
      reason: ''
    };
  }

  onChangeTextEmail = (text) => {
    console.log('xxx onChangeTextEmail', text);
    this.setState({ email_input_value: text });
    this.props.lteSetInputemail(text);

    if (text.length >= 3) {

      if (Utill.emailValidation(text)) {
        this.setState({ emailError: '' });
      } else {
        this.setState({ emailError: this.state.locals.please_enter_valid_email });
      }
    } else if (text.length < 3) {

      this.setState({ emailError: '' });
    }
  }

  onChangeTextCustomerLandline = (text) => {
    console.log('xxx onChangeTextCustomerLandline :', text);
    this.setState({ customerLandline: text });
    this.props.lteSetInputLandlineNumber(text);
    if (text.length >= 3) {
      if (Utill.landlineNumberValidation(text)) {
        this.setState({ landlineError: '' });
      } else {
        this.setState({ landlineError: this.state.locals.please_enter_valid_laneline });
      }
    } else if (text.length < 3) {
      this.setState({ landlineError: '' });
    }
  }

  onChangeTextCustomerMobile = (text) => {
    console.log('xxx onChangeTextCustomerMobile :', text);
    this.setState({ customerMobile: text });
    this.props.lteSetInputMobileNumber(text);
    if (text.length >= 3) {
      if (Utill.mobileNumberValidateAny(text)) {
        this.setState({ mobileError: '' });
      } else {
        this.setState({ mobileError: this.state.locals.please_enter_valid_mobile });
      }
    } else if (text.length < 3) {

      this.setState({ mobileError: '' });
    }
  }

  /**
   * @description Add all images to image upload thread (SQLite Db)
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  addToImageUploadProcess = (response) => {
    console.log('xxx addToImageUploadProcess :: response', response);
    let me = this;
    let transactionId = response.transaction_id;

    if (me.props.common.kyc_nic_front !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_FRONT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT,
        me.props.common.kyc_nic_front);
    }

    if (me.props.common.kyc_nic_back !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_BACK');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_BACK,
        me.props.common.kyc_nic_back);
    }

    if (me.props.common.kyc_driving_licence !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => DRIVING_LICENCE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE,
        me.props.common.kyc_driving_licence);
    }

    if (me.props.common.kyc_passport !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PASSPORT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PASSPORT,
        me.props.common.kyc_passport);
    }

    if (me.props.common.kyc_proof_of_billing !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PROOF_OF_BILLING');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING,
        me.props.common.kyc_proof_of_billing);
    }

    if (me.props.common.kyc_customer_image !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => CUSTOMER_IMAGE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE,
        me.props.common.kyc_customer_image);
    }

    if (me.props.common.kyc_signature !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => SIGNATURE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.SIGNATURE,
        me.props.common.kyc_signature);
    }

    if (me.props.common.kyc_additional_pob !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => ADDITIONAL_POB');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB,
        me.props.common.kyc_additional_pob);
    }

  }

  /**
   * @description Show Activate Success Modal
   * @param {Object} response
   * @memberof LteMainViewContainer
   */
  showActivateSuccessModal = (response) => {
    console.log('xxx showActivateSuccessModal :: response', response);
    this.addToImageUploadProcess(response); //Add to image upload process
    let description = response.description;
    let description_title = response.description_title;

    let passProps = {
      primaryText: this.state.locals.btnOk,
      primaryPress: this.goToHomePage,
      primaryPressPayload: response,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.SuccessAlert,
      description: Utill.getStringifyText(description),
      descriptionTitle : description_title,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorGreen },
      
    };

    console.log('showGeneralModal :: passProps ', passProps);
    Screen.showGeneralModal(passProps);
  };

  render() {

    let totalPaymentProps = this.props.data_totalPaymentProps;
    let eZCashTotalPaymentProps = this.props.data_eZCashTotalPaymentProps;

    let idLabel;
    let idSectionLabel;
    let idNumberRightIconHide = false;
    let idNumberIcon;
    let idIconSize;

    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      idLabel = this.state.locals.labelNIc;
      idSectionLabel = this.state.locals.sectionNICValidation;
      idNumberRightIconHide = this.props.idNumber_input_value === '';
      idNumberIcon = Constants.icon.edit;
      idIconSize = 26;
    } else {
      idLabel = this.state.locals.labelPP;
      idSectionLabel = this.state.locals.sectionPPValidation;
      idNumberIcon = Constants.icon.next;
      idIconSize = Constants.icon.next.defaultFontSize;
    }

    let lte_configuration = this.props.lte_configuration;
    //const serialTypesArray = [this.state.locals.labelIndividualSerial, this.state.locals.labelBundleSerial];
    const serialTypesArray = lte_configuration.serial_type.list;

    const renderSerialMode = () => {
      /* 
      serial_scan_mode = 0 => Individual Serial 
      serial_scan_mode = 1 => Bundle Serial, 
      */
      if (this.props.serial_scan_mode == Constants.BUNDLE_SERIAL) {
        return (
          <SectionContainer >
            <MaterialInput
              label={this.state.locals.labelBundleSerial}
              index={'bundleSerial'}
              ref={'bundleSerial'}
              editable={false}
              value={this.props.bundleSerialValue}
              onIconPress={() => this.onPressSerialScan('bundleSerial')}
              icon={Constants.icon.barcode}
              disableIconTap={false}
              customStyle={styles.materialInputCustomStyle} />
          </SectionContainer>
        );
      } else {
        return (
          <View>
            <SectionContainer >
              <MaterialInput
                label={this.state.locals.labelScanSerialPack}
                index={'serialPack'}
                ref={'serialPack'}
                editable={false}
                value={this.props.serialPackValue}
                onIconPress={() => this.onPressSerialScan('serialPack')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle}
              />
            </SectionContainer>
            <SectionContainer >
              <MaterialInput
                label={this.state.locals.labelScanSimSerial}
                index={'simSerial'}
                ref={'simSerial'}
                editable={false}
                value={this.props.simSerialValue}
                onIconPress={() => this.onPressSerialScan('simSerial')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle}
              />
            </SectionContainer>
          </View>
        );
      }
    };

    const renderCxInfoImageCaptureFields = () => {

      if (this.props.customerType === Constants.CX_TYPE_LOCAL
        && (this.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
          || this.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {

        switch (this.props.customerInfoSectionIdType) {
          case Constants.CX_INFO_TYPE_NIC:
            return (<CustomerInfoNIC currentView={this} />);
          case Constants.CX_INFO_TYPE_PASSPORT:
            return (<CustomerInfoPassport currentView={this} />);
          case Constants.CX_INFO_TYPE_DRIVING_LICENCE:
            return (<CustomerInfoDrivingLicense currentView={this} />);
          default:
            return (<CustomerInfoNIC currentView={this} />);
        }
      } else if ((this.props.customerType === Constants.CX_TYPE_LOCAL
        || this.props.customerType === Constants.CX_TYPE_FOREIGNER)
        && this.props.customerOtpStatus == Constants.CX_OTP_VERIFIED) {
        return (<CustomerOtpVerified currentView={this} />);
      } else {
        return (<CustomerInfoPassport currentView={this} />);
      }
    };

    const renderCxInfoRadioButtonSection = () => {

      if (this.props.customerType === Constants.CX_TYPE_LOCAL
        && (this.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED ||
          this.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {
        return (<SectionContainer hideView={false}>
          <View style={styles.containerRadioWrapper}>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_NIC}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_NIC ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_NIC)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_NIC}
                </Text>
              </RadioButton>
            </View>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_PP}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_PP ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_PASSPORT)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_Passport}
                </Text>
              </RadioButton>
            </View>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_DL}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_DL ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_DRIVING_LICENCE)}>
                <Text numberOfLines={2} style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_Driving_Licence}
                </Text>
              </RadioButton>
            </View>
          </View>
        </SectionContainer>);
      } else {
        return true;
      }
    };

    return (
      <View style={styles.container}>
        {/* NIC INPUT SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.NIC_VALIDATION}
          sectionId={Constants.MAIN_SECTION.NIC_VALIDATION}
          label={idSectionLabel}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.NIC_VALIDATION)}
          validationStatus={this.props.sectionId_validation_status} 
          isFocused={this.props.section_1_visible} >
          <MainView
            formData={this.props}
            key={this.state + 1}
            style={styles.mainSectionContainer}
            visible={this.props.section_1_visible}
            removeWhenHidden
            noAnimation>
            <SectionContainer
              refName={'idNumber'}
              hideView={this.getIndividualItemHideStatus('idNumber')}>
              <MaterialInput
                label={idLabel}
                value={this.props.idNumber_input_value}
                index={'idNumber'}
                ref={'idNumber'}
                error={this.props.idNumber_input_value === '' ? '' : this.props.id_input_error}
                onChangeText={this.onIdNumberTextChange}
                onIconPress={() => this.onPressValidatePassport()}
                customStyle={styles.materialInputCustomStyle}
                icon={idNumberIcon}
                iconSize={idIconSize}
                disableIconTap={false}
                tapToFocus={this.props.customerType === Constants.CX_TYPE_LOCAL}
                hideRightIcon={idNumberRightIconHide}
              />
            </SectionContainer>
          </MainView>
        </InputSection>
        {/* NIC INPUT SECTION END */}
        {/* PRODUCT INFORMATION SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.PRODUCT_INFORMATION}
          sectionId={Constants.MAIN_SECTION.PRODUCT_INFORMATION}
          label={this.state.locals.sectionProductInformation}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.PRODUCT_INFORMATION)}
          validationStatus={this.props.sectionProductInfo_validation_status} 
          isFocused={this.props.section_2_visible} >
          <MainView
            formData={this.props}
            key={this.state + 2}
            style={styles.mainSectionContainer}
            visible={this.props.section_2_visible}
            removeWhenHidden
            noAnimation>
            <SectionContainer
              refName={'serialType'}
              hideView={this.getIndividualItemHideStatus('serialType')}>
              <DropDownInput
                index={'serialType'}
                ref={'serialType'}
                dropDownTitle={this.state.locals.serialType}
                onSelect={this.onSerialTypeDropdownSelect}
                dropDownData={serialTypesArray}
                selectedValue={serialTypesArray[(serialTypesArray.length -1 < this.props.serial_scan_mode ) ? serialTypesArray.length -1 : this.props.serial_scan_mode]}
                defaultIndex={this.props.selected_serial_type}
                icon={Constants.icon.info}
                hideRightIcon={true}
                onPress={() => {
                  console.log('productSerial onPress_do_nothing');
                }}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={serialTypesArray.length == 1}
                disableDropdown={serialTypesArray.length == 1} 
                disableIconTap={serialTypesArray.length == 1} />
            </SectionContainer>
            {/* Render Serial Mode */}
            {renderSerialMode()}
            <SectionContainer
              refName={'offer'}
              hideView={this.getIndividualItemHideStatus('offer')}>
              <DropDownInput
                index={'offer'}
                ref={'offer'}
                dropDownTitle={this.state.locals.offer}
                offerList={this.props.availableLTEOffersList.offerList}
                onSelect={this.onOfferInfoDropdownSelect}
                dropDownData={this.props.availableLTEOffersList.dropDownData}
                selectedValue={this.props.availableLTEOffersList.selectedValue}
                defaultIndex={this.props.availableLTEOffersList.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo(this.props.availableLTEOffersList, 'offers');
                }}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={false}
                disableDropdown={false}
                disableIconTap={false}
                title={this.props.availableLTEOffersList.offerPrice !== '' ? this.state.locals.offerPricePrefix + Utill.numberWithCommas(this.props.availableLTEOffersList.offerPrice) : ''} />
            </SectionContainer>
            <SectionContainer
              refName={'voucherCode'}
              hideView={this.getIndividualItemHideStatus('voucherCode')}>
              <MaterialInput
                label={this.state.locals.labelVoucherCode}
                index={'voucherCode'}
                ref={'voucherCode'}
                error={this.state.voucherCodeError}
                value={this.props.enteredVoucherCode}
                customStyle={styles.materialInputCustomStyle}
                onChangeText={this.onVoucherCodeTextChange}
                onIconPress={() => {
                  this.onPressVoucherCodeValidate();
                }}
                icon={Constants.icon.next}
                iconSize={Constants.icon.next.defaultFontSize}
                disableIconTap={
                  this.props.enteredVoucherCode === '' || 
                  this.state.voucherCodeError !== '' || 
                  (this.props.validatedVoucherCode === this.props.enteredVoucherCode )
                } />
            </SectionContainer>
            <SectionContainer
              refName={'datapackages'}
              hideView={this.getIndividualItemHideStatus('datapackages')}>
              <DropDownInput
                index={'datapackages'}
                ref={'datapackages'}
                dropDownTitle={this.state.locals.dataPackages}
                onSelect={this.onDataPkgInfoDropdownSelect}
                dropDownData={this.props.availableLTEDataPackagesList.dropDownData}
                selectedValue={this.props.availableLTEDataPackagesList.selectedValue}
                defaultIndex={this.props.availableLTEDataPackagesList.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo(this.props.availableLTEDataPackagesList, 'datapackages');
                }}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={this.props.disableField_VoucherCode}
                disableDropdown={this.props.disableField_VoucherCode}
                disableIconTap={this.props.disableField_VoucherCode}
                title={this.props.selected_connection_type === Constants.PREPAID ?
                  '' : (this.props.availableLTEDataPackagesList.dataRental !== '' ?
                    this.state.locals.dataRentalPrefix + Utill.numberWithCommas(this.props.availableLTEDataPackagesList.dataRental) + ' + ' + this.state.locals.tax : '')} />
            </SectionContainer>
            <SectionContainer
              refName={'depositamount'}
              hideView={this.getIndividualItemHideStatus('depositamount')}>
              <MaterialInput
                label={this.state.locals.depositAmount}
                index={'depositamount'}
                ref={'depositamount'}
                editable={false}
                value={this.props.lte_deposit_value.depositAmount !== '' ?
                  `${this.state.locals.rs_label} ${Utill.numberWithCommas(this.props.lte_deposit_value.depositAmount)}` : ''}
                onChangeText={this.onDepositAmountTextChange}
                onIconPress={() => {
                  console.log('## onIconPress #depositamount');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={true}
                disableIconTap={true}
                customStyle={styles.materialInputCustomStyle} />
            </SectionContainer>
            <SectionContainer
              refName={'checkBox_addVoicePhone'}
              customStyle={styles.checkBoxAddVoicePhoneStyle}
              hideView={this.getIndividualItemHideStatus('checkBox_addVoicePhone')}>
              <ElementCheckBox
                checkBoxText={this.state.locals.checkBoxTextAddVoice}
                onClick={() => this.debouncedCheckboxSelect('voice')}
                disabled={!this.props.didSerialValidated}
                isChecked={this.props.lteVoiceCheckboxData.addVoiceChecked} />
              {
                (this.props.analog_phone_visibility)?
                  <ElementCheckBox
                    checkBoxText={this.state.locals.checkBoxTextAnalogPhone}
                    onClick={() => this.debouncedCheckboxSelect('phone')}
                    disabled={!this.props.didSerialValidated}
                    isChecked={this.props.lteVoiceCheckboxData.analogPhoneChecked}
                    isHidden={!this.props.lteVoiceCheckboxData.addVoiceChecked}
                  />
                  :
                  null
              }
            </SectionContainer>
            <SectionContainer
              refName={'voicepackages'}
              hideView={this.getIndividualItemHideStatus('voicepackages')}>
              <DropDownInput
                index={'voicepackages'}
                ref={'voicepackages'}
                dropDownTitle={this.state.locals.voicePackages}
                onSelect={this.onPressVoicePkgInfoDropdownSelect}
                dropDownData={this.props.availableLTEVoicePackagesList.dropDownData}
                selectedValue={this.props.availableLTEVoicePackagesList.selectedValue}
                defaultIndex={this.props.availableLTEVoicePackagesList.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo(this.props.availableLTEVoicePackagesList, 'voicepackages');
                }}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={this.props.disableField_VoucherCode}
                disableDropdown={this.props.disableField_VoucherCode}
                disableIconTap={this.props.disableField_VoucherCode} />
            </SectionContainer>
            <SectionContainer
              refName={'analogPhoneSerial'}
              hideView={this.getIndividualItemHideStatus('analogPhoneSerial')}>
              <MaterialInput
                label={this.state.locals.labelAnalogPhoneSerial}
                index={'analogPhoneSerial'}
                ref={'analogPhoneSerial'}
                editable={false}
                value={this.props.analogPhoneSerialValue}
                onIconPress={() => this.onPressSerialScan('analogPhoneSerial')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle} />
            </SectionContainer>
            <SectionContainer
              refName={'area'}
              hideView={this.getIndividualItemHideStatus('area')}>
              <DropDownInput
                index={'area'}
                ref={'area'}
                modalRef={(ref) => this.dropDownSearch = ref}
                dropDownTitle={this.state.locals.area}
                onSelect={this.onTelcoAreaDropDownSelect}
                // onTextInputDropDownSelect={this.onTextInputDropDownSelect}
                // onTextInputChange={this.onTextInputChange}
                dropDownData={this.props.telcoAreaData.cityNames}
                selectedValue={_.isObject(this.props.lteSelectedArea) ? this.props.lteSelectedArea.city_name
                  : this.state.selectedArea}
                defaultIndex={-1}
                // onDropdownWillHide={this.onAreaSelectionDropdownWillHide}
                // onBlur={this.onAreaSelectionDropdownWillHide}
                // error={this.props.telcoAreaData.error}
                // // defaultIndex={this.props.availableLTEDataPackagesList.defaultIndex}
                // // icon={Constants.icon.info}
                // // onPress={() => { 
                // //   this.onPressInfo(this.props.availableLTEDataPackagesList,'datapackages'); 
                // // }}            
                // isTextInput={true}          
                hideRightIcon
                customStyle={styles.dropDownInputCustomStyle}
                disabled={!this.props.didSerialValidated}
                disableDropdown={!this.props.didSerialValidated}
                disableIconTap={!this.props.didSerialValidated} />
            </SectionContainer>
            <SectionContainer
              refName={'connectionNo'}
              hideView={this.getIndividualItemHideStatus('connectionNo')}>
              <MaterialInput
                label={this.state.locals.labelConnectionNo}
                index={'connectionNo'}
                value={this.props.selected_connection_number.selected_number}
                ref={'connectionNo'}
                editable={false}
                onChangeText={this.onConnectionNoTextChange}
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => this.onPressNumberSelection()}
                icon={Constants.icon.edit}
                hideRightIcon={false}
                tapToFocus={false}
                disableIconTap={!_.isObject(this.props.lteSelectedArea)} />
            </SectionContainer>
            <SectionContainer
              refName={'billingcycle'}
              hideView={this.getIndividualItemHideStatus('billingcycle')}>
              <DropDownInput
                index={'billingcycle'}
                ref={'billingcycle'}
                dropDownTitle={this.state.locals.billingCycle}
                onSelect={this.onPressBillingCycleDropdownSelect}
                dropDownData={this.props.lte_billing_cycle_number.dropDownData}
                selectedValue={this.props.lte_billing_cycle_number.selectedValue}
                defaultIndex={this.props.lte_billing_cycle_number.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo(this.props.lte_billing_cycle_number, 'billingcycle');
                }}
                hideRightIcon={true}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={!_.isObject(this.props.lteSelectedArea)}
                disableDropdown={false}
                disableIconTap={false} />
            </SectionContainer>
            {
              (this.props.getConfigurationRapidEZ.rapid_ez_availability)?
                <SectionContainer
                  refName={'dataSachetReload'}
                  hideView={this.getIndividualItemHideStatus('dataSachetReload')}>
                  <MaterialInput
                    label={this.state.locals.labelDataSachetReload}
                    index={'dataSachetReload'}
                    ref={'dataSachetReload'}
                    value={this.props.reloadAmount !== '' ?
                      `${this.state.locals.rs_label} ${this.props.reload.offer_val.CHARGE}` : ''}
                    customStyle={styles.materialInputCustomStyle}
                    onIconPress={() => this.loadDataSachetOffers()}
                    icon={Constants.icon.edit}
                    hideRightIcon={false}
                    editable={false}
                    tapToFocus={false}
                    disableIconTap={!this.props.sectionProductInfo_validation_status} />
                </SectionContainer>
                :
                null
            }
          </MainView>
        </InputSection>
        {/* PRODUCT INFORMATION SECTION END */}
        {/* CUSTOMER INFORMATION SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.CUSTOMER_INFORMATION}
          sectionId={Constants.MAIN_SECTION.CUSTOMER_INFORMATION}
          customStyle={styles.customerInformationInputSection}
          label={this.state.locals.sectionCustomerInformation}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.CUSTOMER_INFORMATION)}
          validationStatus={this.props.sectionCustomerInfo_validation_status} 
          isFocused={this.props.section_3_visible} >
          <MainView
            formData={this.props}
            key={this.state + 3}
            style={styles.mainSectionContainer}
            visible={this.props.section_3_visible}
            removeWhenHidden
            noAnimation>
            {/* CUSTOMER INFORMATION RADIO BUTTONS START */}
            {renderCxInfoRadioButtonSection()}
            {/* CUSTOMER INFORMATION RADIO BUTTONS END */}
            {/* CUSTOMER INFORMATION IMAGE CAPTURE FIELDS START */}
            {renderCxInfoImageCaptureFields()}
            {/* CUSTOMER INFORMATION IMAGE CAPTURE FIELDS END */}
            <SectionContainer
              refName={'customerMobile'}
              hideView={this.getIndividualItemHideStatus('customerMobile')}>
              <MaterialInput
                label={this.state.locals.labelCustomerMobile}
                index={'customerMobile'}
                ref={'customerMobile'}
                value={this.props.customerMobile}
                keyboardType={'numeric'}
                hideRightIcon
                error={this.state.mobileError}
                maxLength={globalConfig.MOBILE_NUMBER_MAX_LENGTH}
                customStyle={styles.materialInputCustomStyle}
                onChangeText={(text) => this.onChangeTextCustomerMobile(text)}
                onIconPress={() => {
                  console.log('## onIconPress #customerMobile');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.customerMobile == ''} />
            </SectionContainer>
            <SectionContainer
              refName={'customerLandline'}
              hideView={this.getIndividualItemHideStatus('customerLandline')}>
              <MaterialInput
                label={this.state.locals.labelCustomerLandline}
                index={'customerLandline'}
                ref={'customerLandline'}
                keyboardType={'numeric'}
                value={this.props.customerLandline}
                hideRightIcon
                error={this.state.landlineError}
                customStyle={styles.materialInputCustomStyle}
                maxLength={globalConfig.MOBILE_NUMBER_MAX_LENGTH}
                onChangeText={(text) => this.onChangeTextCustomerLandline(text)}
                onIconPress={() => {
                  console.log('## onIconPress #customerLandline');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.customerLandline == ''} />
            </SectionContainer>
            <SectionContainer
              refName={'email'}
              hideView={this.getIndividualItemHideStatus('email')}>
              <MaterialInput
                label={this.state.locals.labelCustomerEmail}
                index={'email'}
                value={this.props.email_input_value}
                ref={'email'}
                onChangeText={(text) => this.onChangeTextEmail(text)}
                error={this.state.emailError}
                hideRightIcon
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => {
                  console.log('## onIconPress #email');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.email_input_value == ''} />
            </SectionContainer>
            {/* SIGNATURE BUTTON START */}
            <SectionContainer>
              <TouchableOpacity
                style={styles.signatureContainer}
                onPress={() => this.onCaptureSignature(totalPaymentProps, eZCashTotalPaymentProps)}
              >
                {this.props.common.kyc_signature !== null
                  ? <Image
                    style={styles.signatureImage}
                    key={this.props.common.kyc_signature.signatureRand}
                    source={{
                      uri: `data:image/jpg;base64,${this.props.common.kyc_signature.signatureBase64}`
                    }}
                    resizeMode="stretch"
                  />
                  :
                  <Text style={styles.signatureTxt}>
                    {this.state.locals.customerSignature}
                  </Text>
                }
              </TouchableOpacity>
            </SectionContainer>
            {/* SIGNATURE BUTTON END */}
          </MainView>
        </InputSection>
        {/* TOTAL PAYMENTS SECTION START */}
        <View>
          <SectionContainer
            refName={'totalPayment'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('totalPayment')}>
            <TotalPayments totalPaymentProps={totalPaymentProps} />
          </SectionContainer>
        </View>
        {/* TOTAL PAYMENTS SECTION END */}
        {/* EzCash TOTAL PAYMENTS SECTION START */}
        <View>
          <SectionContainer
            refName={'ezCashtotalPayment'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('ezCashtotalPayment')}>
            <EzCashPayment eZCashTotalPaymentProps={eZCashTotalPaymentProps} />
          </SectionContainer>
        </View>
        {/* EzCash TOTAL PAYMENTS SECTION END */}
        {/* EZ CASH INPUT START */}
        <SectionContainer
          refName={'ezCashPin'}
          customMarginLeft={{ marginLeft: 50 }}
          hideView={this.getIndividualItemHideStatus('ezCashPin')}
          customStyle={styles.ezSectionCustomStyle}>
          <MaterialInput
            label={this.state.locals.ezCashPin}
            index={'ezCashPin'}
            ref={'ezCashPin'}
            title={this.props.configuration.default_ez_cash_account === '' ?
              '' : `(${this.props.configuration.default_ez_cash_account} ${this.state.locals.ez_cash_pin_title})`}
            secureTextEntry
            maxLength={globalConfig.EZ_CASH_PIN_MAX_LENGTH}
            keyboardType={'numeric'}
            value={this.props.ez_cash_input_value}
            icon={Constants.icon.edit}
            hideRightIcon={this.props.ez_cash_input_value == ''}
            onChangeText={this.onExCashPinTextChange}
            customStyle={styles.materialInputCustomStyle}
            onIconPress={() => {
              console.log('## onIconPress #ezCashPin');
            }}
          />
        </SectionContainer>
        {/* EZ CASH INPUT END */}
        {/* EZ CASH BALANCE */}
        <View>
          <SectionContainer
            refName={'ezCashBalance'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('ezCashBalance')}>
            <EzCashBalance
              ezCashAccountBalance={this.props.ezCashAccountBalance}
              rs_label={this.state.locals.rs_label}
              title={this.state.locals.ez_cash_account_balance} />
          </SectionContainer>
        </View>
        {/* EZ CASH BALANCE END*/}
        {/* FINAL ACTIVATION BUTTON */}
        <View style={styles.activateBtnContainer}>
          <View style={styles.activateDummy} />
          <TouchableOpacity
            style={[styles.activateBtn, {
              backgroundColor: this.props.activateButtonColorStatus ?
                Colors.btnActive : Colors.btnDisable
            }]}
            onPress={() => this.debouncedFinalActivation()}
            disabled={!(this.props.sectionId_validation_status
              && this.props.sectionProductInfo_validation_status)}>
            <Text style={[styles.activateBtnTxt, {
              color: this.props.activateButtonColorStatus ?
                Colors.btnActiveTxtColor : Colors.btnDisableTxtColor
            }]} >
              {this.state.locals.txtActivate}
            </Text>
          </TouchableOpacity>
        </View>
        {/* FINAL ACTIVATION BUTTON END */}
      </View>
    );
  }
}

//Image capture in Customer Information section Local customer or Foreign customer OTP verified
const CustomerOtpVerified = ({ currentView }) => (
  <View>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.addressDifferentFromOtpVerifiedConnectionPre : currentView.state.locals.addressDifferentFromOtpVerifiedConnectionPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('NIC OR PP - POB capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('NIC OR PP - Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//NIC image capture in Customer Information section
const CustomerInfoNIC = ({ currentView }) => (
  <View>
    <SectionContainer>
      <CameraInput
        captureFront={() => {
          console.log('Capture NIC Front');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT);
        }}
        captureBack={() => {
          console.log('Capture NIC Back');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.NIC_BACK);
        }}
        titleLabel={currentView.state.locals.captureNIC}
        icon={(currentView.props.common.kyc_nic_front === null
          && currentView.props.common.kyc_nic_back === null) ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
        frontAndBack={currentView.props.common.kyc_nic_front != null && currentView.props.common.kyc_nic_back != null} />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.addressDifferentFromNIC : currentView.state.locals.addressDifferentFromNICPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('NIC POB capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.state.locals.customerFaceNotClearNIC}
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE, !currentView.props.opt_customerFaceNotClear);
        }}
        isChecked={currentView.props.opt_customerFaceNotClear}
        cameraCaptureLabel={currentView.state.locals.captureCustomerPhoto}
        onCapture={() => {
          console.log('NIC Customer photo capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE);
        }}
        captureIcon={currentView.props.common.kyc_customer_image === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('NIC Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//Driving License image capture in Customer Information section
const CustomerInfoDrivingLicense = ({ currentView }) => (
  <View>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Driving Licence');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE);
        }}
        titleLabel={currentView.state.locals.captureDrivingLicense}
        icon={currentView.props.common.kyc_driving_licence === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.addressDifferentFromDL : currentView.state.locals.addressDifferentFromDLPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('DL Capture POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.state.locals.customerFaceNotClearDL}
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE, !currentView.props.opt_customerFaceNotClear);
        }}
        isChecked={currentView.props.opt_customerFaceNotClear}
        cameraCaptureLabel={currentView.state.locals.captureCustomerPhoto}
        onCapture={() => {
          console.log('DL Capture Customer Photo');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE);
        }}
        captureIcon={currentView.props.common.kyc_customer_image === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('DL Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//Passport image capture in Customer Information section
const CustomerInfoPassport = ({ currentView }) => (
  <View>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Passport Front');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PASSPORT);
        }}
        titleLabel={currentView.state.locals.capturePassport}
        icon={currentView.props.common.kyc_passport === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Address - Passport');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        titleLabel={currentView.state.locals.captureAddress}
        icon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.isPrepaid() ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.lteSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('PP Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

const ElementCheckBox = ({ checkBoxText, onClick, isChecked, isHidden, disabled = false }) => {
  if (isHidden == true) {
    return (<View />);
  } else {
    return (
      <TouchableOpacity style={styles.checkBoxContainer} onPress={onClick} disabled={disabled}>
        <View style={styles.checkBoxView}>
          <CheckBox
            checkBoxColor={isChecked ? Colors.yellow : Colors.colorGrey}
            uncheckedCheckBoxColor={Colors.colorGrey}
            style={styles.checkBoxStyle}
            onClick={onClick}
            disabled={disabled}
            isChecked={isChecked} />
        </View>
        <View style={styles.checkBoxTextView}>
          <Text style={styles.checkBoxText}>
            {checkBoxText}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
};

const CheckBoxWithCameraInput = ({ checkBoxText, onClick, isChecked, cameraCaptureLabel, onCapture, captureIcon = Constants.icon.camera }) => (
  <View style={styles.cameraInputCheckBoxContainerMain}>
    <View style={styles.cameraInputBoxContainer}>
      <View style={styles.cameraInputCheckBoxView}>
        <CheckBox
          checkBoxColor={isChecked ? Colors.yellow : Colors.colorGrey}
          uncheckedCheckBoxColor={Colors.colorGrey}
          style={styles.cameraInputCheckBoxStyle}
          onClick={onClick} isChecked={isChecked} />
      </View>
      <View style={styles.cameraInputCheckBoxTextView}>
        <Text style={styles.cameraInputCheckBoxText}>
          {checkBoxText}
        </Text>
      </View>
    </View>
    {isChecked ?
      <View style={styles.cameraInputBoxContainer}>
        <View style={styles.cameraInputCheckBoxView} />
        <View style={styles.cameraInputCheckBoxTextView}>
          <CameraInput
            captureFront={onCapture}
            titleLabel={cameraCaptureLabel}
            icon={captureIcon} />
        </View>
      </View>
      : true}
  </View>
);


const mapStateToProps = state => {
  console.log('****** REDUX STATE :: LTE => LteMainViewContainer :: lteActivation \n', state.lteActivation);
  console.log('****** REDUX STATE :: LTE => LteMainViewContainer :: common\n', state.common);
  console.log('****** REDUX STATE :: LTE => LteMainViewContainer :: configuration\n', state.configuration);
  const common = state.common;
  const configuration = state.configuration;
  const Language = state.lang.current_lang;

  const lte_configuration = state.configuration.lte_configuration;
  const { lte_activation_status = false } = configuration;
  const section_1_visible = state.lteActivation.section_1_visible;
  const section_2_visible = state.lteActivation.section_2_visible;
  const section_3_visible = state.lteActivation.section_3_visible;

  const om_campaign_record_no = state.lteActivation.om_campaign_record_no;
  const reservation_no = state.lteActivation.reservation_no;
  const reference_no = state.lteActivation.reference_no;
  const customerType = state.lteActivation.customerType;
  const customerExistence = state.lteActivation.customerExistence;
  const customerOtpStatus = state.lteActivation.customerOtpStatus;
  const customerIdType = state.lteActivation.customerIdType;
  const customerInfoSectionIdType = state.lteActivation.customerInfoSectionIdType;
  const customerInfoSectionIdTypeCode = state.lteActivation.customerInfoSectionIdTypeCode;
  const idNumber_input_value = state.lteActivation.idNumber_input_value;
  const id_input_error = state.lteActivation.id_input_error;

  const ez_cash_input_value = state.lteActivation.ez_cash_input_value;

  const idValidationData = state.lteActivation.idValidationData;
  const idValidationStatus = state.lteActivation.idValidationStatus;

  const otpValidationData = state.lteActivation.otpValidationData;
  const otpValidationStatus = state.lteActivation.otpValidationStatus;
  const cx_otp_number = state.lteActivation.cx_otp_number;

  const bundleSerialValue = state.lteActivation.bundleSerialValue;
  const serialPackValue = state.lteActivation.serialPackValue;
  const simSerialValue = state.lteActivation.simSerialValue;
  const analogPhoneSerialValue = state.lteActivation.analogPhoneSerialValue;

  const bundleSerialData = state.lteActivation.bundleSerialData;
  const serialPackData = state.lteActivation.serialPackData;
  const simSerialData = state.lteActivation.simSerialData;
  const analogPhoneSerialData = state.lteActivation.analogPhoneSerialData;

  const allSerialData = state.lteActivation.allSerialData;

  const reloadAmount = state.lteActivation.reloadAmount;
  const reloadOfferCode = state.lteActivation.reloadOfferCode;
  const reload = state.lteActivation.reload;

  const customer_id_info = state.lteActivation.customer_id_info;
  const sectionId_validation_status = state.lteActivation.sectionId_validation_status;
  const sectionProductInfo_validation_status = state.lteActivation.sectionProductInfo_validation_status;
  const sectionCustomerInfo_validation_status = state.lteActivation.sectionCustomerInfo_validation_status;
  const selected_connection_type = state.lteActivation.selected_connection_type;

  const availableLTEOffersList = state.lteActivation.availableLTEOffersList;
  const multiplayOfferData = state.lteActivation.multiplayOfferData;
  const availableLTEDataPackagesList = state.lteActivation.availableLTEDataPackagesList;
  const availableLTEVoicePackagesList = state.lteActivation.availableLTEVoicePackagesList;
  const voucherCodeAvailable = state.lteActivation.voucherCodeAvailable;
  const enteredVoucherCode = state.lteActivation.enteredVoucherCode;
  const isValidVoucherCode = state.lteActivation.isValidVoucherCode;
  const telcoAreaData = state.lteActivation.telcoAreaData;
  const lteSelectedArea = state.lteActivation.lteSelectedArea;
  const lte_deposit_value = state.lteActivation.lte_deposit_value;
  const lteVoiceCheckboxData = state.lteActivation.lteVoiceCheckboxData;
  const ezCashreverseTransaction = state.lteActivation.ezCashreverseTransaction;
  const ezCashSubmitTransaction = state.lteActivation.ezCashSubmitTransaction;
  const ezCashCheckBalance = state.lteActivation.ezCashCheckBalance;
  const email_input_value = state.lteActivation.email_input_value;
  const lte_Device_Selling_Price = state.lteActivation.lteDeviceSellingPrice;
  const customerLandline = state.lteActivation.lte_set_input_landLineNumber;
  const customerMobile = state.lteActivation.lte_set_input_MobileNumber;
  const customerOutstandingData = state.lteActivation.customerOutstandingData;
  const lte_billing_cycle_number = state.lteActivation.lte_billing_cycle_number;
  const availableVaoucherCodeValue = state.lteActivation.availableVaoucherCodeValue;
  const validatedVoucherCode = state.lteActivation.validatedVoucherCode;

  const selected_connection_number = state.lteActivation.selected_connection_number;
  const reserved_number_list = state.lteActivation.reserved_number_list;

  const ezCashAccountBalance = state.lteActivation.ezCashAccountBalance;


  const opt_addressDifferent = state.lteActivation.opt_addressDifferent;
  const opt_customerFaceNotClear = state.lteActivation.opt_customerFaceNotClear;
  const opt_installationAddressDeferent = state.lteActivation.opt_installationAddressDeferent;

  const validateMobileNumber = customerMobile !== null
    && customerMobile !== undefined ? customerMobile : '';


  const lte_total_payment_amount = state.lteActivation.lte_total_payment_amount;
  const eZCashInfo = state.lteActivation.eZCashInfo;

  let selected_serial_type = Constants.INDIVIDUAL_SERIAL;
  try {
    selected_serial_type = state.lteActivation.selected_serial_type == null ?  
      lte_configuration.serial_type.selected : state.lteActivation.selected_serial_type;
  } catch (error) {
    selected_serial_type = Constants.INDIVIDUAL_SERIAL;
  }

  const serial_scan_mode = selected_serial_type;
  const voucher_group_name = state.lteActivation.voucher_group_name;
  const analog_phone_visibility = state.lteActivation.analog_phone_visibility;
  const activity_start_time = state.configuration.activity_start_time;
  const is_voucher_cancelled = state.lteActivation.is_voucher_cancelled;
  let allPaymentData = PaymentCalculations.getPaymentDataFromRedux(state.lteActivation);

  const data_totalPaymentProps = allPaymentData.totalPaymentProps;
  const data_eZCashTotalPaymentProps = allPaymentData.eZCashTotalPaymentProps;
  const voucher_code_validity_offer_code = state.lteActivation.voucher_code_validity_offer_code;
  const same_voucher_code_validate = state.lteActivation.same_voucher_code_validate;
  const getConfiguration  = state.auth.getConfiguration.config;
  const getConfigurationRapidEZ  = state.auth.getConfiguration.rapid_ez_account; 
  const tcUrls = FuncUtils.getValueSafe(state.auth.getConfiguration).agreement_document;

  let { enable_dealer_collection = '' } = getConfiguration;
  let { enable_reload = '' } = getConfiguration;
  
  let didSerialValidated = bundleSerialData !== null
    || (serialPackData !== null && simSerialData !== null);


  let disableField_VoucherCode = true;

  if (voucherCodeAvailable) {
    disableField_VoucherCode = !(enteredVoucherCode !== '' && isValidVoucherCode);
    didSerialValidated = didSerialValidated && isValidVoucherCode && enteredVoucherCode !== '';
  } else {
    disableField_VoucherCode = false;
  }

  console.log('######################## Activate button status ####################');
  //Activate button status - start logic
  let activateButtonStatus;
  let activateButtonColorStatus;

  //Check image capture
  if (customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
    || customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE) {
    console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE');
    if (customerType === Constants.CX_TYPE_LOCAL) {
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL');
      if (customerInfoSectionIdType === Constants.CX_INFO_TYPE_NIC) {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_NIC');
        activateButtonStatus = common.kyc_nic_front !== null && common.kyc_nic_back !== null;
        activateButtonColorStatus = activateButtonStatus;
      } else if (customerInfoSectionIdType === Constants.CX_INFO_TYPE_DRIVING_LICENCE) {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_DRIVING_LICENCE');
        activateButtonStatus = common.kyc_driving_licence !== null;
        activateButtonColorStatus = activateButtonStatus;
      } else {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_PASSPORT');
        activateButtonStatus = common.kyc_passport !== null && common.kyc_proof_of_billing !== null;
        activateButtonColorStatus = activateButtonStatus;
      }
    } else {
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_FOREIGNER');
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_FOREIGNER >> CX_INFO_TYPE_PASSPORT');
      activateButtonStatus = common.kyc_passport !== null && common.kyc_proof_of_billing !== null;
      activateButtonColorStatus = activateButtonStatus;
    }

  } else if (customerOtpStatus == Constants.CX_OTP_VERIFIED) {
    console.log('CX_TYPE_FOREIGNER_OR_LOCAL >> CX_OTP_VERIFIED');
    activateButtonStatus = true;
    activateButtonColorStatus = true;
  } else {
    console.log('CX_INVALID');
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check additional images
  if (opt_addressDifferent && common.kyc_proof_of_billing === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }
  if (opt_customerFaceNotClear && common.kyc_customer_image === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }
  if (opt_installationAddressDeferent && common.kyc_additional_pob === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check analog phone serial if analog phone checked
  if (lteVoiceCheckboxData.analogPhoneChecked && analogPhoneSerialValue === '') {
    activateButtonStatus = false;
    activateButtonColorStatus = false;

  }

  //Check mobile number
  if (!(validateMobileNumber !== '' && Utill.mobileNumberValidateAny(validateMobileNumber))) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check signature capture
  if (common.kyc_signature === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay ', data_eZCashTotalPaymentProps.eZCashtotalPay);
  console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay type ', typeof data_eZCashTotalPaymentProps.eZCashtotalPay);
  console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay 1 ', parseInt(data_eZCashTotalPaymentProps.eZCashtotalPay) != 0);
  console.log('#### data_eZCashTotalPaymentProps.eZCashtotalPay 2 ', toString(data_eZCashTotalPaymentProps.eZCashtotalPay) != "");
  console.log('#### ez_cash_input_value 3 ', ez_cash_input_value.length != 4);
  
  //Check eZ cash PIN
  if (parseInt(data_eZCashTotalPaymentProps.eZCashtotalPay) != 0
    && toString(data_eZCashTotalPaymentProps.eZCashtotalPay) != ""
    && ez_cash_input_value.length != 4) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Activate button status - end logic
  console.log('##################################################################');

  console.log('#### activity_start_time', activity_start_time);
  console.log('#### activateButtonStatus', activateButtonStatus);
  console.log('#### activateButtonColorStatus', activateButtonColorStatus);
  console.log('#### disableField_VoucherCode', disableField_VoucherCode);
  console.log('#### didSerialValidated', didSerialValidated);
  console.log('#### allPaymentData', allPaymentData);
  console.log('#### customerOutstandingData', customerOutstandingData);

  console.log('############# END OF MAP_TO_STATE_PROPS  #########################');

  return {
    common,
    configuration,
    Language,
    lte_configuration,
    activateButtonStatus,
    activateButtonColorStatus,
    section_1_visible,
    section_2_visible,
    section_3_visible,
    customerType,
    customerExistence,
    customerOtpStatus,
    customerIdType,
    customerInfoSectionIdType,
    customerInfoSectionIdTypeCode,
    selected_connection_type,
    customer_id_info,
    idNumber_input_value,
    id_input_error,
    ez_cash_input_value,
    idValidationData,
    idValidationStatus,
    otpValidationData,
    otpValidationStatus,
    cx_otp_number,
    reload,
    reloadAmount,
    reloadOfferCode,
    serial_scan_mode,
    selected_serial_type,
    bundleSerialValue,
    serialPackValue,
    simSerialValue,
    analogPhoneSerialValue,
    bundleSerialData,
    serialPackData,
    simSerialData,
    analogPhoneSerialData,
    allSerialData,
    availableLTEOffersList,
    multiplayOfferData,
    sectionId_validation_status,
    sectionProductInfo_validation_status,
    sectionCustomerInfo_validation_status,
    availableLTEVoicePackagesList,
    availableLTEDataPackagesList,
    disableField_VoucherCode,
    voucherCodeAvailable,
    enteredVoucherCode,
    isValidVoucherCode,
    telcoAreaData,
    lteSelectedArea,
    lte_deposit_value,
    lteVoiceCheckboxData,
    ezCashreverseTransaction,
    ezCashSubmitTransaction,
    ezCashCheckBalance,
    ezCashAccountBalance,
    email_input_value,
    lte_Device_Selling_Price,
    customerLandline,
    customerMobile,
    customerOutstandingData,
    lte_billing_cycle_number,
    selected_connection_number,
    om_campaign_record_no,
    reservation_no,
    reference_no,
    reserved_number_list,
    opt_addressDifferent,
    opt_customerFaceNotClear,
    opt_installationAddressDeferent,
    validateMobileNumber,
    didSerialValidated,
    lte_total_payment_amount,
    eZCashInfo,
    voucher_group_name,
    activity_start_time,
    availableVaoucherCodeValue,
    data_totalPaymentProps,
    data_eZCashTotalPaymentProps,
    validatedVoucherCode,
    voucher_code_validity_offer_code,
    is_voucher_cancelled,
    same_voucher_code_validate,
    getConfiguration,
    enable_dealer_collection,
    enable_reload,
    getConfigurationRapidEZ,
    lte_activation_status,
    analog_phone_visibility,
    tcUrls
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },

  //main section container
  mainSectionContainer: {
    backgroundColor: Colors.appBackgroundColor
  },

  ezSectionCustomStyle: {
    marginTop: 10
  },

  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    // margin: 10,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
    //backgroundColor: '#808080'
  },

  signatureImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined,
  },

  signatureTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  customerInformationInputSection: {
    marginBottom: 10
  },
  containerRadioWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  materialInputCustomStyle: {
    marginTop: -20
  },

  dropDownInputCustomStyle: {
    marginTop: -20
  },

  checkBoxAddVoicePhoneStyle: {
    marginBottom: -4,
    marginTop: -5
  },
  containerRadioBtn: {
    flex: 1,
    // justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 0,
    marginLeft: -25
  },
  radioBtnTxt: {
    paddingLeft: 5,
    fontWeight: 'bold',
    color: Colors.black
  },
  //Check box
  checkBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 8,
    marginTop: 8,
    // marginLeft:5
  },
  cameraInputBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 8,
  },
  cameraInputCheckBoxContainerMain: {
    flex: 1,
    marginBottom: 8,
    flexDirection: 'column'
  },
  checkBoxView: {
    width: 25,
    alignItems: 'center',
  },
  cameraInputCheckBoxView: {
    flex: 0.1,
    alignItems: 'center',
  },
  checkBoxTextView: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    // marginRight: 5,
    //backgroundColor: 'green'
  },
  cameraInputCheckBoxTextView: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },
  checkBoxStyle: {
    padding: 0,
    paddingBottom: 3,
    // paddingRight: 5
  },
  cameraInputCheckBoxStyle: {
    padding: 0,
    paddingRight: 5,
  },
  cameraInputCheckBoxText: {
    paddingLeft: 5
  },
  checkBoxText: {
    fontSize: 15,
    paddingBottom: 3,
    paddingLeft: 5
  },
  activateBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 10,
    marginTop: 37,
    height: 50,
    //backgroundColor: '#155'
  },
  activateDummy: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
    //backgroundColor: '#185',
  },
  activateBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnDisable

  },
  activateBtnTxt: {
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500',
    color: Colors.btnDisableTxtColor,
  }
});


export default connect(mapStateToProps, actions)(LteMainViewContainer);