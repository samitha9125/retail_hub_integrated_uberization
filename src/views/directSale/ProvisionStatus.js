import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  Dimensions,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';

import Styles from '../../config/styles';
import Colors from '../../config/colors';
import * as actions from '../../actions/index';
import strings from '../../Language/Wom';
import { Header } from '../wom/Header';
import Utills from '../../utills/Utills';
import ActIndicator from './warrentySales/ActIndicator';

const { height } = Dimensions.get('screen');
const Utill = new Utills();
class Provisioning extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      provisionedStatusData: props.reqBody.serial_list,
      requestBody: props.reqBody,
      cir: props.cir,
      btnActive: false,
      ezCashPIN: props.ezCashPIN ? props.ezCashPIN : '',
      paymentDetails: {},
      provisionWait: 0,
      provisionTimeOut: 0,
      overallStatus: props.overallStatus,
      locals: {
        lblProvInprogress: strings.provisionInprogress,
        lblProvComplete: strings.provisionComplete,
        lblProvFailed: strings.provisionFailed,
        btnProvision: strings.provisionButton,
        btnConfirm: strings.btnConfirm,
        api_error_message: strings.api_error_message,
        network_error_message: strings.network_error_message,
        lblSerialNo: strings.lblSerialNo,
        warrantyReplacementCaps: strings.warrantyReplacementCaps,
        accessorymessagebody: strings.accessorymessagebody,
        warrantymessagebody: strings.warrantymessagebody,
        lblError: strings.error,
        lblToBeProv: strings.lblToBeProv,
        lblCIR: strings.lblCIR,
        succeesAlert: strings.lblSuccess,
        lblAlert: strings.alertHeader,
        checkStatusMsg: strings.checkStatusMsg
      },
    };
    this.directSaleOption = props.directSaleOption;
    this.sale_date = props.sale_date;
    this.navigatorOb = this.props.navigator;
    this.provisionAttempts = 1;
    this.statusTimeOut = 1;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.resetTo({
      title: this.state.locals.warrantyReplacementCaps,
      screen: 'DialogRetailerApp.views.DirectSale',
      passProps: {
        screenTitle: this.state.locals.warrantyReplacementCaps
      },
    });
    return true;
  }

  provisionOnPress() {
    if (this.directSaleOption == 'accessorysale') {
      this.doAccessarySale();
    } else if (this.directSaleOption == 'warrantyreplacement') {
      this.doCompleteWarranty();
    }
  }

  confirmOnPress() {
    let messageBody = '';

    if (this.directSaleOption == 'accessorysale') {
      messageBody = this.state.locals.accessorymessagebody;
    } else if (this.directSaleOption == 'warrantyreplacement') {
      messageBody = this.state.locals.warrantymessagebody;
    }

    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: 'successWithOk',
        messageHeader: '',
        messageBody,
        messageFooter: '',
        onPressOK: () => {
          this.navigatorOb.resetTo({
            title: this.state.locals.warrantyReplacementCaps,
            screen: 'DialogRetailerApp.views.DirectSale',
            passProps: {
              screenTitle: this.state.locals.warrantyReplacementCaps,
            },
          });
        }
      }
    });
  }

  refreshOnPress() {
    if (this.state.overallStatus == 'TO_BE_PROVISIONED') {
      this.displayCommonAlert('alertWithOK', this.state.locals.lblAlert, this.state.locals.checkStatusMsg, '')
    } else {
      this.getProvisioningStatus();
    }
  }

  doAccessarySale() {
    let requestBody;
    requestBody = { ...this.state.requestBody, cir: this.state.cir, sale_date: this.sale_date }
    console.log('doAccessarySale1 ', this.props.reqBody);
    console.log('doAccessarySale2 ', requestBody);
    console.log('doAccessarySale3 ', this.state.requestBody);

    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('NewAccessorySale', 'AccessorySale', 'cfss', requestBody,
        (response) => {
          this.setState({ apiLoading: false }, () => {
            this.getProvisioningStatus();
          });
        },
        (response) => {
          this.setState({ apiLoading: false }, () => {
            if (response.data.retry == true) {
              this.displayRetryAlert('failureWithRetry', '', response.data.error, '');
            } else {
              this.displayCommonAlert('error', this.state.locals.lblError, response.data.error ? response.data.error : this.state.locals.api_error_message, '');
            }
          });
        },
        (response) => {
          this.setState({ apiLoading: false }, () => {
            let error;
            if (response == 'No_Network') {
              error = this.state.locals.network_error_message;
              this.showNoNetworkModal(() => this.doAccessarySale());
            } else if (response == 'Network Error') {
              error = this.state.locals.network_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            } else {
              error = this.state.locals.api_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            }
          });
        });
    });
  }

  doCompleteWarranty() {
    let requestBody;
    requestBody = { ...this.state.requestBody, cir: this.state.cir, sale_date: this.sale_date }
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('completeWarranty', 'WarrantyReplacement', 'cfss', requestBody,
        (response) => {
          this.setState({ apiLoading: false }, () => {
            this.getProvisioningStatus();
          });
        },
        (response) => {
          this.setState({ apiLoading: false }, () => {
            if (response.data.retry == true) {
              this.displayRetryAlert('failureWithRetry', '', response.data.error, '');
            } else {
              this.displayCommonAlert('defaultError', this.state.locals.lblError, response.data.error);
            }
          });
        },
        (response) => {
          this.setState({ apiLoading: false }, () => {
            let error;
            if (response == 'No_Network') {
              error = this.state.locals.network_error_message;
              this.showNoNetworkModal(() => this.doCompleteWarranty());
            } else if (response == 'Network Error') {
              error = this.state.locals.network_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            } else {
              error = this.state.locals.api_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            }
          });
        });
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: { retryFunc: retryFunc }
    });
  }

  provisionStatusChecking() {
    const provisionWait = this.state.provisionWait;
    let timeCount = 0;
    this.timer = setInterval(() => {
      timeCount += provisionWait;
      if (this.state.overallStatus == 'PROVISIONING_INPROGRESS') {
        if (this.state.provisionTimeOut == timeCount || this.state.provisionTimeOut < timeCount) {
          clearInterval(this.timer);
        } else {
          this.getProvisioningStatus();
        }
      } else {
        clearInterval(this.timer);
      }
    }, provisionWait);
  }

  getProvisioningStatus() {
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('getProvisioningStatus', 'warrantyReplacement', 'cfss', { cir: this.state.cir },
        (response) => {
          this.setState({
            apiLoading: false,
            overallStatus: response.data.data.app_status,
            paymentDetails: response.data.data.app_data,
            provisionedStatusData: response.data.data.app_data.serials,
          }, () => {
            this.setState({
              provisionWait: parseInt(response.data.prov_config.time_interval) * 1000,
              provisionTimeOut: parseInt(response.data.prov_config.timeout) * 1000,
            }, () => {
              this.statusTimeOut++;
              this.statusTimeOut == 1 ? this.provisionStatusChecking() : true;
            }
            );
          });
        }, (response) => {
          this.setState({ apiLoading: false, overallStatus: 'PROVISIONING_FAILED' },
            () => this.displayCommonAlert('defaultAlert', this.state.locals.lblError, response.data.error, ''));
        }, (response) => {
          this.setState({ apiLoading: false }, () => {
            let error;
            if (response == 'Network Error') {
              error = this.state.locals.network_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            } else {
              error = this.state.locals.api_error_message;
              this.navigatorOb.showSnackbar({ text: error });
            }
          });
        });
    });
  }

  displayRetryAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter
      },
      onPressRetry: () => {
        this.provisionOnPress();
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { }
      }
    });
  }

  renderStatus = (status) => {
    switch (status) {
      case 'PROVISIONING_COMPLETED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtSuccess}>
              {this.state.locals.lblProvComplete}
            </Text>
          </View>
        );

      case 'PROVISIONING_INPROGRESS':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblProvInprogress}
            </Text>
          </View>
        );

      case 'PROVISIONING_FAILED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtFailed}>
              {this.state.locals.lblProvFailed}
            </Text>
          </View>
        );

      case 'TO_BE_PROVISIONED':
        return (
          <View style={styles.cardTopSection3}>
            <Text style={styles.txtPending}>
              {this.state.locals.lblToBeProv}
            </Text>
          </View>
        );

      default:
        return true;
    }
  }

  renderListItem = ({ item, index }) => {
    return (
      <View
        key={index}
        style={styles.cardView}>
        <View style={styles.cardTop}>
          <View style={styles.cardTopSection1}>
            <Text style={styles.txtTitle}>
              {item.name}
            </Text>
          </View>
          <View style={{ justifyContent: 'flex-end', paddingRight: 10 }}>
            {this.renderStatus(item.status)}
          </View>
        </View>
        <View style={styles.cardBottom}>
          <Text style={styles.txtContent}>
            {this.state.locals.lblSerialNo} : {item.serial}
          </Text>
          <Text style={styles.txtContent}>
            {this.state.locals.lblCIR} : {this.state.cir}
          </Text>
        </View>
      </View>
    );
  };

  renderButton() {
    if (this.state.overallStatus == 'PROVISIONING_COMPLETED') {
      return (
        <TouchableOpacity style={styles.activeBtn} onPress={() => this.confirmOnPress()}>
          <Text style={styles.btnText}>{this.state.locals.btnConfirm}</Text>
        </TouchableOpacity>
      )
    } else if (this.state.overallStatus == 'TO_BE_PROVISIONED') {
      return (
        <TouchableOpacity style={styles.activeBtn} onPress={() => this.provisionOnPress()}>
          <Text style={styles.btnText}>{this.state.locals.btnProvision}</Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity style={styles.disableBtn} disabled={true}>
          <Text style={styles.btnText}>{this.state.locals.btnConfirm}</Text>
        </TouchableOpacity>
      )
    }
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          refreshPressed={() => this.refreshOnPress()}
          displayRefresh={true}
          headerText={this.props.screenTitle} />
        {this.showIndicator()}
        <View style={styles.contentWrapper}>
          <FlatList
            data={this.state.provisionedStatusData}
            keyExtractor={(x, i) => i.toString()}
            renderItem={this.renderListItem}
            scrollEnabled={true}
            extraData={this.state}
          />
        </View>
        <View style={styles.btnRow}>
          {this.renderButton()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    backgroundColor: Colors.white, flex: 1, paddingTop: 47
  },
  serialText: {
    flex: 2, marginHorizontal: 15, fontSize: 15
  },
  itemName: {
    flex: 1, fontSize: 16, color: 'black', fontWeight: "bold"
  },
  container: {
    flex: 1,
  },
  btnRow: {
    flex: 1, height: 67, flexDirection: "row", backgroundColor: Colors.white, flexDirection: 'row-reverse'    
  },
  activeBtn: {
    height: 40,
    borderRadius: 5,
    backgroundColor: Colors.btnActive,
    justifyContent: "center",
    marginVertical: 15,
    width: 105,
    marginRight: 25
  },
  disableBtn: {
    height: 40,
    borderRadius: 5,
    backgroundColor: Colors.btnDisable,
    justifyContent: "center",
    marginVertical: 15,
    width: 105,
    marginRight: 25
  },
  btnText: {
    textAlign: 'center', color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.btnActiveTxtColor,
  },
  provisionCard: {
    shadowOpacity: 0.3, height: 100, margin: 7, borderRadius: 5,
  },
  topRow: {
    flexDirection: 'row', marginHorizontal: 15, marginTop: 15, flex: 2
  },
  cardView: {
    flex: 1,
    backgroundColor: Colors.white,
    height: 116,
    margin: 5,
    marginVertical: 10,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
    width: Dimensions.get('window').width - 10,
  },
  cardTop: {
    flex: 0.4,
    flexDirection: 'row',
    marginTop: 10,
  },
  cardBottom: {
    flex: 0.6,
    justifyContent: 'space-between',
    marginLeft: 7,
    marginBottom: 10,
    borderColor: Colors.lightGrey
  },
  cardTopSection1: {
    flex: 1,
    marginTop: 12,
    marginLeft: 2,
    flexDirection: "column",
  },
  cardTopSection3: {
    flex: 1.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  txtTitle: {
    fontSize: 18,
    marginLeft: 7,
    color: Colors.black,
  },
  txtContent: {
    fontSize: 16,
    color: Colors.black,
  },
  txtPending: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorDarkOrange,
  },
  txtSuccess: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorGreen,
  },
  txtFailed: {
    fontSize: 16,
    marginLeft: 7,
    color: Colors.colorRed,
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const warrantyReplacement = state.warrantyReplacement;
  return { Language, warrantyReplacement };
};

export default connect(mapStateToProps, actions)(Provisioning);