
/*
Developer ---- Bhagya Rathnayake
Company ---- Omobio (pvt) LTD.
*/

import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
class TextFieldComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            customStyle,
            label,
            onChangeText,
            editable,
            keyboardType,
            maxLength,
            secureTextEntry,
            isFocused,
            value,
            lineWidth,
            onSubmitEditing,
            onBlur
        } = this.props;

        return (
            <View style={[styles.container, customStyle]}>
                <View style={styles.innerContainer}>
                    <View style={styles.textFieldStyle}>
                        <TextField
                            title={this.props.title}
                            label={label}
                            value={value}
                            editable={editable}
                            ref={(refVal) => {
                                refTextInput = refVal;
                                isFocused && refVal !== null
                                    ? refVal.focus()
                                    : true;
                            }}
                            onSubmitEditing={onSubmitEditing}
                            onBlur={onBlur}
                            secureTextEntry={secureTextEntry}
                            onChangeText={onChangeText}
                            keyboardType={keyboardType}
                            maxLength={maxLength}
                            lineWidth={lineWidth} />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const someValue = 'someValue';
    return { someValue };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 8,
        marginRight: 8,
        width: Dimensions
            .get('window')
            .width * 0.9,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    innerContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    textFieldStyle: {
        flex: 9,
        justifyContent: 'flex-start'
    }
});

export default connect(mapStateToProps, actions)(TextFieldComponent);