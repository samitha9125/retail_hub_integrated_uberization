import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Colors from '../../../config/colors';
import AccessorySalePage1 from '../../../views/directSale/accessorySales/AccessorySalePage1';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
class AccessorySales extends Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.topContainer}></View>
        <View style={styles.middleContainer}>
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            <View style={styles.container}>
              <AccessorySalePage1 navigator={navigator} />
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => MainPage ', JSON.stringify(state.delivery));
  const sample = state.delivery.workOrderState;
  return { sample }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  topContainer: {
    flex: 0.05,
  },
  middleContainer: {
    flex: 0.8,
  },
  
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  
  
});

export default connect(mapStateToProps, actions)(AccessorySales);