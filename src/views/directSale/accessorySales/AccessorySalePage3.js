import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, BackHandler } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

import Colors from './../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/AccessorySales';
import strings2 from '../../../Language/Wom';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
import ActIndicator from './ActIndicator';
import TextFieldComponent from '../../wom/components/TextFieldComponent';

const Utill = new Utills();
class AccessorySalesDetailsPage extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    strings2.setLanguage(this.props.lang);
    this.state = {
      data: this.props.data,
      ezCashPIN: '',
      persistenceData: {},
      locale: {
        api_error_message: strings.api_error_message,
        nicNo: strings.NICNoPreview,
        connectionNo: strings.connectionNo,
        contactNo: strings.contactNo,
        contactNoAlt: strings.contactNoAlt,
        accessoryType: strings.accessoryType,
        accessorySerial: strings.serial,
        offer: strings.offer,
        offerPrice: strings.offerPrice,
        warrantyPeriod: strings.warrantyPeriod,
        RCUwarrantyPeriod: strings.RCUwarrantyPeriod,
        PCUwarrantyPeriod: strings.PCUwarrantyPeriod,
        confirmation: strings.confirmation,
        confirm: strings.confirm,
        ezCashMsg: strings.ezCashMsg,
        aSaleHeader: strings.aSaleHeader,
        ezPin: strings.ezPin,
        stbSerial: strings.stbSerial,
        stbWarrenty: strings.stbWarrenty,
        ezCashPINText: strings.ezCashPINText,
        succeesAlert: strings2.lblSuccess,
        lblError: strings2.error,
        msgInvalidEzCash: strings.msgInvalidEzCash
      }
    };
    this.navigatorOb = this.props.navigator;
    this.retry_attempt = 1;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.getPersistData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.navigatorOb.pop();
    return true;
  }

  getPersistData = async () => {
    let persistenceData = await Utill.getPersistenceData();
    await this.setState({ persistenceData: persistenceData });
  }

  ezCashPinInput = (value) => this.setState({ ezCashPIN: value });

  newAccessorySale() {
    if (this.isValidEzCashPIN()) {

      this.setState({ apiLoading: true }, () => {
        let retry_attempt = this.retry_attempt;

        let data = {
          item_type: this.state.data.accessoryType,
          NIC: this.state.data.NIC,
          ConnectionNo: this.state.data.connectionNo,
          ContactNo: this.state.data.contactNo,
          retry_attempt: retry_attempt,
          SaleType: this.state.data.accessoryType,
          Offer: this.state.data.offer.offerCode ? this.state.data.offer.offerCode : '',
          offerPrice: this.state.data.offer.prod_PRICE_WITH_TAX ? this.state.data.offer.prod_PRICE_WITH_TAX : this.state.data.offer.prod_price_with_tax ? this.state.data.offer.prod_price_with_tax : '',
          offerData: this.state.data.offer ? this.state.data.offer : '',
          EzCashPin: this.state.ezCashPIN,
          contactNumberAlt: this.state.data.altNo,
          conn_type: this.state.data.connectionType,
          version_upgrade: 1
        };

        Array.isArray(this.state.data.serial) ? data.serial_list = this.state.data.serial : data.serial_list = [this.state.data.serial];

        this.props.isSTB ? data.replaceble_item = this.state.data.replaceble_item : true;

        Utill.apiRequestPost('NewAccessorySale', 'AccessorySale', 'cfss', data,
          (response) => {
            this.setState({ apiLoading: false }, () => {
              if (response.data.warning_msg == true) {
                this.navigatorOb.push({
                  title: 'IN-PROGRESS WORKORDER',
                  screen: 'DialogRetailerApp.views.CommonAlertModel',
                  passProps: {
                    messageType: 'defaultAlert',
                    messageHeader: '',
                    messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : '',
                    messageFooter: '',
                    onPressOK: () => {
                      if (response.data.prov_status == 'to_be_provision') {
                        const neWSerialArray = data.serial_list.map(item => {
                          const obj = { ...item, status: 'TO_BE_PROVISIONED' }
                          return obj;
                        });

                        data.serial_list = neWSerialArray;
                        this.gotoProvPage2(response, data)
                      } else {
                        this.goToDirectSaleMain();
                      }
                    }
                  }
                });
              } else if (response.data.prov_status == 'to_be_provision') {

                const neWSerialArray = data.serial_list.map(item => {
                  const obj = { ...item, status: 'TO_BE_PROVISIONED' }
                  return obj;
                });

                data.serial_list = neWSerialArray;

                this.gotoProvPage2(response, data);
              } else {
                this.displayCommonAlert('successWithOk', this.state.locale.succeesAlert, response.info ? response.info : response.data.info ? response.data.info : '', '');
              }
            });
          },
          (response) => {
            this.setState({ apiLoading: false }, () => {
              if (response.data.retry == true) {
                retry_attempt += 1;
                this.retry_attempt = retry_attempt;
                this.displayRetryConsumptionAlert('failureWithRetry', '', response.data.error, '');
              } else {
                this.displayCommonAlert('error', this.state.locale.lblError, response.data.error ? response.data.error : this.state.locale.api_error_message, '');
              }
            });
          },
          (response) => {
            this.setState({ apiLoading: false }, () => {
              this.displayCommonAlert('error', this.state.locale.lblError, response, '');
            });
          });
      });
    }
  }

  displayRetryConsumptionAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressRetry: () => { this.newAccessorySale(); }
      },
      overrideBackPress: true
    });
  }

  goToDirectSaleMain() {
    this.navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.DirectSale',
      overrideBackPress: true
    });
  }

  gotoProvPage2 = (response, data) => {
    this.navigatorOb.push({
      title: this.state.locale.aSaleHeader,
      screen: 'DialogRetailerApp.views.WarrentyProvisioning',
      passProps: {
        directSaleOption: 'accessorysale',
        overallStatus: 'TO_BE_PROVISIONED',
        reqBody: data,
        ezCashPIN: this.state.ezCashPIN,
        cir: response.data.cir,
        sale_date: response.data.sale_date,
        screenTitle: this.state.locale.aSaleHeader
      }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: this.state.locale.aSaleHeader,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => {
          if (messageType == 'successWithOk') {
            this.goToDirectSaleMain();
          }
        },
      }
    });
  }

  isValidEzCashPIN() {
    if (this.state.ezCashPIN == '') {
      this.navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: this.state.locale.ezCashMsg,
        },
        overrideBackPress: true
      });
      return false;
    } else if (this.state.ezCashPIN.length > 4 || this.state.ezCashPIN.length < 4) {
      let message = this.state.locale.msgInvalidEzCash;
      this.navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: message,
        },
        overrideBackPress: true
      });
      return false;
    }
    return true;
  }

  renderElementList() {
    return this.state.data.offer.elementwise_details.map(item => {
      return this.renderSTBItem(item);
    });
  }

  renderCardfullSerial() {
    return this.state.data.serial.map((element, index, serialList) => {
      return (
        <View key={index}>
          <View style={styles.detailElimentContainer}>
            <View style={styles.elementLeft}>
              <Text style={styles.contentContainerTxt}>
                {element.name}
              </Text>
            </View>
            <View style={styles.elementRight}>
              <Text style={styles.contentContainerTxt}>:    {element.serial}</Text>
            </View>
          </View>
        </View>
      )
    })
  }

  renderSTBItem = (item) => {
    return (
      <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
          <Text style={styles.contentContainerTxt}>
            {item.name}
          </Text>
        </View>
        <View style={styles.elementRight}>
          <Text style={styles.contentContainerTxt}>:    {item.warranty} </Text>
        </View>
      </View>
    )
  }

  renderItem = () => {
    return (
      <View>
        <View style={styles.detailElimentContainer}>
          <View style={styles.elementLeft}>
            <Text style={styles.contentContainerTxt}>
              {this.state.locale.connectionNo}
            </Text>
          </View>
          <View style={styles.elementRight}>
            <Text style={styles.contentContainerTxt}>:    {this.state.data.connectionNo}</Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.container}>
        {loadingIndicator}
        <View style={styles.topContainer} />
        <View style={styles.contentContainer}>

          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>

            <View style={{ alignItems: 'center' }}>
              <Text style={styles.topContainerTxt}>
                {this.state.locale.confirmation}
              </Text>
            </View>
            {this.props.isSTB ?
              <View>
                {/* stb flow */}
                {this.state.data.connectionNo !== '' ?
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.connectionNo}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.connectionNo}</Text>
                      </View>
                    </View>
                  </View>
                  :
                  true}
                {this.state.data.contactNo !== '' ?
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.contactNo}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.contactNo}</Text>
                      </View>
                    </View>
                  </View>
                  :
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.contactNo}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.altNo}</Text>
                      </View>
                    </View>
                  </View>}
                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.accessoryType}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.accessoryType}</Text>
                    </View>
                  </View>
                </View>
                {Array.isArray(this.state.data.serial) ?
                  [<View>
                    {this.renderCardfullSerial()}
                    {this.renderElementList()}
                  </View>]
                  :
                  <View>
                    <View>
                      <View style={styles.detailElimentContainer}>
                        <View style={styles.elementLeft}>
                          <Text style={styles.contentContainerTxt}>
                            {this.state.locale.stbSerial}
                          </Text>
                        </View>
                        <View style={styles.elementRight}>
                          <Text style={styles.contentContainerTxt}>:    {this.state.data.serial.serial}</Text>
                        </View>
                      </View>
                    </View>
                    {this.renderElementList()}
                  </View>}
                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.offerPrice}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.offerPrice}</Text>
                    </View>
                  </View>
                </View>
              </View>
              :
              <View>
                {/* non stb flow */}
                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.nicNo}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.NIC}</Text>
                    </View>
                  </View>
                </View>
                {this.state.data.connectionNo !== '' ?
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.connectionNo}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.connectionNo}</Text>
                      </View>
                    </View>
                  </View>
                  :
                  true}
                {this.state.data.contactNo !== '' ?
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.contactNo}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.contactNo}</Text>
                      </View>
                    </View>
                  </View>
                  :
                  true}
                {this.state.data.altNo !== '' ?
                  <View>
                    <View style={styles.detailElimentContainer}>
                      <View style={styles.elementLeft}>
                        <Text style={styles.contentContainerTxt}>
                          {this.state.locale.contactNoAlt}
                        </Text>
                      </View>
                      <View style={styles.elementRight}>
                        <Text style={styles.contentContainerTxt}>:    {this.state.data.altNo}</Text>
                      </View>
                    </View>
                  </View>
                  :
                  true}
                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.accessoryType}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.accessoryType}</Text>
                    </View>
                  </View>
                </View>

                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.accessorySerial}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.serial.serial}</Text>
                    </View>
                  </View>
                </View>

                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.offer}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.offer.offer_code}</Text>
                    </View>
                  </View>
                </View>

                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.offerPrice}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.offerPrice}</Text>
                    </View>
                  </View>
                </View>

                <View>
                  <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                      <Text style={styles.contentContainerTxt}>
                        {this.state.locale.warrantyPeriod}
                      </Text>
                    </View>
                    <View style={styles.elementRight}>
                      <Text style={styles.contentContainerTxt}>:    {this.state.data.warrantyPeriod} </Text>
                    </View>
                  </View>
                </View>
              </View>
            }
            <View style={{ paddingTop: 25, paddingHorizontal: 15 }}>
              <TextFieldComponent
                label={this.state.locale.ezPin}
                title={this.state.persistenceData.conn + ' ' + this.state.locale.ezCashPINText}
                value={this.state.selectedSKUWarranty}
                onChangeText={(text) => this.ezCashPinInput(text)}
                maxLength={4}
              />
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={styles.bottomContainer}>

          <View style={styles.payBtnBtnContainer}>
            {this.state.ezCashPIN.length == 4 ?
              <TouchableOpacity style={styles.payBtn} onPress={() => this.newAccessorySale()}>
                <Text style={styles.payBtnText}>{this.state.locale.confirm}</Text>
              </TouchableOpacity> :
              <TouchableOpacity style={styles.payBtnDisable} onPress={() => { }}>
                <Text style={styles.payBtnText}>{this.state.locale.confirm}</Text>
              </TouchableOpacity>}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const accessorySale = state.accessorySales;
  const lang = state.lang.current_lang;
  return { lang, accessorySale };
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 7,
    paddingRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  contentContainer: {
    flex: 0.8,
    marginTop: 7
  },
  bottomContainer: {
    flex: 0.15
  },
  topContainer: {
    flex: 0.05,
    marginLeft: 18,
    marginRight: 18,
    alignItems: 'center'
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
  },
  payBtn: {
    flex: 0.30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    paddingRight: 10,
    marginRight: 25,
    paddingLeft: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnActive
  },
  payBtnDisable: {
    flex: 0.30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    paddingRight: 10,
    marginRight: 25,
    paddingLeft: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnDisable
  },
  topContainerTxt: {
    marginTop: 7,
    marginBottom: 7,
    paddingBottom: 7,
    paddingBottom: 7,
    fontSize: Styles.btnFontSize,
    color: Colors.btnActiveTxtColor,
  },
  payBtnText: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  contentContainerTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: 15,
    color: Colors.btnActiveTxtColor,
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    paddingLeft: 15,
    marginBottom: 10
  },
  elementLeft: {
    flex: 1,
    borderWidth: 0
  },
  elementRight: {
    flex: 1,
    borderWidth: 0
  },
});


export default connect(mapStateToProps, actions)(AccessorySalesDetailsPage); 