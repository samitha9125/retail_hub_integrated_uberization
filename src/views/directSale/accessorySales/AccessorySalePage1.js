import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard, BackHandler, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/AccessorySales';
import * as actions from '../../../actions';
import * as Utils from '../../../utills/UtilsCfss';
import Utills from '../../../utills/Utills';
import ActIndicator from './ActIndicator';
import { Header } from '../../wom/Header';
import TextFieldComponent from '../../wom/components/TextFieldComponent';

const Utill = new Utills();
const { height } = Dimensions.get('window');
class AccessorySalePage1 extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    this.state = {
      nicNO: '',
      nic_error: '',
      connType: '',
      prePostType: '',
      lob: '',

      connection_error: '',
      cxContact_error: '',
      alt_conn_error: '',
      isDisabled: true,

      contactNoAlt: '',
      cxContactNo: '',
      conNumber: '',
      connectionType: '',
      numberType: '',
      apiLoading: false,
      locale: {
        invalidContactNo: strings.invalidContactNo,
        noContactNumber: strings.noContactNumber,
        noRegContactNo: strings.noRegContactNo,
        contactAlt: strings.contactNoAlt,
        connectionNo: strings.connectionNo,
        invalidConnectionNo: strings.invalidConnectionNo,
        contactNo: strings.contactNo,
        NICNo: strings.NICNo,
        invalidNIC: strings.invalidNIC,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        connectionType: strings.connectionTypeInput,
        continue: strings.continue,
        aSaleHeader: strings.aSaleHeader,
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

    this.setState({
      nic_error: '',
      nicNO: '',
      connection_error: '',
      cxContact_error: '',
      alt_conn_error: '',
      contactNoAlt: '',
      cxContactNo: '',
      conNumber: '',
      isDisabled: true
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler() {
    this.resetProps();
    this.navigatorOb.pop();
  }

  resetProps() {
    this.props.accessorySalesReset();
  }

  gotoAccessorySales2() {
    const data = {
      NIC: this.state.nicNO,
      contactNo: this.state.cxContactNo,
      contactNoAlt: this.state.contactNoAlt,
      connectionNo: this.state.conNumber,
      connectionType: this.state.connectionType
    };

    this.props.getAccessorySaleDetail(data);
    this.navigatorOb.push({
      title: this.state.locale.aSaleHeader,
      screen: 'DialogRetailerApp.views.AccessorySalePage2',
      passProps: {
        lob: this.state.lob,
        NIC: this.state.nicNO,
        ...data,
        screenTitle: this.state.locale.aSaleHeader,
      },
      overrideBackPress: true
    });
  }

  validateConnectionNo() {
    this.setState({ apiLoading: true }, () => {
      const data = { NIC: this.state.nicNO, connectionNumber: this.state.conNumber };

      Utill.apiRequestPost('CXContactNo', 'AccessorySale', 'cfss',
        data, this.validateConnectionNoSuccessCb, this.validateConnectionNoFailureCb, this.validateConnectionNoExCb);
    });
  }

  validateConnectionNoSuccessCb = (response) => {
    console.log('validateConnectionNoSuccessCb ', response);
    this.setState({ apiLoading: false, connectionType: response.data.connType ? response.data.connType : '' }, () => {
      try {
        if (response.data.data) {
          if (Array.isArray(response.data.data)) {
            if (response.data.data.length > 0) {
              const cxContactNo = response.data.data[0].contactNumber;
              this.setState({ cxContactNo }, () => this.checkRequiredFields());
            } else {
              this.displayCommonAlert('error', '', response.data.info, '');
            }
          } else {
            this.displayCommonAlert('error', '', response.data.info, '');
          }
        } else {
          this.displayCommonAlert('error', '', response.data.info, '');
        }
      } catch (error) {
        console.log('error ', error);
      }
    });
  }
  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
    this.navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        onPressOK: () => { },
      }
    });
  }

  validateConnectionNoFailureCb = (response) => {
    this.setState({ apiLoading: false, conNumber: '' }, () => {
      this.displayCommonAlert('error', '', response.data.error);
    });
  }

  validateConnectionNoExCb = (response) => {
    this.setState({ apiLoading: false, conNumber: '' }, () => {
      let error;
      if (response == 'No_Network') {
        this.showNoNetworkModal(() => this.validateConnectionNo());
      } else if (response == 'Network Error') {
        error = this.state.locale.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locale.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: { retryFunc: retryFunc }
    });
  }

  onChangeNICNo = (text) => {
    this.setState({ nicNO: text }, () => {
      if (this.state.nicNO !== '' && Utils.validateNIC(text) === false) {
        this.setState({ nic_error: this.state.locale.invalidNIC })
      } else {
        this.setState({ nic_error: '' }, () => Keyboard.dismiss())
      }
    })
  }

  onChangeConnectionNo = (text) => {
    this.setState({ conNumber: text }, () => {
      if (this.state.conNumber == '') {
        this.setState({ connection_error: '' });
      } else if (this.state.conNumber !== '' && Utils.dtvDirectSaleNumValidate(this.state.conNumber) === false && Utils.lteNumValidate(this.state.conNumber) === false) {
        this.setState({ connection_error: this.state.locale.invalidConnectionNo });
      } else {
        let nic_error;
        this.state.nicNO == '' ? nic_error = this.state.locale.invalidNIC : nic_error = '';
        this.setState({ connection_error: '', nic_error }, () => {
          Keyboard.dismiss();
          this.validateConnectionNo();
        });
      }
    })
  }

  onChangeAltContactNo = (text) => {
    this.setState({ contactNoAlt: text }, () => {
      if ((this.state.contactNoAlt !== '' && Utils.gsmNumbervalidate(this.state.contactNoAlt) === false)) {
        this.setState({ alt_conn_error: this.state.locale.invalidContactNo });
      } else {
        this.setState({ alt_conn_error: '' }, () => {
          Keyboard.dismiss();
          this.checkRequiredFields();
        });
      }
    })
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  checkRequiredFields = () => {
    let isDisabled;
    if (this.state.nicNO !== '' && this.state.conNumber !== '' && (this.state.cxContactNo !== '' || this.state.contactNoAlt !== '')) {
      isDisabled = false;
    } else { isDisabled = true; };

    this.setState({ isDisabled });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.props.screenTitle} />
        {this.showIndicator()}
        <View style={styles.topContainer}></View>
        <View style={styles.middleContainer}>
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            {this.state.apiLoading == true ? <ActIndicator animating={true} /> : <View />}
            <View style={styles.container}>
              <View style={styles.textFieldComponent}>
                <TextFieldComponent
                  label={this.state.locale.NICNo}
                  title={''}
                  onChangeText={(text) => this.onChangeNICNo(text)}
                  error={this.state.nic_error}
                />
              </View>
              <View style={styles.textFieldComponent }>
                <TextFieldComponent
                  label={this.state.locale.connectionNo}
                  title={''}
                  keyboardType={'numeric'}
                  disabled={this.state.nicNO == '' ? true : this.state.nic_error == '' ? false : true}
                  onChangeText={(text) => this.onChangeConnectionNo(text)}
                  error={this.state.connection_error}
                />
              </View>
              <View style={styles.textFieldComponent}>
                <TextFieldComponent
                  label={this.state.locale.contactNo}
                  title={''}
                  keyboardType={'numeric'}
                  disabled={true}
                  value={this.state.cxContactNo}
                />
              </View>
              <View style={styles.textFieldComponent}>
                <TextFieldComponent
                  label={this.state.locale.contactAlt}
                  title={''}
                  keyboardType={'numeric'}
                  disabled={(this.state.nicNO == '' && this.state.conNumber == '') ? true : (this.state.nic_error == '' && this.state.connection_error == '') ? false : true}
                  onChangeText={(text) => this.onChangeAltContactNo(text)}
                  error={this.state.alt_conn_error}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            {this.state.isDisabled == false ?
              <TouchableOpacity style={styles.payBtn}
                onPress={() => this.gotoAccessorySales2()}>
                <Text style={styles.payBtnTxt}>
                  {this.state.locale.continue}
                </Text>
              </TouchableOpacity>
              :
              <TouchableOpacity style={styles.payBtnDisabled}
                disabled={true}>
                <Text style={styles.payBtnTxt}>
                  {this.state.locale.continue}
                </Text>
              </TouchableOpacity>}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const accessorySale = state.accessorySales;
  const lang = state.lang.current_lang;
  return { lang, accessorySale }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginLeft: 18,
    marginRight: 18
  },
  payBtnDisabled: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginRight: 25,
    width: 10,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnDeactive,
  },
  bottomContainer: {
    flex: 0.15,
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
    marginTop: 8,
  },
  payBtn: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginRight: 25,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnActive
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  topContainer: {
    flex: 0.05,
  },
  middleContainer: {
    flex: 0.8,
    marginLeft: 8,
    marginRight: 8,
    flexDirection: 'column',
  },
  actIndicator: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  textFieldComponent:{
    marginHorizontal: 20
  }
});

export default connect(mapStateToProps, actions)(AccessorySalePage1);