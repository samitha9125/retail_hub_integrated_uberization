import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../../config/colors';
import * as actions from '../../../../actions';
import strings from '../../../../Language/Wom';
import Styles from '../../../../config/styles';
import imageAtentionIcon from '../../../../../images/common/alert.png';
import imageErrorIcon from '../../../../../images/common/error_msg.png';
import imageSucessIcon from '../../../../../images/common/success_msg.png';
class AccessorySalesGeneralAlert extends Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.Language);
        this.state = {
            customMessage: this.props.message,
            locals: {
                lblSuccess: strings.lblSuccess,
                lblError: strings.error,
                lblOK: strings.btnOk
            }
        };
        this.navigatorOb = this.props.navigator;
    }

    onBnPressYes = () => {
        this.navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        this.props.onPressOK ? this.props.onPressOK() : true;
    }

    render() {
        let alertIcon;
        let display_msg;
        let descriptionContainer;
        if (this.props.messageType == 'error') {
            alertIcon = imageErrorIcon;
            display_msg = this.state.customMessage;
            descriptionContainer = (
                <View style={styles.descriptionContainer}>
                    <Text style={styles.mainText}>{this.state.locals.lblError}</Text>
                    <Text style={styles.descriptionText}>{display_msg}</Text>
                </View>
            );
        } else if (this.props.messageType == 'success') {
            alertIcon = imageSucessIcon;
            display_msg = this.state.customMessage;
            descriptionContainer = (
                <View style={styles.descriptionContainer}>
                    <Text style={styles.mainTextSuccess}>{this.state.locals.lblSuccess}</Text>
                    <Text style={styles.descriptionTextSuccess}>{display_msg}</Text>
                </View>
            );
        } else if (this.props.messageType == 'alert') {
            alertIcon = imageAtentionIcon;
            display_msg = this.state.customMessage;
            descriptionContainer = (
                <View style={styles.descriptionContainer}>
                    <Text style={styles.mainText}>{this.state.locals.lblError}</Text>
                    <Text style={styles.descriptionText}>{display_msg}</Text>
                </View>
            );
        } else {
            alertIcon = imageAtentionIcon;
            display_msg = this.state.customMessage;
            descriptionContainer = (
                <View style={styles.descriptionContainer}>
                    <Text style={styles.mainText}>{this.state.locals.lblError}</Text>
                    <Text style={styles.descriptionText}>{display_msg}</Text>
                </View>
            );
        }
        return (
            <View style={styles.containerOverlay}>
                <View style={styles.topContainer} />
                <View style={styles.modalContainer}>
                    <View style={styles.innerContainer}>
                        <View style={styles.alertImageContainer}>
                            <Image source={alertIcon} style={styles.alertImageStyle} />
                        </View>
                        {descriptionContainer}
                        <View style={styles.bottomCantainerBtn}>
                            <View style={styles.dummyView} />
                            <TouchableOpacity onPress={() => this.onBnPressYes()} style={styles.bottomBtn}>
                                <Text style={styles.bottomCantainerBtnTxt}>
                                    {this.state.locals.lblOK}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomContainer} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerOverlay: {
        flex: 1,
        backgroundColor: Colors.modalOverlayColor
    },
    topContainer: {
        flex: 0.4
    },
    modalContainer: {
        flex: 1.5
    },
    bottomContainer: {
        flex: 0.6
    },
    innerContainer: {
        flex: 1,
        backgroundColor: Colors.backgroundColorWhiteWithAlpa,
        padding: 12,
        paddingBottom: 20,
        marginTop: 25,
        marginBottom: 25,
        marginLeft: 13,
        marginRight: 13
    },
    alertImageContainer: {
        flex: 0.7,
        alignItems: 'center',
        padding: 5
    },
    alertImageStyle: {
        width: 90,
        height: 90,
        marginTop: 5,
        marginBottom: 5
    },
    descriptionContainer: {
        flex: 1.5,
        flexDirection: 'column',
        paddingTop: 10,
        paddingRight: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        marginTop: 10
    },
    descriptionText: {
        fontSize: Styles.delivery.modalFontSize,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        fontWeight: '100'
    },
    mainText: {
        alignItems: 'flex-start',
        fontSize: Styles.delivery.modalFontSize,
        textAlign: 'left',
        color: Colors.colorBlack,
        paddingTop: 5,
        marginLeft: 10,
        marginBottom: 15,
        paddingBottom: 5,
        marginRight: 10,
        marginTop: 20,
        fontWeight: 'bold'
    },
    mainTextSuccess: {
        alignItems: 'flex-start',
        fontSize: 20,
        textAlign: 'left',
        color: Colors.colorBlack,
        paddingTop: 5,
        marginLeft: 10,
        marginBottom: 15,
        paddingBottom: 5,
        marginRight: 10,
        marginTop: 20,
        fontWeight: 'bold'
    },

    descriptionTextSuccess: {
        fontSize: 20,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        fontWeight: '100'
    },
    bottomCantainerBtn: {
        flex: 0.5,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        paddingTop: 5,
        paddingRight: 5,
        paddingBottom: 5,
        marginTop: 10
    },
    dummyView: {
        flex: 4,
        justifyContent: 'center'
    },
    bottomBtn: {
        flex: 1,
        marginRight: 10,
        justifyContent: 'center'
    },
    bottomCantainerBtnTxt: {
        fontSize: Styles.delivery.modalFontSize,
        alignSelf: 'center',
        color: Colors.colorDarkOrange
    }
});

const mapStateToProps = (state) => {
    const Language = state.lang.current_lang;
    return { Language };
}

export default connect(mapStateToProps, actions)(AccessorySalesGeneralAlert);
