import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, BackHandler, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/AccessorySales';
import strings2 from '../../../Language/Wom';
import Utills from '../../../utills/Utills';
import * as actions from '../../../actions';
import { Header } from '../../wom/Header';
import ActIndicator from './ActIndicator';
import RadioButton from '../../wom/components/RadioButtonComponent';
import DropDownInput from '../../wom/components/DropDownInput';
import TextFieldComponent from '../../wom/components/TextFieldComponent';
import SerialListComponent from '../../wom/components/SerialListComponent';

const Utill = new Utills();
const { height } = Dimensions.get('window');
class AccessorySalePage2 extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    strings2.setLanguage(this.props.lang);
    this.state = {
      cardStatus: null,
      isFormValidForNotSTB: false,

      typeList: [],
      typeListDropdown: [],

      selectedItemTypeObj: {},
      offersArray: [],
      offerListDropDown: [],
      scannedItemSerial: {},
      offersDataArray: [],
      selectedSKUValue: '',
      selectedSKUOffer: '',
      selectedSKUOfferPrice: '',
      selectedSKUWarranty: '',
      isViewEnable: false,
      newSTBSerial: '',
      newSIMSerial: '',
      elementwise_details: [],
      selectedOfferObject: {},
      item_condition: '',

      refreshNewSerialData: false,
      refreshNewSimSerialData: false,
      refreshNewSTBSerialData: false,

      isSKUOfferViewEnabled: false,
      isSTBOffersViewEnabled: false,

      SKUList: [],
      SKUListDropDown: [],
      isSTB: false,
      selectedSKUObj: {},

      isOfferViewDisable: true,
      contactNo: this.props.contactNo,
      connectionNo: this.props.connectionNo,
      altNo: this.props.contactNoAlt,

      apiLoading: false,

      selectedSortType: '',
      selectedOffer: '',
      selectedOfferPrice: '',
      warrantyPeriod: '',
      selectedOfferData: [],

      locale: {
        itemType: strings.itemType,
        offer: strings.offer,
        offerPrice: strings.offerPrice,
        warrantyPeriod: strings.warrantyPeriod,
        selectAccessoryType: strings.selectAccessoryType,
        continue: strings.continue,
        price: strings.price,
        offerText: strings.offerText,
        scanSTBSerial: strings.scanSTBSerial,
        scanSIMSerial: strings.scanSIMSerial,
        cardfull: strings2.cardfull,
        cardless: strings2.cardless,
        SKU: strings.SKU,
        STB: strings.STB,
        errFillAll: strings.errFillAll,
        aSaleHeader: strings.aSaleHeader,
        scanSTBSerialHeader: strings.scanSTBSerialHeader,
        lblNewSTBSerial: strings.lblNewSTBSerial,
        lblNewSIMSerial: strings.lblNewSIMSerial,
        lblNewRCUSerial: strings.lblNewRCUSerial,
        lblNewRemoteSerial: strings.lblNewRemoteSerial,
        lblNewPowerSupplySerial: strings.lblNewPowerSupplySerial,
        lblNewCableSerial: strings.lblNewCableSerial,
        lblNewLNBSerial: strings.lblNewLNBSerial,
        network_error_message: strings2.network_error_message,
        api_error_message: strings2.api_error_message,
        newLabelToReplace: strings2.newLabelToReplace
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)

    this.setState({
      cardStatus: null,
      isFormValidForNotSTB: false,

      typeList: [],
      typeListDropdown: [],

      selectedItemTypeObj: {},
      offersArray: [],
      scannedItemSerial: {},
      offersDataArray: [],
      selectedSKUValue: '',
      selectedSKUOffer: '',
      selectedSKUOfferPrice: '',
      selectedSKUWarranty: '',
      isViewEnable: false,
      newSTBSerial: '',
      newSIMSerial: '',
      elementwise_details: [],
      selectedOfferObject: {},

      SKUList: [],
      SKUListDropDown: [],
      selectedSortType: '',
      selectedOffer: '',
      selectedOfferPrice: '',
      warrantyPeriod: '',
      selectedOfferData: [],
    }, () => {
      this.getLOBTypes();
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
  }

  handleBackButtonClick = () => {
    this.navigatorOb.pop({ animated: true, animationType: 'fade' });
    return true;
  }

  getLOBTypes() {
    const data = {
      request_type: 'DIRECT_ACCESORY_SALE'
    };
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('GetAccessoryTypes', 'warrantyReplacement', 'cfss', data,
        this.getLOBTypesSuccessCb, this.getLOBTypesFailureCb, this.getLOBTypesExCb);
    });
  }

  getLOBTypesSuccessCb = (response) => {
    let lobArray = response.data.data.map((item) => { return item.name });
    this.setState({ apiLoading: false, typeListDropdown: lobArray, typeList: response.data.data });
  }

  getLOBTypesFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: { message: response.data.error },
        overrideBackPress: true
      });
    });
  }

  getLOBTypesExCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'No_Network') {
        error = this.state.locale.network_error_message;
        this.showNoNetworkModal(() => this.getLOBTypes());
      } else if (response == 'Network Error') {
        error = this.state.locale.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locale.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  displayAlert() {
    this.navigatorOb.showModal({
      screen: 'DialogRetailerApp.models.GeneralModalException',
      title: 'GeneralModalException',
      passProps: {
        message: this.state.locale.errFillAll
      },
      overrideBackPress: true
    });
  }

  gotoSaleDetails() {
    let data = {
      connectionNo: this.props.accessorySale.accessory_sale_detail.connectionNo,
      contactNo: this.props.accessorySale.accessory_sale_detail.contactNo,
      accessoryType: this.state.selectedSortType,
      altNo: this.props.accessorySale.accessory_sale_detail.contactNoAlt,
      NIC: this.props.accessorySale.accessory_sale_detail.NIC,
      offer: this.state.selectedOfferObject,
      connectionType: this.props.connectionType
    };
    let isSTB = null;

    if (this.state.selectedSortType == 'STB') {
      isSTB = true;
      data.offerPrice = this.state.selectedSKUOfferPrice
      data.warranty = this.state.selectedSKUWarranty;
      data.replaceble_item = this.state.selectedSKUObj;
    } else {
      isSTB = false;
      data.offerPrice = this.state.selectedOfferPrice;
      data.warrantyPeriod = this.state.warrantyPeriod;
    }

    if (this.state.selectedSortType == 'STB' && this.state.cardStatus == false) {
      data.serial = [this.state.newSTBSerial, this.state.newSIMSerial];
    } else if (this.state.selectedSortType == 'STB' && (this.state.cardStatus == true || this.state.cardStatus == null)) {
      data.serial = this.state.newSTBSerial;
    } else {
      data.serial = this.state.scannedItemSerial;
    }

    this.navigatorOb.push({
      title: this.state.locale.aSaleHeader,
      screen: 'DialogRetailerApp.views.AccessorySalesDetailsPage',
      passProps: { data, isSTB }
    });
  }

  typeSelected(idx, value) {
    const selectedItemTypeObj = this.state.typeList[idx];
    let isViewEnable;
    value == 'STB' ? isViewEnable = false : isViewEnable = true;
    this.setState({
      isViewEnable, offerListDropDown: [],
      selectedSortType: value, selectedItemTypeObj, selectedOffer: '', selectedSKUValue: '', selectedSKUObj: {}, selectedSKUWarranty: '',
      selectedOfferPrice: '', warrantyPeriod: '', scannedItemSerial: {}, selectedOfferObject: {}, selectedSKUOffer: '', refreshNewSTBSerialData: true, refreshNewSimSerialData: true, refreshNewSerialData: true
    }, () => {
      this.state.selectedSortType == 'STB' ? this.getSKUData() : true;
    });
  }

  getSKUData() {
    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('getTradeMaterial', 'accessorySale', 'cfss', {
        contact: this.state.contactNo,
        conn_no: this.state.connectionNo,
        contactAlt: this.state.altNo,
      }, (response) => {
        let SKUListNames = response.data.data.map((item) => item.name);
        this.setState({ apiLoading: false, SKUList: response.data.data, SKUListDropDown: SKUListNames, isViewEnable: true });
      }, (response) => {
        this.setState({ apiLoading: false }, () => this.showCommonError(response.data.error));
      }, (error) => {
        this.setState({ apiLoading: false }, () => this.showCommonError(error));
      });
    });
  }

  SKUSelected(idx, value) {
    this.setState({
      offerListDropDown: [],
      selectedSKUValue: value,
      selectedSKUObj: this.state.SKUList[idx],
      isSKUOfferViewEnabled: true,
      selectedSKUOffer: '',
      selectedSKUOfferPrice: '',
      selectedSKUWarranty: '',
      elementwise_details: [],
      selectedOfferObject: {}
    }, () => {
      const sku = this.state.SKUList[idx];
      this.getOfferData(sku)
    });
  }

  getOfferData(sku) {
    try {
      let cardStatus = null;
      let item_condition = '';
      let data = { connectionNo: this.state.connectionNo };

      if (sku.item_condition) {
        item_condition = sku.item_condition;
        if (sku.item_condition !== 'New') {
          cardStatus = false;
        }
      }

      this.state.selectedSortType == 'STB' ? data.material_code = sku.material_code : data.serial = sku;

      this.setState({ apiLoading: true, cardStatus, item_condition }, () => {
        Utill.apiRequestPost('getOffers', 'accessorySale', 'cfss', data, (response) => {
          let offersDataArray = response.data.data.map((item) => { return item });
          let offerListNames = response.data.data.map((item) => { return item.offer_code });

          this.setState({
            apiLoading: false, offerList: response.data.data, offersDataArray,
            offerListDropDown: offerListNames, offersArray: offerListNames
          });
        }, (response) => {
          this.setState({ apiLoading: false }, () => this.showCommonError(response.data.error));
        }, (response) => {
          this.setState({ apiLoading: false }, () => this.showCommonError(response));
        });
      });
    } catch (error) {
      console.log('error ', error);
    }
  }

  showCommonError(error) {
    this.setState({ isLoading: false }, () => {
      this.navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
    });
  }

  offerSelected2(idx, value) {
    const selectedSKUOfferPrice = this.state.offerList[idx].prod_price_with_tax ? this.state.offerList[idx].prod_price_with_tax : '';
    const selectedSKUWarranty = this.state.offerList[idx].elementwise_details.length > 0 ? this.state.offerList[idx].elementwise_details[0].warranty ? this.state.offerList[idx].elementwise_details[0].warranty : '' : '';
    const elementwise_details = this.state.offerList[idx].elementwise_details;
    const selectedOfferObject = this.state.offerList[idx];
    this.setState({
      selectedSKUOffer: value, selectedSKUOfferPrice, isSTBOffersViewEnabled: true,
      selectedSKUWarranty, elementwise_details, selectedOfferObject
    })
  }

  async offerSelected(idx, value) {
    const selectedOfferObject = this.state.offersDataArray[idx];
    const selectedOfferPrice = selectedOfferObject.prod_PRICE_WITH_TAX ? selectedOfferObject.prod_PRICE_WITH_TAX : selectedOfferObject.prod_price_with_tax ? selectedOfferObject.prod_price_with_tax : '';
    const warrantyPeriod = selectedOfferObject.remainingWar ? selectedOfferObject.remainingWar : selectedOfferObject.warranty ? selectedOfferObject.warranty : '';

    const formattedWarrantyPeriod = warrantyPeriod;

    this.setState({ selectedOffer: value, selectedOfferPrice, warrantyPeriod: formattedWarrantyPeriod, selectedOfferObject })
  }

  changeCardLessStatus() {
    this.setState({ cardStatus: !this.state.cardStatus })
  }

  enableContinueBtn() {
    if (this.state.selectedSortType == 'STB') {
      if (this.state.cardStatus == true || this.state.cardStatus == null) {
        if (this.state.selectedSKUValue !== '' && this.state.selectedSKUOffer !== '' && this.state.newSTBSerial !== '') {
          return true
        } else {
          return false;
        }
      } else {
        if (this.state.selectedSKUValue !== '' && this.state.selectedSKUOffer !== '' && this.state.newSTBSerial !== '' && this.state.newSIMSerial !== '') {
          return true
        } else {
          return false;
        }
      }
    } else {
      if (this.state.selectedSortType !== '' && this.state.scannedItemSerial !== {} && this.state.selectedOffer !== '') {
        return true;
      } else {
        return false;
      }
    }
  }

  stbSerialResponse = (isSerialValidate, response) => {
    this.setState({ newSTBSerial: response, isOfferViewDisable: false, refreshNewSTBSerialData: false });
  }

  simSerialResponse = (isSerialValidate, response) => {
    this.setState({ newSIMSerial: response, refreshNewSimSerialData: false });
  }

  newSerialResponse = (isSerialValidate, response) => {
    this.setState({ scannedItemSerial: response, refreshNewSerialData: false }, () => { this.getOfferData(response) });
  }

  renderNewManualLabel(itemType) {
    let newLabel;
    const newLabelToReplace = this.state.locale.newLabelToReplace.toString();
    newLabel = newLabelToReplace.replace(/<item>/g, itemType);
    return newLabel;
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  renderNewSerialLabel() {
    const { lblNewPowerSupplySerial, lblNewRemoteSerial, lblNewCableSerial, lblNewLNBSerial } = this.state.locale;
    if (this.state.selectedSortType == 'Cable') return lblNewCableSerial;
    else if (this.state.selectedSortType == 'Power Supply') return lblNewPowerSupplySerial;
    else if (this.state.selectedSortType == 'Remote') return lblNewRemoteSerial;
    else if (this.state.selectedSortType == 'LNB') return lblNewLNBSerial;
    else return 'new serial';
  }

  render() {
    const { RaidoView, RadioStyle, radioButtonLabelStyle, formInput } = styles;
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.props.screenTitle} />
        {this.showIndicator()}
        <View style={styles.topContainer} />
        <View style={styles.middleContainer}>
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            <View>
              <View style={{ marginHorizontal: 25 }} >
                <DropDownInput
                  alignDropDownnormal={true}
                  dropDownTitle={this.state.locale.itemType}
                  hideRightIcon={true}
                  selectedValue={this.state.selectedSortType}
                  dropDownData={this.state.typeListDropdown}
                  onSelect={(idx, value) => this.typeSelected(idx, value)}
                />
              </View>
              {this.state.isViewEnable ?
                this.state.selectedSortType == 'STB' ?
                  <View>
                    <View style={styles.mainContainer}>
                      <DropDownInput
                        alignDropDownnormal={true}
                        dropDownTitle={this.state.locale.SKU}
                        selectedValue={this.state.selectedSKUValue}
                        dropDownData={this.state.SKUListDropDown}
                        onSelect={(idx, value) => this.SKUSelected(idx, value)}
                      />
                    </View>
                    {this.state.isSKUOfferViewEnabled ?
                      <View style={styles.mainContainer}>
                        <DropDownInput
                          alignDropDownnormal={true}
                          dropDownTitle={this.state.locale.offerText}
                          selectedValue={this.state.selectedSKUOffer}
                          dropDownData={this.state.offerListDropDown}
                          onSelect={(idx, value) => this.offerSelected2(idx, value)}
                        />
                      </View>
                      : true}
                    {this.state.isSTBOffersViewEnabled ?
                      <View>
                        <View style={styles.mainContainer}>
                          <TextFieldComponent
                            label={this.state.locale.price}
                            title={''}
                            disabled={true}
                            value={this.state.selectedSKUOfferPrice}
                          />
                        </View>
                        <View style={styles.mainContainer}>
                          <TextFieldComponent
                            label={this.state.locale.warrantyPeriod}
                            title={''}
                            disabled={true}
                            value={this.state.selectedSKUWarranty}
                          />
                        </View>
                      </View>
                      : true}
                    {this.state.item_condition !== 'New' && this.state.cardStatus !== null ?
                      <View style={formInput}>
                        <View style={RaidoView}>
                          <View style={RadioStyle}>
                            <RadioButton
                              currentValue={this.state.cardStatus}
                              value={true}
                              outerCircleSize={20}
                              innerCircleColor={Colors.radioBtn.innerCircleColor}
                              outerCircleColor={Colors.radioBtn.outerCircleColor}
                              onPress={() => this.changeCardLessStatus()}>
                              <Text style={radioButtonLabelStyle}>{this.state.locale.cardless}</Text>
                            </RadioButton>
                          </View>
                          <View style={RadioStyle}>
                            <RadioButton
                              currentValue={this.state.cardStatus}
                              value={false}
                              outerCircleSize={20}
                              innerCircleColor={Colors.radioBtn.innerCircleColor}
                              outerCircleColor={Colors.radioBtn.outerCircleColor}
                              onPress={(value) => this.changeCardLessStatus()}>
                              <Text style={radioButtonLabelStyle}>{this.state.locale.cardfull}</Text>
                            </RadioButton>
                          </View>
                        </View>
                      </View>
                      : true}
                    {this.state.isSKUOfferViewEnabled ?
                      <View style={styles.mainContainer}>
                        <SerialListComponent
                          refreshData={this.state.refreshNewSTBSerialData}
                          apiParams={{
                            request_type: 'DIRECT_ACCESORY_SALE',
                            app_flow: 'DIRECT_ACCESORY_SALE',
                            ownership: 'CxOwned',
                          }}
                          serialData={{ type: 'STB', name: 'STB', stb_type: this.state.selectedSKUObj }}
                          // manualLable={this.state.locale.lblNewSTBSerial} //renderNewManualLabel
                          manualLable={this.renderNewManualLabel('STB')}
                          isAllSerialsValidated={this.stbSerialResponse}
                          navigatorOb={this.props.navigator}
                          actionName='validateSerialNew'
                          controllerName='serial'
                          moduleName='wom'
                        />
                      </View>
                      : true}
                    {this.state.cardStatus == false ?
                      <View style={styles.mainContainer}>
                        <SerialListComponent
                          refreshData={this.state.refreshNewSimSerialData}
                          apiParams={{
                            request_type: 'DIRECT_ACCESORY_SALE',
                            app_flow: 'DIRECT_ACCESORY_SALE',
                            ownership: 'CxOwned'
                          }}
                          manualLable={this.renderNewManualLabel('SIM')}
                          serialData={{ type: 'SIM', name: 'SIM' }}
                          isAllSerialsValidated={this.simSerialResponse}
                          navigatorOb={this.props.navigator}
                          actionName='validateSerialNew'
                          controllerName='serial'
                          moduleName='wom'
                        />
                      </View>
                      : true}
                  </View>
                  :
                  <View>
                    <View style={{ marginHorizontal: 25 }} >
                      <SerialListComponent
                        refreshData={this.state.refreshNewSerialData}
                        apiParams={{
                          MSISDN: this.state.connectionNo !== '' ? this.state.connectionNo : this.state.altNo,
                          materialStatus: 'NEW',
                          request_type: 'DIRECT_ACCESORY_SALE',
                          app_flow: 'DIRECT_ACCESORY_SALE',
                          ownership: 'CxOwned',
                        }}
                        // manualLable={this.renderNewSerialLabel()}
                        manualLable={this.renderNewManualLabel(this.state.selectedSortType)}
                        serialData={this.state.selectedItemTypeObj}
                        isAllSerialsValidated={this.newSerialResponse}
                        navigatorOb={this.props.navigator}
                        actionName='validateSerialNew'
                        controllerName='serial'
                        moduleName='wom'
                      />
                    </View>
                    {this.state.selectedSortType !== 'STB' && this.state.offerListDropDown.length > 0 ?
                      <View style={{ marginHorizontal: 25 }}>
                        <View>
                          <DropDownInput
                            alignDropDownnormal={true}
                            dropDownTitle={this.state.locale.offer}
                            hideRightIcon={true}
                            selectedValue={this.state.selectedOffer}
                            dropDownData={this.state.offersArray}
                            onSelect={(idx, value) => this.offerSelected(idx, value)}
                          />
                        </View>
                        <View>
                          <TextFieldComponent
                            label={this.state.locale.offerPrice}
                            title={''}
                            disabled={true}
                            value={this.state.selectedOfferPrice}
                          />
                        </View>
                        <View>
                          <TextFieldComponent
                            label={this.state.locale.warrantyPeriod}
                            title={''}
                            disabled={true}
                            value={this.state.warrantyPeriod}
                          />
                        </View>
                      </View>
                      : true}
                  </View>
                : true}
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            {this.enableContinueBtn() ?
              <TouchableOpacity style={styles.payBtn} onPress={() => this.gotoSaleDetails()}>
                <Text style={styles.payBtnTxt}>{this.state.locale.continue}</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity
                disabled={true}
                style={styles.buttonContainerDisabled} >
                <Text style={styles.payBtnTxt}>{this.state.locale.continue}</Text>
              </TouchableOpacity>}
          </View>
        </View>
      </View >
    );
  }
}

const mapStateToProps = state => {
  const accessorySale = state.accessorySales;
  const lang = state.lang.current_lang;
  return { lang, accessorySale }
};

const styles = StyleSheet.create({
  RaidoView: {
    marginVertical: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  RadioStyle: {
    alignItems: 'center',
    flex: 0.8
  },
  buttonContainerDisabled: {
    width: '35%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginRight: 25,
    backgroundColor: Colors.btnDisable,
  },
  radioButtonLabelStyle: {
    marginLeft: 10,
  },
  mainContainer: {
    marginTop: 10,
    marginHorizontal: 25
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
  },
  topContainer: {
    flex: 0.05,
  },
  middleContainer: {
    flex: 0.8,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  bottomContainer: {
    flex: 0.15,
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
    marginTop: 8
  },
  payBtn: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    padding: 10,
    borderRadius: 5,
    marginRight: 25,
    backgroundColor: Colors.btnActive,
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  iconStyle: {
    width: 45,
    height: 45,
    marginTop: 0,
    borderWidth: 1,
    marginBottom: 0,
    marginLeft: 25,
  }, imageContainer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginLeft: '5%',
  }, dropDownMain: {
    marginTop: 25,
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 8,
    backgroundColor: Colors.appBackgroundColor,
  },
  dropDownStyle: {
    flex: 1,
    marginTop: 0,
    width: '90%',
    height: 80,
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropDownSelectedItemView: {
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  selectedItemTextContainer: {
    width: '90%',
    paddingLeft: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderLineColorLightGray
  },
  sortTypeText: {
    flex: 1,
    color: Colors.black,
    fontSize: 15,
    marginTop: 5,
    fontWeight: 'bold'
  },
  dropDownIcon: {
    width: '10%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(AccessorySalePage2);