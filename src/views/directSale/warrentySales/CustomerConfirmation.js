import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, BackHandler, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import * as actions from '../../../actions/index';
import strings from '../../../Language/WarrantyReplacement';
import Utills from '../../../utills/Utills';
import ActIndicator from './ActIndicator';

const { height } = Dimensions.get('screen');
const Utill = new Utills();
class WarrentyReplacementDetailsPage extends Component {
    constructor(props) {
        super(props);
        strings.setLanguage(this.props.lang);
        this.state = {
            apiLoading: false,
            locals: {
                confirmation: strings.confirmation,
                confirmationCaps: strings.confirmCaps,
                title: strings.title,
                api_error_message: strings.api_error_message,
                network_error_message: strings.network_error_message,
                lblError: strings.error
            },
        };
        this.navigatorOb = this.props.navigator;
        this.retry_attempt = 1;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    }

    onHandleBackButton = () => {
        this.navigatorOb.pop();
        return true;
    }

    gotoProvPage2 = (response, data) => {
        this.navigatorOb.push({
            title: '',
            screen: 'DialogRetailerApp.views.WarrentyProvisioning',
            passProps: {
                directSaleOption: 'warrantyreplacement',
                overallStatus: 'TO_BE_PROVISIONED',
                reqBody: data,
                cir: response.data.cir,
                sale_date: response.data.sale_date,
                screenTitle: this.state.locals.title
            }
        });
    }

    goToDirectSaleMain() {
        this.navigatorOb.resetTo({
            screen: 'DialogRetailerApp.views.DirectSale'
        })
    }

    displayCommonAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            title: this.state.locals.title,
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                onPressOK: () => {
                    messageType == 'successWithOk' ?
                        this.goToDirectSaleMain()
                        :
                        this.navigatorOb.pop({
                            animated: true,
                            animationType: 'fade',
                        });
                },
            }
        });
    }

    confirmReplacement() {
        let retry_attempt = this.retry_attempt;

        let data = {
            dtv_acc_no: this.props.info.connectionNo,
            contactAlt: this.props.info.contactNoAlt,
            retry_attempt: retry_attempt,
            contact: this.props.info.contactNo,
            conn_type: this.props.connectionType,
            version_upgrade: 1
        }
        let obj = {};
        obj = { ...this.props.info.serial, main_item: this.props.info.main_item, replaceble_item: this.props.info.replace_item };

        this.props.info.card_Less !== '' ? obj.card_less = this.props.info.card_Less : true;
        this.props.info.sub_items.length > 0 ? obj.sub_items = this.props.info.sub_items : true;
        this.props.info.sim !== null ? obj.sim = this.props.info.sim : true;
        data.serial_list = [obj];

        this.setState({ apiLoading: true }, () => {
            Utill.apiRequestPost('completeWarranty', 'WarrantyReplacement', 'cfss', data,
                (response) => {
                    this.setState({ apiLoading: false }, () => {
                        if (response.data.warning_msg == true) {
                            this.navigatorOb.push({
                                title: '',
                                screen: 'DialogRetailerApp.views.CommonAlertModel',
                                passProps: {
                                    messageType: 'defaultAlert',
                                    messageHeader: '',
                                    messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : '',
                                    messageFooter: '',
                                    onPressOK: () => {
                                        if (response.data.prov_status == 'to_be_provision') {
                                            const neWSerialArray = data.serial_list.map(item => {
                                                const obj = { ...item, status: 'TO_BE_PROVISIONED' }
                                                return obj;
                                            });

                                            data.serial_list = neWSerialArray;
                                            this.gotoProvPage2(response, data)
                                        } else {
                                            this.goToDirectSaleMain()
                                        }
                                    }
                                }
                            });
                        } else if (response.data.prov_status == 'to_be_provision') {
                            const neWSerialArray = data.serial_list.map(item => {
                                const obj = { ...item, status: 'TO_BE_PROVISIONED' }
                                return obj;
                            });

                            data.serial_list = neWSerialArray;

                            this.gotoProvPage2(response, data);
                        } else {
                            this.displayCommonAlert('successWithOk', '', response.data.info ? response.data.info : response.info ? response.info : '', '');
                        }
                    });
                },
                (response) => {
                    this.setState({ apiLoading: false }, () => {
                        if (response.data.retry == true) {
                            retry_attempt += 1;
                            this.retry_attempt = retry_attempt;

                            this.displayRetryConsumptionAlert('failureWithRetry', '', response.data.error, '');
                        } else {
                            this.displayCommonAlert('defaultError', this.state.locals.lblError, response.data.error);
                        }
                    });
                },
                (defaultError) => {
                    this.setState({ apiLoading: false }, () => {
                        let error;
                        if (defaultError == 'No_Network') {
                            error = this.state.locals.network_error_message;
                            this.showNoNetworkModal(() => this.confirmReplacement());
                        } else if (defaultError == 'Network Error') {
                            error = this.state.locals.network_error_message;
                            this.navigatorOb.showSnackbar({ text: error });
                        } else {
                            error = this.state.locals.api_error_message;
                            this.navigatorOb.showSnackbar({ text: error });
                        }
                    });
                });
        });
    }

    displayRetryConsumptionAlert(messageType, messageHeader, messageBody, messageFooter) {
        this.navigatorOb.push({
            screen: 'DialogRetailerApp.views.CommonAlertModel',
            passProps: {
                messageType: messageType,
                messageHeader: messageHeader,
                messageBody: messageBody,
                messageFooter: messageFooter,
                onPressRetry: () => { this.confirmReplacement(); }
            },
            overrideBackPress: true
        });
    }

    showNoNetworkModal(retryFunc) {
        this.props.navigator.showModal({
            screen: 'DialogRetailerApp.modals.NetworkScreen',
            passProps: { retryFunc: retryFunc }
        });
    }

    showIndicator() {
        if (this.state.apiLoading) {
            return (
                <View style={styles.indiView}>
                    <ActIndicator animating={true} />
                </View>
            );
        } else return true;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.showIndicator()}
                <View style={styles.topContainer}>
                    <Text style={styles.topContainerTxt}>
                        {this.state.locals.confirmation}
                    </Text>
                </View>
                <View style={styles.contentContainer}>

                    {this.props.replacementInfo.map((val, i) =>
                        <View key={i}>
                            {(val.value !== undefined && val.value !== null && val.value !== '') ?
                                <View key={i}>
                                    {<View style={styles.detailElimentContainer}>
                                        <View style={styles.elementLeft}>
                                            <Text style={styles.normalTxt}>
                                                {val.text}
                                            </Text>
                                        </View>
                                        <View style={{ marginRight: 8 }}>
                                            <Text style={styles.contentContainerTxt}>:</Text>
                                        </View>
                                        <View style={styles.elementRight}>
                                            <Text style={styles.contentContainerTxt}>{val.value}</Text>
                                        </View>
                                    </View>}
                                </View>
                                : true}
                        </View>)}
                    <View style={styles.payBtnBtnContainer}>
                        <TouchableOpacity style={styles.filterText} onPress={() => this.confirmReplacement()}>
                            <Text style={styles.normalTxt}>{this.state.locals.confirmationCaps}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',

        backgroundColor: Colors.appBackgroundColor
    },
    contentContainer: {
        flex: 0.9
    },
    topContainer: {
        flex: 0.1,
        marginTop: 5,
        height: 100,
        marginLeft: 18,
        marginRight: 18,
        alignItems: 'center'
    },
    payBtnBtnContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginVertical: 10,
        marginRight: 25
    },
    payBtn: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        paddingRight: 10,
        paddingLeft: 10,
        borderRadius: 5,
        backgroundColor: Colors.btnActive
    },
    topContainerTxt: {
        marginTop: 7,
        marginBottom: 7,
        color: Colors.btnDeactiveTxtColor,
        fontSize: Styles.btnFontSize,
        color: Colors.btnActiveTxtColor,
        fontWeight: 'bold',
    }, contentContainerTxt: {
        color: Colors.btnDeactiveTxtColor,
        fontSize: 16,
        color: Colors.btnActiveTxtColor,
        fontWeight: 'bold',
    },
    normalTxt: {
        color: Colors.btnDeactiveTxtColor,
        fontSize: 16,
        color: Colors.btnActiveTxtColor,

    },
    detailElimentContainer: {
        flexDirection: 'row',
        borderWidth: 0,
        marginBottom: 10,
        marginHorizontal: 15
    },
    elementLeft: {
        flex: 1,
        borderWidth: 0
    },
    elementRight: {
        flex: 1,
        borderWidth: 0
    },
    indiView: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
    },
    filterText: {
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 25,
        paddingRight: 25,
        backgroundColor: Colors.btnActive,
        color: Colors.black,
        borderRadius: 5,
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => {
    const warrantyReplacement = state.warrantyReplacement;
    const lang = state.lang.current_lang;
    return { lang, warrantyReplacement }
};

export default connect(mapStateToProps, actions)(WarrentyReplacementDetailsPage);