import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';

import Colors from '../../../config/colors';
import * as actions from '../../../actions';
import strings from '../../../Language/Wom';
class GeneralExceptionSerialModal extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language)
    this.state = {
      locals: {
        lblRescan: strings.lblRescan,
        lblCancel: strings.cancel,
        lblReplace: strings.btnReplace
      }
    }
  }
  onBnPress = () => {
    this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
              <Text style={styles.headerLable}>{this.props.lable}</Text>
              <Text style={styles.headerLable}>:</Text>
              <Text style={styles.headerItem}>{this.props.item}</Text>
            </View>

            <View>
              <Text style={styles.descriptionText}>{this.props.descriptionText1} : {this.props.descriptionText2}</Text>
            </View>
            <View>
              <Text style={styles.descriptionText}>{this.props.descriptionText3}</Text>
            </View>

            <View>
              {this.props.isSuccess ?
                <View style={styles.buttonRow}>
                  <TouchableOpacity onPress={() => this.onBnPress()} style={styles.button} >
                    <Text style={styles.bottomCantainerBtnTxtBlack}>
                      {this.state.locals.lblCancel}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onBnPress()} style={styles.button}>
                    <Text style={styles.bottomCantainerBtnTxt}>
                      {this.state.locals.lblReplace}</Text>
                  </TouchableOpacity>
                </View>
                :
                <View style={styles.buttonRow}>
                  <TouchableOpacity onPress={() => this.onBnPress()} style={styles.button} >
                    <Text style={styles.bottomCantainerBtnTxt}>
                      {this.state.locals.lblRescan}</Text>
                  </TouchableOpacity>
                </View>}
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },
  buttonRow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingTop: 20
  },

  button: {
    paddingLeft: 20,
    paddingRight: 20
  },

  innerContainer: {
    backgroundColor: Colors.backgroundColorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },
  descriptionText: {
    alignItems: 'flex-start',
    fontSize: 16,
    color: Colors.grey,
    marginBottom: 20,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'

  },
  headerLable: {
    alignItems: 'flex-start',
    fontSize: 16,
    color: Colors.colorBlack,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  headerItem: {
    alignItems: 'flex-start',
    fontSize: 16,
    color: Colors.colorBlack,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  bottomCantainerBtn: {
    alignItems: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtn: {
    width: 100

  },
  bottomCantainerBtnTxt: {
    fontSize: 16,
    color: Colors.colorGreen

  },
  bottomCantainerBtnTxtBlack: {
    fontSize: 16
  }

});

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
}

export default connect(mapStateToProps, actions)(GeneralExceptionSerialModal);
