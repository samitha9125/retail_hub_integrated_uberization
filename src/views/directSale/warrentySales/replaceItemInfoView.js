import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, BackHandler, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Colors from './../../../config/colors';
import Styles from '../../../config/styles';
import DropDownInput from '../../wom/components/DropDownInput';
import * as actions from '../../../actions/index';
import Utills from '../../../utills/Utills';
import strings from './../../../Language/WarrantyReplacement';
import strings2 from '../../../Language/Wom';
import ActIndicator from './ActIndicator';
import { Header } from './Header';
import RadioButton from '../../wom/components/RadioButtonComponent';
import SerialListComponent from '../../wom/components/SerialListComponent';

const Utill = new Utills();
const { height } = Dimensions.get('screen');
class ReplaceItemInfoView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    strings2.setLanguage(this.props.lang);
    this.state = {
      conNumber: '',
      numberType: '',
      lobArray: [],
      dropDownDataX: [],
      newSerialResponse: [],
      apiLoading: false,
      cxContactNo: '',
      accessoryType: '',
      selectedReplaceType: '',
      scannedOldSerial: '',
      scannedOldSerialValidated: false,
      cardStatus: null,
      replacebleItemTypes: [],
      subItems: [],

      oldPowerSupply: {},
      oldRCU: {},
      oldSIM: '',
      oldRCUScanned: false,
      oldPowerScanned: false,

      newSerial: {},
      newSIMSerial: {},

      subItemTypes: [],
      warrentyInfo: {},
      conDetails: {
        connectionNumber: this.props.conNumber,
        contactNumber: this.props.cxContactNo,
        contactNoAlt: this.props.contactNoAlt
      },

      selectedItem: {},
      card_less: null,
      selectedReplacement: {},
      isViewNewStbSerial: false,

      refreshNewSIMSerial: false,
      refreshNewSerialData: false,
      refreshOldSIMData: false,
      refreshOldSTBData: false,
      refreshRCUData: false,
      refreshPowerData: false,

      isContinueButtonDisabled: true,

      hasOldSIM: false,
      rcuEnable: false,
      powerSupplyEnable: false,
      contBtnEnable: false,
      scanPowerSupplyserail: '',
      scanRCUserial: '',
      newPowerSupply: '',
      newRcu: '',
      newRcuserial: '',
      newPowerSupplyserial: '',
      locals: {
        warrantyItem: strings.warrantyItem,
        btnContinue: strings.continue,
        newSTBType: strings.newSTBType,
        itemType: strings.itemType,
        lblOldPowerSupplyserial: strings.lblOldPowerSupplySerial,
        lblOldRCUSerial: strings.lbloldRCUSerial,
        connectionNo: strings.connectionNo,
        contactNo: strings.contactNo,
        altNo: strings.contactNoAlt,
        type: strings.accessoryType,
        newWarrantyPeriod: strings.newWarrantyPeriod,
        powerSupplyWarranty: strings.powerSupplyWarranty,
        rcuWarranty: strings.rcuWarranty,
        warrentyText1: strings.warrentyText1,
        warrentyText2: strings.warrentyText2,
        title: strings.title,
        oldSerial: strings.oldSerial,
        oldSIMSerial: strings.oldSIMSerial,
        newSerial: strings.newSerial,
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        lblWarrantyPeriodExpired: strings.lblWarrantyPeriodExpired,
        notEligibleWarranty: strings.notEligibleWarranty,
        Serialnumber: strings.Serialnumber,
        systrmError: strings.systrmError,
        systemError: strings.systemError,
        lblSerial: strings.lblSerial,
        lblNew: strings.lblNew,
        lblCardfull: strings2.cardfull,
        lblCardless: strings2.cardless,
        newlable: strings2.newlable,
        old: strings2.old,
        oldLabletoReplace: strings2.oldLabletoReplace,
        newLabelToReplace: strings2.newLabelToReplace,
        lblSerialLower: strings2.lblSerialLower,
        powerSupplySerial: strings.powerSupplySerial,
        rcuSerial: strings.rcuSerial

      },
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.getLOBTypes();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.navigatorOb.pop();
  }

  capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  gotoDetails = () => {
    let replacementInfo;

    replacementInfo = [
      { text: this.state.locals.connectionNo, value: this.state.conDetails.connectionNumber },
      { text: this.state.locals.contactNo, value: this.state.conDetails.contactNumber }
    ]

    this.state.conDetails.contactNoAlt !== '' ? replacementInfo.push({ text: this.state.locals.altNo, value: this.state.conDetails.contactNoAlt }) : true;

    replacementInfo.push({ text: this.state.locals.type, value: this.state.selectedItem.type });
    replacementInfo.push({ text: this.capitalizeFirstLetter(this.renderNewSerialLabel()), value: this.state.newSerial.serial });
    replacementInfo.push({ text: this.state.selectedItem.type + ' ' + this.state.locals.newWarrantyPeriod, value: this.state.warrentyInfo.warranty });

    if (this.state.selectedItem.type == 'STB') {
      if (this.state.subItems.length > 0) {
        this.state.subItems.map(item => {
          if (item.equipment_type == 'DTV_POWER_SUPPLY' && item.serial == this.state.scanPowerSupplyserail) {
            replacementInfo.push({ text: this.capitalizeFirstLetter(this.state.locals.powerSupplySerial), value: this.state.newPowerSupplyserial })
            replacementInfo.push({ text: this.capitalizeFirstLetter(this.state.locals.powerSupplyWarranty), value: item.warranty })
          }
          else if (item.equipment_type == 'DTV_RCU' && item.serial == this.state.scanRCUserial) {
            replacementInfo.push({ text: this.capitalizeFirstLetter(this.state.locals.rcuSerial), value: this.state.newRcuserial })
            replacementInfo.push({ text: this.capitalizeFirstLetter(this.state.locals.rcuWarranty), value: item.warranty })
          }
        });
      }

    }

    let sub_items = [];
    let card_Less = '';

    let sim = null;

    if (this.state.selectedReplacement.item_condition) {
      if (this.state.rcuEnable == true && this.state.powerSupplyEnable == true) {
        console.log('acessces two item')
        sub_items.push(this.state.oldRCU);
        sub_items.push(this.state.oldPowerSupply);
        console.log('acessces two item', sub_items)
      } else if (this.state.powerSupplyEnable == true) {
        sub_items.push(this.state.oldPowerSupply);
        console.log('acessces powersupply item', sub_items)
      }
      else if (this.state.rcuEnable == true) {
        sub_items.push(this.state.oldRCU);
        console.log('acess oldRCU item', sub_items)
      }
    }


    this.navigatorOb.push({
      title: this.state.locals.title,
      screen: 'DialogRetailerApp.views.WarrentyReplacementDetailsPage',
      passProps: {
        cxContactNo: this.props.cxContactNo,
        connectionType: this.props.connectionType,
        conNumber: this.props.conNumber,
        replacementInfo,
        info: {
          connectionNo: this.state.conDetails.connectionNumber,
          contactNo: this.state.conDetails.contactNumber,
          contactNoAlt: this.state.conDetails.contactNoAlt,
          serial: this.state.newSerial,
          main_item: this.state.warrentyInfo,
          selectedType: this.state.selectedItem.type,
          replace_item: this.state.selectedReplacement,
          card_Less,
          sub_items,
          sim
        }
      }
    });
  }

  getLOBTypes() {
    const data = {
      request_type: 'WARRANTY_REPLACE'
    };

    this.setState({ apiLoading: true }, () => {
      Utill.apiRequestPost('GetAccessoryTypes', 'warrantyReplacement', 'cfss', data,
        this.getAccessoryTypesSuccessCb, this.getAccessoryTypesFailureCb, this.getAccessoryTypesExCb);
    });
  }

  getAccessoryTypesSuccessCb = (response) => {
    let lob = [];
    response.data.data.forEach((item) => { lob.push(item.name); });
    this.setState({ apiLoading: false, lobArray: response.data.data, dropDownDataX: lob });
  }

  getAccessoryTypesFailureCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      this.displayCommonAlert('defaultAlert', 'Error', response.data.error ? response.data.error : this.state.locals.systemError, '')
    });
  }

  getAccessoryTypesExCb = (response) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (response == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(() => this.getLOBTypes());
      } else if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: { retryFunc: retryFunc }
    });
  }

  displayCommonAlert(messageType, messageHeader, messageBody, messageFooter, isLongPopup) {
    this.navigatorOb.push({
      title: this.state.locals.title,
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: messageFooter,
        isLongPopup,
        onPressOK: () => {
          if (messageType == 'error' && messageHeader == false) {
            this.okHandler();
          }
          this.navigatorOb.pop({
            animated: false,
            animationType: 'fade',
          });
        },
      }
    });
  }

  replaceItemSelected(idx, value) {
    if (value) {
      this.setState({
        isContinueButtonDisabled: true,
        subItemTypes: [],
        refreshNewSerialData: true,
        refreshNewSIMSerial: true,
        refreshPowerData: true,
        refreshRCUData: true,
        selectedReplacement: this.state.replacebleItems[idx],
        selectedReplaceType: value,
        isViewNewStbSerial: true,
        cardStatus: null,
        oldPowerScanned: false,
        oldRCUScanned: false,
        rcuEnable: false,
        powerSupplyEnable: false,
        contBtnEnable: false,
        scanPowerSupplyserail: '',
        scanRCUserial: '',
        newSerial: ''
      }, () => {
        if (this.state.selectedReplacement.item_condition == 'Used') { this.setState({ cardStatus: 'N' }); }
      })
    } else {
      this.setState({ subItemTypes: [] });
    }
  }

  lobSelected(idx, value) {
    this.setState({
      selectedItem: this.state.lobArray[idx],
      accessoryType: value,
      selectedReplaceType: '',
      scannedOldSerial: '',
      scannedOldSerialValidated: false,

      refreshNewSerialData: true,
      refreshNewSIMSerial: true,
      refreshOldSIMData: true,
      refreshOldSTBData: true,
      refreshPowerData: true,
      refreshRCUData: true,

      hasOldSIM: false,
      replacebleItemTypes: [],
      selectedReplacement: {},
      subItemTypes: [],
      isContinueButtonDisabled: true,
      isViewNewStbSerial: false,
      card_less: null,
      rcuEnable: false,
      powerSupplyEnable: false,
      contBtnEnable: false
    });
  }

  oldScannerResponse = (isValidated, response) => {
    let subList = [];
    let replaceList = [];
    let subItems = [];
    let replacebleItems = [];
    let oldRCU = {};
    let oldPowerSupply = {};
    let card_less = null;

    if (response.replaceble_items && (response.replaceble_items.length > 0)) {
      response.sub_items.forEach((item) => {
        subList.push(item.type);
      });
      response.replaceble_items.forEach((item) => {
        replaceList.push(item.display_name);
      });
      subItems = response.sub_items.map(item => {
        if (item.warranty_detail.equipment_type == 'DTV_RCU') {
          oldRCU = item.warranty_detail;
        } else if (item.warranty_detail.equipment_type == 'DTV_POWER_SUPPLY') {
          oldPowerSupply = item.warranty_detail;
        }
        return item.warranty_detail;
      });
      replacebleItems = response.replaceble_items;
    }
    response.main_item.card_less ? card_less = response.main_item.card_less : card_less = null;
    this.setState({
      warrentyInfo: response.main_item, scannedOldSerialValidated: true,
      card_less, oldRCU, oldPowerSupply,
      subItemTypes: subList, replacebleItemTypes: replaceList, subItems,
      replacebleItems, refreshOldSTBData: false
    }, () => {
      this.displayConfirmAlert(response.main_item.warranty_status, response.main_item.warranty_period, response.main_item);
    });
  }

  displayConfirmAlert(warrantyStatus, warrantyPeriod, item) {
    let serialNo = '';
    let description1 = '';
    let description2 = '';
    if (warrantyStatus) {
      description1 = this.state.locals.warrentyText1;
      description2 = this.state.locals.warrentyText2;
    } else {
      serialNo = this.state.locals.Serialnumber + item.equipment_serial;
      description1 = this.state.locals.lblWarrantyPeriodExpired;
      description2 = this.state.locals.notEligibleWarranty;
    }

    itemName = this.state.locals.itemType + ' : ' + item.name

    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.WarrantyConfirmAlertScreen',
      passProps: {
        description1, description2, warrantyPeriod, itemName, warrantyStatus, directSale: true, serialNo,
        refreshSerial: () => { this.setState({ refreshOldSTBData: true }) },
        onConfirmed: (value) => { },
        okPressed: () => {
          this.setState({ card_less: null })
        }
      }
    });
  }

  oldSIMScannerResponse = (isValidated, response) => {
    this.setState({ oldSIM: response, refreshOldSIMData: false })
  }

  oldRcuScannerResponse = (isValidated, response) => {
    console.log('OLD RCU SCANNER RESPONCE::', response)
    let contBtnEnable = false;
    if (this.state.rcuEnable == true && this.state.powerSupplyEnable == true) {
      response.serial !== '' && this.state.newSerial !== '' && this.state.scanPowerSupplyserail !== '' ? contBtnEnable = true : contBtnEnable = false
    }
    else {
      response.serial !== '' && this.state.newSerial !== '' ? contBtnEnable = true : contBtnEnable = false
    }
    this.setState({ oldRCU: response, refreshRCUData: false, oldRCUScanned: true, contBtnEnable, scanRCUserial: response.serial })
  }

  oldProwerSupplyResponse = (isValidated, response) => {
    console.log('OLD POWER SUPPLY :: ', response);
    let contBtnEnable = false;
    if (this.state.rcuEnable == true && this.state.powerSupplyEnable == true) {
      this.state.scanRCUserial !== '' && this.state.newSerial !== '' && response.serial !== '' ? contBtnEnable = true : contBtnEnable = false
    } else if (this.state.powerSupplyEnable == true) {
      response.serial !== '' ? contBtnEnable = true : contBtnEnable = false
    } else {
      response.serial !== '' && this.state.newSerial !== '' ? contBtnEnable = true : contBtnEnable = false
    }
    this.setState({
      oldPowerSupply: response, refreshPowerData: false, oldPowerScanned: true, contBtnEnable,
      scanPowerSupplyserail: response.serial
    })
  }

  newScannerResponse = (isValidated, response) => {
    let contBtnEnable = false;
    let rcuEnable = false;
    let newPowerSupply
    let newRcu
    let newRcuserial
    let newPowerSupplyserial
    let powerSupplyEnable = false;
    if (response.inner_items.length > 1) {
      response.inner_items.map((item) => {
        if (item.type == 'Remote') {
          rcuEnable = true
          newRcu = item.type
          newRcuserial = item.serial
        }
        if (item.type == 'PowerSupply') {
          powerSupplyEnable = true
          newPowerSupply = item.type
          newPowerSupplyserial = item.serial
        }
      })
    } else {
      contBtnEnable = true
    }

    this.setState({
      newSerial: response, refreshNewSerialData: false, rcuEnable, powerSupplyEnable, contBtnEnable, newRcu, newRcuserial, newPowerSupply, newPowerSupplyserial
    });
  }


  renderOldSerialLabel() {
    var oldLabletoReplace = this.state.locals.oldLabletoReplace.toString();
    let oldLable = oldLabletoReplace.replace(/<item>/g, this.state.accessoryType);
    return oldLable
  }

  renderNewSerialLabel() {
    var newLabletoReplace = this.state.locals.newLabelToReplace.toString();
    let newLable = newLabletoReplace.replace(/<item>/g, this.state.accessoryType);
    return newLable
  }


  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  renderOldSelectedItem = () => {
    if (this.state.accessoryType !== '') {
      return (
        <View>
          <View style={{ marginHorizontal: 20 }}>
            <SerialListComponent
              refreshData={this.state.refreshOldSTBData}
              apiParams={{ conNo: this.state.conDetails.connectionNumber }}
              manualLable={this.renderOldSerialLabel()}
              serialData={{ ...this.state.selectedItem, sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N' }}
              isAllSerialsValidated={this.oldScannerResponse}
              navigatorOb={this.props.navigator}
              actionName='warrantyDetails'
              controllerName='warrantyReplacement'
              moduleName='cfss'
            />
            {this.state.accessoryType == 'STB' ?
              [this.state.card_less == 'N' ?
                <SerialListComponent
                  serialData={{
                    name: 'SIM', type: 'SIM', sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N',
                    oldSerial: [{ serial: this.state.warrentyInfo.card_sim, name: 'SIM', type: 'SIM' }]
                  }}
                  manualLable={this.renderOldSerialLabel()}
                  refreshData={this.state.refreshOldSIMData}
                  isAllSerialsValidated={this.oldSIMScannerResponse}
                  navigatorOb={this.props.navigator}
                  actionName='warrantyDetails'
                  controllerName='warrantyReplacement'
                  moduleName='cfss'
                  apiDisable={true}
                />
                :
                true]
              :
              true}
          </View>
        </View>
      )
    } else return true
  }

  renderStbSerials = () => {

    if (this.state.accessoryType == 'STB' && this.state.selectedReplacement.item_condition) {
      return (
        <View style={{ marginHorizontal: 20 }}>
          {this.state.rcuEnable == true ?
            <SerialListComponent
              refreshData={this.state.refreshRCUData}
              apiParams={{ selectedItem: this.state.selectedItemType }}
              serialData={{
                sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N',
                name: this.state.locals.lblOldRCUSerial,
                type: 'RCU',
                oldSerial: [this.state.oldRCU]
              }}
              isAllSerialsValidated={this.oldRcuScannerResponse}
              navigatorOb={this.props.navigator}
              actionName='warrantyDetails'
              controllerName='warrantyReplacement'
              moduleName='cfss'
              apiDisable={true}
            /> : null}
          {this.state.powerSupplyEnable == true ?
            <SerialListComponent
              refreshData={this.state.refreshPowerData}
              apiParams={{ selectedItem: this.state.selectedItemType }}
              serialData={{
                sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N',
                name: this.state.locals.lblOldPowerSupplyserial,
                type: 'Power Supply',
                oldSerial: [this.state.oldPowerSupply]
              }}
              isAllSerialsValidated={this.oldProwerSupplyResponse}
              navigatorOb={this.props.navigator}
              actionName='warrantyDetails'
              controllerName='warrantyReplacement'
              moduleName='cfss'
              apiDisable={true}
            />
            : null}
        </View>
      )
    }
  }

  renderNewSerial = () => {
    if (this.state.accessoryType == 'STB') {
      if (this.state.isViewNewStbSerial) {
        return (
          <View style={{ marginHorizontal: 20 }}>
            <SerialListComponent
              refreshData={this.state.refreshNewSerialData}
              apiParams={{
                request_type: 'WARRANTY_REPLACE',
                app_flow: 'WARRANTY_REPLACE',
                ownership: 'CxOwned',
              }}
              manualLable={this.renderNewSerialLabel()}
              serialData={{ ...this.state.selectedItem, sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N', stb_type: this.state.selectedReplacement }}
              isAllSerialsValidated={this.newScannerResponse}
              navigatorOb={this.props.navigator}
              actionName='validateSerialNew'
              controllerName='serial'
              moduleName='wom'
            />
          </View>
        )
      } else return true
    } else {
      return (
        <View style={{ marginHorizontal: 20 }}>
          <SerialListComponent
            refreshData={this.state.refreshNewSerialData}
            apiParams={{
              request_type: 'WARRANTY_REPLACE',
              app_flow: 'WARRANTY_REPLACE',
              ownership: 'CxOwned',
            }}
            manualLable={this.renderNewSerialLabel()}
            serialData={{ ...this.state.selectedItem, sale_type: 'REPLACE', qty: 1, price: '0', basic: 'N' }}
            isAllSerialsValidated={this.newScannerResponse}
            navigatorOb={this.props.navigator}
            actionName='validateSerialNew'
            controllerName='serial'
            moduleName='wom'
          />
        </View>
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title} />
        {this.showIndicator()}
        <View style={styles.topContainer} />
        <View style={styles.middleContainer}>
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            <View style={styles.container}>
              {/* replacement item selection*/}
              <View style={{ marginHorizontal: 20 }}>
                <DropDownInput
                  alignDropDownnormal={true}
                  dropDownTitle={this.state.locals.warrantyItem}
                  hideRightIcon={true}
                  selectedValue={this.state.accessoryType}
                  dropDownData={this.state.dropDownDataX}
                  onSelect={(idx, value) => this.lobSelected(idx, value)}
                />
              </View>
              {/* old serial */}
              {this.renderOldSelectedItem()}
              {((this.state.replacebleItemTypes.length > 0) && (this.state.selectedItem.type === "STB")) &&
                <View>
                  <View style={{ marginHorizontal: 20 }}>
                    <DropDownInput
                      alignDropDownnormal={true}
                      dropDownTitle={this.state.locals.newSTBType}
                      hideRightIcon={true}
                      selectedValue={this.state.selectedReplaceType.length <= 30 ? this.state.selectedReplaceType : this.state.selectedReplaceType.substring(0, 30) + '...'}
                      dropDownData={this.state.replacebleItemTypes}
                      onSelect={(idx, value) => this.replaceItemSelected(idx, value)}
                    />
                  </View>
                </View>}
              {this.renderNewSerial()}
              {this.renderStbSerials()}
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <View style={{ marginRight: 25, marginTop: 15, flex: 0.5 }}>
            <View style={styles.payBtnBtnContainer}>
              {this.state.contBtnEnable ?
                <TouchableOpacity onPress={() => this.gotoDetails()}>
                  <Text style={styles.filterText}>{this.state.locals.btnContinue}</Text>
                </TouchableOpacity>
                :
                <Text style={styles.filterTextDisabled}>{this.state.locals.btnContinue}</Text>}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginRight: 5,
    padding: 5,
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 40,
    marginTop: 15
  },
  payBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.colorPureYellow,
  },
  bottomContainer: {
    flex: 0.15,
  },
  payBtnTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
    marginTop: 8,
  },
  acceptbtn: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  topContainer: {
    flex: 0.05,
  },
  middleContainer: {
    flex: 0.8,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'column',
  }, actIndicator: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }, dropDownMain: {
    marginTop: 8,
    paddingTop: 8,
    paddingBottom: 8,
    marginBottom: 8,
    backgroundColor: Colors.appBackgroundColor,
  },
  dropDownStyle: {
    flex: 1,
    marginTop: 0,
    width: '90%',
    height: 80,
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropDownSelectedItemView: {
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    width: '100%'
  },
  selectedItemTextContainer: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderLineColorLightGray
  },
  sortTypeText: {
    flex: 1,
    color: Colors.black,
    fontSize: 15,
    marginTop: 5,
    fontWeight: 'bold'
  },
  dropDownIcon: {
    width: '10%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  }, dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  iconStyle: {
    width: 40,
    height: 40,
    marginTop: 0,
    borderWidth: 1,
    marginBottom: 0,
    marginLeft: 25
  },
  imageContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 15,
    marginTop: -50
  },
  viewButton: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: '5%',
    paddingRight: '1%'
  },
  buttonContainer: {
    width: '35%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnActive,
    marginRight: 25,
  },
  buttonContainerDisabled: {
    width: '35%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnDisable,
    marginRight: 25
  },
  RaidoView: {
    marginVertical: 25,
    marginHorizontal: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  RadioFirstStyle: {
    alignItems: 'flex-start',
    flex: 1
  },
  RadioSecondStyle: {
    alignItems: 'flex-start',
    flex: 1
  },
  radioButtonLabelStyle: {
    marginLeft: 10,
  },
  mainContainer: {
    marginTop: 10,
  },
  indiView: {
    backgroundColor: Colors.appBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  filterText: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: Colors.button.activeColor,
    color: Colors.black,
    borderRadius: 5,
    textAlign: 'center'
  },
  filterTextDisabled: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: Colors.btnDisable,
    color: Colors.black,
    borderRadius: 5,
    textAlign: 'center'
  },
});

const mapStateToProps = (state) => {
  const warrantyReplacement = state.warrantyReplacement;
  const lang = state.lang.current_lang;
  return { lang, warrantyReplacement };
};

export default connect(mapStateToProps, actions)(ReplaceItemInfoView);