import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard, BackHandler, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

import Colors from './../../../config/colors';
import Styles from '../../../config/styles';
import * as actions from '../../../actions/index';
import * as Utils from '../../../utills/UtilsCfss';
import Utills from '../../../utills/Utills';
import { Header } from './Header';
import ActIndicator from './ActIndicator';
import strings from './../../../Language/WarrantyReplacement';
import TextFieldComponent from '../../wom/components/TextFieldComponent';

const Utill = new Utills();
const { height } = Dimensions.get('screen');
class WarrentyReplacement extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.lang);
    this.state = {
      apiLoading: false,
      isconfirmable: false,
      isContactAvailable: false,
      conNumber: '',
      cxContactNo: '',
      contactNoAlt: '',
      connection_no_error: '',
      alt_conn_error: '',
      connectionType: '',
      disableAltNo: true,
      locals: {
        network_error_message: strings.network_error_message,
        api_error_message: strings.api_error_message,
        connectionNo: strings.connectionNo,
        contactNo: strings.contactNo,
        altContactNo: strings.contactNoAlt,
        btnContinue: strings.continue,
        invalidConnectionNo: strings.invalidConnectionNo,
        invalidContactNo: strings.invalidContactNo,
        noRegContactNo: strings.noRegContactNo,
        error: strings.error,
        title: strings.title,
      },
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    this.props.warratyReplacementReset();
    this.props.resetMobileActivationState();
    this.navigatorOb.pop();
  }

  checkWarrantyContact() {
    Keyboard.dismiss();
    this.setState({ apiLoading: true }, () => {
      const data = {
        connectionNumber: this.state.conNumber,
        LOB: this.props.warranty_replacement_get_lob,
      };
      Utill.apiRequestPost('WarrantyContact', 'WarrantyReplacement', 'cfss', data,
        this.WarrantyContactSuccessCb, this.WarrantyContactFailureCb, this.WarrantyContactExCb);
    });
  }

  WarrantyContactSuccessCb = (response) => {
    this.setState({
      cxContactNo: '',
      connectionType: response.data.connType ? response.data.connType : '',
      apiLoading: false,
    }, () => {
      try {
        if (response.data.data) {
          const cxContactNo = response.data.data;
          this.setState({ cxContactNo, isContactAvailable: true }, () => this.checkBtn());
        } else {
          this.checkBtn();
          this.displayAlert('defaultAlert', '', response.data.error, '');
        }
      } catch (error) {
        console.log('error ', error);
      }
    });
  }

  WarrantyContactFailureCb = (response) => {
    this.setState({
      apiLoading: false,
      cxContactNo: '',
      isContactAvailable: false
    }, () => {
      this.checkBtn();
      this.displayAlert('defaultAlert', this.state.locals.error, response.data.error, '');
    });
  }

  WarrantyContactExCb = (response) => {
    this.setState({
      apiLoading: false,
      isContactAvailable: false,
      cxContactNo: ''
    }, () => {
      let error;
      if (response == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(() => this.checkWarrantyContact());
      } else if (response == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  validateConnectionNumber(conn) {
    if (Utils.lteNumValidate(conn)) { return true }
    else if (Utils.dtvDirectSaleNumValidate(conn)) { return true }
    else { return false }
  }

  checkBtn() {
    if (((this.state.contactNoAlt !== '' && Utils.gsmNumbervalidate(this.state.contactNoAlt)) || this.state.cxContactNo !== '') && (this.state.conNumber !== '' && Utils.dtvDirectSaleNumValidate(this.state.conNumber))) {
      this.setState({ isconfirmable: true });
    } else {
      this.setState({ isconfirmable: false });
    }
  }

  onChangeConnectionNo(text) {
    this.setState({ conNumber: text, cxContactNo: '' }, () => {
      let status = this.validateConnectionNumber(this.state.conNumber);
      if (status == true) {
        this.setState({ connection_no_error: '', disableAltNo: false }, () => this.checkWarrantyContact());
      } else {
        this.setState({ connection_no_error: this.state.locals.invalidConnectionNo, disableAltNo: true });
      }
    });
  }

  onChangeAltContactNo = (contactAlt) => {
    this.setState({ contactNoAlt: contactAlt }, () => {
      if (this.state.contactNoAlt !== '' && Utils.gsmNumbervalidate(this.state.contactNoAlt)) {
        this.setState({ alt_conn_error: '' }, () => {
          Keyboard.dismiss();
          this.checkBtn();
        });
      } else {
        this.setState({ alt_conn_error: this.state.locals.invalidContactNo });
      }
    });
  }

  gotoWarrantyReplacementPage() {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.WarrentyReplacementItemInfo',
      passProps: {
        screenTitle: this.state.locals.title,
        cxContactNo: this.state.cxContactNo,
        connectionType: this.state.connectionType,
        conNumber: this.state.conNumber,
        contactNoAlt: this.state.contactNoAlt
      }
    });
  }

  displayAlert(messageType, messageHeader, messageBody, MessageBody2) {
    this.navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: messageType,
        messageHeader: messageHeader,
        messageBody: messageBody,
        messageFooter: MessageBody2,
        onPressOK: () => { }
      },
      overrideBackPress: true,
    });
  }

  showIndicator() {
    if (this.state.apiLoading) {
      return (
        <View style={styles.indiView}>
          <ActIndicator animating={true} />
        </View>
      );
    } else return true;
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title} />
        {this.showIndicator()}
        <View style={styles.topContainer} />
        <View style={styles.middleContainer}>
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            <View style={styles.container}>
              <View style={{ marginHorizontal: 20 }}>
                <TextFieldComponent
                  label={this.state.locals.connectionNo}
                  title={''}
                  keyboardType={'numeric'}
                  onChangeText={(text) => this.onChangeConnectionNo(text)}
                  error={this.state.connection_no_error}
                />
              </View>
              <View style={{ marginHorizontal: 20 }}>
                <TextFieldComponent
                  label={this.state.locals.contactNo}
                  title={''}
                  keyboardType={'numeric'}
                  disabled={true}
                  value={this.state.cxContactNo}
                />
              </View>
              <View style={{ marginHorizontal: 20 }}>
                <TextFieldComponent
                  label={this.state.locals.altContactNo}
                  title={''}
                  keyboardType={'numeric'}
                  disabled={this.state.disableAltNo}
                  onChangeText={(text) => this.onChangeAltContactNo(text)}
                  error={this.state.alt_conn_error}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            {(!this.state.isconfirmable) ? (
              <TouchableOpacity style={styles.payBtnDisable}>
                <Text style={styles.payBtnTxt}>
                  {this.state.locals.btnContinue}
                </Text>
              </TouchableOpacity>)
              :
              (<TouchableOpacity style={styles.payBtn}
                onPress={() => this.gotoWarrantyReplacementPage()}>
                <Text style={styles.payBtnTxt}>
                  {this.state.locals.btnContinue}
                </Text>
              </TouchableOpacity>)}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    flex: 0.15,
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: 5,
    marginTop: 8,
  },
  payBtn: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    padding: 10,
    borderRadius: 5,
    marginRight: 25,
    backgroundColor: Colors.btnActive
  },
  payBtnDisable: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    padding: 10,
    borderRadius: 5,
    marginRight: 25,
    backgroundColor: Colors.btnDisable
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  topContainer: {
    flex: 0.05,
  },
  middleContainer: {
    flex: 0.8,
    marginLeft: 8,
    marginRight: 8,
    flexDirection: 'column',
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

const mapStateToProps = (state) => {
  const warrantyReplacement = state.warrantyReplacement;
  const lang = state.lang.current_lang;
  return { lang, warrantyReplacement };
};

export default connect(mapStateToProps, actions)(WarrentyReplacement);