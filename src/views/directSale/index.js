import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView, BackHandler } from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import strings1 from '../../Language/WarrantyReplacement';
import strings2 from '../../Language/AccessorySales';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import { Header } from './accessorySales/Header';
import strings from '../../Language/Home.js';
class DirectSale extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    strings1.setLanguage(this.props.Language);
    strings2.setLanguage(this.props.Language)
    this.state = {
      locals: {
        directSales: strings.directSalesTitle,
        lblWarranrtReplacement: strings1.lblReplacement,
        lblAccessorySale: strings2.lblSale,
        saleHeader: strings2.aSaleHeader,
        replaceHeader: strings1.warrantyReplacementCaps
      }
    }
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler() {
    this.navigatorOb.resetTo({
      title: strings.HomeMenu,
      screen: 'DialogRetailerApp.views.HomeTileScreen'
    });   
  }

  gotoWarrantyReplacement() {
    this.navigatorOb.push({
      title: this.state.locals.replaceHeader,
      screen: 'DialogRetailerApp.views.WarrentyReplacement',
      passProps: { screenTitle: this.state.locals.replaceHeader }
    });
  }

  gotoAccessorySales() {
    this.navigatorOb.push({
      title: this.state.locals.saleHeader,
      screen: 'DialogRetailerApp.views.AccessorySalePage1',
      passProps: { screenTitle: this.state.locals.saleHeader }
    });
  }

  render() {
    return (
      <ScrollView style={styles.background}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.directSales} />
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.cardContainer}
            underlayColor={Colors.underlayColor}
            onPress={() => this.gotoAccessorySales()}>
            <View style={styles.leftImageContainer}>
              <Image source={require('../../../images/common/icons/accessorySale.png')} style={styles.orderImage} resizeMode="cover" />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{this.state.locals.lblAccessorySale}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.cardContainer}
            underlayColor={Colors.underlayColor}
            onPress={() => this.gotoWarrantyReplacement()}>
            <View style={styles.leftImageContainer}>
              <Image source={require('../../../images/common/icons/directSale.png')} style={styles.orderImage} resizeMode="cover" />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{this.state.locals.lblWarranrtReplacement}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 100,
  },
  background: {
    backgroundColor: Colors.appBackgroundColor,
    height: '100%'
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 6,
    backgroundColor: Colors.colorYellow,
    alignItems: 'center'
  },
  leftImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 5
  },
  orderImage: {
    alignSelf: 'center',
    width: undefined,
    height: undefined,
    margin: 5,
    padding: 25,
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent
  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  innerText: {
    textAlign: 'center',
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    padding: 8
  },
});

const mapStateToProps = (state) => {
  const Language  = state.lang.current_lang;
  return { Language }
}

export default connect(mapStateToProps, actions)(DirectSale);
