import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Linking,
  BackHandler,
  Alert
} from 'react-native';
import SignatureCapture from 'react-native-signature-capture';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as actions from '../../actions';
import {  Colors, Constants  } from '../../config';
import { Analytics, Screen, FuncUtils } from '../../utills';
import strings from '../../Language/MobileActivaton';
import stringsDel from '../../Language/Delivery';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';

const Utill = new Utills();

let navigatorOb;
let isUserStartSigned;
let ImagePath;

class SignaturePadModule extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    stringsDel.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      isUserStartSigned: false,
      placeYourSignature: strings.placeYourSignature,
      bySigning: strings.bySigining,
      tns: strings.tns,
      enterCustomerSignature: strings.enterCustomerSignature,
      sigReset: strings.sigReset,
      sigOk: strings.sigOk,
      didUserSign: false,
      locals: {
        signaturePadTitle: strings.customerSignatureUC,
        backMessage: stringsDel.backMessage,
        ok : strings.btnOk,
        cancel : strings.btnCancel,
        tnsUc: strings.tnsUc
      }
    };
    navigatorOb = props.navigator;
    isUserStartSigned = false;
    this.imageId = `SIG_${this.props.activity_start_time}`;
    // ImagePathOld = '/storage/emulated/0/DCIM'; ImagePath =
    // `${Environment.getExternalStorageDirectory().toString()}/DCIM`;
    ImagePath = 'temp';
  }

  componentWillMount(){
    Orientation.lockToLandscape();
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });

  }

  onSaveEvent = (result) => {
    console.log('xxx onSaveEvent');
    const signatureId = result.imageId;
    const signatureUri = result.pathName;
    const signatureRand = Math.floor(Math.random() * 1000);
    const signatureBase64 = result.encoded;
    console.log(result);
    const signatureOb = {
      signatureRand,
      signatureId,
      signatureBase64,
      captureType: Constants.IMAGE_CAPTURE_TYPES.SIGNATURE,
      imageUri: signatureUri
    };
    if (isUserStartSigned) {
      this.props.commonSetKycImage(signatureOb);
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    } else {
      Utill.showAlertMsg(this.state.enterCustomerSignature);
    }
  }
  onDragEvent = () => {
    // This callback will be called when the user enters signature
    console.log('onDragEvent');
    isUserStartSigned = true;
    this.setState({ didUserSign: true });
  }

  saveSign = () => {
    this.refs.sign.saveImage();
  }

  resetSign = () => {
    isUserStartSigned = false;
    this.setState({ didUserSign: false });
    this.refs.sign.resetImage();
  }

  handleTndCPress  = () => {
    const { tncUrl } = this.props;
    let extensionType = FuncUtils.getFileExtensionType(tncUrl);
    if (extensionType == 'pdf') {
      let screen = {
        title: this.state.locals.tnsUc,
        id: 'DialogRetailerApp.modals.PdfReaderModal',
      };
      let passProps = {
        tncUrl: this.props.tncUrl,
        title: this.state.locals.tnsUc
      };
      Screen.showModalView(screen, passProps, false);
    } else {
      Linking.openURL(tncUrl);
    }  
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.signaturePadTitle}/>
        <View style={styles.signatureContainer}>
          <View style={styles.topContainer}>
            <View stye={styles.iconView}>
              {this.state.didUserSign == true
                ? <TouchableHighlight
                  style={styles.buttonStyle}
                  underlayColor={Colors.transparent}
                  onPress={() => {
                    this.resetSign();
                  }}>
                  <Ionicons name='md-close' size={30} color={Colors.colorBlack}/>
                </TouchableHighlight>
                : true
              }
            </View>
            <View style={styles.placeYourSignatureView}>
              <Text style={styles.desTxt}>{this.state.placeYourSignature}</Text>
            </View>
            <View stye={styles.iconView}>
              {this.state.didUserSign == true
                ? <TouchableHighlight
                  style={styles.buttonStyle}
                  underlayColor={Colors.transparent}
                  onPress={() => {
                    this.saveSign();
                  }}>
                  <Ionicons name='md-checkmark' size={35} color={Colors.colorBlack}/>
                </TouchableHighlight>
                : true
              }
            </View>
          </View>
          <SignatureCapture
            style={styles.signature}
            ref="sign"
            onSaveEvent={this.onSaveEvent}
            onDragEvent={this.onDragEvent}
            saveImageFileInExtStorage
            showNativeButtons={false}
            showTitleLabel={false}
            viewMode={'landscape'}
            savePath={ImagePath}
            imageId={this.imageId}/>
        </View>
        <View style={styles.buttonContainer}>
          <Text style={styles.desTncTxt}>{this.state.bySigning}
          </Text>
          <Text style={styles.desTncTxtLink} onPress={() => this.handleTndCPress()}>
            {this.state.tns}
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const activity_start_time = state.configuration.activity_start_time;
  const Language = state.lang.current_lang;

  return { activity_start_time , Language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    backgroundColor: Colors.white
  },

  topContainer: {
    flex: 0.8,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0
  },
  signature: {
    flex: 7,
  },
  buttonContainer: {
    flex: 0.8,
    margin: 5,
    marginTop:0,
    // marginBottom:10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.transparent,
  },
  desTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
    fontWeight: 'bold'
  },

  iconView: {
    flex: 1
  },

  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold'
  },
  placeYourSignatureView: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: 15,
    marginLeft: 5,
    textDecorationLine: 'underline',
    fontWeight: 'bold'
  },
  signatureContainer: {
    flex: 7,
    elevation: 3,
    flexDirection: 'column',
    margin: 10, 
    borderColor: Colors.signatureBorderColor
  }
});

export default connect(mapStateToProps, actions)(SignaturePadModule);
