/*
 * File: CommonHelperModule.js
 * Project: Dialog Sales App
 * File Created: Sunday, 23rd December 2018 7:48:07 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Sunday, 23rd December 2018 2:19:08 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import _ from 'lodash';
import { Analytics, Screen, ImageUploadNativeModule, LocalStorageUtils }  from '../../utills/';
import { Constants }  from '../../config/';

/**
 * @description Add to image upload SQLite db
 * @param {Number} txnId
 * @param {Number} imageId
 * @param {String} imageOb
 * @param {String} url
 * @memberof CommonHelperModule
 */
const addToImageUploadQueue = async(txnId, imageId, imageOb, url, lob=Constants.LOB_LTE) => {
  console.log('### COMMON :: addToImageUploadQueue :: txnId, imageId, imageUri, url, lob : ', txnId, imageId, imageOb, url, lob);
  let deviceData = await LocalStorageUtils.getDeviceAndUserData();
  try {
    const id = `${txnId}_${imageId}`;
    const imageFile = imageOb.imageUri.replace('file://', '');
    const uploadSelector = 'encrypt';

    const options = {
      id: id,
      url: url,
      imageFile: imageFile,
      uploadSelector: uploadSelector,
      deviceData: JSON.stringify(deviceData)
    };

    console.log('### COMMON :: addToImageUploadQueue :: options : ', options);

    const successCb = (msg) => {
      console.log('xxxx addToImageUploadQueue successCb');
      Analytics.logEvent('dsa_common_sqlite_success_'+ lob);
      console.log(msg);
    };

    const errorCb = (msg) => {
      console.log('### COMMON ::addToImageUploadQueue errorCb');
      Analytics.logEvent('dsa_common_sqlite_error_'+ lob);
      console.log(msg);
    };

    ImageUploadNativeModule.addToDb(options, errorCb, successCb);

  } catch (e) {
    console.log('### COMMON ::addToImageUploadQueue Image Uploading Failed :', e.message);
    Analytics.logEvent('dsa_common_sqlite_ex_' + lob);
    Screen.showAlert(e.message);
  }
};

export default {
  addToImageUploadQueue
};