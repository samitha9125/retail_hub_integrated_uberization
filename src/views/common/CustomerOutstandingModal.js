/*
 * File: CustomerOutstandingModal.js
 * Project: Dialog Sales App
 * File Created: Thursday, 8th November 2018 6:00:57 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 9th November 2018 10:08:16 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Alert,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Constants, Images, Colors, Styles } from '@Config';
import strings from '../../Language/Common';

const { width, height } = Dimensions.get('window');
class CustomerOutstandingModal extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      nic: this.props.customerData.id_number,
      para: this.props.outstandingData.totalOsAmount,
      locals: {
        passportUC: strings.passportUC,
        nicUC: strings.nicUC,
        yes: strings.yes,
        no: strings.no,
        last: strings.last,
        rs_text: strings.rs_text,
        confirmText: strings.confirmText,
        continueButtonText: strings.continueButtonText,
        cancelButtonTxt: strings.cancelButtonTxt,
        outstandingLeftText: strings.outstandingLeftText,
        outstandingRightText: strings.outstandingRightText,

      }
    };
  }
  componentDidMount() {
    console.log('xxx customerData', this.props.customerData);
    console.log('xxx outstandingData', this.props.outstandingData);
  }

  outstandingConfirmCancel = () => {
    Alert.alert('Confirm', this.state.locals.confirmText, [
      {
        text: this.state.locals.yes,
        onPress: () => this.onConfirmYes()
      }, {
        text: this.state.locals.no,
        onPress: () => console.log('No Pressed'),
        style: 'cancel'
      }
    ], { cancelable: false });

  }

  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = Images.icons.Mobile_icon;
        break;
      case 'DTV':
        icon = Images.icons.Dtv_icon;
        break;
      case 'LTE':
        icon = Images.icons.Lte_icon;
        break;
    }
    return icon;
  }

  onConfirmYes = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonInnerContainer}>
        <TouchableOpacity
          onPress={() => this.outstandingConfirmCancel()}
          style={styles.buttonCancelStyle}>
          <Text style={styles.cancelButtonTextStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.outstandingConfirmContinue(this.props.outstandingData, this.props.customerData)}
          style={styles.buttonContinueStyle}>
          <Text style={styles.continueButtonTextStyle}>
            {this.state.locals.continueButtonText}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderListItem = ({ item }) => {
    let screen = this;
    const { lob, display_name, outstandingAmount } = item;
    return (
      <View style={styles.elementContainer}>
        <View style={styles.elementLeftLobIcon}>
          <Image source={screen.getLobIconSrc(lob)} style={styles.imageIconStyle}/>
        </View>
        <View style={styles.elementLeftView}>
          <Text style={styles.elementLeftText}>{display_name}</Text>
        </View>
        <View style={styles.elementRightView}>
          <View style={styles.elementRightInnerView}>
            <Text style={styles.elementRightRupeeText}>{screen.state.locals.rs_text}</Text>
            <Text style={styles.elementRightText}>{outstandingAmount}</Text>
          </View>
        </View>
      </View>
    );
  };

  render() {

    const customerData = this.props.customerData;
    const outstandingDataOb = this.props.outstandingData;
    const id_type = (customerData.id_type == Constants.ID_PP
      ? this.state.locals.passportUC
      : this.state.locals.nicUC);

    let outstandingData = [];
    outstandingData = [...outstandingDataOb.info];

    return (
      <View style={styles.containerOverlay}>
        <View style={styles.modalContainer}>
          <View style={styles.imageContainerStyle}>
            <Image source={Images.icons.AttentionAlert} style={styles.imageStyle}/>
          </View>
          <View style={styles.nicContainer}>
            <Text style={[styles.textStyle, styles.nicText]}>{id_type}
              : {this.state.nic}</Text>
          </View>
          <View style={styles.outstandingTextContainer}>
            <Text style={styles.textStyle}>
              {this.state.locals.outstandingLeftText}
            </Text>
            <Text style={styles.textStyle}>
              <Text style={styles.priceStyle}>{`${this.state.locals.rs_text} ${this.state.para} `}</Text>
              {this.state.locals.outstandingRightText}
            </Text>
          </View>
          <View style={[styles.outstandingListContainer, outstandingData.length> 4 ? { height: 140 } : { height: 'auto'} ]}>
            <ScrollView>
              <FlatList
                data={outstandingData}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => index.toString()}
                scrollEnabled={true}
                initialNumToRender={4}/>
            </ScrollView>
          </View>
          <View style={styles.bottomTextContainer}>
            <Text style={styles.textStyle}>{this.state.locals.last}</Text>
          </View>
          <View style={styles.buttonContainer}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const language = state.lang.current_lang;
  return { language };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColor
  },

  modalContainer: {
    height: 'auto',
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: {
      width: 0,
      height: 2
    },
    elevation: 2,
    paddingHorizontal: 10
  },

  nicContainer: {
    height: 'auto',
    marginTop: 5,
    padding: 5,
    paddingHorizontal: 14
  },

  nicText: {
    fontWeight: '500',
    fontSize: 20
  },

  outstandingTextContainer: {
    height: 'auto',
    padding: 5,
    marginBottom: 10,
    paddingHorizontal: 14
  },

  outstandingListContainer: {
    height: 'auto',
    padding: 5,
    paddingHorizontal: 14
  },

  bottomTextContainer: {
    height: 'auto',
    padding: 5,
    paddingHorizontal: 14
  },

  buttonContainer: {
    height: 'auto',
    marginTop: 5,
    marginBottom: 5,
    padding: 5
  },

  imageContainerStyle: {
    padding: 5,
    marginTop: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    width: 60,
    height: 60
  },
  textStyle: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
  },

  cancelButtonTextStyle : {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    fontWeight: '500'
  },

  continueButtonTextStyle : {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.btnActive,
    marginLeft: 5,
    fontWeight: '500'
  },
  
  priceStyle: {
    color: Colors.colorRed
  },

  buttonInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },

  buttonContinueStyle: {
    backgroundColor: Colors.colorWhite,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10,
    
  },
  buttonCancelStyle: {
    backgroundColor: Colors.colorWhite,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10,
   
  },

  elementContainer: {
    height: 'auto',
    flexDirection: 'row'
  },

  elementLeftLobIcon: {
    flex: 0.3
  },

  imageIconStyle: {
    width: 22,
    height: 22,
    marginBottom: 10
  },

  elementLeftView: {
    flex: 1.4
  },

  elementRightView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    //backgroundColor: Colors.colorGreen

  },

  elementRightInnerView: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginLeft: 5

  },

  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },

  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'
  },
  elementRightRupeeText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left',
  }
});

export default connect(mapStateToProps, actions)(CustomerOutstandingModal);
