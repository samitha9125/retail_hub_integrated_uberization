/*
 * File: BarcodeScannerModule.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 1st August 2018 10:17:09 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 16th Jan 2019 1:12:07 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard,
  Dimensions, TextInput, BackHandler, Alert, Vibration } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation'
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import Constants from '../../config/constants';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import strings from '../../Language/general';
import ActIndicator from './components/ActivityIndicatorBarcode';
import { Header } from './components/Header';
import _ from 'lodash';

const Utill = new Utills();

const VIBRATION_DURATION = 200;
const MARGIN = 50;
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const maskRowHeight = Math.round((DEVICE_HEIGHT - 300) / 5);
const maskColWidth = (DEVICE_WIDTH - 300) / 2;

const SERIAL_SCANNER_DEBOUNCE_TIME = 2200;
const SERIAL_SCANNER_DEBOUNCE_TIME_AUTO = 1500;
const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

class BarcodeScannerModule extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      locals: {
        backMessage: this.props.backMessage,
        screenTitle: this.props.screenTitle,
        displayMessage: this.props.displayMessage,
        enterSimSerial: strings.enterSimSerial,
        enterValidSim: strings.enterValidSim,
        scanSuccess: strings.scanSuccess,
        barcode_will_scan_automatically: strings.barcode_will_scan_automatically,
        try_to_avoid_shadow: strings.try_to_avoid_shadow,
        invalid_serial_format: strings.invalid_serial_format,
        continueWithCapitalLetter: strings.continueWithCapitalLetter,
        ok: strings.ok,
        cancel: strings.cancel,
        continue: strings.continueWithCapitalLetter,
        btnYes : strings.btnYes,
        btnno : strings.btnNo,
        permission_to_use_camera: 'Permission to use camera',
        permission_to_use_camera_message: 'To Scan the serial, App need your permission to use your camera',
      },
  
      uniqueValue: 0,
      disableContinueButton: true,
      continueButtonClicked: false,
      barcodeValue: null,
      scanSuccess: false,
      serialMaxLength :  this.props.serialMaxLength ? 
        this.props.serialMaxLength : Constants.DEFAULT_SERIAL_MAX_LENGTH
    };

    this.debouncedContinueButtonClick = _.debounce(() => this.onContinueButtonClick(), SERIAL_SCANNER_DEBOUNCE_TIME, DEBOUNCE_OPTIONS);
    this.debouncedContinueApiCall = _.debounce((serialNumber) => this.barcodeReadSuccessApiCall(serialNumber), SERIAL_SCANNER_DEBOUNCE_TIME_AUTO, DEBOUNCE_OPTIONS);
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentWillUnmount() {
    console.log('xxx componentWillUnmount');
    let me = this;
    BackHandler.removeEventListener('hardwareBackPress', me.onHandleBackButton);
    me.props.resetSerialValidationStatus();
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    let me = this;
    BackHandler.addEventListener('hardwareBackPress', me.onHandleBackButton);
    me.props.resetSerialValidationStatus();
    me.props.setSerialInputValue('');
  }

  componentWillReceiveProps(nextProps) {
    console.log('SERIAL VALIDATION :: componentWillReceiveProps :', nextProps);
  }

  forceRemount = () => {
    this.setState(({ uniqueValue }) => ({
      uniqueValue: uniqueValue + 1
    }));
  }

  onHandleBackButton = () => {
    console.log('xxx onHandleBackButton');
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.btnno,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.btnYes,
        onPress: () => this.goBack()
      }
    ], { cancelable: true });
    return true;
  }

  goBack = () => {
    console.log('xxx goBack');
    let me = this;
    const navigatorOb = this.props.navigator;
    me.props.resetSerialValidationStatus();
    me.props.resetSerialFieldValues(me.props.serialType);
    me.props.commonSetApiLoading(false);
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  popView = () => {
    console.log('xxx popView');
    let _this = this;
    const navigatorOb = _this.props.navigator;
    _this.props.resetSerialValidationStatus();
    _this.props.commonSetApiLoading(false);
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  validateSerialFormat = (serial) => {
    console.log('xxx validateSerialFormat :: serial ',serial);
    let me = this;
    if (me.props.inputValidateType ==='ALPHA_NUMERIC') {
      console.log('xxx validateSerialFormat :: ALPHA_NUMERIC');
      return Utill.alphaNumericValidation(serial);
    } else {
      console.log('xxx validateSerialFormat :: otherSerial ');
      return Utill.validateNumberFormat(serial);
    }
  }

  onBarCodeReadCb = (e) => {
    console.log('xxx onBarCodeReadCb', e);
    let me = this;
    Vibration.vibrate(VIBRATION_DURATION);
    let serialNumber = e.data.toString();
    console.log(`xxx onBarCodeReadCb :: serialNumber : ${serialNumber}`);
    if (me.props.sliceNumber) {
      serialNumber = serialNumber.slice(-me.props.sliceLength);
    }
    if (me.validateSerialFormat(serialNumber)) {
      me.props.commonSetApiLoading(true);
      console.log('xxx onBarCodeReadCb :: serialNumber', serialNumber);
      console.log('xxx onBarCodeReadCb :: CALL_API');
      me.setState({ barcodeValue: serialNumber, scanSuccess: true });
      me.debouncedContinueApiCall(serialNumber); 
    }
  };

  barcodeReadSuccessApiCall = (serialNumber) => {
    console.log('xxx barcodeReadSuccessApiCall :: serialNumber ', serialNumber);
    let me = this;
    me.props.setSerialInputValue(serialNumber);
    me.serialValidationApi(serialNumber);

  }

  serialValidationSuccessCb = async (response) => {
    console.log('xxx serialValidationSuccessCb : ', response);
    let me = this;
    await Keyboard.dismiss();
    await me.props.commonSetApiLoading(false);
    await me.setState({ disableContinueButton: false, continueButtonClicked: !me.props.autoValidation },() => {
      if (!me.props.autoValidation) {
        console.log('xxx serialValidationSuccessCb :: AUTOMATICALLY_POP_VIEW');
        _.delay((me) => {
          me.popView();
        }, 5, this);  
      }
    }); 
  };

  serialValidationFailureCb = async (response) => {
    console.log('xxx serialValidationFailureCb : ', response);
    let me = this;
    await me.forceRemount();
    await me.props.commonSetApiLoading(false);
    await me.setState({ disableContinueButton: true, continueButtonClicked: false }, async() => {
      await me.props.resetSerialValidationStatus();
      await me.props.setSerialInputValue('');
      await me.setState({ barcodeValue: '' });
    }); 
  };

  onChangeText = (value) => {
    let me = this;
    console.log('xxx onChangeText');
    this.setState({ barcodeValue: value });
    me.props.resetSerialValidationStatus();
    if ( value == '') {
      this.setState({ disableContinueButton: true });
    } else if (me.validateSerialFormat(value)) {
      console.log('xxx onChangeText :: VALID_NUMBER');
      me.props.setSerialInputValue(value);
      if (me.props.autoValidation && value.length === me.state.serialMaxLength){
        console.log('xxx onChangeText :: AUTO_API_CALL');
        me.props.commonSetApiLoading(true);
        me.setState({ continueButtonClicked: true },() => {
          me.debouncedContinueApiCall(value);
        });        
      } else {
        console.log('xxx onChangeText :: MANUAL_API_CALL');
        this.setState({ disableContinueButton: false });
      }
     
    } else {
      console.log('xxx onChangeText :: INVALID_NUMBER');
      Utill.showAlert(this.state.locals.invalid_serial_format);
      this.setState({ disableContinueButton: true });
    }
  };

  serialValidationApi = (number) => {
    let _this = this;
  
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();

    console.log('xxx serialValidationApi :: number ###### :', number);
    _this.props.setSerialInputValue(number);
    let requestParams = {
      materialType : _this.props.materialType,
      serialType : _this.props.serialType,
      product_family : _this.props.productFamily,
      materialSerial : number,
      lob: _this.props.additionalProps.lob,
      cx_identity_no: _this.props.additionalProps.id_number,
      cx_identity_type: _this.props.additionalProps.id_type,
      customer_status: _this.props.additionalProps.customer_status
    };

    console.log('xxx serialValidationApi :: requestParams :', requestParams);
    console.log('xxx serialValidationApi :: additionalPramsForAPI :', _this.props.additionalPramsForAPI);

    _.merge(requestParams, _this.props.additionalPramsForAPI);

    console.log('xxx serialValidationApi :: merge_requestParams :', requestParams);
    
    _this.props.setSerialValueRedux(number);
    _this.props.validateSerialNumberAPI(requestParams, _this.props.validateUrl, _this); 
  };


  goToNextView = () => {
    console.log('xxx goToNextView');
    let me = this;
    if (me.props.hasNextScreen) {
      if (me.props.didSimValidated) {
        let screen = {
          title: this.props.onReturnViewTitle,
          id: this.props.onReturnView,
        };
        let passProps =  {
          screenTitle: me.props.onReturnViewTitle,
          backMessage: 'backMessage',
          dataProps: me.props.dataProps
        };
    
        Screen.pushView(screen, me, passProps);

      } else {
        Utill.showAlertMsg(me.state.locals.enterValidSim);
      }
    } else {
      me.popView();
    }
  }

  onContinueButtonClick = async () => {
    let me = this;
    console.log('xxx onContinueButtonClick');
    if (me.state.barcodeValue === null || me.state.barcodeValue === '') {
      await Utill.showAlertMsg(me.state.locals.enterValidSim);
      return;
    }
    if (!me.props.autoValidation) {
      console.log('CONTINUE_BUTTON_ACTION_CALL_API');
      await me.barcodeReadSuccessApiCall(this.state.barcodeValue);
    } else {
      console.log('CONTINUE_BUTTON_ACTION');
      await me.goToNextView();
    }
  };

  onClickContinueAction = async () => {
    let me = this;
    
    console.log('xxx onClickContinueAction');
    await Keyboard.dismiss();
    await me.setState({ continueButtonClicked: true }, async() => {
      console.log('xxx onClickContinueAction CALLING_DEBOUNCE_METHOD');
      me.props.commonSetApiLoading(true);
      //me.debouncedContinueButtonClick(); //TODO: review here
      me.onContinueButtonClick();
    });
  }

  render() {
    let me = this;
    let continueButtonEnableStatus = false;
    let continueButtonClicked = false;
    console.log('xxxxxxxxxxxxxxxxxxxx BarcodeScannerModule xxxxxxxxxxxxxxxxxxxxxxxxx');
    console.log('disableContinueButton :: state', me.state.disableContinueButton);
    console.log('buttonDisableStatus_auto :: prop', me.props.buttonDisableStatus_auto);
    if (!this.props.autoValidation) {
      continueButtonEnableStatus = !me.state.disableContinueButton;
      continueButtonClicked = me.state.continueButtonClicked;
    } else {
      continueButtonEnableStatus = !me.props.buttonDisableStatus_auto;
      continueButtonClicked = me.state.continueButtonClicked;
    }

    let loadingIndicator;
    if (me.props.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => me.onHandleBackButton()}
          headerText={me.state.locals.screenTitle}/>
        <RNCamera
          ref={(cam) => {
            me.camera = cam;
          }}
          key={me.state.uniqueValue}
          style={styles.barcodePreview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={me.state.locals.permission_to_use_camera}
          permissionDialogMessage={me.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => me.onBarCodeReadCb(e)}>
          <View style={styles.maskOuterView}>       
            <View style={[{ flex: maskRowHeight  }, styles.maskRow, styles.maskFrame ]} />        
            <View style={[styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]}/>            
            </View>
            <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
          </View>
          <View style ={styles.topMessageViewStyle}>
            <Text style ={styles.displayMessageTextStyle}>{this.props.displayMessage}</Text>
          </View>
          <View style ={styles.bottomMessageViewStyle}>
            <Text style ={styles.displayMessageTextStyle}>{this.state.locals.barcode_will_scan_automatically}</Text>
            <Text style ={styles.displayMessageTextStyle}>{this.state.locals.try_to_avoid_shadow}</Text>
          </View>
        </RNCamera>
        {loadingIndicator}
        <View style={[styles.barcodeOverlay, styles.barcodeBottomOverlay]}>
          <View style={styles.containerBottomBarcode}>
            <View style={styles.barcodeInputContainer}>
              <TextInput
                style={styles.barcodeTxtInput}
                underlineColorAndroid='transparent'
                keyboardType={ me.props.keyboardType}
                value={me.state.barcodeValue}
                placeholder={me.props.textInputLabel}
                maxLength={me.state.serialMaxLength}
                autoFocus={false}
                returnKeyType={'done'}
                selectionColor={'yellow'}
                onChangeText={(value) => me.onChangeText(value)}/>
            </View>
            <TouchableOpacity
              style={continueButtonEnableStatus ? styles.barcodeContinueBtn : styles.barcodeContinueBtnDisabled}
              disabled={!continueButtonEnableStatus || me.props.apiLoading || continueButtonClicked}
              onPress={() => me.onClickContinueAction()}>
              <Text style={continueButtonEnableStatus ? styles.barcodeContinueBtnTxtEnabled : styles.barcodeContinueBtnTxtDisabled}>
                {me.state.locals.continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: BarcodeScannerModule => Common : \n', state.common);
  console.log('****** REDUX STATE :: BarcodeScannerModule => lteActivation : \n', state.lteActivation);
  console.log('****** REDUX STATE :: BarcodeScannerModule => dtvActivation : \n', state.dtvActivation);
  const Language = state.lang.current_lang;
  const apiLoadingLte = state.lteActivation.apiLoading;
  const apiLoadingDtv = state.dtvActivation.apiLoading;
  const api_loading = state.common.api_loading;
  const apiLoading = apiLoadingLte || api_loading || apiLoadingDtv;
  const serialValidationStatus = state.lteActivation.serialValidationStatus;
  const buttonDisableStatus_auto = !serialValidationStatus;

  return {
    Language,
    apiLoading, 
    serialValidationStatus,
    buttonDisableStatus_auto,
  };
};

const styles = StyleSheet.create({
  containerBarcodeScan: {
    flex: 1
  },
  barcodePreview: {
    flex: 1,

  },
  containerBottomBarcode: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackground
  },

  ////////////////////
  maskOuterView: {
    position: 'absolute',
    // top: -30,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  maskInner: {
    width: DEVICE_WIDTH - MARGIN,
    backgroundColor: Colors.colorTransparent,
    borderColor:Colors.colorPureYellow,
    borderWidth: 2,
  },
  maskFrame: {
    backgroundColor: Colors.colorBlack,
  },
  maskRow: {
    width: '100%',     
  },
  maskCenter: { 
    flex: 30,
    flexDirection: 'row' 
  },

  topMessageViewStyle: {
    position: 'absolute' , 
    bottom: (DEVICE_HEIGHT / 3) * 2, 
    alignSelf: 'center',
    
  },
  bottomMessageViewStyle: {
    position: 'absolute' , 
    bottom: 135, 
    alignSelf: 'center',
    
  },

  displayMessageTextStyle: { 
    color:Colors.appBackgroundColor, 
    fontSize: 16
  },
  /////////////////////////

  barcodeOverlay: {
    position: 'absolute',
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  barcodeBottomOverlay: {
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  barcodeInputContainer: {
    flex: 1,
    margin: 10,
  },
  barcodeTxtInput: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  
  barcodeContinueBtn: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.button.activeColor
  },
  barcodeContinueBtnDisabled: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.button.disableColor
  },
  barcodeContinueBtnTxtEnabled: {
    fontSize: Styles.button.defaultFontSize,
    textAlign: 'center',
    fontWeight: Styles.button.defaultFontWeight,
    color: Colors.button.activeTextColor,
  },

  barcodeContinueBtnTxtDisabled: {
    fontSize: Styles.button.defaultFontSize,
    textAlign: 'center',
    fontWeight: Styles.button.defaultFontWeight,
    color: Colors.button.disableTextColor,
  },
});

export default connect(mapStateToProps, actions)(BarcodeScannerModule);