import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  StatusBar,
  BackHandler,
  Alert,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Camera, { constants } from '../../utills/DSCamera';
import * as actions from '../../actions';
import CameraImageSrc from '../../../images/other_images/camera/ic_photo_camera_36pt.png';
import FlashOnImageSrc from '../../../images/other_images/camera/ic_flash_on_white.png';
import FlashOffImageSrc from '../../../images/other_images/camera/ic_flash_off_white.png';
import FlashAutoImageSrc from '../../../images/other_images/camera/ic_flash_auto_white.png';
import strings from '../../Language/general';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import { Header } from './components/Header';
const { width, height } = Dimensions.get('window');

const SHOW_IMAGE_PREVIEW = 1;
const SHOW_CAMERA = 2;

class ImageCaptureModule extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      CameraPreviewTitle: '',
      camera: {
        aspect: constants.Aspect.fill,
        captureTarget: constants.CaptureTarget.temp,
        type: constants.Type.back,
        orientation: constants.Orientation.portrait, 
        flashMode: constants.FlashMode.off,
        captureQuality: constants.CaptureQuality.high,
        fixOrientation: true
      },

      title: {
        nic_front: strings.nic_front,
        nic_back: strings.nic_back,
        passport: strings.passport,
        driving_licence: strings.driving_licence,
        proof_of_billing: strings.proof_of_billing,
        customer_image: strings.customer_image,
        signature: strings.signature,
        installation_address: strings.installation_address,
        ack_form: strings.ack_form
      },

      locals: {
        backMessage : this.props.backMessage? this.props.backMessage : strings.cameraBackMsg,
        ok: 'OK',
        cancel: 'CANCEL'
      },

      continue: strings.continue_uc,
      reScan: strings.re_scan_uc,
      renderView: SHOW_CAMERA,
      captureCount: 0,
      imageSrc: 'default_thumbnail',
      captureTypeNo: this.props.imageCaptureTypeNo
    };

    this.imageArray = [];
  }

  componentWillMount() {
    console.log('######################################################');
    console.log(`imageCaptureTypeNo :${this.props.imageCaptureTypeNo}`);
    this.changeTitle(this.props.imageCaptureTypeNo);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentDidUpdate() {
    this.changeTitle(this.state.captureTypeNo);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }
  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  changeTitle = (key) => {
    let titleName;
    const navigatorOB = this.props.navigator;
    switch (key) {
      case Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT:
        titleName = this.state.title.nic_front;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.NIC_BACK:
        titleName = this.state.title.nic_back;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.PASSPORT:
        titleName = this.state.title.passport;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE:
        titleName = this.state.title.driving_licence;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING:
        titleName = this.state.title.proof_of_billing;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE:
        titleName = this.state.title.customer_image;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.SIGNATURE:
        titleName = this.state.title.signature;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB:
        titleName = this.state.title.installation_address;
        break;
      default:
        titleName = '';
        break;
    }
    if (this.state.CameraPreviewTitle !== titleName){
      this.setState({ CameraPreviewTitle: titleName });
    }
    navigatorOB.setTitle({ title: titleName });
  }

  continueAction = async () => {
    let _this = this;
    console.log('xxx continueAction :: captureTypeNo : '+ _this.state.captureTypeNo);
    const kycOb = {
      captureType: _this.state.captureTypeNo,
      imageUri: _this.state.imageSrc
    };

    await _this.props.commonSetKycImage(kycOb);

    if (_this.state.captureTypeNo === Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT
      && this.props.kyc_nic_front !== null && !_this.props.reCapture) {
      console.log('xxx CAPTURE_TYPE_NIC');
      if (_this.props.kyc_nic_back !== null) {
        console.log('xxx CAPTURE_TYPE_NIC_BACK');
        _this.popScreen();
      } else {
        console.log('xxx CAPTURE_TYPE_NIC_FRONT');
        _this.setState({ renderView: SHOW_CAMERA, captureTypeNo: Constants.IMAGE_CAPTURE_TYPES.NIC_BACK });
      }
    } else {
      console.log('xxx CAPTURE_TYPE_OTHER');
      _this.popScreen();
    }
  }

  popScreen = () => {
    const navigatorOB = this.props.navigator;
    navigatorOB.pop({ animated: true, animationType: 'fade' });
  }

  reScan = () => {
    console.log('xxx reScan');
    this.setState({ renderView: SHOW_CAMERA });
  }

  takePicture = () => {
    console.log('xxx takePicture');
    if (this.camera) {
      this.camera.capture().then((data) => {
        console.log(data);
        this.setState({ renderView: SHOW_IMAGE_PREVIEW, imageSrc: data.uri });
      }).catch(err => console.log(err));
    }
  }

  switchFlash = () => {
    let newFlashMode;
    const { auto, on, off } = constants.FlashMode;
    
    if (this.state.camera.flashMode === auto) {
      newFlashMode = on;
    } else if (this.state.camera.flashMode === on) {
      newFlashMode = off;
    } else if (this.state.camera.flashMode === off) {
      newFlashMode = auto;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode
      }
    });
  }

  get flashIcon() {
    let icon;
    const { auto, on, off } = constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      icon = FlashAutoImageSrc;
    } else if (this.state.camera.flashMode === on) {
      icon = FlashOnImageSrc;
    } else if (this.state.camera.flashMode === off) {
      icon = FlashOffImageSrc;
    }

    return icon;
  }

  render() {
    if (this.state.renderView === SHOW_IMAGE_PREVIEW) {
      return (
        <View style={styles.containerPreview}>
          <Header backButtonPressed={() => this.handleBackButtonClick()} headerText={this.state.CameraPreviewTitle}/>
          <View style={styles.containerTop}>
            <Image
              style={styles.previewImage}
              source={{
                uri: this.state.imageSrc
              }}/>
          </View>
          <View style={styles.containerBottom}>
            <TouchableOpacity style={styles.reScanBtn} onPress={() => this.reScan()}>
              <Text style={styles.reScanBtnBtnTxt}>
                {this.state.reScan}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.continueBtn}
              onPress={() => this.continueAction()}>
              <Text style={styles.continueBtnTxt}>
                {this.state.continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );  
    }   
    else {
      return (
        <View style={styles.containerCamera}>
          <Header backButtonPressed={() => this.handleBackButtonClick()} headerText={this.state.CameraPreviewTitle}/>
          <StatusBar animated hidden/>
          <Camera
            ref={(cam) => {
              this.camera = cam;
            }}
            style={styles.preview}
            aspect={this.state.camera.aspect}
            captureTarget={this.state.camera.captureTarget}
            type={this.state.camera.type}
            flashMode={this.state.camera.flashMode}
            onFocusChanged={() => {}}
            onZoomChanged={() => {}}
            defaultTouchToFocus
            mirrorImage={false}/>
          <View style={[styles.overlay, styles.topOverlay]}>
            <TouchableOpacity style={styles.flashButton} onPress={this.switchFlash}>
              <Image source={this.flashIcon}/>
            </TouchableOpacity>
          </View>
          <View style={[styles.overlay, styles.bottomOverlay]}>
            <TouchableOpacity style={styles.captureButton} onPress={this.takePicture}>
              <Image source={CameraImageSrc}/>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: ImageCaptureModule :: ALL \n', state);
  console.log('****** REDUX STATE :: ImageCaptureModule :: lteActivation\n', state.common);
  console.log('****** REDUX STATE :: ImageCaptureModule :: common\n', state.common);
  const Language = state.lang.current_lang;

  const kyc_nic_front  =  state.common.kyc_nic_front;
  const kyc_nic_back = state.common.kyc_nic_back;

  return { Language, kyc_nic_front , kyc_nic_back };
};

const styles = StyleSheet.create({
  ////////Camera Module ////
  containerCamera: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  topOverlay: {
    top: 55,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: Colors.cameraOverlayColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  captureButton: {
    padding: 15,
    backgroundColor: Colors.colorWhite,
    borderRadius: 40
  },
  ///// NIC capture Module ////
  containerPreview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.cameraPreviewBackgroundColor
  },
  containerTop: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackgroundColor
  },
  previewImage: {
    margin: 5,
    width: width * 0.92,
    height: height * 0.75,
    resizeMode: 'contain'
  },
  reScanBtn: {
    flex: 1,
    margin: 10
  },
  continueBtn: {
    flex: 1,
    margin: 10
  },
  reScanBtnBtnTxt: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  continueBtnTxt: {
    fontSize: 20,
    fontWeight: '500',
    color: Colors.colorBlack,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, actions)(ImageCaptureModule);
