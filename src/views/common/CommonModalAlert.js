/*
 * File: GeneralModalAlert.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 31st October 2018 3:21:24 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 16th Jan 2019 1:12:07 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import Button from '@Components/others/Button';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
const { width, height } = Dimensions.get('window');

class GeneralModalAlert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { 
      modalContainerCustomStyle = {},
      disabled= false,
      hideTopImageView = false,  
      title,
      descriptionTitle,
      description = '', 
      customDescriptionTextView = false,   
      descriptionBottomText,
      descriptionBottomTextJustified,
      primaryPress,
      primaryPressPayload ={} ,
      primaryText,
      secondaryPress,
      secondaryPressPayload = {},
      secondaryText,
      additionalProps,
      titleTextStyle = {}, 
      topTitleContainerStyle = {}, 
      descriptionTextStyle = {}, 
      descriptionBottomTextStyle = {}, 
      descriptionBottomStyle = {}, 
      descriptionTitleTextStyle = {},
      descriptionTitleStyle = {},
      primaryButtonStyle = {}, 
      secondaryButtonStyle = {} ,
    } = this.props;

    const TopImageView = () => {
      if (!hideTopImageView) {
        return (<View style={styles.topImageContainerStyle}>
          <Image
            resizeMode="contain"
            resizeMethod="scale"
            style={styles.titleImageStyle}
            source={this.props.icon}
          />
        </View>);
      } else if (title) {
        return (<View style={[styles.topTitleContainer, topTitleContainerStyle]} >
          <Text style={[styles.titleTextStyle, titleTextStyle]}> {title} </Text>
        </View>);
      }  else {
        return true;
      }  
    };

    const DescriptionTitleView = () => {
      if (descriptionTitle) {
        return ( <View style={[ styles.descriptionTitleContainer, descriptionTitleStyle] }>
          <Text textAlign="center" style={[styles.descriptionTitleTextStyle, descriptionTitleTextStyle]}>{descriptionTitle}</Text>
        </View>);
      } else { 
        return true;
      }
    };

    const DescriptionTextView = () => {
      if (customDescriptionTextView) {
        return true;
      } else {
        return (<Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{description}</Text>);
      }   
    };

    const DescriptionBottomTextView = () => {
      if (descriptionBottomText) {
        return ( <View style={[ styles.descriptionBottomContainer, descriptionBottomStyle] }>
          <Text textAlign="center" style={[styles.descriptionBottomTextStyle, descriptionBottomTextStyle]}>{descriptionBottomText}</Text>
        </View>);
      } else { 
        return true;
      }
    };
    
    const ElementItem = ({ leftTxt, rightTxt }) => (
      <View style={[styles.detailElementContainer]}>
        <View style={styles.elementLeft}>
          <Text style={styles.elementLeftTxt}>
            {leftTxt}</Text>
        </View>
        <View style={styles.elementMiddle}>
          <Text style={styles.elementTxt}>
            :</Text>
        </View>
        <View style={styles.elementRight}>
          <Text style={styles.elementTxt}>
            {rightTxt}</Text>
        </View>
      </View>
    );

    const DescriptionBottomTextViewJustified = () => {
      if (descriptionBottomTextJustified) {
        return ( 
          <View style={[ styles.descriptionBottomContainer] }>
            {descriptionBottomTextJustified.map((value, i) =>
              <ElementItem 
                key={i} 
                leftTxt={value.label} 
                rightTxt={value.value}
              />
            )}
          </View>
        );
      } else { 
        return true;
      }
    };

    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          {/*Top image view*/}
          {TopImageView()}
          {/*Description Title view*/}
          {DescriptionTitleView()}
          <View style={styles.descriptionContainer}>
            {/*Description view*/}
            {DescriptionTextView()}
          </View>
          {/*Description bottom view*/}
          {DescriptionBottomTextView()}
          {/*Description bottom justified list view*/}
          {DescriptionBottomTextViewJustified()}
          <View style={styles.buttonContainer}>
            {secondaryText ? <Button children={secondaryText}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={() =>secondaryPress(secondaryPressPayload)}
            /> : null}
            <Button children={primaryText}
              childStyle={[styles.rightButtonStyle, primaryButtonStyle]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={disabled}
              onPress={() =>primaryPress(primaryPressPayload)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,    
  },
  modalContainer: {
    height: 'auto',
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
    paddingHorizontal: 10,
  },
  
  topImageContainerStyle: {
    height: 64,
    marginBottom : 5,
    marginTop: 10,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },  
  topTitleContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 5,  
  },
  
  descriptionContainer: {
    height: 'auto',
    justifyContent:'flex-start',
    paddingHorizontal: 15,
    paddingTop: 16,
    paddingBottom: 16,
  },
  
  descriptionTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    padding: 5,
  },

  descriptionBottomContainer: {
    height: 'auto',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingTop: 16,
    paddingBottom: 10,
  },
  
  buttonContainer: {
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 16,
    paddingBottom: 10,
    paddingRight: 10, 
  },
  
  titleImageStyle: {
    width: 54,
    height: 54
  },
  
  titleTextStyle: {
    fontSize: Styles.generalModalTitleTextSize,
    fontWeight:'500',
    color: Colors.colorBlack,
  },
  
  descriptionTextStyle: {
    fontSize: Styles.generalModalTextSize,
    color: Colors.generalModalTextColor,
  },
  
  descriptionTitleTextStyle: {
    fontSize: Styles.generalAlertTitleTextSize,
    color: Colors.colorBlack,
    fontWeight: 'bold',
  },

  descriptionBottomTextStyle: {
    fontSize: Styles.generalModalTextSize,
    color: Colors.generalModalTextColor,
    fontWeight: '500',
    alignSelf: 'flex-start'
  },

  leftButtonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingVertical: 5
  },
  rightButtonStyle: {
    color: Colors.colorGreen,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  },

  detailElementContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 5
  },

  elementLeft: {
    flex: 1.2,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.generalModalTextSize,
    alignSelf: 'flex-start',
    fontWeight : '500',

  },
  elementTxt: {
    fontSize: Styles.generalModalTextSize,
    alignSelf: 'flex-start',
    fontWeight : '500'
  },
});

export default GeneralModalAlert;
