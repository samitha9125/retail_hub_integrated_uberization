/*
 * File: SearchBar.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 10:54:05 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:38:55 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */
import React from 'react';
import { TextInput, View, Animated, Keyboard, TouchableOpacity } from 'react-native';
import Colors from '../../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const INITIAL_TOP = -60;

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: props.showOnLoad,
      top: new Animated.Value(
        INITIAL_TOP
      ),
      text:''
    };
    this.navigator = this.props.navigator;
  }

  show = () => {
    const { animate = true, animationDuration = 0 } = this.props;
    // if (clearOnShow) {
    //   this.setState({ input: '' });
    // }
    this.setState({ show: true });
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: 0,
        duration: animationDuration
      }).start();
    } else {
      this.setState({ top: new Animated.Value(0) });
    }
  };

  hide = () => {
    this.setState({ text: '' });
    const { animate = true, animationDuration = 0 } = this.props;
    // if (onHide) {
    //   onHide(this.state.input);
    // }
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: INITIAL_TOP,
        duration: animationDuration
      }).start();

      // const timerId = setTimeout(() => {
      //   this._doHide();
      //   clearTimeout(timerId);
      // }, animationDuration);
    }
    Keyboard.dismiss();
  };

  _handleBlur = () => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur();
    }
  };

  render() {

    const { 
      searchPlaceHolder = 'Search', 
      keyboardType = 'default',
      leftButtonProps = { icon: 'search', action: ()=>{} },
      rightButtonProps = { icon: 'clear', action: ()=>{} },
      hasLeftButton = false,
      hasRightButton = false,
      value=''
    } = this.props;

    return (
      
      <Animated.View style={[styles.viewStyle, { top: this.state.top }]}>
        <View style={styles.searchBarContainer}>
          {hasLeftButton?this.renderSearchLeftButton(leftButtonProps):true}
          <TextInput 
            ref={ref => (this.textInput = ref)}
            style={styles.textInputStyleClass}
            onChangeText={(text) =>{ 
              this.setState({ text: text });
              this.props.onChangeText(text); 
            }}
            onSubmitEditing={Keyboard.dismiss}
            // onLayout={() => this.textInput.focus()}
            onBlur={this._handleBlur}
            value={value}
            underlineColorAndroid='transparent'
            placeholder={searchPlaceHolder}
            keyboardType={keyboardType}
          />
          {hasRightButton?this.renderSearchRightButton(rightButtonProps):true}          
        </View>
      </Animated.View>
    );
  }

  renderSearchLeftButton = (leftButtonProps) => {
    return (
      <TouchableOpacity style={styles.searchBarIcon} onPress={leftButtonProps.action}>
        <MaterialIcons
          name={leftButtonProps.icon}
          size={styles.inputFieldIconSize}
          color={Colors.grey}
        />
      </TouchableOpacity>
    );
  }

  renderSearchRightButton = (rightButtonProps) => {
    return (
      <TouchableOpacity style={styles.clearIcon} onPress={rightButtonProps.action}>
        <MaterialIcons
          name={rightButtonProps.icon}
          size={styles.inputFieldIconSize}
          color={Colors.grey}
        />
      </TouchableOpacity>
    );
  }
}

const styles = {
  inputFieldIconSize: 30,
  viewStyle: {
    // flex:1,
    flexDirection: 'row',
    backgroundColor: Colors.navBarBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    // paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 2,
    position: 'absolute',
    // top: 0,
    left: 0,
    right:0,
    zIndex:9
  },
  textInputStyleClass:{    
    flex:9,
    textAlign: 'left',
    fontSize: 14,
    padding:0
  },
  searchBarContainer:{
    flex:1,
    flexDirection: 'row',
    backgroundColor : Colors.white,
    marginTop: 5,
    marginBottom: 5
  },  
  searchBarIcon:{
    width:35,
    margin: 5
  },
  clearIcon:{
    flex:1,
    margin: 5
  }
};

export { SearchBar };
