/*
 * File: Header.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 10:54:05 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:38:42 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */

import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../config/colors';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.navigator = this.props.navigator;
  }


  BackButtonView = () => {
    const { hideBackButton= false } = this.props;

    if (!hideBackButton) {
      return (<TouchableOpacity 
        underlayColor={Colors.transparent} 
        style={styles.backButton} 
        accessibilityLabel={`button_back`} 
        testID={`button_back`}
        onPress={() => this.props.backButtonPressed()}>
        <MaterialIcons
          name={"arrow-back"}
          size={styles.inputFieldIconSize}
          color={Colors.white}/>
      </TouchableOpacity>); 
    } else {
      return true;
    }
  };

  render() {
    const { testID ='' } = this.props;

    return (
      <View style={styles.viewStyle}>
        <View style={styles.backButtonView}
          accessibilityLabel={`back_${testID}`}
          testID={`back_${testID}`}>
          {this.BackButtonView()}
        </View>
        <View style={styles.headerTitleView}>
          <Text 
            adjustsFontSizeToFit
            numberOfLines={1} 
            accessibilityLabel={testID} 
            testID={testID}
            style={styles.textStyle}>{this.props.headerText}</Text>
        </View>
        <View style={styles.rightButtonsView}
          accessibilityLabel={`right_view_${testID}`}
          testID={`right_view_${testID}`}>
          { this.props.displaySearch ? 
            <TouchableOpacity 
              underlayColor={Colors.transparent} 
              style={styles.filterSearchButton} 
              onPress={this.props.searchButtonPressed.bind(this)}>
              <MaterialIcons
                name={"search"}
                size={styles.inputFieldIconSize}
                color={Colors.white}
              />
            </TouchableOpacity>
            : 
            true }
          { this.props.displayFilter ? 
            <TouchableOpacity 
              underlayColor={Colors.transparent} 
              style={styles.filterSearchButton} 
              onPress={this.props.filterButtonPressed.bind(this)}>
              <MaterialCommunityIcons
                name={"filter-outline"}
                size={styles.inputFieldIconSize}
                color={Colors.white}
              />
            </TouchableOpacity>
            : true}
        </View>
      </View>
    );
  }
}

const styles = {
  inputFieldIconSize: 30,
  viewStyle: {
    flexDirection: 'row',
    backgroundColor: Colors.navBarBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.5,
    // elevation: 2,
    position: 'relative',
    zIndex: 9999
  },
  filterSearchButton: {
    flex:1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55,
    marginRight:10
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55,
    paddingLeft: 10,
    paddingRight: 10,
    //backgroundColor: Colors.colorGreen,

  },
  filterButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  searchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  textStyle: {
    fontSize: 18,
    color: '#FFF',
    fontWeight: '500',
    textAlign: 'center'
  },
  headerTitleView:{
    flex:5,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  backButtonView:{
    flex:1,
    alignItems: 'center',
    marginLeft: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    zIndex: 1000000
  },
  rightButtonsView:{
    flex:2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    height: 55,
  }
};

export { Header };
