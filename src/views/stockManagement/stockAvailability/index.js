import React, { Component } from 'react';
import { View } from 'react-native';
import Colors from '../../../config/colors';
import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import StockAvailabiltyView  from '../../stockManagement/stockAvailability/stockAvailabiltyView';

class StockAvailability extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DeliveryMain :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app');
  }

  render() {
    return (
      <View>
        <StockAvailabiltyView {...this.props}/>
      </View>
    );
  }
}

export default StockAvailability;