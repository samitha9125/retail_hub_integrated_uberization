import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  DatePickerAndroid,
  RefreshControl,
  NetInfo

} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import ModalDropdown from 'react-native-modal-dropdown';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
import ActIndicator from '../../stockManagement/ActIndicator';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Header } from '../../../../src/views/stockManagement/stockAvailability/LobView/Header';
import { Dimensions } from 'react-native';
import imgUtilisable from '../../../../images/stock_availabilty/utilisable.png';
import imgGoods from '../../../../images/stock_availabilty/goods.png';
import imgTransfer from '../../../../images/stock_availabilty/transfer.png';
import imgReturn from '../../../../images/stock_availabilty/return.png';
import imgsold from '../../../../images/stock_availabilty/sold.png';
import imgrecovered_goods from '../../../../images/stock_availabilty/recovered_goods.png';
import imgtransfered from '../../../../images/stock_availabilty/transfered.png';
import strings from '../../../Language/StokAvailabilty';
import _ from 'lodash';
import globalConfig from '../../../config/globalConfig';



const Utill = new Utills();

class StockAvailabiltyView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      Datefilteringcount: [],
      lobArray: [],
      dropDownDataX: [],
      apiLoading: true,
      defaultValue: '',
      showText: false,
      api_error: false,
      selectedType: '',
      selectedStartDate: '',
      selectedEndDate: '',
      filterTypes: ['O2A_DELV'],
      disableFilter: false,
      dropDownData: [],
      screenName: [],
      // screenName1:[],
      screenNameUtilizable: '',
      selectedSortType: '',
      stock_detail: [],
      GoodInTransit: [],
      Utilisable: [],
      minDate: '',
      maxDate: '',
      Sold: [],
      Transferred: [],
      refresh: false,
      api_error_message: strings.api_error_message,
      locals: {
        headerTitle: strings.headerTitle,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        dateRange: strings.dateRange,
        filterBy: strings.filterBy,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };
    this.debounceDetail = _.debounce((item) => this.Detail(item), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });
    this.debounceDetail1 = _.debounce((item) => this.Detail1(item), globalConfig.defaultDelayTime,
      {
        'leading': true,
        'trailing': false
      });

  }



  setIndicater = (loadingState = true) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });

  }

  onRefresh() {
    this.setState({ refresh: false });
    this.LoadData();
    this.state.selectedSortType = ""
    this.state.selectedSortType = "All"
  }

  LoadData = () => {
    console.log('minDate', this.state.minDate)
    const data = {

    };

    Utill.apiRequestPost('getBucketWiseStockCount', 'materialAvailability', 'cfss', data, this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
    this.state.selectedSortType = "All"
  }

  FilterDate = async () => {

    await this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate');
    await this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate');

    const data = {
      FromDate: this.state.selectedStartDate,
      ToDate: this.state.selectedEndDate,
    };
    await Utill.apiRequestPost('getBucketWiseStockCount', 'materialAvailability', 'cfss', data, this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
  }


  FilterLob = (value) => {
    const data = {
      LOB: value

    };
    Utill.apiRequestPost('getBucketWiseStockCount', 'materialAvailability', 'cfss', data, this.LoadDataSuccessCb, this.LoadDataFailureCb, this.LoadDataExcb);
  }

  PostLob = () => {
    const data = {

    };
    Utill.apiRequestPost('GetLOBTypes', 'GoodsAvailability', 'cfss', data, this.LoadDataSuccessLob, this.LoadDataFailureCb, this.LoadDataExcb);
  }

  LoadDataExcb = (error) => {
    console.log('LoadDataExcb', error);
    if (error == 'No_Network') {
      error = this.state.locals.network_error_message;
      this.showNoNetworkModal(this.LoadData);
      try {
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: this.state.locals.network_error_message
          },
          overrideBackPress: true
        });
      } catch (error) {
        console.log(`Show Modal error: ${error.message}`);
      }

    } else {
      const navigatorOb = this.props.navigator;
      try {
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: this.state.locals.api_error_message
          },
          overrideBackPress: true
        });
      } catch (error) {
        console.log(`Show Modal error: ${error.message}`);
      }
    }
    this.setState({ api_error_message: error });
    setTimeout(() => {
      // this.setIndicater(false);
      this.setState({ api_error: true });
    }, 1000);
  }

  LoadDataFailureCb = (response) => {
    console.log('LoadDataFailureCb', response.data.error);
    this.setState({ isLoading: false });
    this.onRefresh();
    const navigatorOb = this.props.navigator;
    try {
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true,


      });
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }
  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: strings.headerTitle
      }
    });
  }


  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }
  //////// snakbar /////////

  LoadDataSuccessCb = (response) => {
    this.setState({
      selectedStartDate: response.data.data.FromDate,
      selectedEndDate: response.data.data.ToDate,
      lobArray: response.data.data.LOB,
      defaultValue: response.data.data.LOB[0],
      Utilisable: response.data.data.lob_data[0].StockList,
      Utilisable1: response.data.data.lob_data[0].bucketName,
      GoodsInTransit: response.data.data.lob_data[1].StockList,
      GoodsInTransit1: response.data.data.lob_data[1].bucketName,
      TransferGoodsInTransit: response.data.data.lob_data[2].StockList,
      TransferGoodsInTransit1: response.data.data.lob_data[2].bucketName,
      RecoverGoods: response.data.data.lob_data[3].StockList,
      RecoverGoods1: response.data.data.lob_data[3].bucketName,


      Sold: response.data.data.date_data[0].StockList,
      Sold1: response.data.data.date_data[0].bucketName,
      Return: response.data.data.date_data[1].StockList,
      Return1: response.data.data.date_data[1].StockList,
      Transferred: response.data.data.date_data[2].StockList,
      Transferred1: response.data.data.date_data[2].bucketName,

    });

    if (response.data.success) {
      this.setState({ dropDownDataX: response.data.data.LOB });

    }

    let stockBucket = [];
    response.data.data.lob_data.map((item, key) => {
      console.log()
      if (item.screenName === 'UTILISABLE_SCREEN') {
        item['bucketImage'] = imgUtilisable;
      } else if (item.screenName === 'GOODSINTRANSIT_SCREEN') {
        item['bucketImage'] = imgGoods;
      } else if (item.screenName === 'TRANSFERGOODSINTRANSIT_SCREEN') {
        item['bucketImage'] = imgTransfer;
      } else if (item.screenName === 'RECOVERGOODS_SCREEN') {
        item['bucketImage'] = imgrecovered_goods;
      }
      stockBucket.push(item);
    });
    let stockDateBucket1 = [];

    response.data.data.date_data.map((item, key) => {
      if (item.screenName === 'SOLD_SCREEN') {
        item['bucketImage'] = imgsold;
      } else if (item.screenName === 'RETURNED_SCREEN') {
        item['bucketImage'] = imgReturn;
      } else if (item.screenName === 'TRANSFERRED_SCREEN') {
        item['bucketImage'] = imgtransfered;
      }
      stockDateBucket1.push(item);
    });
    this.setState({ stockBucket: stockBucket, stockDateBucket1: stockDateBucket1 });
    this.setIndicater(false);

    if (response.data.success) {
      this.setState({ showText: true });
    }
    this.dissmissNoNetworkModal()
  }

  Detail = (item) => {
    const navigatorOb = this.props.navigator;
    for (let i = 0; i < this.state.stockBucket.length; i++) {
      if (item.screenName === 'UTILISABLE_SCREEN') {
        navigatorOb.push({
          title: this.state.Utilisable1,
          screen: 'DialogRetailerApp.views.LobList',
          passProps: {
            List: this.state.Utilisable,
            name: this.state.Utilisable1,
            lob: this.state.selectedSortType
          }

        });
        return;
      } else if (item.screenName === 'GOODSINTRANSIT_SCREEN') {
        navigatorOb.push({
          title: this.state.GoodsInTransit1,
          screen: 'DialogRetailerApp.views.LobList',
          passProps: {
            List: this.state.GoodsInTransit,
            name: this.state.GoodsInTransit1,
            lob: this.state.selectedSortType
          }
        });
        return;
      } else if (item.screenName === 'TRANSFERGOODSINTRANSIT_SCREEN') {
        navigatorOb.push({
          title: this.state.TransferGoodsInTransit,
          screen: 'DialogRetailerApp.views.LobList',
          passProps: {
            List: this.state.TransferGoodsInTransit,
            name: this.state.TransferGoodsInTransit1,
            lob: this.state.selectedSortType
          }
        });
        return;
      } else if (item.screenName === 'RECOVERGOODS_SCREEN') {
        navigatorOb.push({
          title: this.state.RecoverGoods,
          screen: 'DialogRetailerApp.views.LobList',
          passProps: {
            List: this.state.RecoverGoods,
            name: this.state.RecoverGoods1,
            lob: this.state.selectedSortType
          }
        });
        return;
      }
    }
  }

  Detail1 = (item) => {
    const navigatorOb = this.props.navigator;
    for (let i = 0; i < this.state.stockDateBucket1.length; i++) {
      if (item.screenName === 'SOLD_SCREEN') {
        navigatorOb.push({
          title: this.state.Sold,
          screen: 'DialogRetailerApp.views.DateList',
          passProps: {
            List: this.state.Sold,
            name: this.state.Sold1,
            startDate: this.state.selectedStartDate,
            endDate: this.state.selectedEndDate
          }
        });
        return;
      } else if (item.screenName === 'RETURNED_SCREEN') {
        navigatorOb.push({
          title: this.state.Return,
          screen: 'DialogRetailerApp.views.DateList',
          passProps: {
            List: this.state.Return,
            name: 'Returned',
            startDate: this.state.selectedStartDate,
            endDate: this.state.selectedEndDate

          }
        });
        return;
      } else if (item.screenName === 'TRANSFERRED_SCREEN') {
        navigatorOb.push({
          title: this.state.Transferred,
          screen: 'DialogRetailerApp.views.DateList',
          passProps: {
            List: this.state.Transferred,
            name: 'Transferred',
            startDate: this.state.selectedStartDate,
            endDate: this.state.selectedEndDate

          }
        });
        return;
      }
    }
  }

  componentWillMount() {
    this.LoadData();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.setIndicater(false);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  sortTypeSelected(idx, value) {
    this.setState({ selectedSortType: value });
    console.log("idx: ", idx, "\nvalue: ", value);
    this.FilterLob(value);

  }




  renderListItem = ({ item, index }) => (
    <View style={styles.background}>
      {item.count != 0 ?
        <View style={styles.container}>

          <TouchableOpacity
            key={index}
            style={styles.cardContainer}
            underlayColor={Colors.underlayColor}
            onPress={() => this.debounceDetail(item)}>
            <View style={styles.leftImageContainer}>
              <Image source={item.bucketImage} style={styles.orderImage} resizeMode="cover" />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{item.screenTitle}
              </Text>
            </View>
            <View style={styles.rightImageContainer}>

              <View style={styles.orderCountContainer}>
                <Text style={styles.orderCountText}>
                  {item.count}
                </Text>
              </View>
              <View />
            </View>
          </TouchableOpacity>

        </View>

        :
        <TouchableOpacity
          key={index}
          disabled={true}
          style={styles.cardContainer1}
          underlayColor={Colors.underlayColor}
          onPress={() => this.debounceDetail1(item)}>
          <View style={styles.leftImageContainer}>
            <Image source={item.bucketImage} style={styles.orderImage} resizeMode="cover" />
          </View>
          <View style={styles.middleTextContainer}>
            <Text style={styles.innerText}>{item.screenTitle}
            </Text>
          </View>
          <View style={styles.rightImageContainer}>

            <View style={styles.orderCountContainer}>
              <Text style={styles.orderCountText}>
                {item.count}
              </Text>
            </View>
            <View />
          </View>
        </TouchableOpacity>

      }
    </View >
  );
  api_error_message
  renderListItem1 = ({ item, index }) => (
    <View style={styles.background}>
      {item.count != 0 ?
        <View style={styles.container}>
          <TouchableOpacity
            key={index}
            style={styles.cardContainer}
            underlayColor={Colors.underlayColor}
            onPress={() => this.debounceDetail1(item)}>
            <View style={styles.leftImageContainer}>
              <Image source={item.bucketImage} style={styles.orderImage} resizeMode="cover" />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{item.screenTitle}
              </Text>
            </View>
            <View style={styles.rightImageContainer}>

              <View style={styles.orderCountContainer}>
                <Text style={styles.orderCountText}>
                  {item.count}
                </Text>
              </View>
              <View />
            </View>
          </TouchableOpacity>

        </View>

        :
        <TouchableOpacity
          key={index}
          disabled={true}
          style={styles.cardContainer1}
          underlayColor={Colors.underlayColor}
          onPress={() => this.debounceDetail(item)}>
          <View style={styles.leftImageContainer}>
            <Image source={item.bucketImage} style={styles.orderImage} resizeMode="cover" />
          </View>
          <View style={styles.middleTextContainer}>
            <Text style={styles.innerText}>{item.screenTitle}
            </Text>
          </View>
          <View style={styles.rightImageContainer}>

            <View style={styles.orderCountContainer}>
              <Text style={styles.orderCountText}>
                {item.count}
              </Text>
            </View>
            <View />
          </View>
        </TouchableOpacity>

      }
    </View >
  );

  // this.props.navigator.showSnackbar({ text: 'Woo Snacks!' });
  //////--------Date Picker -----//////


  async openDatePicker(selStartDate, selEndDate, dateType) {
    var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '');

    var initialDate;
    var minDate = new Date().setDate((new Date().getDate() - 30));
    var maxDate = new Date();

    console.log('this.state.selectedStartDate ' + selStartDate.slice(0, 10));
    console.log('this.state.selectedEndDate', selEndDate.slice(0, 10));

    let formattedStartDate = selStartDate.slice(0, 10);
    let formattedEndDate = selEndDate.slice(0, 10);

    console.log('formattedStartDate ' + new Date(formattedStartDate));
    console.log('formattedEndDate ', new Date(formattedEndDate));
    console.log('utc: ', utc);

    if (dateType == 'startDate') {
      if (this.state.selectedEndDate == 'Finishing Date') {
        maxDate = new Date(formattedEndDate);
        this.setState({ maxDate: maxDate })
      }
      let stDate = selStartDate.slice(0, 10);
      let stYear = stDate.slice(0, 4);
      let stMonth = parseInt(stDate.slice(5, 7)) - 1;
      let stDay = stDate.slice(8, 10);
      console.log('new Date(stYear,stMonth,stDay): ', stYear + stMonth + stDay);
      initialDate = (selStartDate == '' ? new Date(utc) : new Date(stYear, stMonth, stDay));
    } else {
      if (this.state.selectedEndDate == 'Starting Date') {
        minDate = new Date(formattedStartDate);
        this.setState({ minDate: minDate })
      }
      let enDate = selEndDate.slice(0, 10);
      let enYear = enDate.slice(0, 4);
      let enMonth = parseInt(enDate.slice(5, 7)) - 1;
      let enDay = enDate.slice(8, 10);
      console.log('new Date(stYear,stMonth,stDay): ', enYear + enMonth + enDay);
      initialDate = (selEndDate == '' ? new Date(utc) : new Date(enYear, enMonth, enDay));
    }

    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        minDate: minDate,
        date: initialDate,
        maxDate: maxDate,
        mode: 'calendar'
      });

      if (action !== DatePickerAndroid.dismissedAction) {
        let selectedMonth = (month + 1).toString().length > 1 ? (month + 1).toString() : '0' + (month + 1).toString();
        let selectedDay = (day < 10 ? '0' + day : day);
        let selectedDate = year + '-' + selectedMonth + '-' + selectedDay;
        if (dateType == 'startDate') {
          this.setState({ selectedStartDate: selectedDate });
        } else {
          selectedDate = year + '-' + selectedMonth + '-' + selectedDay;
          this.setState({ selectedEndDate: selectedDate });
        }
      }
    } catch ({ code, message }) {
      console.log('Cannot open date picker', message);
    }
  }




  //////--------Date Picker --------/////
  render() {
    ///----- Date Filter ----////

    const CalendarSelctionStartDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity style={styles.dropdownElementContainer1}>
        <View style={styles.dropdownDataElemint1}>
          <Text style={
            placeHolderText === 'Starting Date' ? styles.dropdownDataElemintTxtPlaceHolder :
              styles.dropdownDataElemintTxt}>
            {placeHolderText}
          </Text>
        </View>

      </TouchableOpacity>
    );

    const CalendarSelctionFinishDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElemint}>
          <Text style={
            placeHolderText === 'Finishing Date' ? styles.dropdownDataElemintTxtPlaceHolder :
              styles.dropdownDataElemintTxt}>
            {placeHolderText}
          </Text>
        </View>

      </TouchableOpacity>
    );

    ///----Date filter End-----////

    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refresh}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
        >
          <View style={styles.dropdownView}>
            {this.state.showText === true ?
              //{/* SORTING VIEW START */}
              <View style={styles.sortingView}>
                {/* SORT BY LABEL START */}
                <View style={styles.sortByLabelContainer}>
                  <Text style={styles.txtSortBy}>{this.state.locals.filterBy}</Text>
                </View>
                {/* SORT BY LABEL END */}
                {/* DROP DOWN SECTION START */}
                <View style={styles.sortByDropDownContainer}>
                  <ModalDropdown
                    options={this.state.dropDownDataX}
                    style={styles.dropDownMain}
                    dropdownStyle={styles.dropDownStyle}
                    defaultValue={this.state.defaultValue}
                    dropdownTextStyle={styles.dropDownTextStyle}
                    dropdownTextHighlightStyle={styles.dropDownTextHighlightStyle}
                    defaultIndex={-1}
                    onSelect={(idx, value) => this.sortTypeSelected(idx, value)}>
                    <View style={styles.dropDownSelectedItemView}>

                      <View style={styles.selectedItemTextContainer}>
                        <Text style={styles.sortTypeText}>
                          {this.state.selectedSortType}
                        </Text>
                      </View>

                      <View style={styles.dropDownIcon}>
                        <Ionicons name='md-arrow-dropdown' size={20} />
                      </View>
                    </View>
                  </ModalDropdown>
                </View>
                {/* DROP DOWN SECTION END */}
              </View>

              : null}</View>
          <FlatList
            data={this.state.stockBucket}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={false}
            refreshing={false}
            onRefresh={this.LoadData} />
          <View>{this.state.showText === true ?

            <View style={styles.dateView}>
              <View style={styles.container1}>
                <View style={{ flexDirection: 'row', flex: 1, padding: 5 }}>
                  <View style={{ flex: 0.3, justifyContent: 'center' }}>
                    <Text style={styles.dateLable}>{this.state.locals.dateRange}</Text>
                  </View>
                  <View style={styles.pickersView}>
                    {/* START DATE */}
                    <View style={styles.filterSection1}>

                      {/* <View style={styles.dropDownTextContainer}>
                      <Text style={styles.dropDownText}>{this.state.locals.startDate}</Text>
                    </View> */}
                      <CalendarSelctionStartDate
                        placeHolderText={this.state.selectedStartDate}
                        onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate')} />
                    </View>
                    <View><Text style={{ paddingBottom: 10 }}> - </Text></View>
                    {/* END DATE */}
                    <View style={styles.filterSection}>
                      <CalendarSelctionFinishDate
                        placeHolderText={this.state.selectedEndDate}
                        onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate')} />
                    </View>
                    <View style={styles.dropdownArrow}>
                      <TouchableOpacity onPress={() => this.FilterDate()}>
                        <View>
                          <Ionicons style={{ paddingBottom: 7 }} name='md-arrow-dropdown' size={20} />
                          {/* <Ionicons  name='ios-arrow-down' size={20}/> */}
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            : null}</View>
          <FlatList
            data={this.state.stockDateBucket1}
            renderItem={this.renderListItem1}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={false}
            refreshing={false}
            onRefresh={this.LoadData} />

        </ScrollView>

      );
    } else {
      screenContent = (

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refresh}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
        >
          <View style={styles.dropdownView}>
            {this.state.showText === true ?
              //{/* SORTING VIEW START */}
              <View style={styles.sortingView}>
                {/* SORT BY LABEL START */}
                <View style={styles.sortByLabelContainer}>
                  <Text style={styles.txtSortBy}>{this.state.locals.filterBy}</Text>
                </View>
                {/* SORT BY LABEL END */}
                {/* DROP DOWN SECTION START */}
                <View style={styles.sortByDropDownContainer}>
                  <ModalDropdown
                    options={this.state.dropDownDataX}
                    style={styles.dropDownMain}
                    dropdownStyle={styles.dropDownStyle}
                    defaultValue={this.state.defaultValue}
                    dropdownTextStyle={styles.dropDownTextStyle}
                    dropdownTextHighlightStyle={styles.dropDownTextHighlightStyle}
                    defaultIndex={-1}
                    onSelect={(idx, value) => this.sortTypeSelected(idx, value)}>
                    <View style={styles.dropDownSelectedItemView}>

                      <View style={styles.selectedItemTextContainer}>
                        <Text style={styles.sortTypeText}>
                          {this.state.selectedSortType}
                        </Text>
                      </View>

                      <View style={styles.dropDownIcon}>
                        <Ionicons name='md-arrow-dropdown' size={20} />
                      </View>
                    </View>
                  </ModalDropdown>
                </View>
                {/* DROP DOWN SECTION END */}
              </View>

              : null}</View>
          <FlatList
            data={this.state.stockBucket}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={false}
            refreshing={false}
            onRefresh={this.LoadData} />
          <View>{this.state.showText === true ?

            <View style={styles.dateView}>
              <View style={styles.container1}>
                <View style={{ flexDirection: 'row', flex: 1, padding: 5 }}>
                  <View style={{ flex: 0.3, justifyContent: 'center' }}>
                    <Text style={styles.dateLable}>Date Range</Text>
                  </View>
                  <View style={styles.pickersView}>
                    {/* START DATE */}
                    <View style={styles.filterSection1}>

                      {/* <View style={styles.dropDownTextContainer}>
                    <Text style={styles.dropDownText}>{this.state.locals.startDate}</Text>
                  </View> */}
                      <CalendarSelctionStartDate
                        placeHolderText={this.state.selectedStartDate}
                        onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate')} />
                    </View>
                    <View><Text style={{ paddingBottom: 10 }}> - </Text></View>
                    {/* END DATE */}
                    <View style={styles.filterSection}>
                      <CalendarSelctionFinishDate
                        placeHolderText={this.state.selectedEndDate}
                        onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate')} />
                    </View>
                    <View style={styles.dropdownArrow}>
                      <TouchableOpacity onPress={() => this.FilterDate()}>
                        <View>
                          <Ionicons style={{ paddingBottom: 7 }} name='md-arrow-dropdown' size={20} />
                          {/* <Ionicons  name='ios-arrow-down' size={20}/> */}
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            : null}</View>
          <FlatList
            data={this.state.stockDateBucket1}
            renderItem={this.renderListItem1}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={false}
            refreshing={false}
            onRefresh={this.LoadData} />

        </ScrollView>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.headerTitle} />
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        {screenContent}
      </View>
    );
  }
}

const ElementSelctionMenu = ({ dropDownData, placeHolderText, onSelect, defaultIndex }) => (
  <ModalDropdown
    options={dropDownData}
    onSelect={onSelect}
    defaultIndex={parseInt(defaultIndex)}
    style={styles.modalDropdownStyles}
    dropdownStyle={styles.dropdownStyle}
    dropdownTextStyle={styles.dropdownTextStyle}
    dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
    <View style={styles.dropdownElementContainer}>
      <View style={styles.dropdownDataElemint}>
        <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
      </View>
      <View style={styles.dropdownArrow}>
        <Ionicons name='md-arrow-dropdown' size={20} />
      </View>
    </View>
  </ModalDropdown>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockManagment ===> MainPage ');
  console.log('****** REDUX STATE :: StockManagment => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({

  background: {
    backgroundColor: Colors.appBackgroundColor,
    flex: 1
  },
  container: {

    height: '100%'
  },
  dropdownView: {
    flex: 1
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 6,
    backgroundColor: Colors.colorYellow,
    alignItems: 'center'
  },

  cardContainer1: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    backgroundColor: Colors.btnDeactive,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 6,
    alignItems: 'center'
  },
  leftImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 5
  },

  orderImage: {
    alignSelf: 'center',
    width: undefined,
    height: undefined,
    margin: 5,
    padding: 25,
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent

  },
  middleTextContainer: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  rightImageContainer: {
    flex: 2,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  orderCountContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',

    padding: 10,
    height: 56,
    width: 75,

  },



  orderCountText: {
    textAlign: 'right',
    fontSize: Styles.delivery.defaultFontSize,
    color: Colors.black
  },

  innerText: {
    textAlign: 'left',
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    padding: 8
  },

  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
  dropDownTextStyle: {
    color: Colors.black,
    fontSize: 15,
    fontWeight: 'bold'
  },
  dropDownMain: {
    width: Dimensions.get('window').width * 0.7
  },
  dropDownStyle: {
    flex: 1,
    width: Dimensions.get('window').width * 0.7,
    height: 160
  },
  dropDownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  dropDownSelectedItemView: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  },
  selectedItemTextContainer: {
    width: '90%',
    paddingLeft: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sortTypeText: {
    flex: 1,
    color: Colors.black,
    fontSize: 15,
    marginTop: 5,
    fontWeight: 'bold'
  },
  dropDownIcon: {
    width: '10%',
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  sortingView: {
    flex: 0.08,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    alignSelf: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'center',

  },
  sortByLabelContainer: {

    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop:10,
    marginLeft: 0
  },
  txtSortBy: {
    fontWeight: 'bold',
    color: Colors.black,
    alignSelf: 'center',
    fontSize: 16
  },
  sortByDropDownContainer: {
    flex: 3,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    height: 60,
  },
  pickersView: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 0.7,
    // width: '75%',
    flexDirection: 'row',
    borderRadius: 8,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: Colors.colorYellow
  },
  container1: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
  },
  filterSection: {

    width: '40%',
    flexDirection: 'column'
  },
  filterSection1: {
    marginLeft: 10,
    width: '40%',
    flexDirection: 'column'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },

  dropdownElementContainer1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,

    // marginLeft: 15,
    // marginRight: 15,


  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',

  },
  dropdownDataElemint1: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    // marginLeft:
  },
  dropdownArrow: {
    flex: 2,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  },
  dropdownDataElemintTxtPlaceHolder: {
    color: Colors.borderColorGray,
    fontSize: 15,
    fontWeight: '400'
  },
  dateView: {
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  dateLable: {
    fontWeight: 'bold',
    color: Colors.black,
    alignSelf: 'center',
    fontSize: 16,


  }


});

export default connect(mapStateToProps, actions)(StockAvailabiltyView);

