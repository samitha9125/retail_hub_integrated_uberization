import React from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, BackHandler, Alert, NetInfo } from 'react-native';
import Colors from '../../../../config/colors';
import strings from '../../../../Language/StockAcceptance';
import { Header } from '../LobView/Header';
import * as actions from '../../../../actions';
import { connect } from 'react-redux';
import Color from '../../../../config/colors';
// const Utill = new Utills();
class DateListView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      data: [],
      page: 0,
      loading: false,
      apiLoading: false,
      isfetching: false,
      refresh: false,
      api_error_message: '',
      locals: {
        screenTitle: this.props.name,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;

  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  gotoDeatilList(keyToSend, valueToSend, discriptionToSend) {
    console.log('keyToSendSend', keyToSend);
    console.log('valueToSendSend', valueToSend);
    console.log('valueToSendSend', discriptionToSend);
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.props.name,
      screen: 'DialogRetailerApp.views.DateListDetail',
      passProps: {
        keyToSend: keyToSend,
        valueToSend: valueToSend,
        discriptionToSend: discriptionToSend,
        name: this.props.name,
        List: this.props.List,
        startDate: this.props.startDate,
        endDate: this.props.endDate
      }
    });
    navigatorOb.pop();
  }

  renderListItem = ({ item }) => (
    <View style={styles.background}>
      <TouchableOpacity
        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.gotoDeatilList(item.key, item.value, item.description)}
      >
        <View>
          <View style={styles.detailElimentContainer}>
            <View style={styles.containerMiddleSection2}>
              <View style={styles.elementLeft}>
                <Text style={styles.elementTxt}>
                  {item.description}</Text>
              </View>
            </View>
            <View style={styles.containerMiddleSection3}>
              <View style={styles.elementLeft}>
                <Text style={styles.elementTxt}>
                  :</Text>
              </View>
            </View>
            <View style={styles.containerMiddleSection4}>
              <View style={styles.elementMiddle}>
                <Text style={styles.elementTxt}>
                  {item.value}</Text>
              </View>
            </View>
            <View style={styles.arrowView}><Image
              source={require('../../../../../images/icons/arrow.png')}
              style={styles.orderImage} resizeMode="cover"
            /></View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );


  render() {
    const CalendarSelctionStartDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer1}>
        <View style={styles.dropdownDataElemint1}>
          <Text style={
            placeHolderText === 'Starting Date' ? styles.dropdownDataElemintTxtPlaceHolder :
              styles.dropdownDataElemintTxt}>
            {placeHolderText}
          </Text>
        </View>

      </TouchableOpacity>
    );

    const CalendarSelctionFinishDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElemint}>
          <Text style={
            placeHolderText === 'Finishing Date' ? styles.dropdownDataElemintTxtPlaceHolder :
              styles.dropdownDataElemintTxt}>
            {placeHolderText}
          </Text>
        </View>

      </TouchableOpacity>
    );
    let screenContent;
    if (this.props.List) {
      screenContent = (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', backgroundColor: Color.appBackgroundColor }}>
            <View style={{ flex: 0.3 }}>
              <View style={{ paddingTop: 20, paddingLeft: 10 }}>
                <Text style={{ color: Color.black, fontWeight: 'bold', fontSize: 16 }}>Date Range</Text>
              </View>
            </View>
            <View style={{
              flex: 0.7
            }}>
              <View style={{ margin: 10, paddingTop: 10, paddingBottom: 10, alignItems: 'center', borderRadius: 8, borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderBottomWidth: 1, borderColor: Colors.colorYellow }}>
                <Text style={{ color: Color.black, fontSize: 16 }}>{this.props.startDate}  -  {this.props.endDate}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: '90%' }}>
            <FlatList
              // onRefresh={() => this.onRefresh()}
              refreshing={this.state.isfetching}
              data={this.props.List}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => item.toString() && index.toString()}
              scrollEnabled={true}
            />
          </View>
        </View>
      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }


    return (

      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle.toUpperCase()} />
        {screenContent}
      </View>

    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockManagment ===> MainPage ');
  console.log('****** REDUX STATE :: StockManagment => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({

  container: {
    height: '100%',
  },
  background: {
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 10
    // flex: 1
  },
  containerMiddleSection2: {
    marginLeft: 20,
    flex: 1.5,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  containerMiddleSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  containerMiddleSection4: {
    paddingLeft: 10,
    flex: 0.3,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginRight: 30
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 20
  },

  elementLeft: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    borderWidth: 0
  },
  elementMiddle: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderWidth: 0,

  },

  elementTxt: {
    fontSize: 15,
    color: Colors.primaryText,

  },
  orderImage: {
    width: 20,
    height: 20

  },
  arrowView: {
    marginRight: 5

  },
  pickersView: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 0.7,
    // width: '75%',
    flexDirection: 'row',
    borderRadius: 8,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: Colors.colorYellow
  },
  filterSection1: {
    marginLeft: 10,
    width: '40%',
    flexDirection: 'column'
  },

});
export default connect(mapStateToProps, actions)(DateListView);
