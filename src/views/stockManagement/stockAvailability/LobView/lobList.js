import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, RefreshControl, BackHandler, Alert, NetInfo } from 'react-native';
import Colors from '../../../../config/colors';
import Styles from '../../../../config/styles';
import strings from '../../../../Language/StokAvailabilty';
import { Header } from './Header';
import * as actions from '../../../../actions';
import { ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Dimensions } from 'react-native';
// const Utill = new Utills();
class LobListView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      data: [],
      page: 0,
      loading: false,
      apiLoading: true,
      isfetching: false,
      refresh: false,
      api_error_message: strings.api_error_message,
      locals: {
        screenTitle: this.props.name,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        filterBy: strings.filterBy,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };

  }
  componentWillMount() {

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.setIndicater(false);
  }
  setIndicater = (loadingState = true) => {
    this.setState({ apiLoading: loadingState });
  }
  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }
  gotoDeatilList(keyToSend, valueToSend, discriptionToSend) {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: this.props.name,
      screen: 'DialogRetailerApp.views.LobListDetail',
      passProps: {
        discriptionToSend: discriptionToSend,
        keyToSend: keyToSend,
        valueToSend: valueToSend,
        name: this.props.name,
        List: this.props.List,
        lob: this.props.lob
      }
    });
    navigatorOb.pop();
  }

  renderListItem = ({ item }) => (
    <View style={styles.background}>
      <TouchableOpacity
        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.gotoDeatilList(item.key, item.value, item.description)}
      >
        <View>
          <View style={styles.detailElimentContainer}>
            <View style={styles.containerMiddleSection2}>
              <View style={styles.elementLeft}>
                <Text style={styles.elementTxt}>
                  {item.description}</Text>
              </View>
            </View>
            <View style={styles.containerMiddleSection3}>
              <View style={styles.elementLeft}>
                <Text style={styles.elementTxt}>
                  :</Text>
              </View>
            </View>
            <View style={styles.containerMiddleSection4}>
              <View style={styles.elementMiddle}>
                <Text style={styles.elementTxt}>
                  {item.value}</Text>
              </View>
            </View>
            <View style={styles.arrowView}><Image
              source={require('../../../../../images/icons/arrow.png')}
              style={styles.orderImage} resizeMode="cover"
            /></View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );

  render() {
    let screenContent;
    if (this.props.List) {
      screenContent = (
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', backgroundColor: Colors.appBackgroundColor }}>
            <View style={{ paddingTop: 10, paddingRight: 35, marginLeft: 25 }}>
              <Text style={styles.txtSortBy}>{this.state.locals.filterBy}</Text>
            </View>
            <View syle={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
              <View style={{ color: Colors.white, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Text style={{ fontWeight: 'bold', color: Colors.black, alignSelf: 'center', fontSize: 16, paddingTop: 10 }}>{this.props.lob}</Text>
              </View>
            </View>
          </View>
          <FlatList style={styles.background}
            // onRefresh={() => this.onRefresh()}
            refreshing={this.state.isfetching}
            data={this.props.List}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={true}
          />
        </View>
      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          {this.props.navigator.showSnackbar({ text: this.state.api_error_message })}
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }
    return (

      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle.toUpperCase()} />
        {/* { loadingIndicator } */}
        {screenContent}
      </View>

    );
  }
}
const ElementSelctionMenu = ({ dropDownData, placeHolderText, onSelect, defaultIndex }) => (
  <ModalDropdown
    options={dropDownData}
    onSelect={onSelect}
    disabled={true}
    defaultIndex={parseInt(defaultIndex)}
    style={styles.dropDownMain}
    dropdownStyle={styles.dropDownStyle}
    dropdownTextStyle={styles.dropdownTextStyle}
    dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
    <View style={styles.dropdownElementContainer}>
      <View style={styles.dropdownDataElemint}>
        <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
      </View>
      <View style={styles.dropdownArrow}>
        <Ionicons name='md-arrow-dropdown' size={20} />
      </View>
    </View>
  </ModalDropdown>
);
const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockManagment ===> MainPage ');
  console.log('****** REDUX STATE :: StockManagment => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};
const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  background: {
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 10
    // flex: 1
  },
  containerMiddleSection2: {
    marginLeft: 20,
    flex: 1.5,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  containerMiddleSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  containerMiddleSection4: {
    paddingLeft: 10,
    flex: 0.3,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginRight: 30
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 20
  },

  elementLeft: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    borderWidth: 0
  },
  elementMiddle: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    borderWidth: 0,

  },

  elementTxt: {
    fontSize: 15,
    color: Colors.primaryText,

  },
  orderImage: {
    width: 20,
    height: 20

  },
  arrowView: {
    marginRight: 5

  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
  dropDownTextContainer: {
    flex: 0.7,
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18

  },
  //modal dropdown styles
  dropDownMain: {
    width: Dimensions.get('window').width * 0.7
  },
  dropDownStyle: {
    flex: 1,
    width: Dimensions.get('window').width * 0.7,
    height: 160
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15,
    fontWeight: 'bold'
  },
  sortByLabelContainer: {

    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop:10,
    marginLeft: 0
  },
  txtSortBy: {
    fontWeight: 'bold',
    color: Colors.black,
    alignSelf: 'center',
    fontSize: 16,

  },
});
export default connect(mapStateToProps, actions)(LobListView);