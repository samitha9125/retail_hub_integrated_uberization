import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  FlatList,
  ScrollView,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import ModalDropdown from 'react-native-modal-dropdown';
import Styles from '../../../../config/styles';
import Colors from '../../../../config/colors';
import Utills from '../../../../utills/Utills';
import ActIndicator from '../../../stockManagement/ActIndicator';
import Ionicons from 'react-native-vector-icons/Ionicons';
import strings from '../../../../Language/Wom';
import { Dimensions } from 'react-native';
import { colors } from '../../../../config/stylescfss';
import { SearchBar } from './SearchBar';
import { Header } from './Header';


const Utill = new Utills();
class LobListDetail extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      Datefilteringcount: [],
      lobArray: [],
      dropDownDataX: [],
      apiLoading: true,
      showText: false,
      api_error: false,
      selectedType: '',
      disableFilter: false,
      dropDownData: [],
      screenName: [],
      screenNameUtilizable: '',
      selectedSortType: '',
      detail: [],
      searchResults: [],
      query: '',
      newData: '',
      fullData: [],
      array: [],
      searchBarHidden: true,
      refresh: false,
      api_error_message: strings.api_error_message,
      locals: {
        screenTitle: this.props.name,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        noserialfound: strings.noserialfound,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }

    };
    this.arrayholder = [];
  }

  setIndicater = (loadingState = true) => {
    this.setState({ apiLoading: loadingState });
  }


  getCount = () => {
    const data = {
      bucket_name: this.props.name,
      material_code: this.props.keyToSend,
    };
    Utill.apiRequestPost('getMaterials', 'materialAvailability', 'cfss', data, this.getCountSuccessCb, this.getCountFailureCb, this.getCountExcb);

  }

  getCountExcb = (error) => {
    if (error == 'No_Network') {
      error = this.state.locals.network_error_message;
      this.showNoNetworkModal(this.getCount);
    } else {
      error = this.state.locals.api_error_message;
    }
    this.setState({ api_error_message: error });
    setTimeout(() => {
      this.setIndicater(false);
      this.setState({ api_error: true });
    }, 1000);
  }

  getCountFailureCb = (response) => {
    this.setState({ api_error_message: response.data.error });
    this.setState({ api_error: true });
    this.setIndicater(false);
  }

  getCountSuccessCb = (response) => {
    this.setState({
      detail: response.data.data,
      array: response.data.data
    });
    this.arrayholder = response.data.data
    this.setIndicater(false);

    if (response.data.success) {
      this.setState({ showText: true });
    }
    this.dissmissNoNetworkModal()
  }



  componentWillMount() {
    this.getCount();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }



  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.setIndicater(false);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }
  
  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }
  handleSearch() {
    this.searchBar.show();
    this.setState({ searchBarHidden: false });
  }


  searchFilterFunction = (text) => {
    const newData = this.arrayholder.filter(item => {
      const itemData = item.serial
      const textData = text;
      return itemData.indexOf(textData) > -1;
    });
    this.setState({ detail: newData });
  };

  sortTypeSelected(idx, value) {
    this.setState({ selectedSortType: value });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle.toUpperCase()
      }
    });
  }


  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }


  renderListItem = ({ item }) => (

    <View>
      <ElementItemWithIcon
        leftTxt={item.serial}
      />
    </View>
  );

  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (

        <ScrollView >
          <FlatList
            data={this.state.detail}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
            scrollEnabled={false}
            refreshing={true}
            ListEmptyComponent={
              <View style={{ marginHorizontal: '35%', marginVertical: '55%' }}>
                <Text style={{ color: Colors.black, fontWeight: 'bold', fontSize: 14 }}>
                  {this.state.locals.noserialfound}
                </Text>
              </View>
            }
          />
        </ScrollView>
      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          {this.props.navigator.showSnackbar({ text: this.state.api_error_message })}
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }
    return (

      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          displaySearch
          headerText={this.state.locals.screenTitle.toUpperCase()} />

        <SearchBar
          ref={(ref) => this.searchBar = ref}
          onChangeText={(text) => { this.searchFilterFunction(text); }}
          onPressClear={() => {
            this.searchBar.hide();
            this.setState({ searchBarHidden: true, detail: this.state.array });
          }
          }
        />

        {loadingIndicator}
        <View style={styles.topHeader}>
          <View style={styles.nameView}>
            <Text style={styles.nameText}>{this.props.discriptionToSend}</Text>
          </View>
          <Text>:</Text>
          <View style={styles.itemView}>
            <Text style={styles.itemText}>{this.props.valueToSend}</Text>
          </View>
        </View>
        {screenContent}
      </View>
    );
  }
}
const ElementItemWithIcon = ({ leftTxt }) => (
  <View style={styles.detailElimentContainer}>
    <View style={styles.containerMiddleSection2}>
      <View style={styles.elementLeft}>
        <Text style={styles.elementTxt}>
          {leftTxt}</Text>
      </View>
    </View>
  </View>
);



const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({


  container: {
    backgroundColor: colors.appBgColor,
    height: '100%'
  },
  containerMiddleSection2: {
    marginLeft: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },
  elementLeft: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    borderWidth: 0
  },

  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    color: Colors.primaryText,
  },

  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
  topHeader: {
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    paddingLeft: 20,
    marginTop: 10
  },
  nameView: {
    paddingRight: 5
  },
  itemView: {
    paddingLeft: 5
  },
  nameText: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: 'bold',
    color: Colors.primaryText,
  },
  itemText: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: 'bold',
    color: Colors.primaryText,
  },
  nodata: {
    fontWeight: 'bold',
    color: Colors.black,
    alignSelf: 'center',
    fontSize: 16,
  }
});

export default connect(mapStateToProps, actions)(LobListDetail);