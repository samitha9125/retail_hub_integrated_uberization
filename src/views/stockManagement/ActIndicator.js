import React from 'react';
import { ActivityIndicator, View, StyleSheet, Text } from 'react-native';
import Orientation from 'react-native-orientation'
import Colors from '../../config/colors';

class ActIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: true,
      locals: {
        validatingTxt: ''

      }
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentDidMount = () => this.closeActivityIndicator();
  componentWillUnmount(){
    Orientation.lockToPortrait()
  }
  closeActivityIndicator = () => this.setState({ animating: this.props.animating });

  render() {
    const animating = this.state.animating;
    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>{this.state.locals.validatingTxt}</Text>
        <ActivityIndicator
          animating={animating}
          color={Colors.activityIndicaterColor}
          size="large"
          style={styles.activityIndicator}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorTransparent,
    paddingVertical: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1000,
    opacity: 0.5
  },
  textStyle: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: "500",
    color: Colors.colorBlack,
    paddingVertical: 20,
    top: 100,
    bottom: 0,
    zIndex: 1000
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  }
});

export default ActIndicator;
