import React, { Component } from 'react';
import { View, Text } from 'react-native';

import Orientation from 'react-native-orientation';
import Analytics from '../../../utills/Analytics';
import CpeTrackerView from './CpeTrackerView';

class CpeTracker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DeliveryMain :: componentDidMount');
    Analytics.logEvent('cfss_cutomer_tracker');
  }

  render() {
    return (
      <CpeTrackerView {...this.props} />

    );
  }
}

export default CpeTracker;