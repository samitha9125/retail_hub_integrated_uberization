import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, BackHandler, Alert, NetInfo } from 'react-native';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import strings from '../../../Language/StockMangement';
import { Header } from '../../wom/Header';

const Utill = new Utills();
class CpeTrackerDetailPage extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      ArrayData:'',
      locals: {
        screenTitle: '',
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        customertracker: strings.customertracker,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };
  }

  componentWillMount() {
    console.log('DATA IS LOAD', this.props.data);
    let keys = Object.keys(this.props.data);
    let val = Object.values(this.props.data);
    let itemArr = keys.map((key, index) => {
      return { key: key, value: val[index] }

    });
    this.setState({
        ArrayData :itemArr
    })
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }
  
  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  renderListItem = ({ item }) => (
    <View>
      <View style={styles.detailElimentContainer}>
        <View style={styles.containerMiddleSection2}>
          <View style={styles.elementLeft}>
            <Text style={styles.elementTxt}>
              {item.key}</Text>
          </View>
        </View>
        <View style={styles.containerMiddleSection3}>
          <View style={styles.elementLeft}>
            <Text style={styles.elementTxt}>
              :</Text>
          </View>
        </View>
        <View style={styles.containerMiddleSection4}>
          <View style={styles.elementMiddle}>
            <Text style={styles.elementTxt}>
              {item.value}</Text>
          </View>
        </View>
      </View>
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.customertracker} />
        <FlatList style={styles.contentContainer}
          data={this.state.ArrayData}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => index}
          scrollEnabled={true}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CoustomerTracker ===> MainPage ');
  console.log('****** REDUX STATE :: CoustomerTracker ===> MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({

  container: {
    height: '100%',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: Colors.appBackgroundColor
  },
  contentContainer: {
    flex: 1.0,
    paddingTop: 10

  },
  containerMiddleSection2: {
    marginLeft: 20,
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  containerMiddleSection3: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  containerMiddleSection4: {
    paddingLeft: 10,
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginRight: 30
  },
  detailElimentContainer: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 20
  },

  elementLeft: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderWidth: 0
  },
  elementMiddle: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderWidth: 0,

  },

  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    color: Colors.primaryText,

  },

});

export default connect(mapStateToProps, actions)(CpeTrackerDetailPage);