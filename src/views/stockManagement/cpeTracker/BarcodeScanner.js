import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
  Alert,
  Keyboard
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import { Header } from '../../../views/wom/Header';
import strings from '../../../Language/Wom';
import ActIndicator from '../../home/ActIndicator';
import Utills from '../../../utills/Utills';
import Color from '../../../config/colors';


const { width } = Dimensions.get('window');
const maskColWidth = (width - 300) / 2;
const VIBRATION_DURATION = 200;
const Utill = new Utills();

class BarcodeScannerCpeTrackerView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      simBackMsg: strings.backMessage,
      isLoading: false,
      locals: {
        api_error_message: strings.api_error_message,
        network_error_message: strings.network_error_message,
        scannerBottomMessage: strings.scannerBottomMessage,
        scannerBottomMessage2: strings.scannerBottomMessage2,
        permission_to_use_camera: 'Permission to use camera',
        permission_to_use_camera_message: 'To Scan the serial, App need your permission to use your camera',
      },
      uniqueValue: 0,
      barcodeValue: null,
      continue: strings.continue,
    };
    this.camera = null;
    this.navigatorOb = this.props.navigator;
    this.scanCounter = 0
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  componentWillReceiveProps(nextProps) {
    console.log('Barcode Scripts :: componentWillReceiveProps', nextProps.simApiFail);
    // if (nextProps.simApiFail) {
    //   this.setState({ barcodeValue: '', scanSucess: false });

    // }

    // if (nextProps.sim_number == '') {
    //   nextProps.resetSimApiFail();
    // }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    Orientation.lockToPortrait()
  }

  onHandleBackButton = () => {
    this.goBack();
    return true;
  }

  onBarCodeReadCb = (e) => {

    console.log('xxx onBarCodeReadCb');
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e
      .data
      .toString();

    console.log(`xxx onBarCodeReadCb :${last8Digit}`);
    this.setState({ barcodeValue: last8Digit });
    console.log("barcodeState", this.state.barcodeValue);
    return;
  };

  onChangeText = (value) => {

    this.setState({ barcodeValue: value });
  };

  goBack = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  returnView() {
    console.log("ReceivedBarcode", this.state.barcodeValue);
    if (this.state.barcodeValue !== null) {
      Keyboard.dismiss();
      const navigatorOb = this.props.navigator;
      navigatorOb.resetTo({
        title: 'CUSTOMER TRACKER',
        screen: 'DialogRetailerApp.views.CpeTracker',
        passProps: {
          barcode: this.state.barcodeValue,
          individual: this.props.barcode,
          bundle: this.props.barcode,
          itemType: this.props.itemType
        }
      });
    }

  }

  handleClick() {
    console.log("===> Button Tapped \n ");
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    this.onHandleBackButton();
  }

  render() {
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.onHandleBackButton()}
          headerText={this.props.scannerHeader ? this.props.scannerHeader : 'SCAN SERIAL'} />
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.cameraView}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => this.onBarCodeReadCb(e)}
        >
        </RNCamera>
        <View style={styles.maskOutter}>
          <View style={{ flex: 1 }}>
            <View style={[{ flex: 2.1 }, styles.maskRow, styles.maskFrame]}
            />
            <View style={[{ flex: 0.75 }, styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]}
              />
            </View>
            <View style={[{ flex: 2.26 }, styles.maskRow, styles.maskFrame]} />
          </View>
        </View>
        <View style={styles.overlayView}>
          <View style={styles.overlayContainer} >
            <Text style={styles.titleText}>Scan {this.props.title}</Text>
            <View>
              <View style={styles.descContainer}>
                <Text style={styles.descText}>{this.state.locals.scannerBottomMessage}</Text>
                <Text style={styles.descText}>{this.state.locals.scannerBottomMessage2}</Text>
              </View>

              <View style={styles.bottomContainer}>
                <TextInput
                  style={styles.barcodeTxtInput}
                  underlineColorAndroid='transparent'
                  keyboardType={'numeric'}
                  value={this.state.barcodeValue}
                  placeholder='Enter serial'
                  autoFocus={false}
                  returnKeyType={'done'}
                  selectionColor={'yellow'}
                  onChangeText={(value) => this.setState({ barcodeValue: value })} />
                <TouchableOpacity
                  style={styles.barcodeContinueBtn}
                  onPress={() => this.returnView()}>
                  <Text style={styles.barcodeContinueBtnTxt}>
                    {this.state.continue}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {this.state.isLoading ?
          <ActIndicator isTextWhite /> : true}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  // const sim_api_fail = state.sim.sim_api_fail;
  const sim_validation = state.sim.sim_validation;
  // let simApiFail = false;

  return { Language, simApiFail: state.sim.sim_api_fail, sim_number: state.mobile.sim_number, sim_validation };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  containerBarcodeScan: {
    flex: 1
  },
  cameraView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  maskOutter: {
    position: 'absolute',
    top: 55,
    left: 0,
    bottom: 0,
    right: 0
  },
  maskInner: {
    width: 312,
    backgroundColor: Colors.colorTransparent,
    borderColor: '#FFC400', //#FFFFC400
    borderWidth: 3,
  },
  maskFrame: {
    backgroundColor: Colors.colorBlack,
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
  barcodeTxtInput: {
    fontSize: Styles.otpModalFontSize,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'left',
    flex: 0.7
  },
  barcodeContinueBtn: {
    height: 36,
    width: 108,
    flex: 0.3,
    borderRadius: 2,
    justifyContent: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: Styles.thumbnailTxtFontSize,
    fontWeight: 'bold',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  overlayView: { position: 'absolute', top: 55, left: 0, right: 0, bottom: 0 },
  overlayContainer: { flex: 1, justifyContent: 'space-between' },
  titleText: { fontSize: 16, lineHeight: 19, color: Colors.appBackgroundColor, marginTop: 105, textAlign: 'center' },
  descContainer: { marginBottom: 79, alignItems: 'center' },
  descText: { fontSize: 16, lineHeight: 19, color: Color.appBackgroundColor },
  bottomContainer: {
    height: 72, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 24, alignItems: 'center', backgroundColor: Colors.barcodeBottomBackColor
  }
});

export default connect(mapStateToProps, actions)(BarcodeScannerCpeTrackerView);
// export default BarcodeScannerCpeTrackerView; 