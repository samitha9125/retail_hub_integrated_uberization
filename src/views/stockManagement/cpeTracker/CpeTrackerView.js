import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, BackHandler, Dimensions, NetInfo, ScrollView, Keyboard } from 'react-native';
import Colors from '../../../config/colors';
import SectionContainer from './SectionContainer';
import * as UtilsCfss from '../../../utills/UtilsCfss';
import Utills from '../../../utills/Utills';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import ActIndicator from './ActIndicator';
import strings from '../../../Language/StockMangement';
import { Header } from '../../wom/Header';
import SerialListComponent from '../../wom/components/SerialListComponent';
import DropDownInput from '../../wom/components/DropDownInput';
import TextFieldComponent from '../../wom/components/TextFieldComponent';

const Utill = new Utills();
const { height } = Dimensions.get('window')
class CpeTrackerView extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      connHolder: '',
      cirNumber: '',
      invoiceNumber: '',
      accountNumber: '',
      barcode: '',
      individualSerial: [],
      connectionNumber: [],
      BundelSerial: [],
      CIR: [],
      Account: [],
      Mobile: [],
      invoice: [],
      nicPass: '',
      nic: '',
      Pass: '',
      PassPortnum: '',
      apiLoading: false,
      isContinueBtnDisabled: true,
      refreshIndividualData: false,
      refreshBundleData: false,
      Response: [],
      barcodebundle: '',
      requestTypeValue: [],
      requestTypeIndexs: [],
      individialTypeIndexs: [],
      individialTypeValue: [],
      selectedRequestType: '',
      selectedRequestTypeID: '',
      selectedindividialType: '',
      selectedindividialTypeID: '',
      requestType: [
        {
          value: 'Account number',
          id: 'ACCOUNT_NO'
        },
        {
          value: 'NIC no',
          id: 'ID'
        },
        {
          value: 'Passport no',
          id: 'PP'
        },
        {
          value: 'Invoice',
          id: 'INVOICE'
        },
        {
          value: 'CIR',
          id: 'CIR'
        },
        {
          value: 'Individual serial',
          id: 'INDIVIDUAL_SERIAL'
        },
        {
          value: 'Bundle serial',
          id: 'BUNDLE_SERIAL'
        }
      ],

      individualType: [
        {
          value: 'STB',
          id: 'STB'
        },
        {
          value: 'RCU',
          id: 'RCU'
        },
        {
          value: 'SIM',
          id: 'SIM'
        },
        {
          value: 'LNB',
          id: 'LNB'
        },
        {
          value: 'POWER SUPPLY',
          id: 'POWER SUPPLY'
        }
      ],

      locals: {
        screenTitle: strings.customertracker,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        customertracker: strings.customertracker,
        track: strings.track,
        accountNumber: strings.accountNumber,
        nicNo: strings.nicNo,
        passportNo: strings.passportNo,
        invoice: strings.invoice,
        cir: strings.cir,
        individualSerial: strings.individualSerial,
        bundelSerial: strings.bundelSerial,
        invalidAccountnumber: strings.invalidAccountnumber,
        invalidCir: strings.invalidCir,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.format_RequestItem_Dropdown();
    this.format_individialItem_Dropdown();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  format_RequestItem_Dropdown() {
    let requestTypeValue = [];
    let requestTypeIndexs = [];

    this.state.requestType.map((item, key) => {
      requestTypeValue.push(item.value);
      requestTypeIndexs.push(item.id);
    });

    this.setState({ requestTypeValue, requestTypeIndexs });
  }

  async select_RequestItem(item) {
    await this.setState({
      selectedRequestType: this.state.requestTypeValue[item],
      selectedRequestTypeID: this.state.requestTypeIndexs[item],
      refreshIndividualData: true,
      refreshBundleData: true,
      connHolder: '',
      connectionNumber: []
    }, () => {
      switch (this.state.selectedRequestType) {
        case 'CIR': this.cir_text_field.clearFieldValue(); break
        case 'Invoice': this.invoice_text_field.clearFieldValue(); break
        case 'Account number': this.account_text_field.clearFieldValue(); break
        case 'NIC no': this.nic_text_field.clearFieldValue(); break
        case 'Passport no': this.passport_text_field.clearFieldValue(); break
        default: true
      }
    });
  }

  format_individialItem_Dropdown() {
    let individialTypeValue = [];
    let individialTypeIndexs = [];

    this.state.individualType.map((item, key) => {
      individialTypeValue.push(item.value);
      individialTypeIndexs.push(item.id);
    });

    this.setState({ individialTypeValue, individialTypeIndexs });
  }

  async select_individialItem(item) {
    await this.setState({
      selectedindividialType: this.state.individialTypeValue[item],
      selectedindividialTypeID: this.state.individialTypeIndexs[item],
      refreshIndividualData: true
    }, () => {
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  loadIndviualScanner() {
    return (
      <View>
        <SerialListComponent
          isReadOnly={true}
          refreshData={this.state.refreshIndividualData}
          manualLable={this.state.locals.lblNewSTBSerial}
          isAllSerialsValidated={this.newIndivialScannerResponse}
          serialData={{ name: this.state.selectedindividialType, type: this.state.selectedindividialType }}
          navigatorOb={this.props.navigator}
        />
      </View>
    )
  }
  loadBundleScanner() {
    return (
      <View>
        <SerialListComponent
          isReadOnly={true}
          refreshData={this.state.refreshBundleData}
          manualLable={this.state.locals.lblNewSTBSerial}
          isAllSerialsValidated={this.newBundleScannerResponse}
          serialData={{ name: this.state.selectedRequestType, type: this.state.selectedRequestType }}
          navigatorOb={this.props.navigator}
        />
      </View>
    )
  }

  //////////////////// individual serial api call ///////////////////////////////
  getindividualserial = () => {
    const data = {
      selectedInp: this.state.selectedRequestTypeID,
      itemType: this.state.selectedindividialType || this.props.itemType,
      serial: this.state.barcode
    };
    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getindividualserialSuccess, this.getindividualserialFailure, this.getindividualserialEx);
    }
    this.setState({ apiLoading: true }, callback)
  }

  getindividualserialEx = (error) => {
    console.log('getindividualserialEx', error);
    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getindividualserial);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }
    this.setState({ apiLoading: false }, callback)
  }

  getindividualserialFailure = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }

  getindividualserialSuccess = (response) => {
    console.log('xxx getNicSuccessco', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Customer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.IndividualSerial
        }
      });
      navigatorOb.pop();
    }
    this.setState({ IndividualSerial: response.data.data, apiLoading: false }, callback);
  }
  /////////////////////individual serial api call end//////////////////////
  /////////////////////////bundel serial api call strat///////////////
  getbundleserial = () => {
    const data = {
      selectedInp: this.state.selectedRequestTypeID,
      cpe_bundle_serial: this.state.barcodebundle
    };
    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getbundleserialSuccess, this.getbundleserialFailure, this.getbundleserialEx);
    }
    this.setState({ apiLoading: true }, callback)
  }

  getbundleserialEx = (error) => {
    console.log('getindividualserialEx', error);
    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getbundleserial);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }
    this.setState({ apiLoading: false }, callback)
  }

  getbundleserialFailure = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }


  getbundleserialSuccess = (response) => {
    console.log('xxx getbundleserialSuccess', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Customer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.BundelSerial
        }
      });
      navigatorOb.pop();
    }

    this.setState({ BundelSerial: response.data.data, apiLoading: false }, callback);
  }

  ///////////////////////////bundel serial api call end///////////////////////////////////////////////

  ///// api call cir //////////
  getCir = () => {
    const data = {
      cir: this.state.cirNumber,
      selectedInp: this.state.selectedRequestTypeID
    };
    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getcirSuccessco, this.getCirFailureco, this.getCirExco);

    }
    this.setState({ apiLoading: true }, callback)
  }
  getCirExco = (error) => {
    console.log('getindividualserialEx', error);
    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getCir);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }

    this.setState({ api_error: false }, callback);
  }

  getCirFailureco = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }


  getcirSuccessco = (response) => {
    console.log('xxx getNicSuccessco', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Customer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.CIR
        }
      });
      navigatorOb.pop();
    }
    this.setState({ CIR: response.data.data, apiLoading: false }, callback);
  }

  ////  api call cir end ////
  ///// api call cir //////////

  getInvoice = () => {
    const data = {
      cpe_invoice: this.state.invoiceNumber,
      selectedInp: this.state.selectedRequestTypeID
    };
    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getInvoiceSuccessco, this.getInvoiceFailureco, this.getInvoiceExco);
    }
    this.setState({ apiLoading: true }, callback)
  }
  getInvoiceExco = (error) => {
    console.log('getindividualserialEx', error);
    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getInvoice);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }
    this.setState({ apiLoading: false }, callback)
  }

  getInvoiceFailureco = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }

  getInnvoiceSuccessco = (response) => {
    console.log('xxx getNicSuccessco', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Coustmer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.invoice
        }
      });
      navigatorOb.pop();

    }
    this.setState({ invoice: response.data.data, apiLoading: false }, callback);
  }

  ////  api call cir end ////
  ///// api call Account Number //////////
  getAccountNumber() {
    const data = {
      account_no: this.state.accountNumber,
      selectedInp: this.state.selectedRequestTypeID
    };
    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getAccountNumberSuccessco, this.getAccountNumberFailureco, this.getAccountNumberExco);
    }

    this.setState({ apiLoading: true }, callback)
  }

  getAccountNumberExco = (error) => {
    console.log('getindividualserialEx', error);

    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getInvoice);
      } else if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }

    this.setState({ apiLoading: false }, callback)
  }

  getAccountNumberFailureco = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }

  getAccountNumberSuccessco = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Coustmer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.Account
        }
      });
      navigatorOb.pop();
    }

    this.setState({ Account: response.data.data, apiLoading: false }, callback);
  }

  getConnectionnumberNicPassEx = (error) => {
    console.log('getindividualserialEx', error);

    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getConnectionnumberNicPass);
      }
      if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }
    this.setState({ apiLoading: false }, callback)
  }

  getConnectionnumberNicPassFailure = (response) => {
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error ? response.data.error : response.data.info ? response.data.info : response.data.data.error ? response.data.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    }
    this.setState({ apiLoading: false }, callback)
  }

  getConnectionnumberNicPassSuccess = (response) => {
    this.setState({
      connectionNumber: response.data.data,
      connHolder: '',
      apiLoading: false
    });
    this.dissmissNoNetworkModal();
  }

  getNicdetail() {
    const data = {
      account_num: this.state.connHolder,
      selectedInp: this.state.selectedRequestTypeID,
      cus_id: this.state.nicPass

    };
    let callback = () => {
      Utill.apiRequestPost('AccOverNic', 'cpeTracker', 'cfss', data,
        this.getNicdetailSuccessco, this.getNicdetailFailureco, this.getNicdetailExco);
    }
    this.setState({ apiLoading: true }, callback)
  }

  getNicdetailExco = (error) => {
    console.log('getindividualserialEx', error);

    let callback = () => {
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getNicdetail);
      }
      if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }

    this.setState({ apiLoading: false }, callback)
  }

  getNicdetailSuccessco = (response) => {
    console.log('xxx getNicSuccessco', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Coustmer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.Account
        }
      });
      navigatorOb.pop();
    }
    this.setState({ Account: response.data.data, apiLoading: false }, callback);
  }

  /// pass port///
  getPassdetail() {
    const data = {
      account_num: this.state.connHolder,
      selectedInp: this.state.selectedRequestTypeID,
      cus_id: this.state.nicPass
    };

    let callback = () => {
      Utill.apiRequestPost('AccOverNic', 'cpeTracker', 'cfss', data,
        this.getPassdetailSuccessco, this.getNicdetailFailureco, this.getPassdetailExco);

    }
    this.setState({ apiLoading: true }, callback)
  }

  getPassdetailExco = (error) => {
    console.log('getindividualserialEx', error);

    let callback = () => {
      if (error == 'Network  Error') {
        error = this.state.locals.network_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
      if (error == 'No_Network') {
        error = this.state.locals.network_error_message;
        this.showNoNetworkModal(this.getPassdetail);
      }
      else {
        error = this.state.locals.api_error_message;
        this.props.navigator.showSnackbar({ text: error });
      }
    }
    this.setState({ apiLoading: false }, callback)
  }

  getPassdetailSuccessco = (response) => {
    console.log('xxx getPassSuccessco', response);
    let callback = () => {
      this.dissmissNoNetworkModal();
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'Coustmer Tracker Detail',
        screen: 'DialogRetailerApp.views.CpeTrackerDetailView',
        passProps: {
          data: this.state.Account
        }
      });
      navigatorOb.pop();
    }
    this.setState({ PassPortnum: response.data.data, apiLoading: false }, callback);
  }

  nic = () => {
    let callback = () => {
      this.props.navigator.showModal({
        screen: 'DialogRetailerApp.models.GeneralModelApprovals',
        title: 'GeneralModalException',
        passProps: {
          stoid: 'NIC /Passpoart' + '' + this.state.nicNumber,
          message: 'No Data Found:'

        },
        overrideBackPress: true
      });
    }
    this.setState({ apiLoading: false }, callback);
  }

  gotoScanner() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'Bundle Serial',
      screen: 'DialogRetailerApp.views.BarcodeScannerCpeTrackerScreen',
      passProps: {
        barcode: this.state.selectedRequestTypeID,
        scannerHeader: this.state.locals.customertracker
      }
    });
    navigatorOb.pop();
  }

  gotoIndividualScanner() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'Individual Serial',
      screen: 'DialogRetailerApp.views.BarcodeScannerCpeTrackerScreen',
      passProps: {
        barcode: this.state.selectedRequestTypeID,
        itemType: this.state.selectedindividialType,
        scannerHeader: this.state.locals.customertracker
      }
    });
    navigatorOb.pop();
  }

  checkNICValidation = (text) => {
    console.log(UtilsCfss.validateNIC(text));
    if (UtilsCfss.validateNIC(text) !== false) {
      this.validateNumber('NIC', text)
    } else {
      this.setState({ connectionNumber: '', connHolder: '' })
    }

  }
  newIndivialScannerResponse = (isValidated, res) => {
    console.log('RESPONSE', res)
    this.setState({ isContinueBtnDisabled: false, barcode: res.serial, refreshIndividualData: false })
  }

  newBundleScannerResponse = (isValidated, res) => {
    console.log('RESPONSE', res)
    this.setState({
      isContinueBtnDisabled: false, barcodebundle: res.serial, refreshBundleData: false
    })
  }

  validateNumber(type, number) {
    const data = {
      P_ID_Type: type,
      P_ID_Number: number,
      selectedInp: this.state.selectedRequestTypeID,
    };

    let callback = () => {
      Utill.apiRequestPost('cpeTrackerCommon', 'cpeTracker', 'cfss', data,
        this.getConnectionnumberNicPassSuccess, this.getConnectionnumberNicPassFailure, this.getConnectionnumberNicPassEx);
    }

    Keyboard.dismiss()
    this.setState({ apiLoading: true }, callback)
  }

  selectConnectionNumber(index) {
    this.setState({ connHolder: this.state.connectionNumber[index] })
  }

  render() {
    let pickerContent;
    if (this.state.selectedRequestType === 'Account number') {
      pickerContent = (
        <View>
          <View style={{ marginHorizontal: 30 }}>
            <TextFieldComponent
              ref={ref => this.account_text_field = ref}
              label={this.state.selectedRequestType}
              title={''}
              keyboardType={'numeric'}
              onChangeText={async AccountNumber =>
                await this.setState({ accountNumber: AccountNumber })
              }
              error={(this.state.accountNumber !== '' && UtilsCfss.dtvNumValidate(this.state.accountNumber) === false && UtilsCfss.lteNumValidate(this.state.accountNumber) === false && UtilsCfss.accountNumFormatValidate(this.state.accountNumber) === false) ? this.state.locals.invalidAccountnumber : ''}
            />
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.accountNumber !== ' ' && UtilsCfss.dtvNumValidate(this.state.accountNumber) === false) ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.accountNumber !== ' ' && UtilsCfss.dtvNumValidate(this.state.accountNumber) === false)}
              onPress={() => this.getAccountNumber()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
    } else if (this.state.selectedRequestType === 'NIC no') {
      pickerContent = (
        <View>
          <View style={{ marginHorizontal: 30 }}>
            <TextFieldComponent
              ref={ref => this.nic_text_field = ref}
              label={this.state.selectedRequestType}
              title={''}
              onChangeText={this.checkNICValidation}
            />
          </View>
          {this.state.connectionNumber != '' ?
            <View style={{ marginHorizontal: 30 }}>
              <SectionContainer >
                <DropDownInput
                  alignDropDownnormal={true}
                  dropDownTitle={'Connection no'}
                  selectedValue={this.state.connHolder}
                  dropDownData={this.state.connectionNumber}
                  onSelect={index => this.selectConnectionNumber(index)}
                />
              </SectionContainer>
            </View> : true}
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.connHolder == '') ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.connHolder == '')}
              onPress={() => this.getNicdetail()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    else if (this.state.selectedRequestType === 'Passport no') {
      pickerContent = (
        <View style={styles.inputsection}>
          <View style={{ marginHorizontal: 30 }}>
            <TextFieldComponent
              ref={ref => this.passport_text_field = ref}
              label={this.state.selectedRequestType}
              title={''}
              onBlur={() => this.validateNumber('PASSPORT', this.state.Pass)}
              onSubmitEditing={() => this.validateNumber('PASSPORT', this.state.Pass)}
              onChangeText={async Port =>
                await this.setState({ Pass: Port })
              }
            />
          </View>
          {this.state.connectionNumber != '' ?
            <View style={{ marginHorizontal: 30 }}>
              <SectionContainer >
                <DropDownInput
                  alignDropDownnormal={true}
                  dropDownTitle={'Connection no'}
                  selectedValue={this.state.connHolder}
                  dropDownData={this.state.connectionNumber}
                  onSelect={index => this.selectConnectionNumber(index)}
                />
              </SectionContainer>
            </View>
            :
            true}
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.connHolder == '') ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.connHolder == '')}
              onPress={() => this.getNicdetail()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    /* hide mobile number */
    else if (this.state.selectedRequestType === 'Invoice') {
      pickerContent = (
        <View style={styles.inputsection}>
          <View style={{ marginHorizontal: 30 }}>
            <TextFieldComponent
              ref={ref => this.invoice_text_field = ref}
              label={this.state.selectedRequestType}
              title={''}
              onChangeText={async invoice =>
                await this.setState({ invoiceNumber: invoice })
              }
            />
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.invoiceNumber == '') ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.invoiceNumber == '')}
              onPress={() => this.getInvoice()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
    }
    else if (this.state.selectedRequestType === 'CIR') {
      pickerContent = (
        <View style={styles.inputsection}>
          <View style={{ marginHorizontal: 30 }}>
            <TextFieldComponent
              ref={ref => this.cir_text_field = ref}
              label={this.state.selectedRequestType}
              title={''}
              onChangeText={async CIR =>
                await this.setState({ cirNumber: CIR })
              }
            />
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={(this.state.cirNumber == '') ? styles.buttonContainereDisable : styles.buttonContainer}
              disabled={(this.state.cirNumber == '')}
              onPress={() => this.getCir()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
    }
    else if (this.state.selectedRequestType === 'Individual serial') {

      pickerContent = (
        <View>
          <View style={{ marginHorizontal: 30, flex: 1 }}>
            <DropDownInput
              alignDropDownnormal={true}
              dropDownTitle={'Item'}
              selectedValue={this.state.selectedindividialType}
              dropDownData={this.state.individialTypeValue}
              onSelect={item => this.select_individialItem(item)}
            />
          </View>
          <View style={{ marginHorizontal: 30 }}>
            {this.loadIndviualScanner()}
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              style={[styles.buttonContainer, this.state.isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
              disabled={this.state.isContinueBtnDisabled}
              onPress={() => this.getindividualserial()}>
              <Text style={styles.acceptbtn}>{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    else if (this.state.selectedRequestType === 'Bundle serial') {
      pickerContent = (
        <View>
          <View style={{ marginHorizontal: 30 }}>
            {this.loadBundleScanner()}
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity style={styles.buttonContainer}
              disabled={this.state.isContinueBtnDisabled}
              style={[styles.buttonContainer, this.state.isContinueBtnDisabled ? { backgroundColor: Colors.btnDeactive } : { backgroundColor: Colors.btnActive }]}
              onPress={() => this.getbundleserial()}
            >
              <Text style={[styles.barcodeContinueBtnTxt, this.state.isContinueBtnDisabled ? { color: Colors.btnDeactiveTxtColor } : { color: Colors.btnActiveTxtColor }]} >{this.state.locals.track}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText='CUSTOMER TRACKER' />
        {
          this.state.apiLoading ?
            <View style={styles.indiView}>
              <ActIndicator animating={true} />
            </View>
            :
            true
        }
        <ScrollView >
          <View style={{ marginHorizontal: 30, flex: 1 }}>
            <DropDownInput
              alignDropDownnormal={true}
              dropDownTitle={'Type'}
              selectedValue={this.state.selectedRequestType}
              dropDownData={this.state.requestTypeValue}
              onSelect={item => this.select_RequestItem(item)}
            />
          </View>
          <View style={{ flex: 1, paddingBottom: 10 }}>
            {pickerContent}
          </View>
        </ScrollView>
      </View >
    );
  }
}
const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  buttonContainer: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnActive,
  },
  buttonContainereDisable: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: Colors.btnDeactive,
  },
  acceptbtn: {
    color: Colors.primaryText,
  },
  viewButton: {
    marginTop: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 20
  },
  containerview: {
    marginHorizontal: 10,
    paddingRight: 0
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
});

export default connect(mapStateToProps, actions)(CpeTrackerView);