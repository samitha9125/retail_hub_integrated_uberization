import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Dimensions, Image } from 'react-native';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import Constants from '../../../config/constants';
class StockRequestApproval extends Component {
  gotoStockRequestApprovalDetails() {
    console.log("::GOTO DETAILS");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK REQUEST APPROVAL',
      screen: 'DialogRetailerApp.views.StockApprovalDetails',
      passProps: {
      }
    });
    navigatorOb.pop();
  }
    renderListItem = ({ item }) => (
      <View style={styles.card}>
        {/* CARD TOP SECTION */}
        <View style={styles.cardTop}>
          {/* SIM NUMBER AND PRIORITY LABEL START  */}
          <View style={styles.cardTopSection1}>
            <View style={styles.stockNumberView}>
              
              <View style={styles.stockNumber}>
                <Text style={styles.stockId}>{item.key1}</Text>
                <View style = {styles.imageReqest}>
                  <Image
                    source={require('../../../../images/delivery/dispatched.png')}
                    style={styles.imageContentPhone}
                  />
                </View>
              </View>
            </View>
          </View>
          {/* SIM NUMBER AND PRIORITY LABEL END */}
          {/* DELIVERY TRUCK AND LABEL START */}
          <View style={styles.cardTopSection2}>
          </View>
          <View style={styles.cardTopSection3}>
            {/* <View style={styles.bottomSpaceView} /> */}
          </View>
        </View>
        {/* CARD BODY */}
        <View style={styles.cardBody}>
          <View style={styles.orderDetails}>
            <View style={styles.orderDetail1}>
              <Text style={styles.txtOrderDetailLabel}> Requested Date</Text>
              <Text style={styles.txtOrderDetailValue}> {item.key2} </Text>
            </View>
            <View style={styles.orderDetail}>
              <Text style={styles.txtOrderDetailLabel}> Requested by </Text>
              <Text style={styles.txtOrderDetailValue}> {item.key3} </Text>
            </View>
            
           
          </View>
        </View>
        {/* CARD BODY END */}
        {/* CARD BOTTOM */}
        <View style={styles.cardBottom}>
          <View style={styles.cardBottomSection1}>
            <TouchableOpacity onPress={() => this.gotoStockRequestApprovalDetails()}>
              <Text style={styles.txtBottom}>
                            DETAILS
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.cardBottomSection2}>
            <TouchableOpacity>
              <Text style={styles.txtBottom}>
                            REJECT
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.cardBottomSection3}>
            <TouchableOpacity>
              <Text style={styles.txtBottomAcceptStock}>
                            APPROVE
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* CARD BOTTM END */}
      </View>
    )
    render() {
      console.log('Stock Acceptance List Printed');
     
      return (
        <View style={styles.mainContainer}>
          {/* HEADER SECTION START */}
          {/* <Header
              backButtonPressed={() => this.handleBackButtonClick()}
              searchButtonPressed={() => this.handleSearch()}
              filterButtonPressed={() => this.handleFilter()}
              displaySearch = {workOrderRetrieveStatus}
              displayFilter = {workOrderRetrieveStatus}
              headerText={screenTitle}/> */}
          {/* HEADER SECTION END */}
          <FlatList
            data={[{ key1: '6567', key2: '2018-01-01', key3: '728 - Ravi Perera' },
              { key1: '6568', key2: '2018-01-01 03:01', key3: '2158 - Nalin Ranasinghe' },
              { key1: '6569', key2: '2018-01-01 03:15', key3: '2365 - Kishan Bandu' },
              { key1: '6570', key2: '2018-01-01 04:25', key3: '201- Ravi Karunathneh' }]}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index}
            scrollEnabled={true}
          />
        </View>
      );
    }
}
const styles = StyleSheet.create({
 
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: "center",
    alignItems: "center",
    alignSelf: 'center'
  },
  card: {
    flex: 1,
    backgroundColor: Colors.white,
    height: Dimensions
      .get('window')
      .height / 3,
    margin: 5,
    marginVertical: 10,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
    width: Dimensions.get('window').width - 10
    
  },
  cardTop: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
    
    
  },
  cardBody: {
    flex: 1.9,
    flexDirection: 'column',
    
  },
  cardBottom: {
    flex: 0.7,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: Colors.lightGrey
  },
  stockId: {
    fontSize: 18,
    color: Colors.black,
   
  },
  txtOrderDetailLabel: {
    flex: 1,
    color: Colors.black,
  },
  txtOrderDetailValue: {
    flex: 1,
    color: Colors.black, 
  },
  cardTopSection1: {
    flex: 1,
    flexDirection: "column",
    
  },
  cardTopSection2: {
    flex: 1.1,
   
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  cardTopSection3: {
    flex: 1.5,
    flexDirection: "column",
    // backgroundColor: "lightyellow",
    justifyContent: "center",
    alignItems: "center"
  },
  cardBottomSection1: {
    flex: 3,
    
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    padding: 15
    // backgroundColor: "blue"
  },
  cardBottomSection2: {
    flex: 1,
    flexDirection: "column",
    // backgroundColor: "lightskyblue",
    justifyContent: "center",
    alignItems: "center"
  },
  cardBottomSection3: {
    flex: 2,
    flexDirection: "column",
    // backgroundColor: "lightgreen",
    justifyContent: "center",
    alignItems: "center"
  },
  stockNumberView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "mediumspringgreen"
  },
  stockNumber: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft:14
  },
 
  orderDetail: {
    flex: 1,
    marginLeft: 10,
    marginBottom: 0,
    marginTop: 0,
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  orderDetail1:{
    flex: 1,
    marginLeft: 10,
    alignItems: 'center',
    flexDirection: 'row'
  },
  orderDetails: {
    flex: 1.6,
    flexDirection: 'column'
  },
 
  imageContentPhone: {
    flex: 1,
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginBottom: 5
  },
  txtBottom: {
    fontWeight: 'bold',
    color: Colors.black
  }, txtBottomAcceptStock: {
    fontWeight: 'bold',
    color: Colors.colorRed
  },
  imageReqest:{
    marginLeft: 20
  }
});
export default StockRequestApproval;