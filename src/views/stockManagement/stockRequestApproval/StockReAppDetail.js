import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet,TouchableOpacity, TextInput } from 'react-native';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
class StockApprovalDetails extends Component {
  state = {
    email: '',
  }

    renderListItem = ({ item }) => (
      <View>
        <ElementItemWithIcon
          leftTxt={item.key1}
          middleTxt={item.key4}
          rightTxt={item.key5} />
      </View>
    );

    render() {
      return (
        <View style={styles.container}>
          <View style={styles.containerTop}>
            <View style={styles.containerTopSection1}>
            </View>
            <View style={styles.containerTopSection2}>
              <Text style={styles.containerTopText}>Requested qty</Text>
            </View>
            <View style={styles.containerTopSection3}>
              <Text style={styles.containerTopText}>Approved qty</Text>
            </View>
          </View>
          <View style={styles.containerMiddle}>
            <FlatList
              data={[{ key1: 'Stock 1', key4: '125', key5: '112' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' },
                { key1: 'Stock 1', key4: '125', key5: '125' }]}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index}
              scrollEnabled={true}
            />
          </View>
          <View style={styles.containerBottom}>
            <View style={styles.containerBottomSection1}>
              
            </View> 
            <View style={styles.containerBottomSection2}>
              <TouchableOpacity  style={styles.buttonCancelContainer}>
                <Text style={styles.acceptbtn}>REJECT</Text>
              </TouchableOpacity>

              <TouchableOpacity  style={styles.buttonContainer}>
                <Text style={styles.acceptbtn}>APPROVE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
}

const ElementItemWithIcon = ({ leftTxt, rightTxt, middleTxt }) => (
  <View style={styles.detailElimentContainer}>
    <View style={styles.containerMiddleSection2}>
      <View style={styles.elementLeft}>
        <Text style={styles.elementTxt}>
          {leftTxt}</Text>
      </View>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
                :</Text>
    </View>
    <View style={styles.containerMiddleSection3}>
      <View style={styles.elementLeft}>
        <Text style={styles.elementTxt}>
          {middleTxt}</Text>
      </View>
    </View>
    <View style={styles.containerMiddleSection4}>
      <View style={styles.elementMiddle}>
        <TextInput style = {styles.input}
          underlineColorAndroid = "transparent"
          keyboardType = 'numeric'
          autoCapitalize = "none"
          onChangeText = {this.handleEmail}>{rightTxt}</TextInput>
      </View>
    </View>
  </View>
);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerTop: {
    flex: 0.1,
    flexDirection: 'row'
  }, 
  containerTopText: {
    fontWeight: 'bold',
    color:Colors.primaryText,
    width: '100%',
    fontSize:  Styles.delivery.defaultFontSize,
    textAlign: 'center',
   
    justifyContent: 'center',
  }, 
  containerTopSection1: {
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'center'
  }, 
  containerTopSection2: {
    flex: 0.3,
    paddingLeft:10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerTopSection3: {
    flex: 0.3,
    marginLeft: 20,
    marginRight:10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddle: {
    flex: 0.75,
    flexDirection: 'row'
  },
  containerMiddleSection2: {
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, 
  containerMiddleSection3: {
    flex: 0.2,
   
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  }, 
  containerMiddleSection4: {
    paddingLeft: -12,
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginRight: 30
  },
  containerBottom: {
    flex: 0.15,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor:Colors.borderLineColorLightGray,
  }, 
  containerBottomSection1: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  containerBottomSection2: {
    marginRight:10,
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
   
  },

  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'

  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    alignSelf: 'flex-start'
  },

 

  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 20
  },

  elementLeft: {
    flex: 1,

    alignItems: 'flex-end',
    justifyContent:'center',
    borderWidth: 0
  },
  elementMiddle: {
    alignItems: 'flex-end',
    justifyContent:'center',
    borderWidth: 0,
    
  },

  elementTxt: {
    fontSize: 12,
    color:Colors.primaryText,
  
  },
  input: {
    textAlign: 'center',
    height: 40,
    width:70,
    fontSize: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.primaryText,
    borderWidth: 1
  },
  acceptbtn:{
    color:Colors.primaryText,
  }
});
export default StockApprovalDetails;