import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import strings from '../../Language/StockMangement';
import { Header } from '../wom/Header';
import img1 from '../../../images/stock_availabilty/stockAcceptance.png';
import img2 from '../../../images/stock_availabilty/stockavalabilty.png';

const Utill = new Utills();
class StockManagement extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      workOrderCount: [],
      apiLoading: true,
      api_error: false,
      locals: {
        screenTitle: strings.title,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        titleStockAvailabilty: strings.titleStockAvailabilty,
        connected: strings.connected,
        disconected: strings.disconected,
        network_conected: strings.network_conected,
        titleStockAcceptance: strings.titleStockAcceptance
      }
      
    };
    this.navigatorOb = this.props.navigator;
  }

  /////////////////////////////////

  gotoStockAvailabilty() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK AVAILABILTY',
      screen: 'DialogRetailerApp.views.StockAvailability',
      passProps: {
      }
    });
    navigatorOb.pop();
  }

  gotoStockRequestAcceptance() {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.StockAcceptance',
      passProps: {
        screenTitle: this.state.locals.titleStockAcceptance
      }
    });
    navigatorOb.pop();
  }

  /////////////////////////////////////

  setIndicater = (loadingState = true) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });

  }

  getCount = () => {
    const data = {

    };

    // Utill.apiRequestPost('getStockManagementHeadingCounts', 'stock', 'dtv', data, this.getCountSuccessCb, this.getCountFailureCb, this.getCountExcb);
    Utill.apiRequestPost('GetSTOInfo', 'GoodsAcceptance', 'cfss', data, this.getCountSuccessCb, this.getCountFailureCb, this.getCountExcb);

  }
  /// - -- 
  getCountExcb = (error) => {
    console.log('getCountExcb', error);
    if (error == 'Network Error') {
      error = this.state.locals.network_error_message;
      this.props.navigator.showSnackbar({ text: error });
    } else {
      error = this.state.locals.api_error_message;
      this.props.navigator.showSnackbar({ text: error });
    }
    this.setState({ api_error_message: error });
    setTimeout(() => {
      this.setIndicater(false);
      this.setState({ api_error: true });
    }, 1000);
  }

  getCountFailureCb = (response) => {
    this.setState({ api_error_message: response.data.error, api_error: true, apiLoading: false }, () => {
      this.props.navigator.showSnackbar({ text: response.data.error });
    });
  }

  getCountSuccessCb = (response) => {
    console.log('getCountSuccessCb');
    console.log('xxx getCountSuccessCb', response.data.data);
    let apiData = response.data.data.sto_count;
    let workOrderCount = [
      {
        key: 0,
        imageUri: img1,
        value: this.state.locals.titleStockAcceptance,
        count: apiData
      }, {
        key: 1,
        imageUri: img2,
        value: this.state.locals.titleStockAvailabilty,
      },
    ];
    this.setIndicater(false);

    let totalWoCount = {
      PENDING: apiData

    };
    this
      .props
      .setWorkOrderCounts(totalWoCount);
    this.setState({ workOrderCount: workOrderCount });
  }

  componentWillMount() {
    this.getCount();
  }


  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setIndicater(false);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  handleBackButtonClick = () => {
    this.okHandler()
    return true;
  }

  okHandler = () => {
    this.navigatorOb.pop({ animated: true, animationType: 'fade' });
    return true;
  }

  renderListItem = ({ item, index }) => (
    <View style={styles.container}>
      <TouchableOpacity
        key={index}
        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.gotoStockRequestAcceptance(index)}>
        <View style={styles.leftImageContainer}>
          <Image source={item.imageUri} style={styles.orderImage} resizeMode="cover" />
        </View>
        <View style={styles.middleTextContainer}>
          <Text style={styles.innerText}>{item.value}
          </Text>
        </View>
        <View style={styles.rightImageContainer}>
          {item.count !== 0
            ? <View style={styles.orderCountContainer}>
              <Text style={styles.orderCountText}>
                {item.count}
              </Text>
            </View>
            : <View />
          }
        </View>
      </TouchableOpacity>

      <TouchableOpacity

        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.gotoStockAvailabilty()}
      >
        <View style={styles.leftImageContainer}>
          <Image source={require('../../../images/stock_availabilty/stockavalabilty.png')} style={styles.orderImage} resizeMode="cover" />
        </View>
        <View style={styles.middleTextContainer}>
          <Text style={styles.innerText}>{this.state.locals.titleStockAvailabilty}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  render() {
    console.log(this.state.workOrderCount);
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (<FlatList
        data={this.state.workOrderCount}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
        scrollEnabled={false}
        refreshing={false}
        onRefresh={this.getCount} />

      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          {/* <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text> */}
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockManagment ===> MainPage ');
  console.log('****** REDUX STATE :: StockManagment => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: Colors.appBackgroundColor
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 6,
    backgroundColor: Colors.colorYellow,
    alignItems: 'center'
  },
  leftImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 5
  },

  orderImage: {
    alignSelf: 'center',
    width: undefined,
    height: undefined,
    margin: 5,
    padding: 25,
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent

  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  rightImageContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  orderCountContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    padding: 12,
    height: 56,
    width: 56,
    borderRadius: 28,
    backgroundColor: Colors.transparent
  },



  orderCountText: {
    textAlign: 'center',
    fontSize: Styles.delivery.defaultFontSize,
    color: Colors.colorBlack
  },

  orderCountText1: {
    textAlign: 'center',
    fontWeight: '400',
    fontSize: Styles.delivery.defaultFontSize,


  },
  innerText: {
    textAlign: 'center',
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    padding: 8
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  }
});

export default connect(mapStateToProps, actions)(StockManagement);

