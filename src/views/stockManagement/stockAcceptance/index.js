/* eslint-disable indent */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Dimensions, Image, Platform } from 'react-native';
import Colors from '../../../config/colors';
import { connect } from 'react-redux';
import strings from './../../../Language/StockAcceptance';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
import ActIndicator from './ActIndicator';
import { Header } from './Header';

const Utill = new Utills();
class StockAcceptance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userLogged: true,
            userData: '',
            resumit: true,
            apiLoading: true,
            locals: {
                activationDetails: strings.activationDetails,
                details: strings.details,
                continue: strings.continue,
                accept: strings.accept,
                reject: strings.reject,
                approved_date: strings.approved_date,
                approved_quantity: strings.approved_quantity,
                approved_quantity_full: strings.approved_quantity_full,
                requested_date: strings.requested_date,
                issued_quantity: strings.issued_quantity,
                issued_quantity_full: strings.issued_quantity_full
            },
            stoData: [],
            STODetails: [],
        };

    }

    componentDidMount() {
        this.props.stockAcceptanceMismatchedSerials([]);
        this.getOrderCount();
    }

    getOrderCount = () => {
        this.setState({ apiLoading: true }, () => {
            Utill.apiRequestPost('GetSTOInfo', 'GoodsAcceptance', 'cfss', {}, this.successCb, this.failureCb, this.exCb);
        })
    }

    successCb = (response) => {
        this.state.stoData = response.data.data.STOInfo;
        const valueArry = [];
        this.state.stoData.forEach((value, key) => {
            const requestedDate = value.RequestDate;
            const approvedDate = value.ApprovedDate;
            const issuedQty = value.IssuedQuantity;
            const approvedQty = value.ApprovedQuantity;
            const requestID = value.STOID;
            const newOb = {
                requestedDate,
                approvedDate,
                issuedQty,
                approvedQty,
                requestID
            };
            valueArry.push(newOb);
            this.setState({ stoData: valueArry });
        });
        this.setState({ apiLoading: false });
    }

    failureCb = (response) => {
        this.setState({ apiLoading: false });
        const navigatorOb = this.props.navigator;
        try {
            navigatorOb.showModal({
                screen: 'DialogRetailerApp.models.GeneralModalException',
                title: 'GeneralModalException',
                passProps: {
                    message: response.data.error
                },
                overrideBackPress: true
            });
        } catch (error) {
            console.log(`Show Modal error: ${error.message}`);
        }
    }

    exCb = (error) => {
        this.setState({ isLoading: false });
        const navigatorOb = this.props.navigator;
        try {
            navigatorOb.showModal({
                screen: 'DialogRetailerApp.models.GeneralModalException',
                title: 'GeneralModalException',
                passProps: {
                    message: error
                },
                overrideBackPress: true
            });
        } catch (error) {
            console.log(`Show Modal error: ${error.message}`);
        }
    }

    gotoRejectStock(requestID) {
        const navigatorOb = this.props.navigator;
        navigatorOb.push({
            title: 'STOCK ACCEPTANCE',
            screen: 'DialogRetailerApp.views.StockAcceptancePage4',
            passProps: {
                requestID: requestID,
            }
        });
        navigatorOb.pop();
    }

    gotoStockAcceptanceDetals() {
        const navigatorOb = this.props.navigator;
        navigatorOb.push({
            title: 'STOCK ACCEPTANCE',
            screen: 'DialogRetailerApp.views.StockAcceptancePage2',
            passProps: {
                screenTitle: 'STOCK ACCEPTANCE'
            }
        });
        navigatorOb.pop();
    }

    getSTODetails(reqId) {
        this.props.stockAcceptanceSelectedSTOID(reqId);
        const data = {
            STORequestID: reqId
        };
        this.setState({ apiLoading: true }, () => {
            Utill.apiRequestPost('GetSTODetailInfo', 'GoodsAcceptance', 'cfss',
                data, this.successCb_Details, this.failureCb_Details, this.exCb_Details);
        });
    }

    successCb_Details = (response) => {
        this.state.STODetails = response.data.data.MaterialData;
        const valueArry = [];
        this.state.STODetails.forEach((value, key) => {
            let icon;
            let activationStatus;
            const allImageStatus = [];
            const issueName = [];
            let count = 0;
            const icon2 = icon;
            const materialCode = value.MaterialCode;
            const approvedQty = value.ApprovedQty;
            const materialDesc = value.MaterialDesc;
            const issuedQty = value.SAPIssuedQty;
            const checked = value.Checked;
            const checkedQty = value.ChekedQty;
            const serialItem = value.SerialItem;
            const serials = value.Serials;
            const Status = value.Status;
            const newOb = {
                materialCode,
                approvedQty,
                materialDesc,
                issuedQty,
                checkedQty,
                serialItem,
                checked,
                Status,
                serials,
            };
            valueArry.push(newOb);
            this.setState({ STODetails: valueArry });
        });
        this.formatIssuedSerials();
        this.setState({ apiLoading: false });
    }

    failureCb_Details = (response) => {
        console.log('xxxx failureCb', response.data);
        this.setState({ apiLoading: false });
        const navigatorOb = this.props.navigator;
        try {
            navigatorOb.showModal({
                screen: 'DialogRetailerApp.models.GeneralModalException',
                title: 'GeneralModalException',
                passProps: {
                    message: response.data.error
                },
                overrideBackPress: true
            });
        } catch (error) {
            console.log(`Show Modal error: ${error.message}`);
        }
    }

    exCb_Details = (error) => {
        this.setState({ apiLoading: false });
        const navigatorOb = this.props.navigator;
        try {
            navigatorOb.showModal({
                screen: 'DialogRetailerApp.models.GeneralModalException',
                title: 'GeneralModalException',
                passProps: {
                    message: error
                },
                overrideBackPress: false
            });
        } catch (error) {
            console.log(`Show Modal error: ${error.message}`);
        }
    }

    formatIssuedSerials = () => {
        this.props.stockAcceptanceIssuedSerialList(this.state.STODetails);
        this.gotoStockAcceptanceDetals();
    }

    handleBackButtonClick = () => {
        this.okHandler();
        return true;
    }

    okHandler() {
        const navigatorOb = this.props.navigator;
        navigatorOb.pop({ animated: true, animationType: 'fade' });

    }

    renderListItem = ({ item }) => (
        <TouchableOpacity onPress={() => this.getSTODetails(item.requestID)}>
            <View style={styles.card}>
                {/* CARD TOP SECTION */}
                <View style={styles.cardTop}>
                    {/* SIM NUMBER AND PRIORITY LABEL START  */}
                    <View style={styles.cardTopSection1}>
                        <View style={styles.simNumberView}>
                            <View style={styles.simNumber}>
                                <Text style={styles.txtSimNumber}>{item.requestID}</Text>
                                <Image
                                    source={require('../../../../images/icons/delivered-box-verification-symbol.png')}
                                    style={styles.imageContentPhone}
                                />
                            </View>
                        </View>
                    </View>
                    {/* SIM NUMBER AND PRIORITY LABEL END */}
                    {/* DELIVERY TRUCK AND LABEL START */}
                    <View style={styles.cardTopSection2}>

                    </View>
                    <View style={styles.cardTopSection3}>
                        {/* <View style={styles.bottomSpaceView} /> */}
                    </View>
                </View>

                {/* CARD BODY */}
                <View style={styles.cardBody}>
                    <View style={styles.orderDetails}>
                        <View style={styles.orderDetail}>
                            <Text style={styles.txtOrderDetailLabel}>{this.state.locals.requested_date}</Text>
                            <Text style={styles.txtOrderDetailValue}> {item.requestedDate} </Text>
                        </View>

                        <View style={styles.orderDetail}>
                            <Text style={styles.txtOrderDetailLabel}> {this.state.locals.approved_date} </Text>
                            <Text style={styles.txtOrderDetailValue}> {item.approvedDate}</Text>
                        </View>
                        <View style={styles.orderDetail}>
                            <Text style={styles.txtOrderDetailLabel}> {this.state.locals.approved_quantity_full} </Text>
                            <Text style={styles.txtOrderDetailValue}> {item.approvedQty} </Text>
                        </View>

                        <View style={styles.orderDetail}>
                            <Text style={styles.txtOrderDetailLabel}> {this.state.locals.issued_quantity_full}</Text>
                            <Text style={styles.txtOrderDetailValue}> {item.issuedQty} </Text>
                        </View>
                    </View>
                </View>
                {/* CARD BODY END */}
            </View>
        </TouchableOpacity>
    )

    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={styles.contentContainer}>
                    <View>
                        <Header
                            backButtonPressed={() => this.handleBackButtonClick()}
                            headerText={this.props.screenTitle} />
                    </View>
                    {this.state.apiLoading == true ? <ActIndicator animating /> : true}
                    <FlatList
                        data={this.state.stoData}
                        renderItem={this.renderListItem}
                        keyExtractor={(item, index) => index}
                        scrollEnabled={true}
                    />
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return state.acceptStock;
};
const shadow = Platform.select({
    android: {
        elevation: 3
    },
    ios: {
        shadowRadius: 2,
        shadowColor: 'rgba(0, 0, 0, 1.0)',
        shadowOpacity: 0.54,
        shadowOffset: { width: 0, height: 2 },
    },
});

const styles = StyleSheet.create({
    bottomContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        backgroundColor: Colors.lightGrey

    },
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        alignSelf: 'center',
        backgroundColor: Colors.appBackgroundColor,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.lightGrey,
        justifyContent: "flex-start",
        alignItems: "center",
        alignSelf: 'center',
        flexDirection: 'column'
    }, contentContainer: {
        flex: 1,
        flexDirection: 'column',
    },
    errorDescView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorDescText: {
        textAlign: 'center',
        fontSize: 17,
        fontWeight: 'bold',
        color: Colors.black
    },
    sortingView: {
        flex: 0.08,
        flexDirection: 'row',
        backgroundColor: Colors.lightGrey,
        alignSelf: 'stretch',
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'center',

    },
    searchBarView: {
        flex: 0.08,
        flexDirection: 'row',
        backgroundColor: Colors.lightGrey,
        alignSelf: 'stretch',
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 5,
    },
    dropDownTextStyle: {
        color: Colors.black,
        fontSize: 15,
        fontWeight: 'bold'
    },
    dropDownMain: {
        width: Dimensions.get('window').width * 0.7
    },
    dropDownStyle: {
        flex: 1,
        width: Dimensions.get('window').width * 0.7,
        height: 160
    },
    dropDownTextHighlightStyle: {
        fontWeight: 'bold'
    },
    dropDownSelectedItemView: {
        flexDirection: 'row',
        backgroundColor: Colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%'
    },
    selectedItemTextContainer: {
        width: '90%',
        paddingLeft: 5,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    sortTypeText: {
        flex: 1,
        color: Colors.black,
        fontSize: 15,
        marginTop: 5,
        fontWeight: 'bold'
    },
    dropDownIcon: {
        width: '10%',
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    card: {
        flex: 1,
        backgroundColor: Colors.white,
        height: Dimensions
            .get('window')
            .height / 3,
        margin: 5,
        marginVertical: 4,
        marginHorizontal: 4,
        shadowOpacity: 0.54,
        shadowRadius: 5,
        shadowColor: 'rgba(0, 0, 0, 1.0)',
        shadowOffset: { height: 2, width: 0 },
        width: Dimensions.get('window').width - 10,
        elevation: 3,
        shadowRadius: 2

    },
    cardTop: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10
        // backgroundColor: 'lightpink'
    },
    cardBody: {
        flex: 1.9,
        flexDirection: 'column',
        // backgroundColor: 'yellow'
    },
    cardBottom: {
        flex: 0.7,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: Colors.lightGrey
        // backgroundColor: 'red'
    },
    sortByLabelContainer: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: 0
    },
    sortByDropDownContainer: {
        flex: 3,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        height: 50,
    },
    txtDispatch: {
        fontWeight: 'bold',
        color: Colors.colorDarkOrange
    },
    txtDetails: {
        fontWeight: 'bold'
    },
    txtSortBy: {
        fontWeight: 'bold',
        color: Colors.black,
        alignSelf: 'flex-start'
    },
    txtDuration: {
        fontWeight: 'bold',
        color: Colors.colorGreen
    },
    txtDurationRed: {
        fontWeight: 'bold',
        color: Colors.colorRed
    },
    txtPriority: {
        fontWeight: 'bold',
        color: Colors.white
    },
    txtSimNumber: {
        fontSize: 18,
        marginLeft: 7,
        color: Colors.black,
    },
    txtAddress: {
        fontWeight: 'bold',
        color: Colors.black
    },
    txtOrderDetailLabel: {
        flex: 1,
        marginLeft: 7,
        color: Colors.black
    },
    txtOrderDetailValue: {
        flex: 1,
        color: Colors.black
    },
    cardTopSection1: {
        flex: 1,
        marginLeft: 2,
        marginTop: 10,
        flexDirection: "column",
        // backgroundColor: "blue"
    },
    cardTopSection2: {
        flex: 1.1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    cardTopSection3: {
        flex: 1.5,
        flexDirection: "column",
        // backgroundColor: "lightyellow",
        justifyContent: "center",
        alignItems: "center"
    },
    cardBottomSection1: {
        flex: 3,
        flexDirection: "row",
        justifyContent: "flex-start",
        marginLeft: 10,
        alignItems: "center",
        // backgroundColor: "blue"
    },
    cardBottomSection2: {
        flex: 1,
        flexDirection: "column",
        // backgroundColor: "lightskyblue",
        justifyContent: "center",
        alignItems: "center"
    },
    cardBottomSection3: {
        flex: 2,
        flexDirection: "column",
        // backgroundColor: "lightgreen",
        justifyContent: "center",
        alignItems: "center"
    },
    simNumberView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        // backgroundColor: "mediumspringgreen"
    },
    deliveryTruckView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
        // backgroundColor: "mediumspringgreen"
    },
    simNumber: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    priorityView: {
        flex: 1,
        borderRadius: 10,
        flexDirection: "row",
        backgroundColor: Colors.colorRed,
        justifyContent: "center",
        alignItems: "center",
        margin: 5
    },
    priorityViewTransparent: {
        flex: 1,
        borderRadius: 10,
        flexDirection: "row",
        backgroundColor: Colors.transparent,
        justifyContent: "center",
        alignItems: "center",
        margin: 5
    },
    bottomSpaceView: {
        flex: 1
    },
    addressField: {
        flex: 0.7,
        margin: 5
    },
    orderDetail: {
        flex: 1,
        margin: 5,
        marginBottom: 0,
        marginTop: 0,
        flexDirection: 'row',
    },
    orderDetails: {
        flex: 1.6,
        flexDirection: 'column'
    },
    directionsView: {
        // height:50,
        // width:50,
        margin: 5,
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageContent: {
        flex: 1,
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        resizeMode: 'contain'
    },
    imageContentPhone: {
        flex: 1,
        width: 25,
        height: 25,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        resizeMode: 'contain',
        marginBottom: 5,
        marginLeft: 2,

    },
    directionsImageContent: {
        flex: 1,
        width: 26,
        height: 26,
        // resizeMode: 'stretch' 
    },
    txtDeliveryContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 10,
        // paddingTop: 10
    },
    deliveryTruckImageContainer: {
        flex: 1
    },
    textInputStyleClass: {
        flex: 1,
        textAlign: 'left',
        height: 35,
        borderWidth: 1,
        borderColor: Colors.navBarBackgroundColor,
        borderRadius: 7,
        backgroundColor: Colors.white,
        padding: 10
    }, txtBottom: {
        fontWeight: 'bold',
        color: Colors.black
    }, txtBottomAcceptStock: {
        fontWeight: 'bold',
        color: Colors.colorRed
    }
});

export default connect(mapStateToProps, actions)(StockAcceptance);