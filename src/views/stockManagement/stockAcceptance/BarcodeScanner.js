import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
  Alert,
  Keyboard,
} from 'react-native';

import Orientation from 'react-native-orientation'
import Camera, { constants } from 'react-native-camera';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/Wom';
import { Header } from '../../../components/others';
import ActIndicator from './ActIndicator';


const Utill = new Utills();
const { width } = Dimensions.get('window');
const maskColWidth = (width - 300) / 2;
const VIBRATION_DURATION = 200;
const MARGIN = 50;
const DEVICE_WIDTH = Dimensions
  .get('window')
  .width;

class StockAcceptanceBarcodeScanner extends Component {
  constructor(props) {
    super(props);
    // this.updateSerialList = this.updateSerialList.bind(this);
    strings.setLanguage(this.props.Language);
    // screenTitles.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      locals: {
        api_error_message: strings.api_error_message,
        network_error_message: strings.network_error_message,
        scannerBottomMessage: strings.scannerBottomMessage,
        scannerBottomMessage2: strings.scannerBottomMessage2,
        simBackMsg: strings.backMessage,
        permission_to_use_camera: 'Permission to use camera',
        permission_to_use_camera_message: 'To Scan the serial, App need your permission to use your camera',
      },

      barcodeValue: null,
      scanSucess: false,
      enterSimSerial: strings.enterSimSerial,
      enterValidSim: strings.enterValidSim,
      continue: strings.continue,
      scanSuccess: strings.scanSuccess,
      serialList: '',
      scannedSerials: [],
      issuedSerialList: '',
      IssSerials: [],
      receivedScannedSerials: [],
      misMatchedSerials: [],
      materialCode: this.props.MatCode,
      materialIndex: this.props.MatIndex,
      // STOId: this.props.STOReqId,
      apiLoading: true,
      uniqueValue: 0,
    };
    this.state.misMatchedSerials = this.props.misMatchedSerialList;
    this.props.stockAcceptanceIssuedSerialList(this.props.issuedSerialList);
    this.navigatorOb = this.props.navigator;
  }

  gotoPage3() {
    const navigatorOb = this.props.navigator;
    STORequestID = this.state.stoId;
    navigatorOb.resetTo({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.StockAcceptancePage3',
      passProps: {
        materialIndex: this.state.materialIndex,
        screenTitle: 'STOCK ACCEPTANCE'
      },
      overrideBackPress: true
    });
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  componentWillReceiveProps(nextProps) {
    // console.log('SIM TEST :: componentWillReceiveProps', nextProps.simApiFail);
    // if (nextProps.simApiFail) {
    //   this.setState({ barcodeValue: '', scanSucess: false });
    //   nextProps.getSimNumberMobileAct('');
    //   nextProps.getMobileNumberMobileAct('');
    // }

    // if (nextProps.sim_number == '') {
    //   nextProps.resetSimApiFail();
    // }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    Orientation.lockToPortrait()
  }

  successCb = (response) => {
    this.getScannedSerialsFromLog();
  }

  failureCb = (response) => {
    console.log('xxxx failureCb', response.data.data);
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    try {
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }

  exCb = (error) => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    try {
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }

  onHandleBackButton = () => {
    console.log('xxxx onHandleBackButton');
    Alert.alert('', this.state.locals.simBackMsg, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.goBack()
      }
    ], { cancelable: true });
    return true;
  }

  onBarCodeReadCb = (e) => {
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e
      .data
      .toString();
    this.setState({ barcodeValue: last8Digit });
    this.state.serialList = e.data.toString();
  };

  onChangeText = (value) => {
    this.setState({ barcodeValue: value });
  };

  goBack = () => {
    console.log('xxx goBack');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  returnView = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  };

  handleClick() {
    console.log("===> Button Tapped \n ");
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    this.onHandleBackButton();
  }

  displayAlertUnknownSerial(serial) {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        messageType: 'error',
        alertHeader: 'Serial No: ' + serial,
        alertMessage: 'This serial number is not in the issued serial list!',
        alertSubMessage: 'Contact the issued warehouse for clarification.'
      },
      overrideBackPress: true
    });
  }

  displayAlertAlreadyScanned() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        messageType: 'error',
        alertHeader: 'Error!',
        alertMessage: 'This serial is already scanned!'
      },
      overrideBackPress: true
    });
  }

  scanNextSerial() {
    if (__DEV__) console.log('-----------scanNextSerial---------------')

    let value = this.state.barcodeValue;
    let serialMatched = false;
    let issuedSerial = false;

    //Scan the JSON Array, locate the scanned Serial and Change its status. 
    for (let i = 0; i < this.props.issuedSerialList[this.state.materialIndex].serials.length; i++) {
      if (this.props.issuedSerialList[this.state.materialIndex].serials[i].serial === value) {
        issuedSerial = true;
        if (this.props.issuedSerialList[this.state.materialIndex].serials[i].status === false) {
          this.props.issuedSerialList[this.state.materialIndex].serials[i].status = true;
          this.props.issuedSerialList[this.state.materialIndex].checkedQty = this.props.issuedSerialList[this.state.materialIndex].checkedQty + 1;
          serialMatched = true;
          this.updateMismatchedSerials(value);
        } else {
          this.displayAlertAlreadyScanned();
          this.setState({ barcodeValue: '' })
          return false;
        }
      } else {

      }
    }
    //End of Serial status changing

    //Change the Material Overall Status;
    if (this.props.issuedSerialList[this.state.materialIndex].checkedQty == this.props.issuedSerialList[this.state.materialIndex].issuedQty) {
      this.props.issuedSerialList[this.state.materialIndex].Status = true;
    }
    //End of changing material overall status

    //Push miss-matched serials to an Array 
    if (serialMatched == false && issuedSerial == false) {
      const mismatchedSerial = this.state.misMatchedSerials.map(item => {
        return item;
      });

      mismatchedSerial.push(value);

      this.displayAlertUnknownSerial(value);
      this.setState({
        barcodeValue: '',
        misMatchedSerials: mismatchedSerial
      }, () => {
        if (__DEV__) console.log('value1 ', this.state.misMatchedSerials);
      });
      return false;
    } else {

    }
    //End of push miss-matched serials into an array

    this.setState({ barcodeValue: '' })
    return true;
  }

  updateMismatchedSerials(scannedSerial) {

    for (let i = 0; i < this.state.misMatchedSerials.length; i++) {
      if (this.state.misMatchedSerials[i] == scannedSerial) {
        this.state.misMatchedSerials.splice(i, 1);
      }
    }
    console.log('MISMATCHES LEFT' + JSON.stringify(this.state.misMatchedSerials));
  }

  scanningComplete() {
    Keyboard.dismiss();
    if (this.state.barcodeValue != '') {
      if (this.scanNextSerial() == true) {
        this.props.stockAcceptanceScannedSerialList(this.props.issuedSerialList);
        this.props.stockAcceptanceMismatchedSerials(this.state.misMatchedSerials);
        this.gotoPage3();
      }

    } else {
      Keyboard.dismiss();
      this.props.stockAcceptanceScannedSerialList(this.props.issuedSerialList);
      this.props.stockAcceptanceMismatchedSerials(this.state.misMatchedSerials);
      this.gotoPage3();
    }

  }

  render() {
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.onHandleBackButton()}
          headerText={this.props.scannerHeader ? this.props.scannerHeader : 'SCAN SERIAL'} />
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.cameraView}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}          
          onBarCodeRead={(e) => this.onBarCodeReadCb(e)}
        >
        </RNCamera>
        <View style={styles.maskOutter}>
          <View style={{ flex: 1 }}>
            <View style={[{ flex: 2.1 }, styles.maskRow, styles.maskFrame]}
            />
            <View style={[{ flex: 0.75 }, styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]}
              />
            </View>
            <View style={[{ flex: 2.26 }, styles.maskRow, styles.maskFrame]} />
          </View>
        </View>
        <View style={styles.overlayView}>
          <View style={styles.overlayContainer} >
            <Text style={styles.titleText}>Scan Serial{this.props.title}</Text>
            <View>
              <View style={styles.descContainer}>
                <Text style={styles.descText}>{this.state.locals.scannerBottomMessage}</Text>
                <Text style={styles.descText}> {this.state.locals.scannerBottomMessage2}</Text>
              </View>

              <View style={styles.bottomContainer}>
                <TextInput
                  style={styles.barcodeTxtInput}
                  underlineColorAndroid='transparent'
                  keyboardType={'numeric'}
                  value={this.state.barcodeValue}
                  placeholder='Enter serial'
                  autoFocus={false}
                  returnKeyType={'done'}
                  selectionColor={'yellow'}
                  onChangeText={(value) => this.setState({ barcodeValue: value })} />
                <TouchableOpacity
                  style={styles.barcodeContinueBtn}
                  onPress={() => this.scanNextSerial()}>
                  <Text style={styles.barcodeContinueBtnTxt}>
                    SCAN NEXT
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.barcodeContinueBtn}
                  onPress={() => this.scanningComplete()}>
                  <Text style={styles.barcodeContinueBtnTxt}>
                    {this.state.continue}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {this.state.isLoading ?
          <ActIndicator isTextWhite /> : true}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const sim_validation = state.sim.sim_validation;
  const issuedSerialList = state.acceptStock.stock_acceptance_issued_serial_list;
  const misMatchedSerialList = state.acceptStock.stock_acceptance_mismatched_serials;

  return { Language, simApiFail: state.sim.sim_api_fail, sim_number: state.mobile.sim_number, sim_validation, issuedSerialList, misMatchedSerialList };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  containerBarcodeScan: {
    flex: 1
  },
  cameraView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  maskOutter: {
    position: 'absolute',
    top: 55,
    left: 0,
    bottom: 0,
    right: 0
  },
  maskInner: {
    width: 312,
    backgroundColor: Colors.colorTransparent,
    // borderColor: Colors.colorPureYellow, //#FFFFC400
    borderColor: '#FFC400', //#FFFFC400
    borderWidth: 3,
  },
  maskFrame: {
    backgroundColor: Colors.colorBlack,
  },
  maskRow: {
    width: '100%',

  },
  maskCenter: { flexDirection: 'row' },
  barcodeTxtInput: {
    fontSize: Styles.otpModalFontSize,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'left',
    flex: 0.4
  },
  barcodeContinueBtn: {
    height: 36,
    width: 110,
    flex: 0.28,
    borderRadius: 2,
    justifyContent: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeNextBtn: {
    height: 36,
    width: 108,
    flex: 0.28,

    borderRadius: 2,
    justifyContent: 'center',
    backgroundColor: Colors.btnActive
  },
  barcodeContinueBtnTxt: {
    fontSize: Styles.thumbnailTxtFontSize,
    fontWeight: 'bold',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  overlayView: { position: 'absolute', top: 55, left: 0, right: 0, bottom: 0 },
  overlayContainer: { flex: 1, justifyContent: 'space-between' },
  titleText: { fontSize: 16, lineHeight: 19, color: Colors.appBackgroundColor, marginTop: 105, textAlign: 'center' },
  descContainer: { marginBottom: 79, alignItems: 'center' },
  descText: { fontSize: 16, lineHeight: 19, color: Colors.appBackgroundColor },
  bottomContainer: { height: 72, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 24, alignItems: 'center', backgroundColor: Colors.barcodeBottomBackColor },

});

export default connect(mapStateToProps, actions)(StockAcceptanceBarcodeScanner);