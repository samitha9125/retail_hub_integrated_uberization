import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Picker, BackHandler } from 'react-native';
import Colors from '../../../config/colors';
class StockAcceptancePage4 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userLogged: true,
      userData: '',
      resumit: true,
      locals: {}
    };

    console.log('<>>>><><><><>><><><><><><><><>>' + props.requestID);
  }

  componentDidUpdate() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerTop}></View>
        <View style={styles.containerMiddle}>
          <Text style={styles.buttonTxt}>Select the Reject Reason for From the List Below</Text>
          <Picker
            selectedValue={this.props.connectionTypeInput}
            mode={'dropdown'}
            style={styles.pickerStyle}
            onValueChange={this.connectionTypeChanged}>
            <Picker.Item label="Invalid Serial Found" value="GSM" />
            <Picker.Item label="Damaged Items" value="DTV" />
            <Picker.Item label="Other" value="HBB" />
          </Picker>
        </View>
        <View style={styles.containerBottom}>
          <View style={styles.containerBottomSection1}></View>
          <View style={styles.containerBottomSection2}>
            <TouchableOpacity style={styles.buttonCancelContainer}></TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}>
              <Text style={styles.buttonTxt}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerTop: {
    flex: 0.1,
    flexDirection: 'row'
  },
  containerMiddle: {
    flex: 0.75,
    margin: 7,
    flexDirection: 'column'
  },
  containerBottom: {
    flex: 0.15,
    flexDirection: 'row'
  },
  containerBottomSection1: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  containerBottomSection2: {
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    alignSelf: 'center'
  },

  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    alignSelf: 'flex-start'
  },
  buttonTxt: {
    color: Colors.colorBlack,
    textAlign: 'left',
    fontSize: 18,
    backgroundColor: Colors.colorTransparent
  }
});

export default StockAcceptancePage4;