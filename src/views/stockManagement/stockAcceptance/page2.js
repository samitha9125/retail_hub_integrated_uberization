import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, TouchableOpacity, Alert, TextInput } from 'react-native';
import Checkbox from 'react-native-check-box';
import Colors from '../../../config/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import iconRejected from '../../../../images/common/cross.png';
import barcodeIcon from '../../../../images/common/barcode.png';
import chevron from '../../../../images/icons/arrow.png';
import strings from './../../../Language/StockAcceptance';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Utills from '../../../utills/Utills';
import ActIndicator from './ActIndicator';
const Utill = new Utills();
import { Header } from './Header';
import { TextField } from 'react-native-material-textfield';

class StockAcceptancePage2 extends Component {

  constructor(props) {
    super(props);
    // strings.setLanguage(this.props.Language);

    this.state = {
      userLogged: true,
      userData: '',
      resumit: true,
      locals: {
        activationDetails: strings.activationDetails,
        details: strings.details,
        accept: strings.accept,
        reject: strings.reject,
        approved_date: strings.approved_date,
        approved_quantity: strings.approved_quantity,
        requested_date: strings.requested_date,
        issued_quantity: strings.issued_quantity,
        checked_quantity: strings.checked_quantity,
      },
      apiLoading: true,
      STODetails: [],
      materialCode: '',
      materialIndex: '',
    };

    console.log('STOTOCCEPT' + JSON.stringify(this.props));

  }

  componentWillMount() {
    // console.log("CHECKED ITEMS ON LOAD WILL MOUNT" + this.state.checkedItems);
  }

  componentDidMount() {
    this.forceUpdate();
    // console.log("CHECKED ITEMS ON LOAD DID MOUNT" + this.state.checkedItems);
  }

  componentWillUpdate() {
    // console.log("CHECKED ITEMS ON LOAD WILL UPDATE" + this.state.checkedItems);
  }

  gotoScanner() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'SCAN SERIAL',
      screen: 'DialogRetailerApp.views.StockAcceptanceBarcodeScanner',
      passProps: {
      }
    });
    navigatorOb.pop();
  }

  gotoPage3 = (materialCode) => {
    let materialIndex = this.getMaterialCodeIndex(materialCode);
    if (materialIndex != -1) {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        title: 'STOCK ACCEPTANCE',
        screen: 'DialogRetailerApp.views.StockAcceptancePage3',
        passProps: {
          screenTitle: 'STOCK ACCEPTANCE',
          MaterialCode: materialCode,
          materialIndex: materialIndex,
        }, overrideBackPress: true
      });
    } else {
      this.displayAlertUnknownSerial()
    }

  }

  gotoPage4() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.StockAcceptancePage4',
      passProps: {
      }
    });
    navigatorOb.pop();
  }

  displayMessageNoSerial(itemName) {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        messageType: 'alert',
        alertHeader: 'Info!',
        alertMessage: 'No serials available for item : ' + itemName
      }
    });
    navigatorOb.pop();
  }

  displayAlertNoItemsSelected() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        messageType: 'error',
        alertHeader: 'Error!',
        alertMessage: 'No items selected'
      }
    });
    navigatorOb.pop();
  }

  displayAlertUnknownSerial() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        messageType: 'error',
        alertHeader: 'System Error!',
        alertMessage: 'No serials found for the material'
      }
    });
  }

  getMaterialCodeIndex(materialCode) {
    let value = materialCode;
    for (let i = 0; i < this.props.issuedSerialList.length; i++) {
      if (this.props.issuedSerialList[i].materialCode === value) {
        return i;
      }
    }
    return -1; //to handle the case where the value doesn't exist
  }

  confirmStockAcceptance() {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.ConfirmModal',
      passProps: {
        messageType: 'error',
        messageHeader: 'Info!',
        mesageContent: 'The selected items will be accepted. Sure you want to proceed?',
      }
    });
    navigatorOb.pop();
  }

  displayAlert(messageType, header, body) {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.AlertSuccess',
      passProps: {
        alertHeader: header,
        messageType: messageType,
        alertMessage: body,
      }
    });
  }

  displayConfirmation(messageType, header, body) {
    console.log("SCANNER ACCESSED>>>>>>>>>>>>>>>>>>>>>>>");
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK ACCEPTANCE',
      screen: 'DialogRetailerApp.views.ConfirmModal',
      passProps: {
        messageHeader: 'Stock Request ID : ' + this.props.stoID,
        messageType: 'info',
        mesageContent: 'Do you want to accept & acquire stock?',
        onPressYes: () => this.acceptStocks()
      }
    });
  }

  checkBoxValueChange = (materialCode, Status) => {
    let materialIndex = this.getMaterialCodeIndex(materialCode)
    if (Status == true) {
      this.props.issuedSerialList[materialIndex].Status = false;
    } else {
      this.props.issuedSerialList[materialIndex].Status = true;
    }
    this.props.stockAcceptanceIssuedSerialList(this.props.issuedSerialList);
  }

  renderListItem = ({ item }) => (
    <View style={styles.detailElimentContainer}>
      <View style={styles.containerMiddleSection1}>
        <View style={styles.elementRightWithIcon}>
          {item.serialItem === 'serial' ?
            <Checkbox
              isChecked={item.Status}
              disabled={true}
              checkBoxColor={Colors.colorYellow} />
            :
            <Checkbox
              onClick={() => this.checkBoxValueChange(item.materialCode, item.Status)}
              isChecked={item.Status}
              disabled={false}
              checkBoxColor={Colors.colorYellow} />
          }
        </View>
      </View>
      <View style={styles.containerMiddleSection2}>
        <View style={styles.elementLeftForTitle}>
          {item.serialItem === 'serial' ?
            <View style={styles.containerMiddleSection5}>
              <TouchableOpacity onPress={() => this.gotoPage3(item.materialCode)}>
                <Text style={styles.elementTitleTxt}>
                  {item.materialDesc}</Text>
              </TouchableOpacity>
            </View> :
            <View style={styles.containerMiddleSection5}>
              <Text style={styles.elementTitleTxt}>
                {item.materialDesc}</Text>
            </View>
          }
        </View>
      </View>
      <View style={styles.elementMiddle}>
        <Text style={styles.elementTxt}>
          :</Text>
      </View>
      <View style={styles.containerMiddleSection3}>
        <View style={styles.elementLeft}>
          <Text style={styles.elementTxt}>
            {item.issuedQty}</Text>
        </View>
      </View>
      {item.serialItem === 'serial' ?
        <View style={styles.containerMiddleSection4}>
          <View style={styles.elementMiddle}>
            <Text style={styles.elementTxt}>
              {item.checkedQty}</Text>
          </View>
        </View> :
        <View style={styles.containerMiddleSection4}>
          <View style={styles.elementMiddle}>
            <TextInput style={styles.elementTxt}>
              {item.checkedQty}</TextInput>
          </View>
        </View>
      }
      {item.serialItem === 'd ' ?
        <View style={styles.containerMiddleSection5}>
          <TouchableOpacity onPress={() => this.gotoPage3(item.materialCode)}>
            {/* <Image source={chevron} style={styles.iconStyle} /> */}
            <View>
              <Image source={chevron} style={styles.iconStyle} />
              {/* <Ionicons name='md-arrow-dropright' size={30} /> */}
            </View>
          </TouchableOpacity>
        </View> :
        <View style={styles.containerMiddleSection5}>
          <TouchableOpacity onPress={() => this.displayMessageNoSerial(item.key1)}>
            {/* <Image source={chevron} style={styles.iconStyle} /> */}
            {/* <Text>></Text> */}
          </TouchableOpacity>
        </View>
      }
    </View>
  );


  handleBackButtonClick = () => {
   this.okHandler();
    return true;
  }

  okHandler() {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  acceptStocks = () => {
    let STOID = this.props.stoID;
    let STOData = this.props.issuedSerialList;
    const mismatched_serials = this.props.mismatched_serials;
    const Data = {
      ...STOData,
      mismatched_serials: mismatched_serials
    }

    const data = {
      STOID: STOID,
      Data: Data,
    };
    Utill.apiRequestPost('AcceptStocks_MIFE', 'GoodsAcceptance', 'cfss', data, this.successAcceptCb, this.failureAcceptCb, this.exAcceptCb);
  }

  successAcceptCb = (response) => {

    this.displayAlert('success', 'Success!', response.data.info);
  }

  failureAcceptCb = (response) => {
    this.displayAlert('error', 'Error!', response.data.error);
  }

  exAcceptCb = (response) => {
    this.displayAlert('error', 'Error!', response);
  }

  render() {
    return (
      <View style={styles.container}>

        <View>
          <Header
            backButtonPressed={() => this.handleBackButtonClick()}
            headerText={this.props.screenTitle} />
        </View>

        {/* {this.state.apiLoading == true ? <ActIndicator animating /> : true} */}
        <View style={styles.containerTop}>
          <View style={styles.containerTopSection1}>
          </View>
          <View style={styles.containerTopSection2}>
            <Text style={styles.containerTopText}>{this.state.locals.issued_quantity}</Text>
          </View>
          <View style={styles.containerTopSection3}>
            <Text style={styles.containerTopText}>{this.state.locals.checked_quantity}</Text>
          </View>
        </View>
        <View style={styles.containerMiddle}>
          {/* {console.log(JSON.stringify(this.state.STODetails))} */}
          <FlatList
            data={this.props.issuedSerialList}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index}
            scrollEnabled={true}
          />
        </View>
        <View style={styles.containerBottom}>
          <View style={styles.containerBottomSection1}>
            {/* <TouchableOpacity onPress={() => this.gotoScanner()}>
                            <Image source={barcodeIcon} style={styles.iconStyle} />
                        </TouchableOpacity> */}
          </View>
          <View style={styles.containerBottomSection2}>
            <TouchableOpacity style={styles.buttonCancelContainer}>
              {/* <Text style={styles.buttonTxt}>{this.state.locals.reject}</Text> */}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.displayConfirmation()} style={styles.buttonContainer} disabled={false}>
              <Text style={styles.buttonTxt}>{this.state.locals.accept}</Text>
            </TouchableOpacity>

          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ leftTxt, rightTxt }) => (
  <View style={styles.detailElimentContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementItemWithIconFont = ({ leftTxt, rightTxt, icon }) => (
  <View style={styles.detailElimentContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRightWithIcon}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
      <Image source={icon} style={styles.iconStyle} />
    </View>
  </View>
);

const mapStateToProps = (state) => {
  this.checkedItems = state.acceptStock.stock_acceptance_elements_list;
  const materialCode = state.acceptStock.stock_acceptance_issued_serial_list.materialCode;
  const issuedSerialList = state.acceptStock.stock_acceptance_issued_serial_list;
  const stoID = state.acceptStock.stock_acceptance_selected_sto_id;
  const mismatched_serials = state.acceptStock.stock_acceptance_mismatched_serials;
  return { materialCode, issuedSerialList, stoID, mismatched_serials };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },
  containerTop: {
    flex: 0.1,
    flexDirection: 'row'
  }, containerTopText: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    justifyContent: 'center',
    color: Colors.black
  }, containerTopSection1: {
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'center'
  }, containerTopSection2: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerTopSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerTopSection4: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center'
  }, containerMiddle: {
    flex: 0.75,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  }, containerMiddleSection1: {
    flex: 0.1,
    flexDirection: 'column',
    margin: 7,
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection2: {
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  }, containerMiddleSection3: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection4: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }, containerMiddleSection5: {
    flex: 0.1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerBottom: {
    borderWidth: 0.5,
    borderColor: Colors.borderLineColorLightGray,
    flex: 0.15,
    flexDirection: 'row'
  }, containerBottomSection1: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }, containerBottomSection2: {
    flex: 0.7,
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  detailsContainer: {
    borderWidth: 0,
    padding: 15
  },

  imageContainer: {
    alignItems: 'center'
  },

  imageStyle: {
    width: 50,
    height: 50,
    marginTop: 15,
    marginBottom: 5
  },

  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 5,
    paddingBottom: 10,
    margin: 10,
    marginRight: 0
  },

  dummyView: {
    flex: 0.6
  },

  dummyView3: {
    flex: 1.2

  },
  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'

  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    alignSelf: 'flex-start'
  },

  dummyView2: {
    flex: 0.2
  },

  buttonTxt: {
    color: Colors.btnActiveTxtColor,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: Colors.colorTransparent
  },

  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },

  elementLeft: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  elementLeftForTitle: {
    flex: 0.5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  elementMiddle: {
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  elementRight: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
  },
  elementTxt: {
    fontSize: 15,
    alignItems: 'center',
    justifyContent: 'center',
    color: Colors.black

  },
  elementTitleTxt: {
    fontSize: 15,
    color: Colors.black,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  elementRightWithIcon: {
    flex: 1,
    borderWidth: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconStyle: {
    width: 25,
    height: 25,
    marginTop: 0,
    borderWidth: 1,
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  }
})


export default connect(mapStateToProps, actions)(StockAcceptancePage2);