import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, Alert, ScrollView } from 'react-native';
import Checkbox from 'react-native-check-box';
import Colors from '../../../config/colors';
import barcodeIcon from '../../../../images/icons/barcode.png'
import strings from '../../../Language/StockAcceptance'
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from './Header';

class StockAcceptancePage3 extends Component {

    constructor(props) {
        super(props);
        // strings.setLanguage(this.props.Language);
        this.state = {
            userLogged: true,
            userData: '',
            numberList: [],
            resumit: true,
            locals: {
                // activationDetails: strings.activationDetails,
                // details: strings.details,
                // accept: strings.accept,
                // reject: strings.reject,
                // approved_date: strings.approved_date,
                // approved_quantity: strings.approved_quantity,
                // requested_date: strings.requested_date,
                // issued_quantity: strings.issued_quantity,
                // checked_quantity: strings.checked_quantity,
                // continue: strings.continue,
            },
            issuedRawSerials: [],
            issuedSerialList: '',
            issuedSerialListJSON: '',
            issuedSerialArray: this.props.SerialList,
            materialCode: this.props.MaterialCode,
            materialIndex: this.props.materialIndex,
            stoId: this.props.STORequestID,
            jsnTest: [],

        };
        this.state.issuedSerialList = this.props.issuedSerialList;
        console.log('sampleBhagya' + JSON.stringify(this.props.issuedSerialList));
    }

    gotoScanner() {
        const navigatorOb = this.props.navigator;
        navigatorOb.push({
            title: 'SCAN SERIAL',
            screen: 'DialogRetailerApp.views.StockAcceptanceBarcodeScanner',
            passProps: {
                MatCode: this.state.materialCode,
                MatIndex: this.state.materialIndex,
            }
        });
    }

    gotoPage2() {
        const navigatorOb = this.props.navigator;
        navigatorOb.resetTo({
            title: 'STOCK ACCEPTANCE',
            screen: 'DialogRetailerApp.views.StockAcceptancePage2',
            passProps: {
                screenTitle: 'STOCK ACCEPTANCE'
            },
            overrideBackPress: true
        });
    }

    handleBackButtonClick = () => {
        this.okHandler()
    }

    okHandler() {
        const navigatorOb = this.props.navigator;
        navigatorOb.pop({ animated: true, animationType: 'fade' });
    }

    checkBoxValueChange = (key) => {
        console.log("CHECK BOX VALUE CHANGED OF " + key);
    }

    componentWillMount() {
    }

    checkScannedSerials = (key) => {
        return true;
    }

    checkScannedSerials() {
        console.log("SCANNED SERIALS ARE NOW CHECKING>>>>>>>>>>>>>>>>>>>>");
    }

    renderListItem = ({ item }) => (
        <View>
            {/* {console.log("ISS111" + item)} */}
            {item.status == true ?
                <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                        <Checkbox isChecked={item.status} disabled={true} checkBoxColor={Colors.colorYellow} />
                    </View>
                    <View style={styles.elementRight}>
                        <Text style={styles.elementTxtSuccess}>
                            {item.serial}</Text>
                    </View>
                </View> :
                <View style={styles.detailElimentContainer}>
                    <View style={styles.elementLeft}>
                        <Checkbox isChecked={item.status} disabled={true} checkBoxColor={Colors.colorYellow} />
                    </View>
                    <View style={styles.elementRight}>
                        <Text style={styles.elementTxtFaulty}>
                            {item.serial}</Text>
                    </View>
                </View>}
        </View>
    );

    renderMisItem = ({ item }) => (
        <View style={{flexDirection: 'row', marginVertical:5}}>
        <View style={styles.elementLeft}>
            <Checkbox isChecked={item} disabled={true} checkBoxColor={Colors.colorYellow} />
        </View>
        <View style={styles.elementRight}>
            <Text style={styles.elementTxtMis}>
                {item}</Text>
        </View>
        </View>
    );



    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Header
                        backButtonPressed={() => this.handleBackButtonClick()}
                        headerText={this.props.screenTitle} />
                </View>
                <View style={styles.containerTop}>
                    <Text style={styles.titleText}>{this.props.issuedSerialList[this.state.materialIndex].materialDesc}</Text>
                </View>
                <ScrollView style={styles.containerMiddle}>
                    <FlatList
                        data={this.props.issuedSerialList[this.state.materialIndex].serials}
                        renderItem={this.renderListItem}
                        keyExtractor={(item, index) => index}
                        scrollEnabled={false}
                    />
                    <View>
                        <FlatList
                            data={this.props.misMatchedSerialList}
                            renderItem={this.renderMisItem}
                            keyExtractor={(item, index) => index}
                            scrollEnabled={false}
                        />
                    </View>
                </ScrollView>


                <View style={styles.containerBottom}>
                    <View style={styles.containerBottomSection1}>
                        <TouchableOpacity onPress={() => this.gotoScanner()}>
                            <Image source={barcodeIcon} style={styles.iconStyle} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerBottomSection2}>
                        <TouchableOpacity style={styles.buttonCancelContainer}>

                        </TouchableOpacity>

                        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.gotoPage2()}>
                            <Text style={{ fontWeight: 'bold' }}>
                                CONTINUE
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}

const ElementItemWithCheckBoxSuccess = ({ rightTxt }) => (
    <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
            <Checkbox onClick={() => this.checkBoxValueChange(item.key1)} disabled={true} />
        </View>
        <View style={styles.elementRight}>
            <Text style={styles.elementTxtSuccess}>
                {rightTxt}</Text>
        </View>
    </View>
);

const ElementItemWithCheckBoxFaulty = ({ rightTxt }) => (
    <View style={styles.detailElimentContainer}>
        <View style={styles.elementLeft}>
            <Checkbox />
        </View>
        <View style={styles.elementRight}>
            <Text style={styles.elementTxtFaulty}>
                {rightTxt}</Text>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginLeft: 2,
        // marginRight: 2,
        flexDirection: 'column',
        backgroundColor: Colors.backgroundColorWhiteWithAlpa
    },
    containerTop: {
        marginLeft: 7,
        marginRight: 2,
        flex: 0.1,
        justifyContent: 'flex-start',
        flexDirection: 'row'
    }, containerTopText: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        justifyContent: 'center',
    }, containerTopSection1: {
        flex: 0.5,
        flexDirection: 'column',
        margin: 7,
        alignItems: 'center'
    }, containerTopSection2: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerTopSection3: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerTopSection4: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center'
    }, containerMiddle: {
        marginLeft: 2,
        marginRight: 2,
        flex: 0.75,
        // flexDirection: 'row'
    }, containerMiddleSection1: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection2: {
        flex: 0.4,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection3: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection4: {
        flex: 0.2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }, containerMiddleSection5: {
        flex: 0.1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerBottom: {
        borderWidth: 0.5,
        borderRightColor: Colors.transparent,
        borderLeftColor: Colors.transparent,
        borderColor: Colors.borderLineColorLightGray,
        flex: 0.15,
        flexDirection: 'row'
    }, containerBottomSection1: {
        flex: 0.3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    }, containerBottomSection2: {
        flex: 0.7,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    detailsContainer: {
        borderWidth: 0,
        padding: 15
    },

    imageContainer: {
        alignItems: 'center'
    },

    imageStyle: {
        width: 50,
        height: 50,
        marginTop: 15,
        marginBottom: 5
    },

    bottomContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingRight: 5,
        paddingBottom: 10,
        margin: 10,
        marginRight: 0
    },

    dummyView: {
        flex: 0.6
    },

    dummyView3: {
        flex: 1.2

    },
    buttonCancelContainer: {
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.colorTransparent,
        height: 45,
        borderRadius: 5,
        alignSelf: 'flex-start'

    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.btnActive,
        height: 45,
        borderRadius: 5,
        marginLeft: 5,
    },

    dummyView2: {
        flex: 0.2
    },

    buttonTxt: {
        color: Colors.colorBlack,
        textAlign: 'center',
        backgroundColor: Colors.colorTransparent
    }, titleText: {
        paddingTop: 8,
        paddingLeft: 8,
        color: Colors.colorBlack,
        textAlign: 'left',
        backgroundColor: Colors.colorTransparent,
        fontSize: 18,
    },

    detailElimentContainer: {
        flexDirection: 'row',
        borderWidth: 0,
        marginBottom: 10
    },

    elementLeft: {
        paddingLeft: 8,
        flex: 0.1,
        borderWidth: 0
    },
    elementRight: {
        flex: 0.9,
        borderWidth: 0,
    },
    elementTxt: {
        fontSize: 18
    },
    elementTxtSuccess: {
        fontSize: 18,
        color: Colors.colorGreen
    },
    elementTxtFaulty: {
        fontSize: 18,
        color: Colors.colorBlack
    },
    elementTxtMis: {
        fontSize: 18,
        color: Colors.colorRed
    },
    elementRightWithIcon: {
        flex: 1,
        borderWidth: 0,
        flexDirection: 'row'
    },
    iconStyle: {
        width: 45,
        height: 45,
        marginTop: 0,
        borderWidth: 1,
        marginBottom: 0,
        marginLeft: 25
    }
});

const mapStateToProps = (state) => {
    console.log("STATE PAGE 3 MAP STATE TO PROPS");
    const scannedSerialCheck = state.acceptStock.stock_acceptance_scanned_serial_check;
    const scannedSerials = state.acceptStock.stock_acceptance_scanned_serial_list;
    const issuedSerialList = state.acceptStock.stock_acceptance_issued_serial_list;
    const misMatchedSerialList = state.acceptStock.stock_acceptance_mismatched_serials;
    return { scannedSerials, issuedSerialList, scannedSerialCheck, misMatchedSerialList };
}

export default connect(mapStateToProps, actions)(StockAcceptancePage3)