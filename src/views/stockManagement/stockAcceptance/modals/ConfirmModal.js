import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Orientation  from 'react-native-orientation'
import { connect } from 'react-redux';
import * as actions from '../../../../actions/index';
import Colors from '../../../../config/colors';
import Styles from '../../../../config/styles';
import Constants from '../../../../config/constants';
import Utills from '../../../../utills/Utills';
import imageAtentionIcon from '../../../../../images/common/alert.png';

const Utill = new Utills();

class ConfirmModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModel: false,
            apiData: {},
            locals: {
            },
            messageType: this.props.messageType,
            messageHeader: this.props.messageHeader,
            messageContent: this.props.mesageContent,
        };
    }

    componentWillMount(){
        Orientation.lockToPortrait()
    }

    componentWillUnmount(){
        Orientation.lockToPortrait()
    }

    exCb = () => {
        Utill.showAlertMsg('System Error');
    }

    onBnPressYes = () => {
        // const navigatorOb = this.props.navigator;
        // navigatorOb.push({
        //     title: 'STOCK ACCEPTANCE',
        //     screen: 'DialogRetailerApp.views.AlertSuccess',
        //     passProps: {
        //         messageType: 'success',
        //         alertHeader: 'Done!',
        //         alertMessage: 'Stock Accepted!'
        //     }
        // });
        // navigatorOb.pop();
    }

    onBnPressCancel = () => {
        const navigatorOb = this.props.navigator;
        navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        navigatorOb.pop();
    }

    render() {
        return (
            <View style={styles.containerOverlay}>
                <View style={styles.topContainer} />
                <View style={styles.modalContainer}>
                    <View style={styles.innerContainer}>
                        <View style={styles.alertImageContainer}>
                            <Image source={imageAtentionIcon} style={styles.alertImageStyle} />
                        </View>
                        <View style={styles.descriptionContainer}>
                            <Text style={styles.workOrderId}>{this.state.messageHeader}</Text>
                            <Text style={styles.descriptionText}>{this.state.messageContent}</Text>
                        </View>
                        <View style={styles.bottomCantainerBtn}>
                            <View style={styles.dummyView} />
                            <TouchableOpacity
                                onPress={() => this.onBnPressCancel()}
                                style={styles.bottomBtnCancel}>
                                <Text style={styles.bottomCantainerCancelBtnTxt}>
                                    {'NO'}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.onPressYes()}
                                style={styles.bottomBtn}>
                                <Text style={styles.bottomCantainerBtnTxt}>
                                    {'YES'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomContainer} />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const acceptedStocks = state.acceptStoks;
    return { acceptedStocks };
};

const styles = StyleSheet.create({

    containerOverlay: {
        flex: 1,
        backgroundColor: Colors.modalOverlayColor
    },

    topContainer: {
        flex: 0.4
    },
    modalContainer: {
        flex: 1.5
    },

    bottomContainer: {
        flex: 0.6
    },

    innerContainer: {
        flex: 1,
        backgroundColor: Colors.backgroundColorWhiteWithAlpa,
        padding: 12,
        paddingBottom: 20,
        marginTop: 25,
        marginBottom: 25,
        marginLeft: 13,
        marginRight: 13
    },

    alertImageContainer: {
        flex: 0.7,
        alignItems: 'center',
        padding: 5
    },

    alertImageStyle: {
        width: 90,
        height: 90,
        marginTop: 5,
        marginBottom: 5
    },

    descriptionContainer: {
        flex: 1.5,
        flexDirection: 'column',
        paddingTop: 10,
        paddingRight: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        marginTop: 10
    },
    descriptionText: {
        fontSize: Styles.delivery.modalFontSize,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        fontWeight: '100'

    },
    workOrderId: {
        alignItems: 'flex-start',
        fontSize: Styles.delivery.modalFontSize,
        textAlign: 'left',
        color: Colors.colorBlack,
        paddingTop: 5,
        marginLeft: 10,
        marginBottom: 15,
        paddingBottom: 5,
        marginRight: 10,
        marginTop: 20,
        fontWeight: 'bold'
    },

    bottomCantainerBtn: {
        flex: 0.5,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        paddingTop: 5,
        paddingRight: 5,
        paddingBottom: 5,
        marginTop: 10
    },

    dummyView: {
        flex: 3,
        justifyContent: 'center'
    },

    bottomBtnCancel: {
        flex: 1,
        justifyContent: 'center'
    },
    bottomBtn: {
        flex: 1,
        marginRight: 10,
        justifyContent: 'center'
    },
    bottomCantainerBtnTxt: {
        fontSize: Styles.delivery.modalFontSize,
        alignSelf: 'center',
        color: Colors.colorRed
    },
    bottomCantainerCancelBtnTxt: {
        fontSize: Styles.delivery.modalFontSize,
        alignSelf: 'center',
        color: Colors.colorBlack
    }

});

export default connect(mapStateToProps, actions)(ConfirmModal);
// export default ConfirmModal;
