
/*
 * @export
 * @class  AgentListItem
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This is a Presentational component used to render list items of subagents.
 *
 */

import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../config/colors';

export default class AgentListItem extends React.Component {
  render() {
    if (this.props.item) {
      return (
        <TouchableOpacity
          activeOpacity={0.5} onPress={() => this.props.onItemPress(this.props.item)} 
          style={styles.keyboardAwareScrollViewContainer}>
          <View
            style={styles.separator}
          />
          <View style={styles.textWrap}>
            <View style={styles.textViewStyle} >
              <Text style={styles.text}>{this.props.item.sub_agent_display}</Text>
            </View>
            <View style={{ flex:1 }} >
              <Text style={[styles.text, { color: this.props.item.status == "Pending" ? Colors.colorAgentPending : this.props.item.status == "In-Active" ? Colors.colorAgentInActive : Colors.colorAgentActive }]}>{this.props.item.status}</Text>
            </View>
            <View style={styles.textViewStyleRight} >
              <Text style={styles.text}>{this.props.item.nickName}</Text>
            </View>
            <View style={styles.iconWrap}>
              <Ionicons name='ios-arrow-forward' size={25} color='black' />
            </View>
          </View>
          {(this.props.index == this.props.ArrayLength - 1 )? (<View style={styles.separator}/>): null }
        </TouchableOpacity>
      );
    }
  }
}

/**
   * Style sheet for AGentList class .  
   * @const styles({})
   * @memberof AgentListItem
*/


const styles = StyleSheet.create({
  textWrap: {
    flex: 1,
    padding: 15,
    flexDirection: 'row',

  },
  text: {
    color: Colors.black,
    flexDirection: 'row'
  },
  iconWrap: { 
    flex: 0.3, 
    alignItems: 'flex-end' 
  },
  separator: {
    height: 0.9,
    width: "100%",
    backgroundColor: Colors.listSeparatorColor,

  },
  textViewStyle: {   
    flex:1.5,
    paddingRight: 30 
  },
  textViewStyleRight: {   
    flex:2,
    paddingLeft: 30 
  }
});

