
/*
 * @export
 * @class  SubAgentDetails
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata Pvt LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This Main Container for SubAgent Status Management.
 *
 */
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Switch } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import ActIndicator from '../../general/ActIndicator';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/settings';
import GeneralModel from '@Components/GenaralModel';
import images from '../../../config/images';

const Utill = new Utills();
class SubAgentDetails extends React.Component {

    static navigatorStyle = {
      navBarHidden: false,
      drawUnderNavBar: false,
      statusBarColor: Colors.statusBarColor,
      navBarTextColor: Colors.navBarTextColor,
      navBarButtonColor: Colors.navBarButtonColor,
      navBarBackgroundColor: Colors.navBarBackgroundColor,
      navBarComponentAlignment: 'center',
      navBarTitleTextCentered: true

    };
    constructor(props) {
      super(props);
      strings.setLanguage(this.props.language);
      {/** Local state of @SubAGentDETAILS ***/ }
      this.state = {
        userLogged: true,
        agentDetails: {},
        isLoading: false,
        resendOTP: strings.resendOTP,
        ok: strings.ok,
        deviceChange: strings.deviceChange,
        status: false,
        deviceChangeModel: false,
        deviceChangeDesc: '',
        otpRef: '',
        pinSentDesc: strings.pinSentDesc,
        resendPin: strings.resendPin,
        enterPin: strings.enterPin,
        didntReceivePin: strings.didntReceivePin,
        confirm: strings.confirm,
        cancel: strings.cancel,
        pin: '',
        agentAddedModel: false,
        OnResendPress: false,
        activationConfirmModel: false,
        inActivateConfirmModel: false,
        activateInfo: strings.ActivateInfo,
        inActivateInfo: strings.InActivateInfo,
        yes: strings.yes,
        no: strings.no,
        Erroinfo: '',
        ErrorModel: false,
        Welcome: strings.welcome,
        noAgent: strings.noAgentInfo,
        agentDetailsOb: this.props.agentDetails,


      };
    }

    componentWillMount() {
      Orientation.lockToPortrait();
    }

    componentDidMount() {
      this.loadAgentDetails();
    }

    componentWillUnmount() {
      Orientation.lockToPortrait();
    }
  
    loadAgentDetails = () => {
      this.setState({ isLoading: true });
      const data = { 
        sub_agent_id: this.props.agentDetails.sub_agent_id 
      
      };

      Utill.apiRequestPost('subAgentsDetailsById', 'subAgent', 'ccapp', data, this.successCb, this.failureCb, this.exCB);

    }


    successCb = (response) => {
      console.log('Data load :', response);
      this.setState({ isLoading: false, agentDetails: response.data.info, inActivateConfirmModel: false });

      if (response.data.info.status == 'Active') {
        this.setState({ status: true });
      }  else if (response.data.info.status == 'Pending'){
        this.setState({ status: true });
      } else {
        this.setState({ status: false });
      }

    }

    failureCb = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });
    }


    exCB = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });
    }

    onSendOTP() {
      const data = { 
        retailer_msisdn: this.state.agentDetails.sub_agent_msisdn 
      };
      this.setState({ isLoading: true });
      Utill.apiRequestPost('sendNewOTP', 'common', 'ccapp', data, this.successSendOTP, this.failureSendOTP, this.exSendOTP);

    }

    onBnPressYes = () => {
      if (this.state.pin == '') return;
      const data = {
        retailer_msisdn: this.state.agentDetails.sub_agent_msisdn,
        ref_id: this.state.ref_id,
        opt_pin: this.state.pin
      };
      this.setState({ isLoading: true });
      this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
      if (this.state.OnResendPress == false) {
        console.log('xxx updateSubAgentStatus');
        Utill.apiRequestPost('updateSubAgentStatus', 'subAgent', 'ccapp', data, this.success, this.failure, this.ex);
      } else {
        console.log('xxx reSedPinConfirmation');
        Utill.apiRequestPost('resedPinConfirmation', 'subAgent', 'ccapp', data, this.successCbModel, this.failureCbModel, this.exCBModel);
      }

    }


    successCbModel = (response) => {
      console.log('Data load :', response);
      this.setState({ pinSuccessInfo: response.data.info, agentAddedModel: true, isLoading: false });
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
      this.loadAgentDetails();

    }

    loadOTPModel(){
      var passProps = {
        mobile_number: this.state.agentDetails.sub_agent_display,
        ref_id: this.state.ref_id,
        agentDetails: this.state.agentDetails,
        onBnPressYes: () => this.onBnPressYes(),
        onTextChange: (text) => this.onTextChange(text),
        resendOtp: () => this.onResendOTP(),
        pin: this.state.pin

      };
      this.props.navigator.showModal({
        screen: 'DialogRetailerApp.modals.OTPModalScreenAgent',
        title: this.state.welcome,
        passProps: passProps,
        overrideBackPress: true
      });
    }

    failureCbModel = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });
     
    }


    exCBModel = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });
    }


    onTextChange(text) {
      this.setState({ pin: text });
      console.log('onTextChange', text);
    }


    successSendOTP = (response) => {
      console.log('Data load :', response);
      this.setState({ isLoading: false, ref_id: response.data.info.reference_id, pin: '', activationConfirmModel: false });
      let passProps = {
        mobile_number: this.state.agentDetails.sub_agent_display,
        ref_id: this.state.ref_id,
        agentDetails: this.state.agentDetails,
        onBnPressYes: () => this.onBnPressYes(),
        onTextChange: (text) => this.onTextChange(text),
        resendOtp: () => this.onResendOTP(),
        pin: this.state.pin

      };
      this.props.navigator.showModal({
        screen: 'DialogRetailerApp.modals.OTPModalScreenAgent',
        title: this.state.welcome,
        passProps: passProps,
        overrideBackPress: true
      });


    }

    failureSendOTP = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });

    }


    exSendOTP = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });

    }

    onResendOTP() {
      const data = { 
        retailer_msisdn: this.state.agentDetails.sub_agent_msisdn, 
        ref_id: this.state.ref_id 
      };
      this.setState({ isLoading: true });
      Utill.apiRequestPost('sendNewOTP', 'common', 'ccapp', data, this.successResendOTP, this.failureResendOTP, this.exResendOTP);
    }


    successResendOTP = (response) => {
      console.log('Data load :', response);
      this.setState({ isLoading: false });
    }

    failureResendOTP = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });

    }

    exResendOTP = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });
    }

    onDeviceChange() {
      const data = { 
        retailer_msisdn: this.state.agentDetails.sub_agent_msisdn 
      };
      this.setState({ isLoading: true });
      Utill.apiRequestPost('changeDeviceByRetailer', 'SubAgent', 'ccapp', data, this.successDeviceChange, this.failureDeviceChange, this.exDeviceChange);

    }

    successDeviceChange = (response) => {
      console.log('Data load :', response);
      this.setState({ isLoading: false, deviceChangeModel: true, deviceChangeDesc: response.data.info });

    }

    failureDeviceChange = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });
    }


    exDeviceChange = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });

    }

    onSwitchChange() {
      if (this.state.agentDetails.status == 'Pending') {
        this.setState({ OnResendPress: true, inActivateConfirmModel: true });
      }
      else if (this.state.agentDetails.status == 'In-Active') {
        this.setState({ OnResendPress: false, activationConfirmModel: true });

      } else {
        this.setState({ OnResendPress: true, inActivateConfirmModel: true });

      }
    }

    success = (response) => {
      console.log('Data load :', response);
      if (this.state.OnResendPress == false) {
        this.setState({ pinSuccessInfo: response.data.info, agentAddedModel: true });
      }
      this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
      this.loadAgentDetails();
    }

    failure = (response) => {
      console.log("API Error : ", response);
      this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });
    }


    ex = (response) => {
      console.log('EX Callback :', response);
      this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });
    }


    onResendPin() {
      console.log('xxx onResendPin');
      let self = this;
      this.setState({ OnResendPress: true }, () => {
        self.onSendOTP();
      });
    }


    inActivateAgent() {
      const data = { 
        retailer_msisdn: this.state.agentDetails.sub_agent_msisdn 
      };

      Utill.apiRequestPost('updateSubAgentStatus', 'subAgent', 'ccapp', data, this.success, this.failure, this.ex);
    }

    activateAgent () {
      this.onSendOTP();
    }


    render() {
      let { agentDetails } = this.state;
      let agentDetailsOb = agentDetails;
      if (agentDetailsOb) {
        return (
          <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
            {this.state.isLoading ?
              <ActIndicator displayText={'Loading'} animating />
              : true
            }
            <View style={styles.container}>
              <View style={styles.numberWrap}>
                <Text style={styles.text}>{agentDetails.sub_agent_display}</Text>
                <View style={styles.switchWrap}>
                  <Switch onTintColor={Colors.green} value={this.state.status} onValueChange={() => this.onSwitchChange()} />
                </View>
              </View>
              <View style={styles.statusWrap}>
                <Text style={[styles.textStatus, { color: agentDetails.status == "Pending" ? Colors.colorAgentPending : agentDetails.status == "In-Active" ? Colors.colorAgentInActive: Colors.colorAgentActive }]}>{agentDetails.status}</Text>
              </View>
              {agentDetailsOb.status == 'In-Active' ? null : <View>
                <TouchableOpacity activeOpacity={0.5} onPress={() => this.onResendPin()} >
                  <View
                    style={styles.separator}
                  />
                  <View style={styles.textWrap}>
                    <View style={{ paddingRight: 30 }} >
                      <Text style={styles.text}>{this.state.resendOTP}</Text>
                    </View>
                  </View>
                  {agentDetailsOb.status == 'Pending' ? <View
                    style={styles.separator}
                  /> : null}
                </TouchableOpacity>
                {agentDetailsOb.status == 'Pending' ? null : <TouchableOpacity activeOpacity={0.5} onPress={() => this.onDeviceChange()} >
                  <View
                    style={styles.separator}
                  />
                  <View style={styles.textWrap}>
                    <View style={{ paddingRight: 30 }} >
                      <Text style={styles.text}>{this.state.deviceChange}</Text>
                    </View>
                  </View>
                  <View
                    style={styles.separator}
                  />
                </TouchableOpacity>}
              </View>}

            </View>
            {/*******************************************/}

            {/*General Model for Device Change Success*/}
            <GeneralModel
              hideModel={() => this.setState({ deviceChangeModel: false })}
              primaryPress={() => this.setState({ deviceChangeModel: false })}
              secondoryPress={() => { }}
              disabled={false}
              primaryText={this.state.ok}
              modalVisible={this.state.deviceChangeModel}
              icon={images.icons.SuccessAlert}
              description={this.state.deviceChangeDesc}
              ButtonColor={Colors.green}
            />
            {/*******************************************/}

            {/*General Model for AgentAdded Change Success*/}
            <GeneralModel
              hideModel={() => this.setState({ agentAddedModel: false })}
              primaryPress={() => this.setState({ agentAddedModel: false })}
              secondoryPress={() => { }}
              disabled={false}
              primaryText={this.state.ok}
              modalVisible={this.state.agentAddedModel}
              icon={images.icons.Success}
              description={this.state.pinSuccessInfo}
              ButtonColor={Colors.green}
            />
            {/*******************************************/}

            {/*General Model for Activate Confirmation model */}
            <GeneralModel
              hideModel={() => this.setState({ activationConfirmModel: false })}
              primaryPress={() => this.activateAgent()}
              secondoryPress={() => this.setState({ activationConfirmModel: false })}
              disabled={false}
              secondryText={this.state.no}
              primaryText={this.state.yes}
              modalVisible={this.state.activationConfirmModel}
              icon={images.icons.Alert}
              description={this.state.activateInfo}
              ButtonColor={Colors.btnActive}
            />
            {/*******************************************/}

            {/*General Model for in-Activate Confirmation model */}
            <GeneralModel
              hideModel={() => this.setState({ inActivateConfirmModel: false })}
              primaryPress={() => this.inActivateAgent()}
              secondoryPress={() => this.setState({ inActivateConfirmModel: false })}
              disabled={false}
              secondryText={this.state.no}
              primaryText={this.state.yes}
              modalVisible={this.state.inActivateConfirmModel}
              icon={images.icons.Alert}
              description={this.state.inActivateInfo}
              ButtonColor={Colors.btnActive}
            />
            {/*******************************************/}

            {/*General Model for Errors */}
            <GeneralModel
              hideModel={() => this.setState({ ErrorModel: false })}
              primaryPress={() => this.setState({ ErrorModel: false })}
              disabled={false}
              primaryText={this.state.ok}
              modalVisible={this.state.ErrorModel}
              icon={images.icons.FailureAlert}
              description={this.state.Erroinfo}
              ButtonColor={Colors.colorRed}
            />
            {/*******************************************/}

          </KeyboardAwareScrollView>
        );
      } else return <EmptyFeed
        text={this.state.noAgent}
      />;
    }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: index => SubAgentDetails ', state);
  const language = state.lang.current_lang;
  return { language };
};

/**
   * Style sheet for AGentList class .  
   * @const styles({})
   * @memberof subAgentDetails
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 15,

  },
  numberWrap: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 20,
    paddingLeft: 15

  },
  statusWrap: {
    paddingTop: 10,
    paddingLeft: 15,
    paddingBottom: 50

  },
  textWrap: {
    flex: 1,
    padding: 15,
    flexDirection: 'row',

  },
  text: {
    color: Colors.black,
    fontSize: 18,
    fontWeight: 'bold'
  },
  textStatus: {
    color: Colors.black,
    fontSize: 15,
  },
  switchWrap: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 20
  },
  separator: {
    height: 1,
    width: "100%",
    backgroundColor: Colors.listSeparatorColor,
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
 
});

export default connect(mapStateToProps, actions)(SubAgentDetails);
