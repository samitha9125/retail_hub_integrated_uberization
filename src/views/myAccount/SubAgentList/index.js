
/*
 * @export
 * @class  AgentList
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This is the main Container for Subagent list and Add new Sub agent flow. 
 * Serves as a Higher order component distributing data to low order components.
 *
 */
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, FlatList } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import ActIndicator from '../../general/ActIndicator';
import EmptyFeed from '@Components/EmptyFeed';
import Utills from '../../../utills/Utills';
import AgentListItem from './AgentListItem';
import strings from '../../../Language/settings';
import GeneralModel from '@Components/GenaralModel';
import images from '../../../config/images';

const Utill = new Utills();

class AgentList extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    {/** Local state of @AgentList ***/ }
    this.state = {
      userLogged: true,
      agentList: [],
      isLoading: false,
      pin: '',
      pinSuccessInfo: '',
      agentAddedModel: false,
      ok: strings.ok,
      addNew: strings.addNEW,
      noAgentInfo: strings.noAgentInfo,
      Erroinfo: '',
      ErrorModel: false,
      refreshing: false,
      nickName: '',
      Welcome : strings.welcome,
      locals: {
        loading: 'Loading',
        subAgentTitle: strings.subAgentTitle
      }

    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentDidMount() {
    this.LoadAllAgents();
  }


  /**
   * This function used to catch the View appear event.  
   * @param {event} id 
   * @memberof SubagentList
   */
  onNavigatorEvent(event) {
    if (event.id === 'didAppear') {
      this.LoadAllAgents();
    }
  }


  /**
  * This function is used to load all sub agents using an api call.  
  * @memberof SubagentList
  */
  LoadAllAgents() {
    this.setState({ isLoading: true });
    Utill.apiRequestPost('getAllSubAgents', 'subAgent', 'ccapp', {}, this.successCb, this.failureCb, this.exCB);

  }


  /**
  * This function is used as a success call back for api calls.  
  * @param {response} 
  * @memberof SubagentList
  */
  successCb = (response) => {
    console.log('Data load :', response);
    this.setState({ isLoading: false, agentList: response.data.info, refreshing: false });

  }

  /**
 * This function is used as a failure call back for api calls.  
 * @param {response} 
 * @memberof SubagentList
 */

  failureCb = (response) => {
    console.log("API Error  initial data: ", response);
    this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true, refreshing: false });
  }


  /**
  * This function is used as a extended call back for api calls.  
  * @param {response} 
  * @memberof SubagentList
  */
  exCB = (response) => {
    console.log('EX Callback initial Data :', response);
    //this.setState({ isLoading: false, refreshing: false });
    this.setState({ isLoading: false, refreshing: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });

  }

  /**
  * This function is used as a onPress function for  subagents list item.  
  * @param {item} 
  * @memberof SubagentList
  */
  onItemPress(item) {
    this.props.navigator.push({
      title: this.state.locals.subAgentTitle, screen: 'DialogRetailerApp.views.myAccount.SubAgentDetails', passProps: {
        agentDetails: item
      },

    });
  }


  /**
  * This is the call back function of TextInput set number to local state.  
  * @param {text} 
  * @memberof SubagentList
  */
  onTextChangeNumber(text) {
    this.setState({ pin: text });
    console.log(text);
  }

/**
* This function is used check User availability
* @param {number} 
* @memberof SubagentList
*/
checkUserAvailability = (number, name) => {
  const data = { 
    retailer_msisdn : number 
  };
  this.setState({ isLoading: true, number: number, nickName: name });
  let me = this;
  const checkUserAvailabilitySuccessCb = function (response) {
    console.log('xxx checkUserAvailabilitySuccessCb', response);
    me.onSendOTP(number, name);
  };

  Utill.apiRequestPost('checkUserAvailability', 'subAgent', 'ccapp', data, checkUserAvailabilitySuccessCb, this.failureSendOTP, this.exSendOTP);
}

  /**
  * Send OTP api call action.  
  * @param {number} 
  * @memberof SubagentList
  */
onSendOTP(number, name) {
  const data = { 
    retailer_msisdn: number 
  };
  this.setState({ isLoading: true, number: number, nickName: name });
  Utill.apiRequestPost('sendNewOTP', 'common', 'ccapp', data, this.successSendOTP, this.failureSendOTP, this.exSendOTP);

}

  /**
  * This function is used as a success call back for api calls.  
  * @action sendOTP 
  * @param {response} 
  * @memberof SubagentList
  */
  successSendOTP = (response) => {
    console.log('Data load :', response);
    this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
    this.setState({ isLoading: false, ref_id: response.data.info.reference_id, pin: '' });

    var passProps = {
      mobile_number: response.data.info.sender_msisdn,
      ref_id: response.data.info.reference_id,
      onBnPressYes: () => this.onBnPressYes(),
      onTextChange: (text) => this.onTextChange(text),
      resendOtp: () => this.onResendOTP(),
      pin: this.state.pin,
      //getButtonEnableStatus: () => this.getButtonEnableStatus()

    };
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.OTPModalScreenAgent',
      title: this.state.Welcome,
      passProps: passProps,
      overrideBackPress: true
    });
  }


  /**
  * This function is used as a failure call back for api calls.  
  * @action sendOTP 
  * @param {response} 
  * @memberof SubagentList
  */
  failureSendOTP = (response) => {
    console.log("API Error : ", response);
    this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });

  }


  /**
  * This function is used as a Extended call back for api calls.  
  * @action sendOTP 
  * @param {response} 
  * @memberof SubagentList
  */
  exSendOTP = (response) => {
    console.log('EX Callback :', response);
    //this.setState({ isLoading: false });
    this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });

  }


  /** This is the function to confirm otp  verification.
  * @function onBnPressYes 
  * @param {response} 
  * @memberof SubagentList
  */
  onBnPressYes = () => {
    if (this.state.pin == '') return;
    const data = {
      retailer_msisdn: this.state.number,
      ref_id: this.state.ref_id,
      opt_pin: this.state.pin,
      nickName: this.state.nickName,
    };
    this.setState({ isLoading: true });
    Utill.apiRequestPost('createSubAgent', 'subAgent', 'ccapp', data, this.successCbModel, this.failureCbModel, this.exCBModel);
  }


  /**
 * This function is used as a Success call back for api calls.  
 * @function SuccessCbModel
 * @param {response} 
 * @memberof SubagentList
 */
  successCbModel = (response) => {
    console.log('Data load :', response);
    this.setState({ pinSuccessInfo: response.data.info, agentAddedModel: true, isLoading: false, nickName: '' });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this.LoadAllAgents();


  }

  /**
 * This function is used as a Failure call back for api calls.  
 * @function failureCbModel
 * @param {response} 
 * @memberof SubagentList
 */
  failureCbModel = (response) => {
    console.log("API Error : ", response);
    this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true, nickName: '' });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

  }

  /**
  * This function is used as a exCBModel call back for api calls.  
  * @function exCBModel
  * @param {response} 
  * @memberof SubagentList
  */
  exCBModel = (response) => {
    console.log('EX Callback :', response);
    // this.setState({ isLoading: false, nickName: '' });
    this.setState({ isLoading: false,  Erroinfo: Utill.getStringifyText(response), ErrorModel: true, nickName: '' });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }



  /**
  * This function is used to Resend OTP.  
  * @function Resend
  * @param {response} 
  * @memberof SubagentList
  */
  onResendOTP() {
    const data = { 
      retailer_msisdn: this.state.number, 
      ref_id: this.state.ref_id 
    };

    this.setState({ isLoading: true });
    Utill.apiRequestPost('sendNewOTP', 'common', 'ccapp', data, this.successResendOTP, this.failureResendOTP, this.exResendOTP);
  }


  /**
   * This function is used as a successResendOTP call back for api calls.  
   * @function successResendOTP
   * @param {response} 
   * @memberof SubagentList
   */
  successResendOTP = (response) => {
    console.log('Data load :', response);
    this.setState({ isLoading: false });
  }

  /**
  * This function is used as a failureResendOTP call back for api calls.  
  * @function failureResendOTP
  * @param {response} 
  * @memberof SubagentList
  */
  failureResendOTP = (response) => {
    console.log("API Error : ", response);
    this.setState({ isLoading: false, Erroinfo: response.data.error, ErrorModel: true });

  }

  /**
  * This function is used as a exResendOTP call back for api calls.  
  * @function exResendOTP
  * @param {response} 
  * @memberof SubagentList
  */
  exResendOTP = (response) => {
    console.log('EX Callback :', response);
    //this.setState({ isLoading: false });
    this.setState({ isLoading: false, Erroinfo: Utill.getStringifyText(response), ErrorModel: true });
  }


  /**
 * This function is used to Add new sub agents.  
 * @function AddNewAgent 
 * @memberof SubagentList
 */
  AddNewAgent() {
    var passProps = {
      onSendpinPress: (number, name) => this.checkUserAvailability(number, name),

    };
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NumberModelAgent',
      title: this.state.Welcome,
      passProps: passProps,
      overrideBackPress: true
    });

  }


  /**
   * This function is used to set pin value to the state from the text input.  
   * @param text
   * @function onTextChange 
   * @memberof SubagentList
   */
  onTextChange(text) {
    this.setState({ pin: text });
    console.log('onTextChange', text);
  }

  getButtonEnableStatus = () => {
    return Utill.validateDigit(this.state.pin) 
    && this.state.pin !== '' 
    && this.state.pin.length == 4 ;
  }


  /**
  * This function is used to extract a unique key for the flat-list render.  
  * @param text
  * @param index
  * @function _keyExtractor 
  * @memberof SubagentList
  */
  _keyExtractor = (item, index) => item.sub_agent_id;


  /**
  * This methond handles pull to refresh in the flat-list.  
  * @function onHandleRefresh()
  * @memberof SubagentList
  */
  onHandleRefresh = () => {
    this.setState({ refreshing: true });
    this.LoadAllAgents();
  }

  ItemSeparatorComponent = () => (
    <View style={styles.agentListSeparator}/>   
  );

  /**
  * This Render method for the subgentList Class.  
  * @function render()
  * @memberof SubagentList
  */

  render() {
    let { agentList } = this.state;
    return (
      <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
        {this.state.isLoading ?
          <ActIndicator displayText={this.state.locals.loading} animating />
          : true
        }
        <View style={styles.container}>
          {/* ADD NEW SUBAGENT BUTTON VIEW */}
          <View style={styles.buttonWrap}>
            <ElementFooter
              okButtonText={this.state.addNew}
              onClickOk={() => this.AddNewAgent()}
              self={this}
            />
          </View>

          {/* Renders all the sub agents ${Flat-List}*/}
          <View style={styles.agentList}>
            <FlatList
              style={styles.searchList}
              data={agentList}
              extraData={this.state}
              refreshing={this.state.refreshing}
              onRefresh={this.onHandleRefresh}
              keyExtractor={this._keyExtractor}
              ListEmptyComponent={() => (
                <EmptyFeed
                  text={this.state.noAgentInfo}
                />
              )}
              //ItemSeparatorComponent={this.ItemSeparatorComponent}
              renderItem={({ item, index }) => (
                <AgentListItem index={index} ArrayLength={agentList.length} item={item} onItemPress={(item) => this.onItemPress(item)} />
              )}

            />
          </View>
          {/*************************************************/}
        </View>

        {/*General Model for Agent Added Change Success*/}
        <GeneralModel
          hideModel={() => this.setState({ agentAddedModel: false })}
          primaryPress={() => this.setState({ agentAddedModel: false })}
          secondoryPress={() => { }}
          disabled={false}
          primaryText={this.state.ok}
          modalVisible={this.state.agentAddedModel}
          icon={images.icons.SuccessAlert}
          description={this.state.pinSuccessInfo}
          ButtonColor={Colors.green}
        />
        {/*******************************************/}

        {/*General Model for Errors */}
        <GeneralModel
          hideModel={() => this.setState({ ErrorModel: false })}
          primaryPress={() => this.setState({ ErrorModel: false })}
          disabled={false}
          primaryText={this.state.ok}
          modalVisible={this.state.ErrorModel}
          icon={images.icons.FailureAlert}
          description={this.state.Erroinfo}
          ButtonColor={Colors.colorRed}
        />
        {/*******************************************/}
      </KeyboardAwareScrollView>
    );
  }
}


/**
   * This is the primary button presentation Component.  
   * @param okButtonText
   * @param onClickOk
   * @param self 
   * @function ElementFooter()
   * @memberof SubagentList
*/

const ElementFooter = ({ okButtonText, onClickOk, self }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView} />
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={0.5}>
      <Text style={styles.buttonTextStyle}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: index => SubAgentList ', state);
  const language = state.lang.current_lang;
  return { language };
};

/**
   * Style sheet for AGentList class .  
   * @const styles({})
   * @memberof SubagentList
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 15
  },
  buttonWrap: {
    flex: 1,
  },
  dummyView: {
    flex: 1.4,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },

  buttonTextStyle: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  },

  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  agentList: {
    flex: 1,
    paddingTop: 10,
  },
  agentListSeparator: {
    height: 1,
    width: "100%",
    backgroundColor: Colors.listSeparatorColor,

  },
});

export default connect(mapStateToProps, actions)(AgentList);
