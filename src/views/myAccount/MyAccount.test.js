/*
 * Copyright (C) Dialog axiata PVT LTD
 * Author : Arafath Misree
 * Lastupdated by : Arafath Misree
 * Created on : N/A
 * This Main Test file for unit testing on MyAccount module.
 *
 */

import 'react-native';
import React from 'react';
import MyAccount from './index';
import renderer from 'react-test-renderer';


/***** 
  *  Test case 1 
  *  Case : MyAccount renders Correctly
  *********/

it('My account Home  Renders  correctly', () => {
  const tree = renderer.create(
    <MyAccount />
  ).toJSON();
});