
/*
 * @export
 * @class  MyAccount
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This Main Menu for subagent Management.
 *
 */
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ez_cash_rapid_ez_icon from '../../../images/drawer/ez_cash_rapid_ez.png';
import SubAgentManagement from '../../../images/drawer/SubAgentManagement.png';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import strings from '../../Language/settings';

class MyAccount extends React.Component {
  constructor(props) {
    {/** Local state of @MyAccount ***/ }
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      txtNumberSelection: strings.txtNumberSelection,
      txtNumberSelectionTitle: strings.txtNumberSelectionTitle,
      subAgent: strings.subAgent,
      subAgentTitle: strings.subAgentTitle,
    };
  }

  /**
  * This function is used to navigate through menu.  
  * @param key 
  * @function onSettingsMenuTap()
  * @memberof MyAccount
  */

  onSettingsMenuTap = (key) => {
    console.log('xxxx onSettingsMenuTap=>', key);
    const navigator = this.props.navigator;
    switch (key) {
      case 1:
        navigator.push({ title: this.state.subAgentTitle, screen: 'DialogRetailerApp.views.myAccount.SubAgents' });
        break;
      case 2:
        navigator.push({ title: this.state.txtNumberSelectionTitle, screen: 'DialogRetailerApp.views.NumberSelectionScreen' });
        break;
      default:
        navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.HomeTileScreen' });
        break;
    }
  };


  /**
  * This is the  Render method for the MyAccount Class.  
  * @function render()
  * @memberof MyAccount
  */
  render() {
    return (
      <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
        <View style={styles.container}>
          {/*Menu item */}
          { this.props.isCFSSAgent == false?
            <TouchableOpacity
              key={1}
              style={styles.cardContainer}
              onPress={() => this.onSettingsMenuTap(1)}>
              <View style={styles.leftImageContainer}>
                <Image source={SubAgentManagement} style={styles.ez_cash_rapid_ez_icon} />
              </View>
              <View style={styles.middleTextContainer}>
                <Text style={styles.innerText}>{this.state.subAgent}
                </Text>
              </View>
              <View style={styles.rightImageContainer}>
                <Ionicons name='ios-arrow-forward' size={25} color='black' />
              </View>
            </TouchableOpacity>
            : true }
          {/*Menu item */}
          <TouchableOpacity
            key={2}
            style={styles.cardContainer}
            onPress={() => this.onSettingsMenuTap(2)}>
            <View style={styles.leftImageContainer}>
              <Image source={ez_cash_rapid_ez_icon} style={styles.ez_cash_rapid_ez_icon} />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{this.state.txtNumberSelection}
              </Text>
            </View>
            <View style={styles.rightImageContainer}>
              <Ionicons name='ios-arrow-forward' size={25} color='black' />
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}


/**
   * Function to map redux state into myAccount.  
   * @function mapStateToProps()
   * @param state
   * @return {object}
   * @memberof MyAccount
*/
const mapStateToProps = (state) => {
  console.log("IN MY ACCOUNT", JSON.stringify(state.auth));
  return { isCFSSAgent: state.auth.isCFSSAgent, language: state.lang.current_lang };
};


/**
   * Style sheet for MyAccount class .  
   * @const styles({})
   * @memberof MyAccount
*/
const styles = StyleSheet.create({
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5
  },
  leftImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 8
  },
  ez_cash_rapid_ez_icon: {
    height: 37,
    width: 37
  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  rightImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  innerText: {
    flex: 1,
    justifyContent: 'center',
    fontSize: 20,
    color: Colors.colorBlack,
    padding: 8
  }
});

export default connect(mapStateToProps, actions)(MyAccount);
