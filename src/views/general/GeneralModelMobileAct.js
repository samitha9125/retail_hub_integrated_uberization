import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Picker,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import Utills from '../../utills/Utills';

import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Orientation from 'react-native-orientation';

const Utill = new Utills();

class GeneralModelMobileAct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      otpMobileNumber: '',
      pendingState: 0
    };
    this.navigator = this.props.navigator;
  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }
  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.otp_validation !== null && nextProps.otp_validation.success == true && !nextProps.skip_validation && nextProps.model_popped && !nextProps.otp_popped) {
      console.log('xxxxxxxxxxxxxxxxxx OTP TRIGGERED xxxxxxxxxxxxxxxxxx');
      this
        .props
        .navigator
        .dismissLightBox();
      this
        .props
        .mobileActOtpPopped(true);
      const navigator = this.props.navigator;
      navigator.showModal({ title: 'MobileActModel', screen: 'DialogRetailerApp.models.MobileActOtpModal', overrideBackPress: true });
    } else if (nextProps.m_connect_validation !== null && nextProps.m_connect_validation.success == true && !nextProps.skip_validation && !nextProps.m_connect_popped) {
      this.setState({ pendingState: 1 });
      this
        .props
        .mobileActMConnectPopped(true);
      this
        .props
        .navigator
        .dismissLightBox();

      Alert.alert('M CONNECT', 'Verify using M Connect', [
        {
          text: 'VERIFY',
          onPress: () => this.onMConnectVerify()
        }
      ], { cancelable: false });
    } else {
      //DO nothing
    }
  }

  onMConnectVerify = () => {
    const data = {
      auth_req_id: this.props.m_connect_validation.auth_req_id
    };

    Utill.apiRequestPost('mConnectStatus', 'account', 'ccapp', data, this.mConnectSuccess, this.mConnectFailure);
  }

  mConnectSuccess = (response) => {
    if (response.data.data.status == 1) {
      Alert.alert(JSON.stringify('MConnect Verified!'));
      this
        .props
        .getOtpStatusMobileAct(3);
      this
        .props
        .resetMobileActMConnectValidation();
    } else if (response.data.data.status == 2) {
      this
        .props
        .resetMobileActivationState();
      Alert.alert(JSON.stringify('MConnect Verification failed!'));
    } else if (response.data.data.status == 0) {
      this.setState({
        pendingState: this.state.pendingState + 1
      });

      if (this.state.pendingState <= 3) {
        Alert.alert('M CONNECT', 'Verify using M Connect', [
          {
            text: 'VERIFY',
            onPress: () => this.onMConnectVerify()
          }
        ], { cancelable: false });
      } else {
        Alert.alert(JSON.stringify('MConnect Verification failed!'));
        this
          .props
          .resetMobileActivationState();
      }

      // Alert.alert(JSON.stringify('MConnect Verification Pending!'));
    }
  }

  mConnectFailure = (response) => {
    this
      .props
      .resetMobileActivationState();
    Alert.alert(JSON.stringify(response.data.error));
  }

  onSkipValidation(props) {
    this
      .props
      .getOtpStatusMobileAct(2);
    this
      .props
      .navigator
      .dismissLightBox();
    this
      .props
      .getSkipValidationMobileAct(true);
  }

  onSendValidation() {
    if (this.props.otp_number === 'Select a Number') {
      Utill.showAlertMsg('Please select a number from list!');
    } else {
      //otp
      const data = {
        customer_msisdn: this.props.otp_number
      };

      this
        .props
        .mobileActOTPValidation(data);

      this
        .props
        .getOtpStatusMobileAct(1);
    }
  }

  valueChanged(number) {
    this
      .props
      .getOTPNumberMobileAct(number);

    this.setState({ otpMobileNumber: number });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.topContainer}>
            <Text style={styles.mainText}>
              Customer details are already with Dialog.
            </Text>
            <Text style={styles.mainText}>
              Select a mobile number to validate details
            </Text>
            <Picker
              style={styles.picker}
              selectedValue={this.state.otpMobileNumber}
              mode={'dropdown'}
              onValueChange={this
                .valueChanged
                .bind(this)}>
              <Picker.Item label={'Select a Number'} value={'Select a Number'}/>{this.props.numberList == undefined || this.props.numberList == null
                ? <View/>
                : this
                  .props
                  .numberList
                  .map(number => (<Picker.Item key={number} label={Utill.numberMask(number)} value={number}/>))}
            </Picker>
            <View style={styles.containerOne}>

              <TouchableOpacity
                style={styles.touchOpacity}
                onPress={this
                  .onSendValidation
                  .bind(this)}
                activeOpacity={1}>
                <Text style={styles.subText}>
                  SEND VALIDATION</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity
              onPress={this
                .onSkipValidation
                .bind(this)}
              style={styles.bottomBtn}>
              <Text style={styles.bottomBtnTxt}>Skip Validation</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  let numberList;
  const otp_validation = state.mobile.otp_validation;
  const local_foreign = state.mobile.local_foreign;
  const m_connect_validation = state.mobile.m_connect_validation;
  const otp_number = state.mobile.otp_number;
  const model_popped = state.mobile.model_popped;
  const otp_popped = state.mobile.otp_popped;
  const m_connect_popped = state.mobile.m_connect_popped;
  if (state.mobile.nic_validation !== null) {
    numberList = state.mobile.nic_validation.notificationNumberList;
  } else {
    numberList = [
      {
        arr: 1,
        arr: 2
      }
    ];
  }
  return {
    state,
    numberList,
    otp_validation,
    m_connect_validation,
    model_popped,
    m_connect_popped,
    otp_number,
    otp_popped,
    local_foreign
  };
};

const styles = StyleSheet.create({
  container: {
    height: Dimensions
      .get('window')
      .height * 0.55,
    backgroundColor: Colors.colorWhite,
    padding: 10,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: Colors.colorWhite
  },
  topContainer: {
    backgroundColor: Colors.colorWhite
  },
  bottomCantainerBtn: {
    alignSelf: 'center'
  },

  bottomBtn: {
    width: '100%'
  },
  bottomBtnTxt: {
    alignSelf: 'center',
    textDecorationLine: 'underline',
    color: Colors.colorBlue
  },

  mainText: {
    fontSize: Styles.otpModalFontSize,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 15,
    fontWeight: '100'
  },
  picker: {
    width: '100%',
    margin: 0,
    padding: 0,
    marginLeft: 10,
    borderWidth: 1
  },
  containerOne: {
    alignSelf: 'center',
    paddingTop: 20,
    paddingBottom: 10,
    width: '100%'
  },
  touchOpacity: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorYellow,
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
    marginRight: 10
  },

  subText: {
    color: Colors.colorBlack,
    backgroundColor: Colors.colorBackground
  }

});

export default connect(mapStateToProps, actions)(GeneralModelMobileAct);
