import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import imageSuccessAlert from '../../../images/common/success_msg.png';
import Orientation from 'react-native-orientation';

class GeneralModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false
    };
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  onBnPress(props) {
    this.props.navigator.dismissLightBox();
    // this
    // .props
    // .resetMobileActivationState();
    this.props.navigator.resetTo({
      title: 'Dialog Sales App', 
      screen: 'DialogRetailerApp.views.HomeTileScreen' 
    });
  }

  render() {
    console.log(this.props);
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.alertImageContainer}>
            <Image source={imageSuccessAlert} style={styles.alertImageStyle} />
          </View>
          <Text style={styles.desTxt}>Success</Text>
          <Text style={styles.titleTxtStyle}>ORDER ID: {this.props.order_id}</Text> 
          <Text style={styles.titleTxtStyle}>{this.props.message}</Text>
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity onPress={this.onBnPress.bind(this)} style={styles.bottomBtn}>
              <Text style={styles.bottomCantainerBtnTxt}>
                {'OK'}</Text>
            </TouchableOpacity>

          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions
      .get('window')
      .height * 0.5,
    backgroundColor: '#ffffff',
    //borderRadius: 5,
    //paddingLeft: 18,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: '#fff'
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 80,
    height: 80,
    marginTop: 10,
    marginBottom: 10
  },

  amountStyle: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 20,
  },

  titleTxtStyle: {
    fontSize: 20,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 0,
    fontWeight: '100'

  },
  desTxt: {
    fontSize: 23,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold',
    // textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 25
  },

  bottomBtn: {
    width: 40

  },
  bottomCantainerBtnTxt: {
    fontSize: 20,
    color: 'green'

  }

});

export default connect(null, actions)(GeneralModel);
