import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

import imageSuccessAlert from '../../../images/common/success_msg.png';
import imageErrorsAlert from '../../../images/common/error_msg.png';
import Orientation from 'react-native-orientation';

class GeneralModalPinChange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false
    };
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  async onBnPress(props) {
    const me = this;

    setTimeout(() => {
      me
        .props
        .navigator
        .dismissLightBox();
    }, 50);

    if (this.props.status == 'Success') {
      this
        .props
        .resetMobileActivationState();

      console.log('xxx onLogout');
      const navigator = this.props.navigator;
      try {
        let isFirstTime = await AsyncStorage.getItem('isFirstTime');
        await AsyncStorage.removeItem('isFirstTime');
        await AsyncStorage.removeItem('api_key');
        await AsyncStorage.removeItem('token');
        await AsyncStorage.setItem('isFirstTime', isFirstTime);
        navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.LoginScreen' });
      } catch (error) {
        console.log(`AsyncStorage error: ${error.message}`);
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.alertImageContainer}>
            {this.props.status == 'Success'
              ? <Image source={imageSuccessAlert} style={styles.alertImageStyle}/>
              : <Image source={imageErrorsAlert} style={styles.alertImageStyle}/>
            }
          </View>
          <View>
            <Text style={styles.desTxt}>{this.props.message}</Text>
          </View>
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity
              onPress={this
                .onBnPress
                .bind(this)}
              style={styles.bottomBtn}>
              <Text style={styles.bottomCantainerBtnTxt}>
                {'OK'}</Text>
            </TouchableOpacity>

          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions
      .get('window')
      .height * 0.5,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: Colors.colorWhite
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },

  desTxt: {
    alignItems: 'flex-start',
    fontSize: 20,
    marginBottom: 0,
    marginLeft: 10,
    marginTop: 5,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 25
  },

  bottomBtn: {
    width: 40

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen

  }

});

export default connect(null, actions)(GeneralModalPinChange);
