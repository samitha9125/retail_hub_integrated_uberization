import { AppRegistry, Dimensions } from 'react-native';
import React, { Component } from 'react';
import { View, Animated, PanResponder } from 'react-native';
const { width, height } = Dimensions.get('window');

class DragComponent extends Component {
  state = {
    valueX: '',
    preX: "",
    preY: "",
    respond: true
  };

  constuctor(props) {
    console.log(props);
  }

  componentDidMount() { }

  componentWillMount() {
    this.value = { x: 0, y: 0 };
    this.anim = new Animated.ValueXY();
    /*this.anim0 = new Animated.Value(0);*/
    this.anim.addListener(value => {
      this.value = value;
    });

    this.guestureResponder = {
      onStartShouldSetResponder: evt => {
        return this.state.respond;
      },
      onMoveShouldSetResponder: evt => true,
      onResponderGrant: evt => {
        this.setState({
          preX: evt.nativeEvent.pageX,
          preY: evt.nativeEvent.pageY
        });
        this.anim.setOffset({
          x: this.value.x - evt.nativeEvent.pageX,
          y: this.value.y - evt.nativeEvent.pageY
        });
        this.anim.setValue({
          x: evt.nativeEvent.pageX,
          y: evt.nativeEvent.pageY
        });
      },
      onResponderReject: evt => {
        console.log('onResponderReject');
      },
      onResponderMove: Animated.event(
        [
          {
            nativeEvent: { pageX: this.anim.x, pageY: this.anim.y }
          }
        ],
        {}
      ),
      onResponderRelease: evt => {
        this.setState({ respond: false });
        setTimeout(() => {
          this.setState({ respond: true });
        }, 2000);
        if ((Math.abs(this.state.preX - evt.nativeEvent.pageX) < 5) && (Math.abs(this.state.preY - evt.nativeEvent.pageY) < 5)) {

          console.log('check123' + this.props.navigation);
          this.props.navigation.push({
            // title: 'PENDING WORK ORDERS',
            screen: this.props.screen,
            passProps: {

            }
          })

        }
        this.anim.flattenOffset();
        if (evt.nativeEvent.pageY > (height - 66)) {
          this.anim.setValue({
            x: this.value.x,
            y: (height * 0.2) - 88
          });
        }

        if (evt.nativeEvent.pageX > (width - 33)) {
          this.anim.setValue({
            x: (width * 0.2) - 66,
            y: this.value.y
          });
        }

        if (evt.nativeEvent.pageX < 33) {
          this.anim.setValue({
            x: -(width * 0.8),
            y: this.value.y
          });
        }

        if (evt.nativeEvent.pageY < (33)) {
          this.anim.setValue({
            x: this.value.x,
            y: -(height * 0.8)
          });
        }
      }
    };
  }

  render() {
    return (
      <Animated.View
        style={[
          { transform: this.anim.getTranslateTransform() },
          styles.container
        ]}
        {...this.guestureResponder}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    elevation: 20,
    top: height * 0.75,
    left: width * 0.8
  }
};

export default DragComponent;
