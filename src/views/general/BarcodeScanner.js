import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
  Alert,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import strings from '../../Language/general';
import { Header } from '../common/components/Header';
import screenTitles from '../../Language/SimChange';
import ActIndicator from '../common/components/ActivityIndicatorBarcode';
import _ from 'lodash';

const Utill = new Utills();

const VIBRATION_DURATION = 200;
const MARGIN = 50;
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const maskRowHeight = Math.round((DEVICE_HEIGHT - 300) / 5);
const maskColWidth = (DEVICE_WIDTH - 300) / 2;

class BarcodeScanner extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    screenTitles.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      locals: {
        backMessage: strings.simBackMsg,
        screenTitle: this.props.screenTitle,
        displayMessage: this.props.displayMessage,
        enterSimSerial: strings.enterSimSerial,
        enterValidSim: strings.enterValidSim,
        scanSuccess: strings.scanSuccess,
        scan_sim_serial: strings.scan_SIM_serial,
        barcode_will_scan_automatically: strings.barcode_will_scan_automatically,
        try_to_avoid_shadow: strings.try_to_avoid_shadow,
        invalid_serial_format: strings.invalid_serial_format,
        continueWithCapitalLetter: strings.continueWithCapitalLetter,
        ok: strings.ok,
        cancel: strings.cancel,
        continue: strings.continueWithCapitalLetter,
        permission_to_use_camera: 'Permission to use camera',
        permission_to_use_camera_message: 'To Scan the serial, App need your permission to use your camera',
      },
      uniqueValue: 0,
      barcodeValue: null,
      scanSucess: false,
      enterSimSerial: strings.enterSimSerial,
      enterValidSim: strings.enterValidSim,
      continue: strings.continue,
      scanSuccess: strings.scanSuccess,
      BarcodeScannerTitle: screenTitles.ScanSim,
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  forceRemount = () => {
    this.setState(({ uniqueValue }) => ({
      uniqueValue: uniqueValue + 1
    }));
  }

  componentWillReceiveProps(nextProps) {
    console.log('SIM TEST :: componentWillReceiveProps', nextProps.simApiFail);
    if (nextProps.simApiFail) {
      this.setState({ barcodeValue: '', scanSucess: false });
      this.forceRemount();
      nextProps.getSimNumberMobileAct('');
      nextProps.getMobileNumberMobileAct('');
    }

    if (nextProps.sim_number == '') {
      nextProps.resetSimApiFail();
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    Orientation.lockToPortrait()
    if (this.state.barcodeValue === null || this.state.barcodeValue.length !== 8) {
      this.props.getSimNumberMobileAct('');
    }
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.goBack()
      }
    ], { cancelable: true });
    return true;
  }

  goBack = () => {
    console.log('xxx goBack');
    this.props.resetValidateSimNumber();
    this.props.getSimNumberMobileAct('');
    this.props.getMobileNumberMobileAct('');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  onBarCodeReadCb = (e) => {
    console.log('xxx onBarCodeReadCb');
    this.props.getMobileNumberMobileAct('');
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e.data.toString().slice(-8);
    console.log(`xxx onBarCodeReadCb :${last8Digit}`);
    if (!isNaN(parseInt(last8Digit))) {
      this.setState({ barcodeValue: last8Digit, scanSucess: true });
      this.props.getSimNumberMobileAct(last8Digit);
    }
  };

  onChangeText = (value) => {
    console.log('xxx onChangeText');
    this.props.resetValidateSimNumber();
    this.props.getMobileNumberMobileAct('');
    this.setState({ barcodeValue: value });
    this.props.getSimNumberMobileAct(value);
  };

  returnView = () => {
    console.log('xxx returnView');
    if (this.state.barcodeValue === null || this.state.barcodeValue.length !== 8) {
      Utill.showAlertMsg(this.state.enterValidSim);
      return;
    }
    this.props.navigator.pop();
  };

  render() {
    let me = this;
    let continueButtonEnableStatus = false;
    if (me.props.sim_number !== '' 
    && me.state.barcodeValue !== null 
    && me.state.barcodeValue.length === 8
    && me.props.mobile_number !== '') {
      continueButtonEnableStatus = true;
    }

    let loadingIndicator;
    if (me.props.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.BarcodeScannerTitle}/>
        <RNCamera
          ref={(cam) => {
            me.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.barcodePreview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => me.onBarCodeReadCb(e)}>
          <View style={styles.maskOuterView}>       
            <View style={[{ flex: maskRowHeight  }, styles.maskRow, styles.maskFrame ]} />        
            <View style={[styles.maskCenter]}>
              <View style={[{ width: maskColWidth }, styles.maskFrame]} />
              <View style={styles.maskInner} />
              <View style={[{ width: maskColWidth }, styles.maskFrame]}/>            
            </View>
            <View style={[{ flex: maskRowHeight }, styles.maskRow, styles.maskFrame]} />
          </View>
          <View style ={styles.topMessageViewStyle}>
            <Text style ={styles.displayMessageTextStyle}>{me.state.locals.scan_sim_serial}</Text>
          </View>
          <View style ={styles.bottomMessageViewStyle}>
            <Text style ={styles.displayMessageTextStyle}>{me.state.locals.barcode_will_scan_automatically}</Text>
            <Text style ={styles.displayMessageTextStyle}>{me.state.locals.try_to_avoid_shadow}</Text>
          </View>
        </RNCamera>     
        {loadingIndicator}
        <View style={[styles.barcodeOverlay, styles.barcodeBottomOverlay]}>
          <View style={styles.containerBottomBarcode}>
            <View style={styles.barcodeInputContainer}>
              <TextInput
                style={styles.barcodeTxtInput}
                underlineColorAndroid='transparent'
                keyboardType={'numeric'}
                value={this.state.barcodeValue}
                placeholder={this.state.enterSimSerial}
                maxLength={8}
                autoFocus={false}
                returnKeyType={'done'}
                selectionColor={'yellow'}
                onChangeText={(value) => this.onChangeText(value)}/>
            </View>
            <TouchableOpacity
              style={continueButtonEnableStatus ? styles.barcodeContinueBtnEnabled : styles.barcodeContinueBtnDisabled}
              onPress={() => me.returnView()}
              disabled={!continueButtonEnableStatus || me.props.apiLoading}>
              <Text style={continueButtonEnableStatus ? styles.barcodeContinueBtnTxtEnabled : styles.barcodeContinueBtnTxtDisabled}>
                {me.state.locals.continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const Language = state.lang.current_lang;
  const sim_validation = state.sim.sim_validation;
  const apiLoading = state.mobile.mobileActLoading;
  const mobile_number = state.mobile.mobile_number;
  

  return { apiLoading, mobile_number, Language, simApiFail: state.sim.sim_api_fail, sim_number: state.mobile.sim_number, sim_validation };
};

const styles = StyleSheet.create({
  containerBarcodeScan: {
    flex: 1
  },
  barcodePreview: {
    flex: 1,
  },
  containerBottomBarcode: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackgroundColor
  },
  barcodeOverlay: {
    position: 'absolute',
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  barcodeBottomOverlay: {
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  barcodeInputContainer: {
    flex: 1,
    margin: 10,
    //backgroundColor: 'green'
  },
  barcodeTxtInput: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },

  barcodeContinueBtnEnabled: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.button.activeColor
  },
  barcodeContinueBtnDisabled: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.button.disableColor
  },
  barcodeContinueBtnTxtEnabled: {
    fontSize: Styles.button.defaultFontSize,
    textAlign: 'center',
    fontWeight: Styles.button.defaultFontWeight,
    color: Colors.button.activeTextColor,
  },

  barcodeContinueBtnTxtDisabled: {
    fontSize: Styles.button.defaultFontSize,
    textAlign: 'center',
    fontWeight: Styles.button.defaultFontWeight,
    color: Colors.button.disableTextColor,
  },

  ////////////////////
  maskOuterView: {
    position: 'absolute',
    // top: -30,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  maskInner: {
    width: DEVICE_WIDTH - MARGIN,
    backgroundColor: Colors.colorTransparent,
    borderColor:Colors.colorPureYellow,
    borderWidth: 2,
  },
  maskFrame: {
    backgroundColor: Colors.colorBlack,
  },
  maskRow: {
    width: '100%',     
  },
  maskCenter: { 
    flex: 30,
    flexDirection: 'row' 
  },

  topMessageViewStyle: {
    position: 'absolute' , 
    bottom: (DEVICE_HEIGHT / 3) * 2, 
    alignSelf: 'center',
    
  },

  bottomMessageViewStyle: {
    position: 'absolute' , 
    bottom: 135, 
    alignSelf: 'center',
    
  },

  displayMessageTextStyle: { 
    color:Colors.appBackgroundColor, 
    fontSize: 16
  },
});

export default connect(mapStateToProps, actions)(BarcodeScanner);