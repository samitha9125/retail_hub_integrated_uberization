import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Analytics from '../../utills/Analytics';
import strings from '../../Language/MobileActivaton.js';
import Utills from '../../utills/Utills';
import Orientation from 'react-native-orientation';

/**
 * NOT Using
 * 
 */

const Utill = new Utills();

class OtpModel extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      enteredOtp: '',
      otpCount: 0
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  valueChanged(number) {
    this.setState({ enteredOtp: number });
  }

  onBtnPressCancel = () => {
    let me = this;
    setTimeout(function () {
      me
        .props
        .navigator
        .dismissLightBox();
      me
        .props
        .resetMobileActivationState();
      console.log("OTP Model cancel pressed");
    }, 50);
  }

  onBtnPressContinue = () => {

    if (this.props.state.mobile.otp_validation !== null && (this.props.state.mobile.otp_validation.otp_pin == this.state.enteredOtp)) {

      this
        .props
        .navigator
        .dismissLightBox();
      this
        .props
        .resetMobileActOTPValidation();
      this
        .props
        .getOtpStatusMobileAct(true);
      Utill.showAlertMsg('OTP Validation Successful!');

    } else {
      Utill.showAlertMsg('OTP Validation Failed!');
    }

  }

  onSkipValidation = () => {}

  onSendValidation = () => {}

  otpContinue = () => {
    let me = this;

    if (this.props.state.mobile.otp_validation !== null && (this.props.state.mobile.otp_validation.otp_pin == this.state.enteredOtp)) {

      setTimeout(function () {
        Utill.showAlertMsg('OTP Validation Successful!');
        this.navigator.dismissModal({ animated: true, animationType: 'slide-down' });

        me
          .props
          .resetMobileActOTPValidation();
        me
          .props
          .getOtpStatusMobileAct(1);
      }, 1000);

      Analytics.logEvent('dsa_mobile_activation_otp_success', { customerNumber: this.props.otp_number });

    } else {

      if (this.state.otpCount == 3) {
        setTimeout(function () {
          Utill.showAlertMsg('OTP Validation Failed!');
          this.navigator.dismissModal({ animated: true, animationType: 'slide-down' });

          me
            .props
            .resetMobileActOTPValidation();
        }, 1000);
      } else {
        Utill.showAlertMsg('OTP Validation Failed!');
      }

      Analytics.logEvent('dsa_mobile_activation_otp_failed', { customerNumber: this.props.otp_number });

    }

  }

  otpCancel() {
    const me = this;
    setTimeout(function () {
      Utill.showAlertMsg('OTP Verification Skipped!');
      me
        .props
        .navigator
        .dismissLightBox();
      me
        .props
        .resetMobileActOTPValidation();
      me
        .props
        .getOtpStatusMobileAct(2);
    }, 1000);

    Analytics.logEvent('dsa_mobile_activation_otp_cancel', { customerNumber: this.props.otp_number });

  }

  onResendPin = () => {
    this.setState({
      otpCount: this.state.otpCount + 1
    });

    const data = {
      customer_msisdn: this.props.otp_number
    };

    this
      .props
      .mobileActOTPValidation(data);

    Utill.showAlertMsg('OTP Resent!');

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.topContainer}>
            <View style={styles.containerNumber}>
              <Text style={styles.mainText}>
                5 digit PIN number is sent to {Utill.numberMask(this.props.otp_number)}
              </Text>
            </View>
            <Text style={styles.mainText}>
              Enter PIN
            </Text>
            <View style={styles.containerOne}>
              <TextInput
                style={styles.numberSearchInput}
                onChangeText={(value) => this.valueChanged(value)}
                value={this.state.enteredOtp}/>
            </View>
            <View style={styles.containerResend}>
              <View>
                <Text style={styles.mainText}>
                  Didn't recieve the 5 digit PIN?
                </Text>
              </View>
              <View style={styles.bottomCantainerBtn}>
                <TouchableOpacity
                  onPress={this
                    .onResendPin
                    .bind(this)}
                  style={styles.bottomBtn}>
                  <Text style={styles.resendButton}>RESEND PIN</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.containerOne}>
              <TouchableOpacity
                style={styles.touchOpacity}
                onPress={this
                  .otpContinue
                  .bind(this)}>
                <Text style={styles.subText}>
                  VALIDATE</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity
              onPress={this
                .otpCancel
                .bind(this)}
              style={styles.bottomBtn}>
              <Text style={styles.bottomBtnTxt}>Skip Validation</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const otp_number = state.mobile.otp_number;
  return { state, otp_number };
};

const styles = StyleSheet.create({
  container: {
    width: Styles.otpModelWidth,
    height: Styles.otpModalHeight,
    backgroundColor: Colors.colorWhite,
    borderRadius: 5,
    padding: 16,
    marginTop: 25
  },

  innerContainer: {
    backgroundColor: Colors.colorWhite
  },
  topContainer: {
    backgroundColor: Colors.colorWhite
  },

  bottomCantainerBtn: {
    alignSelf: 'center'
  },

  bottomBtn: {
    width: '100%'
  },
  bottomBtnTxt: {
    alignSelf: 'center',
    color: Colors.colorBlue
  },

  mainText: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 15,
    fontWeight: '100'
  },
  containerOne: {
    alignSelf: 'center',
    paddingTop: 20,
    paddingBottom: 10,
    width: '100%'
  },
  touchOpacity: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.yellow,
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
    marginRight: 10
  },

  numberSearchInput: {
    height: 40,
    width: 270,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    marginLeft: 9
  },
  containerResend: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },

  containerNumber: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  resendButton: {
    fontSize: 15,
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10,
    marginTop: 15,
    fontWeight: '100',
    color: Colors.yellow
  },

  subText: {
    color: Colors.colorBlack,
    backgroundColor: Colors.colorBackground,
    fontWeight: 'bold'
  }

});

export default connect(mapStateToProps, actions)(OtpModel);
