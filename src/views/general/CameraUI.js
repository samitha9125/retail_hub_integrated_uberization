import React, { Component } from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
  NativeModules,
  PermissionsAndroid
} from 'react-native';

import DSCamera from '../../utills/DSCamera';

async function requestCameraPermission() {
	console.log("************** Request Camera Permission");
	
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        'title': 'Camera Permission',
        'message': 'Sales App needs access to your camera'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera")
    } else {
      console.log("Camera permission denied")
    }
  } catch (err) {
    console.warn(err)
  }
}

export default class CameraUI extends Component {
  constructor(props) {
    super(props);

	console.log("************** constructor");
	
    this.camera = null;

    this.state = {
      path: null,
      camera: {
        aspect: DSCamera.constants.Aspect.fill,
        captureTarget: DSCamera.constants.CaptureTarget.temp,
        type: DSCamera.constants.Type.back,
        orientation: DSCamera.constants.Orientation.portrait,
        flashMode: DSCamera.constants.FlashMode.off,
        captureQuality: DSCamera.constants.CaptureQuality.high,
		fixOrientation: true
      },
      isRecording: false
    };
	
	console.log("************** Going to Request Camera Permission");
	requestCameraPermission();
  }

  takePicture = () => {
    console.log('takePicture');
	
    if (this.camera) {
      this.camera.capture()
        .then((data) => {
		  console.log('Picture taken');
		  console.log(data);
		  this.setState({ path: data.uri })
		})
        .catch(err => console.error(err));
    }
  }

  startRecording = () => {
    if (this.camera) {
      this.camera.capture({ mode: DSCamera.constants.CaptureMode.video })
        .then((data) => console.log(data))
        .catch(err => console.error(err));
      this.setState({
        isRecording: true
      });
    }
  }

  stopRecording = () => {
    if (this.camera) {
      this.camera.stopCapture();
      this.setState({
        isRecording: false
      });
    }
  }

  switchType = () => {
    let newType;
    const { back, front } = DSCamera.constants.Type;

    if (this.state.camera.type === back) {
      newType = front;
    } else if (this.state.camera.type === front) {
      newType = back;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  }

  get typeIcon() {
    let icon;
    const { back, front } = DSCamera.constants.Type;

    if (this.state.camera.type === back) {
      icon = require('../../../images/camera/ic_camera_rear_white.png');
    } else if (this.state.camera.type === front) {
      icon = require('../../../images/camera/ic_camera_front_white.png');
    }

    return icon;
  }

  switchFlash = () => {
    let newFlashMode;
    const { auto, on, off } = DSCamera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      newFlashMode = on;
    } else if (this.state.camera.flashMode === on) {
      newFlashMode = off;
    } else if (this.state.camera.flashMode === off) {
      newFlashMode = auto;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode,
      },
    });
  }

  get flashIcon() {
    let icon;
    const { auto, on, off } = DSCamera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      icon = require('../../../images/camera/ic_flash_auto_white.png');
    } else if (this.state.camera.flashMode === on) {
      icon = require('../../../images/camera/ic_flash_on_white.png');
    } else if (this.state.camera.flashMode === off) {
      icon = require('../../../images/camera/ic_flash_off_white.png');
    }

    return icon;
  }

  renderCamera() {
    return (
      <DSCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          onFocusChanged={() => {}}
          onZoomChanged={() => {}}
          defaultTouchToFocus
          mirrorImage={false}
        >
        <View style={[styles.overlay, styles.topOverlay]}>
          <TouchableOpacity
            style={styles.flashButton}
            onPress={this.switchFlash}
          >
            <Image
              source={this.flashIcon}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.overlay, styles.bottomOverlay]}>
			<TouchableOpacity
                style={styles.captureButton}
                onPress={this.takePicture}
			>
			  <Image
				  source={require('../../../images/camera/ic_photo_camera_36pt.png')}
			  />
			</TouchableOpacity>
        </View>
	  </DSCamera>
    );
  }

  renderImage() {
    return (
      <View style={styles.container}>
        <Image
          source={{ uri: this.state.path }}
          style={styles.imagePreview}
        />
        <Text
          style={styles.cancel}
          onPress={() => this.setState({ path: null })}
        >Cancel
        </Text>
      </View>
    );
  }
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          animated
          hidden
        />
        {this.state.path ? this.renderImage() : this.renderCamera()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
	backgroundColor: 'white',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  imagePreview: {
    flex: 1,
	resizeMode: 'contain'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  buttonsSpace: {
    width: 10,
  },
  cancel: {
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: 'transparent',
    color: '#000000',
    fontWeight: '600',
    fontSize: 17
  },
});
