import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Orientation from 'react-native-orientation';

import imageSuccessAlert from '../../../images/common/success_msg.png';

class GenaralModelRejectApprovals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false
    };
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  onBnPress() {
    this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
    this
      .props
      .navigator
      .resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.StockRequestApproval' });

  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.container}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageSuccessAlert} style={styles.alertImageStyle} />
            </View>
            <View style={styles.sucessMessage}>
              <Text style={styles.desTxt}>Stock Reject is successful.</Text>
            </View>
            {/* <Text style={styles.titleTxtStyle}>Stock ID: {this.props.STOID}</Text> */}
            <Text style={styles.titleTxtStyle}>{this.props.message}</Text>
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity onPress={() => { this.onBnPress(); }} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'OK'}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },
  container: {
    height: Dimensions
      .get('window')
      .height * 0.5,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: Colors.colorWhite
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },

  titleTxtStyle: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 5,
    fontWeight: '100'

  },
  desTxt: {
    alignItems: 'flex-start',
    fontSize: 20,
    marginBottom: 0,
    marginLeft: 10,
    marginTop: 5,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 25
  },

  bottomBtn: {
    width: 40

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen
  },
  sucessMessage: {
    marginTop: '5%'
  }
});

export default connect(null, actions)(GenaralModelRejectApprovals);
