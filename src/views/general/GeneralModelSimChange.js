import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

import imageSuccessAlert from '../../../images/common/success_msg.png';
import imageErrorsAlert from '../../../images/common/error_msg.png';
import Orientation from 'react-native-orientation';

class GeneralModelSimChange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false
    };
  }

  

  onBnPress(props) {
    this
      .props
      .navigator
      .dismissLightBox();

    if (this.props.title == 'Success') {
      this
        .props
        .resetSimChangeState();

      this
        .props
        .navigator
        .resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    }
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.alertImageContainer}>
            {this.props.title == 'Success'
              ? <Image source={imageSuccessAlert} style={styles.alertImageStyle}/>
              : <Image source={imageErrorsAlert} style={styles.alertImageStyle}/>
            }
          </View>
          {this.props.title == 'Success'
            ? <View>
              <Text style={styles.desTxt}>Account : {this.props.account}</Text>
              <Text style={styles.desTxt}>SIM No : {this.props.sim}</Text>
              <Text style={styles.titleTxtStyle}>{this.props.content}</Text>
            </View>
            : <Text style={styles.desTxt}>{this.props.message}</Text>
          }
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity
              onPress={this
                .onBnPress
                .bind(this)}
              style={styles.bottomBtn}>
              <Text style={styles.bottomCantainerBtnTxt}>
                {'OK'}</Text>
            </TouchableOpacity>

          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions
      .get('window')
      .height * 0.5,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 10,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: Colors.colorWhite
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },

  titleTxtStyle: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 5,
    fontWeight: '100'

  },
  desTxt: {
    alignItems: 'flex-start',
    fontSize: 20,
    marginBottom: 0,
    marginLeft: 10,
    marginTop: 5,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 25
  },

  bottomBtn: {
    width: 40

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen

  }

});

export default connect(null, actions)(GeneralModelSimChange);
