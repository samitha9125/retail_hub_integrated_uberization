import React from 'react';
import { ActivityIndicator, View, StyleSheet, Text, Dimensions } from 'react-native';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';

const MARGIN = 50;
const DEVICE_HEIGHT = Dimensions
  .get('window')
  .height;

class ActIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: true,
      locals: {
        validatingTxt: 'Validating'

      }
    };
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
    this.closeActivityIndicator();
  }

  closeActivityIndicator = () => this.setState({ animating: this.props.animating });

  render() {
    const animating = this.state.animating;
    return (
      <View style={styles.container}>
        {!this.props.displayText?
          <Text style={[styles.textStyle, this.props.style]}>{this.state.locals.validatingTxt}</Text>
          : true
        }
        
        <ActivityIndicator
          animating={animating}
          color={Colors.activityIndicaterColor}
          size="large"
          style={styles.activityIndicator}/>

        {this.props.displayText? 
          <Text style={styles.customTextStyle}>{this.props.displayText}</Text>
          : true
        }
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex:1,
    flexDirection:'column',
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorTransparent,
    paddingVertical: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1000000,
    opacity: 0.5
  },
  customTextStyle: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    fontWeight: "500",
    color: Colors.colorBlack,
    // paddingVertical: 200,
    zIndex: 1000,
    // backgroundColor: 'blue',
    // paddingTop: 100,
    marginTop: 280,
    height: 30 
  },
  textStyle: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:  'center',
    fontWeight: "500",
    fontSize: 20,
    color: Colors.colorPureYellow,
    paddingVertical: 17,
    top: DEVICE_HEIGHT/2,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1000000
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  }
});

export default ActIndicator;
