import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  StatusBar,
  BackHandler,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import Camera, { constants } from '../../utills/DSCamera';
import * as actions from '../../actions';

import CameraImagrSrc from '../../../images/other_images/camera/ic_photo_camera_36pt.png';
import FlashOnImagrSrc from '../../../images/other_images/camera/ic_flash_on_white.png';
import FlashOffImagrSrc from '../../../images/other_images/camera/ic_flash_off_white.png';
import FlashAutoImagrSrc from '../../../images/other_images/camera/ic_flash_auto_white.png';
import strings from '../../Language/general';
import Colors from '../../config/colors';
import { Header } from '../../components/others';

class CameraReCaptureScreen extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      userLogged: true,
      userData: '',
      cameraBackMsg: strings.cameraBackMsg,
      CameraPreviewTitle: '',
      camera: {
        aspect: constants.Aspect.fill,
        captureTarget: constants.CaptureTarget.temp,
        type: constants.Type.back,
        orientation: constants.Orientation.portrait, //portrait,landscape
        flashMode: constants.FlashMode.off,
        captureQuality: constants.CaptureQuality.high,
        fixOrientation: true
      },

      title: {
        nic_front: strings.nic_front,
        nic_back: strings.nic_back,
        passport: strings.passport,
        driving_licence: strings.driving_licence,
        proof_of_billing: strings.proof_of_billing,
        cusomter_image: strings.cusomter_image,
        signature: strings.nic_frosignaturent,
        ack_form: strings.ack_form
      },

      continue: strings.continue,
      reScan: strings.reScan,

      renderView: 2,
      imageSrc: 'default_thumbnail',
      captureType: this.props.information_id_type,
      captureTypeNo: this.props.kyc_capture_type_no,
      kycImage: this.props.kyc_image,
      pobImage: this.props.pob_image,
      customerImage: this.props.customerImage
    };
  }

  componentWillMount() {
    console.log(`information_id_type :${this.props.information_id_type}`);
    console.log(`kyc_capture_type_no :${this.props.kyc_capture_type_no}`);
    this.changeTitle(this.props.kyc_capture_type_no);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  componentDidUpdate() {
    this.changeTitle(this.state.captureTypeNo);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
  }

    onHandleBackButton = () => {
      console.log('xxxx onHandleBackButton');
      Alert.alert('', this.state.cameraBackMsg, [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => this.goBack()
        }
      ], { cancelable: true });
      return true;
    }

    goBack = () => {
      console.log('xxx goBack');
      const navigatorOb = this.props.navigator;
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    }

    changeTitle = (key) => {
      let titleName;
      const naviGater = this.props.navigator;
      switch (key) {
        case 1:
          titleName = this.state.title.nic_front;
          break;
        case 2:
          titleName = this.state.title.nic_back;
          break;
        case 3:
          titleName = this.state.title.passport;
          break;
        case 4:
          titleName = this.state.title.driving_licence;
          break;
        case 5:
          titleName = this.state.title.proof_of_billing;
          break;
        case 6:
          titleName = this.state.title.cusomter_image;
          break;
        case 7:
          titleName = this.state.title.ack_form;
          break;
        default:
          titleName = '';
          break;
      }
      
      if (this.state.CameraPreviewTitle !== titleName){
        this.setState({ CameraPreviewTitle: titleName });
      }

      naviGater.setTitle({ title: titleName });
    }

    continueAction = () => {
      console.log('xxx continueAction');
      const props = this.props;
      const kycOb = {
        captureType: this.state.captureTypeNo,
        imageUri: this.state.imageSrc
      };

      switch (this.state.captureTypeNo) {
        case 1:
          props.getKycNic1CaptureMobileAct(kycOb);
          break;
        case 2:
          props.getKycNic2CaptureMobileAct(kycOb);
          break;
        case 3:
          props.getKycCaptureMobileAct(kycOb);
          break;
        case 4:
          props.getKycCaptureMobileAct(kycOb);
          break;
        case 5:
          props.getKycPobCaptureMobileAct(kycOb);
          break;
        case 6:
          props.getKycCustomerCaptureMobileAct(kycOb);
          break;
        case 7:
          props.getKycCaptureMobileAct(kycOb);
          break;
        default:
          break;
      }

      this.popScreen();
    }

    popScreen = () => {
      this
        .props
        .navigator
        .pop({ animated: true, animationType: 'fade' });
    }

    reScan = () => {
      console.log('xxx reScan');
      this.setState({ renderView: 2 });
    }
    takePicture = () => {
      console.log('xxx takePicture');

      if (this.camera) {
        this
          .camera
          .capture()
          .then((data) => {
            console.log(data);
            this.setState({ renderView: 1, imageSrc: data.uri });
          })
          .catch(err => console.log(err));
      }
    }

    switchFlash = () => {
      let newFlashMode;
      const { auto, on, off } = constants.FlashMode;

      if (this.state.camera.flashMode === auto) {
        newFlashMode = on;
      } else if (this.state.camera.flashMode === on) {
        newFlashMode = off;
      } else if (this.state.camera.flashMode === off) {
        newFlashMode = auto;
      }

      this.setState({
        camera: {
          ...this.state.camera,
          flashMode: newFlashMode
        }
      });
    }

    get flashIcon() {
      let icon;
      const { auto, on, off } = constants.FlashMode;

      if (this.state.camera.flashMode === auto) {
        icon = FlashAutoImagrSrc;
      } else if (this.state.camera.flashMode === on) {
        icon = FlashOnImagrSrc;
      } else if (this.state.camera.flashMode === off) {
        icon = FlashOffImagrSrc;
      }

      return icon;
    }

    handleClick() {
      console.log("===> Button Tapped \n ") ;
      console.log(JSON.stringify(this.props));
      console.log(JSON.stringify(this.state));
      this.onHandleBackButton();
    }

    render() {
      // <View>
      switch (this.state.renderView) {
        case 1:
          return (
            <View style={styles.containerPreview}>
              <Header backButtonPressed={() => this.handleClick()} headerText={this.state.CameraPreviewTitle}/>
              <View style={styles.containerTop}>
                <Image
                  style={styles.previewImage}
                  source={{
                    uri: this.state.imageSrc
                  }}/>
              </View>
              <View style={styles.containerBottom}>
                <TouchableOpacity style={styles.reScanBtn} onPress={() => this.reScan()}>
                  <Text style={styles.reScanBtnBtnTxt}>
                    {this.state.reScan}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.continueBtn}
                  onPress={() => this.continueAction()}>
                  <Text style={styles.continueBtnTxt}>
                    {this.state.continue}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        case 2:
          return (
            <View style={styles.containerCamera}>
              <Header backButtonPressed={() => this.handleClick()} headerText={this.state.CameraPreviewTitle}/>
              <StatusBar animated hidden/>
              <Camera
                ref={(cam) => {
                  this.camera = cam;
                }}
                style={styles.preview}
                aspect={this.state.camera.aspect}
                captureTarget={this.state.camera.captureTarget}
                type={this.state.camera.type}
                flashMode={this.state.camera.flashMode}
                onFocusChanged={() => {}}
                onZoomChanged={() => {}}
                defaultTouchToFocus
                mirrorImage={false}/>
              <View style={[styles.overlay, styles.topOverlay]}>
                <TouchableOpacity style={styles.flashButton} onPress={this.switchFlash}>
                  <Image source={this.flashIcon}/>
                </TouchableOpacity>
              </View>
              <View style={[styles.overlay, styles.bottomOverlay]}>
                <TouchableOpacity style={styles.captureButton} onPress={this.takePicture}>
                  <Image source={CameraImagrSrc}/>
                </TouchableOpacity>
              </View>
            </View>
          );

        default:
          return (
            <View style={styles.containerPreview}>
              <Header backButtonPressed={() => this.handleClick()} headerText={this.state.CameraPreviewTitle}/>

              <View style={styles.containerTop}>
                <Image
                  style={styles.previewImage}
                  source={{
                    uri: this.state.imageSrc
                  }}/>
              </View>
              <View style={styles.containerBottom}>
                <TouchableOpacity style={styles.reScanBtn} onPress={() => this.reScan()}>
                  <Text style={styles.reScanBtnBtnTxt}>
                                    Rescan
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.continueBtn}
                  onPress={() => this.continueAction()}>
                  <Text style={styles.continueBtnTxt}>
                                    Continue
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          );
      }
    }
}

const mapStateToProps = state => {
  const information_id_type = state.mobile.information_id_type;
  const kyc_capture_type_no = state.mobile.kyc_capture_type_no;
  const kyc_image = state.mobile.kyc_capture;
  const pobImage = state.mobile.kyc_pob_capture;
  const customerImage = state.mobile.kyc_customer_capture;
  const Language = state.lang.current_lang;
  return {
    information_id_type,
    kyc_capture_type_no,
    kyc_image,
    pobImage,
    customerImage,
    Language
  };
};

const styles = StyleSheet.create({
  ////////Camera Module ////
  containerCamera: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: Colors.cameraOverlayColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  captureButton: {
    padding: 15,
    backgroundColor: Colors.colorWhite,
    borderRadius: 40
  },
  ///// NIC capture Module ////
  containerPreview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.cameraPreviewBackgroundColor
  },
  containerTop: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackgroundColor
  },
  previewImage: {
    margin: 5,
    width: 350,
    height: 450,
    resizeMode: 'contain'
  },
  reScanBtn: {
    flex: 1,
    margin: 10
  },
  continueBtn: {
    flex: 1,
    margin: 10
  },
  reScanBtnBtnTxt: {
    fontSize: 20,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  continueBtnTxt: {
    fontSize: 20,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, actions)(CameraReCaptureScreen);
