import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

import imageErrorsAlert from '../../../images/common/error_msg.png';
import Orientation from 'react-native-orientation';

class GeneralExceptionModal extends Component {
  constructor(props) {
    super(props);
  }

  onBnPress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageErrorsAlert} style={styles.alertImageStyle}/>

            </View>
            <View>
              <Text style={styles.descriptionText}>{this.props.message}</Text>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity onPress={() => this.onBnPress()} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'OK'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    backgroundColor: Colors.backgroundColorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },
  descriptionText: {
    alignItems: 'flex-start',
    fontSize: Styles.modal.errorDescrptionFontSize,
    color: Colors.colorBlack,
    marginBottom: 20,
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'justify'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtn: {
    width: 45

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen

  }

});

export default GeneralExceptionModal;
