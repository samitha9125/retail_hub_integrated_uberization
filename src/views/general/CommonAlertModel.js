import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Orientaton from 'react-native-orientation';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import imageAtentionIcon from '../../../images/common/alert.png';
import imageErrorIcon from '../../../images/common/error_msg.png';
import imageSucessIcon from '../../../images/common/success_msg.png';
import strings from '../../Language/Wom';
class CommonAlertModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      messageHeader: this.props.messageHeader,
      messageBody: this.props.messageBody,
      messageFooter: this.props.messageFooter,
      apiData: {},
      locals: {
        serialno: strings.serialno
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentWillMount(){
    Orientaton.lockToPortrait()
  }

  componentWillUnmount(){
    Orientaton.lockToPortrait()
  }

  onBtnPressCancel = () => {
    if (this.props.cancelBtn) {
      this.props.cancelBtn();
    }
    this.navigatorOb.pop();
  }

  onBtnPressOk = () => {
    if (this.props.onPressOK) {
      this.props.onPressOK();
    }
    this.navigatorOb.pop();
  }

  onSuccessAlertBtnPressOk = () => {
    this.props.onPressOK && this.props.onPressOK !== undefined ? this.props.onPressOK() : true;
    this.navigatorOb.pop();
  }

  onBtnPressOkDefault = () => {
    this.navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.HomeTileScreen',
      passProps: {

      },
      overrideBackPress: true,
    });
  }

  onPressBtn() {
    this.props.btnFun ? this.props.btnFun() : true;
    this.navigatorOb.pop();
  }

  onPressRetry() {
    this.props.onPressRetry ? this.props.onPressRetry() : true;
    this.navigatorOb.pop();
  }

  renderListItem = ({ item }) => (
    <View>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.provisionText}>{item.key}</Text>
        <Text style={styles.provisionText}>{this.state.locals.serialno}</Text>
        <Text style={styles.provisionText}>{item.value}</Text>
      </View>
    </View>
  );

  renderAdditionalInformation = ({ item }) => {
    return (
      <View style={styles.additionalInformationRow}>
        <Text style={styles.additionalInformationText}>{item.label}</Text>
        <Text style={styles.additionalInformationText}>: {item.value}</Text>
      </View>
    );
  }

  render() {
    let alertIcon;
    let display_msg;
    let descriptionContainer;
    let alertIconView;
    let bottomContainer1;
    let bottomContainer2;
    if (this.props.messageType == 'error') {
      alertIcon = imageErrorIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.descriptionText}>{this.state.messageBody}</Text>
          <Text style={styles.descriptionText}>{this.state.messageFooter}</Text>
        </View>
      );

      bottomContainer1 = (
        <View />
      );
      bottomContainer2 = (
        <TouchableOpacity onPress={() => this.onBtnPressOk()} style={styles.bottomBtn}>
          <Text style={styles.bottomCantainerBtnTxtWarning}>
            {'OK'}</Text>
        </TouchableOpacity>
      );
    } else if (this.props.messageType == 'success') {
      alertIcon = imageSucessIcon;
      // display_msg = this.props.info;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.descriptionTextSuccess}>{this.state.messageBody}</Text>
          <Text style={styles.descriptionText}>{this.state.messageFooter}</Text>
        </View>
      );

      bottomContainer1 = (
        <TouchableOpacity onPress={() => this.onBtnPressCancel()} style={styles.bottomBtn}>
          <Text style={styles.bottomCantainerBtnTxtRegular}>
            {'CANCEL'}</Text>
        </TouchableOpacity>
      );
      bottomContainer2 = (

        <TouchableOpacity onPress={() => this.onBtnPressOk()} style={styles.bottomBtn}>
          <Text style={styles.bottomCantainerBtnTxtSuccess}>
            {'REPLACE'}</Text>
        </TouchableOpacity>
      );
    } else if (this.props.messageType == 'alert') {

      alertIcon = imageAtentionIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <View>
            <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          </View>
          {this.props.showAdditionalInformation ?
            <View style={{ margin: 10 }}>
              <FlatList
                data={this.props.additionalInformation}
                renderItem={this.renderAdditionalInformation}
                scrollEnabled={true}
                extraData={this.state}
              />
            </View>
            : null}
          <View>
            <Text style={styles.mainText}>{this.state.messageBody}</Text>
          </View>
          {/* <Text style={styles.descriptionText}>{this.state.messageFooter}</Text> */}
        </View>
      );

      bottomContainer1 = (
        <View style={styles.cancelCallRow}>
          <View style={styles.cancelView}>
            <TouchableOpacity
              onPress={() => this.onBtnPressCancel()}
            >
              <Text style={styles.noButModal}>NO</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity
              onPress={() => this.onPressBtn()}
            >
              <Text style={styles.yesButModal}>YES</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
      bottomContainer2 = (
        <View></View>
      );
    } else if (this.props.messageType == 'alertWithOK') {
      alertIcon = imageAtentionIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.descriptionText}>{this.state.messageBody}</Text>
          <Text style={styles.descriptionText}>{this.state.messageFooter}</Text>
        </View>
      );

      bottomContainer1 = (
        <View />
      );
      bottomContainer2 = (
        <TouchableOpacity onPress={() => this.onBtnPressOk()} style={styles.bottomBtn}>
          <Text style={styles.bottomCantainerBtnTxtWarning}>
            {'OK'}</Text>
        </TouchableOpacity>
      );
    } else if (this.props.messageType == 'successWithOk') {
      alertIcon = imageSucessIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.descriptionText}>{this.state.messageBody}</Text>
          <Text style={styles.descriptionText}>{this.state.messageFooter}</Text>
        </View>
      );

      bottomContainer1 = (
        <View />
      );
      bottomContainer2 = (
        <TouchableOpacity onPress={() => this.onSuccessAlertBtnPressOk()} style={styles.bottomBtn}>
          <Text style={styles.bottomCantainerBtnTxtWarning}>
            {'OK'}</Text>
        </TouchableOpacity>
      );
    } else if (this.props.messageType == 'alertNoHeader') {

      alertIcon = imageAtentionIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
        </View>
      );

      bottomContainer1 = (
        <View style={styles.cancelCallRow}>
          <View style={styles.cancelView}>
            <TouchableOpacity
              onPress={() => this.onBtnPressCancel()}
            >
              <Text style={styles.noButModal}>NO</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity
              onPress={() => this.onPressBtn()}
            >
              <Text style={styles.yesButModal}>YES</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
      bottomContainer2 = (
        <View></View>
      );
    } else if (this.props.messageType == 'defaultAlert') {

      alertIcon = imageErrorIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.mainText}>{this.state.messageBody}</Text>
          {/* <Text style={styles.descriptionText}>{this.state.messageFooter}</Text> */}
        </View>
      );

      bottomContainer1 = (
        <View style={styles.cancelCallRow}>
          <View style={styles.cancelView}>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity
              onPress={() => {
                this.props.onPressOK ? this.props.onPressOK() : true;
                this.props.navigator.pop();
              }}
            >
              <Text style={styles.yesButModal}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
      bottomContainer2 = (
        <View></View>
      );
    } else if (this.props.messageType == 'provisionFailed') {
      alertIcon = imageErrorIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <FlatList style={styles.background}
            data={this.state.messageBody}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => item.toString() && index.toString()}
            scrollEnabled={true}
          />
        </View>
      );

      bottomContainer1 = (
        <View style={styles.cancelCallRow}>
          <View style={styles.cancelView}>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity
              onPress={() => {
                this.props.onPressOK ? this.props.onPressOK() : true;
                this.props.navigator.pop();
              }}
            >
              <Text style={styles.yesButModal}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>

      );

    } else if (this.props.messageType == 'failureWithRetry') {
      alertIcon = imageErrorIcon;

      alertIconView = (
        <View style={styles.alertImageContainer}>
          <Image source={alertIcon} style={styles.alertImageStyle} />
        </View>
      );

      descriptionContainer = (
        <View style={styles.descriptionContainer}>
          <Text style={styles.mainText}>{this.state.messageHeader}</Text>
          <Text style={styles.mainText}>{this.state.messageBody}</Text>
        </View>
      );

      bottomContainer1 = (
        <View style={styles.cancelCallRow}>
          <View style={styles.cancelView}>
            <TouchableOpacity
              onPress={() => this.onBtnPressCancel()}
            >
              <Text style={styles.noButModal}>CANCEL</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity
              onPress={() => this.onPressRetry()}
            >
              <Text style={styles.yesButModal}>RETRY</Text>
            </TouchableOpacity>
          </View>
        </View>

      );
      bottomContainer2 = (
        <View></View>
      );
    }

    return (
      <View style={styles.containerOverlay}>
        {this.props.isLongPopup ? (
          <View style={styles.modalContainerLong}>
            {alertIconView}
            {descriptionContainer}
            <View style={styles.bottomCantainerBtn}>
              {bottomContainer1}
              {bottomContainer2}
            </View>
          </View>
        ) : (
            <View style={styles.modalContainer}>
              {alertIconView}
              {descriptionContainer}
              <View style={styles.bottomCantainerBtn}>
                {bottomContainer1}
                {bottomContainer2}
              </View>
            </View>
          )}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => GeneralAlert\n', state.delivery);
  const Language = state.lang.current_lang;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const warrantyReplacement = state.warrantyReplacement;
  return { Language, currentWorkOrderId, warrantyReplacement };
};

const styles = StyleSheet.create({
  cancelCallRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingBottom: 15
  },
  cancelView: {
    marginRight: 25
  },
  yesButModal: {
    fontSize: 18,
    color: '#ff8329'
  },
  noButModal: {
    fontSize: 18,
    color: '#000000'
  },
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor,
    justifyContent: 'center',
  },
  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    height: 'auto',
    paddingBottom: 17,
    paddingTop: 21,
    paddingHorizontal: 13,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    marginHorizontal: 40
  },
  modalContainerLong: {
    height: 'auto',
    paddingBottom: 17,
    paddingTop: 21,
    paddingHorizontal: 13,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    marginHorizontal: 40
  },
  bottomContainer: {
    flex: 0.6
  },

  innerContainer: {
    flex: 1,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    // flex: 0.7,
    alignItems: 'center',
    // padding: 5
  },

  alertImageStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    // marginTop: 5,
    // marginBottom: 5
  },

  descriptionContainer: {
    marginVertical: 24,
    // flex: 1,
    // flexDirection: 'column',
    // paddingTop: 10,
    // paddingRight: 5,
    // paddingLeft: 5,
    // paddingBottom: 5,
    // marginTop: 10
  },
  descriptionText: {
    fontSize: 15,
    textAlign: 'left'
    // marginBottom: 5,
    // marginLeft: 10,
    // marginRight: 10,
    // marginTop: 5,
    // fontWeight: '100'

  },
  mainText: {
    // alignItems: 'flex-start',
    // fontSize: Styles.delivery.modalFontSize,
    fontSize: 16,
    textAlign: 'left',
    color: Colors.colorBlack,
    // paddingTop: 5,
    // marginLeft: 10,
    marginBottom: 15,
    // paddingBottom: 5,
    // marginRight: 10,
    // marginTop: 10,
    // fontWeight: 'bold'
  },

  mainTextSuccess: {
    // alignItems: 'flex-start',
    fontSize: 16,
    textAlign: 'left',
    // color: Colors.colorBlack,
    // paddingTop: 5,
    // marginLeft: 10,
    // marginBottom: 15,
    // paddingBottom: 5,
    // marginRight: 10,
    // marginTop: 20,
    // fontWeight: 'bold'
  },

  descriptionTextSuccess: {
    fontSize: 16,
    // marginBottom: 5,
    // marginLeft: 10,
    // marginRight: 10,
    // marginTop: 5,
    textAlign: 'left',
    fontWeight: '100'

  },

  bottomCantainerBtn: {
    // flex: 1,
    // flexDirection: 'row',
    flexDirection: 'row',
    // alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // paddingTop: 5,
    // paddingRight: 5,
    // paddingBottom: 5,
    // marginTop: 10
  },

  dummyView: {
    flex: 2,
    // justifyContent: 'center',
    // borderWidth: 1,
  },
  bottomBtn: {
    height: 36,
    width: 75,
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1,
    // marginRight: 10,
    // justifyContent: 'center'
  },
  bottomCantainerBtnTxtRegular: {
    // fontSize: Styles.delivery.modalFontSize,
    fontSize: 15,
    // alignSelf: 'center',
    textAlign: 'center',
    color: Colors.colorBlack
  },
  bottomCantainerBtnTxtSuccess: {
    // fontSize: Styles.delivery.modalFontSize,
    fontSize: 15,
    // alignSelf: 'center',
    textAlign: 'center',
    color: Colors.colorGreen
  },
  bottomCantainerBtnTxtWarning: {
    // fontSize: Styles.delivery.modalFontSize,
    fontSize: 15,
    // alignSelf: 'center',
    textAlign: 'center',
    color: Colors.colorDarkOrange
  },
  provisionText: {
    fontSize: 16,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: 'bold',
    paddingRight: 5,
    marginBottom: 10,

  },
  additionalInformationText: {
    flex: 1,
    fontSize: 14,
    marginVertical: 5,
    fontWeight: 'bold'
  },
  additionalInformationRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
});

export default connect(mapStateToProps, actions)(CommonAlertModel)
