import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import imageAtentionIcon from '../../../images/common/alert.png';



class GeneralConfirm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      apiData: {},
      locals: {
      }
    };
  }

  onBnPressYes = () => {
    this.setState({ isLoading: false });
  }

  onBnPressCancel = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageAtentionIcon} style={styles.alertImageStyle} />
            </View>
            <View style={styles.descriptionContainer}>
              {this.state.isLoading
                ? <ActIndicator animating />
                : <View />}
              <View>
                <Text style={styles.stockId}>{this.props.Header}</Text>
              </View>
              <View >
                <Text style={styles.descriptionText}>{this.props.descriptionText}</Text>
              </View>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <View style={styles.dummyView} />
              <TouchableOpacity
                onPress={() => this.onBnPressCancel()}
                style={styles.bottomBtnCancel}>
                <Text style={styles.bottomCantainerCancelBtnTxt}>
                  {'NO'}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onBnPressYes()}
                style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'YES'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    flex: 1.5
  },

  bottomContainer: {
    flex: 0.3
  },

  innerContainer: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    flex: 0.7,
    alignItems: 'center',
    padding: 5
  },

  alertImageStyle: {
    width: 90,
    height: 90,
    marginTop: 5,
    marginBottom: 5
  },

  descriptionContainer: {
    flex: 1.5,
    flexDirection: 'column',
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 15,
    marginTop: 10
  },
  descriptionText: {
    fontSize: Styles.delivery.modalFontSize,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontWeight: '100'

  },
  stockId: {
    alignItems: 'flex-start',
    fontSize: Styles.delivery.modalFontSize,
    textAlign: 'left',
    color: Colors.colorBlack,
    paddingTop: 5,
    marginLeft: 10,
    marginBottom: 15,
    paddingBottom: 5,
    marginRight: 10,
    marginTop: 20,
    fontWeight: 'bold'
  },

  bottomCantainerBtn: {
    flex: 0.4,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 10
  },

  dummyView: {
    flex: 3,
    justifyContent: 'center'
  },

  bottomBtnCancel: {
    flex: 1,
    justifyContent: 'center'
  },
  bottomBtn: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'center'
  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.green
  },
  bottomCantainerCancelBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorBlack
  }

});

export default connect(mapStateToProps)(GeneralConfirm);