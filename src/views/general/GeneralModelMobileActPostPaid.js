import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Picker,
  Alert,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Orientation from 'react-native-orientation';
import strings from '../../Language/MobileActivaton.js';
/**
 * Curently using for both prepaid and postpaid
 *
 * 'DialogRetailerApp.models.GeneralModelMobileActPostPaid'
 */

const Utill = new Utills();

class GeneralModelMobileAct extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      openModel: false,
      otpMobileNumber: '',
      pendingState: 0,
      locals: {
        customer_already_with_dialog: strings.customer_already_with_dialog,
        select_mobile_number: strings.select_mobile_number,
        send_validation: strings.send_validation,
        skip_validation: strings.skip_validation,
        msg_please_select_no: strings.msg_please_select_no,
        otp_select_a_number: strings.otp_select_a_number
      }
    };
    this.navigator = this.props.navigator;
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }
  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentWillReceiveProps(nextProps) {
    const navigatorOB = this.props.navigator;
    if (nextProps.otp_validation !== null && nextProps.otp_validation.success == true && !nextProps.skip_validation && nextProps.model_popped && !nextProps.otp_popped) {
      console.log('xxxxxxxxxxxxxxxxxx OTP TRIGGERED-POSTPAID xxxxxxxxxxxxxxxxxx');
      navigatorOB.dismissModal({ animated: true, animationType: 'slide-down' });
      this
        .props
        .mobileActOtpPopped(true);
      navigatorOB.showModal({ title: 'MobileActModel', screen: 'DialogRetailerApp.models.MobileActOtpModal', overrideBackPress: true });
    } else if (nextProps.m_connect_validation !== null && nextProps.m_connect_validation.success == true && !nextProps.skip_validation && !nextProps.m_connect_popped) {
      this.setState({ pendingState: 1 });
      this
        .props
        .mobileActMConnectPopped(true);
      navigatorOB.dismissModal({ animated: true, animationType: 'slide-down' });
      Alert.alert('M CONNECT', 'Verify using M Connect', [
        {
          text: 'VERIFY',
          onPress: () => this.onMConnectVerify()
        }
      ], { cancelable: false });
    } else {
      //DO nothing
    }
  }

  //Not Using - m-Connect
  onMConnectVerify = () => {
    const data = {
      auth_req_id: this.props.m_connect_validation.auth_req_id
    };

    Utill.apiRequestPost('mConnectStatus', 'account', 'ccapp', data, this.mConnectSuccess, this.mConnectFailure);
  }

  //Not Using - m-Connect
  mConnectSuccess = (response) => {
    if (response.data.data.status == 1) {
      Alert.alert(JSON.stringify('MConnect Verified!'));
      this
        .props
        .getOtpStatusMobileAct(3);
      this
        .props
        .resetMobileActMConnectValidation();
    } else if (response.data.data.status == 2) {

      if (this.props.pre_post == 'pre') {
        this
          .props
          .resetMobileActivationState();
      } else {
        this
          .props
          .resetMobileActivationState();
        const me = this;
        setTimeout(() => {
          console.log('CHANGING POST');
          me
            .props
            .getPrePostMobileAct('post');
        }, 50);
      }

      Alert.alert(JSON.stringify('MConnect Verification failed!'));
    } else if (response.data.data.status == 0) {
      this.setState({
        pendingState: this.state.pendingState + 1
      });

      if (this.state.pendingState <= 3) {
        Alert.alert('M CONNECT', 'Verify using M Connect', [
          {
            text: 'VERIFY',
            onPress: () => this.onMConnectVerify()
          }
        ], { cancelable: false });
      } else {
        Alert.alert(JSON.stringify('MConnect Verification failed!'));
        if (this.props.pre_post == 'pre') {
          this
            .props
            .resetMobileActivationState();
        } else {
          this
            .props
            .resetMobileActivationState();
          const me = this;
          setTimeout(() => {
            console.log('CHANGING POST');
            me
              .props
              .getPrePostMobileAct('post');
          }, 50);

        }
      }
      // Alert.alert(JSON.stringify('MConnect Verification Pending!'));
    }
  }

  //Not Using - m-Connect
  mConnectFailure = (response) => {
    if (this.props.pre_post == 'pre') {
      this
        .props
        .resetMobileActivationState();
    } else {
      this
        .props
        .resetMobileActivationState();
      const me = this;
      setTimeout(() => {
        console.log('CHANGING POST');
        me
          .props
          .getPrePostMobileAct('post');
      }, 50);
    }
    Alert.alert(JSON.stringify(response.data.error));
  }

  onSkipValidation = () => {
    this
      .props
      .getOtpStatusMobileAct(2);

    this
      .navigator
      .dismissModal({ animated: true, animationType: 'slide-down' });

    this
      .props
      .getSkipValidationMobileAct(true);
  }

  onSendValidation = () => {
    if (this.props.otp_number === 'Select a Number') {
      Utill.showAlertMsg(this.state.locals.msg_please_select_no);
    } else {
      //otp
      const data = {
        customer_msisdn: this.props.otp_number
      };

      this
        .props
        .mobileActOTPValidation(data);

      this
        .props
        .getOtpStatusMobileAct(1);
    }

  }

  valueChanged = (number) => {
    this
      .props
      .getOTPNumberMobileAct(number);

    this.setState({ otpMobileNumber: number });
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.textViewContainer}>
              {/* Top text view */}
              <Text style={styles.mainText}>
                {this.state.locals.customer_already_with_dialog}
              </Text>
              <Text style={styles.mainText}>
                {this.state.locals.select_mobile_number}
              </Text>
            </View>
            {/* Number selection Dropdown*/}
            <View style={styles.pickerViewContainer}>
              <Picker
                style={styles.picker}
                selectedValue={this.state.otpMobileNumber}
                mode={'dropdown'}
                onValueChange={this.valueChanged}>
                <Picker.Item
                  label={this.state.locals.otp_select_a_number}
                  value={'Select a Number'}/>{this.props.numberList == undefined || this.props.numberList == null
                  ? <View/>
                  : this
                    .props
                    .numberList
                    .map(number => (<Picker.Item key={number} label={Utill.numberMask(number)} value={number}/>))}
              </Picker>
            </View>
            {/* Send validation button */}
            <View style={styles.validationButtonContainer}>
              <TouchableOpacity
                style={styles.validationBtn}
                onPress={this.onSendValidation}>
                <Text style={styles.validationBtnText}>
                  {this.state.locals.send_validation}</Text>
              </TouchableOpacity>
            </View>
            {/* Skip validation button */}
            <View style={styles.skipViewContainer}>
              <TouchableOpacity onPress={this.onSkipValidation} style={styles.skipBtn}>
                <Text style={styles.skipBtnTxt}>
                  {this.state.locals.skip_validation}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const mapStateToProps = state => {
  let numberList;
  const language = state.lang.current_lang;
  const otp_validation = state.mobile.otp_validation;
  const local_foreign = state.mobile.local_foreign;
  const pre_post = state.mobile.pre_post;
  const m_connect_validation = state.mobile.m_connect_validation;
  const otp_number = state.mobile.otp_number;
  const model_popped = state.mobile.model_popped;
  const otp_popped = state.mobile.otp_popped;
  const m_connect_popped = state.mobile.m_connect_popped;
  if (state.mobile.nic_validation !== null) {
    numberList = state.mobile.nic_validation.notificationNumberList;
  } else {
    numberList = [
      {
        arr: 1
      }
    ];
  }
  return {
    state,
    language,
    numberList,
    otp_validation,
    m_connect_validation,
    model_popped,
    m_connect_popped,
    otp_number,
    otp_popped,
    local_foreign,
    pre_post
  };
};

const styles = StyleSheet.create({
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.5
  },
  modalContainer: {
    flex: 1,
    //width: 450,
    width: Dimensions
      .get('window')
      .width * 0.95,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    flex: 0.6
  },

  innerContainer: {
    //flex: 1,
    height: 280,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 5,
    paddingTop: 10,
    paddingHorizontal: 10,
    marginLeft: 10,
    marginRight: 10
  },

  textViewContainer: {
    //flex: 1,
    height: 100,
    marginLeft: 5,
    marginRight: 5,
    //backgroundColor: Colors.colorGreen
  },
  pickerViewContainer: {
    //flex: 0.5,
    height: 60,
    //backgroundColor: Colors.colorPureYellow
  },
  validationButtonContainer: {
    //flex: 0.7,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: Colors.colorGreen
  },
  skipViewContainer: {
    //flex: 0.2,
    height: 26,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: Colors.colorGreen //TODO
  },
  mainText: {
    textAlign: 'left',
    fontSize: Styles.otpModalFontSize,
    marginTop: 10,
    fontWeight: '100'
  },
  picker: {
    flex: 1,
    margin: 0,
    marginTop: 5,
    padding: 0,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1
  },

  skipBtn: {
    flex: 1,
    padding: 10,
    paddingHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: Colors.colorGreen //TODO
  },
  skipBtnTxt: {
    textDecorationLine: 'underline',
    color: Colors.colorBlue,
    textAlign: 'center'
  },

  validationBtn: {
    //flex: 1,
    margin: 5,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Colors.colorYellow,
    height: 45,
    borderRadius: 5
  },

  validationBtnText: {
    color: Colors.colorBlack,
    margin: 5,
    backgroundColor: Colors.colorBackground
  }

});

export default connect(mapStateToProps, actions)(GeneralModelMobileAct);
