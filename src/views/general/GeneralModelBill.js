import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import imageSuccessAlert from '../../../images/common/success_msg.png';
import imageErrorsAlert from '../../../images/common/error_msg.png';
import colors from '../../config/colors';
import Orientation from 'react-native-orientation';

class GeneralModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false
    };
  }

  onBnPress(props) {

    this.props.navigator.dismissLightBox();
        
    if (this.props.title == 'Success'){
      this.props.resetBillPaymentState();
      this.props.navigator.resetTo({
        title: 'Dialog Sales App', 
        screen: 'DialogRetailerApp.views.HomeTileScreen' 
      });
    }
        
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    console.log(this.props);
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <View style={styles.alertImageContainer}>
            {this.props.status == 'Success'? 
              <Image source={imageSuccessAlert} style={styles.alertImageStyle} />:
              <Image source={imageErrorsAlert} style={styles.alertImageStyle} />
            } 
          </View>
          {this.props.title == 'Success'?
            <View>
              <Text style={styles.desTxt}>Account: {this.props.title}</Text>
              <Text style={styles.titleTxtStyle}><Text style={styles.amountStyle}>Rs {this.props.amount} </Text>{this.props.content}</Text>
            </View>:    
            <Text style={styles.desTxt}>{this.props.message}</Text>
          }
          <View style={styles.bottomCantainerBtn}>
            <TouchableOpacity onPress={this.onBnPress.bind(this)} style={styles.bottomBtn}>
              <Text style={styles.bottomCantainerBtnTxt}>
                {'OK'}</Text>
            </TouchableOpacity>

          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions
      .get('window')
      .height * 0.5,
    backgroundColor: colors.white,
    //borderRadius: 5,
    //paddingLeft: 18,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10
  },

  innerContainer: {
    backgroundColor: colors.white
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 80,
    height: 80,
    marginTop: 10,
    marginBottom: 10
  },

  amountStyle: {
    color: colors.green,
    fontWeight: 'bold',
    fontSize: 20,
  },

  titleTxtStyle: {
    fontSize: 20,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 0,
    fontWeight: '100'

  },
  desTxt: {
    fontSize: 23,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold',
    // textAlign: 'center'
  },

  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 25
  },

  bottomBtn: {
    width: 40

  },
  bottomCantainerBtnTxt: {
    fontSize: 20,
    color: colors.green

  }

});

export default connect(null, actions)(GeneralModel);
