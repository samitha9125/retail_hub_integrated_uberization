import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Linking,
  BackHandler,
  Alert
} from 'react-native';
import SignatureCapture from 'react-native-signature-capture';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import strings from '../../Language/MobileActivaton';
import stringsDel from '../../Language/Delivery';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import Orientation from 'react-native-orientation';

const Utill = new Utills();

let navigatorOb;
let isUserStartSigined;
let ImagePath;

class SignaturePad extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    stringsDel.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      isUserStartSigin: false,
      placeYourSignature: strings.placeYourSignature,
      bySigining: strings.bySigining,
      tns: strings.tns,
      enterCustomerSignature: strings.enterCustomerSignature,
      sigReset: strings.sigReset,
      sigOk: strings.sigOk,
      didUserSign: false,
      locals: {
        signaturePadTitle: strings.customerSignatureUC,
        backMessage: stringsDel.backMessage
      }
    };
    navigatorOb = props.navigator;
    isUserStartSigined = false;
    this.imageId = `SIG_${this.props.unique_tx_id}_${Utill.getCurrentTimeStamp()}`;
    // ImagePathOld = '/storage/emulated/0/DCIM'; ImagePath =
    // `${Environment.getExternalStorageDirectory().toString()}/DCIM`;
    ImagePath = 'temp';
  }

  componentWillMount(){
    Orientation.lockToLandscape();
  }

  componentDidMount() {
    Orientation.lockToLandscape();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });

  }

  onSaveEvent = (result) => {
    console.log('xxx onSaveEvent');
    const signatureId = result.imageId;
    const signatureUri = result.pathName;
    const signatureRand = Math.floor(Math.random() * 1000);
    const signatureBase = result.encoded;
    console.log(result);
    const signatureOb = {
      signatureRand,
      signatureId,
      signatureUri,
      signatureBase
    };
    if (isUserStartSigined) {
      this
        .props
        .getSignatureMobileAct(signatureOb);
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    } else {
      Utill.showAlertMsg(this.state.enterCustomerSignature);
    }
  }
  onDragEvent = () => {
    // This callback will be called when the user enters signature
    console.log('onDragEvent');
    isUserStartSigined = true;
    this.setState({ didUserSign: true });
  }

  saveSign = () => {
    this
      .refs
      .sign
      .saveImage();
  }

  resetSign = () => {
    isUserStartSigined = false;
    this.setState({ didUserSign: false });
    this
      .refs
      .sign
      .resetImage();
  }
  render() {
    const { downlodUrl } = this.props;
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.signaturePadTitle}/>
        <View style={styles.signatureContainer}>
          <View style={styles.topContainer}>
            <View stye={styles.iconView}>
              {this.state.didUserSign == true
                ? <TouchableHighlight
                  style={styles.buttonStyle}
                  underlayColor={Colors.transparent}
                  onPress={() => {
                    this.resetSign();
                  }}>
                  <Ionicons name='md-close' size={30} color={Colors.colorBlack}/>
                </TouchableHighlight>
                : true
              }
            </View>
            <View style={styles.placeYourSignatureView}>
              <Text style={styles.desTxt}>{this.state.placeYourSignature}</Text>
            </View>
            <View stye={styles.iconView}>
              {this.state.didUserSign == true
                ? <TouchableHighlight
                  style={styles.buttonStyle}
                  underlayColor={Colors.transparent}
                  onPress={() => {
                    this.saveSign();
                  }}>
                  <Ionicons name='md-checkmark' size={35} color={Colors.colorBlack}/>
                </TouchableHighlight>
                : true
              }
            </View>
          </View>
          <SignatureCapture
            style={styles.signature}
            ref="sign"
            onSaveEvent={this.onSaveEvent}
            onDragEvent={this.onDragEvent}
            saveImageFileInExtStorage
            showNativeButtons={false}
            showTitleLabel={false}
            viewMode={'landscape'}
            savePath={ImagePath}
            imageId={this.imageId}/>
        </View>
        <View style={styles.buttonContainer}>
          <Text style={styles.desTncTxt}>{this.state.bySigining}
          </Text>
          <Text style={styles.desTncTxtLink} onPress={() => Linking.openURL(downlodUrl)}>
            {this.state.tns}
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const unique_tx_id = state.configuration.activity_start_time;
  const Language = state.lang.current_lang;

  return { unique_tx_id, Language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    backgroundColor: Colors.white
  },

  topContainer: {
    flex: 0.8,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0
  },
  signature: {
    flex: 7,
  },
  buttonContainer: {
    flex: 0.8,
    margin: 5,
    marginTop:0,
    // marginBottom:10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: Colors.transparent,
  },
  desTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
    fontWeight: 'bold'
  },

  iconView: {
    flex: 1
  },

  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold'
  },
  placeYourSignatureView: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: 15,
    marginLeft: 5,
    textDecorationLine: 'underline',
    fontWeight: 'bold'
  },
  signatureContainer: {
    flex: 7,
    elevation: 3,
    flexDirection: 'column',
    margin: 10, 
    borderColor: Colors.signatureBorderColor
  }
});

export default connect(mapStateToProps, actions)(SignaturePad);
