import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import Colors from '../../../../config/colors';
import Styles from '../../../../config/styles';
import Constants from '../../../../config/constants';
import Utills from '../../../../utills/Utills';
import imageAtentionIcon from '../../../../../images/common/alert.png';

const Utill = new Utills();

class ConfirmModelWarrantyReplacement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModel: false,
            apiData: {},
            locals: {
                descriptionText: 'Do you want to dispatch the work order ?'
            }
        };
    }

    exCb = () => {
        console.log('exCb');
        Utill.showAlertMsg('System Error');
    }

    updateWorkOrderSuccessCb = (response) => {
        console.log('updateWorkOrderSuccessCb');
        console.log('this.props.insideDetailView', this.props.insideDetailView);
        console.log('xxx updateWorkOrderSuccessCb', response.data.info);
        this
            .props
            .setWorrkOrderStatus(Constants.workOrdrState.DISPATCHED);
        this
            .props
            .setCurrentWorkOrderJobStatus('DISPATCHED');
        if (!this.props.insideDetailView) {
            const navigatorOb = this.props.navigator;
            navigatorOb.push({
                title: 'Pending Work Orders',
                screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen',
                passProps: {
                    workOrdrState: Constants.workOrdrState.DISPATCHED
                }
            });
        }

    }

    updateWorkOrderFailureCb = (response) => {
        console.log('updateWorkOrderFailureCb');
        console.log('xxx updateWorkOrderFailureCb', response);
        const navigatorOb = this.props.navigator;
        navigatorOb.showModal({
            title: 'General Alert Modal',
            screen: 'DialogRetailerApp.modals.GeneralAlertModal',
            overrideBackPress: true,
            passProps: {
                error: response.data.error,
                response: response.data,
                messageType: 'alert'
            }
        });
    }

    updateWorkOrder = (order_id, job_id, updateType) => {
        console.log('updateWorkOrder');
        const data = {
            order_id: order_id,
            job_id: job_id,
            command: updateType
        };

        Utill.apiRequestPost('updateWorkOrder', 'womDelivery', 'delivery', data, this.updateWorkOrderSuccessCb, this.updateWorkOrderFailureCb, this.exCb);
    }

    onBnPressYes = (workOrderId, jobId) => {
        const navigatorOb = this.props.navigator;
        navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        this.updateWorkOrder(workOrderId, jobId, 'DISPATCH');
    }

    onBnPressCancel = () => {
        const navigatorOb = this.props.navigator;
        navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        this
            .props
            .setWorrkOrderStatus(Constants.workOrdrState.PENDING);
    }

    render() {
        const { currentWorkOrderId, currentWorkOrderJobId } = this.props;
        console.log('workOrderId=>', currentWorkOrderId);
        console.log('jobId=>', currentWorkOrderJobId);
        return (
            <View style={styles.containerOverlay}>
                <View style={styles.topContainer} />
                <View style={styles.modalContainer}>
                    <View style={styles.innerContainer}>
                        <View style={styles.alertImageContainer}>
                            <Image source={imageAtentionIcon} style={styles.alertImageStyle} />
                        </View>
                        <View style={styles.descriptionContainer}>
                            <Text style={styles.workOrderId}>Work Order No : {currentWorkOrderId}</Text>
                            <Text style={styles.descriptionText}>{this.state.locals.descriptionText}</Text>
                        </View>
                        <View style={styles.bottomCantainerBtn}>
                            <View style={styles.dummyView} />
                            <TouchableOpacity
                                onPress={() => this.onBnPressCancel()}
                                style={styles.bottomBtnCancel}>
                                <Text style={styles.bottomCantainerCancelBtnTxt}>
                                    {'NO'}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.onBnPressYes(currentWorkOrderId, currentWorkOrderJobId)}
                                style={styles.bottomBtn}>
                                <Text style={styles.bottomCantainerBtnTxt}>
                                    {'YES'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomContainer} />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('****** REDUX STATE :: DELIVERY => ConfrimAlert\n', state.delivery);
    const Language = state.lang.current_lang;
    const workOrderState = state.delivery.workOrderState;
    const wom_api_order_details = state.delivery.wom_api_order_details;
    const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
    const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
    const currentWorkOrderId = state.delivery.currentWorkOrderId;
    const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
    const currentWorkOrderJobStatus = state.delivery.currentWorkOrderJobStatus;

    return {
        Language,
        workOrderState,
        wom_api_order_details,
        wom_api_order_customerDetails,
        wom_api_order_addionalDetails,
        currentWorkOrderId,
        currentWorkOrderJobId,
        currentWorkOrderJobStatus
    };
};

const styles = StyleSheet.create({

    containerOverlay: {
        flex: 1,
        backgroundColor: Colors.modalOverlayColor
    },

    topContainer: {
        flex: 0.4
    },
    modalContainer: {
        flex: 1.5
    },

    bottomContainer: {
        flex: 0.6
    },

    innerContainer: {
        flex: 1,
        backgroundColor: Colors.backgroundColorWhiteWithAlpa,
        padding: 12,
        paddingBottom: 20,
        marginTop: 25,
        marginBottom: 25,
        marginLeft: 13,
        marginRight: 13
    },

    alertImageContainer: {
        flex: 0.7,
        alignItems: 'center',
        padding: 5
    },

    alertImageStyle: {
        width: 90,
        height: 90,
        marginTop: 5,
        marginBottom: 5
    },

    descriptionContainer: {
        flex: 1.5,
        flexDirection: 'column',
        paddingTop: 10,
        paddingRight: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        marginTop: 10
    },
    descriptionText: {
        fontSize: Styles.delivery.modalFontSize,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        fontWeight: '100'

    },
    workOrderId: {
        alignItems: 'flex-start',
        fontSize: Styles.delivery.modalFontSize,
        textAlign: 'left',
        color: Colors.colorBlack,
        paddingTop: 5,
        marginLeft: 10,
        marginBottom: 15,
        paddingBottom: 5,
        marginRight: 10,
        marginTop: 20,
        fontWeight: 'bold'
    },

    bottomCantainerBtn: {
        flex: 0.5,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        paddingTop: 5,
        paddingRight: 5,
        paddingBottom: 5,
        marginTop: 10
    },

    dummyView: {
        flex: 3,
        justifyContent: 'center'
    },

    bottomBtnCancel: {
        flex: 1,
        justifyContent: 'center'
    },
    bottomBtn: {
        flex: 1,
        marginRight: 10,
        justifyContent: 'center'
    },
    bottomCantainerBtnTxt: {
        fontSize: Styles.delivery.modalFontSize,
        alignSelf: 'center',
        color: Colors.colorRed
    },
    bottomCantainerCancelBtnTxt: {
        fontSize: Styles.delivery.modalFontSize,
        alignSelf: 'center',
        color: Colors.colorBlack
    }

});

export default ConfirmModelWarrantyReplacement;
