import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  TextInput,
  Vibration,
  BackHandler,
  Alert,
  Keyboard
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Orientation from 'react-native-orientation'
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import strings from '../../Language/general';
import ActIndicator from './ActIndicator';
import { Header } from '../common/components/Header';
import screenTitles from '../../Language/SimChange';

const Utill = new Utills();

const VIBRATION_DURATION = 200;
const MARGIN = 50;
const DEVICE_WIDTH = Dimensions
  .get('window')
  .width;

class BarcodeScannerCommon extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    screenTitles.setLanguage(this.props.Language);
    this.camera = null;
    this.state = {
      simBackMsg: strings.simBackMsg,
      disableContinueButton: true, 
      barcodeValue: null,
      scanSucess: false,
      enterSimSerial: strings.enterSimSerial,
      enterValidSim: strings.enterValidSim,
      continue: strings.continue,
      scanSuccess: strings.scanSuccess,
      BarcodeScannerTitle: screenTitles.ScanSim,
      uniqueValue: 0,
      locals: {
        permission_to_use_camera: 'Permission to use camera',
        permission_to_use_camera_message: 'To Scan the serial, App need your permission to use your camera',
      }
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait()
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onHandleBackButton);
    if (this.state.barcodeValue === null || this.state.barcodeValue.length !== 8) {
      this
        .props
        .genaralSetSimNumber('');
    }
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  forceRemount = () => {
    this.setState(({ uniqueValue }) => ({
      uniqueValue: uniqueValue + 1
    }));
  }

  componentWillReceiveProps(nextProps) {
    console.log('SIM VALIDATION:: componentWillReceiveProps', nextProps.validateApiFail);
    console.log('SIM VALIDATION:: componentWillReceiveProps', nextProps);
    if (nextProps.validateApiFail && nextProps.simValidationStatus == 'fail') {
      this.setState({ barcodeValue: '', scanSucess: false });
      this.forceRemount();
      nextProps.genaralSetSimNumber('');
    }

    if (nextProps.simNumber == '') {
      nextProps.genaralResetSimApiStatus();
    }

    if (nextProps.simValidationStatus == 'initial' && nextProps.simNumber.length == 8 && nextProps.simNumber !== this.props.simNumber) {
      const data = {
        sim: nextProps.simNumber
      };

      nextProps.genaralSetApiLoading(true);
      console.log('xxxx genaralValidateSimNumber this.props.validateUrl', this.props.validateUrl);
      if (this.props.validateUrl !== undefined && this.props.validateUrl !== null ){
        console.log('xxxx genaralValidateSimNumber Delivery API');
        nextProps.genaralValidateSimNumber(data, this.props.validateUrl);
      } else {
        console.log('xxxx genaralValidateSimNumber Common API');
        nextProps.genaralValidateSimNumber(data);      
      }
      
    }

    if (nextProps.didSimValidated) {
      Keyboard.dismiss();
      this.setState({ disableContinueButton: false });
    }
  }

  onHandleBackButton = () => {
    console.log('xxxx onHandleBackButton');
    Alert.alert('', this.state.simBackMsg, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.goBack()
      }
    ], { cancelable: true });
    return true;
  }

  goBack = () => {
    console.log('xxx goBack');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  onBarCodeReadCb = (e) => {
    console.log('xxx onBarCodeReadCb');
    Vibration.vibrate(VIBRATION_DURATION);
    const last8Digit = e
      .data
      .toString()
      .slice(-8);
    console.log(`xxx onBarCodeReadCb :${last8Digit}`);
    if (!isNaN(parseInt(last8Digit))) {
      this.setState({ barcodeValue: last8Digit, scanSucess: true });
      this
        .props
        .genaralSetSimNumber(last8Digit);
    }
  };

  onChangeText = (value) => {
    console.log('xxx onChangeText');
    this.setState({ barcodeValue: value });
    this
      .props
      .genaralResetSimApiStatus();

    this
      .props
      .genaralSetSimNumber(value);
  };

  returnView = () => {
    console.log('xxx returnView');
    const navigatorOb = this.props.navigator;
    if (this.state.barcodeValue === null || this.state.barcodeValue.length !== 8) {
      Utill.showAlertMsg(this.state.enterValidSim);
      return;
    }
    if (this.props.hasNextScreen) {
      if (this.props.didSimValidated) {
        const navigatorOb = this.props.navigator;
        navigatorOb.push({
          title: this.props.onReturnViewTitle,
          screen: this.props.onReturnView,
          passProps: {
            dataProps: this.props.dataProps
          }
        });

      } else {
        Utill.showAlertMsg(this.state.enterValidSim);
      }
    } else {
      navigatorOb.pop({ animated: true, animationType: 'fade' });
    }
  };

  render() {

    let scanSucessMesage;
    if (this.state.scanSucess) {
      scanSucessMesage = (
        <View style={styles.detectedTxtView}>
          <Text style={styles.detectedTxt}>
            {this.state.scanSuccess}
          </Text>
        </View>
      );
    } else {
      scanSucessMesage = true;
    }
    let loadingIndicator;
    if (this.props.api_loading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.containerBarcodeScan}>
        <Header
          backButtonPressed={() => this.onHandleBackButton()}
          headerText={this.state.BarcodeScannerTitle}/>
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          key={this.state.uniqueValue}
          style={styles.barcodePreview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={this.state.locals.permission_to_use_camera}
          permissionDialogMessage={this.state.locals.permission_to_use_camera_message}
          onBarCodeRead={(e) => this.onBarCodeReadCb(e)}/>
        <View style={[styles.barcodeOverlay, styles.barcodeTopOverlay]}>
          <View style={styles.barcodeViewFinder}/> 
          {/* There may be issue occur in this place - aware evil space issue */}
          {scanSucessMesage}
        </View>
        {loadingIndicator}
        <View style={[styles.barcodeOverlay, styles.barcodeBottomOverlay]}>
          <View style={styles.containerBottomBarcode}>
            <View style={styles.barcodeInputContainer}>
              <TextInput
                style={styles.barcodeTxtInput}
                underlineColorAndroid='transparent'
                keyboardType={'numeric'}
                value={this.state.barcodeValue}
                placeholder={this.state.enterSimSerial}
                maxLength={8}
                autoFocus={false}
                returnKeyType={'done'}
                selectionColor={'yellow'}
                onChangeText={(value) => this.onChangeText(value)}/>
            </View>
            <TouchableOpacity
              style={this.state.disableContinueButton == false ? styles.barcodeContinueBtn : styles.barcodeContinueBtnDisabled}
              disabled={this.state.disableContinueButton}
              onPress={() => this.returnView()}>
              <Text style={this.state.disableContinueButton == false ? styles.barcodeContinueBtnTxt : styles.barcodeContinueBtnTxt}>
                {this.state.continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: BracodeScanner => delivery\n', state.delivery);
  console.log('****** REDUX STATE :: BracodeScanner => genaral\n', state.genaral);

  const Language = state.lang.current_lang;
  const simValidationStatus = state.genaral.simValidationStatus;
  const didSimValidated = state.genaral.didSimValidated;
  const simNumber = state.genaral.simNumber;
  const validateApiFail = state.genaral.validateApiFail;
  const material_code = state.genaral.material_code;
  const api_loading = state.genaral.api_loading;

  return {
    Language,
    simValidationStatus,
    didSimValidated,
    simNumber,
    validateApiFail,
    material_code,
    api_loading
  };
};

const styles = StyleSheet.create({
  containerBarcodeScan: {
    flex: 1
  },
  barcodePreview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  containerBottomBarcode: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.barcodeBackgroundColor
  },
  barcodeViewFinder: {
    marginTop: 130,
    width: DEVICE_WIDTH - MARGIN,
    height: 140,
    borderWidth: 3,
    borderRadius: 2,
    borderColor: Colors.colorPureYellow,
    backgroundColor: Colors.colorTransparent
  },

  detectedTxtView: {
    marginTop: 15,
    width: DEVICE_WIDTH - MARGIN,
    height: 30,
    backgroundColor: Colors.colorTransparent
  },

  detectedTxt: {
    textAlign: 'center',
    color: Colors.colorWhite,
    fontWeight: '400',
    fontSize: Styles.barcodeDtectedTxtFontSize
  },

  barcodeOverlay: {
    position: 'absolute',
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  barcodeTopOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
    //backgroundColor: 'rgba(0,0,0,0.4)',
  },
  barcodeBottomOverlay: {
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
    //backgroundColor: 'rgba(0,0,0,0.4)',
  },
  barcodeInputContainer: {
    flex: 1,
    margin: 10,
    //backgroundColor: 'green'
  },
  barcodeTxtInput: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  barcodeContinueBtn: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.colorYellow
  },
  barcodeContinueBtnDisabled: {
    flex: 0.8,
    margin: 10,
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    height: 45,
    borderRadius: 5,
    backgroundColor: Colors.transparent
  },
  barcodeContinueBtnTxt: {
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    textAlign: 'center'
  },
  barcodeContinueBtnTxtDisabled:{
    fontSize: Styles.btnFontSize,
    fontWeight: '400',
    color: Colors.btnDeactiveTxtColor,
    textAlign: 'center'
  }
});

export default connect(mapStateToProps, actions)(BarcodeScannerCommon);