/*
 * File: SimChangeDetails.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 9th May 2018 3:08:29 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:53:29 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  BackHandler,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from '../../../views/transactionHistory/Header';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import ActIndicator from '../ActIndicator';
import strings from '../../../Language/ActivationStatus';
import iconActive from '../../../../images/common/icons/icon_state_success.png';
import iconPending from '../../../../images/common/icons/icon_state_pending.png';
import image_state_detail_success from '../../../../images/common/icons/icon_detail_state_success.png';
import image_state_detail_pending from '../../../../images/common/icons/icon_detail_state_pending.png';

/**
 * @description
 * @class SimChangeDetails
 * @extends {React.Component}
 */
class SimChangeDetails extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      locals: {
        screenTitle:  strings.simChangeHistoryTitle,
        ok: strings.ok,
        connectionNo: strings.connectionNo,
        simNo: strings.simNo,
        transID: strings.transID,
        connectionStatus: strings.connectionStatus,
        activatedBy: strings.activatedBy,
        date: strings.date,
        time: strings.time,
        backMessage: strings.backMessage,
        pending: strings.pendingTxt,
        active: strings.active,
        inActive: strings.inActive,
        disconnected: strings.disconnected,
      },
      
    };
   
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }
 
  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    console.log('xxx handleBackButtonClick');
    this.okHandler();
    return true;
  }

  onPressOk = () => {
    console.log('xxx onPressOk');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  /**
   * @description Get connection sim change status icon
   * @param {String} status
   * @returns {Object} icon
   * @memberof SimChangeDetails
   */
  getImageSrc = (status) => {
    console.log('xxxx getImageSrc', status);
    if (status == 'active') {
      return iconActive;
    } else if (status == 'pending') {
      return iconPending;
    } else {
      return iconPending;
    }
  }

/**
 * @description Get connection activation status name and icon
 * @param {String} status
 * @returns {Object} { conSt, conStIcon }
 * @memberof SimChangeDetails
 */
getConnectionStatus = (status) => {
  let conSt;
  let conStIcon = null;
  if (status == 'active') {
    conSt = this.state.locals.active;
    conStIcon = image_state_detail_success;
  } else if (status == 'pending') {
    conSt = this.state.locals.pending;
    conStIcon = image_state_detail_pending;
  } else {
    conSt = this.state.locals.pending;
    conStIcon = image_state_detail_pending; 
  }
  
  return { conSt, conStIcon };
}
 
render() {
  let loadingIndicator;
  let item = this.props.item;
  let detailsArray = [
    {
      label: this.state.locals.connectionNo,
      value: item.mobileNumber,
      icon: null
    }, {
      label: this.state.locals.simNo,
      value: item.simNo,
      icon: null
    },
    {
      label: this.state.locals.transID,
      value: item.transID,
      icon: null
    },
    {
      label: this.state.locals.activatedBy,
      value: item.activatedBy,
      icon: null
    },
    {
      label: this.state.locals.connectionStatus,
      value: this.getConnectionStatus(item.status).conSt,
      icon: this.getConnectionStatus(item.status).conStIcon,
    },
    {
      label: this.state.locals.date,
      value: item.date,
      icon: null
    },
    {
      label: this.state.locals.time,
      value: item.time,
      icon: null
    }
  ];

  if (this.props.api_loading) {
    loadingIndicator = (<ActIndicator animating/>);
  } else {
    loadingIndicator = true;
  }

  const getElementView = (index, elementValue)=> {

    if (elementValue.icon != undefined && elementValue.icon != null){
      return (<ElementItemWithIcon key={index} leftTxt={elementValue.label} rightTxt={elementValue.value} icon={elementValue.icon}/>);
    } else {
      return (<ElementItem key={index} leftTxt={elementValue.label} rightTxt={elementValue.value}/>);
    }
  };

  return (
    <View style={styles.container}>
      <Header
        backButtonPressed={() => this.handleBackButtonClick()}
        headerText={this.state.locals.screenTitle}/> 
      {/* There may be issue occur in this place - aware evil space issue */}
      {loadingIndicator}
      <View style={styles.iconContainer}>
        <Image source={this.getImageSrc(item.status)} style={styles.imageIconStyle}/>
      </View>
      <View style={styles.detailsContainer}>
        <ScrollView>
          {detailsArray.map((value, i) => getElementView(i, value))}
        </ScrollView>
      </View>
      <View style={styles.containerBottom}>
        <ElementFooterButton 
          okButtonText={this.state.locals.ok}
          onClickOk={() => this.onPressOk()}
        />
      </View>
    </View>

  );
}
}

const ElementItem = ({ leftTxt, rightTxt }) => (
  <View style={styles.detailElementContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementItemWithIcon = ({ leftTxt, rightTxt, icon }) => (
  <View style={styles.detailElementContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRightWithIcon}>
      <View style={styles.elementRightTextContainer}>
        <Text style={styles.elementTxt}>
          {rightTxt}</Text>
      </View>
      <View style={styles.elementRightIconContainer}>
        <Image source={icon} style={styles.iconStyle}/>
      </View>
    </View>
  </View>
);

const ElementFooterButton = ({
  okButtonText,
  onClickOk,
  disabled = false
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={disabled}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TabBarAll => TransactionHistory ', state.transHistory);
  const api_loading = state.transHistory.api_loading;
  const Language = state.lang.current_lang;

  return {  Language, api_loading };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  iconContainer: {
    flex: 1.2,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },

  detailsContainer: {
    flex: 6,
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },
  containerBottom: {
    backgroundColor: Colors.colorTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },

  detailElementContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },

  imageIconStyle: {
    width: 58,
    height: 58,
    marginBottom: 5   
  },

  elementLeft: {
    flex: 1.3,
    marginLeft: 10,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },
  elementRightWithIcon: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '500',
    color: Colors.colorBlackDelivery
  },
  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize
  },

  elementRightTextContainer: {
    flex: 3,
  },

  elementRightIconContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    
  },

  iconStyle: {
    width: 12,
    height: 12,
    margin: 5,
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    flexDirection: 'row',
    padding: 0,
    paddingTop: 0,
    paddingBottom: 10,
  },
  dummyView: {
    flex: 1.5
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 48,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  }
});

export default connect(mapStateToProps, actions)(SimChangeDetails);
