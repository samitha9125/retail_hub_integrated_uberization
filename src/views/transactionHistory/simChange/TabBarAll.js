/*
 * File: TabBarAll.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:38:25 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

//TODO: - 
/**
 * Create a redux method to clear search string.
 * Add the clear method to pull to refresh
 * Hide Search bar when pull to refresh is called
 */
import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableHighlight, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Images from '@Config/images';
import EmptyFeed from '@Components/EmptyFeed';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import strings from '../../../Language/ActivationStatus';
import globalConfig from '../../../config/globalConfig';

import _ from 'lodash';

const pageLimit = globalConfig.transactionHistoryLoadLimit;

/**
 * @description
 * @class TabBarAll
 * @extends {React.Component}
 */
class TabBarAll extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      dataArray: [],
      start: 0,
      isLoading: false,
      locals: {
        nicFront: strings.nicFront,
        nicBack: strings.nicBack,
        pp: strings.pp,
        dl: strings.dl,
        pob: strings.pob,
        customerImage: strings.customerImage,
        ack: strings.ack,
        pendingTxt: strings.pendingTxt,
        approvedTxt: strings.approvedTxt,
        rejectedTxt: strings.rejectedTxt,
        errorDesc: strings.errorDesc,
      }
    };
    this.debouncedLoadLisItem = _.debounce((item)=>this.onListItemTap(item),globalConfig.listItemTapInterval,
      { 'leading': true,
        'trailing': false });
    this.debouncedHandleLoadMore = _.debounce(this.handleLoadMore,250,
      { 'leading': true,
        'trailing': false });

  }

  componentWillReceiveProps(nextProps){
    console.log("RECEIVING: ",JSON.stringify(nextProps));
  }

  componentDidMount() {
    console.log('### simChange=>TabBarAll :: componentDidMount');
    this.callApi();
  }

  componentDidUpdate() {
    console.log('xxxxx simChange=>TabBarAll :: componentDidUpdate');
  }


  /**
   * @description Sim change history all list backend API call
   * @memberof TabBarAll
   */
  callApi = () => {
    // if (this.props.sim_change_history_search_string.length > 0){
    //   return;
    // }
    console.log('xxxx callApi');
    // this.props.resetSIMChangeHistoryLists();
    let data = {
      tab_status: 'all',
      start : this.state.start,
      limit :pageLimit,
    };

    if (this.props.sim_change_history_search_string !== ''){
      if (this.props.sim_change_history_search_string.customer_msisdn){
        console.log("Has search string customer_msisdn");
        data = {
          tab_status: 'all',
          customer_msisdn: this.props.sim_change_history_search_string.customer_msisdn,
          limit: this.props.sim_change_history_search_string.limit,
          start: parseInt(this.props.sim_change_history_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }

      if (this.props.sim_change_history_search_string.start_date && this.props.sim_change_history_search_string.end_date){
        console.log("Has search string customer_msisdn");
        data = {
          tab_status: 'all',
          start_date: this.props.sim_change_history_search_string.start_date,
          end_date: this.props.sim_change_history_search_string.end_date,
          limit: this.props.sim_change_history_search_string.limit,
          start: parseInt(this.props.sim_change_history_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }
    }

    this.props.getSimChangeHistoryList(data);
    Analytics.logEvent('dsa_sim_change_history_all');

  }

  /**
 * @description Handel list item tap event
 * @param {Object} { item }
 * @memberof TabBarAll
 */
  onListItemTap = (item) => {
    console.log('xxxx onListItemTap', item);
    const navigatorOb = this.props.navigator; 
    navigatorOb.push({
      title: 'SimChangeDetailsScreen',
      screen: 'DialogRetailerApp.views.SimChangeDetailsScreen',
      passProps: {
        item: item
      }
    });
  };

  /**
   * @description Get status image icon 
   * @param {String} status
   * @memberof TabBarAll
   */
  getStatusIconSrc = (status) => {
    console.log('xxxx getStatusIconSrc', status);
    if (status == 'active') {
      return Images.icons.Success_200px;
    } else if (status == 'pending') {
      return Images.icons.Pending_200px;
    } else {
      return Images.icons.Pending_200px;
    }
  }

  /**
   * @description Handle pull to refresh
   * @memberof TabBarAll
   */
  pullToRefresh = () => {
    // this.props.resetSIMChangeSearchResults();
    console.log('xxx pullToRefresh');

    if (this.props.shouldHideSearch){
      this.props.shouldHideSearch();
      return;
    }
    
    this.setState({
      start: 0
    }, async() => {
      await this.props.transResetAll();
      await this.callApi(); 
    });
  }

  /**
 * @description Handle infinite scroll
 * @param {String} distanceFromEnd
 * @memberof TabBarAll
 */
  handleLoadMore = () => {
    let listData = [];

    if (this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null 
      && this.props.sim_change_history_all_list.length > 0 
      && this.props.sim_change_history_search_string !== ''){

      listData = this.props.sim_change_history_all_list;
    }
    if (this.props.sim_change_history_search_string === '' 
      && this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null){

      listData = this.props.sim_change_history_all_list;
    }

    if ( this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null 
      && this.props.sim_change_history_all_list.length == 0 
      && this.props.sim_change_history_search_string !== ''){

      listData = [];
    }
    console.log('xxx handleLoadMore');
    // if (this.state.start == 2) {
    //   return;
    // }
    this.setState({
      start: listData.length
    }, () => {
      this.callApi();
    });
  }

  /**
 * @description Generate list item
 * @param {Object} { item }
 * @memberof TabBarAll
 */
  renderListItem = ({ item }) => (
    <TouchableHighlight onPress={()=>{this.debouncedLoadLisItem(item);}}>
      <View style={styles.listView_row}>
        <View style={styles.listView_col1}>
          <Image source={this.getStatusIconSrc(item.status)} style={styles.imageStatusIconStyle}/>
        </View>
        <View style={styles.listView_col2}>
          <Text style={styles.rowText}>{item.mobileNumber}</Text>
        </View>
        <View style={styles.listView_col3}>
          <Text style={styles.rowTextIssue}>{item.connType}</Text>
        </View>
        <View style={styles.listView_col5}>
          <View style={styles.listView_col5_1}>
            <Text style={styles.rowTextDate}>{item.date}</Text>
            <Text style={styles.rowTextTime}>{item.time}</Text>
          </View>
        </View>
        <View style={styles.listView_col6}>
          {forwardIcon}
        </View>
      </View>
    </TouchableHighlight>
  );

  render() {
    let listData = [];

    if (this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null 
      && this.props.sim_change_history_all_list.length > 0 
      && this.props.sim_change_history_search_string !== ''){

      listData = this.props.sim_change_history_all_list;
    }
    if (this.props.sim_change_history_search_string === '' 
      && this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null){

      listData = this.props.sim_change_history_all_list;
    }

    if ( this.props.sim_change_history_all_list !== undefined 
      && this.props.sim_change_history_all_list !== null 
      && this.props.sim_change_history_all_list.length == 0 
      && this.props.sim_change_history_search_string !== ''){

      listData = [];
    }

    console.log("In render method");

    return (
      <View style={styles.containerTab}>
        <FlatList
          data={listData}
          renderItem={this.renderListItem}
          refreshing={this.props.api_loading}
          onRefresh={this.pullToRefresh}
          onEndReached={({ distanceFromEnd }) => {
            if (distanceFromEnd >= 0) {
              console.log('on end reached call api', distanceFromEnd);
              console.log('this.props.sim_change_history_all_list', this.props.sim_change_history_all_list.length);
              
              //call API to get next page values
              this.debouncedHandleLoadMore();
            }
            console.log('on end reached ', distanceFromEnd);
          }}
          onEndReachedThreshold ={0.05}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => {
            if (this.props.api_loading)
              return null;
            return (
              <EmptyFeed
                text={this.state.locals.errorDesc}
                messageTxtStyle={styles.errorDescText}
              />
            );
          }
          }
          scrollEnabled={true}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: simChange :: TabBarAll => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const api_loading = state.transHistory.api_loading;
  const sim_change_history_all_list = state.transHistory.sim_change_history_all_list;
  const sim_change_history_search_string = state.transHistory.sim_change_history_search_string;
  
  return { 
    Language, 
    api_loading, 
    sim_change_history_all_list, 
    sim_change_history_search_string 
  };
};

const styles = StyleSheet.create({

  containerTab: {
    flex: 1
  },
  listView_row: {
    flexDirection: 'row',
    height: 50,
    borderWidth: 0.5,
    borderColor: Colors.borderLineColor,
    backgroundColor: Colors.colorWhite
  },
  listView_col1: {
    flex: 1.2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20
  },
  listView_col2: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 10
  },

  listView_col3: {
    flex: 2.5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 15
  },

  listView_col5: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 8,
    paddingTop: 8
  },

  listView_col5_1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  
  listView_col6: {
    flex: 0.6,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingRight: 10
  },
  rowText: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 15,
    color: Colors.colorBlack,
    textAlign: 'left',
    fontWeight: '400'
  },

  rowTextDate: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },

  rowTextTime: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },
  iconStyle: {
    textAlign: 'center',
    marginLeft: 10
  },

  imageStatusIconStyle : {
    width: 20,
    height: 20,
    margin: 5
  },
  errorDescText:{ 
    textAlign: 'center', 
    fontSize: 17, 
    fontWeight:'bold', 
    color: Colors.black 
  }
});

const forwardIcon = (<Icon
  name="ios-arrow-forward"
  size={25}
  color="black"
  style={styles.iconStyle}/>);

export default connect(mapStateToProps, actions)(TabBarAll);
