/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:53:04 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { View, Dimensions, StyleSheet, BackHandler } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import globalConfig from '../../../config/globalConfig';
import { Header } from '../../../views/transactionHistory/Header';
import { SearchBar } from '../SearchBar';
import TabBarFailed from './TabBarFailed';
import TabBarAll from './TabBarAll';
import strings from '../../../Language/ActivationStatus';
import Analytics from '../../../utills/Analytics';

import _ from 'lodash';

const pageLimit = globalConfig.transactionHistoryLoadLimit;

const INITIAL_LAYOUT_WIDTH = {
  height: 0,
  width: Dimensions.get('window').width,
};
/**
 * @description
 * @class SimChangeHistoryMainView
 * @extends {React.Component}
 */
class SimChangeHistoryMainView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      index: 0,
      routes: [
        { key: 'all', title: strings.all },
        { key: 'pending', title: strings.pending },
      ],
      locals:{
        backMessage: strings.backMessageNew,
        headerTitle: strings.simChangeHistoryTitle,
        tabAll:  strings.all,
        tabPending: strings.pending,
      },
      searchBarHidden: true,
      AllRoute:null,
      PendingRoute:null,
      _renderScene:null,
      searchResult:'',
      start:0
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  shouldHideSearch = () =>{
    console.log("Should hide search");

    this.props.resetSIMChangeSearchResults(true);
    this.searchBar.hide();
    this.searchRecords('');
    this.setState({ searchBarHidden: true });
  }

  componentWillMount(){
    console.log('### SimChangeHistoryMainScreen :: componentWillMount');
    if (
      this.state.AllRoute == null ||
      this.state.PendingRoute == null ||
      this.state._renderScene == null) 
    {
      const AllRoute = () => <TabBarAll 
        shouldHideSearch={()=>this.shouldHideSearch()} 
        customProps={this.state.searchResult} 
        {...this.props}/>;
      const PendingRoute = () => <TabBarFailed 
        shouldHideSearch={()=>this.shouldHideSearch()}
        {...this.props}/>;

      const _renderScene = SceneMap({
        all: AllRoute,
        pending: PendingRoute,
      });

      this.setState({ AllRoute: AllRoute, PendingRoute: PendingRoute, _renderScene: _renderScene });
    }
  }

  handleBackButtonClick = () => {
    if (this.state.searchBarHidden == false){
      console.log('xxxx handleBackButtonClick :: searchBarHidden'),
      this.searchBar.hide();
      this.setState({ searchBarHidden: true });
      return true;
    }
    this.okHandler();
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    this.props.resetSIMChangeSearchResults();
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.TransactionHistoryMainScreen',
      title: 'BillPaymentHistoryMainScreen',
      animated: true, 
      animationType: 'slide-up' 
    });
  }

  /**
   * @description handle index change
   * @param {String} index
   * @memberof SimChangeHistoryMainView
   */
  _handleIndexChange = (index) => {
    console.log("TabIndex: ",index);
    console.log('sim_change_history_search_string: ',this.props.sim_change_history_search_string);
    console.log('sim_change_pending_history_search_string: ',this.props.sim_change_pending_history_search_string);


    this.setState({ index: index, start: 0 });

    if (this.props.sim_change_history_search_string || this.props.sim_change_pending_history_search_string){
      return;
    }

    this.props.resetSIMChangeHistoryLists();
    this.props.transShowApiLoading(true);
    
    if (index == 0){
      this.callAllTabApi();
    } else {
      this.callPendingTabApi();
    }
  }

  /**
   * @description Sim change history all list backend API call
   * @memberof TabBarFailed
   */
  callPendingTabApi = (action) => {
    console.log('xxxx callPendingTabApi');
    console.log('this.props.sim_change_history_search_string',this.props.sim_change_history_search_string);
    // this.props.resetSIMChangeSearchResults();
    this.setState({ isLoading: true });
    let data = {
      tab_status: 'pending',
      start : 0,
      limit :pageLimit,
    };

    if (!(this.props.sim_change_history_search_string === '')){
      data = { ...data,...this.props.sim_change_history_search_string };
    }

    if (action !== null && action ==='reset'){
      data = {
        tab_status: 'pending',
        start : 0,
        limit :pageLimit,
      };
    }

    this.props.getSimChangeHistoryPendingList(data);
    Analytics.logEvent('dsa_sim_change_history_pending');

  }

  /**
   * @description Sim change history all list backend API call
   * @memberof TabBarAll
   */
  callAllTabApi = (action) => {
    console.log('xxxx callApi');
    console.log('this.props.sim_change_history_search_string',this.props.sim_change_history_search_string);
    // this.props.resetSIMChangeSearchResults();

    let data = {
      tab_status: 'all',
      start : 0,
      limit :pageLimit,
    };

    if (!(this.props.sim_change_history_search_string === '')){
      data = { ...data,...this.props.sim_change_history_search_string };
    }

    if (action !== null && action ==='reset'){
      data = {
        tab_status: 'all',
        start : 0,
        limit :pageLimit,
      };
    }

    this.props.getSimChangeHistoryList(data);
    Analytics.logEvent('dsa_sim_change_history_all');
  }

  /**
 * @description Render header
 * @param {Object} props
 * @memberof SimChangeHistoryMainView
 */
  _renderHeader = props => <TabBar  
    style={{ backgroundColor: Colors.navBarBackgroundColor }} 
    tabStyle={{ backgroundColor: Colors.transparent }} 
    indicatorStyle={{ backgroundColor: Colors.white }}
    {...props} 
  />;

  /**
 * A custom debounce method to throttle user input in Search Bar
 *
 * @param {*} fn
 * @param {*} delay
 * @returns
 * @memberof SimChangeHistoryMainView
 */
  debounce(fn, delay) {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  }

  /**
 * @description Search SIM change history status list
 * @param {String} text
 * @memberof SimChangeHistoryMainView
 */
  searchRecords = async(text) =>{

    await this.props.resetSIMChangeSearchResults(true);
    console.log('=========> In searchRecords', 'text: ',text);
    if (text === '') {
      await this.props.transResetAll();
      this.props.resetSIMChangeHistoryLists(true);
      await this.props.resetSIMChangeSearchResults(true);
      
      console.log("Done reset search data");
      console.log('this.props.sim_change_history_search_string',this.props.sim_change_history_search_string);

      this.callAllTabApi('reset');

      _.delay(function(currContext){
        currContext.callPendingTabApi('reset');
      },50,this);
        
      return;
    }

    this.props.resetSIMChangeHistoryLists(true);
    if (isNaN(text)) return;

    let data = { 
      "customer_msisdn": text,
      "start" : 0,
      "limit" :pageLimit 
    };

    const successCbAll = (response) => {
  
      console.log('search :: successCbAll', response);
    };

    const errorCbAll = (response) => {
      console.log('search :: errorCbAll', response);
    };

    console.log('curr tab state: ', this.state.index);
    console.log('xxxx search AllTabApi');

    data = {
      tab_status: 'all',
      start : 0,
      limit :pageLimit,
      "customer_msisdn": text
    };
  
    this.props.getSimChangeHistoryList(data, successCbAll, errorCbAll);
    console.log('xxxx search PendingTabApi');

    _.delay(function(currContext){
      data = {
        tab_status: 'pending',
        start : 0,
        limit :pageLimit,
        "customer_msisdn": text
      };
      currContext.props.getSimChangeHistoryPendingList(data, successCbAll, errorCbAll);
    },100,this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          filterButtonPressed={() => this.handleFilter()}
          displaySearch 
          displayFilter 
          headerText={ this.state.locals.headerTitle }/>
        
        <SearchBar
          ref={(ref) => this.searchBar = ref}
          onChangeText={this.debounce(this.searchRecords,1000)}
          onPressClear={()=>{ 
            // this.searchRecords('');
            this.props.resetSIMChangeSearchResults(true);
            this.searchBar.hide();
            this.searchRecords('');
            this.setState({ searchBarHidden: true });
          }
          }
        /> 

        { this.state._renderScene !== null ? 
          <TabViewAnimated
            navigationState={this.state}
            renderScene={this.state._renderScene}
            renderHeader={this._renderHeader}
            onIndexChange={this._handleIndexChange}
            useNativeDriver
            initialLayout={INITIAL_LAYOUT_WIDTH}
          />
          : true }
      </View>
    );
  }

  /**
 * @description Show search bar
 * @memberof SimChangeHistoryMainView
 */
  handleSearch = ()=>{
    this.searchBar.show();
    this.setState({ searchBarHidden: false });
  }

  /**
 * @description Show filter view
 * @memberof SimChangeHistoryMainView
 */
  handleFilter = () =>{
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'FILTER', 
      screen: 'DialogRetailerApp.views.FilterSimChangeHistoryScreen',
      passProps: {
      }
    });
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: simChange :: index => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const sim_change_history_search_string = state.transHistory.sim_change_history_search_string;
  const sim_change_pending_history_search_string = state.transHistory.sim_change_pending_history_search_string;

  return { 
    Language,
    sim_change_history_search_string,
    sim_change_pending_history_search_string 
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps, actions)(SimChangeHistoryMainView);