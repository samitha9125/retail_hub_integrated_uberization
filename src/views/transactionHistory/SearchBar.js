/*
 * File: SearchBar.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 10:54:05 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:38:55 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */
import React from 'react';
import { TextInput, View, TouchableOpacity, Animated, Keyboard } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../config/colors';

const INITIAL_TOP = -60;

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: props.showOnLoad,
      top: new Animated.Value(
        INITIAL_TOP
      ),
      text:''
    };
    this.navigator = this.props.navigator;
  }

  show = () => {
    const { animate = true, animationDuration = 0 } = this.props;
    // if (clearOnShow) {
    //   this.setState({ input: '' });
    // }
    this.setState({ show: true });
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: 0,
        duration: animationDuration
      }).start();
    } else {
      this.setState({ top: new Animated.Value(0) });
    }
    this.textInput.focus();
  };

  hide = () => {
    this.setState({ text: '' });
    const { animate = true, animationDuration = 0 } = this.props;
    // if (onHide) {
    //   onHide(this.state.input);
    // }
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: INITIAL_TOP,
        duration: animationDuration
      }).start();

      // const timerId = setTimeout(() => {
      //   this._doHide();
      //   clearTimeout(timerId);
      // }, animationDuration);
    }
    Keyboard.dismiss();
  };

  _handleBlur = () => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur();
    }
  };

  render() {

    return (
      
      <Animated.View style={[styles.viewStyle, { top: this.state.top }]}>
        <View style={styles.searchBarContainer}>
          <View style={styles.searchBarIcon}>
            <MaterialIcons
              name={"search"}
              size={styles.inputFieldIconSize}
              color={Colors.grey}
            />
          </View>
          <TextInput 
            ref={ref => (this.textInput = ref)}
            style={styles.textInputStyleClass}
            onChangeText={(text) =>{ 
              this.setState({ text: text });
              this.props.onChangeText(text); 
            }}
            onSubmitEditing={Keyboard.dismiss}
            // onLayout={() => this.textInput.focus()}
            onBlur={this._handleBlur}
            value={this.state.text}
            underlineColorAndroid='transparent'
            placeholder="Search"
            keyboardType={'numeric'}
          />

          <TouchableOpacity style={styles.clearIcon} onPress={() => { 
            this.props.onPressClear();
            this.hide(); 
          }}>
            <MaterialIcons
              name={"clear"}
              size={styles.inputFieldIconSize}
              color={Colors.grey}
            />
          </TouchableOpacity>
          
        </View>
      </Animated.View>
    );
  }
}

const styles = {
  inputFieldIconSize: 30,
  viewStyle: {
    // flex:1,
    flexDirection: 'row',
    backgroundColor: Colors.navBarBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    // paddingTop: 15,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.5,
    // elevation: 2,
    position: 'absolute',
    // top: 0,
    left: 0,
    right:0,
    zIndex:9999
  },
  filterSearchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  filterButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  searchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  textStyle: {
    fontSize: 18,
    color: '#FFF',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  headerTitleView:{
    flex:5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    height: 55
  },
  backButtonView:{
    flex:1,
    alignItems: 'center',
    marginLeft: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  rightButtonsView:{
    flex:2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  textInputStyleClass:{    
    flex:9,
    textAlign: 'left',
    // height: 35,
    // borderWidth: 1,
    // borderColor: Colors.navBarBackgroundColor,
    // borderRadius: 7 ,

  },
  searchBarContainer:{
    flex:1,
    flexDirection: 'row',
    backgroundColor : Colors.white,
    margin: 10
  },
  searchBarIcon:{
    flex:1,
    margin: 5
    // backgroundColor : Colors.colorBlue
  },
  clearIcon:{
    flex:1,
    margin: 5
  }
};

export { SearchBar };
