/*
 * File: BillPaymentDetails.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 9th May 2018 7:37:59 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 5:02:08 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from '../../../views/transactionHistory/Header';
import ActIndicator from '../ActIndicator';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/ActivationStatus';

/**
 * @description
 * @class BillPaymentDetails
 * @extends {React.Component}
 */
class BillPaymentDetails extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        screenTitle: strings.billPaymentHistoryTitle,
        ok:  strings.ok,
        connectionType: strings.connectionType,
        connectionNo: strings.connectionNo,
        transID: strings.transID,
        executedBy: strings.executedBy,  
        date: strings.date,
        time: strings.time,
        amount: strings.amount,
        backMessage: strings.backMessage
      },     
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }
 
  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    console.log('xxx handleBackButtonClick');
    this.okHandler();
    return true;
  }

  onPressOk = () => {
    console.log('xxx onPressOk');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }
 
  render() {
    let loadingIndicator;
    let item = this.props.item;
    let detailsArray = [
      {
        label: this.state.locals.connectionType,
        value: item.connectionType
      }, {
        label: this.state.locals.connectionNo,
        value: item.mobileNumber
      },
      {
        label: this.state.locals.transID,
        value: item.transID
      },
      {
        label: this.state.locals.executedBy,
        value: item.executedBy
      },
      {
        label: this.state.locals.date,
        value: item.date
      },
      {
        label: this.state.locals.time,
        value: item.time
      },
      {
        label: this.state.locals.amount,
        value: item.amount
      }
    ];

    if (this.props.api_loading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <View style={styles.iconContainer} /> 
        <View style={styles.detailsContainer}>
          <ScrollView>
            {detailsArray.map((value, i) => 
              <ElementItem 
                key={i} 
                leftTxt={value.label} rightTxt={value.value} 
                //Check last item and add more margin
                customContainerStyle={(detailsArray.length == i + 1) ? styles.detailElementContainerCustom : {}}/>)}
          </ScrollView>
        </View>
        <View style={styles.containerBottom}>
          <ElementFooterButton 
            okButtonText={this.state.locals.ok}
            onClickOk={() => this.onPressOk()}
            disabled={this.state.isDisableButtons}
          />
        </View>
      </View>

    );
  }
}

const ElementItem = ({ leftTxt, rightTxt, customContainerStyle={} }) => (
  <View style={[styles.detailElementContainer, customContainerStyle]}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementFooterButton = ({
  okButtonText,
  onClickOk,
  disabled = false,
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={disabled}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: BillPaymentDetails => TransactionHistory ', state.transHistory);
  const api_loading = state.transHistory.api_loading;
  const Language = state.lang.current_lang;

  return {  Language, api_loading };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  iconContainer: {
    flex: 0.4,
  },

  detailsContainer: {
    flex: 6,
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },
  containerBottom: {
    backgroundColor: Colors.colorTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },

  detailElementContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },

  detailElementContainerCustom: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10,
    marginTop: 25
  },

  elementLeft: {
    flex: 1.2,
    marginLeft: 10,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '500',
    color: Colors.colorBlackDelivery
  },
  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    flexDirection: 'row',
    padding: 0,
    paddingTop: 0,
    paddingBottom: 10,
  },
  dummyView: {
    flex: 1.5
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 48,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  }
});

export default connect(mapStateToProps, actions)(BillPaymentDetails);
