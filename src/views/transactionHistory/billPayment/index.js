/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:59:35 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { View, StyleSheet, BackHandler } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import { Header } from '../../../views/transactionHistory/Header';
import { SearchBar } from '../SearchBar';
import globalConfig from '../../../config/globalConfig';
import BillPaymentHistoryList from './BillPaymentHistoryList';
import strings from '../../../Language/ActivationStatus';

const pageLimit = 15;

/**
 * @description
 * @class BillPaymentHistoryMainView
 * @extends {React.Component}
 */
class BillPaymentHistoryMainView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals:{
        backMessage: strings.backMessageNew,
        headerTitle: strings.billPaymentHistoryTitle,
      },
      searchBarHidden: true
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    if (this.state.searchBarHidden == false){
      this.searchBar.hide();
      console.log('xxxx handleBackButtonClick :: searchBarHidden'),
      this.setState({ searchBarHidden: true });
      return true;
    }
    console.log('xxx handleBackButtonClick');
    this.okHandler();
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    this.props.resetBillPaymentSearchResults();
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.TransactionHistoryMainScreen',
      title: 'BillPaymentHistoryMainScreen',
      animated: true, 
      animationType: 'slide-up' 
    });
  }

  /**
 * @description Search bill payment history list
 * @param {String} text
 * @memberof BillPaymentHistoryMainView
 */
  searchRecords = async(text) => {
    console.log('xxx searchRecords');
    let data = { 
      "account_no": text,
      "start": 0,
      "limit": pageLimit
    };

    if (text === ''){
      data = { 
        "start": 0,
        "limit": pageLimit
      };
    }

    await this.props.resetBillPaymentSearchResults();
    this.props.getBillPaymentHistoryList(data);  
  }

  shouldHideSearch = () => {
    console.log("Should Hide Search");
    this.searchBar.hide();
    this.props.resetBillPaymentSearchResults();
    this.setState({ searchBarHidden: true }, ()=>{
      this.searchRecords('');
    });
  }

  /**
   * @description A custom debounce method to throttle user input in Search Bar
   * @param {Function} fn
   * @param {Number} delay
   * @returns
   * @memberof SimChangeHistoryMainView
   */
  debounce = (fn, delay) => {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          filterButtonPressed={() => this.handleFilter()}
          displaySearch 
          displayFilter 
          headerText={ this.state.locals.headerTitle}/>
        <SearchBar
          ref={(ref) => this.searchBar = ref}
          onChangeText={this.debounce(this.searchRecords, globalConfig.searchDebounceDelay)}
          onPressClear={()=>{ 
            this.searchBar.hide();
            this.props.resetBillPaymentSearchResults();
            this.setState({ searchBarHidden: true }, ()=>{
              this.searchRecords('');
            });
          }
          }
        />
        <BillPaymentHistoryList { ...this.props} shouldHideSearch={()=>this.shouldHideSearch()} />
      </View>
    );
  }

  /**
   * @description Show search bar
   * @memberof BillPaymentHistoryMainView
   */
  handleSearch = () =>{
    this.searchBar.show();
    this.setState({ searchBarHidden: false });
  }

  /**
   * @description Show filter screen
   * @memberof BillPaymentHistoryMainView
   */
  handleFilter(){
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'FILTER', 
      screen: 'DialogRetailerApp.views.FilterBillPaymentHistoryScreen',
      passProps: {
      }
    });
  }
}


const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TabBarAll => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const api_loading = state.transHistory.api_loading;
  
  return { Language, api_loading };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.colorWhite
  },
});


export default connect(mapStateToProps, actions)(BillPaymentHistoryMainView);