import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  DatePickerAndroid
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import { Header } from '../../../views/transactionHistory/Header';
import Styles from '../../../config/styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/ActivationStatus';

const Utill = new Utills();
const pageLimit = 15;


class FilterBillPaymentHistoryView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        screenTitle: strings.screenTitleFilter,
        backMessage: strings.backMessageBillPaymentHistory,
        type: strings.requestType,
        date: strings.date,
        lob: strings.lob,
        connectionType: strings.connectionType,
        filter: strings.screenTitleFilter,
        gsm: strings.gsm,
        lte: strings.lte,
        dtv: strings.dtv,
        prepaid: strings.prepaid,
        postpaid: strings.postpaid,
        btnClearAll: strings.btnClearAll,
        btnFilter: strings.btnFilter,
        please_fill_the_details: strings.please_fill_the_details,
        please_select_start_date:  strings.please_select_start_date,
        please_select_end_date: strings.please_select_end_date,      
      },
      selectedType: '',
      selectedStartDate: 'Starting Date',
      selectedEndDate: 'Finishing Date',
      disableFilter: false,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  render() {
    const CalendarSelectionStartDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElement}>
          <Text style={ 
            placeHolderText === 'Starting Date' ? styles.dropdownDataElementTxtPlaceHolder :
              styles.dropdownDataElementTxt}>
            {placeHolderText}
          </Text>
        </View>
        <View style={styles.dropdownArrow}>
          <View>
            <Ionicons  name='ios-arrow-down' size={20}/>
          </View>
        </View>
      </TouchableOpacity>
    );

    const CalendarSelectionFinishDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElement}>
          <Text style={ 
            placeHolderText === 'Finishing Date'  ? styles.dropdownDataElementTxtPlaceHolder :
              styles.dropdownDataElementTxt}>
            {placeHolderText}
          </Text>
        </View>
        <View style={styles.dropdownArrow}>
          <View>
            <Ionicons name='ios-arrow-down' size={20}/>
          </View>
        </View>
      </TouchableOpacity>
    );

    const ElementFooter = ({ okButtonText, onClickOk, disabled }) => (
      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={ okButtonText == 'CLEAR ALL'? styles.buttonClearContainer : styles.buttonContainer }
          onPress={onClickOk}
          activeOpacity={1}
          disabled={disabled}>
          <Text style={styles.buttonTxt}>{okButtonText}</Text>
        </TouchableOpacity>
      </View>
    );
    
    return (
      <View style={styles.mainContainer}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.filter}/>
        <View style={styles.container}>
          <View style={styles.dropDownTextContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.date}</Text>
          </View>
          <View style={styles.pickersView}>
            {/* START DATE */}
            <View style={styles.filterSection}>
              <CalendarSelectionStartDate
                placeHolderText={this.state.selectedStartDate}
                onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate')}/>
            </View>
            {/* END DATE */}
            <View style={styles.filterSection}>
              <CalendarSelectionFinishDate
                placeHolderText={this.state.selectedEndDate}
                onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate')}/>
            </View>
          </View>
          <View style={styles.footerSection}>
            <View style={styles.footerButtonView}>
              <ElementFooter
                okButtonText={this.state.locals.btnClearAll}
                onClickOk={() => this.clearAllButtonHandler()}/>
              <ElementFooter
                okButtonText={this.state.locals.btnFilter}
                onClickOk={() => this.filterButtonHandler()}
                disabled={this.state.disableFilter}/>
            </View>
          </View>
        </View>
      </View>
    );
  }

  /**
   * @description
   * @memberof FilterBillPaymentHistoryView
   */
  clearAllButtonHandler = () =>{
    this.setState({ selectedStartDate: 'Starting Date', selectedEndDate: 'Finishing Date' });
  }

  /**
   * @description Get data fill status
   * @memberof FilterBillPaymentHistoryView
   */
  getDataFillStatus = () =>{
    console.log('xxx getDataFillStatus');
    console.log(this.state.selectedStartDate !== 'Starting Date');
    console.log(this.state.selectedEndDate !== 'Finishing Date');
    return (
      this.state.selectedStartDate !== 'Starting Date'
      || this.state.selectedEndDate !== 'Finishing Date');
  }

  /**
   * @description
   * @memberof FilterBillPaymentHistoryView
   */
  filterButtonHandler = () =>{

    let self = this;
    this.setState({ disableFilter: true }, async() => { 
      if (!this.getDataFillStatus()) {
        Utill.showAlertMsg(this.state.locals.please_fill_the_details);
        self.setState({ disableFilter: false });
        return;
      }
      if (this.state.selectedStartDate == 'Starting Date') {
        Utill.showAlertMsg(this.state.locals.please_select_start_date);
        self.setState({ disableFilter: false });
        return;
      }
      if (this.state.selectedEndDate == 'Finishing Date') {
        Utill.showAlertMsg(this.state.locals.please_select_end_date);
        self.setState({ disableFilter: false });
        return;
      }

      await self.props.resetBillPaymentSearchResults();


      let data = {
        start_date: this.state.selectedStartDate,
        end_date: this.state.selectedEndDate,
        limit: pageLimit,
        start: 0
      };

      await self.props.getBillPaymentHistoryList(data);
      console.log('filter data ', data);
      const navigatorOb = this.props.navigator; 
      //hide filter view and go back to list view
      navigatorOb.pop({ animated: true, animationType: 'fade' }); 
    });

    
  }

  async openDatePicker(selStartDate, selEndDate, dateType){
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'');
    
    var initialDate;
    var minDate = new Date().setDate((new Date().getDate() - 30));
    var maxDate = new Date();

    console.log('this.state.selectedStartDate ' + selStartDate.slice(0,10));
    console.log('this.state.selectedEndDate', selEndDate.slice(0,10));

    let formattedStartDate = selStartDate.slice(0,10); 
    let formattedEndDate = selEndDate.slice(0,10); 

    console.log('formattedStartDate ' + new Date(formattedStartDate));
    console.log('formattedEndDate ', new Date(formattedEndDate));
    console.log('utc: ',utc);

    if (dateType == 'startDate'){
      if (this.state.selectedEndDate !== 'Finishing Date'){
        maxDate = new Date(formattedEndDate);
      }
      let stDate = selStartDate.slice(0,10);
      let stYear = stDate.slice(0,4);
      let stMonth = parseInt(stDate.slice(5,7)) - 1;
      let stDay = stDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', stYear+stMonth+stDay);
      initialDate =  (selStartDate == ''? new Date(utc) : new Date(stYear,stMonth,stDay));
    } else {
      if (this.state.selectedStartDate !== 'Starting Date'){
        minDate = new Date(formattedStartDate);
      }
      let enDate = selEndDate.slice(0,10);
      let enYear = enDate.slice(0,4);
      let enMonth = parseInt(enDate.slice(5,7)) - 1;
      let enDay = enDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', enYear+enMonth+enDay);
      initialDate =  (selEndDate == ''? new Date(utc) : new Date(enYear,enMonth,enDay));
    }

    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        minDate: minDate,
        date: initialDate,
        maxDate: maxDate,
        mode:'calendar'
      });
      
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        let selectedMonth = (month<10?'0'+(month+1):(month+1));
        let selectedDay = (day<10?'0'+day:day);

        // let selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 00:00:00';
        let selectedDate = year+'-'+selectedMonth+'-'+selectedDay;

        if (dateType == 'startDate'){
          this.setState({ selectedStartDate: selectedDate });
        } else {
          // if (this.state.selectedStartDate === selectedDate){
          // selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 23:59:59';
          selectedDate = year+'-'+selectedMonth+'-'+selectedDay;

          // }
          this.setState({ selectedEndDate: selectedDate });
        }
        // this.openTimePicker(unformattedSelectedDate, dateType);
      }
    } catch ({ code, message }) {
      console.log('Cannot open date picker', message);
    }
  }

}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: billPayment :: FilterBillPaymentHistoryView  => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({
  mainContainer:{ 
    flex:1, 
    backgroundColor: Colors.appBackgroundColor 
  },
  pickersView:{ 
    flex:1, 
    flexDirection:'row' 
  },
  container: {
    flex: 0.8,
    backgroundColor: Colors.appBackgroundColor,
    paddingBottom: 20
  },
  filterSection:{
    flex:1,
    flexDirection: 'column'
  },
  footerSection:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    // backgroundColor: 'pink'
  },
  footerButtonView:{ 
    flex:0.9, 
    flexDirection: 'row', 
    justifyContent: 'flex-end'
  },
  dropDownTextContainer:{
    flex: 0.35,
    paddingTop:40
    //backgroundColor: 'pink'
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 20,
    // fontWeight: 'bold'

  },
  //button styles
  bottomContainer: {
    height: 55,
    flex: 1,

    backgroundColor: Colors.appBackgroundColor,
    // flexDirection: 'row',
    padding: 5,
    // paddingTop: 15,
    // paddingBottom: 15,
    marginRight: 0
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    // marginRight: 10,
    // alignSelf: 'flex-end'
  },
  buttonClearContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },
  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElement: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElementTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  },
  dropdownDataElementTxtPlaceHolder:{
    color: Colors.borderColorGray,
    fontSize: 15,
    fontWeight: '400'
  }
});

export default connect(mapStateToProps, actions)(FilterBillPaymentHistoryView);
