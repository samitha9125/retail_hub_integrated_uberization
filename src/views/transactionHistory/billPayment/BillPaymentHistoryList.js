/*
 * File: BillPaymentHistoryList.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 5:01:25 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import EmptyFeed from '@Components/EmptyFeed';
import strings from '../../../Language/ActivationStatus';
import icon_mobile_src from '../../../../images/common/icons/icon_mobile_black.png';
import icon_dtv_src from '../../../../images/common/icons/icon_dtv_black.png';
import icon_lte_src from '../../../../images/common/icons/icon_broadband_black.png';
import globalConfig from '../../../config/globalConfig';

import _ from 'lodash';

const pageLimit = globalConfig.transactionHistoryLoadLimit;


/**
 * @description
 * @class BillPaymentHistoryList
 * @extends {React.Component}
 */
class BillPaymentHistoryList extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      dataArray: [],
      start: 0,
      isLoading: false,
      locals: {
        errorDesc: strings.errorDesc,
      }
    };

    this.debouncedLoadLisItem = _.debounce((item)=>this.onListItemTap(item),globalConfig.listItemTapInterval,
      { 'leading': true,
        'trailing': false });
    this.debouncedHandleLoadMore = _.debounce(this.handleLoadMore,250,
      { 'leading': true,
        'trailing': false });

  }

  componentDidMount() {
    console.log('### ActivationStatusScreen :: componentDidMount');
    this.callApi();
  }

  componentDidUpdate() {
    console.log('xxxxx componentDidUpdate');
  }

  /**
   * @description Call bill payment history list API
   * @memberof BillPaymentHistoryList
   */
  callApi = () => {
    console.log('xxxx callApi');
    // this.props.resetBillPaymentSearchResults();
    let data = {
      start : this.state.start,
      limit :pageLimit,
    };

    // Check if an search input exists
    if (this.props.bill_payment_history_search_string !== ''){
      if (this.props.bill_payment_history_search_string.account_no){
        console.log("Has search string account number");
        let params = {
          account_no: this.props.bill_payment_history_search_string.account_no,
          limit: this.props.bill_payment_history_search_string.limit,
          start: parseInt(this.props.bill_payment_history_search_string.start) + pageLimit
        };
        data = { ...data,...params };
      }

      // Check if a filter query exists
      if (this.props.bill_payment_history_search_string.start_date && this.props.bill_payment_history_search_string.end_date){
        let params = {
          start_date: this.props.bill_payment_history_search_string.start_date,
          end_date: this.props.bill_payment_history_search_string.end_date,
          limit: this.props.bill_payment_history_search_string.limit,
          start: parseInt(this.props.bill_payment_history_search_string.start) + pageLimit
        };
        data = { ...data,...params };
      }
    }

    this.props.getBillPaymentHistoryList(data);
    Analytics.logEvent('dsa_bill_payment_history_api');

  }

  /**
   * @description Handel list item tap event
   * @param {Object} { item }
   * @memberof BillPaymentHistoryList
   */
  onListItemTap = (item) => {
    console.log('xxxx onListItemTap', item);
    const navigatorOb = this.props.navigator; 
    navigatorOb.push({
      title: 'SimChangeDetailsScreen',
      screen: 'DialogRetailerApp.views.BillPaymentDetailsScreen',
      passProps: {
        item: item
      }
    });
  };

  /**
   * @description Get lob icon 
   * @param {String} lob
   * @returns {Icon} icon 
   * @memberof BillPaymentHistoryList
   */
  getLobIcon = (lob) => {
    console.log('xxxx getLobIcon', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = mobileIcon;
        break;
      case 'DTV':
        icon = tvIcon;
        break;
      case 'LTE':
        icon = broadbandIcon;
        break;       
      default:
        icon = mobileIcon;
        break;
    }
    return icon;
  }

  /**
   * @description Get lob image source 
   * @param {String} lob
   * @returns {String} icon 
   * @memberof BillPaymentHistoryList
   */
  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = icon_mobile_src;
        break;
      case 'DTV':
        icon = icon_dtv_src;
        break;
      case 'LTE':
        icon = icon_lte_src;
        break;       
      default:
        icon = icon_mobile_src;
        break;
    }
    return icon;
  }

  /**
   * @description Handle pull to refresh
   * @memberof BillPaymentHistoryList
   */
  pullToRefresh = () => {
    console.log('xxx pullToRefresh');
    console.log('props: ', this.props);

    if (this.props.shouldHideSearch){
      this.props.shouldHideSearch();
      return;
    }

    this.setState({
      start: 0
    }, async() => {
      await this.props.transResetAll();
      await this.callApi();      
    });
  }
 
  /**
 * @description Handle infinite scroll
 * @param {String} distanceFromEnd
 * @memberof BillPaymentHistoryList
 */
  handleLoadMore = () => {
    let listData = [];

    if ( this.props.bill_payment_history_list !== undefined
      && this.props.bill_payment_history_list !== null 
      && this.props.bill_payment_history_list.length > 0 
      && this.props.bill_payment_history_search_string !== ''){

      listData = this.props.bill_payment_history_list;
    }
    if (this.props.bill_payment_history_list !== undefined 
      && this.props.bill_payment_history_list !== null
      && this.props.bill_payment_history_search_string === ''){

      listData = this.props.bill_payment_history_list;
    }

    if (this.props.bill_payment_history_list !== null 
      && this.props.bill_payment_history_list.length == 0 
      && this.props.bill_payment_history_search_string !== ''){

      listData = [];
    }
    console.log('xxx handleLoadMore');
    this.setState({
      start: listData.length
    }, () => {
      this.callApi();
    });
  }

  /**
 * @description Generate list item
 * @param {Object} { item }
 * @memberof BillPaymentHistoryList
 */
  renderListItem = ({ item }) => (
    <TouchableHighlight onPress={()=>{this.debouncedLoadLisItem(item);}}>
      <View style={styles.listView_row}>
        <View style={styles.listView_col1}>
          <Image source={this.getLobIconSrc(item.lob)} style={styles.imageIconStyle}/>
        </View>
        <View style={styles.listView_col2}>
          <Text style={styles.rowText}>{item.mobileNumber}</Text>
        </View>
        <View style={styles.listView_col3}>
          <Text style={styles.rowTextIssue}>{item.amount}</Text>
        </View>
        <View style={styles.listView_col5}>
          <View style={styles.listView_col5_1}>
            <Text style={styles.rowTextDate}>{item.date}</Text>
            <Text style={styles.rowTextTime}>{item.time}</Text>
          </View>
        </View>
        <View style={styles.listView_col6}>
          {forwardIcon}
        </View>
      </View>
    </TouchableHighlight>
  );

  render() {
    let listData = [];

    if ( this.props.bill_payment_history_list !== undefined
      && this.props.bill_payment_history_list !== null 
      && this.props.bill_payment_history_list.length > 0 
      && this.props.bill_payment_history_search_string !== ''){

      listData = this.props.bill_payment_history_list;
    }
    if (this.props.bill_payment_history_list !== undefined 
      && this.props.bill_payment_history_list !== null
      && this.props.bill_payment_history_search_string === ''){

      listData = this.props.bill_payment_history_list;
    }

    if (this.props.bill_payment_history_list !== null 
      && this.props.bill_payment_history_list.length == 0 
      && this.props.bill_payment_history_search_string !== ''){

      listData = [];
    }

    return (
      <View style={styles.containerTab}>
        <FlatList
          data={listData}
          renderItem={this.renderListItem}
          refreshing={this.props.api_loading}
          onRefresh={this.pullToRefresh}
          onEndReached={({ distanceFromEnd }) => {
            if (distanceFromEnd >= 0) {
              console.log('in  condition on end reached call api', distanceFromEnd);
              console.log('in  condition this.props.bill_payment_history_list', this.props.bill_payment_history_list.length);
              
              //call API to get next page values
              this.debouncedHandleLoadMore();
            }
            console.log('on end reached ', distanceFromEnd);
          }}
          onEndReachedThreshold ={0.05}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => {
            if (this.props.api_loading)
              return null;
            return (
              <EmptyFeed
                text={this.state.locals.errorDesc}
                messageTxtStyle={styles.errorDescText}
              />
            );
          }
          }
          scrollEnabled={true}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: billPayment :: BillPaymentHistoryList => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const api_loading = state.transHistory.api_loading;
  const bill_payment_history_list = state.transHistory.bill_payment_history_list;
  const bill_payment_history_search_string = state.transHistory.bill_payment_history_search_string;

  return { 
    Language, 
    api_loading, 
    bill_payment_history_list,
    bill_payment_history_search_string 
  };
};

const styles = StyleSheet.create({

  containerTab: {
    flex: 1
  },
  listView_row: {
    flexDirection: 'row',
    height: 50,
    borderWidth: 0.5,
    borderColor: Colors.borderLineColor,
    backgroundColor: Colors.colorWhite
  },
  listView_col1: {
    flex: 1.2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20
  },
  listView_col2: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 10
  },

  listView_col3: {
    flex: 2.5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 15
  },

  listView_col5: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 8,
    paddingTop: 8
  },

  listView_col5_1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  listView_col6: {
    flex: 0.6,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingRight: 10
  },
  rowText: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 15,
    color: Colors.colorBlack,
    textAlign: 'left',
    fontWeight: '400'
  },

  rowTextDate: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },

  rowTextTime: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },
  iconStyle: {
    textAlign: 'center',
    marginLeft: 10
  },

  imageIconStyle: {
    width: 22,
    height: 22,
    margin: 5
  },
  errorDescText:{ 
    textAlign: 'center', 
    fontSize: 17, 
    fontWeight:'bold', 
    color: Colors.black 
  }
});

const mobileIcon = (<Icon
  name="old-mobile"
  size={22}
  color="black"
  style={styles.iconStyle}/>);
const tvIcon = (<Icon
  name="tv"
  size={18}
  color="black"
  style={styles.iconStyle}/>);
const broadbandIcon = (<Icon3
  name="radio-tower"
  size={20}
  color="black"
  style={styles.iconStyle}/>);
const forwardIcon = (<Icon2
  name="ios-arrow-forward"
  size={25}
  color="black"
  style={styles.iconStyle}/>);


export default connect(mapStateToProps, actions)(BillPaymentHistoryList);
