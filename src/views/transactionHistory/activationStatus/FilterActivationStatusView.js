/*
 * File: FilterActivationStatusView.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:22:10 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  Dimensions,
  TouchableOpacity,
  DatePickerAndroid,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from '../../../views/transactionHistory/Header';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import strings from '../../../Language/ActivationStatus';

import globalConfig from '../../../config/globalConfig';

const pageLimit = globalConfig.transactionHistoryLoadLimit;

const Utill = new Utills();

/**
 * @description
 * @class FilterActivationStatusView
 * @extends {React.Component}
 */
class FilterActivationStatusView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        screenTitle: strings.screenTitleFilter,
        backMessage: strings.backMessageActivationStatus,
        type: strings.requestType,
        date: strings.date,
        lob: strings.lob,
        connectionType: strings.connectionType,
        filter: strings.screenTitleFilter,
        gsm: strings.gsm,
        lte: strings.lte,
        dtv: strings.dtv,
        prepaid: strings.prepaid,
        postpaid: strings.postpaid,
        btnClearAll: strings.btnClearAll,
        btnFilter: strings.btnFilter,
        please_fill_the_details: strings.please_fill_the_details,
        please_select_start_date:  strings.please_select_start_date,
        please_select_end_date: strings.please_select_end_date,
      },

      selectedType: '',
      selectedStartDate: 'Starting Date',
      selectedEndDate: 'Finishing Date',
      selectedLobType: 'GSM',
      selectedConnectionType: 'P',
      selectedLobTypeIndex: 0,
      selectedConnectionTypeIndex: 0,
      disableFilter: false,
      selectedLobTypeArray : [ 'GSM',  'LTE',  'DTV'],
      selectedConnectionTypeArray : [ 'P',  'O'],
      didStartDateSelected: false,
      didEndDateSelected: false,
      didItemSelected: false
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  selectFilterLobType = (idx, value) => {
    console.log('xxx selectFilterLobType', value);
    this.setState({ selectedLobType: this.state.selectedLobTypeArray[idx], selectedLobTypeIndex: idx, didItemSelected: true });
  }

  selectFilterConnectionType = (idx, value) => {
    console.log('xxx selectFilterConnectionType', value);
    this.setState({ selectedConnectionType: this.state.selectedConnectionTypeArray[idx],  selectedConnectionTypeIndex: idx, didItemSelected: true  });
  }


  render() {
    let dropDownDataLobType = [ this.state.locals.gsm,  this.state.locals.lte,  this.state.locals.dtv]; 
    let dropDownDataConnectionType = [ this.state.locals.prepaid,  this.state.locals.postpaid]; 

    let selectedLobTypeLabel = dropDownDataLobType[parseInt(this.state.selectedLobTypeIndex)];
    let selectedConnectionType= dropDownDataConnectionType[parseInt(this.state.selectedConnectionTypeIndex)];

    const CalendarSelectionStartDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElement}>
          <Text style={ 
            placeHolderText === 'Starting Date' ? styles.dropdownDataElementTxtPlaceHolder :
              styles.dropdownDataElementTxt}>
            {placeHolderText}
          </Text>
        </View>
        <View style={styles.dropdownArrow}>
          <View>
            <Ionicons name='md-calendar' size={30}/>
          </View>
        </View>
      </TouchableOpacity>
    );

    const CalendarSelectionFinishDate = ({ placeHolderText, onSelect }) => (
      <TouchableOpacity onPress={() => onSelect()} style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElement}>
          <Text style={ 
            placeHolderText === 'Finishing Date'  ? styles.dropdownDataElementTxtPlaceHolder :
              styles.dropdownDataElementTxt}>
            {placeHolderText}
          </Text>
        </View>
        <View style={styles.dropdownArrow}>
          <View>
            <Ionicons name='md-calendar' size={30}/>
          </View>
        </View>
      </TouchableOpacity>
    );

    const ElementSelectionMenu = ({ dropDownData, onSelect, defaultIndex, selectedValue, customDropdownStyle={} }) => (
      <ModalDropdown
        options={dropDownData}
        onSelect={(idx, value) => onSelect(idx, value)}
        style={styles.modalDropdownStyles}
        disabled={false}
        defaultIndex={parseInt(defaultIndex)} 
        dropdownStyle={[styles.dropdownStyle, customDropdownStyle]}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElement}>
            <Text style={styles.dropdownDataElementTxt}>{selectedValue}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20}/>
          </View>
        </View>
      </ModalDropdown>
    );

    const ElementFooter = ({ okButtonText, onClickOk, disabled, inactiveButtonType=false }) => (
      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={ inactiveButtonType ? styles.buttonClearContainer : styles.buttonContainer }
          onPress={onClickOk}
          activeOpacity={1}
          disabled={disabled}>
          <Text style={styles.buttonTxt}>{okButtonText}</Text>
        </TouchableOpacity>
      </View>
    );
    
    return (
      <View style={styles.mainContainer}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/>
        <View style={styles.container}>
          {/* LINE OF BUSINESS TYPE SECTION */}
          <View style={styles.textLabelContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.lob}</Text>
          </View>
          <View style={styles.selectionView}>    
            <ElementSelectionMenu
              dropDownData={dropDownDataLobType}
              defaultIndex={this.state.selectedLobTypeIndex}
              onSelect={this.selectFilterLobType}
              selectedValue={selectedLobTypeLabel}
              customDropdownStyle={styles.customDropdownStyle}/>   
          </View>
          {/* CONNECTION TYPE SECTION */}
          <View style={styles.textLabelContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.connectionType}</Text>
          </View>
          <View style={styles.selectionView}>   
            <ElementSelectionMenu
              dropDownData={dropDownDataConnectionType}
              defaultIndex={this.state.selectedConnectionTypeIndex}
              onSelect={this.selectFilterConnectionType}
              selectedValue={selectedConnectionType}/>   
          </View>
          {/* DATE FILTER SECTION */}
          <View style={styles.textLabelContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.date}</Text>
          </View>
          <View style={styles.pickersView}>
            {/* START DATE */}
            <View style={styles.filterSection}>
              <CalendarSelectionStartDate
                placeHolderText={this.state.selectedStartDate}
                onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate')}/>
            </View>
            {/* END DATE */}
            <View style={styles.filterSection}>
              <CalendarSelectionFinishDate
                placeHolderText={this.state.selectedEndDate}
                onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate')}/>
            </View>
          </View>
          <View style={styles.footerSection}>
            <View style={styles.footerButtonView}>
              <ElementFooter
                inactiveButtonType
                okButtonText={this.state.locals.btnClearAll}
                onClickOk={() => this.clearAllButtonHandler()}/>
              <ElementFooter
                okButtonText={this.state.locals.btnFilter}
                onClickOk={() => this.filterButtonHandler()}
                disabled={this.state.disableFilter}/>
            </View>
          </View>
        </View>
      </View>
    );
  }

  /**
   * @description Get data fill status
   * @memberof FilterActivationStatusView
   */
  getDataFillStatus = () =>{
    return (this.state.didItemSelected 
      || this.state.selectedLobTypeIndex !== -1
      ||  this.state.selectedConnectionTypeIndex !== -1 );
  }

  /**
   * @description Clear All ButtonHandler
   * @memberof FilterActivationStatusView
   */
  clearAllButtonHandler = () =>{
    this.setState({ 
      selectedStartDate: 'Starting Date', 
      selectedEndDate: 'Finishing Date', 
      selectedLobType: '', 
      selectedConnectionType: '',
      selectedLobTypeIndex: -1, 
      selectedConnectionTypeIndex: -1,
      didStartDateSelected: false,
      didEndDateSelected: false,
      didItemSelected: false
    });

    this.props.transSetActivationStatusFilter(null);
  }

  /**
   * @description Filter button handler
   * @memberof FilterActivationStatusView
   */
  filterButtonHandler = async() =>{
    let self = this;
    console.log('xxx filterButtonHandler');
    this.setState({ disableFilter: true }, async() => {

      if (!this.getDataFillStatus()) {
        Utill.showAlertMsg(this.state.locals.please_fill_the_details);
        self.setState({ disableFilter: false });
        return;
      }

      if (!this.state.didStartDateSelected && this.state.didEndDateSelected ) {
        Utill.showAlertMsg(this.state.locals.please_select_start_date);
        self.setState({ disableFilter: false });
        return;
      }

      if (this.state.didStartDateSelected && !this.state.didEndDateSelected ) {
        Utill.showAlertMsg(this.state.locals.please_select_end_date);
        self.setState({ disableFilter: false });
        return;
      }

      let filter={};

      if (this.state.selectedLobTypeIndex !== -1){
        filter.lob = this.state.selectedLobType;
      }

      if (this.state.selectedConnectionTypeIndex !== -1){
        filter.conn_type = this.state.selectedConnectionType;
      }

      if (this.state.didStartDateSelected && this.state.didEndDateSelected){
        filter.start_date = this.state.selectedStartDate;
        filter.end_date = this.state.selectedEndDate;
      }

      const data = {
        start : 0,
        limit :pageLimit,
        filter : filter
      };

      console.log('filter data ', data);
      const navigatorOb = this.props.navigator; 

      const successCb = (response) => {
        console.log('filter :: successCb', response);
        // self.setState({ disableFilter: false });      
      };

      const errorCb = (response) => {
        console.log('filter :: errorCb', response);
        // self.setState({ disableFilter: false });
        // let error = response;
        // if (response.data != undefined && response.data != null ) {
        //   error = response.data.error;
        // }

        // console.log('xxx errorCb :: error ', error);
        // Utill.showAlertMsg(error);     
      };

      let isFirstLoading = true;
      //Clear old states
      await this.props.transResetAll();
      await this.props.transSetActivationStatusFilter(filter);
      await this.props.getActivationStatusAllList(data, successCb, errorCb, isFirstLoading);
      await this.props.getActivationStatusRejectedList(data, successCb, errorCb);
      //hide filter view and go back to list view
      navigatorOb.pop({ animated: true, animationType: 'fade' }); 

    });
  }



  /**
 * @description Open date picker
 * @param {String} selStartDate
 * @param {String} selEndDate
 * @param {String} dateType
 * @memberof FilterActivationStatusView
 */
  async openDatePicker(selStartDate, selEndDate, dateType){
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'');
    
    var initialDate;
    var minDate = new Date().setDate((new Date().getDate() - 30));
    var maxDate = new Date();

    console.log('this.state.selectedStartDate ' + selStartDate.slice(0,10));
    console.log('this.state.selectedEndDate', selEndDate.slice(0,10));

    let formattedStartDate = selStartDate.slice(0,10); 
    let formattedEndDate = selEndDate.slice(0,10); 

    console.log('formattedStartDate ' + new Date(formattedStartDate));
    console.log('formattedEndDate ', new Date(formattedEndDate));
    console.log('utc: ',utc);

    if (dateType == 'startDate'){
      if (this.state.selectedEndDate !== 'Finishing Date'){
        maxDate = new Date(formattedEndDate);
      }
      let stDate = selStartDate.slice(0,10);
      let stYear = stDate.slice(0,4);
      let stMonth = parseInt(stDate.slice(5,7)) - 1;
      let stDay = stDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', stYear+stMonth+stDay);
      initialDate =  (selStartDate == ''? new Date(utc) : new Date(stYear,stMonth,stDay));
    } else {
      if (this.state.selectedStartDate !== 'Starting Date'){
        minDate = new Date(formattedStartDate);
      }
      let enDate = selEndDate.slice(0,10);
      let enYear = enDate.slice(0,4);
      let enMonth = parseInt(enDate.slice(5,7)) - 1;
      let enDay = enDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', enYear+enMonth+enDay);
      initialDate =  (selEndDate == ''? new Date(utc) : new Date(enYear,enMonth,enDay));
    }

    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        minDate: minDate,
        date: initialDate,
        maxDate: maxDate,
        mode:'calendar'
      });
      
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ didItemSelected: true });
        // Selected year, month (0-11), day
        let selectedMonth = (month<10?'0'+(month+1):(month+1));
        let selectedDay = (day<10?'0'+day:day);
        // let selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 00:00:00';
        let selectedDate = year+'-'+selectedMonth+'-'+selectedDay;

        if (dateType == 'startDate'){
          this.setState({ selectedStartDate: selectedDate, didStartDateSelected: true });
        } else {
          // if (this.state.selectedStartDate === selectedDate){
          // selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 23:59:59';
          selectedDate = year+'-'+selectedMonth+'-'+selectedDay;

          // }
          this.setState({ selectedEndDate: selectedDate, didEndDateSelected: true });
        }
        // this.openTimePicker(unformattedSelectedDate, dateType);
      } else {
        if (dateType == 'startDate'){
          this.setState({ didStartDateSelected: false });
        } else {
          this.setState({ didEndDateSelected: false });
        }
      }
    } catch ({ code, message }) {
      console.log('Cannot open date picker', message);
      if (dateType == 'startDate'){
        this.setState({ didStartDateSelected: false });
      } else {
        this.setState({ didEndDateSelected: false });
      }
    }
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: FilterActivationStatusView  => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({
  mainContainer:{ 
    flex:1, 
    backgroundColor: Colors.appBackgroundColor 
  },
  container: {
    flex: 0.9,
    backgroundColor: Colors.appBackgroundColor,
    paddingBottom: 20
  },

  textLabelContainer:{
    flex: 0.35,
    paddingTop:40,
    marginBottom: 8
  },

  selectionView:{ 
    flex:1, 
    flexDirection:'row' 
  },
  pickersView:{ 
    flex:1, 
    flexDirection:'row' 
  },

  footerSection:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: 10,
    marginTop: 15
  },

  filterSection:{
    flex:1,
    flexDirection: 'column'
  },

  footerButtonView:{ 
    flex:0.9, 
    flexDirection: 'row', 
    justifyContent: 'flex-end'
  },

  //modal dropdown styles
  modalDropdownStyles: {
    width: Dimensions.get('window').width 
  },

  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 20,
    // fontWeight: 'bold'

  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },
  dropdownStyle: {
    width: Dimensions.get('window').width - 30 ,
    height: 80,
    marginLeft: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },

  customDropdownStyle: {
    height: 120,
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  //button styles
  bottomContainer: {
    height: 55,
    flex: 1,

    backgroundColor: Colors.appBackgroundColor,
    // flexDirection: 'row',
    padding: 5,
    // paddingTop: 15,
    // paddingBottom: 15,
    marginRight: 0
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    // marginRight: 10,
    // alignSelf: 'flex-end'
  },

  buttonClearContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5
  },

  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },

  dropdownDataElement: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5
  },

  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },

  dropdownDataElementTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  },
  
  dropdownDataElementTxtPlaceHolder:{
    color: Colors.borderColorGray,
    fontSize: 15,
    fontWeight: '400'
  }
});

export default connect(mapStateToProps, actions)(FilterActivationStatusView);
