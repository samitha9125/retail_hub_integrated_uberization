/*
 * File: TabBarAll.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 16th May 2018 3:07:45 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 3:43:54 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import strings from '../../../Language/ActivationStatus';
import Images from '@Config/images';
import EmptyFeed from '@Components/EmptyFeed';
import icon_mobile_src from '../../../../images/common/icons/icon_mobile_black.png';
import icon_dtv_src from '../../../../images/common/icons/icon_dtv_black.png';
import icon_lte_src from '../../../../images/common/icons/icon_lte_black.png';
import globalConfig from '../../../config/globalConfig';

import _ from 'lodash';
const pageLimit = globalConfig.transactionHistoryLoadLimit;

/**
 * @description
 * @class TabBarAll
 * @extends {React.Component}
 */
class TabBarAll extends React.Component {
  constructor(props) {
    console.log('xxx TabBarAll ::  constructor');
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      dataArray: [],
      processingData: false,
      start: 0,
      isLoading: false,
      locals: {
        nicFront: strings.nicFront,
        nicBack: strings.nicBack,
        pp: strings.pp,
        dl: strings.dl,
        pob: strings.pob,
        customerImage: strings.customerImage,
        ack: strings.ack,
        pendingTxt: strings.pendingTxt,
        approvedTxt: strings.approvedTxt,
        rejectedTxt: strings.rejectedTxt,
        errorDesc: strings.errorDesc,
      }
    };
    this.debouncedLoadLisItem = _.debounce((item)=>this.onListItemTap(item),globalConfig.searchDebounceDelay,
      { 'leading': true,
        'trailing': false });

    this.debouncedHandleLoadMore = _.debounce(this.handleLoadMore,250,
      { 'leading': true,
        'trailing': false });
  }

  componentDidMount() {
    console.log('### ActivationStatusScreen :: componentDidMount');
    // if (this.props.activation_status_history_rejected_search_string === '')
    // this.callApi();
  }

  componentDidUpdate() {
    console.log('xxxxx componentDidUpdate');
  }

  /**
   * @description Activation status all list backend API call
   * @memberof TabBarAll
   */
  callApi = (isFirstLoading) => {
    console.log('xxxx callApi', isFirstLoading);
    console.log('this.props.activation_status_history_search_string', this.props.activation_status_history_search_string);
    let data = {
      start : this.state.start,
      limit :pageLimit,
      filter: this.props.transSetActivationStatusFilter
    };

    const successCb = (response) => {
      console.log('filter :: successCb', response);

    };

    const errorCb = (response) => {
      console.log('filter :: errorCb', response);
    };

    if (this.props.activation_status_history_search_string !== ''){
      if (this.props.activation_status_history_search_string.search){
        console.log("Has search string in activation status");
        data = {
          search: this.props.activation_status_history_search_string.search,
          limit: this.props.activation_status_history_search_string.limit,
          start: parseInt(this.props.activation_status_history_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }

      if (this.props.activation_status_history_search_string.filter){
        console.log("Has search string in activation status");
        data = {
          filter: this.props.activation_status_history_search_string.filter,
          limit: this.props.activation_status_history_search_string.limit,
          start: parseInt(this.props.activation_status_history_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }
    }

    this.props.getActivationStatusAllList(data, successCb, errorCb, isFirstLoading);

    Analytics.logEvent('dsa_activation_status_all');
    
  }

  /**
   * @description Handel list item tap event
   * @param {Object} { item }
   * @memberof TabBarAll
   */
  onListItemTap = (item) => {
    console.log('xxxx onListItemTap', item);
    let requestParms = { order_id: item.order_id };
    this.props.getActivationStatusDetailView(requestParms, this);

  };

  showDetailsScreen = (item) => {
    console.log('xxxx showDetailsScreen', item);
    const navigatorOb = this.props.navigator; 
    navigatorOb.push({
      title: 'SimChangeDetailsScreen',
      screen: 'DialogRetailerApp.views.ActivationStatusDetailsScreen',
      passProps: {
        item: item
      }
    });

  }

  /**
   * @description get LOB icon
   * @param {String} lob
   * @memberof TabBarAll
   */
  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = icon_mobile_src;
        break;
      case 'DTV':
        icon = icon_dtv_src;
        break;
      case 'LTE':
        icon = icon_lte_src;
        break;       
      default:
        icon = icon_mobile_src;
        break;
    }
    return icon;
  }

  /**
   * @description Get status image icon 
   * @param {String} status
   * @memberof TabBarAll
   */
  getStatusIconSrc = (status) => {
    console.log('xxxx getStatusIconSrc', status);
    if (status == 'APPROVED') {
      return Images.icons.Success_200px;
    } else if (status == 'PENDING') {
      return Images.icons.Pending_200px;
    } else if (status == 'REJECTED') {
      return Images.icons.Failure_200px;
    } else {
      return Images.icons.Pending_200px;
    }
  }
 
  /**
   * @description Handle pull to refresh
   * @memberof TabBarAll
   */
  pullToRefresh = () => {
    console.log('xxx pullToRefresh');
    let isFirstLoading = false;
    if (this.props.shouldHideSearch){
      this.props.shouldHideSearch();
      return;
    }

    this.setState({
      start: 0
    }, async() => {
      //clear filter values
      // this.props.resetActivationStatusSearchString();

      // this.props.transSetActivationStatusFilter(null);
      //Clear old states
      await this.props.transResetAll();
      await this.callApi(isFirstLoading);
    });
  }

/**
 * @description Handle infinite scroll
 * @param {String} distanceFromEnd
 * @memberof TabBarAll
 */
handleLoadMore = (distanceFromEnd) => {
  let { activation_status_all_list = [] } = this.props;
  let isFirstLoading = false;
  console.log('xxx handleLoadMore :: distanceFromEnd', distanceFromEnd);
  this.setState({
    start: activation_status_all_list.length
  }, () => {
    this.callApi(isFirstLoading);
  });
}

/**
 * @description Generate list item
 * @param {Object} { item }
 * @memberof TabBarAll
 */
renderListItem = ({ item }) => (
  <TouchableHighlight onPress={()=>{this.debouncedLoadLisItem(item);}}>
    <View style={styles.listView_row}>
      <View style={styles.listView_col1}>
        <Image source={this.getStatusIconSrc(item.order_status)} style={styles.imageStatusIconStyle}/>
      </View>
      <View style={styles.listView_col2}>
        <Text style={styles.rowText}>{item.connectionNo}</Text>
      </View>
      <View style={styles.listView_col3}>
        <Image source={this.getLobIconSrc(item.lob)} style={styles.imageIconStyle}/>
      </View>
      <View style={styles.listView_col5}>
        <View style={styles.listView_col5_1}>
          <Text style={styles.rowTextDate}>{item.date}</Text>
          <Text style={styles.rowTextTime}>{item.time}</Text>
        </View>
      </View>
      <View style={styles.listView_col6}>
        {forwardIcon}
      </View>
    </View>
  </TouchableHighlight>
);

render() {
  return (
    <View style={styles.containerTab}>
      <FlatList
        data={this.props.activation_status_all_list}
        renderItem={this.renderListItem}
        refreshing={this.props.api_loading}
        onRefresh={this.pullToRefresh}
        onEndReached={({ distanceFromEnd }) => {
          if (distanceFromEnd >= 0) {
            console.log('on end reached call api', distanceFromEnd);
            console.log('this.props.activation_status_all_list', this.props.activation_status_all_list.length);
              
            //call API to get next page values
            this.debouncedHandleLoadMore();
          }
          console.log('on end reached ', distanceFromEnd);
        }}
        onEndReachedThreshold ={0.05}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={() => {
          if (this.props.api_loading)
            return null;
          return (
            <EmptyFeed
              text={this.state.locals.errorDesc}
              messageTxtStyle={styles.errorDescText}
            />
          );
        }
        }
        scrollEnabled={true}/>
    </View>
  );
}
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: TabBarAll => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const api_loading = state.transHistory.api_loading;
  const activation_status_all_list = state.transHistory.activation_status_all_list;
  const transSetActivationStatusFilter = state.transHistory.transSetActivationStatusFilter;
  const activation_status_history_search_string = state.transHistory.activation_status_history_search_string;
  const activation_status_details = state.transHistory.activation_status_details;

  return { 
    Language, 
    api_loading , 
    activation_status_all_list, 
    transSetActivationStatusFilter, 
    activation_status_history_search_string,
    activation_status_details 
  };
};

const styles = StyleSheet.create({

  containerTab: {
    flex: 1
  },
  listView_row: {
    flexDirection: 'row',
    height: 50,
    borderWidth: 0.5,
    borderColor: Colors.borderLineColor,
    backgroundColor: Colors.colorWhite
  },
  listView_col1: {
    flex: 1.2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20
  },
  listView_col2: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 10
  },

  listView_col3: {
    flex: 2.5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 5,
    paddingTop: 5
  },

  listView_col5: {
    flex: 3.2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 8,
    paddingTop: 8
  },
  listView_col5_1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  listView_col6: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingRight: 10
  },
  rowText: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 15,
    color: Colors.colorBlack,
    textAlign: 'left',
    fontWeight: '400'
  },

  rowTextDate: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },

  rowTextTime: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },
  iconStyle: {
    textAlign: 'center',
    marginLeft: 10
  },

  imageStatusIconStyle : {
    width: 20,
    height: 20,
    margin: 5
  },

  imageIconStyle: {
    width: 28,
    height: 28,
    margin: 5
  },
  errorDescText:{ 
    textAlign: 'center', 
    fontSize: 18, 
    fontWeight:'bold', 
    color: Colors.black 
  }
});

const forwardIcon = (<Icon
  name="ios-arrow-forward"
  size={25}
  color="black"
  style={styles.iconStyle}/>);

export default connect(mapStateToProps, actions)(TabBarAll);
