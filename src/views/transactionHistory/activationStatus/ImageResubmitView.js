import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  BackHandler,
  Alert
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import CaptureId from './components/CaptureId';
import CaptureThumbnails from './components/CaptureThumbnails';
import ActIndicator from './../ActIndicator';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import { Header } from '../../common/components/Header';
import Utills from '../../../utills/Utills';
import { Analytics, Screen } from '../../../utills';
import { globalConfig, Constants, Colors, Styles } from '../../../config';
import strings from '../../../Language/ActivationStatus';
import strings_common from '../../../Language/Common';
import errorIcon from '../../../../images/common/icons/icon_state_fail.png';

const Utill = new Utills();

const DEFAULT_IMAGE_LIST = [false, false, false, false,  false, false, false, false];

class ImageResubmitView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    strings_common.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      data: false,
      isImageCaptured: false,
      enableButton: false,
      locals: {
        screenTitle: strings.activationDetailsTitle,
        activationStatusTitle: strings.activationStatusTitle,
        btnLater: strings.BtnLater,
        btnResubmit: strings.BtnResubmit,
        connectionNo: strings.connectionNo,
        pleaseCaptureAllImages: strings.pleaseCaptureAllImages,
        successfullyReSubmitted: strings.succesfullyReSubmitted,
        backMessage: strings.backMessage,
        capture:strings.capture,
        ok : strings.ok,
        cancel : strings.cancel,
        do_uou_want_to_cancel_image_capture_and_go_back: strings_common.do_uou_want_to_cancel_image_capture_and_go_back
      }
    };

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.commonResetKycImages();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
    this.props.commonResetKycImages();
  }

  onPressBack = () => {
    console.log('xxx onPressBack');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
    this.props.commonResetKycImages();
  }

  componentWillReceiveProps(nextProps) {
    console.log('xxx componentWillReceiveProps', nextProps);
    const rejectedImages = this.props.rejectedImages;
    const capturedImages = this.props.imageArray;
    let errorImgCount = 0;
    let capturedImagesCount = 0;

    for (const i of capturedImages) {
      console.log('xxxxxxx capturedImages', i);
      if (i === true) {
        capturedImagesCount++;
      }
    }
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    console.log(errorImgCount, capturedImagesCount);

    for (const i of rejectedImages) {
      console.log('xxxxxxx rejectedImages', i);
      errorImgCount++;

    }
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    console.log(errorImgCount, capturedImagesCount);

    if (capturedImagesCount > 0 && capturedImagesCount === errorImgCount) {
      this.setState({ readyToSubmit: true });
    } else {
      this.setState({ readyToSubmit: false });
    }
  }

  /**
   * @description Open image capture camera module
   * @param {Number} captureType
   * @memberof ImageResubmitView
   */
  onPressReCapture = (captureType) => {
    console.log('#### onPressReCapture : captureType => ', captureType);
    let _this = this;
    let screen = {
      title: 'Image Capture Module',
      id: 'DialogRetailerApp.views.ImageCaptureModule',
    };

    let passProps = {
      screenTitle: 'Image Capture Module',
      backMessage: _this.state.locals.do_uou_want_to_cancel_image_capture_and_go_back,
      imageCaptureTypeNo: captureType,
      lob: Constants.LOB_DTV,
      reCapture: true
    };

    Screen.pushView(screen, _this, passProps);
  }

  showActivationStatusScreen = () => {
    console.log('xxx showActivationStatusScreen');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      title: this.state.locals.activationStatusTitle,
      screen: 'DialogRetailerApp.views.ActivationStatusMainScreen',
      passProps: { }
    });
    this.props.transResetAll(); 
  }

  
  /**
   * @description Add all images to image upload thread (SQLite Db)
   * @param {Object} response
   * @memberof ImageResubmitView
   */
  addToImageUploadProcess = (response) => {
    console.log('xxx addToImageUploadProcess :: response', response);
    let me = this;
    let transactionId = me.props.selectedItem.transID;

    if (me.props.common.kyc_nic_front !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_FRONT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT,
        me.props.common.kyc_nic_front);
    }

    if (me.props.common.kyc_nic_back !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_BACK');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_BACK,
        me.props.common.kyc_nic_back);
    }

    if (me.props.common.kyc_driving_licence !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => DRIVING_LICENCE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE,
        me.props.common.kyc_driving_licence);
    }

    if (me.props.common.kyc_passport !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PASSPORT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PASSPORT,
        me.props.common.kyc_passport);
    }

    if (me.props.common.kyc_proof_of_billing !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PROOF_OF_BILLING');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING,
        me.props.common.kyc_proof_of_billing);
    }

    if (me.props.common.kyc_customer_image !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => CUSTOMER_IMAGE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE,
        me.props.common.kyc_customer_image);
    }

    if (me.props.common.kyc_signature !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => SIGNATURE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.SIGNATURE,
        me.props.common.kyc_signature);
    }

    if (me.props.common.kyc_additional_pob !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => ADDITIONAL_POB');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB,
        me.props.common.kyc_additional_pob);
    }

  }

  /**
   * @description Add to image upload SQLite db
   * @param {Number} txnId
   * @param {Number} imageId
   * @param {String} imageOb
   * @memberof ImageResubmitView
   */
  addToImageUploadQueue = async (txnId, imageId, imageOb) => {
    console.log('xxx RESUBMIT :: addToImageUploadQueue :: txnId, imageId, imageUri : ', txnId, imageId, imageOb);
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id = `${txnId}_${imageId}`;
      const url = Utill.createApiUrl('imageUploadEncResubmit', 'initAct', 'ccapp');
      const imageFile = imageOb.imageUri.replace('file://', '');
      const uploadSelector = 'encrypt';

      const options = {
        id: id,
        url: url,
        imageFile: imageFile,
        uploadSelector: uploadSelector,
        deviceData: JSON.stringify(deviceData)
      };

      console.log('xxx RESUBMIT :: addToImageUploadQueue :: options : ', options);

      const successCb = (msg) => {
        console.log('xxxx addToImageUploadQueue successCb');
        Analytics.logEvent('dsa_image_re_submit_add_success');
        console.log(msg);
      };

      const errorCb = (msg) => {
        console.log('xxxx addToImageUploadQueue errorCb ');
        Analytics.logEvent('dsa_image_re_submit_add_error');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, successCb);

    } catch (e) {
      console.log('addToImageUploadQueue Image Uploading Failed :', e.message);
      Analytics.logEvent('dsa_image_re_submit_sql_ex');
      this.showAlert(e.message);
    }
  }

  updateProcessSuccessCb = (response) => {
    console.log('xxx updateProcessSuccessCb :: response', response);
    this.addToImageUploadProcess(response);
    Screen.showDialogBox(this.state.locals.successfullyReSubmitted, '', this.showActivationStatusScreen, this.state.locals.ok );

  };

  updateResubmitProcess = () => {
    console.log('xxx updateResubmitProcess');
    let _this = this;
    if (!this.state.readyToSubmit) {
      console.log('xxx updateResubmitProcess :: SKIP API call');
      Utill.showAlertMsg(this.state.locals.pleaseCaptureAllImages);
      return;
    }
    const requestParams = {
      txId: this.props.selectedItem.transID,
      connection_number: this.props.selectedItem.connectionNo,
      order_id: this.props.selectedItem.order_id
    };

    Analytics.logEvent('dsa_activation_status_resubmit');
    this.props.transImageResubmitProcess(requestParams, _this);

  };

  render() {
    let buttonColor;
    if (this.state.readyToSubmit) {
      buttonColor = Colors.btnActive;
    } else {
      buttonColor = Colors.btnDisable;
    }

    let loadingIndicator;
    if (this.props.api_loading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    const selectedItem = this.props.selectedItem;
    let rejectedImages = this.props.rejectedImages;

   
    console.log('xxxx selectedItem', selectedItem);
    console.log('xxxx rejectedImages', rejectedImages);

    const setCaptureItem = (key, item, isImageCaptured, onPressReCapture) => {
      console.log('xxx setCaptureItem', item);
      if (isImageCaptured) {
        return (<CaptureThumbnails
          key={key}
          label={this.state.locals.capture + item.name}
          onPress={onPressReCapture}
        />);
      } else {
        return (<CaptureId
          key={key}
          label={this.state.locals.capture + item.name}
          onPress={onPressReCapture}
        />);
      }
    };

    return (
      <View style={styles.container}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {loadingIndicator}
        <View style={styles.containerImage}>
          <Image source={errorIcon} style={styles.imageStyle} />
        </View>
        <View style={styles.innerContainer}>
          <View style={styles.connectionContainer}>
            <View style={styles.connectionLabel}>
              <Text style={styles.connectionLabelLeftTxt}>
                {this.state.locals.connectionNo}</Text>
            </View>
            <View style={styles.connectionLabelMiddle}>
              <Text style={styles.connectionLabelMiddleTxt}>
                :</Text>
            </View>
            <View style={styles.connectionLabelRight}>
              <Text style={styles.connectionLabelRightTxt}>
                {selectedItem.connectionNo}</Text>
            </View>
          </View>
          {/*Set rejected image list */}
          {rejectedImages.map((value, i) =>
            setCaptureItem(i, value, this.props.imageArray[value.id - 1], () => this.onPressReCapture(value.id)))
          }
        </View>
        <View style={styles.bottomContainer}>
          <TouchableOpacity
            style={styles.laterButton}
            onPress={this.onPressBack}>
            <Text style={styles.laterButtonTxt}>{this.state.locals.btnLater}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={false}
            style={[
              styles.submitButton, {
                backgroundColor: buttonColor
              }
            ]}
            onPress={() => this.updateResubmitProcess()} >
            <Text style={[styles.submitButtonTxt]}>{this.state.locals.btnResubmit}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: ACTIVATION_STATUS => TransactionHistory :: transHistory\n', state.transHistory);
  console.log('****** REDUX STATE :: ACTIVATION_STATUS => ImageResubmitView :: common\n', state.common);

  const common = state.common;
  const api_loading = state.transHistory.api_loading;
  const Language = state.lang.current_lang;
  let imageArray = DEFAULT_IMAGE_LIST;

  imageArray[0]= common.kyc_nic_front !== null;
  imageArray[1]= common.kyc_nic_back !== null;
  imageArray[2]= common.kyc_passport !== null;
  imageArray[3]= common.kyc_driving_licence !== null;
  imageArray[4]= common.kyc_proof_of_billing !== null;
  imageArray[5]= common.kyc_customer_image !== null;
  imageArray[6]= common.kyc_signature !== null;
  imageArray[7]= common.kyc_additional_pob !== null;

  console.log('****** REDUX STATE :: imageArray ', imageArray);

  return {
    common,
    api_loading,
    Language,
    imageArray,
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  containerImage: {
    alignItems: 'center'

  },
  imageStyle: {
    width: 60,
    height: 60,
    marginTop: 20,
    marginBottom: 20
  },

  bottomContainer: {
    paddingRight: 5,
    paddingBottom: 10,
    margin: 10,
    marginRight: 0,
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },

  laterButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    width: '30%',
    marginRight: 10
  },
  laterButtonTxt: {
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent
  },

  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    borderRadius: 5,
    zIndex: 100,
    width: '40%',
    marginRight: 30
  },
  submitButtonTxt: {
    color: Colors.colorBlack,
    textAlign: 'center',
    backgroundColor: Colors.colorTransparent
  },
  innerContainer: {
    borderWidth: 0,
    padding: 15,
    marginLeft: 5
  },
  connectionContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginLeft: 5,
    marginBottom: 10
  },

  connectionLabel: {
    flex: 1,
    borderWidth: 0
  },

  connectionLabelLeftTxt: {
    fontSize: Styles.actStatus.defaultFontSize,
    fontWeight: 'bold'
  },

  connectionLabelMiddle: {
    borderWidth: 0,
    marginRight: 10
  },

  connectionLabelMiddleTxt: {
    fontSize: Styles.actStatus.defaultFontSize,
    fontWeight: 'bold'
  },

  connectionLabelRight: {
    flex: 1,
    borderWidth: 0
  },
  connectionLabelRightTxt: {
    fontSize: Styles.actStatus.defaultFontSize,
    fontWeight: 'bold'
  }
});

export default connect(mapStateToProps, actions)(ImageResubmitView);
