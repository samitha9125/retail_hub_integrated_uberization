/*
 * File: TabBarRejected.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 16th May 2018 3:07:45 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:16:25 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import strings from '../../../Language/ActivationStatus';
import Images from '@Config/images';
import EmptyFeed from '@Components/EmptyFeed';
import icon_mobile_src from '../../../../images/common/icons/icon_mobile_black.png';
import icon_dtv_src from '../../../../images/common/icons/icon_dtv_black.png';
import icon_lte_src from '../../../../images/common/icons/icon_broadband_black.png';
import globalConfig from '../../../config/globalConfig';

import _ from 'lodash';
const pageLimit = globalConfig.transactionHistoryLoadLimit;

/**
 * @description
 * @class TabBarRejected
 * @extends {React.Component}
 */
class TabBarRejected extends React.Component {
  constructor(props) {
    super(props);
    console.log('xxx TabBarRejected ::  constructor');    
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      dataArray: [],
      processingData: false,
      start: 0,
      isLoading: false,
      locals: {
        nicFront: strings.nicFront,
        nicBack: strings.nicBack,
        pp: strings.pp,
        dl: strings.dl,
        pob: strings.pob,
        customerImage: strings.customerImage,
        ack: strings.ack,
        pendingTxt: strings.pendingTxt,
        approvedTxt: strings.approvedTxt,
        rejectedTxt: strings.rejectedTxt,
        errorDesc: strings.errorDesc,
        ad_pob:strings.proof_of_installation
      }
    };

    this.debouncedLoadLisItem = _.debounce((item)=>this.onListItemTap(item), globalConfig.searchDebounceDelay,
      { 'leading': true,
        'trailing': false });

    this.debouncedHandleLoadMore = _.debounce(this.handleLoadMore,250,
      { 'leading': true,
        'trailing': false });

  }

  componentDidMount() {
    console.log('### ActivationStatusScreen :: componentDidMount');
    // if (this.props.activation_status_history_rejected_search_string === '')
    let isFirstLoading = true;
    this.callApi(isFirstLoading);
  }

  componentDidUpdate() {
    console.log('xxxxx componentDidUpdate');
  }
  
  /**
 * @description Activation status rejected list backend API call
 * @memberof TabBarRejected
 */
  callApi = (isFirstLoading) => {
    console.log('xxxx callApi', isFirstLoading);
    console.log('this.props.activation_status_history_rejected_search_string', this.props.activation_status_history_rejected_search_string);
    let data = {
      start : this.state.start,
      limit :pageLimit,
      filter: this.props.transSetActivationStatusFilter
    };

    const successCb = (response) => {
      console.log('filter :: successCb', response);

    };

    const errorCb = (response) => {
      console.log('filter :: errorCb', response);
    };

    if (this.props.activation_status_history_rejected_search_string !== ''){
      if (this.props.activation_status_history_rejected_search_string.search){
        console.log("Has search string in activation status");
        data = {
          search: this.props.activation_status_history_rejected_search_string.search,
          limit: this.props.activation_status_history_rejected_search_string.limit,
          start: parseInt(this.props.activation_status_history_rejected_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }

      if (this.props.activation_status_history_rejected_search_string.filter){
        console.log("Has search string in activation status");
        data = {
          filter: this.props.activation_status_history_rejected_search_string.filter,
          limit: this.props.activation_status_history_rejected_search_string.limit,
          start: parseInt(this.props.activation_status_history_rejected_search_string.start) + pageLimit
        };
        console.log("Params: ", data);
      }
    }

    this.props.getActivationStatusRejectedList(data, successCb, errorCb, isFirstLoading);
    // this.props.getActivationStatusAllList(data, successCb, errorCb);
    Analytics.logEvent('dsa_activation_status_rejected');

  }

  /**
   * @description Handel list item tap event
   * @param {Object} { item }
   * @memberof TabBarRejected
   */


  onListItemTap = (item) => {
    console.log('xxxx onListItemTap', item);
    let requstParms = { order_id: item.order_id };
    this.props.getActivationStatusDetailView(requstParms, this);

  };

  showDetailsScreen = (item) => {
    console.log('xxxx showDetailsScreen', item);
    const navigatorOb = this.props.navigator; 
    navigatorOb.push({
      title: 'SimChangeDetailsScreen',
      screen: 'DialogRetailerApp.views.ActivationStatusDetailsScreen',
      passProps: {
        item: item
      }
    });

  }
  
  /**
 * @description get LOB icon
 * @param {String} lob
 * @memberof TabBarRejected
 */
  getCaptureDescription = (value) => {
    let description;
    switch (value) {
      case 1:
        description = this.state.locals.nicFront;
        break;
      case 2:
        description = this.state.locals.nicBack;
        break;
      case 3:
        description = this.state.locals.pp;
        break;
      case 4:
        description = this.state.locals.dl;
        break;
      case 5:
        description = this.state.locals.pob;
        break;
      case 6:
        description = this.state.locals.customerImage;
        break;
      case 7:
        description = this.state.locals.ack;
        break;
      case 8:
        description = this.state.locals.ad_pob;
        break;
      default:
        description = this.state.locals.nicFront;
    }

    return description;
  };

  /**
 * @description Get status icon image
 * @param {String} status
 * @memberof TabBarRejected
 */
  getLobIconSrc = (lob) => {
    console.log('xxxx getLobIconSrc', lob);
    let icon;
    switch (lob) {
      case 'GSM':
        icon = icon_mobile_src;
        break;
      case 'DTV':
        icon = icon_dtv_src;
        break;
      case 'LTE':
        icon = icon_lte_src;
        break;       
      default:
        icon = icon_mobile_src;
        break;
    }
    return icon;
  }

  /**
   * @description Get status image icon 
   * @param {String} status
   * @memberof TabBarRejected
   */
  getStatusIconSrc = (status) => {
    console.log('xxxx getStatusIconSrc', status);
    if (status == 'APPROVED') {
      return Images.icons.Success_200px;
    } else if (status == 'PENDING') {
      return Images.icons.Pending_200px;
    } else if (status == 'REJECTED') {
      return Images.icons.Failure_200px;
    } else {
      return Images.icons.Failure_200px;
    }
  }

  /**
 * @description Get issue name
 * @param {Array} imageList
 * @memberof TabBarRejected
 */
  getIssueName = (imageList) => {
    console.log('xxxx getIssueName', imageList);
    const issueName = [];
    for (let prop in imageList) {
      if (!imageList.hasOwnProperty(prop)) {
        continue;
      }
      if (imageList[prop].imageStatus == 'REJECTED'){
        issueName.push(this.getCaptureDescription(imageList[prop].imageId));
      }

    }
    console.log('xxxx getIssueName :: issueName', issueName);
    return issueName.join();
  }
 
  /**
   * @description Handle pull to refresh
   * @memberof TabBarRejected
   */
  pullToRefresh = () => {
    console.log('xxx pullToRefresh');
    let isFirstLoading = false;
    if (this.props.shouldHideSearch){
      this.props.shouldHideSearch();
      return;
    }

    this.setState({
      start: 0
    }, async() => {
      //clear filter values
      // this.props.resetActivationStatusSearchString();

      // this.props.transSetActivationStatusFilter(null);
      //Clear old states
      await this.props.transResetAll();
      await this.callApi(isFirstLoading);
    });
  }

  /**
 * @description Handle infinite scroll
 * @param {String} distanceFromEnd
 * @memberof TabBarRejected
 */
  handleLoadMore = () => {
    let { activation_status_rejected_list=[] } = this.props;
    let isFirstLoading = false;
    console.log('xxx handleLoadMore ');
    this.setState({
      start: activation_status_rejected_list.length
    }, () => {
      this.callApi(isFirstLoading);
    });
  }

  /**
 * @description generate list item
 * @param {Object} { item }
 * @memberof TabBarRejected
 */
  renderListItem = ({ item }) => (
    <TouchableHighlight onPress={()=>{this.debouncedLoadLisItem(item);}}>
      <View style={styles.listView_row}>
        <View style={styles.listView_col1}>
          <Image source={this.getStatusIconSrc(item.order_status)} style={styles.imageStatusIconStyle}/>
        </View>
        <View style={styles.listView_col2}>
          <Text style={styles.rowText}>{item.connectionNo}</Text>
        </View>
        <View style={styles.listView_col3}>
          <Text style={styles.rowTextIssue}>{this.getIssueName(item.imageList)}</Text>
        </View>
        <View style={styles.listView_col5}>
          <View style={styles.listView_col5_1}>
            <Text style={styles.rowTextDate}>{item.date}</Text>
            <Text style={styles.rowTextTime}>{item.time}</Text>
          </View>
        </View>
        <View style={styles.listView_col6}>
          {forwardIcon}
        </View>
      </View>
    </TouchableHighlight>
  );

  render() {
    return (
      <View style={styles.containerTab}>
        <FlatList
          data={this.props.activation_status_rejected_list}
          renderItem={this.renderListItem}
          refreshing={this.props.api_loading}
          onRefresh={this.pullToRefresh}
          onEndReached={({ distanceFromEnd }) => {
            if (distanceFromEnd >= 0) {
              console.log('on end reached call api', distanceFromEnd);
              console.log('this.props.activation_status_rejected_list', this.props.activation_status_rejected_list.length);
              
              //call API to get next page values
              this.debouncedHandleLoadMore();
            }
            console.log('on end reached ', distanceFromEnd);
          }}
          onEndReachedThreshold ={0.05}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => {
            if (this.props.api_loading)
              return null;
            return (
              <EmptyFeed
                text={this.state.locals.errorDesc}
                messageTxtStyle={styles.errorDescText}
              />
            );
          }
          }
          scrollEnabled={true}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: TabBarRejected => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const api_loading = state.transHistory.api_loading;
  const activation_status_rejected_list = state.transHistory.activation_status_rejected_list;
  const transSetActivationStatusFilter = state.transHistory.transSetActivationStatusFilter;
  const activation_status_history_rejected_search_string = state.transHistory.activation_status_history_rejected_search_string;

  return { 
    Language, 
    api_loading , 
    activation_status_rejected_list, 
    transSetActivationStatusFilter,
    activation_status_history_rejected_search_string 
  };
};

const styles = StyleSheet.create({

  containerTab: {
    flex: 1
  },
  listView_row: {
    flexDirection: 'row',
    height: 50,
    borderWidth: 0.5,
    borderColor: Colors.borderLineColor,
    backgroundColor: Colors.colorWhite
  },
  listView_col1: {
    flex: 1.2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    marginLeft: 6
  },
  listView_col2: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 7
  },

  listView_col3: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingLeft: 15
  },

  listView_col5: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 8,
    paddingTop: 8
  },
  listView_col5_1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  listView_col6: {
    flex: 0.8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    paddingRight: 10
  },
  rowText: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 15,
    color: Colors.colorBlack,
    textAlign: 'left',
    fontWeight: '400'
  },

  rowTextDate: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },

  rowTextTime: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 12,
    textAlign: 'left',
    color: Colors.colorBlack,
    fontWeight: '400'
  },

  rowTextIssue: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 13,
    color: Colors.colorBlack,
    textAlign: 'center',
    fontWeight: '400',
    paddingLeft: 1
  },
  iconStyle: {
    textAlign: 'center',
    marginLeft: 10
  },

  imageStatusIconStyle : {
    width: 20,
    height: 20,
    margin: 5
  },
  errorDescText:{ 
    textAlign: 'center', 
    fontSize: 18, 
    fontWeight:'bold', 
    color: Colors.black 
  }
});

const forwardIcon = (<Icon
  name="ios-arrow-forward"
  size={25}
  color="black"
  style={styles.iconStyle}/>);

export default connect(mapStateToProps, actions)(TabBarRejected);
