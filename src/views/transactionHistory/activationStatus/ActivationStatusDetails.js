/*
 * File: ActivationStatusDetails.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 16th May 2018 3:07:45 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:21:14 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Alert,
  BackHandler,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { Header } from '../../../views/transactionHistory/Header';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import ActIndicator from '../ActIndicator';
import Constants from '../../../config/constants';
import strings from '../../../Language/ActivationStatus';
import image_state_active from '../../../../images/common/icons/icon_state_success.png';
import image_state_pending from '../../../../images/common/icons/icon_state_pending.png';
import image_state_rejected from '../../../../images/common/icons/icon_state_fail.png';
import image_state_detail_pending from '../../../../images/common/icons/icon_detail_state_pending.png';
import image_state_detail_success from '../../../../images/common/icons/icon_detail_state_success.png';
import image_state_detail_rejected from '../../../../images/common/icons/icon_detail_state_fail.png';
import _ from "lodash";
/**
 * @description
 * @class ActivationStatusDetails
 * @extends {React.Component}
 */
class ActivationStatusDetails extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      resubmit: true,
      locals: {
        screenTitle: strings.activationStatusTitle,
        ok: strings.ok,
        cancel: strings.cancel,
        later: strings.BtnLater,
        resubmit: strings.BtnResubmit,
        nicNo: strings.nicNo,
        ppNo: strings.ppNo,
        connectionNo: strings.connectionNo,
        transID: strings.transID,
        connectionStatus: strings.connectionStatus,
        activatedBy: strings.activatedBy,
        date: strings.date,
        time: strings.time,
        simNo: strings.simNo,
        contactNo: strings.contactNo,
        mobileContactNo: strings.mobileContactNo,
        ezCashRef: strings.ezCashRef,
        nicFront: strings.nicFront,
        nicBack: strings.nicBack,
        pp: strings.pp,
        dl: strings.dl,
        pob: strings.pob,
        customerImage: strings.customerImage,
        ack: strings.ack,
        poi: strings.poi,
        pendingTxt: strings.pendingTxt,
        approvedTxt: strings.approvedTxt,
        rejectedTxt: strings.rejectedTxt,
        active: strings.active,
        inActive: strings.inActive,
        disconnected: strings.disconnected,
        backMessage: strings.backMessage,
        imei_serial: strings.imei_serial,
        sim_serial: strings.sim_serial,
        bundle_serial: strings.bundle_serial,
        additonalPob:strings.proof_of_installation,
        rejectedImageFailed:strings.rejectedImageFailed,
        loadingText: strings.loadingText,
        oafSerial: strings.oafSerial,
        stbSerial: strings.stbSerial,
        additionalRental: strings.additionalRental,
        additionalChannels: strings.additionalChannels,
        packageRental: strings.packageRental,
        package: strings.package,
        installationStatus: strings.installationStatus,
        dtvConnectionStatus: strings.dtvConnectionStatus,
        paymentPlan: strings.paymentPlan,
        CIRNo: strings.CIRNo,
        accountNo: strings.accountNo
      },

    };

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  onPressOk = () => {
    console.log('xxx onPressOk');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  onClickLater = () => {
    console.log('xxx onClickLater');
    this.onPressOk();
  }

  /**
 * @description Get capture description
 * @param {String} value
 * @returns {String} description
 * @memberof ActivationStatusDetails
 */
  getCaptureDescription = (value) => {
    let description;
    switch (parseInt(value)) {
      case Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT:
        description = this.state.locals.nicFront;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.NIC_BACK:
        description = this.state.locals.nicBack;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.PASSPORT:
        description = this.state.locals.pp;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE:
        description = this.state.locals.dl;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING:
        description = this.state.locals.pob;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE:
        description = this.state.locals.customerImage;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.SIGNATURE:
        description = this.state.locals.ack;
        break;
      case Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB:
        description = this.state.locals.additonalPob;
        break;
      default:
        description = this.state.locals.nicFront;
    }

    return description;
  };

  /**
 * @description Get image label and value
 * @param {Object} imageItem
 * @returns {Object} { imageData }
 * @memberof ActivationStatusDetails
 */
  getImageLabelAndValue = (imageItem) => {
    console.log('xxxx getImageLabelAndValue', imageItem);
    let imageData = {};
    let label = this.getCaptureDescription(imageItem.imageId);
    console.log(imageItem.imageStatus);
    if (imageItem.imageStatus == "APPROVED") {
      imageData = {
        label: label,
        value: this.state.locals.approvedTxt,
        icon: image_state_detail_success,
      };
    }
    else if (imageItem.imageStatus == "REJECTED") {
      imageData = {
        label: label,
        value: this.state.locals.rejectedTxt,
        icon: image_state_detail_rejected
      };
    }
    else {
      imageData = {
        label: label,
        value: this.state.locals.pendingTxt,
        icon: image_state_detail_pending
      };

    }
    imageData.imageStatus = imageItem.imageStatus;
    imageData.imageId = imageItem.imageId;
    console.log('xxxx getImageLabelAndValue :: imageData ', imageData);
    return imageData;
  }

  /**
 * @description Get image label and value
 * @param {Object} imageItem
 * @returns {Object} { imageData }
 * @memberof ActivationStatusDetails
 */
  getImageLabelAndValue2 = (imageItem) => {
    console.log('xxxx getImageLabelAndValue', imageItem);
    let imageData = {};
    let label = this.getCaptureDescription(imageItem.imageId);
    console.log(imageItem.imageStatus);
    switch (imageItem.imageStatus) {
      case imageItem.imageStatus == "APPROVED":
        imageData = {
          label: label,
          value: this.state.locals.approvedTxt,
          icon: image_state_detail_success
        };
        break;
      case imageItem.imageStatus == "PENDING":
        imageData = {
          label: label,
          value: this.state.locals.pendingTxt,
          icon: image_state_detail_pending
        };
        break;
      case imageItem.imageStatus == "REJECTED":
        imageData = {
          label: label,
          value: this.state.locals.rejectedTxt,
          icon: image_state_detail_rejected
        };
        break;
      default:
        console.log('xxx default');
        imageData = {
          label: label,
          value: this.state.locals.pendingTxt,
          icon: image_state_detail_pending
        };
        break;
    }
    imageData.imageStatus = imageItem.imageStatus;
    imageData.imageId = imageItem.imageId;
    console.log('xxxx getImageLabelAndValue :: imageData ', imageData);
    return imageData;
  }

  /**
 * @description Get connection activation status
 * @param {String} key
 * @returns {Object} { conSt, conStIcon }
 * @memberof ActivationStatusDetails
 */
  getConnectionStatus = (key) => {
    let conSt;
    let conStIcon = null;
    switch (key) {
      case 'NC':
        conSt = this.state.locals.inActive;
        conStIcon = image_state_detail_rejected;
        break;
      case 'C':
        conSt = this.state.locals.active;
        conStIcon = image_state_detail_success;
        break;
      case 'B':
        conSt = 'Barred';
        conStIcon = image_state_detail_rejected;
        break;
      case 'T':
        conSt = 'Both Way Barred';
        conStIcon = image_state_detail_rejected;
        break;
      case 'D':
        conSt = this.state.locals.disconnected;
        conStIcon = image_state_detail_rejected;
        break;
      default:
        conSt = this.state.locals.inActive;
        conStIcon = image_state_detail_rejected;
        break;
    }
    return { conSt, conStIcon };
  }

  /**
 * @description Get installation activation status
 * @param {String} key
 * @returns {Object} { conSt, conStIcon }
 * @memberof ActivationStatusDetails
 */
getInstallationStatus = (key) => {
  let conStIcon = null;
  switch (key) {
    case 'PENDING':
      conStIcon = image_state_detail_pending;
      break;
    case 'DELIVERED':
      conStIcon = image_state_detail_success;
      break;
    default:
      conStIcon = undefined;
      break;
  }
  return { conStIcon };
}

  /**
 * @description Get connection status image source
 * @param {String} status
 * @returns {Object} { icon }
 * @memberof ActivationStatusDetails
 */
  getImageSrc = (status) => {
    console.log('xxxx getImageSrc', status);
    if (status == 'APPROVED') {
      return image_state_active;
    } else if (status == 'PENDING') {
      return image_state_pending;
    } else if (status == 'REJECTED') {
      return image_state_rejected;
    } else {
      return image_state_pending;
    }
  }

  /**
   * @description Show image resubmit view
   * @param {Object} item
   * @param {Array} rejectedImages
   * @memberof ActivationStatusDetails
   */
  onPressResubmit = (item, rejectedImages) => {
    console.log('xxx onPressResubmit', rejectedImages);
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      screen: 'DialogRetailerApp.views.ImageResubmitScreen',
      title: this.state.locals.activationStatusTitle,
      passProps: {
        selectedItem: item,
        rejectedImages: rejectedImages
      }
    });
  }

  /**
   * @description Return rejected image name, id and, state 
   * @param {Object} image
   * @returns { name, id, state }
   * @memberof ActivationStatusDetails
   */
  getRejectedImage = (image) => {
    console.log('xxxx getRejectedImage', image);
    let name = '';
    let id = -1;
    let state = false;
    let imageStatus = image.imageStatus;
    if (image.imageStatus == 'REJECTED') {
      name = this.getCaptureDescription(image.imageId);
      id = image.imageId;
      state = true;
    }

    console.log('xxxx getRejectedImage  name, id, state =>', name, id, state);

    return { name, id, state, imageStatus };
  }

  showImagePreview = (elementValue) => {
    console.log("showImagePreview :: Item: ", this.props.item);
    let data ={
      "order_id":this.props.item.order_id,
      "image_type":elementValue.imageId
    };

    this.props.getRejectedImageData(data,(response)=>{
      console.log("Rejected Image success res: ", JSON.stringify(response));
      let { image="" } = response.data;
      this
        .props
        .navigator
        .showModal({
          screen: 'DialogRetailerApp.views.ImagePreviewScreen',
          title: 'PREVIEW',
          passProps: {
            imageData:image
          },
        });
    },(response)=>{
      Alert.alert('', this.state.locals.rejectedImageFailed);
      console.log("Rejected Image failure res: ", JSON.stringify(response));
    });

    
  };

  render() {
    let loadingIndicator;
    let item = this.props.item;

    // item = {
    //   "connectionNo": "767596080",
    //   "connType": "PREPAID",
    //   "lob": "DTV",
    //   "simNo": "99998809",
    //   "id": "912232123V",
    //   "idType": "NIC",
    //   "transID": "49238923",
    //   "order_id": "49238923",
    //   "contactNo": "777777777",
    //   "cir_no": "1612303402",
    //   "package_name": "GOLD",
    //   "package_rental": "Rs 0.00 + Tax Monthly",
    //   "additional_channels": "TVN, COLORS INFINITY, AXN_ENT_ENGLISH, SONY_ENT_ENGLISH, SONY_SPORTS",
    //   "additional_rental": "Rs 0.00 + Tax Monthly",
    //   "stb_serial": "98979949",
    //   "oaf_serial": "57171511030021252",
    //   "dtv_account_number": "767596080",
    //   "installation_status": {
    //     "code": "PENDING",
    //     "value": "Pending"
    //   },
    //   "date": "05/02/2019",
    //   "time": "02:15: AM",
    //   "activatedBy": "0767948000",
    //   "ezCashRef": " - ",
    //   "ezCashReferences": [],
    //   "rejectedReasons": " - ",
    //   "imageList": [
    //     {
    //       "imageId": "4",
    //       "name": "DL",
    //       "imageStatus": "APPROVED"
    //     },
    //     {
    //       "imageId": "7",
    //       "name": "ACK",
    //       "imageStatus": "APPROVED"
    //     }
    //   ],
    //   "connection_status": "NC",
    //   "order_status": "PENDING"
    // };

    console.log("ActivationStatusDetails :: item", item);
    let idTypeName;
    let rejectedImages = [];
    let ezcashArray = [];
    const { ezCashReferences = [] } = item;

    if (this.props.api_loading) {
      loadingIndicator = (<ActIndicator text={this.state.locals.loadingText} animating />);
    } else {
      loadingIndicator = true;
    }

    if (item.idType === 'NIC' || item.idType === null) {
      idTypeName = this.state.locals.nicNo;
    } else {
      idTypeName = this.state.locals.ppNo;
    }


    let detailsArray = [
      {
        label: idTypeName,
        value: item.id,
        icon: null
      },
      {
        label: this.state.locals.connectionNo,
        value: item.connectionNo,
        icon: null
      },
      {
        label: this.state.locals.connectionStatus,
        value: this.getConnectionStatus(item.connection_status).conSt,
        icon: this.getConnectionStatus(item.connection_status).conStIcon,
      },
      {
        label: this.state.locals.transID,
        value: item.order_id,
        icon: null
      },
      {
        label: this.state.locals.activatedBy,
        value: item.activatedBy,
        icon: null
      },
      {
        label: this.state.locals.date,
        value: item.date,
        icon: null
      },
      {
        label: this.state.locals.time,
        value: item.time,
        icon: null
      },

      {
        label: this.state.locals.simNo,
        value: item.simNo,
        icon: null
      },
      {
        label: this.state.locals.contactNo,
        value: item.contactNo,
        icon: null
      },
      // {
      //   label: this.state.locals.ezCashRef,
      //   value: item.ezCashRef,
      //   icon: null
      // },
    ];

    if (item.lob == 'DTV'){
      const { 
        installation_status = { code: "", value: "" },
        contactNo = "",
        oaf_serial = "",
        stb_serial = "",
        additional_rental = "",
        time = "",
        package_name = "",
        package_rental = "",
        additional_channels = "",
        order_id = "",
        activatedBy = "",
        date = "",
        connection_status = "",
        id = "",
        dtv_account_number = "",
        cir_no = "",
        connType = ""
      } = item;

      detailsArray = [
        {
          label: idTypeName,
          value: id,
          icon: null
        },
        {
          label: this.state.locals.accountNo,
          value: dtv_account_number,
          icon: null
        },
        {
          label: this.state.locals.CIRNo,
          value: cir_no,
          icon: null
        },
        {
          label: this.state.locals.paymentPlan,
          value: connType,
          icon: null
        },
        {
          label: this.state.locals.dtvConnectionStatus,
          value: this.getConnectionStatus(connection_status).conSt,
          icon: this.getConnectionStatus(connection_status).conStIcon,
        },
        {
          label: this.state.locals.installationStatus,
          value: installation_status.value,
          icon: this.getInstallationStatus(installation_status.code).conStIcon,
        },
        {
          label: this.state.locals.transID,
          value: order_id,
          icon: null
        },
        {
          label: this.state.locals.activatedBy,
          value: activatedBy,
          icon: null
        },
        {
          label: this.state.locals.date,
          value: date,
          icon: null
        },
        {
          label: this.state.locals.time,
          value: time,
          icon: null
        },
        {
          label: this.state.locals.package,
          value: package_name,
          icon: null
        },
        {
          label: this.state.locals.packageRental,
          value: package_rental,
          icon: null
        },
        {
          label: this.state.locals.additionalChannels,
          value: additional_channels,
          icon: null
        },
        {
          label: this.state.locals.additionalRental,
          value: additional_rental,
          icon: null
        },
        {
          label: this.state.locals.stbSerial,
          value: stb_serial,
          icon: null
        },
        {
          label: this.state.locals.oafSerial,
          value: oaf_serial,
          icon: null
        },
        {
          label: this.state.locals.mobileContactNo,
          value: contactNo,
          icon: null
        },
      ];

      // TODO: Uncomment below to remove empty valued objects
      // _.remove(detailsArray, (obj)=>{
      //   return obj.value == "";
      // });
    }

    
    let contactNoIndex = _.findIndex(detailsArray, {  label: this.state.locals.contactNo });
    if (item.lob == 'DTV')
      contactNoIndex = _.findIndex(detailsArray, {  label: this.state.locals.mobileContactNo });

      
    if (ezCashReferences == 0) {
      detailsArray.splice(contactNoIndex + 1, 0, {
        label: this.state.locals.ezCashRef,
        value: item.ezCashRef,
        icon: null
      });
    }

    if (item.lob === 'LTE' && item.imeiSerial != null) {

      detailsArray.splice(7, 0, {
        label: strings.imei_serial,
        value: item.imeiSerial,
        icon: null
      }  
      );
    }

    if (item.lob === 'LTE' && item.imeiSerial === null) {
      detailsArray.splice(7, 0, {
        label: strings.bundle_serial,
        value: item.simNo,
        icon: null
      }
      );
    }

    ezCashReferences.forEach(function (ezCashReferences, index) {
      console.log('ezCashReferences index', index);
      console.log('ezCashReferences index', ezCashReferences);   
      if (ezCashReferences) {

        ezcashArray.push({
          label: ezCashReferences.label,
          value: ezCashReferences.value,
          icon: null
        });
      }
    });

    for (let prop in item.imageList) {
      if (!item.imageList.hasOwnProperty(prop)) {
        continue;
      }
      let rejectedImage = this.getRejectedImage(item.imageList[prop]);
      if (rejectedImage.state) {
        rejectedImages.push(rejectedImage);
      }

      detailsArray.push(this.getImageLabelAndValue(item.imageList[prop]));
    }

    const getElementView = (index, elementValue, context) => {

      if (elementValue.icon != undefined && elementValue.icon != null) {
        return (<ElementItemWithIcon key={index} context={context} elementValue={elementValue} leftTxt={elementValue.label} rightTxt={elementValue.value} icon={elementValue.icon} />);
      } else {
        return (<ElementItem key={index} leftTxt={elementValue.label} rightTxt={elementValue.value} />);
      }
    };

    const getElementViewForeZcashRef = (index, elementValue) => {

      return (<ElementItemForEzcash key={index} leftTxt={elementValue.label} rightTxt={elementValue.value} />);

    };

    const getButtonView = () => {
      if (item.order_status == 'REJECTED') {
        return (<ElementFooterTwoButton
          okButtonText={this.state.locals.resubmit}
          cancelButtonText={this.state.locals.later}
          onClickOk={() => this.onPressResubmit(item, rejectedImages)}
          onClickCancel={() => this.onClickLater()}
          disableOkButton={this.state.isDisableButtons}
          disableCancelButton={this.state.isDisableButtons}
        />);
      } else {
        return (<ElementFooterButton
          okButtonText={this.state.locals.ok}
          onClickOk={() => this.onPressOk()}
          disabled={this.state.isDisableButtons}
        />);
      }
    };

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <View style={styles.iconContainer}>
          <Image source={this.getImageSrc(item.order_status)} style={styles.imageIconStyle} />
        </View>
        <View style={styles.detailsContainer}>
          <ScrollView>
            {detailsArray.map((value, i) => getElementView(i, value, this))}
            {ezcashArray.length !== 0 ? 
              <View>  
                <View style={styles.elementEzcashReference}>
                  <Text style={styles.elementLeftTxt}>{this.state.locals.ezCashRef}</Text>
                </View>
              </View>: 
              <View/>
            }
            {ezcashArray.map((value, i) => getElementViewForeZcashRef(i, value ))}
          </ScrollView>
        </View>
        <View style={styles.containerBottom}>
          {getButtonView()}
        </View>
      </View>
    );
  }
}

const ElementItem = ({ leftTxt, rightTxt }) => {
  if (rightTxt == "") return null;

  return (
    <View style={styles.detailElementContainer}>
      <View style={styles.elementLeft}>
        <Text style={styles.elementLeftTxt}>
          {leftTxt}</Text>
      </View>
      <View style={styles.elementMiddle}>
        <Text style={styles.elementTxt}>
        :</Text>
      </View>
      <View style={styles.elementRight}>
        <Text style={styles.elementTxt}>
          {rightTxt}</Text>
      </View>
    </View>
  );};

const ElementItemForEzcash = ({ leftTxt, rightTxt }) => (
  <View style={styles.detailElementContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementItemWithIcon = ({ leftTxt, elementValue, context, rightTxt, icon }) =>{ 
  console.log("ElementItemWithIcon :: elementValue: ", elementValue);
  if (elementValue == "") return null;
  return (
    <View style={styles.detailElementContainer}>
      <View style={styles.elementLeft}>
        <Text style={styles.elementLeftTxt}>
          {leftTxt}</Text>
      </View>
      <View style={styles.elementMiddle}>
        <Text style={styles.elementTxt}>
        :</Text>
      </View>
      <View style={styles.elementRightWithIcon}>
        <View style={styles.elementRightTextContainer}>
          { elementValue.imageStatus === "REJECTED" ?
            <Text onPress={()=>context.showImagePreview(elementValue)} style={styles.elementTxtHyperlink}>{rightTxt}</Text>
            :
            <Text style={styles.elementTxt}>{rightTxt}</Text>
          }
          

        </View>
        <View style={styles.elementRightIconContainer}>
          <Image source={icon} style={styles.iconStyle} />
        </View>
      </View>
    </View>
  );
};

const ElementFooterButton = ({
  okButtonText,
  onClickOk,
  disabled = false
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView} />
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={disabled}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const ElementFooterTwoButton = ({
  okButtonText,
  cancelButtonText,
  onClickOk,
  onClickCancel,
  disableOkButton = false,
  disableCancelButton = false
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView2} />
    <TouchableOpacity
      style={styles.cancelButtonContainer}
      onPress={onClickCancel}
      activeOpacity={1}
      disabled={disableCancelButton}>
      <Text style={styles.buttonTxt}>{cancelButtonText}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={disableOkButton}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: ActivationStatusDetails => TransactionHistory ', state.transHistory);
  const api_loading = state.transHistory.api_loading;
  const Language = state.lang.current_lang;

  return { Language, api_loading };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  iconContainer: {
    flex: 1.1,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },

  detailsContainer: {
    flex: 6,
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },
  containerBottom: {
    backgroundColor: Colors.colorTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },

  detailElementContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10,
    alignItems: 'center',
    flex:4
  },

  imageIconStyle: {
    width: 58,
    height: 58,
    marginTop: 5,
    marginBottom: 5
  },

  elementLeft: {
    flex: 0.7,
    marginLeft: 10,
    borderWidth: 0
  },
  elementEzcashReference: {
    flex: 0.7,
    marginLeft: 10,
    borderWidth: 0,
    marginBottom: 10,
    marginTop: 20
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },

  elementRightTextContainer: {
    flex: 3,
  },

  elementRightIconContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',

  },

  elementRightWithIcon: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.actStatus.defaultTextFontSize,
    fontWeight: '200',
    color: Colors.colorGrey
  },
  elementTxt: {
    fontSize: Styles.actStatus.defaultTextFontSize,
    fontWeight: '400',
    color: Colors.colorBlackDelivery
  },

  elementTxtHyperlink: {
    fontSize: Styles.actStatus.defaultTextFontSize,
    color: Colors.hyperLinkColor,
    textDecorationLine: 'underline',
  },
  iconStyle: {
    width: 12,
    height: 12,
    margin: 5,
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    flexDirection: 'row',
    padding: 0,
    paddingTop: 0,
    paddingBottom: 10,
  },
  dummyView: {
    flex: 1.5
  },

  dummyView2: {
    flex: 0.8
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 48,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20,
    alignSelf: 'flex-end'
  },
  cancelButtonContainer: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 48,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
    alignSelf: 'flex-end'
  },

  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  }
});

export default connect(mapStateToProps, actions)(ActivationStatusDetails);
