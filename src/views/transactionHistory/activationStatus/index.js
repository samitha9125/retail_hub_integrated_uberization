/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 11:55:35 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 3:52:18 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { View, Dimensions, StyleSheet, BackHandler } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Analytics from '../../../utills/Analytics';
import Colors from '../../../config/colors';
import { Header } from '../../../views/transactionHistory/Header';
import { SearchBar } from '../SearchBar';
import globalConfig from '../../../config/globalConfig';
import strings from '../../../Language/ActivationStatus';
import TabBarRejected from './TabBarRejected';
import TabBarAll from './TabBarAll';

import _ from 'lodash';
const pageLimit = globalConfig.transactionHistoryLoadLimit;

const INITIAL_LAYOUT_WIDTH = {
  height: 0,
  width: Dimensions.get('window').width,
};
/**
 * @description
 * @class ActivationStatusMainView
 * @extends {React.Component}
 */
class ActivationStatusMainView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      index: 0,
      start:0,
      routes: [
        { key: 'rejected', title: strings.rejected },
        { key: 'all', title: strings.all }
      ],
      locals:{
        backMessage:  strings.backMessageNew,
        headerTittle: strings.activationStatusTitle,
        tabAll: strings.all,
        tabRejected: strings.rejected,
      },
      searchBarHidden: true,
      AllRoute:null,
      rejectedRoute:null,
      _renderScene:null
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    if (this.state.searchBarHidden == false){
      console.log('xxxx handleBackButtonClick :: searchBarHidden'),
      this.searchBar.hide();
      this.setState({ searchBarHidden: true });
      return true;
    }
    console.log('xxx handleBackButtonClick');
    this.okHandler();
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      screen: 'DialogRetailerApp.views.TransactionHistoryMainScreen',
      title: 'BillPaymentHistoryMainScreen',
      animated: true, 
      animationType: 'slide-up' 
    });
  }

  /**
   * @description Search activation status list
   * @param {String} text
   * @memberof ActivationStatusMainView
   */
  searchRecords = async(text) => {
    this.props.resetActivationStatusRejectedList();
    this.props.resetActivationStatusAllList();

    if (text === '') {
      await this.props.transResetAll(); 
      console.log("Done reset search data");
      console.log('this.props.activation_status_history_rejected_search_string',this.props.activation_status_history_rejected_search_string);
      this.loadTabData('reset');   
      return;
    }

    console.log('xxx searchRecords');
    // this.props.resetActivationStatusAllList();
    // resetActivationStatusRejectedList
    let data = {
      search: text,
      limit: pageLimit,
      start:0
    };
    const successCbAll = (response) => {
      console.log('search :: successCbAll', response);
    };

    const errorCbAll = (response) => {
      console.log('search :: errorCbAll', response);
    };

    const successCbRejected = (response) => {
      console.log('search :: successCbRejected', response);

    };
    const errorCbRejected = (response) => {
      console.log('search :: errorCbRejected', response);
    };

    // this.props.resetActivationStatusRejectedList();
    // this.props.resetActivationStatusAllList();
    let isFirstLoading = true;
    this.props.getActivationStatusRejectedList(data, successCbRejected, errorCbRejected, isFirstLoading);

    _.delay(function(currContext){
      data = {
        search: text,
        limit: pageLimit,
        start: 0
      };
      currContext.props.getActivationStatusAllList(data, successCbAll, errorCbAll, isFirstLoading);
    },100,this);
    // this.props.getActivationStatusAllList(data, successCbAll, errorCbAll);
    // this.props.getActivationStatusAllList(data, successCbAll, errorCbAll);
    // this.props.getActivationStatusRejectedList(data, successCbRejected, errorCbRejected); 
  }

  /**
   * @description A custom debounce method to throttle user input in Search Bar
   * @param {Function} fn
   * @param {Number} delay
   * @returns
   * @memberof ActivationStatusMainView
   */
  debounce = (fn, delay) => {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  }

  /**
 * @description handle index change
 * @param {String} index
 * @memberof ActivationStatusMainView
 */
  _handleIndexChange = (index) => {
    console.log("TabIndex: ",index);
    console.log('activation_status_history_rejected_search_string: ',this.props.activation_status_history_rejected_search_string);
    console.log('activation_status_history_search_string: ',this.props.activation_status_history_search_string);
    
    this.setState({ index: index, start: 0 });

    if (this.props.activation_status_history_rejected_search_string && this.props.activation_status_history_search_string){
      return;
    }
    
    // this.props.resetSIMChangeHistoryLists();
    this.props.resetActivationStatusRejectedList();
    // this.props.resetActivationStatusAllList();
    this.props.transShowApiLoading(true);
    if (index == 0){
      this.loadTabData('REJECTED');
    } else {
      this.loadTabData('ALL');
    }
  }

  /**
   * @description Activation history backend API call to load tab data
   * @memberof ActivationStatusMainView
   */
  loadTabData = (loadTabData) => {
    console.log('xxxx loadTabData :: ', loadTabData);
    console.log('this.props.activation_status_history_rejected_search_string',this.props.activation_status_history_rejected_search_string);
    // this.props.resetSIMChangeSearchResults();
    const data = {
      start : this.state.start,
      limit :pageLimit,
      // filter: this.props.transSetActivationStatusFilter
    };

    const successCb = (response) => {
      console.log('filter :: successCb', response);

    };

    const errorCb = (response) => {
      console.log('filter :: errorCb', response);
    };

    if (loadTabData !== null && loadTabData ==='reset'){
      let isFirstLoading = false;
      this.props.getActivationStatusRejectedList(data, successCb, errorCb, isFirstLoading);
      _.delay(function(currContext){
        currContext.props.getActivationStatusAllList(data, successCb, errorCb, isFirstLoading);
      },50,this);

      return;
    }
    if (loadTabData === 'REJECTED'){
      let isFirstLoading = true;
      console.log('CALL REJECTED API');
      this.props.getActivationStatusRejectedList(data, successCb, errorCb, isFirstLoading);
      Analytics.logEvent('dsa_activation_status_rejected');
    } else {
      let isFirstLoading = true;
      console.log('CALL ALL API');
      this.props.getActivationStatusAllList(data, successCb, errorCb, isFirstLoading);
      Analytics.logEvent('dsa_activation_status_all');
    }
  }

  _renderHeader = props => <TabBar  
    style={{ backgroundColor: Colors.navBarBackgroundColor }} 
    tabStyle={{ backgroundColor: Colors.transparent }} 
    indicatorStyle={{ backgroundColor: Colors.white }}
    {...props} 
  />;

  componentWillMount(){
    if (this.state.AllRoute == null ||
        this.state.rejectedRoute == null ||
        this.state._renderScene == null) {

      const AllRoute = () => <TabBarAll 
        shouldHideSearch={()=>this.shouldHideSearch()} 
        {...this.props}/>;
      const rejectedRoute = () => <TabBarRejected 
        shouldHideSearch={()=>this.shouldHideSearch()} 
        {...this.props}/>;
      const _renderScene = SceneMap({
        rejected: rejectedRoute,
        all: AllRoute
      });

      this.setState({ AllRoute: AllRoute, rejectedRoute: rejectedRoute, _renderScene: _renderScene });
    }
  }

  shouldHideSearch = () =>{
    console.log("Should hide search");
    this.props.resetActivationStatusSearchString();
    this.props.resetActivationStatusRejectedList();
    this.props.resetActivationStatusAllList();
    this.searchBar.hide();
    this.searchRecords('');
    // this.props.resetSIMChangeSearchResults();
    this.setState({ searchBarHidden: true });
  }

  render() {
    return (
      <View style={styles.container}>    
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          filterButtonPressed={() => this.handleFilter()}
          displaySearch 
          displayFilter 
          headerText={ this.state.locals.headerTittle }/>      
        <SearchBar
          ref={(ref) => this.searchBar = ref}
          onChangeText={this.debounce(this.searchRecords, globalConfig.searchDebounceDelay)}
          onPressClear={()=>{ 
            this.props.resetActivationStatusSearchString();
            this.props.resetActivationStatusRejectedList();
            this.props.resetActivationStatusAllList();
            this.searchBar.hide();
            this.searchRecords('');
            // this.props.resetSIMChangeSearchResults();
            this.setState({ searchBarHidden: true });
          }
          }
        /> 

        { this.state._renderScene !== null ? 
          <TabViewAnimated
            navigationState={this.state}
            renderScene={this.state._renderScene}
            renderHeader={this._renderHeader}
            onIndexChange={this._handleIndexChange}
            useNativeDriver
            initialLayout={INITIAL_LAYOUT_WIDTH}
          />
          : true }
      </View>
    );
  }

  /**
   * @description Show search bar
   * @memberof ActivationStatusMainView
   */
  handleSearch = () =>{
    this.searchBar.show();
    this.setState({ searchBarHidden: false });
  }

  /**
   * @description Show filter view
   * @memberof ActivationStatusMainView
   */
  handleFilter = () =>{
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'FILTER', 
      screen: 'DialogRetailerApp.views.FilterActivationStatusScreen',
      passProps: {
      }
    });
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: FilterActivationStatusView  => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  const transSetActivationStatusFilter = state.transHistory.transSetActivationStatusFilter;
  const activation_status_history_rejected_search_string = state.transHistory.activation_status_history_rejected_search_string;
  const activation_status_history_search_string = state.transHistory.activation_status_history_search_string;
  return { 
    Language, 
    transSetActivationStatusFilter, 
    activation_status_history_rejected_search_string,
    activation_status_history_search_string 
  };
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps, actions)(ActivationStatusMainView);