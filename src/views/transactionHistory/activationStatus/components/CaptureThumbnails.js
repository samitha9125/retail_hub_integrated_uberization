/*
 * File: CaptureThumbnails.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 30th May 2018 12:20:57 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:24:01 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Colors from '../../../../config/colors';
import Styles from '../../../../config/styles';

class CaptureThumbnails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sectionClick: false,
      defaultState: true
    };
  }

  render() {
    const { label, onPress } = this.props;

    return (
      <View style={styles.container}>
        <View style={[styles.innerTextContainer]}>
          <Text style={styles.innerText}>{label}
          </Text>
        </View>
        <TouchableOpacity style={[styles.innerImageContainer2]} onPress={onPress}>
          <FontAwesome name='picture-o' size={26} color={Colors.colorBlack}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 2,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 15,
    backgroundColor: Colors.colorWhite
  },
  innerImageContainer2: {
    flex: 2,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: Colors.colorWhite,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  innerTextContainer: {
    flex: 4,
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 8,
    backgroundColor: Colors.colorWhite,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  innerText: {
    color: Colors.thumbnailTxtColor,
    fontSize: Styles.thumbnailTxtFontSize
  }

});

export default CaptureThumbnails;
