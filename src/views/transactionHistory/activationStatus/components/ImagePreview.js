/*
 * File: ImagePreview.js
 * Project: Dialog Sales App
 * File Created: Thursday, 20th September 2018 9:59:20 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Thursday, 20th September 2018 10:04:58 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */

import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Image,
  Text
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import { Header } from '../../../../views/transactionHistory/Header';
import Colors from '../../../../config/colors';
import strings from '../../../../Language/ActivationStatus';
import Orientation from 'react-native-orientation';

class ImagePreview extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        screenTitle: "PREVIEW",
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

    
  componentWillMount(){
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  render() {

    const { imageData } = this.props;

    return (
      <View style={styles.mainContainer}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/>
        <View style={styles.container}>
          
          { imageData == ''? 
            <Text style={styles.errorText}>Image Preview failed</Text> 
            :
            <Image 
              source={{ uri:imageData }}
              resizeMode="cover"
              style={{ flex:1 }}
            />
          }
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: activationStatus :: ActivationStatusDetails => TransactionHistory ', state.transHistory);
  const api_loading = state.transHistory.api_loading;
  const Language = state.lang.current_lang;

  return { Language, api_loading };
};

const styles = StyleSheet.create({
  mainContainer:{ 
    flex:1, 
    backgroundColor: Colors.appBackgroundColor 
  },
  container: {
    flex: 0.9,
    backgroundColor: Colors.appBackgroundColor,
    paddingBottom: 20,
  },
  errorText:{ 
    textAlign: 'center', 
    fontWeight: 'bold', 
    justifyContent: 'center', 
    alignItems: 'center', 
    flex: 1,
    top: '30%' 
  }
});

export default connect(mapStateToProps, actions)(ImagePreview);
