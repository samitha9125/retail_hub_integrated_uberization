/*
 * File: SectionContainer.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 30th May 2018 12:20:57 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:24:09 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Colors from '../../../../config/colors';

class SectionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  render() {
    const { customStyle, children } = this.props;
    return (
      <View style={[styles.container, customStyle]}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 2,
    marginTop: 2,
    marginLeft: 35,
    marginRight: 15,
    backgroundColor: Colors.appBackgroundColor
  }
});

export default SectionContainer;
