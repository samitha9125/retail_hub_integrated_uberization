/*
 * File: ActIndicator.js
 * Project: Dialog Sales App
 * File Created: Friday, 20th April 2018 2:09:00 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 19th June 2018 4:44:31 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { ActivityIndicator, View, StyleSheet, Text } from 'react-native';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';

/**
 * @description
 * @class ActIndicator
 * @extends {React.Component}
 */
class ActIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: true,
      locals: {
        validatingTxt: 'Loading'

      }
    };
  }

    
  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
    this.closeActivityIndicator();
  }

  closeActivityIndicator = () => this.setState({ animating: this.props.animating });

  render() {
    const animating = this.state.animating;
    let validatingText = this.props.text ? this.props.text : this.state.locals.validatingText;

    return (
      <View style={styles.container}>
        <View style={styles.indicatorStyle}>
          <Text style={styles.textStyle}>{validatingText}</Text>
          <ActivityIndicator
            animating={animating}
            color={Colors.activityIndicaterColor}
            size="large"
            style={styles.activityIndicator}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    paddingVertical: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 100000,
    opacity: 0.5
  },
  textStyle: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: "500",
    color: Colors.colorBlack,
    paddingVertical: 100,
    zIndex: 1000,
    paddingTop: 110,
    marginTop: 100
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    marginBottom: 45

  },
  indicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});

export default ActIndicator;
