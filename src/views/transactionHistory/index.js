import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import ActIndicator from './ActIndicator';
import strings from '../../Language/ActivationStatus';
import { Header } from './Header';
import img1 from '../../../images/menu_icons/mobile_activation.png';
import img2 from '../../../images/menu_icons/sim_change.png';
import img3 from '../../../images/menu_icons/bill_payment.png';

class DeliveryHome extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      workOrderCount: [],
      apiLoading: false,
      api_error: false,
      locals: {
        screenTitle: strings.transactionHistory,
        network_error_message: 'Network Error, \nPlease check your internet connection',
        api_error_message: 'Error occured while loading the data',
        continue: strings.continue,
        cancel: strings.cancel,
        activationStatusTitle: strings.activationStatusTitle,
        simChangeHistoryTitle: strings.simChangeHistoryTitle,
        billPaymentHistoryTitle: strings.billPaymentHistoryTitle,
        activationStatusTile: strings.activationStatusTile,
        simChangeHistoryTile: strings.simChangeHistoryTile,
        billPaymentHistoryTile: strings.billPaymentHistoryTile,
      }
    };
  }

  setIndicator = (loadingState = true) => {
    console.log('xxx setIndicator');
    this.setState({ apiLoading: loadingState });
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.transResetAll();

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setIndicator(false);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    this.okHandler();
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this.props;
    me.resetMobileActivationState();
  }

  showTappedView = (index) => {
    const navigator = this.props.navigator;
    switch (index){
      case 0:
        this.props.transSetActivationStatusFilter(null);
        navigator.push({
          screen: 'DialogRetailerApp.views.ActivationStatusMainScreen',
          title: this.state.locals.activationStatusTitle
        });
        break;
      case 1:
        navigator.push({
          screen: 'DialogRetailerApp.views.SimChangeHistoryMainScreen',
          title: this.state.locals.activationStatusTitle
        });
        break;
      case 2:
        navigator.push({
          screen: 'DialogRetailerApp.views.BillPaymentHistoryMainScreen',
          title: this.state.locals.billPaymentHistoryTitle
        });
        break;
    }

  }

  renderListItem = ({ item, index }) => (
    <View style={styles.container}>
      <TouchableOpacity
        key={index}
        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.showTappedView(index)}>
        <View style={styles.leftImageContainer}>
          <Image source={item.imageUri} style={styles.orderImage} resizeMode="cover"/>
        </View>
        <View style={styles.middleTextContainer}>
          <Text style={styles.innerText}>{item.value}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  render() {
    let workOrderCount = [
      {
        key: 0,
        imageUri: img1,
        value: this.state.locals.activationStatusTile,
      }, {
        key: 1,
        imageUri: img2,
        value: this.state.locals.simChangeHistoryTile,
      }, {
        key: 2,
        imageUri: img3,
        value: this.state.locals.billPaymentHistoryTile,
      }
    ];

    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <FlatList
          data={workOrderCount}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => index.toString()}
          scrollEnabled={false}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: index => TransactionHistory ', state.transHistory);
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 6,
    backgroundColor: Colors.colorYellow,
    alignItems: 'center'
  },
  leftImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 5
  },

  orderImage: {
    alignSelf: 'center',
    width: undefined,
    height: undefined,
    margin: 5,
    padding: 25,
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent

  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  innerText: {
    textAlign: 'center',
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '500',
    color: Colors.colorBlack,
    padding: 8
  },
});

export default connect(mapStateToProps, actions)(DeliveryHome);
