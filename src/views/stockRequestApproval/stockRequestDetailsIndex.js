import React, { Component } from 'react';
import { View } from 'react-native';

import Orientation from 'react-native-orientation';
import Analytics from '../../utills/Analytics';
import StockRequestDetailView  from './stockRequestDetails';

class StockRequestApprovalDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### DeliveryMain :: componentDidMount');
    Analytics.logEvent('dsa_mobile_delivery_app');
  }

  render() {
    return (
      <View>
        <StockRequestDetailView {...this.props}/>
      </View>
    );
  }
}

export default StockRequestApprovalDetail;