import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import ActIndicator from '../../views/stockManagement/ActIndicator';
import strings from '../../Language/StockMangement';
import { Header } from '../wom/Header';

const Utill = new Utills();
class StockRequestDetailView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: true,
      showText: false,
      api_error: false,
      STOCKDETAIL: [],
      Value: '',
      Holder: [],
      isfetching: false,
      locals: {
        screenTitle: strings.RequestApproval,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        reject: strings.reject,
        descriptionText: strings.descriptionText,
        stockRequestId: strings.stockRequestId,
        approved: strings.approved,
        connected:strings.connected,
        disconected:strings.disconected,
        network_conected:strings.network_conected
      }
    };
    this.stockArrayId = NaN;
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.loadDataDetail();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }


  okHandler = () => {
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  loadDataDetail = () => {
    this.setState({ apiLoading: true }, () => {
      const data = { STOID: this.props.stoid };
      Utill.apiRequestPost('GetRequestApprovalDetails', 'GoodsApproval', 'cfss', data, this.getCountSuccessCb, this.getCountFailureCb, this.getCountExcb);
    })
  }

  getCountSuccessCb = (response) => {
    let array = response.data.data;
    let showText = null;
    response.data.success ? showText = true : showText = false;
    array.forEach((element, index) => {
      array[index]['REQUESTED_QTY_FINAL'] = element.REQUESTED_QTY;
    });
    this.setState({ apiLoading: false, STOCKDETAIL: array, showText })
  }

  getCountFailureCb = (response) => {
    this.setState({ apiLoading: false, api_error: true, api_error_message: response.data.error }, () => {
      // to-do add  failure alert
    })
  }

  getCountExcb = (error) => {
    this.setState({ apiLoading: false, api_error_message: error, api_error: true }, () => {
      if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    })
  }

  onRefresh() {
    this.setState({ isfetching: true }, () => { this.loadDataDetail() });
  }

  async _updateText(text, index) {
    let array = this.state.STOCKDETAIL;
    array[index].REQUESTED_QTY = parseInt(text) || 0;
    await this.setState({ STOCKDETAIL: array });
  }

  Approved = () => {
    this.setState({ apiLoading: true }, () => {
      const data = {
        STOID: this.props.stoid,
        MaterialData: this.state.STOCKDETAIL,
        approve_action: 'approve',
        isLoading: false
      };
      Utill.apiRequestPost('ApproveSTORequest', 'GoodsApproval', 'cfss', data, this.approveSuccess, this.approveFailure, this.approveEx);
    });
  }

  approveSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'successWithOk',
          messageHeader: this.state.locals.stockRequestId + this.props.stoid,
          messageBody: response.data.info ? response.data.info : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.pop() }
        }
      });
    });
  }

  approveFailure = (response) => {
    this.setState({ apiLoading: false }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Alert!',
          messageBody: response.data.error,
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => {navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    });
  }

  approveEx = (res) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  onClickApproved = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: 'alert',
        messageHeader: this.state.locals.stockRequestId + this.props.stoid,
        messageBody: this.state.locals.descriptionText,
        messageFooter: "",
        btnFun: () => this.Approved(),
      },
      overrideBackPress: true,
    });
  }

  Reject = () => {
    this.setState({ apiLoading: true }, () => {
      const data = {
        STOID: this.props.stoid,
        MaterialData: this.state.STOCKDETAIL,
        approve_action: 'cancel',
        isLoading: false
      };
      Utill.apiRequestPost('ApproveSTORequest', 'GoodsApproval', 'cfss', data, this.rejectSuccess, this.rejectFailure, this.rejectEx);
    });
  }

  rejectSuccess = (response) => {
    this.setState({ apiLoading: false }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'successWithOk',
          messageHeader: this.state.locals.stockRequestId + this.props.stoid,
          messageBody: response.data.info,
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.pop() }
        }
      });
    });
  }

  rejectFailure = (response) => {
    this.setState({ apiLoading: false }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Error',
          messageBody: response.data.error ? response.data.error : '',
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => {navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' }); }
        }
      });
    });
  }

  rejectEx = (res) => {
    this.setState({ apiLoading: false }, () => {
      let error;
      if (res == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  onClickReject = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      screen: 'DialogRetailerApp.views.CommonAlertModel',
      passProps: {
        messageType: 'alert',
        messageHeader: this.state.locals.stockRequestId + this.props.stoid,
        messageBody: this.state.locals.descriptionText,
        messageFooter: "",
        btnFun: () => this.Reject(),
      },
      overrideBackPress: true,
    });
  }

  cannotApproved = () => {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.models.GeneralModelApprovals',
      title: 'GeneralModalException',
      passProps: {
        stoid: 'Cannot Approve' + ' ' + this.props.stoid,
        message: 'Approved Qty for : ' + this.state.STOCKDETAIL[this.stockArrayId].MATERIAL_DESC ? this.state.STOCKDETAIL[this.stockArrayId].MATERIAL_DESC : '' + ' ' + 'exceeds Requested Qty',
      },
      overrideBackPress: true
    });
  }

  renderListItem = ({ item, index }) => (
    <View>
      <View style={styles.detailElimentContainer}>
        <View style={styles.containerMiddleSection2}>
          <View style={styles.elementLeft}>
            <Text style={styles.elementTxt}>
              {item.MATERIAL_DESC}</Text>
          </View>
        </View>
        <View style={styles.elementMiddle}>
          <Text style={styles.elementTxt}>
            :</Text>
        </View>

        <View style={styles.containerMiddleSection5}>
          <View style={styles.elementLeft}>
            <Text style={styles.elementTxt}>
              {item.AVAILABLE_QTY}</Text>
          </View>
        </View>

        <View style={styles.containerMiddleSection3}>
          <View style={styles.elementLeft}>
            <Text style={styles.elementTxt}>
              {item.REQUESTED_QTY_FINAL}</Text>
          </View>
        </View>
        <View style={styles.containerMiddleSection4}>
          <View style={styles.elementMiddle}>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              keyboardType='numeric'
              autoCapitalize="none"
              placeholder=""
              value={item.REQUESTED_QTY.toString()}
              onChangeText={text => this._updateText(text, index)}
            />
          </View>
        </View>
      </View>
    </View>
  );

  _approvable() {
    for (let i = 0; i < this.state.STOCKDETAIL.length; i++) {
      let item = this.state.STOCKDETAIL[i];
      if (parseInt(item.REQUESTED_QTY_FINAL) < parseInt(item.REQUESTED_QTY)) {
        this.stockArrayId = i;
        return false;
      }
    }
    return true;
  }

  _onApprovePressed = () => {
    if (this._approvable()) {
      this.onClickApproved();
    } else {
      this.cannotApproved();
    }
  }

  _onRejectPressed = () => {
    this.onClickReject();
  }

  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (
        <View style={styles.container}>
          {this.state.showText === true ?
            <View style={styles.containerTop}>
              <View style={styles.containerTopSection1}>
              </View>
              <View style={styles.containerTopSection4}>
                <Text style={styles.containerTopText}>Avail</Text>
                <Text style={styles.containerTopText}> qty</Text>
              </View>
              <View style={styles.containerTopSection2}>
                <Text style={styles.containerTopText}>Requ</Text>
                <Text style={styles.containerTopText}> qty</Text>
              </View>
              <View style={styles.containerTopSection3}>
                <Text style={styles.containerTopText}>Approved</Text>
                <Text style={styles.containerTopText}> qty</Text>
              </View>
            </View>
            : null}
          <View style={styles.containerMiddle}>

            <FlatList
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isfetching}
              data={this.state.STOCKDETAIL}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => item.toString() && index.toString()}
              extraData={this.state}
              scrollEnabled={true}
              refreshing={false}
            />

          </View>
          {this.state.showText === true ?
            <View style={styles.containerBottom}>
              <View style={styles.containerBottomSection1}>

              </View>
              <View style={styles.containerBottomSection2}>
                <TouchableOpacity style={styles.buttonCancelContainer}
                  onPress={this._onRejectPressed}>
                  <Text style={styles.acceptbtn}>{this.state.locals.reject}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonContainer}
                  onPress={this._onApprovePressed}>
                  <Text style={styles.acceptbtn}>{this.state.locals.approved}</Text>
                </TouchableOpacity>
              </View>
            </View>
            : null}
        </View>

      );
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {loadingIndicator}
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockRequestDetail => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    height: '100%'
  },
  containerTop: {
    flex: 0.1,
    flexDirection: 'row'
  },
  containerTopText: {
    fontWeight: 'bold',
    color: Colors.primaryText,
    width: '100%',
    fontSize: 15,
    textAlign: 'center',

    justifyContent: 'center',
  },
  containerTopSection1: {
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerTopSection2: {
    flex: 0.2,
    paddingLeft: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerTopSection4: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  containerTopSection3: {
    flex: 0.3,
    marginLeft: 20,
    marginRight: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerMiddle: {
    flex: 0.75,
    flexDirection: 'row'
  },
  containerMiddleSection2: {
    flex: 0.5,
    marginLeft: 5,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  containerMiddleSection3: {
    flex: 0.2,
    marginLeft: 10,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  containerMiddleSection5: {
    flex: 0.2,
    marginLeft: 10,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  containerMiddleSection4: {
    paddingLeft: -5,
    flex: 0.4,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginRight: 20
  },
  containerBottom: {
    flex: 0.2,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: Colors.borderLineColorLightGray,
  },
  containerBottomSection1: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  containerBottomSection2: {
    marginRight: 10,
    marginTop: 10,
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
  },
  buttonCancelContainer: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent,
    height: 45,
    borderRadius: 5,
    alignSelf: 'flex-start'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    alignSelf: 'flex-start'
  },
  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 20
  },

  elementLeft: {
    flex: 1,
    marginLeft: 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderWidth: 0
  },
  elementMiddle: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderWidth: 0,
  },
  elementTxt: {
    fontSize: 12,
    color: Colors.primaryText,
  },
  input: {
    textAlign: 'center',
    height: 40,
    width: 70,
    fontSize: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.primaryText,
    borderWidth: 1
  },
  acceptbtn: {
    color: Colors.primaryText,
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  },
});
export default connect(mapStateToProps, actions)(StockRequestDetailView);
