import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  RefreshControl,
  Dimensions,
  NetInfo
} from 'react-native';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';
import ActIndicator from '../../views/stockManagement/ActIndicator';
import strings from '../../Language/StockMangement';
import { Header } from '../wom/Header';

const Utill = new Utills();
const { height } = Dimensions.get('screen');
class StockRequestApprovalView extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: false,
      showText: false,
      api_error: false,
      STOCKREQESTAPPROVAL: [],
      stoid: '',
      isfetching: false,
      locals: {
        screenTitle: strings.RequestApproval,
        network_error_message: strings.network_error_message,
        backMessage: strings.backMessage,
        api_error_message: strings.api_error_message,
        continue: strings.continue,
        cancel: strings.cancel,
        requestedDate: strings.requestedDate,
        requestedby: strings.requestedby,
        nodata: strings.nodata,
        connected:strings.connected,
        disconected:strings.disconected,
        network_conected:strings.network_conected
      }
    };
    this.navigatorOb = this.props.navigator;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleFirstConnectivityChange);
    this.getCount();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleFirstConnectivityChange);
  }

  handleBackButtonClick = () => {
    this.okHandler();
    return true;
  }

  _handleFirstConnectivityChange = isConnected => {
    if (isConnected == false) {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_error_message,
        actionText: this.state.locals.disconected,
        actionId: 'fabClicked',
        actionColor: Colors.colorRed,
        textColor: Colors.white,
        duration: 'indefinite'
      })
    } else {
      this.props.navigator.showSnackbar({
        text: this.state.locals.network_conected,
        actionText: this.state.locals.connected,
        actionId: 'fabClicked',
        actionColor: Colors.radioBtn,
        textColor: Colors.white,
      })
    }
  }

  okHandler = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  onRefresh() {
    this.setState({ isfetching: true }, () => {
      this.getCount();
    });
  }

  getCount = () => {
    this.setState({ apiLoading: true }, () => {
      const data = {
        FunctionalType: 'StockRequest',
        Status: 'Pending',
      };

      Utill.apiRequestPost('GetRequestApprovalInfo', 'GoodsApproval', 'cfss', data, this.getCountSuccessCb, this.getCountFailureCb, this.getCountExcb);
    });
  }

  getCountSuccessCb = (response) => {
    this.setState({
      STOCKREQESTAPPROVAL: response.data.data ? response.data.data : [],
      stoid: response.data.data.STOID ? response.data.data.STOID : '',
      api_error: false,
      apiLoading: false
    }, () => { this.dissmissNoNetworkModal() });
  }

  getCountFailureCb = (response) => {
    this.setState({ apiLoading: true, api_error: true }, () => {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({
        screen: 'DialogRetailerApp.views.CommonAlertModel',
        passProps: {
          messageType: 'defaultAlert',
          messageHeader: 'Error',
          messageBody: response.data.error,
          messageFooter: "",
          overrideBackPress: true,
          onPressOK: () => { navigatorOb.pop() }
        }
      });
    });
  }

  getCountExcb = (error) => {
    this.setState({ apiLoading: true, api_error: true }, () => {

      if (error == 'No_Network') {
        this.showNoNetworkModal(this.getCount);
      }
      else if (error == 'Network Error') {
        error = this.state.locals.network_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      } else {
        error = this.state.locals.api_error_message;
        this.navigatorOb.showSnackbar({ text: error });
      }
    });
  }

  showNoNetworkModal(retryFunc) {
    this.props.navigator.showModal({
      screen: 'DialogRetailerApp.modals.NetworkScreen',
      passProps: {
        retryFunc: retryFunc,
        screenTitle: this.state.locals.screenTitle
      }
    });
  }

  dissmissNoNetworkModal() {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  gotoStockRequestApprovalList(STOID) {
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'STOCK REQUEST APPROVAL',
      screen: 'DialogRetailerApp.views.StockRequestDetailView',
      passProps: { stoid: STOID }
    });
    navigatorOb.pop();
  }

  renderListItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => this.gotoStockRequestApprovalList(item.STOID)}>
      <View style={styles.card}>
        <View style={styles.cardTop}>
          <View style={styles.cardTopSection1}>
            <View style={styles.stockNumberView}>

              <View style={styles.stockNumber}>
                <Text style={styles.stockId}>{item.STOID}</Text>
                <View style={styles.imageReqest}>
                  <Image
                    source={require('../../../images/menu_icons/stock_request_approval.png')}
                    style={styles.imageContentPhone}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.cardTopSection2}>
          </View>
          <View style={styles.cardTopSection3}>
          </View>
        </View>
        {/* CARD BODY */}
        <View style={styles.cardBody}>
          <View style={styles.orderDetails}>
            <View style={styles.orderDetail1}>
              <Text style={styles.txtOrderDetailLabel}>{this.state.locals.requestedDate}</Text>
              <View style={styles.dateCreateView}>
                <Text style={styles.txtOrderDetailValue}> {item.CREATED_DATE} </Text>
              </View>
            </View>
            <View style={styles.orderDetail}>
              <Text style={styles.txtOrderDetailLabel1}>{this.state.locals.requestedby}</Text>
              <Text style={styles.txtOrderDetailid}> {item.AGENT_ID} </Text>
              <Text style={styles.txtOrderDetailid}> - </Text>
              <Text style={styles.txtOrderDetailValue}> {item.AGENT_NAME} </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  render() {
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (
        <View style={styles.background}>
          <FlatList
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isfetching}
            data={this.state.STOCKREQESTAPPROVAL}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
            scrollEnabled={true}
            refreshing={false}
            ListEmptyComponent={
              <View style={{ marginVertical: '55%', alignItems: 'center' }}>
                <Text style={{ color: Colors.black, fontWeight: 'bold', fontSize: 14 }}>
                  {this.state.locals.nodata}
                </Text>
              </View>}
          />
        </View>
      );
    } else {
      screenContent = (
        <View style={styles.wholeContain}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.isfetching}
                onRefresh={this.onRefresh.bind(this)}
              />
            }>
          </ScrollView>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle} />
        {this.state.apiLoading ?
          <View style={styles.indiView}>
            <ActIndicator animating={true} />
          </View>
          :
          true}
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockManagment ===> MainPage ');
  console.log('****** REDUX STATE :: StockManagment => MainPage ');
  const Language = state.lang.current_lang;
  return { Language };
};

const styles = StyleSheet.create({
  background: {
    backgroundColor: Colors.appBackgroundColor,
    flex: 1
  },
  container: {
    height: '100%'
  },
  card: {
    flex: 1,
    backgroundColor: Colors.white,
    height: Dimensions
      .get('window')
      .height / 5,
    margin: 5,
    marginVertical: 5,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation: 4,
    width: Dimensions.get('window').width - 10
  },
  cardTop: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  cardBody: {
    flex: 1.9,
    flexDirection: 'column',
  },
  stockId: {
    fontSize: 18,
    color: Colors.black,
  },
  txtOrderDetailLabel: {
    flex: 0.9,
    color: Colors.black,
  },
  txtOrderDetailLabel1: {
    flex: 0.6,
    color: Colors.black,
  },
  txtOrderDetailValue: {
    flex: 1,
    color: Colors.black,
  },
  txtOrderDetailid: {
    flex: 0.1,
    color: Colors.black,
  },
  cardTopSection1: {
    flex: 1,
    flexDirection: "column",
  },
  cardTopSection2: {
    flex: 1.1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  cardTopSection3: {
    flex: 1.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  stockNumberView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  stockNumber: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 14
  },
  indiView: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
  },
  orderDetail: {
    flex: 5,
    marginLeft: 10,
    marginBottom: 0,
    marginTop: 0,
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  orderDetail1: {
    flex: 5,
    marginLeft: 10,
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  orderDetails: {
    flex: 1.6,
    flexDirection: 'column'
  },
  imageContentPhone: {
    flex: 1,
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginBottom: 5
  },
  imageReqest: {
    marginLeft: 20
  },
  dateCreateView: {
    flex: 2,
    marginLeft: 10
  },
  wholeContain: {
    height: '100%',
    backgroundColor: Colors.appBackgroundColor
  },

});

export default connect(mapStateToProps, actions)(StockRequestApprovalView);
