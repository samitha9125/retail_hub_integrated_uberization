import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import ActIndicator from '../ActIndicator';
import imageAtentionIcon from '../../../../images/common/alert.png';
import strings from '../../../Language/Wom';

const Utill = new Utills();

class GeneralModalConfirmApproval extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      openModel: false,
      apiData: {},
      locals: {
        descriptionText: 'Do you want to approve the stock request?',
        api_error_message: strings.api_error_message
      }
    };
  }
  componentWillUnmount(){
    
  }
  approveEx = (error) => {
    console.log('approveEx', error);
    if (error == 'Network Error') {
      const navigatorOb = this.props.navigator;
      try {
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message: this.state.locals.network_error_message
          },
          overrideBackPress: true
        });
      } catch (error) {
        console.log(`Show Modal error: ${error.message}`);
      }
      
    } else {
      const navigatorOb = this.props.navigator;
      try {
        navigatorOb.showModal({
          screen: 'DialogRetailerApp.models.GeneralModalException',
          title: 'GeneralModalException',
          passProps: {
            message:  this.state.locals.api_error_message
          },
          overrideBackPress: true
        });
      } catch (error) {
        console.log(`Show Modal error: ${error.message}`);
      }
    }
    this.setState({ api_error_message: error });
    setTimeout(() => {
      // this.setIndicater(false);
      this.setState({ api_error: true });
    }, 1000);
  }

  approveSuccess = (response) => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this
      .props
      .navigator
      .showModal({
        title: 'GeneralModalBill', 
        screen: 'DialogRetailerApp.models.GenaralModelSucessApproval',
        passProps: {
          message: response.data.info
        },
        overrideBackPress: true 
      });
  }

  approveFailure = (response) => {
    console.log('xxxx approveFailure', response.data.error);
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    try {
      navigatorOb.showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
    } catch (error) {
      console.log(`Show Modal error: ${error.message}`);
    }
  }

  onBnPressYes = ( ) => {
    this.setState({ isLoading: false });
    
    const data = {
      STOID: this.props.stoid,
      MaterialData: this.props.MaterialData,
      approve_action:'approve',
      isLoading: false
    };
    console.log('yyyyyyyyyyy',this.state.approvedQty);
    this.setState({ isLoading: true });
    Utill.apiRequestPost('ApproveSTORequest', 'GoodsApproval', 'cfss', data, this.approveSuccess, this.approveFailure, this.approveEx);

  }

  onBnPressCancel = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer} />
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageAtentionIcon} style={styles.alertImageStyle} />
            </View>
            <View style={styles.descriptionContainer}>
              {this.state.isLoading
                ? <ActIndicator animating/>
                : <View/>}
              <View>
                <Text style={styles.stockId}>Stock Request ID : {this.props.stoid}</Text>
              </View>
              <View >
                <Text style={styles.descriptionText}>{this.state.locals.descriptionText}</Text>
              </View>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <View style={styles.dummyView} />
              <TouchableOpacity
                onPress={() => this.onBnPressCancel()}
                style={styles.bottomBtnCancel}>
                <Text style={styles.bottomCantainerCancelBtnTxt}>
                  {'NO'}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onBnPressYes()}
                style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'YES'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: StockRequestDetail ===> Confirm Model ');
  console.log('****** REDUX STATE :: StockRequestDetail => Confirm Model ');
  const Language = state.lang.current_lang;
  return { Language };
};


const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    flex: 1.5
  },

  bottomContainer: {
    flex: 0.3
  },

  innerContainer: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    flex: 0.7,
    alignItems: 'center',
    padding: 5
  },

  alertImageStyle: {
    width: 90,
    height: 90,
    marginTop: 5,
    marginBottom: 5
  },

  descriptionContainer: {
    flex: 1.5,
    flexDirection: 'column',
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 15,
    marginTop: 10
  },
  descriptionText: {
    fontSize: Styles.delivery.modalFontSize,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontWeight: '100'

  },
  stockId: {
    alignItems: 'flex-start',
    fontSize: Styles.delivery.modalFontSize,
    textAlign: 'left',
    color: Colors.colorBlack,
    paddingTop: 5,
    marginLeft: 10,
    marginBottom: 15,
    paddingBottom: 5,
    marginRight: 10,
    marginTop: 20,
    fontWeight: 'bold'
  },

  bottomCantainerBtn: {
    flex: 0.4,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 10
  },

  dummyView: {
    flex: 3,
    justifyContent: 'center'
  },

  bottomBtnCancel: {
    flex: 1,
    justifyContent: 'center'
  },
  bottomBtn: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'center'
  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorRed
  },
  bottomCantainerCancelBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorBlack
  }

});

export default connect(mapStateToProps, actions)(GeneralModalConfirmApproval);