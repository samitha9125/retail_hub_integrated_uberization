import React from 'react';
import { StyleSheet, View, TextInput, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';

class UserInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  pinChanged = (text) => {
    this.props.getPin(text);
  }

  render() {
    return (
      <View style={styles.inputWrapper}>
        <Image source={this.props.source} style={styles.inlineImg}/>
        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor={Colors.placeholderTextColor}
          underlineColorAndroid={Colors.underlineColorAndroid}
          value={this.props.pin}
          onChangeText={this.pinChanged}
          accessibilityLabel={'text_input_login_pin'} 
          testID={'text_input_login_pin'}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {

  return { pin: state.auth.pin };
};

const DEVICE_WIDTH = Dimensions
  .get('window')
  .width;
const MARGIN = 45;
const BORDER_RADIUS = 7;

const styles = StyleSheet.create({
  input: {
    backgroundColor: Colors.loginBackgroundColor,
    width: DEVICE_WIDTH - MARGIN,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: BORDER_RADIUS,
    color: Colors.colorBlack,
    borderColor: Colors.borderColorGray,
    borderWidth: 0.4
  },
  inputWrapper: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 35,
    top: 9
  }
});

export default connect(mapStateToProps, actions)(UserInput);
