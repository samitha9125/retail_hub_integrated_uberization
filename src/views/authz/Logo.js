import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Colors from '../../config/colors';
import logoImg from '../../../images/dialog_logo.png';
class Logo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      locals: {
        mottoTxt: 'Enabling Excellent Service'
      }
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={logoImg} style={styles.image}/>
        <Text 
          style={styles.text}
          accessibilityLabel={'text_view_enabling_excellent_service'} 
          testID={'text_view_enabling_excellent_service'}
        >{this.state.locals.mottoTxt}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: 185,
    height: 125,
    justifyContent: 'center'
  },
  text: {
    color: Colors.colorLightBlack,
    backgroundColor: Colors.colorTransparent,
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginTop: 20,
    fontSize: 18,
    marginRight: 5
  }
});

export default Logo;
