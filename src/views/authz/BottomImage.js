/*
 * File: BottomImage.js
 * Project: Dialog Sales App
 * File Created: Friday, 15th June 2018 7:07:59 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 18th June 2018 5:30:26 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { View, Text, StyleSheet, Keyboard, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Colors, Images, globalConfig }  from '../../config/';
import { MessageUtils } from '../../utills';
import Utills from '../../utills/Utills';
import UpdateChecker from '../../utills/UpdateChecker';
import strings from '../../Language/settings';
import _ from 'lodash';

const Utill = new Utills();
class BottomImage extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      current_login_pin_number: '',
      locals: {
        forgot_pin: 'Forgot your PIN ?',
        first_time_here: 'First time here ?',
        get_started: 'GET STARTED',
        dummy_text: 'Let us know your mobile number to begin',
        forgot_your_pin : 'Call retailer hotline to reset your PIN',
        seems_u_r_register_as_retailer: 'Seems you are not registered as a Dialog Retailer\n\nDo you want to join?',
        do_u_want_join: 'Do you want to join?',
        let_us_know_mobile_number: 'Let us know your mobile number to begin',
        footer_note: 'Note : The SIM of above number needs to be in this phone',
        welcome_modal_text: 'Welcome to\nDialog Sales App!',
        assign_pin_to_begin: 'Assign a PIN to begin',
        assign_new_pin_to_begin: 'Assign a new PIN to begin',
        pin_is_successfully_sent : 'PIN is successfully sent',
        api_failure: 'API failure',
        system_error: 'System Error, Please try again later',
        pin_numbers_are_not_matched: 'PIN numbers are not matched',
        continue: 'CONTINUE',
        yes: 'YES',
        no: 'NO',
        ok: 'OK',
        set: 'SET',
        let_s_start: 'LET\'S START',
        mobile_number: 'Mobile Number',
        pin: 'PIN',
        new_pin: 'New PIN',
        re_confirm_pin: 'Reconfirm PIN',
        invalid_number: 'Invalid number',
        invalid_mobile_number: 'Invalid mobile number format',
        txtNumberSelectionTitle: strings.txtNumberSelectionTitle,
      }
    };
    this.deBouncedCallGetInStartApi = _.debounce(()=>this.callGetInStartApi(),globalConfig.buttonTapDelayTime,
      { 
        'leading': true,
        'trailing': false 
      });

    this.deBouncedCallForgotPin = _.debounce(()=>this.forgotPinAction(),globalConfig.buttonTapDelayTime,
      { 
        'leading': true,
        'trailing': false 
      });
  }

  componentWillReceiveProps(nextProps){
    console.log('nextProps', nextProps);
  }

/**
 * @description Forgot your PIN actions goes here
 * @memberof BottomImage
 */
forgotPinAction = () => {
  const data = {
    action: 'forgotPin',
    controller: 'account',
  };

  this.props.authGetStartInApi(data, this.authGetStartInApiSuccessCb, this.authGetStartInApiFailureCb );
};

/**
 * @description Show modal dynamically according to backend action
 * @param {String} modalType
 * @param {Object} additionalProps
 * @memberof BottomImage
 */
getInStartAction = (modalType, additionalProps) => {
  console.log('xxx getInStartAction');
  let passProps = {};
  let screenId = '';
  let primaryPress;
  let modalHeightRatio = 0.4;
  let hideTopImageView = false;
  let primaryButtonStyle = {};

  switch (modalType) {
    case "WELCOME_DIALOG_S1":  
      screenId = 'DialogRetailerApp.modals.PinResetModalScreen';
      passProps =  {
        titleText : this.state.locals.welcome_modal_text,
        primaryText : this.state.locals.set,
        icon : Images.icons.DialogLogo, 
        description : this.state.locals.assign_pin_to_begin,  
        primaryPress : this.onPressAssignPin,
        disabled : false,
        hideTitleView: false,
        onInputTextChange_1 : this.onInputTextChange_1,
        onInputTextChange_2 : this.onInputTextChange_2,      
        textInputLabel_1 : this.state.locals.pin,
        textInputLabel_2 : this.state.locals.re_confirm_pin,
        textInputErrorValue : this.state.locals.invalid_number,
        additionalProps: additionalProps       
      };
      break;
        
    case "WELCOME_NON_DIALOG_S2": 
    case "WELCOME_NON_DIALOG_S4":
      screenId = 'DialogRetailerApp.modals.GeneralModalAuthScreen';
      passProps = {
        primaryPress:this.onPressSendOtp,
        disabled:false,
        primaryText:this.state.locals.continue,
        titleText:this.state.locals.welcome_modal_text,
        icon:Images.icons.DialogLogo,
        textInputLabel:this.state.locals.mobile_number,
        textInputErrorValue:this.state.locals.invalid_mobile_number,
        description:this.state.locals.let_us_know_mobile_number,
        footerDescription:this.state.locals.footer_note,
        needTextInput:true,
        needTwoButton: false,
        needFooterDescription:true, 
        additionalProps: additionalProps 
      };
      break;
    case "JOIN_TO_APP_S3":
    case 'JOIN_TO_APP_S4':
      screenId = 'DialogRetailerApp.modals.GeneralModalAuthScreen';
      passProps = {
        modalHeightRatio: 0.55, 
        titleText:this.state.locals.welcome_modal_text,
        icon:Images.icons.DialogLogo,
        description:this.state.locals.seems_u_r_register_as_retailer,
        primaryPress:() => this.showRegisterScreen(additionalProps.device_data),
        disabled:false,
        secondaryPress:this.modalDismiss,
        primaryText:this.state.locals.yes,
        secondaryText:this.state.locals.no,
        needTextInput:false,
        needTwoButton: true,
        needFooterDescription:false,  
        additionalProps: additionalProps          
      };
      break;
    case "OTP_AUTO_DETECT_S2":
      screenId = 'DialogRetailerApp.modals.OTPModalScreen';
      passProps =  {
        onPressVerify: this.onPressVerifyOneTimePwd,
        onPressResend: this.onPressReSendOtp,
        OTPType: modalType,
        otpRef: additionalProps.info.reference_id,
        mobileNo: additionalProps.info.sender_msisdn,
        isResendApiActive: this.props.auth_api_loading,
        additionalProps: additionalProps,
        otpRexEx: additionalProps.info.reg_exp
      };
      break;
    case "NEW_PIN_S2":
      screenId = 'DialogRetailerApp.modals.PinResetModalScreen';
      passProps =  {
        titleText : this.state.locals.welcome_modal_text,
        primaryText : this.state.locals.set,
        icon : Images.icons.DialogLogo, 
        description : this.state.locals.assign_new_pin_to_begin,  
        primaryPress : this.onPressAssignPin,
        disabled : false,
        hideTitleView: true,
        onInputTextChange_1 : this.onInputTextChange_1,
        onInputTextChange_2 : this.onInputTextChange_2,      
        textInputLabel_1 : this.state.locals.new_pin,
        textInputLabel_2 : this.state.locals.re_confirm_pin,
        textInputErrorValue : this.state.locals.invalid_number,
        additionalProps: additionalProps  
      };
      break;
    case "AGENT_LOGIN": 
      primaryPress = this.modalDismiss;
      modalHeightRatio = 0.27;
      primaryButtonStyle = { color: Colors.colorDarkOrange };
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: primaryPress,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio, 
        description:additionalProps.message.description,
        primaryButtonStyle  : primaryButtonStyle,
        additionalProps: additionalProps
      };
      break;
    case "ALERT_SUCCESS_S1":
    case "ALERT_SUCCESS_S2":
    case "ALERT_SUCCESS_S3":
    case "ALERT_SUCCESS_LETS_START":   
      switch (modalType) {
        case "ALERT_SUCCESS_S1":
        case "ALERT_SUCCESS_S2":
        case "ALERT_SUCCESS_S3":
          primaryPress = this.actionPressAlertOk;
          break;
        case "ALERT_SUCCESS_LETS_START":
          primaryPress = this.actionLogin;
          break;   
        default:
          primaryPress = this.modalDismiss;
          break;
      }
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: primaryPress,
        primaryText: additionalProps.message.button,
        disabled:false,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio, 
        icon:Images.icons.SuccessAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps
      };
      break;
    case "ALERT_NOTICE_CONTACT_DIALOG_TODO":
      modalHeightRatio = 0.27;
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_NOTICE_CONTACT_DIALOG_PIN_RESET":
    case "ALERT_NOTICE_CONTACT_DIALOG":  
      modalHeightRatio = 0.27;
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        customDescriptionTextView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_NOTICE":
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: hideTopImageView,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_FAILURE":
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: () => this.modalDismiss(),
        primaryText: this.state.locals.ok,
        disabled: false,
        hideTopImageView: hideTopImageView,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon: Images.icons.FailureAlert,
        description: additionalProps.error,
        primaryButtonStyle  : { color: Colors.colorRed }
      };  
      break;
    default:
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: () => this.modalDismiss(),
        primaryText: this.state.locals.ok,
        disabled: false,
        modalHeightRatio: modalHeightRatio,
        icon: Images.icons.FailureAlert,
        description: this.state.locals.system_error,
        primaryButtonStyle  : { color: Colors.colorRed }
      };
  }

  const navigatorOb = this.props.navigator;
  navigatorOb.showModal({
    screen: screenId,
    title: 'Welcome',
    passProps: passProps,
    overrideBackPress: true
  });

}

  onInputTextChange = (text) =>{
    console.log('xxx onInputTextChange', text);
  }

  onInputTextChange_1 = (text) =>{
    console.log('xxx onInputTextChange_1', text);
  }

  onInputTextChange_2 = (text) =>{
    console.log('xxx onInputTextChange_2', text);
  }

  /**
   * @description dismiss modal
   * @memberof BottomImage
   */
  modalDismiss = () => {
    console.log('xxx modalDismiss');
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

/**
 * @description Show home screen after successful login
 * @param {Object} data
 * @memberof BottomImage
 */
showHomeScreen = (data)=> {
  console.log('xxx showHomeScreen');
  //Check if there "null" or "undefined" is received to tile data and prevent login
  if (data.data.tileData === undefined || data.data.tileData === null) {
    let modalHeightRatio = 0.35;
    let error = this.state.locals.system_error;
    this.showActionScreen('ALERT_FAILURE', { error, modalHeightRatio });
    return;
  }
  this.props.setAllowedServices(data.data.data); //Not using
  this.props.setTileData(JSON.stringify(data.data.tileData));

  const { ezCash =[], rapidEz =[], selectedIndexes ={ ezCash:"", rapidEz:"" } } = data.data;
      
  var numberSelectionArray = {
    ezCash: ezCash,
    rapidEz: rapidEz, 
    selectedIndexes: selectedIndexes
  };

  let ezCashArray =  numberSelectionArray.ezCash;
  let rapidEzArray = numberSelectionArray.rapidEz;
  // let selectedIndexes = numberSelectionArray.selectedIndexes;

  let showNumberSelectionModal = false;

  if (ezCashArray.length == 0 && rapidEzArray.length == 0){
    showNumberSelectionModal = false;
  }

  if (ezCashArray.length == 1 && rapidEzArray.length == 1){
    showNumberSelectionModal = false;
  }

  if ((ezCashArray.length == 1 && rapidEzArray.length > 1) && selectedIndexes.rapidEz === ""){
    showNumberSelectionModal = true;
  }

  if ((rapidEzArray.length == 1 && ezCashArray.length > 1) && selectedIndexes.ezCash === ""){
    showNumberSelectionModal = true;
  }

  if (ezCashArray.length > 1 && selectedIndexes.ezCash === ""){
    showNumberSelectionModal = true;
  }

  if (rapidEzArray.length > 1 && selectedIndexes.rapidEz === ""){
    showNumberSelectionModal = true;
  }

  if (selectedIndexes.ezCash !== "" && selectedIndexes.rapidEz !== ""){
    showNumberSelectionModal = false;
  }


  console.log("xxx showHomeScreen :: DATA: ", JSON.stringify(data.data));
  console.log("xxx showHomeScreen :: numberSelectionArray DATA: ", JSON.stringify(numberSelectionArray));

  if (showNumberSelectionModal == true && this.props.isRetailer == false){
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      screen: 'DialogRetailerApp.views.NumberSelectionScreen',
      title: this.state.locals.txtNumberSelectionTitle,
      passProps: {
        availableNumbers: numberSelectionArray,
        onUnmount: () => {
          console.log('xxx showHomeScreen :', data);
          const navigator = this.props.navigator;
          this.setState({ isLoading: false, loginBtnTap: false });
          Keyboard.dismiss();
          navigator.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
        }
      },
      navigatorButtons: {
        leftButtons: [
          {}
        ]
      },
      overrideBackPress: true
    });
  }
  else {
    const navigator = this.props.navigator;
    this.setState({ isLoading: false, loginBtnTap: false });
    Keyboard.dismiss();
    navigator.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });         
  }
}

/**
 * @description login action 
 * @memberof BottomImage
 */
actionLogin = () =>{
  console.log('xxx actionLogin');
  this.modalDismiss();
  const data = {
    action: 'login',
    controller: 'account',
    lang: 'en',
    pin: this.state.current_login_pin_number,
  };

  let self = this;
  const loginSuccessCb = function (response) {
    console.log('xxx actionLogin :: loginSuccessCb', response);
    let data = response.data;
    let action = data.action;

    if (data.heart_interval) {
      console.log('heart_interval', data);
      AsyncStorage.setItem('heartBeatInterval', data.heart_interval.toString(), () => {
        AsyncStorage.getItem('heartBeatInterval', (err, result) => {
          console.log('heart_interval2 ', result);
        });
      });
    }

    if (action == 'SUCCESS_LOGIN'){
      console.log('Login Success :: normal flow');
      self.showHomeScreen(response);
    }
    else {
      console.log('Login Success :: special flow');
      self.authGetStartInApiSuccessCb.apply(this, arguments);
    }
   
  };

  this.props.authGetStartInApi(data, loginSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Action alert press ok
 * @memberof BottomImage
 */
actionPressAlertOk = () =>{
  this.modalDismiss();
  console.log('xxx actionPressAlertOk');
}

/**
 * @description Show register screen
 * @param {Object} deviceData
 * @memberof BottomImage
 */
showRegisterScreen = (deviceData) =>{
  console.log('xxx showRegisterScreen :: deviceData', deviceData);
  this.modalDismiss();
  let self = this;
  const navigatorOb = this.props.navigator;
  navigatorOb.push({
    title: 'RETAILER', 
    screen: 'DialogRetailerApp.views.RetailerRegistrationScreen',
    passProps: {
      authGetStartInApiSuccessCb: self.authGetStartInApiSuccessCb,
      authGetStartInApiFailureCb: self.authGetStartInApiFailureCb,
      verified_otp_number: deviceData.msisdn
    }
  });
}


/**
 * @description Assign new PIN
 * @param {Number} pin1
 * @param {Number} pin2
 * @param {Number} additionalProps
 * @returns null
 * @memberof BottomImage
 */
onPressAssignPin = (pin1, pin2, additionalProps) => {
  console.log('xxx onPressAssignPin :: pin1, pin2, additionalProps', pin1, pin2, additionalProps);
  if (pin1 == ''|| pin2 == '' ) {
    Utill.showAlertMsg(this.state.locals.pin_numbers_are_not_matched);
    return;
  }
    
  if (pin1 != pin2) {
    Utill.showAlertMsg(this.state.locals.pin_numbers_are_not_matched);
    return;
  }
  const data = {
    action: 'pinReset',
    controller: 'account',
    pin1: pin1,
    pin2: pin2,
    msisdn : additionalProps.device_data.msisdn
  };

  this.setState({ current_login_pin_number: pin1 });

  let self = this;
  const pinResetSuccessCb = function (response) {
    console.log('xxx pinResetSuccessCb', response);
    self.modalDismiss();
    self.authGetStartInApiSuccessCb.apply(this, arguments);
  };

  this.props.authGetStartInApi(data, pinResetSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Resend OTP via SMS
 * @param {Number} number
 * @param {Number} otpRef
 * @memberof BottomImage
 */
onPressReSendOtp = (number, otpRef) => {
  console.log('xxx onPressReSendOtp :: number,otpRef ', number, otpRef);
  this.modalDismiss();//TODO
  const data = {
    action: 'getStartSendOtp',
    controller: 'account',
    otp_msisdn : number,
    ref_id: otpRef
  };

  const reSendOtpSuccessCb = function (response) {
    console.log('xxx reSendOtpSuccessCb', response);
  };

  this.props.authGetStartInApi(data, this.authGetStartInApiSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Send OTP via SMS
 * @param {Number} number
 * @memberof BottomImage
 */
  onPressSendOtp = (number) =>{
    console.log('xxx onPressSendOtp', number);
    const data = {
      action: 'getStartSendOtp',
      controller: 'account',
      otp_msisdn : number,
    };

    let self = this;
    const sentOtpSuccessCb = function (response) {
      console.log('xxx sentOtpSuccessCb', response);
      self.modalDismiss();
      self.authGetStartInApiSuccessCb.apply(this, arguments);
    };

    this.props.authGetStartInApi(data, sentOtpSuccessCb, this.authGetStartInApiFailureCb );
  }
  
  /**
   * @description Verify OTP value with backend
   * @param {Number} OTPValue
   * @param {Number} otpRef
   * @param {Number} mobileNo
   * @memberof BottomImage
   */
onPressVerifyOneTimePwd = (OTPValue, otpRef, mobileNo) => {
  console.log('xxx onPressVerifyOneTimePwd :: OTPValue, otpRef, mobileNo =>', OTPValue, otpRef, mobileNo);
  const data = {
    action: 'getStartValidateOtp',
    controller: 'account',
    otp_msisdn : mobileNo,
    otp_pin: OTPValue,
    ref_id: otpRef

  };

  let self = this;
  const verifyOneTimePwdSuccessCb = function (response) {
    console.log('xxx verifyOneTimePwdSuccessCb', response);
    self.modalDismiss();
    self.authGetStartInApiSuccessCb.apply(this, arguments);
  };

  
  this.props.authGetStartInApi(data, verifyOneTimePwdSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Get in start API call
 * @memberof BottomImage
 */
callGetInStartApi = () =>{
  console.log('xxx callGetInStartApi');
  UpdateChecker.updateCheck(this.updateCheckSuccessCb);
}

/**
 * @description UpdateCheck Success Callback
 * @param {Object} response
 * @memberof BottomImage
 */
updateCheckSuccessCb = (response)  => {
  console.log('xxx updateCheckSuccessCb', response);
  const data = {
    action: 'getStart',
    controller: 'account'
  };

  this.props.authGetStartInApi(data, this.authGetStartInApiSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Get in start API call Success Callback
 * @param {Object} actionData
 * @memberof BottomImage
 */
authGetStartInApiSuccessCb = (actionData) => {
  console.log('xxx authGetStartInApiSuccessCb', actionData.data);
  let data = actionData.data;
  let action = data.action;
  this.getInStartAction(action, data);
}

/**
 * @description Get in start API call Failure Callback
 * @param {Object} errorData
 * @memberof BottomImage
 */
authGetStartInApiFailureCb = (errorData) => {
  console.log('xxx authGetStartInApiFailureCb', errorData);
  let error;
  if (!_.isNil(errorData.data)) {
    error = MessageUtils.getFormatedErrorMessage(errorData.data);
  } else {
    error = MessageUtils.getExceptionMessage(errorData);
  }
  console.log('xxx authGetStartInApiFailureCb :: error ', error);
  this.getInStartAction('ALERT_FAILURE', { error });
}

render() {
  return (
    <View style={styles.containerWrapper}>
      <View style={styles.forgotPin}>
        <Text
          style={styles.forgotPinText}
          onPress=
            {() => this.deBouncedCallForgotPin() }
          accessibilityLabel={'text_view_forgot_pin'} 
          testID={'text_view_forgot_pin'}
        >{this.state.locals.forgot_pin}</Text>
      </View>
      <View style={styles.getInStart}>
        <View style={styles.getInStartTextContainer}>
          <Text 
            style={styles.getInStartText}
            accessibilityLabel={'text_view_first_time_here'} 
            testID={'text_view_first_time_here'}
          >{this.state.locals.first_time_here}</Text>
        </View>
        <View style={styles.getInStartTextButtonContainer}>
          <Text
            style={styles.getInStartTextButton}
            onPress=
              {() => this.deBouncedCallGetInStartApi()}
            disabled={this.props.auth_api_loading}
            accessibilityLabel={'text_view_get_started'} 
            testID={'text_view_get_started'}>
            {this.state.locals.get_started}</Text>
        </View>
      </View>
    </View>
  );
}
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: BottomImage => auth', state.auth);
  const Language = state.lang.current_lang;
  const auth_api_loading = state.auth.auth_api_loading;
  const auth_get_start_data = state.auth.auth_get_start_data;

  return {  Language, auth_api_loading, auth_get_start_data };
};


const styles = StyleSheet.create({
  containerWrapper: {
    flex: 3
  },

  forgotPin: {
    flex: 0.5,
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  getInStart: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  forgotPinText: {
    fontSize: 15,
    color: Colors.urlLinkColor,
    textDecorationLine: 'underline',
    fontWeight: '400'
  },

  getInStartTextContainer: {
    flex: 1
  },
  getInStartTextButtonContainer: {
    flex: 1
  },
  getInStartText: {
    alignSelf: 'flex-end',
    fontSize: 18,
    marginRight: 8
  },
  getInStartTextButton: {
    fontSize: 19,
    alignSelf: 'flex-start',
    fontWeight: '500',
    marginLeft: 5,
    color: Colors.gettingStartedButtonTextColor
  }
});

export default connect(mapStateToProps,actions)(BottomImage);

