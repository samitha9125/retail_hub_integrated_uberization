import React from 'react';
import { Linking, BackHandler, NetInfo, Platform, PermissionsAndroid, AsyncStorage, View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import BottomImage from './BottomImage';
import AppVersion from './AppVersion';
import Utills from '../../utills/Utills';
import DeviceInfo from 'react-native-device-info';
import strings from '../../Language/general';
import DeviceCompatibleModel from './DeviceCompatibleModal';
import Global from '../../config/globalConfig';
import { Navigation } from 'react-native-navigation';
import { Colors } from '../../config';
const Utill = new Utills();

class LoginMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      loading: false,
      userLogged: false,
      modalVisible: false,
      msisdn: '',
      loginButtonDisabled: false,
      LimitedFunctions: [],
      simChange: strings.SimChange,
      BillPay: strings.BillPay,
      MobActivate: strings.MobActivate,
      VoLteActivation: strings.VoLteActivation,
      ActivationState: strings.ActivationState,
      doc990: strings.doc990,
      WorkOrders: strings.WorkOrders,
      transactionHistory: strings.transactionHistory,
      DTVActivation: strings.DTVActivation,

      locals: { ok: strings.ok },
      deviceCompatibleInfo: {
        description: "",
      },
      disabled: false,
      connectionType: ''
    };
  }

  componentWillMount() {
    this.props.resetAuth();
    Utill.setLang('en');
  }

  async componentDidMount() {
    console.log("#### authz :: componentDidMount");
    console.log("Global.heartBeatTimer", Global.heartBeatTimer);
    await this.requestAppPermission();
    await this.requestCameraPermission();
    await this.requestStoragePermission();
    let isFirstTime = await AsyncStorage.getItem('isFirstTime');
    console.log("isFirstTime: ", isFirstTime);
    if (isFirstTime !== 'yes')
      this.versionCompatibilityCheck();
    Utill.retrieveItem('isMsisdn').then((item) => {
      if (item != null && item == "true") {
        this.setState({ loginButtonDisabled: true });
      }
    }).catch((error) => {
      console.log('Promise is rejected with error: ' + error);
    });
    if (Global.heartBeatTimer !== undefined) {
      console.log("Global.heartBeatTimer cleared");
      clearInterval(Global.heartBeatTimer);
      Global.heartBeatTimer = undefined;
    }

    await this.props.resetBillPaymentState();
    await this.props.getresetPendingOrders();
    await this.props.genaralResetAll();
    await this.props.resetMobileActivationState();
    await this.props.resetPinState();
    await this.props.resetSimChangeState();
    await this.props.transResetAll();

  }

  /**
 * This function maps the title of the functions when an id is passed.
 * 
 * @param {Number} id 
 * @returns {Object} {title:''}
 * @memberof LoginMain
 */
  mapIdToData(id) {
    const DATA_MAPPING = [{
      "id": 1,
      "title": this.state.BillPay
    },
    {
      "id": 2,
      "title": this.state.simChange
    },
    {
      "id": 3,
      "title": this.state.MobActivate
    },
    {
      "id": 4,
      "title": this.state.DTVActivation
    },
    {
      "id": 5,
      "title": this.state.VoLteActivation
    },
    {
      "id": 6,
      "title": this.state.transactionHistory
    },
    {
      "id": 7,
      "title": this.state.WorkOrders
    },
    {
      "id": 8,
      "title": this.state.doc990
    }];

    let data = {};
    DATA_MAPPING.forEach(function (item) {
      if (item.id == id) {
        data.title = item.title;
      }
    });
    return data;
  }

  versionCompatibilityCheck() {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
      this.setState({ netWorkType: connectionInfo.type, connectionType: connectionInfo.effectiveType, isLoading: true }, () => {

        var data = {
          deviceid: DeviceInfo.getModel(),
          deviceName: DeviceInfo.getBrand(),
          deviceRAM: DeviceInfo.getTotalMemory(),
          memory: DeviceInfo.getTotalDiskCapacity(),
          msisdn: this.state.msisdn,
          manufacture: DeviceInfo.getManufacturer(),
          network: this.state.connectionType,
          cellularType: this.state.netWorkType,
          androidVersion: DeviceInfo.getAPILevel(),
        };
        Utill.apiRequestPost2('CompatibleCheck', 'initAct', 'ccapp', data, this.versionCompatibilityCheckSuccessCb, this.failureCb, this.exCb);
      });
    });
  }

  versionCompatibilityCheckSuccessCb = (response) => {
    console.log('versionCompatibilityCheckSuccessCb :', response);
    this.setState({ isLoading: false, deviceCompatibleInfo: response.data.info });
    if (response.data.info.limited_functions) {
      response.data.info.limited_functions.map((id, index) => {
        let mappedData = this.mapIdToData(parseInt(id));
        this.setState({ LimitedFunctions: this.state.LimitedFunctions.concat({ "function": mappedData.title, "id": id }) });
      });
      var mutate = this.state.deviceCompatibleInfo["LimitedFunctions"] = this.state.LimitedFunctions;
      this.setState({ DeviceCompatibleModel: mutate });
    }

    console.log("Device array", this.state.deviceCompatibleInfo);
    if (response.data.info.state == 'pending' || response.data.info.state == 'blocked') {
      Utill.retrieveItem('msisdn').then((item) => {
        console.log('ITEM', item);
        if (item == null) {
          this.setState({ modalVisible: true });
        } else if (response.data.info.state == 'blocked') {
          this.setState({ modalVisible: true });
        }
      }).catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
    }
  }

  failureCb = (response) => {
    console.log("failureCb : ", response);
    this.setState({ isLoading: false });
  }

  exCb = (response) => {
    console.log("exCb  : ", response);
    this.setState({ isLoading: false });
  }

  onTextChange(text) {
    this.setState({ msisdn: text });
  }

  openUrl() {
    var spillable = this.state.deviceCompatibleInfo.url.split('/');
    var action = spillable[0];
    var controller = spillable[1];
    let url = Utill.createApiUrl(controller, action, '');
    console.log(url);
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        return Linking.openURL(url);
      }
    }).catch(err => console.log('An error occurred', err));
  }

  onOkPress() {
    const data = {
      msisdn: this.state.msisdn,
      deviceModel: DeviceInfo.getModel(),
      androidVersion: DeviceInfo.getAPILevel(),
      devicePlatform: Platform.OS,
      appVersion: DeviceInfo.getVersion(),
      imei: DeviceInfo.getUniqueID()
    };
    this.setState({ isLoading: true, modalVisible: false });
    if (this.state.deviceCompatibleInfo.state == 'blocked') {
      Utill.apiRequestPost2('BlockedStateDevice', 'initAct', 'ccapp', data, this.success, this.failure, this.ex);
    } else if (this.state.deviceCompatibleInfo.state == 'pending') {
      this.deviceStateChange("false");
    }
  }

  failure = (response) => {
    console.log(response);
    this.setState({ isLoading: false });
    BackHandler.exitApp();
  }

  ex = (error) => {
    console.log(error);
    this.setState({ isLoading: false });
    BackHandler.exitApp();
  }

  success = (response) => {
    this.setState({ isLoading: false, loginButtonDisabled: true });
    this.deviceStateChange("true");
    BackHandler.exitApp();
  }

  deviceStateChange(value) {
    var item1 = Utill.storeItem('msisdn', this.state.msisdn);
    var isMsisdn = Utill.storeItem('isMsisdn', value);
    console.log('stored item :', item1);
    console.log('stored isMsisdn :', isMsisdn);
  }

  async requestAppPermission() {
    console.log('************** Request Phone Permission');
    try {
      let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE);
      console.log('************** Request Phone Permission 1');
      if (!granted) {
        console.log('************** Phone Permission Not Granted');
        granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('************** You can use the Phone');
        } else {
          console.log('************** Phone permission denied');
        }
      } else {
        console.log('************** Phone permission is already granted');
      }
    } catch (err) {
      console.log('************** Request Phone Permission error');
    }
  }

  async requestCameraPermission() {
    console.log('************** Request Camera Permission');
    try {
      let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
      console.log('************** Request Camera Permission 1');
      if (!granted) {
        console.log('************** Camera Permission Not Granted');
        granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('************** You can use the Camera');
        } else {
          console.log('************** Camera permission denied');
        }
      } else {
        console.log('************** Camera permission is already granted');
      }
    } catch (err) {
      console.log('************** Request Camera Permission error');
    }
  }

  async requestStoragePermission() {
    console.log('************** Request WRITE_EXTERNAL_STORAGE Permission');
    try {
      let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      console.log('************** Request WRITE_EXTERNAL_STORAGE Permission 1');
      if (!granted) {
        console.log('************** File Permission Not Granted');
        granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('************** You can use the WRITE_EXTERNAL_STORAGE');
        } else {
          console.log('************** WRITE_EXTERNAL_STORAGE permission denied');
        }
      } else {
        console.log('************** WRITE_EXTERNAL_STORAGE permission is already granted');
      }
    } catch (err) {
      console.log('************** Request WRITE_EXTERNAL_STORAGE Permission error');
    }
  }

  onPress = () => {
    this.props.navigator.push({
      // title: _this.state.BillPay,
      screen: 'DialogRetailerApp.views.Uber',
      passProps: {
        // defaultEzNumber: defaultEzNumber
      }
    });

    // this.props.navigation.push({
    //   // title: 'PENDING WORK ORDERS',
    //   screen: 'DialogRetailerApp.views.settings.MainScreen',
    //   passProps: {

    //   }
    // });
  }
  render() {
    return (
      <Wallpaper>
        {/* <Logo />
        <Form />
        <ButtonSubmit loginButtonDisabled={this.state.loginButtonDisabled} navigator={this.props.navigator} />
        <BottomImage {... this.props} />
        <AppVersion />
        <DeviceCompatibleModel
          msisdn={this.state.msisdn}
          deviceCompatibleInfo={this.state.deviceCompatibleInfo}
          disabled={this.state.deviceCompatibleInfo.state == 'pending' ? false : (this.state.msisdn == '' || !Utill.gsmDialogNumvalidate(this.state.msisdn))}
          onOkPress={() => this.onOkPress()}
          onTextChange={text => {
            return this.onTextChange(text);
          }}
          modalVisible={this.state.modalVisible}
          locals={this.state.locals}
          openUrl={() => this.openUrl()}
        /> */}
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity style={{ padding: 30, backgroundColor: Colors.green }} onPress={this.onPress}>
            <Text style={{ color: Colors.white }}>Uber</Text>
          </TouchableOpacity>
        </View>
      </Wallpaper>

    );
  }
}

export default connect(null, actions)(LoginMain);
