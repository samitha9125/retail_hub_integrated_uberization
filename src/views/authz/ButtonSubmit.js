/*
 * File: ButtonSubmit.js
 * Project: Dialog Sales App
 * File Created: Friday, 15th June 2018 7:07:59 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net) 
 * -----
 * Last Modified: Tuesday, 26th June 2018 
 * Modified By:  Damith Nuwan Sampath (nuwan@omobio.net) 
 *               Nipuna H Herath (nipuna@omobio.net)
 *               Arafath Misree (arafath@omobio.net)
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Dimensions,
  Keyboard,
  PermissionsAndroid,
  AsyncStorage,
  Linking,
  NetInfo,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Colors, Images, globalConfig }  from '../../config/';
import { Analytics, Screen, LocalStorageUtils, MessageUtils } from '../../utills';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import strings from '../../Language/settings';
import _ from 'lodash';

const Utill = new Utills();

const MARGIN = 47;
const BORDER_RADIUS = 7;
const DEVICE_WIDTH = Dimensions.get('window').width;

class ButtonSubmit extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      loginBtnTap: false,
      isLoading: false,
      current_login_pin_number: '',
      locals: {
        loginButtonText: 'LOGIN',
        txtNumberSelectionTitle: strings.txtNumberSelectionTitle,
        accessRemoved: strings.accessRemoved,
        continue: strings.continue,
        ok: strings.ok,
        cancel: strings.cancel,
        forgot_pin: 'Forgot your PIN ?',
        first_time_here: 'First time here ?',
        get_started: 'GET STARTED',
        dummy_text: 'Let us know your mobile number to begin',
        seems_u_r_register_as_retailer: 'Seems you are not registered as a Dialog Retailer\n\nDo you want to join?',
        do_u_want_join: 'Do you want to join?',
        let_us_know_mobile_number: 'Let us know your mobile number to begin',
        footer_note: 'Note : The SIM of above number needs to be in this phone',
        welcome_modal_text: 'Welcome to\nDialog Sales App!',
        assign_pin_to_begin: 'Assign a PIN to begin',
        pin_is_successfully_sent: 'PIN is successfully sent',
        api_failure: 'API failure',
        system_error: 'System Error, Please try again later',
        pin_numbers_are_not_matched: 'PIN numbers are not matched',
        please_grant_phone_permission: 'Please grant phone permission',
        please_enter_pin_number: 'Please enter PIN number..!',
        new_update_available: 'New update available',
        yes: 'YES',
        no: 'NO',
        update: 'UPDATE',
        later: 'LATER',
        set: 'SET',
        let_s_start: 'LET\'S START',
        mobile_number: 'Mobile number',
        pin: 'PIN',
        new_pin: 'New PIN',
        re_confirm_pin: 'Reconfirm PIN',
        invalid_number: 'Invalid number',
        invalid_mobile_number: 'Invalid mobile number format',
      },
    };
    this.deBouncedCheckUpdateAndLogin = _.debounce(()=>this.checkUpdateAndLogin(),globalConfig.buttonTapDelayTime,
      { 
        'leading': true,
        'trailing': false 
      });
  }

  componentDidMount() {
    this.setState({ isLoading: false, loginBtnTap: false });
  }

  /**
   * @description Set retailer type as SUB or MAIN
   * @param {Boolean} isMainRetailer
   * @memberof ButtonSubmit
   */
  setRetailerType = async (isMainRetailer) =>  {
    let _this = this;
    console.log('## setRetailerType :: isMainRetailer - ', isMainRetailer);
    if (!isMainRetailer) {//TODO: need to clarify the CFSS change
      console.log('AGENT_IS_A MAIN_RETAILER');
	    console.log('## setRetailerType :: isMainRetailer - true');
	    try {
	      await AsyncStorage.setItem("isRetailer", "true");
	    } catch (error) {
	    console.log(`## setRetailerType :: AsyncStorage error: ${error.message}`);
	    }
	    _this.props.setRetailer(true);
	  } else {
      console.log('AGENT_IS_A SUB_RETAILER');
	    try {
	      console.log('## setRetailerType :: isMainRetailer - false');
	      await AsyncStorage.setItem("isRetailer", "false");
	    } catch (error) {
	      console.log(`## setRetailerType :: AsyncStorage error: ${error.message}`);
	    }
	    _this.props.setRetailer(false);
	  }

  }

  /**
   * @description Set CFSS agent type 
   * @param {Boolean} isCFSSAgent
   * @memberof ButtonSubmit
   */
  setCfssAgentType = async (isCFSSAgent) =>  {
    let _this = this;
    console.log('## setCfssAgentType :: isCFSSAgent - ', isCFSSAgent);
    if (isCFSSAgent) {
      console.log('AGENT_IS_A CFSS_AGENT');
	    console.log('## setCfssAgentType :: isCFSSAgent - true');
	    try {
	      await AsyncStorage.setItem("isCFSSAgent", "true");
	    } catch (error) {
	    console.log(`## setCfssAgentType :: AsyncStorage error: ${error.message}`);
	    }
	    _this.props.setAgentType(true);
	  } else {
      console.log('AGENT_IS_NOT_A CFSS_AGENT');
	    try {
	      console.log('## setCfssAgentType :: isCFSSAgent - false');
	      await AsyncStorage.setItem("isCFSSAgent", "false");
	    } catch (error) {
	      console.log(`## setCfssAgentType :: AsyncStorage error: ${error.message}`);
	    }
	    _this.props.setAgentType(false);
	  }

  }

  /**
   * @description Save heartbeat interval
   * @param {Int} heartbeat_interval
   * @memberof ButtonSubmit
   */
  setHeartbeatInterval = async (heartbeat_interval = null) =>  {
    console.log('setHeartbeatInterval :: heartbeat_interval - ', heartbeat_interval);
    if (heartbeat_interval) {
      await AsyncStorage.setItem('heartBeatInterval', heartbeat_interval.toString(), async () => {
        await AsyncStorage.getItem('heartBeatInterval', (err, result) => {
          console.log('setHeartbeatInterval :: getHeartBeatInterval - ', result);
        });
      });
    }
  }

  /**
   * @description Show home screen after successful login
   * @param {Object} response
   * @memberof ButtonSubmit
   */
  showHomeScreen = (response) =>  {
    console.log('showHomeScreen :: response', response.data);
    let _this = this;
    try {
      if (_.isNil(response.data.tileData)) {
        let modalHeightRatio = 0.35;
        let error = this.state.locals.system_error;
        this.showActionScreen('ALERT_FAILURE', { error, modalHeightRatio });
        return;
      }
      _this.props.setTileData(JSON.stringify(response.data.tileData));
      
      const { heart_interval = null } = response.data;
      const { sub_retailer = false, is_cfss_agent = false } = response.data.pre_data;
      const { enable = false, accounts = {} } = response.data.agent_wallet;
      const { ez_cash = [], rapid_ez = [], selected = { ez_cash: '', rapid_ez: '' }, prompt_selection = false } = accounts;
  
      console.log('showHomeScreen :: agent_wallet =>\n', response.data.agent_wallet);
  
      let numberSelectionArray = {
        ezCash: ez_cash,
        rapidEz: rapid_ez, 
        selectedIndexes: {
          ezCash : selected.ez_cash,
          rapidEz : selected.rapid_ez
        }
      };
  
      _this.setRetailerType(sub_retailer); 
      _this.setCfssAgentType(is_cfss_agent);
      _this.setHeartbeatInterval(heart_interval);
  
      let enable_wallet_manager = enable;
      let prompt_wallet_selection = prompt_selection;
  
      console.log('showHomeScreen :: agent_wallet => enable_wallet_manager & prompt_wallet_selection\n', enable_wallet_manager, prompt_wallet_selection);
  
      console.log("showHomeScreen :: DATA: ", JSON.stringify(response.data));
      console.log("showHomeScreen :: numberSelectionArray DATA: ", JSON.stringify(numberSelectionArray));
  
      if (enable_wallet_manager && prompt_wallet_selection){
        _this.showNumberSelectionScreen(response, numberSelectionArray);
        console.log('showHomeScreen :: SHOW NUMBER_SELECTION_MODAL');
      } else {
        LocalStorageUtils.saveUserDataAfterLogin(response);
        _this.resetToHomeScreen();
        console.log('showHomeScreen :: SHOW HOME_SCREEN');
      }
    } catch (error) {
      console.log('showHomeScreen :: SHOW HOME_SCREEN', error);
      let modalHeightRatio = 0.35;
      let err = this.state.locals.system_error;
      this.showActionScreen('ALERT_FAILURE', { error : err, modalHeightRatio });
    }   
  }

/**
 * @description Save banner images
 * @param {Object} data
 * @memberof ButtonSubmit
 */
saveBannerImages = async(data) =>  {
  console.log('saveBannerImages', data);
  await LocalStorageUtils.saveBannerData(data); 
}
 
/**
 * @description Reset to home screen
 * @memberof ButtonSubmit
 */
resetToHomeScreen = () =>  {
  console.log('resetToHomeScreen');
  const navigatorOb = this.props.navigator;
  this.setState({ isLoading: false, loginBtnTap: false });
  Keyboard.dismiss();
  this.props.commonGetBannerImages(this.saveBannerImages);
  navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });    
}

/**
 * @description Show modal eZ Cash and RapidEz number selection screen
 * @param {Object} response
 * @param {Array} numberSelectionArray
 * @memberof ButtonSubmit
 */
showNumberSelectionScreen = (response, numberSelectionArray)=> {
  console.log('showNumberSelectionScreen :: response =>', response);
  console.log('showNumberSelectionScreen :: numberSelectionArray =>', numberSelectionArray);
  let _this = this;
  const navigatorOb = _this.props.navigator;
  navigatorOb.showModal({
    screen: 'DialogRetailerApp.views.NumberSelectionScreen',
    title: _this.state.locals.txtNumberSelectionTitle,
    passProps: {
      availableNumbers: numberSelectionArray,
      login_success_response: response,
      popped_at_login: true,
      onUnmount: () => {
        Keyboard.dismiss();
        const navigatorObNew = this.props.navigator;
        _this.setState({ isLoading: false, loginBtnTap: false });
        console.log('showNumberSelectionScreen => showHomeScreen :: response', response);
        navigatorObNew.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
      }
    },
    navigatorButtons: {
      leftButtons: [
        {}
      ]
    },
    overrideBackPress: true
  });
}

/**
 * @description Show modal dynamically according to backend action
 * @param {String} modalType
 * @param {Object} additionalProps
 * @memberof ButtonSubmit
 */
showActionScreen = (modalType, additionalProps)=> {
  console.log('xxx showActionScreen :: action =>', modalType);
  let passProps = {};
  let screenId = '';
  let primaryPress;
  let deviceChanged = false;
  let modalHeightRatio = 0.4;
  let hideTopImageView = false;
  let primaryButtonStyle = {};

  switch (modalType) {
    case "WELCOME_DIALOG_S1":  
      screenId = 'DialogRetailerApp.modals.PinResetModalScreen';
      passProps =  {
        titleText : this.state.locals.welcome_modal_text,
        primaryText : this.state.locals.set,
        icon : Images.icons.DialogLogo, 
        description : this.state.locals.assign_pin_to_begin,  
        primaryPress : this.onPressAssignPin,
        disabled : false,
        hideTitleView: false,    
        textInputLabel_1 : this.state.locals.pin,
        textInputLabel_2 : this.state.locals.re_confirm_pin,
        textInputErrorValue : this.state.locals.invalid_number,
        additionalProps: additionalProps       
      };
      break;
				
    case "WELCOME_NON_DIALOG_S2":
    case "WELCOME_NON_DIALOG_S4":
      screenId = 'DialogRetailerApp.modals.GeneralModalAuthScreen';
      passProps = {
        primaryPress:this.onPressSendOtp,
        disabled:false,
        primaryText:this.state.locals.continue,
        titleText:this.state.locals.welcome_modal_text,
        icon:Images.icons.DialogLogo,
        textInputLabel:this.state.locals.mobile_number,
        textInputErrorValue:this.state.locals.invalid_mobile_number,
        description:this.state.locals.let_us_know_mobile_number,
        footerDescription:this.state.locals.footer_note,
        needTextInput:true,
        needTwoButton: false,
        needFooterDescription:true, 
        additionalProps: additionalProps 
      };
      break;
    case "JOIN_TO_APP_S3":
    case 'JOIN_TO_APP_S4':
      screenId = 'DialogRetailerApp.modals.GeneralModalAuthScreen';
      passProps = {
        modalHeightRatio: 0.55, 
        titleText:this.state.locals.welcome_modal_text,
        icon:Images.icons.DialogLogo,
        description:this.state.locals.seems_u_r_register_as_retailer,
        primaryPress:() => this.showRegisterScreen(additionalProps.device_data),
        disabled:false,
        secondaryPress:this.modalDismiss,
        primaryText:this.state.locals.yes,
        secondaryText:this.state.locals.no,
        needTextInput:false,
        needTwoButton: true,
        needFooterDescription:false,  
        additionalProps: additionalProps          
      };
      break;
    case "OTP_AUTO_DETECT_S2":
      screenId = 'DialogRetailerApp.modals.OTPModalScreen';
      passProps =  {
        onPressVerify: this.onPressVerifyOneTimePwd,
        onPressResend: this.onPressReSendOtp,
        OTPType: modalType,
        otpRef: additionalProps.info.reference_id,
        mobileNo: additionalProps.info.sender_msisdn,
        isResendApiActive: this.props.auth_api_loading,
        additionalProps: additionalProps,
        otpRexEx: additionalProps.info.reg_exp
      };
      break;
    case "NEW_PIN_S2":
      screenId = 'DialogRetailerApp.modals.PinResetModalScreen';
      passProps =  {
        titleText : this.state.locals.welcome_modal_text,
        primaryText : this.state.locals.set,
        icon : Images.icons.DialogLogo,
        hideTitleView: true,  
        description : this.state.locals.assign_pin_to_begin,  
        primaryPress : this.onPressAssignPin,
        disabled : false,    
        textInputLabel_1 : this.state.locals.new_pin,
        textInputLabel_2 : this.state.locals.re_confirm_pin,
        textInputErrorValue : this.state.locals.invalid_number,
        additionalProps: additionalProps  
      };
      break;
    case "FORCE_UPDATE":
      screenId = 'DialogRetailerApp.modals.CommonModalAlert';
      passProps =  {
        primaryPress: () =>this.actionAppUpdate(additionalProps.app_url),
        primaryText: this.state.locals.update,
        disabled:false,
        primaryButtonStyle  : { color: Colors.colorGreen },
        hideTopImageView: true,
        title : this.state.locals.new_update_available,
        titleTextStyle: { fontSize: 22 },
        description:additionalProps.info,
      };
      break;
    case "AGENT_LOGIN": 
      primaryPress = this.modalDismiss;
      modalHeightRatio = 0.27;
      primaryButtonStyle = { color: Colors.colorDarkOrange };
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: primaryPress,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio, 
        description:additionalProps.message.description,
        primaryButtonStyle  : primaryButtonStyle,
        additionalProps: additionalProps
      };
      break;
    case "ALERT_SUCCESS_S1":
    case "ALERT_SUCCESS_S2":
    case "ALERT_SUCCESS_S3":
    case "ALERT_SUCCESS_LETS_START":
    case "ALERT_SUCCESS_DEVICE_CHANGE_LETS_START":
      switch (modalType) {
        case "ALERT_SUCCESS_S1":
        case "ALERT_SUCCESS_S2":
        case "ALERT_SUCCESS_S3":
          primaryPress = this.actionPressAlertOk;
          break;
        case "ALERT_SUCCESS_LETS_START":
          deviceChanged = false;
          primaryPress = () =>this.actionLogin(deviceChanged);
          break;
        case "ALERT_SUCCESS_DEVICE_CHANGE_LETS_START":
          deviceChanged = true;
          primaryPress = () =>this.actionLogin(deviceChanged);
          break;
        default:
          primaryPress = this.modalDismiss;
          break;
      }
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: primaryPress,
        primaryText: additionalProps.message.button,
        disabled:false,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio, 
        icon:Images.icons.SuccessAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps
      };
      break;
    case "ALERT_NOTICE_DEVICE_CHANGE":
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.onPressDeviceChange,
        secondaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        secondaryText: additionalProps.message.buttonLeft,
        disabled:false,
        hideTopImageView: hideTopImageView,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange },
        secondaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_NOTICE_CONTACT_DIALOG_TODO": 
      modalHeightRatio = 0.27;
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_NOTICE_CONTACT_DIALOG_PIN_RESET": 
    case "ALERT_NOTICE_CONTACT_DIALOG":
      modalHeightRatio = 0.27;
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: true,
        customDescriptionTextView: true,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_NOTICE":
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: this.modalDismiss,
        primaryText: additionalProps.message.button,
        disabled:false,
        hideTopImageView: hideTopImageView,
        modalHeightRatio:additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon:Images.icons.AttentionAlert,
        description:additionalProps.message.description,
        additionalProps: additionalProps,
        primaryButtonStyle  : { color: Colors.colorDarkOrange }
      };
      break;
    case "ALERT_FAILURE":
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: () => this.modalDismiss(),
        primaryText: this.state.locals.ok,
        disabled: false,
        hideTopImageView: hideTopImageView,
        modalHeightRatio: additionalProps.modalHeightRatio ? additionalProps.modalHeightRatio:  modalHeightRatio,
        icon: Images.icons.FailureAlert,
        description: additionalProps.error,
        primaryButtonStyle  : { color: Colors.colorRed }
					
      };  
      break;
    default:
      screenId = 'DialogRetailerApp.modals.GeneralModalAlert';
      passProps = {
        primaryPress: () => this.modalDismiss(),
        primaryText: this.state.locals.ok,
        disabled: false,
        modalHeightRatio: modalHeightRatio,
        icon: Images.icons.FailureAlert,
        description: this.state.locals.system_error,
        primaryButtonStyle  : { color: Colors.colorRed }
      };
  }

  console.log('xxx showActionScreen :: screenId =>', screenId);
  console.log('xxx showActionScreen :: passProps =>', passProps);

  const navigatorOb = this.props.navigator;
  navigatorOb.showModal({
    screen: screenId,
    title: 'Welcome',
    passProps: passProps,
    overrideBackPress: true
  });

}

/**
 * @description Show login screen
 * @param {Boolean} deviceChange
 * @memberof ButtonSubmit
 */
actionLogin = (deviceChange = false) =>{
  console.log('xxx actionLogin');
  this.modalDismiss();
  let pin = deviceChange ? this.props.pin : this.state.current_login_pin_number;
  const data = {
    action: 'login',
    controller: 'auth',
    module: '',
    lang: 'en',
    pin: pin,
  };

  let _this = this;
  const loginSuccessCb = function (response) {
    console.log('xxx actionLogin :: loginSuccessCb', response);
    let data = response.data;
    let action = data.action;

    if (action == 'SUCCESS_LOGIN'){
      console.log('Login Success :: normal flow');
      console.log('Success Res: ', JSON.stringify(data));
      _this.showHomeScreen(response);
    }
    else {
      console.log('Login Success :: special flow');
      _this.authGetStartInApiSuccessCb.apply(_this, arguments);
    } 
  };

  this.props.authGetStartInApi(data, loginSuccessCb, this.authGetStartInApiFailureCb );
}

checkLanguage = async(response, langSuccess) =>{
  console.log("### checkLanguage");
  const { data = {} } = response;
  const { pre_data = {} } = data;
  const { lang=null, langIndex=null } = pre_data;
  console.log('lang: ', lang);
  console.log('langIndex: ', langIndex);
  await this.props.setLanguage('en', 0);
  if (lang == null || langIndex == null){
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      screen: 'DialogRetailerApp.views.settings.LanguageSelectionScreen',
      title: 'Welcome',
      passProps: {
        initialSetup: true,
        languageSelectionCb:()=>{
          langSuccess();
        }
      },
      navigatorButtons: {
        leftButtons: [
          {}
        ]
      },
      overrideBackPress: true
    });
  }
}

/**
 * @description Dismiss any modal
 * @memberof ButtonSubmit
 */
modalDismiss = () => {
  console.log('xxx modalDismiss');
  Navigation.dismissModal({ animationType: 'slide-down' });
};


/**
 * @description Action alert press ok
 * @memberof ButtonSubmit
 */
actionPressAlertOk = () =>{
  this.modalDismiss();
  console.log('xxx actionPressAlertOk');
}

/**
 * @description Press cancel to dismiss modal
 * @param {Object} response
 * @memberof ButtonSubmit
 */
actionDismissUpdate = (response) => {
  console.log('actionDismissUpdate :: data ', response);
  this.modalDismiss();
  this.userOnBoardingAction(response);
}
  

/**
 * @description Action App update
 * @param {String} app_url
 * @memberof ButtonSubmit
 */
actionAppUpdate = (app_url) =>{
  this.modalDismiss();
  console.log('xxx actionAppUpdate :: app_url ', app_url);
  try {
    console.log('Opening Google Play Store .....');
    Linking.openURL(app_url);
  } catch (error) {
    console.log('Opening Google Play Store - Error', error);
  }
}

/**
 * @description Show prompt update alert
 * @param {Object} response
 * @memberof ButtonSubmit
 */
showPromptUpdateAlert = (response) =>{
  console.log('showPromptUpdateAlert :: response.data ', response.data);
  let passProps =  {
    primaryPress: () =>this.actionAppUpdate(response.data.app_url),
    secondaryPress: () => this.actionDismissUpdate(response),
    primaryText: this.state.locals.update,
    secondaryText: this.state.locals.later,
    disabled:false,
    primaryButtonStyle  : { color: Colors.colorGreen },
    secondaryButtonStyle  : { color: Colors.colorGreen },
    hideTopImageView: true,
    title : this.state.locals.new_update_available,
    titleTextStyle: { fontSize: 22 },
    description:response.data.info,
  };

  Screen.showGeneralModal(passProps);
}

/**
 * @description Login button click action (Call version check API)
 * @memberof ButtonSubmit
 */
checkUpdateAndLogin = async() =>{
  // TODO: this.props.loginButtonDisabled == true
  if (this.state.isLoading || this.state.loginBtnTap || this.props.auth_api_loading ) {
    return;
  }
  const Permission = await this.checkPermissionReadPhoneState();
  if (!Permission) {
    let modalHeightRatio = 0.35;
    let error = this.state.locals.please_grant_phone_permission;
    Analytics.logEvent('dsa_login_android_permission_issue');
    this.showActionScreen('ALERT_FAILURE', { error, modalHeightRatio });
    return;
  }

  if (this.props.pin.length === 0) {
    let modalHeightRatio = 0.35;
    let error = this.state.locals.please_enter_pin_number;
    Analytics.logEvent('dsa_login_empty_pin');
    this.showActionScreen('ALERT_FAILURE', { error, modalHeightRatio });
    return;
  }

  this.loginEntryPoint();
}

/**
* @description User on-boarding action
* @param {Object} response
* @memberof ButtonSubmit
*/
userOnBoardingAction = (response) =>{
  console.log('userOnBoardingAction :: response ', response);	
  let _this = this;
  let data = response.data;
  let action = data.action;

  if (action == 'SUCCESS_LOGIN'){
    console.log('userOnBoardingAction :: SUCCESS_LOGIN');
    const { lang = null, langIndex = null } = response.data.pre_data;
    if (lang == null || langIndex == null){
      _this.checkLanguage(response, ()=> {
        console.log('userOnBoardingAction - Login Success :: set language');
        _this.showHomeScreen(response);
      });
    } else {
      console.log('userOnBoardingAction - Login Success :: normal flow');
      _this.showHomeScreen(response);
    }
    
  } else {
    console.log('userOnBoardingAction - special flow');
    _this.authGetStartInApiSuccessCb(response);
  }
}

/**
* @description Login entry point
* @memberof ButtonSubmit
*/
loginEntryPoint =()=>{
  let _this = this;
  console.log('loginEntryPoint');	
  NetInfo.getConnectionInfo().then((connectionInfo) => {  	
    const requestData = {
      action: 'login',
      controller: 'auth',
      module: '',
      lang: 'en',
      pin: this.props.pin,
      networkType: connectionInfo.type
    };

    const loginSuccessCb = function (response) {
      console.log('loginEntryPoint :: loginSuccessCb', response);
      let data = response.data;
      let prompt_update = data.prompt_update;
   
      if (prompt_update) {
        console.log('loginSuccessCb :: prompt_update');
        _this.showPromptUpdateAlert(response);
      } else {
        console.log('loginSuccessCb :: normal_flow');
        _this.userOnBoardingAction(response);
      }
    };

    console.log('loginEntryPoint :: requestData: \n', JSON.stringify(requestData));
    _this.props.authGetStartInApi(requestData, loginSuccessCb, _this.authGetStartInApiFailureCb );
  });
}

/**
 * @description Show register screen
 * @param {Object} deviceData
 * @memberof ButtonSubmit
 */
showRegisterScreen = (deviceData) =>{
  console.log('xxx showRegisterScreen :: deviceData', deviceData);
  this.modalDismiss();
  let _this = this;
  const navigatorOb = this.props.navigator;
  navigatorOb.push({
    title: 'RETAILER', 
    screen: 'DialogRetailerApp.views.RetailerRegistrationScreen',
    passProps: {
      authGetStartInApiSuccessCb: _this.authGetStartInApiSuccessCb,
      authGetStartInApiFailureCb: _this.authGetStartInApiFailureCb,
      verified_otp_number: deviceData.msisdn
    }
  });
}

/**
 * @description Assign new PIN
 * @param {Number} pin1
 * @param {Number} pin2
 * @param {Number} additionalProps
 * @returns null
 * @memberof ButtonSubmit
 */
onPressAssignPin = (pin1, pin2, additionalProps) =>{
  console.log('xxx onPressAssignPin :: pin1, pin2, additionalProps', pin1, pin2, additionalProps);
  if (pin1 == ''|| pin2 == '' ) {
    Utill.showAlertMsg(this.state.locals.pin_numbers_are_not_matched);
    return;
  }
		
  if (pin1 != pin2) {
    Utill.showAlertMsg(this.state.locals.pin_numbers_are_not_matched);
    return;
  }
  const data = {
    action: 'pinReset',
    controller: 'account',
    pin1: pin1,
    pin2: pin2,
    msisdn : additionalProps.device_data.msisdn
  };

  this.setState({ current_login_pin_number: pin1 });

  let _this = this;
  const pinResetSuccessCb = function (response) {
    console.log('xxx pinResetSuccessCb', response);
    _this.modalDismiss();
    _this.authGetStartInApiSuccessCb.apply(_this, arguments);
  };

  this.props.authGetStartInApi(data, pinResetSuccessCb, _this.authGetStartInApiFailureCb );
}

/**
 * @description Resend OTP via SMS
 * @param {Number} number
 * @param {Number} otpRef
 * @param {Object} additionalProps
 * @memberof ButtonSubmit
 */
onPressReSendOtp = (number, otpRef, additionalProps) =>{
  console.log('xxx onPressReSendOtp :: number,otpRef, additionalProps ', number, otpRef, additionalProps);
  this.modalDismiss();//TODO
  const data = {
    action: 'getStartSendOtp',//TODO sendNewOTP
    controller: 'account',//TODO common
    otp_msisdn : number,
    ref_id: otpRef,
    otp_data: additionalProps.otp_data
  };

  const reSendOtpSuccessCb = function (response) {
    console.log('xxx reSendOtpSuccessCb', response);
  };

  this.props.authGetStartInApi(data, this.authGetStartInApiSuccessCb, this.authGetStartInApiFailureCb );
}
	

/**
 * @description Send OTP via SMS
 * @param {Number} number
 * @memberof ButtonSubmit
 */
onPressSendOtp = (number, additionalProps) =>{
  console.log('xxx onPressSendOtp :: number, additionalProps', number, additionalProps);
  this.modalDismiss();
  const data = {
    action: 'getStartSendOtp',
    controller: 'account',
    otp_msisdn : number,
    otp_data: additionalProps.otp_data
  };

  let _this = this;
  const sentOtpSuccessCb = function (response) {
    console.log('xxx sentOtpSuccessCb', response);
    _this.modalDismiss();
    _this.authGetStartInApiSuccessCb.apply(_this, arguments);
  };

  this.props.authGetStartInApi(data, sentOtpSuccessCb, this.authGetStartInApiFailureCb );
}
	
/**
 * @description Verify OTP value with backend
 * @param {Number} OTPValue
 * @param {Number} otpRef
 * @param {Number} mobileNo
 * @memberof ButtonSubmit
 */
onPressVerifyOneTimePwd = (OTPValue, otpRef, mobileNo) =>{
  console.log('xxx onPressVerifyOneTimePwd :: OTPValue, otpRef, mobileNo =>', OTPValue, otpRef, mobileNo);
  const data = {
    action: 'loginOTP',
    controller: 'account',
    otp_msisdn : mobileNo,
    otp_pin: OTPValue,
    ref_id: otpRef

  };

  let _this = this;
  const verifyOneTimePwdSuccessCb = function (response) {
    console.log('xxx verifyOneTimePwdSuccessCb', response);
    _this.modalDismiss();
    _this.authGetStartInApiSuccessCb.apply(_this, arguments);
  };

	
  this.props.authGetStartInApi(data, verifyOneTimePwdSuccessCb, this.authGetStartInApiFailureCb );
}

/**
 * @description Show device change alert
 * @param {Object} additionalProps
 * @memberof ButtonSubmit
 */
onPressDeviceChange = (additionalProps) =>{
  console.log('xxx onPressDeviceChange', additionalProps);
  const data = {
    action: 'deviceChangeConfirmation',
    controller: 'account',
    msisdn: additionalProps.msisdn,
  };

  let _this = this;
  const deviceChangeSuccessCb = function (response) {
    console.log('xxx deviceChangeSuccessCb', response);
    let deviceChange = true;
    _this.modalDismiss();
    _this.actionLogin(deviceChange);
  };

  this.props.authGetStartInApi(data, deviceChangeSuccessCb, this.authGetStartInApiFailureCb);
}

/**
 * @description Check & request device phone permission to access IMEI and other data
 * @param {Object} additionalProps
 * @memberof ButtonSubmit
 */
checkPermissionReadPhoneState = async () => {
  console.log('checkPermissionReadPhoneState');
  try {
    const checkPermission = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE);
    console.log('checkPermissionReadPhoneState :: Permissions ', checkPermission);
    if (checkPermission) {
      console.log('checkPermissionReadPhoneState :: Permissions - ENABLED');
      return true;   
    } else {
      console.log('checkPermissionReadPhoneState :: Permissions - DENIED');
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('checkPermissionReadPhoneState :: Permissions REQUEST - GRANTED');
        return true;
      } else {
        console.log('checkPermissionReadPhoneState :: Permissions REQUEST - DENIED');
        return false;
      }
    }
  } catch (err) {
    console.log('checkPermissionReadPhoneState :: error' ,err);
    console.log('checkPermissionReadPhoneState :: Permissions REQUEST - EXCEPTION');
    return false;   
  }
}


/**
 * @description Get in start API call Success Callback
 * @param {Object} apiResponse
 * @memberof ButtonSubmit
 */
authGetStartInApiSuccessCb = (apiResponse) => {
  let data = apiResponse.data;
  let action = data.action;
  console.log('xxx authGetStartInApiSuccessCb :: data ', data);
  console.log('xxx authGetStartInApiSuccessCb :: action ', action);
  this.showActionScreen(action, data);
}

/**
 * @description Get in start API call Failure Callback
 * @param {Object} errorData
 * @memberof ButtonSubmit
 */
authGetStartInApiFailureCb = (errorData) => {
  console.log('xxx authGetStartInApiFailureCb :: errorData', errorData);
  let error ;
  if (!_.isNil(errorData.data)) {
    error = MessageUtils.getFormatedErrorMessage(errorData.data);
  } else {
    error = MessageUtils.getExceptionMessage(errorData);
  }
  console.log('xxx authGetStartInApiFailureCb :: error ', error);
  let modalHeightRatio = 0.35;
  this.showActionScreen('ALERT_FAILURE', { error, modalHeightRatio });
}

render() {
  let loadingIndicator;
  let apiCallingState = this.state.loginBtnTap || this.state.isLoading || this.props.auth_api_loading;

  if (apiCallingState) {
    loadingIndicator = (<ActIndicator animating={true}/>);
  } else {
    loadingIndicator = true;
  }
  return (
    <View style={styles.container}>
      {loadingIndicator}
      <TouchableOpacity
        style={styles.button}
        disabled={apiCallingState}
        accessibilityLabel={'button_login'} 
        testID={'button_login'}
        onPress={()=> this.deBouncedCheckUpdateAndLogin()}>
        <Text style={styles.text}>{this.state.locals.loginButtonText}</Text>
      </TouchableOpacity>
    </View>

  );
}
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: authz :: ButtonSubmit  => auth ', state.auth);
  //console.log('****** REDUX STATE :: authz :: ButtonSubmit  => auth ', JSON.stringify(state.auth));
  return {
    pin: state.auth.pin,
    auth_api_loading: state.auth.auth_api_loading,
    Language: state.lang.current_lang
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: MARGIN,
    borderRadius: BORDER_RADIUS,
    zIndex: 100,
    width: DEVICE_WIDTH - MARGIN
  },

  text: {
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontWeight: '600'
  }
});

export default connect(mapStateToProps, actions)(ButtonSubmit);