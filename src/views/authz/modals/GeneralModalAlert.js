/*
 * File: GeneralModalAlert.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 13th June 2018 2:52:00 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 8:54:35 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  Linking,
  StyleSheet
} from 'react-native';
import Button from '@Components/others/Button';
import Colors from '../../../config/colors';
const { width, height } = Dimensions.get('window');

export default class GeneralModalAlert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { 
      description = '', 
      modalHeightRatio,
      disabled= false,
      hideTopImageView= false,   
      customDescriptionTextView = false,   
      primaryPress,
      primaryText,
      secondaryPress,
      secondaryText,
      additionalProps,
      descriptionTextStyle = {}, 
      primaryButtonStyle = {}, 
      secondaryButtonStyle = {} ,
    } = this.props;

    let modalContainerCustomStyle = { height: height * modalHeightRatio };

    const TopImageView = () => {
      if (!hideTopImageView) {
        return (<View style={styles.topContainer} >
          <View style={styles.imageContainerStyle}>
            <Image
              resizeMode="contain"
              resizeMethod="scale"
              style={styles.titleImageStyle}
              source={this.props.icon}
            />
          </View>
        </View>);
      } else {
        return true;
      }   
    };

    const ContactDialogTextView  = () => {
      let descriptionTextStyle =  {
        fontSize: 18
      };
    
      let callNumberTextStyle =  {
        fontSize: 18,
        color: Colors.colorBlue,
        marginLeft: 5,
        textDecorationLine: 'underline',
      };

      let viewStyle =  {
        flex: 1,
        flexDirection: 'row'
      };
      let mainViewStyle =  {
        flex: 1,
      };

      let message_data = this.props.additionalProps.message_data;
    
      return (
        <View style={mainViewStyle}>
          <View style={viewStyle}>
            <View><Text style={descriptionTextStyle}>{message_data.left}</Text></View>
            <View><Text style={callNumberTextStyle} onPress ={() => Linking.openURL('tel:'+message_data.contact_number)}>{message_data.contact_number}</Text></View>      
          </View>
          <View><Text style={descriptionTextStyle}>{message_data.right}</Text></View>
        </View>);
      
    };

    const DescriptionTextView = () => {
      if (customDescriptionTextView) {
        return ContactDialogTextView();
      } else {
        return (<Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{description}</Text>);
      }   
    };

    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          {/*Top image view*/}
          {TopImageView()}
          <View style={styles.descriptionContainer}>
            {/*Description view*/}
            {DescriptionTextView()}
          </View>
          <View style={styles.buttonContainer}>
            {secondaryText ? <Button children={secondaryText}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={() =>secondaryPress(additionalProps)}
            /> : null}
            <Button children={primaryText}
              childStyle={[styles.buttonStyle, primaryButtonStyle]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={disabled}
              onPress={() =>primaryPress(additionalProps)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
   
  },
  modalContainer: {
    height: height * 0.5,
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
  },

  topContainer: {
    flex: 1.2,
    paddingTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    paddingTop: 30,
  },
  buttonContainer: {
    flex: 0.6,
    flexDirection: 'row',
    paddingTop: 18,
    paddingBottom: 15,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
   
  },
  imageContainerStyle: {
    flex: 1,
    margin : 10,
    alignItems: 'center'
  },
  
  titleImageStyle: {
    width: 56,
    height: 56
  },

  descriptionTextStyle: {
    fontSize: 18,
    color: Colors.colorLightBlack,
  },

  leftButtonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: 20,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingRight: 10,
    padding: 5
  },

  buttonStyle: {
    color: Colors.colorGreen,
    fontSize: 20,
    fontWeight:'500'
  }
});
