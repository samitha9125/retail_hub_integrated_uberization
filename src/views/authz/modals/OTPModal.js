
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Colors from '../../../config/colors';
import Analytics from '../../../utills/Analytics';
import Button from '@Components/others/Button';
import Utills from '../../../utills/Utills';
import SmsRetriever  from 'react-native-sms-retriever';
import _ from 'lodash';

const Utill = new Utills();

class OTPModal extends React.Component {
  subscription;
  constructor(props) {
    super(props);
    this.state = {
      locals: {
        OTP_text: 'PIN is sent to ',
        Cancel: 'CANCEL',
        Confirm: 'CONFIRM',
        lblResendPIN: 'RESEND PIN',
        lblDintReceivePIN: 'Didn\'t Receive PIN?',
        lblEnterPIN: 'Enter PIN'
      },
      OTPValue: ''
    };
  }

  componentDidMount() {
    this._onSmsListenerStart();
  }

  componentWillUnmount() {
    this._onSmsListenerStop();
  }

  _onPhoneNumberPressed = async () => {
    console.log('_onPhoneNumberPressed');
    try {
      const phoneNumber = await SmsRetriever.requestPhoneNumber();
      console.log('_onPhoneNumberPressed :: phoneNumber ', phoneNumber);
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  };

  _onSmsListenerStop = async () => {
    console.log('_onSmsListenerStop');
    Analytics.logEvent('dsa_stop_sms_listener');
    SmsRetriever.removeSmsListener();
  }

  _onSmsListenerStart = async () => {
    console.log('_onSmsListenerStart');
    try {
      const registered = await SmsRetriever.startSmsRetriever(); 
      if (registered) {
        SmsRetriever.addSmsListener(this._onReceiveSms);  
      } 
      console.log(`SMS Listener Registered: ${registered}`);
      console.log('dsa_sms_regex_prop', this.props.otpRexEx);
      Analytics.logEvent('dsa_sms_regex_prop', { regex_prop: this.props.otpRexEx });
      Analytics.logEvent('dsa_start_sms_listener');
    } catch (error) {
      console.log(`SMS Listener Error: ${JSON.stringify(error)}`);
      Analytics.logEvent('dsa_err_sms_listener', { error: error });
    }
  };

  _onReceiveSms = (event) => {
    console.log('_onReceiveSms');
    try { 
      let sms_message = !_.isNil(event) ? event.message : '';
      Analytics.logEvent('dsa_sms_receive', { message: sms_message });
      console.log(`_onReceiveSms: `, sms_message);
      SmsRetriever.removeSmsListener();
      let verificationCodeRegex = '';
      Analytics.logEvent('dsa_sms_regex', { regex: this.props.otpRexEx });
      if (!_.isNil(this.props.otpRexEx)){
        verificationCodeRegex = new RegExp(this.props.otpRexEx);
      }
      Analytics.logEvent('dsa_sms_regex', { regex_after: verificationCodeRegex });
    
      if (verificationCodeRegex.test(sms_message)) {
        let verificationCode = sms_message.match(/[0-9]{4}/)[0];
        console.log('sms_message :', sms_message);
        console.log('sms_message.match(verificationCodeRegex)[1]:', sms_message.match(verificationCodeRegex));
        console.log('verificationCode:', verificationCode);
        this.setState({ OTPValue: verificationCode });
        Analytics.logEvent('dsa_sms_otp_pin', { otp_pin: verificationCode });
      } 
    } catch (error) {
      Analytics.logEvent('dsa_err_sms_receive', { error: error });
      console.log(`SMS Receive Error: ${JSON.stringify(error)}`);
    }
  };

  onBnPressDismiss = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  isValidOTP() {
    return this.state.OTPValue.length == 4 ? true : false;
  }

  render() {
    const {
      descriptionTextStyle = {},
      secondaryButtonStyle = {},
      onPressVerify,
      onPressResend,
      otpRef = '',
      mobileNo = '',
      additionalProps = {},
      isResendApiActive = false
    } = this.props;

    return (
      <View style={styles.modalStyle}>
        <View style={[styles.scrollViewContainer]}>
          <View style={styles.descriptionContainer}>
            <Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{this.state.locals.OTP_text}{Utill.fullNumberMask(mobileNo)}</Text>
          </View>
          <View style={styles.textInputContainer}>
            <View style={styles.textFieldTitle}>
              <Text textAlign="center" style={styles.textFieldTitleStyle}>{this.state.locals.lblEnterPIN}</Text>
            </View>
            <View style={styles.OTPFieldTitle}>
              <Text textAlign="center" style={styles.OTPFieldTextStyle}>{this.state.OTPValue}</Text>
            </View>
            <View style={styles.getInStart}>
              <View style={styles.getInStartTextContainer}>
                <Text style={styles.getInStartText}>{this.state.locals.lblDintReceivePIN}</Text>
              </View>
              <View style={styles.getInStartTextButtonContainer}>
                <Text
                  style={[styles.getInStartTextButton,  {
                    color: isResendApiActive ?
                      Colors.btnDisableTxtColor:
                      Colors.gettingStartedButtonTextColor 
                      
                  }]}
                  disabled={isResendApiActive}
                  onPress=
                    {() => onPressResend(mobileNo, otpRef, additionalProps)}> {this.state.locals.lblResendPIN} </Text>
              </View>
            </View>
          </View>

          <View style={styles.buttonContainer}>
            <Button children={this.state.locals.Cancel}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={this.onBnPressDismiss}
            />
            <Button children={this.state.locals.Confirm}
              childStyle={[styles.buttonStyle,
                {
                  color: this.isValidOTP() == true ?
                    Colors.gettingStartedButtonTextColor :
                    Colors.btnDisableTxtColor
                }]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={this.isValidOTP() == false}
              onPress={() => onPressVerify(this.state.OTPValue, otpRef, mobileNo)}
            />
          </View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  modalStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
    padding: 0
  },
  scrollViewContainer: {
    height: 250,
    backgroundColor: Colors.colorWhite,
    width: '90%',
  },
  descriptionContainer: {
    flex: 0.3,
    justifyContent: 'center',
    marginRight: 20,
    marginLeft: 20,
    marginTop: 10,
  },
  textInputContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 5,
    marginLeft: 20,
  },
  textFieldTitle: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 20,
    marginTop: 5
  },
  OTPFieldTitle: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 20,
    marginTop: -5,
    borderBottomWidth: 1,
    borderColor: Colors.borderColorGray

  },
  buttonContainer: {
    flex: 0.3,
    flexDirection: 'row',
    paddingBottom: 15,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  descriptionTextStyle: {
    fontSize: 18,
    color: Colors.black
  },
  textFieldTitleStyle: {
    fontSize: 18,
    color: Colors.black,
    fontWeight: '500'
  },
  OTPFieldTextStyle: {
    fontSize: 18,
    color: Colors.black,
    fontWeight: '500',
    marginLeft: 5
  },
  leftButtonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle: {
    paddingRight: 10,
    padding: 5
  },

  buttonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: '500'
  },
  getInStart: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 20,
  },
  getInStartTextContainer: {
    flex: 0.6,
  },
  getInStartTextButtonContainer: {
    flex: 0.4
  },
  getInStartText: {
    fontSize: 18,
    marginRight: 8,
    color: Colors.colorLightBlack
  },
  getInStartTextButton: {
    fontSize: 19,
    alignSelf: 'flex-end',
    fontWeight: '500',
    color: Colors.gettingStartedButtonTextColor
  }
});

export default OTPModal;
