/*
 * File: PinResetModal.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 11:26:42 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 8:54:59 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Button from '@Components/others/Button';
import Colors from '../../../config/colors';
import Utills from '../../../utills/Utills';
const { width, height } = Dimensions.get('window');

const Utill = new Utills();

export default class PinResetModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  
      pin_number: '',
      re_pin_number: '',     
    };

  }

  onInputTextChange_1 = (text) => {
    console.log('xxx onInputTextChange_1', text);
    this.setState({ pin_number : text });
  }

  onInputTextChange_2 = (text) => {
    console.log('xxx onInputTextChange_2', text);
    this.setState({ re_pin_number : text });
  }

  validatePinNumber = (pin, textInputErrorValue) => {
    console.log('xxx validatePinNumber', pin);
    return (pin == '' || Utill.validateDigit(pin) && pin.length == 4) ? '' : textInputErrorValue;
  }

  getButtonEnableStatus = () => {
    console.log('xxx getButtonEnableStatus');
    let pin1 = this.state.pin_number;
    let pin2 = this.state.re_pin_number;
    return (pin1 != '' && pin2 != '' && Utill.validateDigit(pin1) && Utill.validateDigit(pin2)) && pin1.length == 4 && pin2.length == 4;
  }

  render() {
    const {   
      primaryPress,
      additionalProps ={},
      titleText = '',
      description = '',
      textInputLabel_1 = '',
      textInputLabel_2 = '',
      textInputErrorValue = 'Invalid number',
      textInputMaxLength,
      keyboardType,
      secureTextEntry=true,
      hideTitleView = true,
      descriptionTextStyle = {}, 
      primaryButtonStyle = {}, 
      secondaryButtonStyle = {} ,
    
    } = this.props;

    let modalContainerCustomStyle = {};

    if (hideTitleView) {
      modalContainerCustomStyle = {
        height: height * 0.55,
      };
    }

    const TitleView = () => {
      if (!hideTitleView) {
        return (<View style={styles.titleContainer} >
          <View style={styles.imageContainerStyle}>
            <Image
              resizeMode="contain"
              resizeMethod="scale"
              style={styles.titleImageStyle}
              source={this.props.icon}
            />
          </View>
          <View style={styles.titleTextContainer}>
            <Text textAlign="center" style={styles.titleTextStyle}>{titleText}</Text>
          </View>
        </View>);
      } else {
        return true;
      }   
    };

    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          {TitleView()}
          <View style={styles.descriptionContainer}>
            <Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{description}</Text>
          </View>
          <View style={styles.textInputContainer}>
            <TextField
              label={textInputLabel_1}
              value={this.state.pin_number}
              secureTextEntry={secureTextEntry}
              maxLength={textInputMaxLength ? textInputMaxLength : 4}
              baseColor={Colors.colorGrey}
              keyboardType={keyboardType ? keyboardType : "numeric"}
              onChangeText={text => this.onInputTextChange_1(text)}
              error={this.validatePinNumber(this.state.pin_number, textInputErrorValue)}
            />
            <TextField             
              label={textInputLabel_2}
              value={this.state.re_pin_number}
              secureTextEntry={secureTextEntry}
              maxLength={textInputMaxLength ? textInputMaxLength : 4}
              baseColor={Colors.colorGrey}
              keyboardType={keyboardType ? keyboardType : "numeric"}
              onChangeText={text => this.onInputTextChange_2(text)}
              error={this.validatePinNumber(this.state.re_pin_number, textInputErrorValue)}
            />
          </View>
          <View style={styles.buttonContainer}>
            {this.props.secondaryText ? <Button children={this.props.secondaryText}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={primaryPress}
            /> : null}
            <Button children={this.props.primaryText}
              childStyle={[styles.buttonStyle, primaryButtonStyle, 
                { color: this.getButtonEnableStatus() ? 
                  Colors.gettingStartedButtonTextColor : 
                  Colors.btnDisableTxtColor }]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={!this.getButtonEnableStatus()}
              onPress={() => primaryPress(this.state.pin_number, this.state.re_pin_number, additionalProps)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
   
  },
  modalContainer: {
    height: height * 0.7,
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
  },

  titleContainer: {
    flex: 1,
    paddingTop: 18,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  descriptionContainer: {
    flex: 0.7,
    justifyContent: 'center',
    padding: 20,
    paddingTop: 25,
  },
  textInputContainer:{ 
    flex: 1,
    justifyContent: 'center', 
    marginTop: -25, 
    paddingBottom: 5,
    paddingHorizontal: 20,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 18,
    paddingBottom: 15,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
   
  },
  imageContainerStyle: {
    margin : 10,
    marginTop: 30,
    flex: 1,
    alignItems: 'center'
  },
  
  titleTextContainer:  {
    flex: 3,
    alignItems: 'flex-start',   
    marginTop: 30,
  },

  titleImageStyle: {
    width: 55,
    height: 55
  },

  titleTextStyle:  {
    fontSize: 20,
    color: Colors.colorBlack,

  },

  descriptionTextStyle: {
    fontSize: 16,
    color: Colors.colorLightBlack,
  },

  leftButtonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: 20,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingRight: 10,
    padding: 5
  },

  buttonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: 20,
    fontWeight:'500'
  }
});
