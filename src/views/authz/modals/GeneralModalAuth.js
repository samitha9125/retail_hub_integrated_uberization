/*
 * File: GeneralModalAuth.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 5th June 2018 5:03:04 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Tuesday, 26th June 2018 5:44:25 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, View, Text, Image, Dimensions } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Colors from '../../../config/colors';
import Button from '@Components/others/Button';
const { width, height } = Dimensions.get('window');
import Utills from '../../../utills/Utills';

const Utill = new Utills();

class GeneralModalAuth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile_number: ''
    };
  }

  onInputTextChange(text){
    this.setState({ mobile_number: text });
  }

  render() {
    const { 
      modalHeightRatio = 0.7, 
      descriptionTextStyle = {}, 
      secondaryButtonStyle = {} ,
      primaryButtonStyle = {},
      titleText = '',
      description = '',
      textInputLabel = '',
      textInputErrorValue = 'Invalid number',
      footerDescription ='',
      keyboardType,
      primaryPress,
      secondaryPress,
      needTextInput= false,
      needFooterDescription = true,
      needTwoButton = false,
      additionalProps ={},
    } = this.props;

    let modalContainerCustomStyle = { height: height * modalHeightRatio };
    let textInputValue = this.state.mobile_number;

    const TextInputView = () => {
      if (needTextInput) {
        return (<View style={styles.textInputContainer}>
          <TextField
            label={textInputLabel}
            value={this.state.mobile_number}
            maxLength={Utill.getMobileNumberLengthLimit(textInputValue)}
            baseColor={Colors.colorGrey}
            keyboardType={keyboardType ? keyboardType : "numeric"}
            onChangeText={text => this.onInputTextChange(text)}
            error={(textInputValue == '' || Utill.mobileNumberValidateDialog(textInputValue)) ? '' : textInputErrorValue }
          />
        </View>);
      } else {
        return true;
      }   
    };

    const FooterDescriptionView = () => {
      if (needFooterDescription) {
        return ( <View style={styles.footerDescriptionContainer}>
          <Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{footerDescription}</Text>
        </View>);
      } else {
        return true;
      }   
    };

    const ButtonView = () => {
      if (needTwoButton) {
        return (<View style={styles.buttonContainer}>
          {this.props.secondaryText ? <Button children={this.props.secondaryText}
            childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
            ContainerStyle={styles.leftButtonContainerStyle}
            onPress={secondaryPress}
          /> : null}
          <Button children={this.props.primaryText}
            childStyle={[styles.leftButtonStyle, primaryButtonStyle]}
            ContainerStyle={styles.rightButtonContainerStyle}
            onPress={()=>primaryPress()}
          />
        </View> );
      } else {
        return (
          <View style={styles.buttonContainer}>
            <Button children={this.props.primaryText}
              childStyle={[
                styles.buttonStyle, 
                { color: Utill.mobileNumberValidateDialog(this.state.mobile_number) == true ? 
                  Colors.gettingStartedButtonTextColor : 
                  Colors.btnDisableTxtColor } ]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={Utill.mobileNumberValidateDialog(this.state.mobile_number) == false}
              onPress={()=>primaryPress(this.state.mobile_number, additionalProps)}
            />
          </View>);
      }   
    };
     
    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          <View style={styles.titleContainer} >
            <View style={styles.imageContainerStyle}>
              <Image
                resizeMode="contain"
                resizeMethod="scale"
                style={styles.titleImageStyle}
                source={this.props.icon}
              />
            </View>
            <View style={styles.titleTextContainer}>
              <Text textAlign="center" style={styles.titleTextStyle}>{titleText}</Text>
            </View>
          </View>
          <View style={styles.descriptionContainer}>
            <Text textAlign="center" style={[styles.descriptionTextStyle, descriptionTextStyle]}>{description}</Text>
          </View>
          {/*Text input view*/}
          {TextInputView()}  
          {/*Additional description view*/}
          {FooterDescriptionView()}
          {/*Button view*/}
          {ButtonView()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
  },
  modalContainer: {
    height: height * 0.7,
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
  },
  titleContainer: {
    flex: 1,
    paddingTop: 18,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',

  },
  descriptionContainer: {
    flex: 1.3,
    justifyContent: 'center',
    paddingBottom: 10,
    paddingHorizontal: 20,
    paddingTop: 25,
  },
  textInputContainer:{ 
    flex: 0.7,
    justifyContent: 'center', 
    marginTop: -10, 
    paddingBottom: 5,
    paddingHorizontal: 20,
  },

  footerDescriptionContainer: { 
    flex: 1,
    justifyContent: 'center', 
    paddingBottom: 5,
    paddingHorizontal: 20,
    paddingTop: 20,
  },

  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 5,
    paddingBottom: 15,
    paddingRight: 20,
   
  },
  imageContainerStyle: {
    margin : 10,
    marginTop: 30,
    flex: 1,
    alignItems: 'center'
  },
  
  titleTextContainer:  {
    flex: 3,
    alignItems: 'flex-start',   
    marginTop: 30,
  },

  titleImageStyle: {
    width: 55,
    height: 55
  },

  titleTextStyle:  {
    fontSize: 20,
    fontWeight:'400',
    color: Colors.colorBlack,

  },

  descriptionTextStyle: {
    fontSize: 18,
    paddingTop: 15,
    color: Colors.colorBlack,
  },

  leftButtonStyle: {
    color: Colors.gettingStartedButtonTextColor,
    fontSize: 20,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingRight: 10,
    padding: 5
  },

  buttonStyle: {
    fontSize: 20,
    fontWeight:'500'
  }
});

export default GeneralModalAuth;
