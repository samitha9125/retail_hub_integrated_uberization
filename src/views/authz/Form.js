import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import Colors from '../../config/colors';
import UserInput from './UserInput';
import passwordImg from '../../../images/authz_images/password.png';
import eyeImg from '../../../images/authz_images/eye_black.png';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      locals: {
        placeholderText: 'Enter PIN'
      }
    };
  }
  showPass = ()=> {
    if (this.state.press === false) {
      this.setState({ showPass: false, press: true });
    } else {
      this.setState({ showPass: true, press: false });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <UserInput
          source={passwordImg}
          secureTextEntry={this.state.showPass}
          placeholder={this.state.locals.placeholderText}
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}/>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.btnEye}
          onPress={this.showPass}>
          <Image source={eyeImg} style={styles.iconEye}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 5,
    marginBottom: -15
  },

  btnEye: {
    position: 'absolute',
    flex: 1,
    alignSelf: 'center',
    top: 6,
    right: 28
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: Colors.showPwtintColor
  }
});

export default Form;
