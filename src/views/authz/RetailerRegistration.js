/*
 * File: RetailerRegistration.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 11:26:42 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 18th June 2018 5:31:37 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  TextInput,
  Alert
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import strings from '../../Language/settings';
import Color from '../../config/colors';

const Utill = new Utills();

class RetailerRegistration extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {  
      name: '',
      address: '',
      contact_no: '',
      nic_brc_no: '',
      locals: {
        screenTitle: 'RETAILER',
        continue: 'SUBMIT',
        name: 'Name',
        address: 'Address',
        contact_no: 'Contact no',
        nic_brc_no: 'NIC/BRC no',
        please_fill_all_value: 'Please fill all values',
        please_enter_valid_mobile_number: 'Please enter valid mobile number',
        please_enter_valid_name: 'Please enter valid name',
        please_enter_valid_address: 'Please enter valid address',
        please_enter_valid_id: 'Please enter valid ID',
        this_field_is_required : 'field is required',
        backMessage: 'Do you want to cancel the operation & go back?',
      }

    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx onPressOk');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo( { title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.LoginScreen' } );
  }

  /**
   * @description On change name text function
   * @param {String} text
   * @memberof RetailerRegistration
   */
  onChangeTextName = (text) => {
    console.log('xxx onChangeTextName', text);
    this.setState({ name : text });

  }

  /**
   * @description On change address text function
   * @param {String} text
   * @memberof RetailerRegistration
   */
  onChangeTextAddress = (text) => {
    console.log('xxx onChangeTextAddress', text);
    this.setState({ address : text });

  }

  /**
   * @description On change contact no text function
   * @param {String} text
   * @memberof RetailerRegistration
   */
  onChangeTextContactNo = (text) => {
    console.log('xxx onChangeTextContactNo', text);
    this.setState({ contact_no : text });

  }

  /**
   * @description On change id text function
   * @param {String} text
   * @memberof RetailerRegistration
   */
  onChangeTextId = (text) => {
    console.log('xxx onChangeTextId', text);
    this.setState({ nic_brc_no : text });

  }  

  /**
   * @description Submit data for backend API
   * @memberof RetailerRegistration
   */
  submitData = () => {
    console.log('xxx submitData');
    if (this.state.name == '' ) {
      Utill.showAlertMsg(this.state.locals.name + ' '+ this.state.locals.this_field_is_required);
      return;
    }

    if (this.state.address == '' ) {
      Utill.showAlertMsg(this.state.locals.address + ' '+ this.state.locals.this_field_is_required);
      return;
    }

    if (this.state.contact_no == '' ) {
      Utill.showAlertMsg(this.state.locals.contact_no + ' '+ this.state.locals.this_field_is_required);
      return;
    }

    if (this.state.nic_brc_no == '' ) {
      Utill.showAlertMsg(this.state.locals.nic_brc_no + ' '+ this.state.locals.this_field_is_required);
      return;
    }
  
    if (!Utill.mobileNumberValidateAny(this.state.contact_no)) {
      Utill.showAlertMsg(this.state.locals.please_enter_valid_mobile_number);
      return;
    }

    if (!Utill.nameValidation(this.state.name)) {
      Utill.showAlertMsg(this.state.locals.please_enter_valid_name);
      return;
    }

    if (!Utill.addressValidation(this.state.address)) {
      Utill.showAlertMsg(this.state.locals.please_enter_valid_address);
      return;
    }

    if (!Utill.alphaNumericValidation(this.state.nic_brc_no)) {
      Utill.showAlertMsg(this.state.locals.please_enter_valid_id);
      return;
    }

    const data = {
      action: 'newRetailerInfo',
      controller: 'account',
      current_screen: 'REGISTER_RETAILER_S4',
      msisdn: this.props.verified_otp_number,
      name: this.state.name,
      address: this.state.address,
      contactNo: this.state.contact_no,
      nic: this.state.nic_brc_no,
    };

    let self = this;
    const submitDataSuccessCb = function (response) {
      console.log('xxx submitDataSuccessCb', response);
      self.okHandler();
      self.props.authGetStartInApiSuccessCb.apply(this, arguments);
    };

    this.props.authGetStartInApi(data, submitDataSuccessCb, this.props.authGetStartInApiFailureCb );
  }

  /**
   * @description Get form value field status
   * @memberof RetailerRegistration
   */
  getFormFillStatus = () => {
    console.log('xxx getFormFillStatus');
    return this.state.name !== '' 
    && this.state.address !== '' 
    && this.state.contact_no !== '' 
    && this.state.nic_brc_no !== ''
    && Utill.nameValidation(this.state.name)
    && Utill.addressValidation(this.state.address)
    && Utill.mobileNumberValidateAny(this.state.contact_no)
    && Utill.alphaNumericValidation(this.state.nic_brc_no);
  }

  render() {
    let submitButtonColor;
    if (this.getFormFillStatus()) {
      console.log('xxx enable SubmitButtonColor :: true');
      submitButtonColor = {
        backgroundColor: Colors.btnActive
      };
    } else {
      console.log('xxx enable SubmitButtonColor :: false');
      submitButtonColor = {
        backgroundColor: Colors.btnDisable
      };
    }

    let loadingIndicator;

    if (this.props.auth_api_loading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/>
        {/* There may be issue occur in this place - Aware evil space issue */}
        {loadingIndicator}
        <KeyboardAwareScrollView innerRef={ref => {this.scroll = ref;}} alwaysBounceHorizontal ={true} contentInsetAdjustmentBehavior = "always" style={styles.scrollViewContainer}>
          <View style={styles.containerInputFields}>
            <TextInputField
              index={1}
              keyboardType={'default'}
              label={this.state.locals.name}
              placeholder={''}          
              onChangeText={(text) => this.onChangeTextName(text)}/>
            <TextInputField
              index={2}
              keyboardType={'default'}
              label={this.state.locals.address}
              placeholder={''}   
              onChangeText={(text) => this.onChangeTextAddress(text)}/>
            <TextInputField
              index={3}
              keyboardType={'numeric'}
              label={this.state.locals.contact_no}
              maxLength={Utill.getMobileNumberLengthLimit(this.state.contact_no)}
              placeholder={''}
              onChangeText={(text) => this.onChangeTextContactNo(text)}/>
            <TextInputField
              index={4}
              keyboardType={'default'}
              label={this.state.locals.nic_brc_no}
              placeholder={''}
              onChangeText={(text) => this.onChangeTextId(text)}/>       
          </View>      
        </KeyboardAwareScrollView>
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtonText={this.state.locals.continue}
            onClickOk={() => this.submitData()}
            activateColor={submitButtonColor}/>
        </View>
      </View>
    );
  }
}

const TextInputField = ({ index, label, placeholder, keyboardType, multiline, maxLength, onChangeText }) => (
  <View style={styles.TextInputFieldContainer}>
    <View style={styles.inputFieldLabelContainer}>
      <Text style={styles.inputFieldLabelText}>{label}</Text>
    </View>
    <View style={styles.textInputContainer}>
      <TextInput
        index={index}
        style={styles.textInput}
        placeholder={placeholder}
        multiline={multiline}
        maxLength={maxLength}
        keyboardType={keyboardType}
        underlineColorAndroid ={Colors.texInputUnderlineColor}
        onChangeText={onChangeText}/>
    </View>
  </View>
);

const ElementFooter = ({ okButtonText, onClickOk, activateColor }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={[styles.buttonContainer, activateColor]}
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: authz :: RetailerRegistration  => auth ', state.auth);
  const Language = state.lang.current_lang;
  const auth_api_loading =  state.auth.auth_api_loading;

  return {
    Language,
    auth_api_loading
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.appBackgroundColor
  },

  scrollViewContainer: {
    flex: 1,
  },
  containerInputFields: {
    flex: 5,
  },
  containerBottom: {
    justifyContent : 'flex-end',
    alignItems : 'center' 
  },

  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },

  //Input fields
  TextInputFieldContainer: {
    paddingTop: 10,
    marginTop: 5,
    height: 90,
  },

  inputFieldLabelContainer: {
    flex: 0.8,
  },
  textInputContainer: {
    flex: 1.2,
  },

  inputFieldLabelText: {
    textAlign: 'left',
    color: Colors.colorBlack,
    fontSize: 18,
    fontWeight: '400',
    marginLeft: 15,
    marginRight: 20,
    padding: 5,
    
  },

  textInput: {
    height: 40,
    padding: 5,
    marginTop: 12,
    marginBottom: 8,
    marginLeft: 15,
    marginRight: 20
  },


});

export default connect(mapStateToProps, actions)(RetailerRegistration);
