import React, { Component } from 'react';
import { KeyboardAvoidingView, ScrollView, Dimensions, Modal, View, Text, Image } from 'react-native';
import Utills from '../../utills/Utills';
import Button from '@Components/others/Button';
import colors from '../../config/colors';
const { width } = Dimensions.get('window');
const Utill = new Utills();

export default class GenaralModel extends Component {
    constructor(props) {
        super(props);


    }
    render() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                    visible={this.props.modalVisible}
                    hardwareAccelerated={true}
                    onRequestClose={
                        this.props.hideModel
                    }>
                    {this.props.parentView ? this.props.parentView : <KeyboardAvoidingView style={{ flex: 1, paddingVertical: width / 4, paddingHorizontal: 20 }}>
                        < ScrollView >
                            <View style={{ backgroundColor: '#FFF' }}>
                                {this.props.childView ? this.props.childView :
                                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                                        <View style={{ marginTop: 30 }}>
                                            {this.props.icon ? <Image
                                                resizeMode="contain"
                                                resizeMethod="scale"
                                                style={{ width: 50, height: 50 }}
                                                source={this.props.icon}
                                            /> : null}
                                        </View>
                                        <View style={{ paddingTop: 20, justifyContent: 'center', padding: 10 }}>
                                            <Text textAlign="center" style={{}}>{this.props.description}</Text>
                                        </View>

                                    </View>}
                                <View style={{ flex: 1, paddingTop: 18, paddingBottom: 15, paddingRight: 20, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row' }}>
                                    {this.props.secondryText ? <Button children={this.props.secondryText}
                                        childStyle={{ color: colors.btnActive, fontSize: 18 }}
                                        ContainerStyle={{ paddingRight: 40 }}
                                        onPress={this.props.secondoryPress}
                                    /> : null}
                                    <Button children={this.props.primaryText}
                                        childStyle={{ color: this.props.ButtonColor ? this.props.ButtonColor : colors.btnActive, fontSize: 18 }}
                                        ContainerStyle={{}}
                                        disabled={this.props.disabled}
                                        onPress={this.props.primaryPress}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>}
                </Modal>

            </View >
        );
    }
}


