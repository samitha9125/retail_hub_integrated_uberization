/*
 * File: DeviceCompatibleModal.js
 * This is the DeviceCompatibleModel Model Reusable Component.
 * Project: Dialog Sales App
 * File Created: Monday, 2nd July 2018 12:01:38 am
 * Author: Arafath Misree (arafath@omobio.net)
 * -----
 * Last Modified: Monday, 9th July 2018 6:45:58 pm
 * Modified By: Arafath Misree (arafath@omobio.net)
 *              Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { KeyboardAvoidingView, ScrollView, Dimensions, Modal, View, Text, Image, StyleSheet } from 'react-native';
import Utills from '../../utills/Utills';
import Images from '@Config/images';
import Button from '@Components/others/Button';
import colors from '../../config/colors';
import strings from '../../Language/general';
const { width } = Dimensions.get('window');
import { TextField } from 'react-native-material-textfield';

const Utill = new Utills();

export default class DeviceCompatibleModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locals: {
        enter_contact_number_for_assistance: 'Enter contact number for assistance',
        invalid_number: 'Invalid number'
      },
    };
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          style={styles.modalContainer}
          visible={this.props.modalVisible}
          hardwareAccelerated={true}
          onRequestClose={() => {
          }}>
          <KeyboardAvoidingView style={styles.KeyboardAvoidingViewStyle} keyboardShouldPersistTaps="always">
            <ScrollView>
              <View style={styles.scrollViewContainer}>
                <View style={styles.topContainer} >
                  <View style={styles.imageContainerStyle}>
                    <Image
                      resizeMode="contain"
                      resizeMethod="scale"
                      style={styles.imageStyle}
                      source={Images.icons.AttentionAlert}
                    />
                  </View>
                  <View style={styles.descriptionContainerStyle}>
                    <Text textAlign="center" style={styles.descriptionTextStyle}>{this.props.deviceCompatibleInfo.description}</Text>
                  </View>
                  {this.props.deviceCompatibleInfo.LimitedFunctions ?
                    (<View style={styles.limitedFunctionsListContainerStyle}>
                      {this.props.deviceCompatibleInfo.LimitedFunctions.map((item, i) => (
                        <Text key={i} style={styles.limitedFunctionsListTextStyle}>{item.function}</Text>
                      ))}
                    </View>) : null
                  }
                  <View style={styles.openUlrContainerStyle}>
                    <Text style={styles.openUlrTextStyle} onPress={() => this.props.openUrl()}>{strings.viewSupportedSpec}</Text>
                  </View>
                </View>
                {this.props.deviceCompatibleInfo.state == 'pending' ? null : (
                  <View style={styles.textInputContainerStyle}>
                    <TextField
                      label={this.state.locals.enter_contact_number_for_assistance}
                      value={this.props.msisdn}
                      maxLength={Utill.getMobileNumberLengthLimit(this.props.msisdn)}
                      baseColor={colors.colorGrey}
                      keyboardType={"numeric"}
                      onChangeText={text => this.props.onTextChange(text)}
                      error={(this.props.msisdn == '' || Utill.mobileNumValidate(this.props.msisdn)) ? '' : this.state.locals.invalid_number}
                    />
                  </View>
                )}
                <View style={styles.bottomContainerStyle}>
                  <Button children={this.props.locals.ok}
                    childStyle={[styles.buttonTextStyle, Utill.getButtonTextColor(Utill.mobileNumValidate(this.props.msisdn) || (this.props.deviceCompatibleInfo.state == 'pending'))]}
                    ContainerStyle={styles.buttonContainerStyle}
                    disabled={this.props.disabled}
                    onPress={this.props.onOkPress}
                  />
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  KeyboardAvoidingViewStyle: {
    flex: 1,
    paddingBottom: 2,
    paddingVertical: width / 4,
    paddingHorizontal: 15,
    backgroundColor: colors.modalOverlayColorLow
  },
  scrollViewContainer: {
    backgroundColor: colors.colorWhite,
    padding: 5
  },
  topContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  imageContainerStyle: {
    marginTop: 20
  },
  imageStyle: {
    width: 50,
    height: 50
  },

  descriptionContainerStyle: {
    paddingTop: 20,
    justifyContent: 'center',
    padding: 12,
    alignSelf: 'flex-start',
  },

  descriptionTextStyle: {
    color: colors.colorLightBlack,
    fontSize: 17,
    textAlign: 'left'
  },
  openUlrContainerStyle: {
    padding: 12,
    alignSelf: 'flex-start',
  },

  openUlrTextStyle: {
    textDecorationLine: 'underline',
    color: colors.colorBlue,
    textAlign: 'left',
    fontSize: 16,
    
  },

  limitedFunctionsListContainerStyle: {
    paddingTop: 10,
    alignSelf: 'flex-start',
  },

  limitedFunctionsListTextStyle: {
    fontSize: 17,
    color: colors.colorBlack,
    fontWeight: 'bold',
    marginLeft: 45
  },
  bottomContainerStyle: {
    flex: 1,
    paddingTop: 18,
    paddingBottom: 10,
    paddingRight: 20,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  buttonTextStyle: {
    color: colors.gettingStartedButtonTextColor,
    fontSize: 18,
    fontWeight: 'bold'
  },
  buttonContainerStyle: {
    flex: 1
  },
  textInputContainerStyle: {
    paddingTop: 5,
    justifyContent: 'center',
    paddingHorizontal: 12
  }

});


