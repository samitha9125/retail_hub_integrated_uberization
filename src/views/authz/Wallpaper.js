import React, { } from 'react';
import { View, ImageBackground, StyleSheet } from 'react-native';
import bgSrc from '../../../images/authz_images/wallpaper.png';

class Wallpaper extends React.Component {
  render() {
    return (
      <View style={styles.containerWrapper}>
        <ImageBackground style={styles.picture} source={bgSrc}>
          {this.props.children}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerWrapper: {
    flex: 1,
    zIndex: -1
  },
  picture: {
    flex: 1,
    width: null,
    height: null,
  }
});

export default Wallpaper;
