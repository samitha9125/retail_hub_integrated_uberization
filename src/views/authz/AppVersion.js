/*
 * File: AppVersion.js
 * Project: Dialog Sales App
 * File Created: Monday, 16th April 2018 10:21:38 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 18th June 2018 5:44:01 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Global from './../../config/globalConfig';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

class AppVersion extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text 
          style={styles.versionText}
          accessibilityLabel={'text_view_app_version'} 
          testID={'text_view_app_version'}
        >
          Version - {Global.appVersion}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 0
  },
  versionText: {
    fontSize: Styles.appVersionFontSize,
    color: Colors.colorBlack
  }
});

export default AppVersion;
