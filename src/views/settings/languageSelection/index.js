import React from 'react';
import { StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SectionItems from './SectionItems';
import Colors from '../../../config/colors';

class LanguageSelectionMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userLogged: true,
      userData: ''
    };
  }

  render() {
    const { navigator, initialSetup, languageSelectionCb } = this.props;
    return (
      <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
        <View style={styles.container}>
          <SectionItems navigator={navigator} initialSetup={initialSetup} languageSelectionCb={languageSelectionCb} />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 15
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  }
});

export default LanguageSelectionMain;
