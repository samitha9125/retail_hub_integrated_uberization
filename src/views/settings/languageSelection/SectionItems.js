import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import RadioForm, { RadioButton, RadioButtonLabel, RadioButtonInput } from 'react-native-simple-radio-button';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../../Language/settings';
import Utills from '../../../utills/Utills';
import { LocalStorageUtils } from '../../../utills';


const Utill = new Utills();

class SectionItems extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    Utill.setLang(this.props.language);
    this.state = {
      userLogged: true,
      userData: '',
      selctLanguageTxt: strings.selectLanguage,
      buttonText: strings.btnTxt,
      languageSelectionValue: 'en',
      langValue: this.props.langValue,
      languageSelectionData: [
        {
          label: 'English',
          value: 'en'
        }, {
          label: 'සිංහල',
          value: 'si'
        }, {
          label: 'தமிழ்',
          value: 'ta'
        }
      ]
    };
  }


  componentWillMount(){
    this.getLanguage();
  }

  componentWillUnmount =()=>{
    if (this.props.languageSelectionCb){
      this.props.languageSelectionCb();
    }
  }

  onRadioButtonPress = (i, obj) => {
    this.setState({ languageSelectionValue: obj.value, langValue: i });
  }

  getLanguage = async () => {
    console.log('getLanguage');
    try {
      console.log("getLanguage :: languageSelectionValue: ", this.state.languageSelectionValue);
      console.log("getLanguage :: langValue: ", this.state.langValue);
      let currentLanguage = LocalStorageUtils.getPersistenceLanguage();

      for (let index = 0; index < this.state.languageSelectionData.length; index++) {
        const element = this.state.languageSelectionData[index];
        if (element.value == currentLanguage) { 
          this.setState({ langValue: index });
          return;
        }
      }
    } catch (error) {
      this.setState({ langValue: 0 });
      console.log('getLanguage :: error', error);
    }
  }

  changeLang = async() => {
    this.props.setLanguage(this.state.languageSelectionValue, this.state.langValue);
    await LocalStorageUtils.setLanguage(this.state.languageSelectionValue, this.state.langValue);
    await LocalStorageUtils.savePersistenceLanguage(this.state.languageSelectionValue);
    let data = {
      lang: this.state.languageSelectionValue
    };

    Utill.apiRequestPost2('setUserLanguage', 'account', 'ccapp', data, ()=>{
      if (this.props.initialSetup){
        console.log('pop language selection');
        this.props.navigator.dismissModal({ animated: true, animationType: 'slide-down' });
      } else {
        console.log('Inside drawer');
        this.props.navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.HomeTileScreen' });
      }
    });
  }

  dismissLightBox = () => {
    this.props.navigator.dismissLightBox();
  };
    
  renderRadioButton = (i, obj) => {
    console.log("renderRadioButton i: "+i + "\n obj: "+ JSON.stringify(obj));
    return (
      <RadioButton 
        labelHorizontal 
        style={styles.radioButtonStyle} 
        key={i}>
        <RadioButtonInput
          obj={obj}
          index={i}
          isSelected={this.state.langValue === i}
          onPress={() => this.onRadioButtonPress(i, obj)}
          buttonInnerColor={this.state.langValue === i
            ? Colors.radioBtnSelected
            : Colors.radioBtnDefault}
          buttonOuterColor={this.state.langValue === i
            ? Colors.radioBtnSelected
            : Colors.radioBtnDefault}
          buttonSize={14}
          buttonStyle={{}}
          buttonWrapStyle={styles.buttonWrapStyle}
          accessibilityLabel={`button_radio_select_language_${obj.value}`} 
          testID={`button_radio_select_language_${obj.value}`}
        />
        <RadioButtonLabel
          obj={obj}
          index={i}
          onPress={() => this.onRadioButtonPress(i, obj)}
          labelStyle={styles.radioFormLabelStyle}
          labelWrapStyle={{}}
          accessibilityLabel={`button_select_language_${obj.value}`} 
          testID={`button_select_language_${obj.value}`}
        />
      </RadioButton>
    );
  }

  render() {
    console.log(this.props.language);
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <View style={styles.radioFormContainer}>
            <Text 
              style={styles.langSelectTxt}
              accessibilityLabel={'text_view_select_language'} 
              testID={'text_view_select_language'}
            >{this.state.selctLanguageTxt}</Text>
            <RadioForm animation>
              {this
                .state
                .languageSelectionData
                .map((obj, i) => this.renderRadioButton(i, obj))
              }
            </RadioForm>

          </View>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            <TouchableOpacity 
              accessibilityLabel={'button_change_language'} 
              testID={'button_change_language'}
              style={styles.payBtn} 
              onPress={() => this.changeLang()}>
              <Text style={styles.payBtnTxt}>
                {this.state.buttonText}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: LanguageSelection => SectionItems ', state);
  return { language: state.lang.current_lang, langValue: state.lang.langValue };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },

  topContainer: {
    flex: 2
  },

  radioButtonStyle: {
    alignSelf: 'flex-start'
  },
  langSelectTxt: {
    fontSize: Styles.defaultFontSize,
    color: Colors.radioFormLabelColor,
    margin: 10,
    marginTop: 2,
    marginBottom: 8,
    padding: 5
  },
  radioFormContainer: {
    flex: 1,
    alignItems: 'flex-start',
    paddingTop: 10,
    paddingLeft: 10,
    paddingBottom: 50,
    borderWidth: 0,
    marginLeft: 10,
    marginBottom: 70
  },

  buttonWrapStyle: {
    marginLeft: 5,
    padding: 7
  },
  radioFormLabelStyle: {
    fontSize: Styles.defaultFontSize,
    color: Colors.radioFormLabelColor,
    lineHeight: 25,
    marginLeft: 10
  },
  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginLeft: 18,
    marginRight: 18
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 5,
    marginTop: 8
  },
  payBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnActive
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  }
});

export default connect(mapStateToProps, actions)(SectionItems);