import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';

import imageSuccessAlert from '../../../../../images/common/success_msg.png';
import imageErrorsAlert from '../../../../../images/common/error_msg.png';

class GeneralModalPinChange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      data: this.props.apiResponse.data
    };
  }

  onBnPress = async() => {

    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    if (this.state.data.success) {

      this
        .props
        .resetMobileActivationState();
      const navigator = this.props.navigator;
      try {
        let isFirstTime = await AsyncStorage.getItem('isFirstTime');
        await AsyncStorage.removeItem('isFirstTime');
        await AsyncStorage.removeItem('api_key');
        await AsyncStorage.removeItem('token');
        await AsyncStorage.setItem('isFirstTime', isFirstTime);
        navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.LoginScreen' });
      } catch (error) {
        console.log(`AsyncStorage error: ${error.message}`);
      }
    }
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              {this.state.data.success
                ? <Image source={imageSuccessAlert} style={styles.alertImageStyle}/>
                : <Image source={imageErrorsAlert} style={styles.alertImageStyle}/>
              }
            </View>

            {this.state.data.success
              ? <View>
                <Text style={styles.descriptionText}>{this.state.data.info}</Text>
              </View>
              : <View>
                <Text style={styles.descriptionText}>{this.state.data.error}</Text>
              </View>
            }
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity onPress={() => this.onBnPress()} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'OK'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {

  const apiResponse = state.mobile.api_response;

  return { apiResponse };

};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },

  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    backgroundColor: '#ffff',
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    alignItems: 'center'
  },

  alertImageStyle: {
    width: 70,
    height: 70,
    marginTop: 10,
    marginBottom: 10
  },
  descriptionText: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10,
    textAlign: 'left',
    fontWeight: '100'

  },
 
  bottomCantainerBtn: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtn: {
    width: 45

  },
  bottomCantainerBtnTxt: {
    fontSize: 20,
    color: 'green'
  }

});

export default connect(mapStateToProps, actions)(GeneralModalPinChange);
