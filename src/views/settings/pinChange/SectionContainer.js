import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import Colors from '../../../config/colors';

class SectionContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            sectionClick: false,
            deafaultState: true
        };
    }

    render() {
        const { customStyle, children } = this.props;

        return (
            <View style={[styles.container, customStyle]}>
                {children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 0,
        marginTop: -10,
        marginLeft: 35,
        marginRight: 15,
        backgroundColor: Colors.appBackgroundColor
    }
});

export default SectionContainer;
