import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';

class MaterialInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  inputChanged = (text) => {
    if (this.props.index === 1) {
      this
        .props
        .getCurrentPin(text);
    } else if (this.props.index === 2) {
      this
        .props
        .getNewPin(text);
    } else if (this.props.index === 3) {
      this
        .props
        .getConfirmedPin(text);
    }
  }

  render() {
    const {
      customStyle,
      label,
      title,
      icon,
      secureTextEntry,
      keyboardType,
      editable
    } = this.props;
    console.log(this.props.value);
    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldStyle}>
            <TextField
              label={label}
              title={title}
              value={this.props.value}
              secureTextEntry={secureTextEntry}
              keyboardType={keyboardType}
              onChangeText={this.inputChanged}
              editable={editable}
              maxLength={this.props.maxLength}/>
          </View>
          <View style={styles.imageStyle}>
            <MaterialIcons
              name={icon}
              size={Styles.inputFieldIconSize}
              color={Colors.defaultIconColorBlack}/>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log(state);
  let value = '';
  switch (ownProps.index) {
    case 1:
      value = state.pin.current_pin;
      break;
    case 2:
      value = state.pin.new_pin;
      break;
    case 3:
      value = state.pin.confirmed_pin;
      break;
    default:
      value = '';
  }

  return { value };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 0,
    marginTop: -18,
    marginLeft: Styles.matirialInput.marginLeft,
    marginRight: Styles.matirialInput.marginRight
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  textFieldStyle: {
    flex: 7,
    justifyContent: 'flex-start'
  },
  imageStyle: Styles.matirialInput.imageStyle
});

export default connect(mapStateToProps, actions)(MaterialInput);
