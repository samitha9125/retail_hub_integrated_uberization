import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import MaterialInput from './MaterialInput';
import InputSection from './InputSection';
import SectionContainer from './SectionContainer';
import ActIndicator from './ActIndicator';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import strings from '../../..//Language/settings';
import Utills from '../../../utills/Utills';

const Utill = new Utills();

class SectionItems extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      phone: '',
      section_1_label: strings.currentPin,
      label_1: '',
      title_1: '',
      section_2_label: strings.newPin,
      lable2: '',
      title2: '',
      section_3_label: strings.confirmPin,
      lable3: '',
      title3: '',
      buttonText: strings.pinBtnTxt,
      connSelectionOption: 'mobile',
      apiLoading: false,
      activateBtnTxtColor: Colors.btnDeactiveTxtColor,
      activateBtnColor: Colors.btnDeactive
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.firstValidated) {
      nextProps.getNewPin('');
      nextProps.getConfirmedPin('');
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated) {
      this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
    } else {
      this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
    }
  }

  dismissLightBox = () => {
    this
      .props
      .navigator
      .dismissLightBox();
  };

  pinSuccess = (response) => {
    this.setState({ isLoading: false });
    this
      .props
      .getApiResponseMobileAct(response);

    this
      .props
      .navigator
      .showModal({ title: 'GeneralModalPinChange', screen: 'DialogRetailerApp.models.GeneralModalPinChange', overrideBackPress: true });
  }
  pinFailure = (response) => {
    this
      .props
      .getApiResponseMobileAct(response);
    this.setState({ isLoading: false });

    this
      .props
      .navigator
      .showModal({ title: 'GeneralModalPinChange', screen: 'DialogRetailerApp.models.GeneralModalPinChange', overrideBackPress: true });
  }

  pinEx = (error) => {
    this.setState({ isLoading: false });
    Utill.showAlertMsg(error);
  }

  showPinchangeModel = () => {
    const data = {
      old_pw: this.props.currentPin,
      new_pw: this.props.newPin
    };

    this.setState({ isLoading: true });
    Utill.apiRequestPost('changePassword', 'account', 'ccapp', data, this.pinSuccess, this.pinFailure, this.pinEx);
  };

  render() {
    return (
      <View style={styles.container}>
        <InputSection
          key={2}
          label={this.state.section_1_label}
          colorCode={this.props.firstValidated} />
        <SectionContainer >
          <MaterialInput
            label={this.state.label_1}
            index={1}
            title={this.state.title_1}
            icon={'mode-edit'}
            maxLength={4}
            secureTextEntry />
        </SectionContainer>

        <InputSection
          key={3}
          label={this.state.section_2_label}
          colorCode={this.props.firstValidated && this.props.secondValidated} />{this.state.isLoading
            ? <ActIndicator animating />
            : <View />}
        <SectionContainer >
          <MaterialInput
            label={this.state.label_1}
            index={2}
            title={this.state.title_1}
            icon={'mode-edit'}
            maxLength={4}
            editable={this.props.firstValidated}
            secureTextEntry />
        </SectionContainer>

        <InputSection
          key={4}
          label={this.state.section_3_label}
          colorCode={this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated} />
        <SectionContainer >
          <MaterialInput
            label={this.state.label_1}
            index={3}
            title={this.state.title_1}
            icon={'mode-edit'}
            maxLength={4}
            editable={this.props.firstValidated && this.props.secondValidated}
            secureTextEntry />
        </SectionContainer>

        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            <TouchableOpacity
              style={[
                styles.payBtn, {
                  backgroundColor: this.state.activateBtnColor
                }
              ]}
              onPress={() => this.showPinchangeModel()}
              disabled={!(this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated)}>
              <Text
                style={[
                  styles.payBtnTxt, {
                    color: this.state.activateBtnTxtColor
                  }
                ]}>
                {this.state.buttonText}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firstValidated = false;
  let secondValidated = false;
  let thirdValidated = false;

  if (state.pin.current_pin !== '') {
    firstValidated = true;
  } else {
    firstValidated = false;
  }

  if (state.pin.new_pin !== '' && state.pin.new_pin !== state.pin.current_pin && state.pin.new_pin.length === 4) {
    secondValidated = true;
  } else {
    secondValidated = false;
  }

  if (state.pin.confirmed_pin === state.pin.new_pin && state.pin.confirmed_pin !== '') {
    thirdValidated = true;
  } else {
    thirdValidated = false;
  }

  const Language = state.lang.current_lang;

  return {
    firstValidated,
    secondValidated,
    thirdValidated,
    currentPin: state.pin.current_pin,
    newPin: state.pin.new_pin,
    Language
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginLeft: 18,
    marginRight: 18
  },

  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 5,
    marginTop: 8
  },
  payBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnActive
  },
  payBtnTxt: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  }
});

export default connect(mapStateToProps, actions)(SectionItems);