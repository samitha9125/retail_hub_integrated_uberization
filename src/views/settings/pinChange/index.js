import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

import SectionItems from './SectionItems';
import Colors from '../../../config/colors';

class PinChangeMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userLogged: true
        };
    }

    componentWillMount() {
        this
            .props
            .resetPinState();
    }

    render() {
        const { navigator } = this.props;
        return (
            <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
                <View style={styles.container}>
                    <SectionItems navigator={navigator}/>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.appBackgroundColor,
        paddingTop: 15
    },
    keyboardAwareScrollViewContainer: {
        backgroundColor: Colors.appBackgroundColor,
        zIndex: 10000
    }
});

export default connect(null, actions)(PinChangeMain);
