import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import update from 'immutability-helper';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import ActIndicator from '../../general/ActIndicator';
import strings from '../../../Language/settings';
import { LocalStorageUtils } from '../../../utills/';

const Utill = new Utills();

class NumberSelection extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);

    this.state = {
      locals:{
        txtBtnSubmit: strings.btnSubmit,
        rapidEzAccount: strings.rapidEzAccountLabel,
        ezCashAccount: strings.ezCashAccountLabel,
        pleaseSelectDefaultAccount : strings.pleaseSelectDefaultAccount,
        okText: strings.ok,
      },
      selectedEzNumber: '',
      selectedRapidEzNumber: '',
      userSelectedEzCashIndex: '',
      userSelectedRapidEzIndex:'',
      disableEzCashField: false,
      disableRapidEzField: false,
      isLoading: false,
      valuesChanged:false,
      availableNumbers: {
        "ezCash":
          [],
        "rapidEz":
        [],
        "selectedIndexes":{
          "ezCash": '',
          "rapidEz": ''
        }
      }
    };
  }

  alertMessage = (msg) => {
    console.log('xxx alertMessage', msg);
    Alert.alert('', msg, [
      {
        text: this.state.locals.okText,
        onPress: () => console.log('do nothing')
      }
    ], { 
      cancelable: true 
    });
  };

  submitNumberChange = () => {

    let currentEzCashNumber = this.state.availableNumbers.ezCash[this.state.availableNumbers.selectedIndexes.ezCash];
    let currentRapidEzCashNumber = this.state.availableNumbers.rapidEz[this.state.availableNumbers.selectedIndexes.rapidEz];
    let valuesChanged = (this.state.selectedEzNumber != currentEzCashNumber || this.state.selectedRapidEzNumber != currentRapidEzCashNumber);
    
    console.log("availableNumbers.ezCash.length: ", this.state.availableNumbers.ezCash.length);
    console.log("userSelectedEzCashIndex: " + this.state.userSelectedEzCashIndex);
    console.log("availableNumbers.rapidEz.length: ",this.state.availableNumbers.rapidEz.length);
    console.log("userSelectedRapidEzIndex: ", this.state.userSelectedRapidEzIndex);
    console.log((this.state.availableNumbers.ezCash.length > 1 && this.state.userSelectedEzCashIndex === ''));
    console.log((this.state.availableNumbers.rapidEz.length > 1 && this.state.userSelectedRapidEzIndex === ''));
    
    if ( (this.state.availableNumbers.ezCash.length > 1 && this.state.userSelectedEzCashIndex === '')
    ||
    (this.state.availableNumbers.rapidEz.length > 1 && this.state.userSelectedRapidEzIndex === '')
    ){
      this.alertMessage(this.state.locals.pleaseSelectDefaultAccount);
      return;
    } 

    if (!valuesChanged) {
      console.log('No Change');
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    } else {
      console.log("Values Changed: \n ezCash: ",this.state.selectedEzNumber, "\n RapidEz: ",this.state.selectedRapidEzNumber);
      let data = {
        "EzCash": this.state.selectedEzNumber,
        "rapidEz": this.state.selectedRapidEzNumber
      };
      this.updateDefaultNumbers(data);
    }
  
  }

  updateDefaultNumbers = (data) =>{
    // this.props.setNumberCheckPerformed(true);
    console.log('updateDefaultNumbers :: ', data);
    let _this = this;
    try {      
      let responseParams = {};
      if (_this.props.popped_at_login) {   
        let pre_data = _this.props.login_success_response.data.pre_data;
        responseParams = update(data, { $merge: pre_data });
      } else {
        responseParams = data;
      }
      console.log('updateDefaultNumbers :: availableNumbers >', _this.props.availableNumbers);
      console.log('updateDefaultNumbers :: popped_at_login >', _this.props.popped_at_login);
      console.log('updateDefaultNumbers :: login_success_response >', _this.props.login_success_response);
      Utill.apiRequestPost('UpdatePaymentAccounts', 'gsmConnection', 'ccapp', responseParams, (successCb) => {
        const navigatorOb = _this.props.navigator;
        console.log('updateDefaultNumbers :: successCb ', successCb);
        if (_this.props.popped_at_login) {   
          console.log('updateDefaultNumbers :: popped_at_login ');   
          LocalStorageUtils.saveUserDataAfterLogin(_this.props.login_success_response);
        }  
        if (_this.props.availableNumbers !== undefined){
          console.log('updateDefaultNumbers :: availableNumbers ', _this.props.availableNumbers);
          navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
        } else { 
          navigatorOb.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.HomeTileScreen' });
        }   
      }, (error) => { 
        console.log('updateDefaultNumbers :: ex ', error);
        Utill.showAlert(error);
      },(ex) => { 
        console.log('updateDefaultNumbers :: ex ', ex);
        Utill.showAlert(ex);
      });
    } catch (error) {
      console.log('updateDefaultNumbers :: ex_error ', error);
      Utill.showAlert(Utill.getSystemError());
    }
  }

  componentWillUnmount(){
    /**
     * onUnmount will be undefined when navigated to this view from the settings menu.
     */
    if (this.props.onUnmount !== undefined){
      this.props.onUnmount();
    }
  }

  componentDidMount(){

    this.setState({ isLoading: true });

    console.log("this.props.availableNumbers: ", this.props.availableNumbers );

    if (this.props.availableNumbers !== undefined) {
      console.log("this.props.availableNumbers: \n",this.props.availableNumbers );

      let ezCashArray =  this.props.availableNumbers.ezCash;
      let rapidEzArray = this.props.availableNumbers.rapidEz;
  
      let disableEzCashField = false;
      let disableRapidEzField = false;

      if (ezCashArray.length == 0 || ezCashArray.length == 1){
        disableEzCashField = true;
      }

      if (rapidEzArray.length == 0 || rapidEzArray.length == 1){
        disableRapidEzField = true;
      }

      this.setState({ 
        availableNumbers: this.props.availableNumbers,
        disableEzCashField:disableEzCashField,
        disableRapidEzField:disableRapidEzField 
      }, () => {
        this.setState({ 
          selectedEzNumber: this.state.availableNumbers.ezCash[this.state.availableNumbers.selectedIndexes.ezCash],
          selectedRapidEzNumber: this.state.availableNumbers.rapidEz[this.state.availableNumbers.selectedIndexes.rapidEz],
          userSelectedEzCashIndex: this.state.availableNumbers.selectedIndexes.ezCash,
          userSelectedRapidEzIndex: this.state.availableNumbers.selectedIndexes.rapidEz,
          isLoading: false
        });
      });


    } else {
      let data = {
        type: 'avaibableList'
      };
      Utill.apiRequestPost('checkPaymentAccounts', 'gsmConnection', 'ccapp', data, (response) => {

        console.log("avaibableList: ", response);

        var numberSelectionArray = {
          ezCash: response.data.ezCash,
          rapidEz: response.data.rapidEz, 
          selectedIndexes: response.data.selectedIndexes
        };

        let ezCashArray =  numberSelectionArray.ezCash == null ? [] : numberSelectionArray.ezCash;
        let rapidEzArray = numberSelectionArray.rapidEz == null ? [] : numberSelectionArray.rapidEz;
    
        let disableEzCashField = false;
        let disableRapidEzField = false;

        if (ezCashArray.length == 0 || ezCashArray.length == 1){
          disableEzCashField = true;
        }
        if (rapidEzArray.length == 0 || rapidEzArray.length == 1){
          disableRapidEzField = true;
        }
    
        this.setState({ 
          availableNumbers: numberSelectionArray, 
          disableEzCashField:disableEzCashField,
          disableRapidEzField:disableRapidEzField  
        }, () => {
          this.setState({ 
            selectedEzNumber: this.state.availableNumbers.ezCash[this.state.availableNumbers.selectedIndexes.ezCash],
            selectedRapidEzNumber: this.state.availableNumbers.rapidEz[this.state.availableNumbers.selectedIndexes.rapidEz],
            userSelectedEzCashIndex: this.state.availableNumbers.selectedIndexes.ezCash,
            userSelectedRapidEzIndex: this.state.availableNumbers.selectedIndexes.rapidEz,
            isLoading: false
          });
        });
      },(errorResponse) => {
        console.log("Failure Cb", errorResponse);
        this.setState({ isLoading: false } );
      },(exError) => {
        console.log("exception Cb", exError);
        this.setState({ isLoading: false } );
      });
    }
  }

  setSelectedEzNumber = (idx, value) => {
    console.log('xxx setSelectedValue', idx);
    console.log('xxx setSelectedEzNumber', value);

    this.setState({
      selectedEzNumber: value,
      userSelectedEzCashIndex: idx
    }, ()=>{
      let currentEzCashNumber = this.state.availableNumbers.ezCash[this.state.availableNumbers.selectedIndexes.ezCash];
      let currentRapidEzCashNumber = this.state.availableNumbers.rapidEz[this.state.availableNumbers.selectedIndexes.rapidEz];
      let valuesChanged = (this.state.selectedEzNumber != currentEzCashNumber|| this.state.selectedRapidEzNumber != currentRapidEzCashNumber);
      this.setState({ valuesChanged: valuesChanged });
    });

  }

  setSelectedRapidEzNumber = (idx, value) => {
    console.log('xxx setSelectedValue', idx);
    console.log('xxx setSelectedRapidEzNumber', value);

    this.setState({
      selectedRapidEzNumber: value,
      userSelectedRapidEzIndex: idx
    }, () =>{
      console.log('saved state');
      console.log(JSON.stringify(this.state));
      let currentEzCashNumber = this.state.availableNumbers.ezCash[this.state.availableNumbers.selectedIndexes.ezCash];
      let currentRapidEzCashNumber = this.state.availableNumbers.rapidEz[this.state.availableNumbers.selectedIndexes.rapidEz];
      let valuesChanged = (this.state.selectedRapidEzNumber != currentRapidEzCashNumber || this.state.selectedEzNumber != currentEzCashNumber);
      this.setState({ valuesChanged: valuesChanged });
    });
  }

  render() {

    let innerContent;

    innerContent = (
      <View style={styles.containerDropDown}>
        <View style={styles.dropDownTextContainer}>
          <Text style={styles.dropDownText}>{this.state.locals.rapidEzAccount}</Text>    
          <ElementSelctionMenu
            dropDownData={this.state.availableNumbers.rapidEz}
            type={'rapidEz'}
            disabled={this.state.disableRapidEzField}
            placeHolderText={
              this.state.selectedRapidEzNumber
            }
            onSelect={(idx, value) => this.setSelectedRapidEzNumber(idx, value)}
            defaultIndex={
              this.state.userSelectedEzCashIndex === ''?
                this.state.availableNumbers.selectedIndexes.rapidEz
                :
                this.state.userSelectedEzCashIndex
            }/>
        </View>
        <View style={styles.dropDownTextContainer}>
          <Text style={styles.dropDownText}>{this.state.locals.ezCashAccount}</Text>
          {/* </View> */}
          <ElementSelctionMenu
            dropDownData={this.state.availableNumbers.ezCash}
            type={'ezCash'}
            disabled={this.state.disableEzCashField}
            placeHolderText={
              this.state.selectedEzNumber
            }
            onSelect={(idx, value) => this.setSelectedEzNumber(idx, value)}
            defaultIndex={
              this.state.userSelectedEzCashIndex === ''? 
                this.state.availableNumbers.selectedIndexes.ezCash
                : 
                this.state.userSelectedEzCashIndex
            }/>
        </View>
      </View>
    );
    

    return (
      <View style={styles.container}>
        {this.state.isLoading ? 
          <ActIndicator displayText={'Loading'} animating/>
          :true
        }
        
        { innerContent }
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtontext={this.state.locals.txtBtnSubmit}
            onClickOk={() => this.submitNumberChange()}
            self= { this }
          />
            
        </View>
      </View>
    );
  }
}

const ElementSelctionMenu = ({ dropDownData, placeHolderText, onSelect, defaultIndex, disabled }) => (
  <ModalDropdown
    options={dropDownData}
    onSelect={onSelect}
    disabled={disabled}
    defaultIndex={parseInt(defaultIndex)}
    style={styles.modalDropdownStyles}
    dropdownStyle={ styles.dropdownStyle }
    dropdownTextStyle={styles.dropdownTextStyle}
    dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
    <View style={styles.dropdownElementContainer}>
      <View style={styles.dropdownDataElemint}>

        <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
      </View>
      {disabled == false ? 
        <View style={styles.dropdownArrow}>
          <Ionicons name='md-arrow-dropdown' size={20}/>
        </View>  
        : true
      }

    </View>
  </ModalDropdown>
);

const ElementFooter = ({ okButtontext, onClickOk, self }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={ self.state.valuesChanged == true? styles.buttonContainer : styles.buttonContainerDisabled }
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={self.state.valuesChanged == true? styles.buttonTxt : styles.buttonTxtDisabled}>{okButtontext}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = state => {
  const language = state.lang.current_lang;
  return { language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerDropDown: {
    flex: 2,
    paddingTop: 10,
    marginTop: 15,
    //backgroundColor: 'pink'

  },
  containerBottom: {
    flex: 2,
    // justifyContent: 'flex-end',
    alignItems: 'flex-start',
    // backgroundColor: 'green',
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },

  buttonContainerDisabled:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnDeactive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },
  buttonTxtDisabled:{
    textAlign: 'center',
    color: Colors.btnDeactiveTxtColor,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },
  dropDownTextContainer: {
    flex: 0.6,
    //backgroundColor: 'pink'
  },

  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18

  },
  //modal dropdown styles
  modalDropdownStyles: {
    //flex: 1
    width: Dimensions.get('window').width 
  },
  dropdownStyle: {
    width: Dimensions.get('window').width - 30 ,
    marginLeft: 15,
    marginTop: 15,
    height:80,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor,
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  }
});

export default connect(mapStateToProps, actions)(NumberSelection);