import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';//MaterialIcons
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ez_cash_rapidez_icon from '../../../images/drawer/ez_cash_rapid_ez.png';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import strings from '../../Language/settings.js';

class SettingsMain extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      txtLanguageSelction: strings.txtLanguageSelction,
      txtNumberSelection: strings.txtNumberSelection,
      txtNumberSelectionTitle: strings.txtNumberSelectionTitle,
      txtPinReset: strings.txtPinReset
    };
  }

  onSettingsMenuTap = (key) => {
    console.log('xxxx onSettingsMenuTap=>', key);
    const navigator = this.props.navigator;
    switch (key) {
      case 1:
        navigator.push({ title: this.state.txtPinReset, screen: 'DialogRetailerApp.views.settings.PinChangeScreen' });
        break;
      case 2:
        navigator.push({ title: this.state.txtLanguageSelction, screen: 'DialogRetailerApp.views.settings.LanguageSelectionScreen' });
        break;
      // REMOVED FROM INTERIM CHANGES
      // MOVED THIS TO MY ACCOUNT TAB IN DRAWER
      // case 3:
      //   navigator.push({ title: this.state.txtNumberSelectionTitle, screen: 'DialogRetailerApp.views.NumberSelectionScreen' });
      //   break;
      default:
        navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.HomeTileScreen' });
        break;
    }
  };

  render() {
    return (
      <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
        <View style={styles.container}>
          <TouchableOpacity
            key={1}
            style={styles.cardContainer}
            onPress={() => this.onSettingsMenuTap(1)}
            accessibilityLabel={'button_pin_change'} 
            testID={'button_pin_change'}>
            <View style={styles.leftImageContainer}>
              <Ionicons name='md-unlock' size={37} color='black' />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{this.state.txtPinReset}
              </Text>
            </View>
            <View style={styles.rightImageContainer}>
              <Ionicons name='ios-arrow-forward' size={25} color='black' />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            key={2}
            style={styles.cardContainer}
            onPress={() => this.onSettingsMenuTap(2)}
            accessibilityLabel={'button_language_selection'} 
            testID={'button_language_selection'}>
            <View style={styles.leftImageContainer}>
              <Ionicons name='ios-chatbubbles' size={37} color='black' />
            </View>
            <View style={styles.middleTextContainer}>
              <Text style={styles.innerText}>{this.state.txtLanguageSelction}
              </Text>
            </View>
            <View style={styles.rightImageContainer}>
              <Ionicons name='ios-arrow-forward' size={25} color='black' />
            </View>
          </TouchableOpacity> 
        </View>
      </KeyboardAwareScrollView>
    );
  }
}


const mapStateToProps = (state) => {
  return { language: state.lang.current_lang,  isRetailer: state.auth.isretailer, };
};

const styles = StyleSheet.create({
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5
  },
  leftImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 8
  },
  ez_cash_rapidez_icon: {
    height: 37,
    width: 37
  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  rightImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },
  innerText: {
    flex: 1,
    justifyContent: 'center',
    fontSize: 20,
    color: Colors.colorBlack,
    padding: 8
  }
});

export default connect(mapStateToProps, actions)(SettingsMain);
