import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  BackHandler
} from 'react-native';
import Orientation from 'react-native-orientation';
import Pdf from 'react-native-pdf';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import ActIndicator from '../../general/ActIndicator';
import strings from '../../../Language/settings';
import { Header } from '../../common/components/Header';
import Utills from '../../../utills/Utills';

const Utill = new Utills();

class Help extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      locals: {
        helpTitle: this.props.title
      }
    };
  }

  componentDidMount() {
    console.log('xxxxx componentDidMount help screen');
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    let requestParams = {
      language: this.props.language
    };
    Utill.apiRequestPost('getUserManualByLang', 'userManual', 'ccapp', requestParams, this.onSuccess, this.onFailure);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    this.okHandler();  
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  onSuccess = (response) => {
    console.log('xxx onSuccess', response);
    if (response.data.response.view_mode === 'L') {
      console.log('view mode change');
      Orientation.lockToLandscape();
    } else {
      Orientation.lockToPortrait();
    }
    console.log('xxx show help screen url :: response', response.data.response.url);
    console.log('PDF_URL', response.data.response.url);
    let url = response.data.response.url;
    let view_mode = response.data.response.view_mode;
    this.setState({ url: url });
    this.setState({ view_mode: view_mode });
  };

  onFailure = (response) => {
    console.log('xxx show help screen loading url if error :: response', response);
  };

  render() {
    let loadingIndicator;

    if (this.props.auth_api_loading) {
      loadingIndicator = (<ActIndicator animating />);
    } else {
      loadingIndicator = true;
    }
    console.log(this.state.url, '###################### PDF_URL ####################');
    console.log(this.state.view_mode, "############### VIEW_MODE ###################");
    const source = {
      // uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf',   
      uri: this.state.url,
      cache: true
    };
    if (this.state.view_mode === 'L') {
      return (
        <View style={styles.container}>
          <Header
            backButtonPressed={() => this.handleBackButtonClick()}
            headerText={this.state.locals.helpTitle}
            accessibilityLabel={'header_help'} 
            testID={'header_help'}/>
          {loadingIndicator}
          <Pdf
            source={source}
            style={styles.pdfL}
          // scale={2.76}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <Header
            backButtonPressed={() => this.handleBackButtonClick()}
            headerText={this.state.locals.helpTitle}
            accessibilityLabel={'header_help'} 
            testID={'header_help'}/>
          {loadingIndicator}
          <Pdf
            source={source}
            style={styles.pdf} />
        </View>
      );
    }
  }
}

const mapStateToProps = state => {
  console.log('****** REDUX STATE :: LTE => help :: index \n', state);
  const language = state.lang.current_lang;
  const auth_api_loading = state.auth.auth_api_loading;
  return {
    language,
    auth_api_loading
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width
  },
  pdfL: {
    flex: 1,
    width: Dimensions.get('window').height
  },
});

export default connect(mapStateToProps, actions)(Help);