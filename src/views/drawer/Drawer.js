import React from 'react';
import { StyleSheet, View, AsyncStorage, Dimensions, Linking, ScrollView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import DrawerItems from './DrawerItem';
import strings from '../../Language/Home';
import DeviceCompatibleModel from '../../views/authz/DeviceCompatibleModal';
import { Analytics, Screen, AccountUtils } from '../../utills';
import { Colors, Constants, Images } from '../../config/';
import Utills from '../../utills/Utills';
import img2 from '../../../images/menu_icons/sim_change.png';
import img1 from '../../../images/menu_icons/bill_payment.png';
import img3 from '../../../images/menu_icons/mobile_activation.png';
import img4 from '../../../images/menu_icons/television.png'; 
import img6 from '../../../images/menu_icons/activation_status.png';
import img7 from '../../../images/menu_icons/delivery_truck.png';
import hbb from '../../../images/menu_icons/hbb.png';
import img10 from '../../../images/menu_icons/cart.png';
import img11 from '../../../images/menu_icons/customer_tracker.png';
import img12 from '../../../images/menu_icons/stock.png';
import img13 from '../../../images/menu_icons/stock_request_approval.png';
import img14 from '../../../images/menu_icons/workoder.png';
import img15 from '../../../images/menu_icons/cart.png';
import img97 from '../../../images/drawer/help.png';
import img98 from '../../../images/drawer/settings.png';
import img99 from '../../../images/drawer/myAccount.png';
import img100 from '../../../images/drawer/logout.png';

const { width, height } = Dimensions.get('window');
// eslint-disable-next-line no-constant-condition
const drawerWidth = width - 60;
const Utill = new Utills();

class Drawer extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      SimChangeTit: strings.SimChangeTit,
      BillPayTit: strings.BillPayTit,
      MobActivateTit: strings.MobActivateTit,
      ActivationStateTit: strings.transactionHistoryTit,
      SimChange: strings.SimChange,
      BillPay: strings.BillPay,
      MobActivate: strings.MobActivate,
      ActivationState: strings.transactionHistory,
      WorkOrders: strings.WorkOrders,
      hbb: strings.hbb,
      stockManagement: strings.stockManagement,//cfss
      customerTracker: strings.customerTracker,
      stockRequestApproval: strings.stockRequestApproval,
      directSales: strings.directSales,
      wom: strings.wom,//cfsss
      setting: strings.setting,
      settingTit: strings.settingTit,
      rejected: strings.rejected,
      Logout: strings.Logout,
      myAccount: strings.myAccount,
      myAccountTitle: strings.myAccountTitle,
      hbbActivation: strings.hbb,
      DTVActivation: strings.DTVActivation,
      all: strings.all,
      help: strings.help,
      helpTit: strings.helpTit,
      modalVisible: false,
      btnOk: strings.btnOk,
      deviceCompatibleInfo: {
        state: 'pending',
        description: ''
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log("## Drawer :: componentWillReceiveProps");
    const language = nextProps.language;
    if (language !== null) {
      strings.setLanguage(language);
      this.setStrings();
    }
  }

  componentDidMount() {
    this.getLanguage();
  }

  setStrings = ()=> {
    this.setState({
      SimChangeTit: strings.SimChangeTit,
      BillPayTit: strings.BillPayTit,
      MobActivateTit: strings.MobActivateTit,
      HbbActivateTit: strings.hbb,
      ActivationStateTit: strings.transactionHistoryTit,
      myAccountTitle: strings.myAccountTitle,
      SimChange: strings.SimChange,
      BillPay: strings.BillPay,
      MobActivate: strings.MobActivate,
      hbb: strings.hbb,
      ActivationState: strings.transactionHistory,
      rejected: strings.rejected,
      setting: strings.setting,
      settingTit: strings.settingTit,
      help: strings.help,
      helpTit: strings.helpTit,
      Logout: strings.Logout,
      myAccount: strings.myAccount,
      all: strings.all,
      DTVActivationTit: strings.DTVActivationTit
    });
  }

   getLanguage = async ()=> {
     try {
       let perData = await Utill.getPersistenceData();
       console.log("Language on persistance: ", perData.lang);
       console.log("\nthis.state.languageSelectionValue: ", this.state.languageSelectionValue);
       console.log("\nthis.state.langValue: ", this.state.langValue);
       strings.setLanguage(perData.lang);
       this.setStrings();
     } catch (error) {
       console.log(`AsyncStorage error: ${error.message}`);
     }
   }

  onDrawerBtnPress = (key, menuObject = null) => {
    console.log('xxx onDrawerBtnPress =>', key);
    this.props.configSetActivityStartTime();
    console.log('xxx menuObject =>', menuObject);
    this.toggleDrawer();
    if (menuObject !== null) {
      if (menuObject.tile_status == false) {
        this.setState({
          modalVisible: true,
          deviceCompatibleInfo: {
            state: 'pending',
            description: menuObject.compat_data.description,
            url: menuObject.compat_data.url
          }
        });
        return;
      }
    }
    const navigator = this.props.navigator;

    switch (key) {
      case Constants.TILE_ID.BILL_PAYMENT:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.BILL_PAYMENT);
        break;
      case Constants.TILE_ID.SIM_CHANGE:
        navigator.push({ title: this.state.SimChangeTit, screen: 'DialogRetailerApp.views.SimChangeScreen' });
        break;
      case Constants.TILE_ID.GSM_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.GSM_ACTIVATION);
        break;
      case Constants.TILE_ID.DTV_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.DTV_ACTIVATION);
        break;
      case Constants.TILE_ID.TRACTIONS_HISTORY:
        navigator.push({
          screen: 'DialogRetailerApp.views.TransactionHistoryMainScreen',
          title: this.state.ActivationStateTit
        });
        break;
      case Constants.TILE_ID.DELIVERY_APP:
        navigator.push({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
        break;
      // Case 8 is Doc990. Doc990 won't be available on Drawer.
      case Constants.TILE_ID.LTE_ACTIVATION:
        this.checkDefaultAccountApiCall(Constants.TILE_ID.LTE_ACTIVATION);
        break;
      case 10:
        navigator.push({
          title: "DIRECT SALES",
          screen: 'DialogRetailerApp.views.DirectSale',
          passProps: {
            screenTitle: "DIRECT SALES",
          },
        });
        break;
      case 11:
        navigator.push({
          title: "CUSTOMER TRACKER",
          screen: 'DialogRetailerApp.views.CpeTracker'
        });
        break;
      case 12:
        navigator.push({
          title: "STOCK MANAGEMENT",
          screen: 'DialogRetailerApp.views.StockManagement'
        });
        break;
      case 13:
        navigator.push({
          title: "STOCK REQUEST APPROVAL",
          screen: 'DialogRetailerApp.views.StockRequestApproval'
        });
        break;
      case 14:
        navigator.push({
          title: "WORK ORDER",
          screen: 'DialogRetailerApp.views.WomLandingView'
        });
        break;
      case 15:
        navigator.push({
          title: "DTV POST TO PRE CONVERSION",
          screen: 'DialogRetailerApp.views.PostToPreConversionScreen',
          passProps: {
            name: 'DTV POST TO PRE CONVERSION'
          }
        });
        break;
      case 97:
        navigator.push({ 
          title: this.state.helpTit, 
          screen: 'DialogRetailerApp.views.settings.HelpScreen' ,
          passProps: {
            title: this.state.helpTit
          }
        });
        break;  
      case 98:
        navigator.push({ title: this.state.settingTit, screen: 'DialogRetailerApp.views.settings.MainScreen' });
        break;
      case 99:
        navigator.push({ title: this.state.myAccountTitle, screen: 'DialogRetailerApp.views.myAccount.MainScreen' });
        break;
      default:
        navigator.resetTo({ title: this.state.ActivationStateTit, screen: 'DialogRetailerApp.views.HomeTileScreen' });
        break;
    }
  };

  /**
   * This function will push the user from the home menu to relevant screen after checking if the required
   * EzCash and RapidEz accounts are configured.
   * 
   * @param {Number} key - id of the tile selected
   * @memberof Drawer
   */

  checkDefaultAccountApiCall = (key) => {
    console.log('checkDefaultAccountApiCall =>', key);
    this.props.getConfigurations(this, key);
  }

  
  checkDefaultAccountAndPush = (key) => {
    console.log('## checkDefaultAccountAndPush :: ', key);
    let _this = this;
    const navigator = _this.props.navigator;
    let defaultEzNumber = _this.props.getConfiguration.payment_accounts.ezCashNumber !== null ? _this.props.getConfiguration.payment_accounts.ezCashNumber[0] : "";
 
    _this.props.configSetDefaultEzCashAccount(defaultEzNumber);

    switch (key){
      case 1:
        navigator.push({ 
          title: _this.state.BillPay, 
          screen: 'DialogRetailerApp.views.BillPaymentScreen',
          passProps: { 
            defaultEzNumber: defaultEzNumber
          }
        }); 
        break;
      case 3:
        navigator.push({ 
          title: _this.state.MobActivate, 
          screen: 'DialogRetailerApp.views.MobileActivationScreen', 
          passProps: { 
            title: _this.state.MobActivate,
            defaultEzNumber: defaultEzNumber 
          } 
        });
        break;
      case 4:
        _this.checkEzcashAccount({ 
          title: _this.state.dtvActivation, 
          screen: 'DialogRetailerApp.views.DtvActScreen', 
          passProps: { 
            title: _this.state.DTVActivation,
            defaultEzNumber: defaultEzNumber 
          } 
        });
        break;
      case 9:
        _this.checkEzcashAccount({
          title: _this.state.hbb,
          screen: 'DialogRetailerApp.screens.CoverageMapScreen',
          passProps: {
            title: this.state.hbb,
            defaultEzNumber: defaultEzNumber
          }
        });
        break;
      default:
        console.log('Check Default Acc Default block');
    }

  }

  onOkPress = () => {
    console.log('## Drawer :: onOkPress');
    this.setState({ modalVisible: false });
  }

  openUrl = () => {
    var spillable = this.state.deviceCompatibleInfo.url.split('/');
    var action = spillable[0];
    var controller = spillable[1];
    let url = Utill.createApiUrl(controller, action, '');
    console.log(url);
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        return Linking.openURL(url);
      }
    }).catch(err => console.log('An error occurred', err));
  }

  onLogout = async () => {
    console.log('## Drawer :: onLogout');
    this.toggleDrawer();
    let data = {};
    let isCFSSAgent = await AsyncStorage.getItem('isCFSSAgent');
    if (!_.isNil(isCFSSAgent) && (isCFSSAgent == 'true' || isCFSSAgent == true)) {
      console.log('onLogout :: isCFSSAgent TRUE - ', isCFSSAgent);
      Utill.getGPSLocation()
        .then(location => {
          data = {
            latitude: location.latitude,
            longitude: location.longitude
          };
          console.log('onLogout :: CFSSAgent_WITH_LOCATION_DATA - ', data);
          this.logOutRequests(data);
        })
        .catch(response => {
          data = {
            latitude: 0,
            longitude: 0
          };
          console.log('onLogout :: CFSSAgent_WITH_OUT_LOCATION_DATA - ', response);
          this.logOutRequests(data);
        });
    } else {
      console.log('onLogout :: Retailer TRUE');
      this.logOutRequests(data);
    }
  }

  logOutRequests = (data) => {
    this.clearPersistanceDataToLogout();
    Utill.stopHeartBeatService();
    try {
      Utill.apiRequestPost2('logout', 'account', 'ccapp', data, this.logOutRequestCallback, this.logOutRequestCallback, this.logOutRequestCallback);
    } catch (error) {
      console.log(`logOutRequests :: error: ${error.message}`);
    }
  }

  logOutRequestCallback = (response) => {
    console.log("logOutRequestCallback :: ", response);
  }

  clearPersistanceDataToLogout = async () => {
    console.log('clearPersistanceDataToLogout');
    this.props.setLanguage('en', 0);
    const navigator = this.props.navigator;
    try {
      let isFirstTime = await AsyncStorage.getItem('isFirstTime');
      await AsyncStorage.removeItem('isFirstTime');
      await AsyncStorage.removeItem('api_key');
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('conn');
      await AsyncStorage.clear();
      await AsyncStorage.setItem('isFirstTime', isFirstTime);
      console.log(AsyncStorage.getItem('isFirstTime'));
      console.log(AsyncStorage.getItem('api_key'));
      console.log(AsyncStorage.getItem('token'));
      console.log(AsyncStorage.getItem('conn'));
      navigator.resetTo({ title: 'Dialog Sales App', screen: 'DialogRetailerApp.views.LoginScreen' });
    } catch (error) {
      console.log(`clearPersistanceDataToLogout :: AsyncStorage error: ${error.message}`);
    }
  }

  toggleDrawer = () => {
    this.props.navigator.toggleDrawer({ side: 'left' });
  };

  /**
   * @description This function  will check ezcash availability
   * @param {Object} pushView
   * @memberof TileView
   */
  checkEzcashAccount = (pushView) => {
    if ( this.props.getConfiguration.ez_cash_account.ez_cash_account_availability == false ){
      let description = this.props.ezCash_error;
      let passProps = {
        primaryText: this.state.btnOk,
        primaryPress: () => this.onPressOk(),
        primaryButtonStyle: { color: Colors.colorRed },
        disabled: false,
        hideTopImageView: false,
        icon: Images.icons.FailureAlert,
        description: Utill.getStringifyText(description),
      };
      Screen.showGeneralModal(passProps);
    } else {
      const navigator = this.props.navigator;
      navigator.push(pushView);
    }
  }

  /**
   * @description Back to homeScreen
   * @memberof BottomItem
   */
  onPressOk = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animationType: 'slide-down' });
    navigator.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }

  renderMenuItems = () => {
    console.log('## Drawer :: renderMenuItems');
    console.log("## Drawer :: tileData: ", this.props.tileData);
    var menuItemsArray = [];
    var allowedServices = [];

    console.log('## Drawer :: tileData => this.props.tileData', this.props.tileData);

    if (_.isEmpty(this.props.tileData)) {
      console.log('## Drawer :: tileData => empty ');
      return;
    }

    if (this.props.tileData === undefined || this.props.tileData === null || this.props.tileData.length === 0) {
      console.log('## Drawer :: tileData => undefined, null or empty ');
      return;
    }

    let services = JSON.parse(this.props.tileData);
    let leftArray = services[0];
    let rightArray = services[1];

    console.log('leftArray: ', leftArray);
    console.log('rightArray: ', rightArray);

    if (leftArray === undefined || rightArray === undefined) {
      console.log('## Drawer :: leftArray,rightArray === undefined');
      return;
    }

    allowedServices = leftArray.concat(rightArray);
    console.log('allowedServices drawer: ', allowedServices);

    if (allowedServices == null) {
      return;
    }

    for (var key in allowedServices) {
      // skip loop if the property is from prototype
      // if (!allowedServices.hasOwnProperty(key)) continue;
      // var obj = allowedServices[key];
      console.log("key: ", key);
      console.log("allowedServices[key].id: ", allowedServices[key].id);
      console.log("allowedServices[key]: ", allowedServices[key]);
      let menuObject = allowedServices[key];
      // console.log("Obj: ", obj);
      if (allowedServices !== null) {
        switch (parseInt(allowedServices[key].id)) {
          case 1:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={1}
              key={1}
              imgSrc={img1}
              accessibilityLabel={'button_bill_payment'} 
              testID={'button_bill_payment'}
              label={this.state.BillPay} />);
            break;
          case 2:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={2}
              key={2}
              imgSrc={img2}
              accessibilityLabel={'button_sim_change'} 
              testID={'button_sim_change'}
              label={this.state.SimChange} />);
            break;
          case 3:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={3}
              key={3}
              imgSrc={img3}
              accessibilityLabel={'button_mobile_activation'} 
              testID={'button_mobile_activation'}
              label={this.state.MobActivate} />);
            break;
          case 4:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              index={4}
              key={4}
              imgSrc={img4}
              accessibilityLabel={'button_dtv_activation'} 
              testID={'button_dtv_activation'}
              label={this.state.DTVActivation} />);
            break;
          case 6:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={6}
              key={6}
              imgSrc={img6}
              accessibilityLabel={'button_transaction_history'} 
              testID={'button_transaction_history'}
              label={this.state.ActivationState} />);
            break;
          case 7:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={7}
              key={7}
              imgSrc={img7}
              accessibilityLabel={'button_work_orders'} 
              testID={'button_work_orders'}
              label={this.state.WorkOrders} />);
            break;
          // Case 8 is Doc990. Doc990 won't be available on Drawer.
          case 9:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              index={9}
              key={9}
              imgSrc={hbb}
              accessibilityLabel={'button_hbb_activation'} 
              testID={'button_hbb_activation'}
              label={this.state.hbb} />);
            break;
          case 10:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={10}
              key={10}
              imgSrc={img10}
              testID={'side_menu_10'}
              label={this.state.directSales} />);
            break;
          case 11:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={11}
              key={11}
              imgSrc={img11}
              testID={'side_menu_11'}
              label={this.state.customerTracker} />);
            break;
          case 12:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={12}
              key={12}
              imgSrc={img12}
              testID={'side_menu_12'}
              label={this.state.stockManagement} />);
            break;
          case 13:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={13}
              key={13}
              imgSrc={img13}
              testID={'side_menu_13'}
              label={this.state.stockRequestApproval} />);
            break;
          case 14:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={14}
              key={14}
              imgSrc={img14}
              label={this.state.wom} />);
            break;
          case 15:
            menuItemsArray.push(<DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index, menuObject)}
              customStyles={styles.topDrawerItem}
              index={15}
              key={15}
              imgSrc={img15}
              label={this.state.directSales} />);
            break;
          default:
            console.log('Drawer default');
        }
      }
    }
    return (
      menuItemsArray.map(menuItem => (menuItem))
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          // contentContainerStyle={{ height:height }}
          accessibilityLabel={'drawer_list'} 
          testID={'drawer_list'}
          style={styles.scrollContainer}>
          <View style={styles.containerTopMost} />
          <View key={this.props.allowedServices} style={styles.containerTop}>
            {this.renderMenuItems()}
          </View>
          <View style={styles.containerMiddle} />
          <View style={styles.containerBottom}>
            <DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index)}
              customStyles={styles.bottomDrawerItem}
              index={97}
              key={97}
              imgSrc={img97}
              accessibilityLabel={'button_help'} 
              testID={'button_help'}
              label={this.state.help} />
            <DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index)}
              customStyles={styles.bottomDrawerItem}
              index={98}
              key={98}
              imgSrc={img98}
              accessibilityLabel={'button_settings'} 
              testID={'button_settings'}
              label={this.state.setting} />
            {console.log("this.props.isRetailer: ", this.props.isRetailer)}
            {this.props.isRetailer ? <DrawerItems
              onPress={(index) => this.onDrawerBtnPress(index)}
              customStyles={styles.bottomDrawerItem}
              index={99}
              key={99}
              imgSrc={img99}
              accessibilityLabel={'button_my_account'} 
              testID={'button_my_account'}
              label={this.state.myAccount} /> : <View />}
            <DrawerItems
              onPress={() => this.onLogout()}
              customStyles={styles.bottomDrawerItem}
              index={100}
              key={100}
              imgSrc={img100}
              accessibilityLabel={'button_logout'} 
              testID={'button_logout'}
              label={this.state.Logout} />
          </View>
          <View style={styles.containerLast} />
        </ScrollView>
        <DeviceCompatibleModel
          msisdn={111}
          deviceCompatibleInfo={this.state.deviceCompatibleInfo}
          onOkPress={() => this.onOkPress()}
          modalVisible={this.state.modalVisible}
          locals={{ ok: 'OK' }}
          openUrl={() => this.openUrl()}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Drawer => state ', state);
  const language = state.lang.current_lang;
  const allowedServices = state.auth.allowedServices;
  const isRetailer = state.auth.isretailer;
  const tileData= state.auth.tileData;
  const getConfiguration = state.auth.getConfiguration;
  const { ezCash_error = '' } = getConfiguration.ez_cash_account;

  return {
    language,
    allowedServices,
    isRetailer,
    tileData,
    getConfiguration,
    ezCash_error,
  };
};

const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: drawerWidth,
    flexDirection: 'column',
    zIndex: 9999999,
    backgroundColor: Colors.appBackgroundColor
  },

  scrollContainer: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    height: height
  },

  containerTopMost: {
    flex: 0.1,
    height: 10,
    width: drawerWidth,
    justifyContent: 'center',

  },
  containerTop: {
    flex: 0.8,
    width: drawerWidth,
    justifyContent: 'flex-start'
  },
  containerMiddle: {
    flex: 0.3,
    height: 55,
    width: drawerWidth,
    justifyContent: 'flex-start'
  },
  containerBottom: {
    flex: 1.2,
    width: drawerWidth,
    justifyContent: 'flex-end',
    flexDirection: 'column'
  },
  // containerBottom2: {
  //   height : height/3.2,
  //   width : drawerWidth,
  //   justifyContent: 'flex-end',
  // },
  containerLast: {
    flex: 0.1,
    width: drawerWidth,
    justifyContent: 'flex-start'
  },
  bottomDrawerItem: {
    alignSelf: 'flex-end'
  },
  topDrawerItem: {
    alignSelf: 'flex-start'
  }
});

export default connect(mapStateToProps, actions)(Drawer);
