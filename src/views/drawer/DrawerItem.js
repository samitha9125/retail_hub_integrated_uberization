import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import { StyleSheet, View, Dimensions, Text, TouchableOpacity, Image } from 'react-native';
import Colors from '../../config/colors';
const { width } = Dimensions.get('window');

const drawerWidth = width-60;
class DrawerItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  getDynamicStyle = () => {
    if (this.props.language == 'en'){
      return { height:54 };
    }
    if (this.props.language == 'si'){
      return { height:56 };
    }
    if (this.props.language == 'ta'){
      return { height:60 };
    }

  }

  // getDynamicFontStyle = () => {
  //   if (this.props.language !== 'en'){
  //     return { height:55 };
  //   }
  //   if (this.props.language !== 'si'){
  //     return { height:56 };
  //   }
  //   if (this.props.language !== 'ta'){
  //     return { height:'75' };
  //   }

  // }

  render() {
    const { onPress, index, label, imgSrc, customStyles={}, testID, accessibilityLabel } = this.props;

    return (
      <View style={[styles.container, customStyles, this.getDynamicStyle() ]}>
        <View 
          key={index} 
          style={[styles.cardContainer,this.getDynamicStyle()]}
          // accessibilityLabel={`side_menu_item_${index}`} 
          // testID={`side_menu_item_${index}`} 
        >
          <View style={styles.imageContainer}>
            <TouchableOpacity style={styles.imageContainer1} onPress={() => onPress(index)}>
              <Image style={styles.imageStyle} source={imgSrc} />
            </TouchableOpacity>
          </View>
          <View 
            style={[styles.textContainer,this.getDynamicStyle()]}
            accessibilityLabel={accessibilityLabel}
            testID={testID}
          >
            <Text style={[styles.innerText, { fontSize: this.props.language !== 'en'? 18 : 20 }]} onPress={() => onPress(index)}>{label}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  language: state.lang.current_lang,
});

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: Colors.appBackgroundColor,
    height: 55,
    width:drawerWidth-10,
    marginTop:5,
    margin: 5,
    // backgroundColor: 'purple',
    flexDirection:'column'
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'flex-start',
    // alignItems: 'flex-start',
    // alignSelf: 'center',
    padding: 5,
    // backgroundColor: 'red'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // // paddingRight: 20,
    padding: 1,
    flexDirection: 'row',
    marginRight: 5,
    // backgroundColor: 'green'
  },
  imageContainer1: {
    flex: 1,
    justifyContent: 'center',
    alignSelf:'center',
    // alignItems: 'center',
    // padding: 5,
    // // paddingRight: 20,
    // margin: 2,
    // backgroundColor: 'purple',
    flexDirection: 'column'
  },
  textContainer: {
    flex: 6,
    // justifyContent: 'center',
    // alignItems: 'flex-start',
    // marginLeft: 5,
    // paddingLeft: 5,
    // paddingRight: 5,
    // backgroundColor: 'blue'
  },
  imageStyle: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',

    width: '100%',
    // height: 48,
    // margin: 5,
    resizeMode:'center',
    // backgroundColor: 'yellow'
  },
  innerText: {
    flex: 1,
    justifyContent: 'center',
    fontSize: 20,
    fontWeight: '400',
    color: Colors.colorBlack,
    padding: 8,
    // lineHeight:26
    // backgroundColor: 'pink'
  }
});
export default connect(mapStateToProps,actions)(DrawerItem);