import React from 'react';
import { StyleSheet, View, Keyboard, TouchableOpacity } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

class MaterialInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  inputChanged = (text) => {
    if (this.props.label === 'Connection No') {
      this
        .props
        .getConnectionNumberBill(text);
      console.log('updated Connection Number');
    } else if (this.props.label === 'Eg: 199 or 199.00') {
      this
        .props
        .getAmountBill(text);
      console.log('updated Amount');
    } else if (this.props.label === 'eZ Cash Account PIN') {
      this
        .props
        .getEzCashPinBill(text);
      console.log('eZ Cash Account PIN');
    }
  }

  render() {
    const {
      customStyle,
      title,
      icon,
      secureTextEntry,
      keyboardType,
      editable,
      disabled,
      ref,
      isNeedToFocus,
      onTextFieldPress,
      onPress = () => console.log('onPressDeafault')
    } = this.props;

    let refTextInput = ref;

    const keyboardAwareOnPress = function () {
      if (isNeedToFocus) {
        refTextInput.focus();
      } else {
        Keyboard.dismiss();
      }
      onPress.apply(this, arguments);
    };

    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldStyle}>
            <TextField
              title={title}
              label=''
              ref={ref => refTextInput = ref}
              value={this.props.value}
              maxLength={this.props.maxLength}
              secureTextEntry={secureTextEntry}
              keyboardType={keyboardType}
              onChangeText={this.inputChanged}
              editable={editable}
              onFocus={onTextFieldPress}/>
          </View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={keyboardAwareOnPress}
            disabled={disabled}>
            <MaterialIcons
              name={icon}
              size={Styles.inputFieldIconSize}
              color={Colors.defaultIconColorBlack}/>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  let value = '';
  switch (ownProps.label) {
    case 'Connection No':
      value = state.bill.connection_number;
      break;
    case 'Eg: 199 or 199.00':
      value = state.bill.amount;
      break;
    case 'eZ Cash Account PIN':
      value = state.bill.ez_cash_pin;
      break;
    default:
      return '';
  }
  return { value };
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 0,
    marginTop: -28,
    marginLeft: Styles.matirialInput.marginLeft,
    marginRight: Styles.matirialInput.marginRight
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  textFieldStyle: {
    flex: 7,
    justifyContent: 'flex-start'
  },
  imageStyle: Styles.matirialInput.imageStyle
});

export default connect(mapStateToProps, actions)(MaterialInput);