import React from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import Orientation from 'react-native-orientation';
import Colors from '../../config/colors';

class ActIndicator extends React.Component {
  state = {
    animating: true
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  componentDidMount = () => {
    Orientation.lockToPortrait();
    this.closeActivityIndicator();
  }
  
  closeActivityIndicator = () => this.setState({ animating: this.props.animating });

  render() {
    const animating = this.state.animating;
    return (
      <View style={styles.container}>
        <ActivityIndicator
          animating={animating}
          color={Colors.activityIndicaterColor}
          size="large"
          style={styles.activityIndicator}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorTransparent,
    paddingVertical: 20,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1000,
    opacity: 0.5
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  }
});

export default ActIndicator;
