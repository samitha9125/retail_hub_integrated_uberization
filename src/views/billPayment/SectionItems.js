import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Picker } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import MaterialInput from './MaterialInput';
import InputSection from './InputSection';
import SectionContainer from './SectionContainer';
import ActIndicator from './ActIndicator';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import strings from '../../Language/BillPayment.js';

const Utill = new Utills();

class SectionItems extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      userData: '',
      phone: '',
      connSelectionOption: 'mobile',
      isLoading: false,
      connection: strings.connction,
      mobile: strings.mobile,
      number: strings.number,
      amount: strings.amount,
      BtnTxt: strings.btnTxt,
      EzTitle: strings.ezTitle,
      ezPin: strings.ezPin,
      invalidDtv: strings.invalidDtv,
      invalidMobile: strings.invalidMobile,
      invalidLte: strings.invalidLte,
      invalidAmount: strings.invalidAmount,
      minimumAmmountValue: strings.minimumAmmountValue,
      maximumAmmountValue: strings.maximumAmmountValue,
      activateBtnTxtColor: Colors.btnDeactiveTxtColor,
      activateBtnColor: Colors.btnDeactive,
      agentMssdn: global.agentMssdn,
      start_ts: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('COMMING FROM PROPS', nextProps);
    if (nextProps.connectionTypeInput == 'DTV' && nextProps.connectionNumberInput.length == 8 && !nextProps.firstValidated) {
      Utill.showAlertMsg(this.state.invalidDtv);
      return;
    }

    if (this.props.connectionTypeInput == 'GSM' && Utill.gsmDigitValidation(nextProps.connectionNumberInput) && !nextProps.firstValidated) {
      Utill.showAlertMsg(this.state.invalidMobile);
      return;
    }

    if (this.props.connectionTypeInput == 'HBB' && (nextProps.connectionNumberInput.length == 9 || nextProps.connectionNumberInput.length == 10) && !nextProps.firstValidated) {
      Utill.showAlertMsg(this.state.invalidLte);
      return;
    }

    if (nextProps.firstValidated && nextProps.amountInput !== '' && (nextProps.amountInput !== this.props.amountInput) && !nextProps.secondValidated) {
      if (nextProps.amountInput > 25000) {
        Utill.showAlertMsg(this.state.maximumAmmountValue);
      } else {
        Utill.showAlertMsg(this.state.invalidAmount);
        return;
      }
    }

    if (nextProps.firstValidated && nextProps.secondValidated && nextProps.thirdValidated) {
      this.setState({ activateBtnTxtColor: Colors.btnActiveTxtColor, activateBtnColor: Colors.btnActive });
    } else {
      this.setState({ activateBtnTxtColor: Colors.btnDeactiveTxtColor, activateBtnColor: Colors.btnDeactive });
    }
  }

  componentDidMount(){
    console.log("In bill payment section item did mount\n" + JSON.stringify(this.props));

  }

  onBillBalanceSuccess = (response) => {
    this.setState({ isLoading: false });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalConfirmBill',
        title: 'GeneralModalConfirmBill',
        passProps: {
          message: response.data.info,
          start_ts: this.state.start_ts
        },
        overrideBackPress: true
      });

  }

  onBillBalanceFailure = (response) => {
    this.setState({ isLoading: false });
    // this.props.getApiResponseMobileAct(response);
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
  }

  onBillBalanceEx = (error) => {
    this.setState({ isLoading: false });
    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });
  }

  onBillPay = () => {
    let end_ts = Math.round((new Date()).getTime() / 1000);

    if (this.props.amountInput < 50) {
      Utill.showAlertMsg(this.state.minimumAmmountValue);
      return;
    }
    const data = {
      ezcash_pin: this.props.ezCashInput,
      conn_ref: this.props.connectionNumberInput,
      conn_type: this.props.connectionTypeInput,
      amount: this.props.amountInput,
      start_ts: this.state.start_ts,
      end_ts: end_ts
    };
    console.log("data:",data);
    this.setState({ isLoading: true });
    Utill.apiRequestPost2('checkBalance', 'bill', 'ccapp', data, this.onBillBalanceSuccess, this.onBillBalanceFailure, this.onBillBalanceEx);
  };

  connectionTypeChanged = (text) => {
    if (this.state.start_ts == '') {
      let start_ts = Math.round((new Date()).getTime() / 1000);
      this.setState({ start_ts: start_ts });
    }
    this
      .props
      .getConnectionNumberBill('');
    this
      .props
      .getConnectionTypeBill(text);
  }

  setStartTime = () =>{
    if (this.state.start_ts == '') {
      let start_ts = Math.round((new Date()).getTime() / 1000);
      this.setState({ start_ts: start_ts });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <InputSection key={1} label={this.state.connection} colorCode/>
        <SectionContainer >
          <Picker
            selectedValue={this.props.connectionTypeInput}
            mode={'dropdown'}
            style={styles.pickerStyle}
            onValueChange={this.connectionTypeChanged}>
            <Picker.Item label="Mobile" value="GSM"/>
            <Picker.Item label="DTV" value="DTV"/>
            <Picker.Item label="HBB" value="HBB"/>
          </Picker>
        </SectionContainer>
        <InputSection
          key={2}
          label={this.state.number}
          colorCode={this.props.firstValidated}/>{this.state.isLoading
          ? <ActIndicator animating/>
          : <View/>}
        <SectionContainer >
          <MaterialInput
            label='Connection No'
            ref={'ConnectionNo'}
            isNeedToFocus
            title=''
            icon={'mode-edit'}
            keyboardType={'numeric'}
            maxLength={this.props.maxLength}
            onTextFieldPress={ ()=> this.setStartTime()}
          />
        </SectionContainer>

        <InputSection
          key={3}
          label={this.state.amount}
          colorCode={this.props.firstValidated && this.props.secondValidated}/>
        <SectionContainer >
          <MaterialInput
            label='Eg: 199 or 199.00'
            ref={'amount'}
            isNeedToFocus
            title='Eg: 199 or 199.00'
            icon={'mode-edit'}
            keyboardType={'numeric'}
            editable={this.props.firstValidated}/>
        </SectionContainer>

        <InputSection
          key={4}
          label={this.state.ezPin}
          colorCode={this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated}/>
        <SectionContainer >
          <MaterialInput
            label='eZ Cash Account PIN'
            ref={'ezCashPin'}
            isNeedToFocus
            title={this.props.defaultEzNumber == "" ? `eZ Cash Account PIN` : `(${this.props.defaultEzNumber}) eZ Cash Account PIN`}
            icon={'mode-edit'}
            keyboardType={'numeric'}
            maxLength={4}
            secureTextEntry
            editable={this.props.firstValidated && this.props.secondValidated}/>
        </SectionContainer>

        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            <TouchableOpacity
              style={[
                styles.payBtn, {
                  backgroundColor: this.state.activateBtnColor
                }
              ]}
              onPress={() => this.onBillPay()}
              disabled={!(this.props.firstValidated && this.props.secondValidated && this.props.thirdValidated)}>
              <Text
                style={[
                  styles.payBtnTxt, {
                    color: this.state.activateBtnTxtColor
                  }
                ]}>
                {this.state.BtnTxt}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {

  console.log("===BILL PAYMENT== state.auth.getConfiguration", state.auth.getConfiguration);

  let firstValidated = false;
  let secondValidated = false;
  let thirdValidated = false;
  let maxLength = '';
  let { conn_check_regex = {} } = state.auth.getConfiguration;
  let { DTV:DTVRegex = "" } = conn_check_regex;

  const regex = /^\d+(.)?(\d{1,2})?$/;

  switch (state.bill.connection_type) {
    case 'DTV':
      Utill.dtvNumValidate(state.bill.connection_number, DTVRegex).length == 8
        ? firstValidated = true
        : firstValidated = false;
      maxLength = 8;
      break;
    case 'GSM':
      firstValidated = Utill.gsmDialogNumvalidate(state.bill.connection_number);
      maxLength = 10;
      break;
    case 'HBB':
      state.bill.connection_number.length == 9
        ? firstValidated = true
        : firstValidated = false;
      maxLength = 10;
      break;
    default:
      return '';
  }

  if (regex.test(state.bill.amount) && state.bill.amount > 0 && state.bill.amount < 25000) {
    secondValidated = true;
  } else {
    secondValidated = false;
  }

  if (state.bill.ez_cash_pin === '' || state.bill.ez_cash_pin.length < 4) {
    thirdValidated = false;
  } else {
    thirdValidated = true;
  }

  console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  console.log(Utill.mobileNumValidateDialog(state.bill.connection_number));
  console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  console.log('firstValidated', 'secondValidated', 'thirdValidated');
  console.log(firstValidated, secondValidated, thirdValidated);

  return {
    language: state.lang.current_lang,
    connectionNumberInput: state.bill.connection_number,
    connectionTypeInput: state.bill.connection_type,
    ezCashInput: state.bill.ez_cash_pin,
    amountInput: state.bill.amount,
    firstValidated,
    secondValidated,
    thirdValidated,
    maxLength
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
    //backgroundColor: Colors.colorBlue
  },
  pickerStyle: {
    flex: 1,
    margin: 0,
    padding: 0,
    marginLeft: 10,
    marginRight: 0,
    borderWidth: 1
  },

  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginLeft: 18,
    marginRight: 18
  },

  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 5,
    marginTop: 8
  },
  payBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnDeactive
  },
  payBtnTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  }
});

export default connect(mapStateToProps, actions)(SectionItems);
