import React from 'react';
import { StyleSheet, View, Alert, BackHandler } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import SectionItems from './SectionItems';
import * as actions from '../../actions';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import { Header } from '../../components/others';
import strings from '../../Language/BillPayment';
import screenTitles from '../../Language/Home';

const Utill = new Utills();

class BillPaymentMain extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      userData: '',
      BillPaymentTitle: screenTitles.BillPay,
      locals: {
        backMessage: strings.backMessage
      }
    };
  }

  componentWillMount() {
    this.props.resetBillPaymentState();
    console.log('### BillPayment :: getConnNumber #####');
    Utill.getConnNumber();
    Orientation.lockToPortrait();
  }

  componentDidMount() {
    console.log('### BillPayment :: componentDidMount #####');
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    console.log("In bill payment screen main did mount\n" + JSON.stringify(this.props.defaultEzNumber));
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    Orientation.lockToPortrait();
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this.props;
    me.resetMobileActivationState();
  }

  render() {
    const { navigator } = this.props;
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.BillPaymentTitle}/>
        <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
          <View style={styles.sectionContainer}>
            <SectionItems
              navigator={navigator}
              defaultEzNumber={this.props.defaultEzNumber}/>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const language = state.lang.current_lang;
  return { language };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  sectionContainer: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 15
  }
});

export default connect(mapStateToProps, actions)(BillPaymentMain);
