import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import * as actions from '../../../actions';
import ActIndicator from '../ActIndicator';
import Utills from '../../../utills/Utills';
import Orientation from 'react-native-orientation';

const Utill = new Utills();

class GeneralModalConfirmBill extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onBnPressOk = () => {
    let end_ts = Math.round((new Date()).getTime() / 1000);

    //Call the same bill payment ajax call
    const data = {
      ezcash_pin: this.props.ezCashInput,
      conn_ref: this.props.connectionNumberInput,
      conn_type: this.props.connectionTypeInput,
      amount: this.props.amountInput,
      isLoading: false,
      start_ts: this.props.start_ts,
      end_ts: end_ts
    };

    this.setState({ isLoading: true });
    Utill.apiRequestPost('doBillPayment', 'bill', 'ccapp', data, this.onBillPaySuccess, this.onBillPayFailure, this.onBillPayEx);

  }

  onBillPaySuccess = (response) => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    this
      .props
      .getApiResponseMobileAct(response);
    this
      .props
      .navigator
      .showModal({ title: 'GeneralModalBill', screen: 'DialogRetailerApp.models.GeneralModalBill', overrideBackPress: true });
  }

  onBillPayFailure = (response) => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: response.data.error
        },
        overrideBackPress: true
      });
  }

  onBillPayEx = (error) => {
    this.setState({ isLoading: false });
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

    this
      .props
      .navigator
      .showModal({
        screen: 'DialogRetailerApp.models.GeneralModalException',
        title: 'GeneralModalException',
        passProps: {
          message: error
        },
        overrideBackPress: true
      });

  }

  onBnPressCancel = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  componentWillUnmount(){
    Orientation.lockToPortrait();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View>
              <Text style={styles.descriptionText}>{this.props.message}</Text>
            </View>
            {this.state.isLoading
              ? <ActIndicator animating/>
              : <View/>}
            <View style={styles.bottomCantainerBtn}>
              <TouchableOpacity
                onPress={() => this.onBnPressCancel()}
                style={styles.bottomBtnLeft}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'Cancel'}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onBnPressOk()} style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {'OK'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const apiResponse = state.mobile.api_response;
  return { apiResponse, connectionNumberInput: state.bill.connection_number, connectionTypeInput: state.bill.connection_type, ezCashInput: state.bill.ez_cash_pin, amountInput: state.bill.amount };

};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },
  topContainer: {
    flex: 0.3
  },
  modalContainer: {
    flex: 1
  },

  bottomContainer: {
    flex: 0.5
  },

  innerContainer: {
    backgroundColor: Colors.backgroundColorWhite,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  descriptionText: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: '100'

  },

  bottomCantainerBtn: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 15
  },

  bottomBtnLeft: {
    width: 80,
    alignSelf: 'flex-start'
  },

  bottomBtn: {
    width: 45,
    alignSelf: 'flex-end'

  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.btnFontSize,
    color: Colors.colorGreen

  }

});

export default connect(mapStateToProps, actions)(GeneralModalConfirmBill);
