import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/MobileActivaton.js';
import Orientation from 'react-native-orientation';

class ConfirmModal extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      package: [{
        data: 'NIC/Passport No',
        value: this.props.confirmationData.NICPassportNo
      }, {
        data: 'Connection No',
        value: this.props.confirmationData.mobileNumber
      }, {
        data: 'SIM No',
        value: this.props.confirmationData.simNumber
      }
      ],
      locals: {
        confirmButtonTxt: 'CONFIRM',
        cancelButtonTxt: 'CANCEL',
        modalTitle: 'Confirmation'
      }
    };
  }

  
  componentWillMount(){
    Orientation.lockToPortrait();
  }

  dismissModal = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  openSignature = () => {
    console.log('Open Signature');
    this.dismissModal();
    setTimeout(()=>{
      this.props.showSignature();
    },500);
    // Orientation.lockToLandscapeLeft();
    // 
    // if (this.props.isExsistingCustomer) {
    //   console.log('finalActivationMethod');
    //   this
    //     .props
    //     .finalActivationMethod();

    // } else {
    // const navigatorOb = this.props.navigator;

    
    // }
  }

  componentWillUnmount(){
    // if (this.props.onUnmount !== undefined){
    //   this.props.onUnmount();
    // }
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => this.dismissModal()}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => this.openSignature()}>
          <Text style={styles.textStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value}/>)

  render() {
    const finalPakageDetails = this.state.package;
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.containerStyle}>
          <View style={styles.section_1_Container}>
            <Text style={styles.titleStyle}>{this.state.locals.modalTitle}</Text>
          </View>
          <View style={styles.section_2_Container}>
            <FlatList
              data={finalPakageDetails}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={true}/>
          </View>
          <View style={styles.section_2_1_Container}/>
          <View style={styles.section_4_Container}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => (
  <View style={styles.viewStyle}>
    <View style={styles.elementLeftItem}>
      <Text style={[styles.textStyleLeft]}>{data}</Text>
    </View>
    <View style={styles.elementrightItem}>
      <Text style={styles.textStyleRight}>{value}</Text>
    </View>
  </View>
);

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
    flex: 0.5,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 35,
    marginTop: 20,
    width: Dimensions.get('window').width - 40,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    flex: 2
  },
  section_2_Container: {
    flex: 6
  },
  section_2_1_Container: {
    flex: 0.1,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    marginBottom: 10,
    marginTop: 10,
    // backgroundColor: 'red'
  },
  section_3_Container: {
    flex: 3,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1
  },

  elementLeftItem: {
    flex: 0.9

  },
  elementrightItem: {
    flex: 1
  },
  viewStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.defaultFontSize,
    color: Colors.colorBlack,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '400'
  },

  textStyleRight: {
    fontSize: Styles.defaultFontSize,
    color: Colors.colorBlack
  },
  textStyleLeft: {
    fontWeight: '500',
    fontSize: Styles.defaultFontSize,
    color: Colors.colorBlack

  },
  titleStyle: {
    fontSize: 20,
    color: Colors.colorBlack,
    fontWeight: '500',
    marginBottom:30
  },
  confirmButtonStyle: {
    flex:0.4,
    backgroundColor: '#FDC32D',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  cancelButtonStyle: {
    flex:0.4,
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: 5,
    marginLeft: 5,
    padding: 10
  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
    // marginBottom: 15
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION', state.mobile);
  const language = state.lang.current_lang;
  const local_foreign = 'local';
  const idNumber= '9135585555v';
  const connectionNo = '033123456';
  const simNumber = '5855255855555';
  const offer = 'Standard';
  const dataPacks = 'Lite Plus';
  const pkgRental = 'Rs 599';
  const contactNumber = '0777123456';
  const email = 'email';
  const connectionFee = 'Rs 5000';
  const depositAmount = 'Rs 300';
  const totalPayment = 'RS 5300';


  return {
    language,
    local_foreign,
    idNumber,
    connectionNo,
    simNumber,
    offer,
    dataPacks,
    pkgRental,
    contactNumber,
    email,
    connectionFee,
    depositAmount,
    totalPayment
  };
};

export default connect(mapStateToProps, actions)(ConfirmModal);