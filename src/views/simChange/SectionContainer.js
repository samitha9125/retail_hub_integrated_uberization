import React from 'react';
import { StyleSheet, View } from 'react-native';
import Colors from '../../config/colors';

class SectionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  render() {
    const { customStyle, children } = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.placeholderView}></View>
        <View style={[styles.container, customStyle]}>
        
          {children}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:7,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 4,
    marginTop: 4,
    // marginLeft: 45,
    marginRight: 5,
    backgroundColor: Colors.appBackgroundColor
  },
  mainContainer:{ 
    flex:1, 
    flexDirection:'row' 
  },
  placeholderView:{ 
    flex:0.7, 
  }
});

export default SectionContainer;