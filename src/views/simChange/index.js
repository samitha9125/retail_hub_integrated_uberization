import React from 'react';
import { StyleSheet, View, Alert, BackHandler } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Orientation from 'react-native-orientation';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import SectionItems from './SectionItems';
import Colors from '../../config/colors';
import { Header } from '../../components/others';
import screenTitles from '../../Language/Home.js';
import strings from '../../Language/SimChange.js';
class SimChangeMain extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      userData: '',
      SimChangeTitle: screenTitles.SimChangeTit,
      locals: {
        backMessage: strings.backMessage
      }
    };
  }

  componentWillMount() {
    Orientation.lockToPortrait();
    this
      .props
      .resetSimChangeState();
    this
      .props
      .resetMobileActivationState();

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    Orientation.lockToPortrait();
  }

  handleBackButtonClick = () => {
    this.okHandler();  
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
    const me = this.props;
    me.resetMobileActivationState();
    me.getSignatureMobileAct(null);
    me.genaralSetSimNumber('');
  }

  render() {
    const { navigator } = this.props;
    return (
      <View style={styles.container}>
      
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.SimChangeTitle}/>
        
        <KeyboardAwareScrollView style={styles.keyboardAwareScrollViewContainer}>
          <View style={styles.sectionContainer}>
            <SectionItems navigator={navigator}/>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const language = state.lang.current_lang;
  const apiLoading = state.auth.api_loading;

  return { language, apiLoading };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  sectionContainer: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor,
    paddingTop: 15
  }
});

export default connect(mapStateToProps, actions)(SimChangeMain);
