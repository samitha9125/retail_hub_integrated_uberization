import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert, Image, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import MaterialInput from './MaterialInput';
import InputSection from './InputSection';
import SectionContainer from './SectionContainer';
import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import strings from '../../Language/SimChange';
import Analytics from '../../utills/Analytics';
import CheckBox from 'react-native-check-box';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import ActIndicator from './ActIndicator';
import Constants from '../../config/constants';
const Utill = new Utills();
const reg = /^[a-zA-Z0-9]+$/;


class SectionItems extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      userLogged: true,
      userData: '',
      phone: '',
      connSelectionOption: 'mobile',
      sim_serial: '',
      material_code: '',
      isLoading: false,
      loginBtnTap: false,
      buttonSimchangeText: strings.ButtonTxt,
      scanSim: strings.ScanSim,
      nicPassport: strings.NicPassport,
      mobileNo: strings.MobileNo,
      invalidNic: strings.InvalidNic,
      invalidMobile: strings.InvalidMobile,
      activateBtnTxtColor: Colors.btnDeactiveTxtColor,
      activateBtnColor: Colors.btnDeactive,
      locals:{
        customerSignature: strings.customerSignature,
        changeSimButtonText: strings.changeSimButtonText,
        labelSimNo: strings.labelSimNo,
        labelScanSimSerial: strings.labelScanSimSerial,
        label_mobile_number: strings.label_mobile_number,
        label_customer_validation: strings.label_customer_validation,
        label_customer_nic_and_passport_no: strings.label_customer_nic_and_passport_no,
        label_product_information: strings.label_product_information,
        checkbox_customer_nic_check:strings.checkbox_customer_nic_check,
        submit_valid_passport: strings.submit_valid_passport,

      },
      start_ts:'',
      isOriginalNICChecked: false,
      NICPassportNo: '',
      mobileNumber: '',
      simNo:'',
      mobileNumberValidated: false,
      isValidCustomer: false
    };

  }

  componentWillReceiveProps(nextProps) {
    console.log('COMPONENT SECTION ITEMS ', nextProps);
    if (nextProps.simNumber == ''){
      nextProps.getSignatureMobileAct(null);
    }
  }

  onPressSIM(){
    console.log('xxx onPressSIM');
    this
      .props
      .navigator
      .push({
        title: 'SCAN SIM',
        screen: 'DialogRetailerApp.views.BarcodeScannerCommonScreen'
      });
  }

  onSimSuccess = (response) => {
    // this.setState({ isLoading: false });
    this.props.getApiLoading(false);
    Analytics.logEvent('dsa_sim_change_success');

    this.uploadImages(response.data.data.signature_id);
    this
      .props
      .getApiResponseMobileAct(response);

    this
      .props
      .navigator
      .showModal({ title: 'GeneralModalSimChange', screen: 'DialogRetailerApp.models.GeneralModalSimChange', overrideBackPress: true });
    
    this.setState({ isLoading: false });
  }

  uploadImages = (uniqueId) => {
    console.log('xxx uploadImages');
    if (this.props.signature.pathName !== null) {
      this.addToCaptureDb(uniqueId, Constants.imageCaptureTypes.SIGNATURE, this.props.signature.signatureUri);
    } else {
      Utill.showAlertMsg('Error in Uploding Images');
    }
  }

  onSimFailure = (response) => {
    this.setState({ isLoading: false });
    Analytics.logEvent('dsa_sim_change_fail');
    this.showAlert(response.data.error);
  }

  onSimEx = (response) => {
    this.setState({ isLoading: false });
    Analytics.logEvent('dsa_sim_change_ex');
    this.showAlert(response.data.error);
  }

  getTermAndCondUrl = () => {
    return 'https://www.dialog.lk/tc';
  }

  onCaptureSignature(){
    const navigatorOb = this.props.navigator;
    console.log('xxx navigatorOb', navigatorOb);

    let data = {
      NICPassportNo: this.state.NICPassportNo,
      mobileNumber: this.state.mobileNumber,
      simNumber: this.props.simNumber
    };
    
    navigatorOb.showModal({
      title: 'Confirm',
      screen: 'DialogRetailerApp.views.SimChangeConfirmModal',
      overrideBackPress: true,
      passProps: {
        onUnmount: () => {
          // self.dismissModal();
        },
        showSignature: () => {
          navigatorOb.push({
            title: '',
            screen: 'DialogRetailerApp.views.SignaturePadCommonScreen',
            passProps: {
              downlodUrl: this.getTermAndCondUrl()
            }
          });
        },
        confirmationData: data
      }
    });
  }

  showAlert = (msg) => {
    Alert.alert('', msg);
  };

  addToCaptureDb = async (txnId, imageId, imageUri) => {
    console.log('xxx addToCaptureDb');
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id1 = `${txnId}_${imageId}`;
      const url1 = Utill.createApiUrl('captureSignature', 'sim', 'ccapp');
      const imageFile1 = imageUri.replace('file://', '');
      const uploadSelector1 = 'encrypt'; 

      const options = {
        id: id1,
        url: url1,
        imageFile: imageFile1,
        uploadSelector: uploadSelector1,
        deviceData: JSON.stringify(deviceData)
      };

      const errorCb = (msg) => {
        console.log('xxxx addToCaptureDb errorCb ');
        console.log(msg);
      };
      const sucessCb = (msg) => {
        console.log('xxxx addToCaptureDb sucessCb');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, sucessCb);
    } catch (e) {
      console.log('addToCaptureDb Image Uploading Failed', e.message);
      Utills.showAlertMsg(e.message);
    }
  }

  validateMobileNumber = (mobileNo) =>{

    this.clearProductInfoSection();

    let allFieldsValidated = this.state.isOriginalNICChecked == true && this.state.NICPassportNo.length > 4;
    let isValidMobileNumber = Utill.gsmDialogNumvalidate(mobileNo);

    console.log("allFieldsValidated: ", allFieldsValidated,"\n");
    console.log("mobileNumberValidated: ", isValidMobileNumber,"\n");

    let mobileNumberValidated = allFieldsValidated == true && isValidMobileNumber == true;

    this.setState({ mobileNumberValidated: mobileNumberValidated, mobileNumber: mobileNo },()=>{
      if (mobileNumberValidated == true) {
        Keyboard.dismiss();
        this.doCustomerValidation();
      }
    });

  }

  doCustomerValidation = () =>{

    if (!reg.test(this.state.NICPassportNo)) {
      this.showAlert(this.state.locals.submit_valid_passport);
      return;
    } 

    let allFieldsValidated = this.state.NICPassportNo.length > 4;
    let isValidMobileNumber = Utill.gsmDialogNumvalidate(this.state.mobileNumber);
    let mobileNumberValidated = allFieldsValidated == true && isValidMobileNumber == true;

    if (mobileNumberValidated == true) {
      this.props.getApiLoading(true);
      // if (this.state.mobileNumberValidated == true) {
      const data = {
        mobile_number: this.state.mobileNumber,
        id_number: this.state.NICPassportNo
      };
      this.setState({ isLoading: true });
      Utill.apiRequestPost('validateCustomer', 'sim', 'ccapp', data, this.onCustomerValidationSuccess, this.onCustomerValidationFailure, this.onCustomerValidationSimEx);
    }
  }

  onCustomerValidationSuccess = (response) =>{
    this.setState({ isLoading: false, isValidCustomer: true });
    this.props.getApiLoading(false);
    console.log("Customer Validation Success: \n", JSON.stringify(response));
  }

  onCustomerValidationFailure = (response) =>{
    this.setState({ isLoading: false, isValidCustomer:false },()=>{
      const me = this.props;
      me.resetMobileActivationState();
      me.getSignatureMobileAct(null);
      me.genaralResetSimApiStatus();
    });
    this.showAlert(response.data.error);
    this.props.getApiLoading(false);
    console.log("Customer Validation Fail: \n", JSON.stringify(response));
  }

  clearProductInfoSection = () => {
    console.log('Clear product info');
    this.setState({ isLoading: false, isValidCustomer:false },()=>{
      const me = this.props;
      me.resetMobileActivationState();
      me.getSignatureMobileAct(null);
      me.genaralResetSimApiStatus();
    });
  }

  onCustomerValidationSimEx = (response) =>{
    this.setState({ isLoading: false, isValidCustomer:false },()=>{
      const me = this.props;
      me.resetMobileActivationState();
      me.getSignatureMobileAct(null);
      me.genaralResetSimApiStatus();
    });
    this.props.getApiLoading(false);
    console.log("Customer Validation Ex: \n", JSON.stringify(response));
  }

  checkOriginalDocuments = () => {
    let start_ts = Math.round((new Date()).getTime() / 1000);

    if (this.state.isOriginalNICChecked == true){
      this.setState({ 
        start_ts:'',
        NICPassportNo: '',
        mobileNumber: '',
        simNo:'',
        mobileNumberValidated: false,
        isValidCustomer: false },()=>{
        const me = this.props;
        me.resetMobileActivationState();
        me.getSignatureMobileAct(null);
        me.genaralResetSimApiStatus();
      });
    }
    this.setState({ isOriginalNICChecked: !this.state.isOriginalNICChecked, start_ts: start_ts });
  };

  doSimChange = () => {
    if (this.state.isLoading == true){
      return;
    }
    let end_ts = Math.round((new Date()).getTime() / 1000);
    let material_code = (this.props.material_code.material_code !== undefined && this.props.material_code.material_code) ? 
      this.props.material_code.material_code : '';
    let sim_serial_number = (this.props.material_code.sim_serial_number!== undefined  && this.props.material_code.sim_serial_number) ? 
      this.props.material_code.sim_serial_number : '';

    console.log('doSimChange');
    const data = {
      material_code: material_code,
      sim_serial_number: sim_serial_number,
      mobile_number: this.state.mobileNumber,
      id_number: this.state.NICPassportNo,
      start_ts: this.state.start_ts,
      end_ts: end_ts
    };

    console.log("Sim Change params: ", data);
    this.setState({ isLoading: true });
    Utill.apiRequestPost('doSimChange', 'sim', 'ccapp', data, this.onSimSuccess, this.onSimFailure, this.onSimEx);
  };

  mobileNumberLengthLimit(){
    if (this.state.mobileNumber[0] == 0) {
      return 10;
    }
    return 9;
  }

  validateNICPassport(text){
    if (!reg.test(text)) {
      this.debounce( this.showAlert(this.state.locals.submit_valid_passport), 1000);
    } 

    this.setState({ NICPassportNo: text.replace(/[^a-zA-Z0-9]/g, ''), mobileNumber: '' },()=>{
      this.clearProductInfoSection();
    });
  }

  /**
   * @description A custom debounce method to throttle user input in Search Bar
   * @param {Function} fn
   * @param {Number} delay
   * @returns
   * @memberof ActivationStatusMainView
   */
  debounce = (fn, delay) => {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  }

  render() {

    let shouldEnableNICPassport = this.state.isOriginalNICChecked == true;
    let shouldEnableMobileNumber = this.state.NICPassportNo.length>4 && reg.test(this.state.NICPassportNo);
    let mobileNumberValidated = this.state.isValidCustomer;
    let shouldEnableSimChange = (this.props.signature !== null && this.props.signature !== undefined) && this.props.simNumber;

    return (
      <View style={styles.container}>
        {this.state.isLoading ? 
          <ActIndicator animating/>   
          : true}
           
        {/* NIC INPUT SECTION START */}
        <InputSection
          key={1}
          label={this.state.locals.label_customer_validation}
          validationStatus={mobileNumberValidated}/>
        <SectionContainer> 
          <ElementCheckBox
            checkBoxText={this.state.locals.checkbox_customer_nic_check}
            onClick={() => this.checkOriginalDocuments()}
            isChecked={this.state.isOriginalNICChecked}/> 
        </SectionContainer>
        <SectionContainer >
          <MaterialInput
            label={this.state.locals.label_customer_nic_and_passport_no}
            index={'idNumber'}
            ref={'idNumber'}
            onSubmitEditing={ text =>{ this.doCustomerValidation(); } }
            onBlur={ text =>{ this.doCustomerValidation(); } }
            editable={shouldEnableNICPassport}
            value={this.state.NICPassportNo}
            onChangeText={text =>{ this.validateNICPassport(text); }}
            // onIconPress={() => this.onPressValidatePassport()}
            customStyle={styles.materialInputCustomStyle}
            // icon={Constants.icon.next}
            // disableIconTap={false}
            hideRightIcon={true}/>
        </SectionContainer>
        {/* NIC INPUT SECTION END */}

        <SectionContainer >
          <MaterialInput
            label={this.state.locals.label_mobile_number}
            index={'mobileNumber'}
            ref={'mobileNumber'}
            maxLength={this.mobileNumberLengthLimit()}
            editable={ shouldEnableMobileNumber }
            value={this.state.mobileNumber}
            onChangeText={text=>{ this.validateMobileNumber(text); }}
            // onIconPress={() => this.onPressValidatePassport()}
            customStyle={styles.materialInputCustomStyle}
            // icon={Constants.icon.next}
            // disableIconTap={false}
            hideRightIcon={true}
            keyboardType={'numeric'}/>
        </SectionContainer>
        <InputSection
          key={2}
          label={this.state.locals.label_product_information}
          validationStatus={this.props.sectionId_validation_status}/>
        { mobileNumberValidated == true ?
          <SectionContainer >
            <MaterialInput
              label={this.props.simNumber == '' ? this.state.locals.labelScanSimSerial : this.state.locals.labelSimNo}
              index={'simSerial'}
              ref={'simSerial'}
              maxLength={8}
              editable={false}
              value={this.props.simNumber}
              onIconPress={()=>{this.onPressSIM();}}
              customStyle={styles.materialInputCustomStyle}
              lineWidth={this.props.simNumber == '' ? 0 : StyleSheet.hairlineWidth }
              icon={{
                name: '',
                iconType: '',
                id: 'barcode',
                defaultFontSize: 40
              }}
              disableIconTap={false}
            // isFocused = { this.state.isFocused }
            />
          </SectionContainer>
          : 
          <SectionContainer> 
            <View style={ styles.SIMScanPlaceHolder } ></View> 
          </SectionContainer> }
        {/* SIGNATURE BUTTON START */}
        {/* <SectionContainer> */}
        <View style={styles.signatureView}>
          <TouchableOpacity
            style={styles.signatureContainer}
            onPress={() => this.onCaptureSignature()}
            disabled={this.props.simNumber == ''}
          >

            {this.props.signature !== null && this.props.signature !== undefined
              ? <Image
                style={styles.signatureImage}
                key={this.props.signature.signatureRand}
                source={{
                  uri: `data:image/jpg;base64,${this.props.signature.signatureBase}`
                }}
                resizeMode="stretch"
              />
              : 
              <Text style={styles.signatureTxt}>
                {this.state.locals.customerSignature}
              </Text>
            } 
          </TouchableOpacity>
        </View>
        {/* </SectionContainer> */}

        <View style={styles.bottomContainer}>
          <View style={styles.payBtnBtnContainer}>
            <TouchableOpacity
              style={[ shouldEnableSimChange == false ? styles.payBtn : styles.payBtnEnabled ]}
              onPress={() => this.doSimChange()}
              disabled={shouldEnableSimChange == false}>
              <Text
                style={[ shouldEnableSimChange == false ? styles.payBtnTxt : styles.payBtnTxtEnabled ]}>
                {this.state.locals.changeSimButtonText}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View> 
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: Sim Change => genaral\n', state.genaral);
  console.log('****** REDUX STATE :: Sim Change => mobile\n', state.mobile);
  const language = state.lang.current_lang;
  const simNumber = state.genaral.simNumber;
  const signature = state.mobile.signature;
  const material_code = state.genaral.material_code;

  return {
    language, 
    simNumber: simNumber,
    signature: signature,
    material_code: material_code 
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },
  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    // margin: 10,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
    //backgroundColor: '#808080'
  },
  signatureTxt: {
    color: Colors.black,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  },
  materialInputCustomStyle :{
    marginTop: -20
  },
  SIMScanPlaceHolder:{
    margin:40
  },
  //Check box
  checkBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 8,
    marginTop: 8,
    paddingLeft:-6,
    backgroundColor: Colors.appBackgroundColor,
  },
  checkBoxView: {
    flex:0.1,
    // alignItems: 'center',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  checkBoxTextView: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: 5,
    //backgroundColor: 'green'
  },
  checkBoxText: {
    fontSize: 15,
    paddingBottom: 3,
    paddingLeft: 5,
    color: Colors.colorBlack
  },
  bottomContainer: {
    flex: 1,
    marginTop: 5,
    height: 100,
    marginLeft: 15, 
    marginRight: 5
  },
  checkBoxStyle:{
    left: 0
  },
  payBtnBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    // margin: 5,
    marginTop: 5
  },
  payBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // padding: 10,
    borderRadius: 3,
    backgroundColor: Colors.btnDeactive
  },
  payBtnEnabled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // padding: 10,
    // borderRadius: 5,
    backgroundColor: Colors.btnActive
  },
  payBtnTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  },
  payBtnTxtEnabled:{
    color: Colors.colorBlack,
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500'
  },
  signatureView:{ 
    marginLeft: 15, 
    marginRight: 5 
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  }
});

const ElementCheckBox = ({ checkBoxText, onClick, isChecked }) => (
  <View style={styles.checkBoxContainer}> 
    <View style={styles.checkBoxView}>
      <CheckBox checkBoxColor={isChecked == true ? Colors.yellow : Colors.grey} style={styles.checkBoxStyle} onClick={onClick} isChecked={isChecked}/>
    </View>
    <View style={styles.checkBoxTextView}>
      <Text style={styles.checkBoxText}>
        {checkBoxText}
      </Text>
    </View>
  </View>
);

export default connect(mapStateToProps, actions)(SectionItems);
