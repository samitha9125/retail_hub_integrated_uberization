import React from 'react';
import { StyleSheet, View, TouchableOpacity, Keyboard, Image } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import simIcon from '../../../images/icons/barcode.png';

class MaterialInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  render() {
    const {
      customStyle,
      ref,
      label,
      icon,
      onIconPress,
      onChangeText,
      editable,
      disableIconTap,
      keyboardType,
      maxLength,
      secureTextEntry,
      isFocused,
      hideRightIcon,
      value,
      lineWidth,
      onSubmitEditing,
      onBlur
    } = this.props;

    const keyboardAwareOnPress = function () {
      // console.log('refTextInput', refTextInput);
      // onIconPress.apply(this, arguments);
      Keyboard.dismiss();
      onIconPress();
    };

    let rightIcon;
    let refTextInput = ref;

    if (!hideRightIcon) {
      if (icon.iconType === 'MaterialCommunityIcons') {
        rightIcon = (<MaterialCommunityIcons
          name={icon.id}
          size={icon.defaultFontSize}
          color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'Ionicons') {
        rightIcon = (<Ionicons name={icon.id} size={26} color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'FontAwesome') {
        rightIcon = (<FontAwesome name={icon.id} size={26} color={Colors.defaultIconColorBlack}/>);
      } else if (icon.iconType === 'Entypo') {
        rightIcon = (<Entypo name={icon.id} size={24} color={Colors.defaultIconColorBlack}/>);
      } else if (icon.id === 'barcode') {
        rightIcon = (<Image source={simIcon} style={styles.iconEye}/>);
      } else {
        rightIcon = (<MaterialIcons name={icon.id} size={26} color={Colors.defaultIconColorBlack}/>);
      }
    } else {
      rightIcon = (true);
    }

    return (
      <View style={[styles.container, customStyle]}>
        <View style={styles.innerContainer}>
          <View style={styles.textFieldStyle}>
            <TextField
              title={this.props.title}
              label={label}
              value={value}
              editable={editable}
              ref={(refVal) => {
                refTextInput = refVal;
                isFocused && refVal !== null
                  ? refVal.focus()
                  : true;
              }}
              onSubmitEditing={onSubmitEditing}
              onBlur={onBlur}
              secureTextEntry={secureTextEntry}
              onChangeText={onChangeText}
              keyboardType={keyboardType}
              maxLength={maxLength}
              lineWidth={lineWidth}/>
          </View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={keyboardAwareOnPress}
            disabled={disableIconTap}>
            {rightIcon}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log('****** REDUX STATE :: MaterialInput => LTE ', state.lteActivation, ownProps);
  const someValue = 'someValue';
  return { someValue };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 2,
    marginLeft: 2,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    //backgroundColor: 'green'
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
    //backgroundColor: 'yellow'
  },
  textFieldStyle: {
    flex: 9,
    justifyContent: 'flex-start'
  },
  imageStyle: {
    flex: 1.5,
    justifyContent: 'flex-end',
    margin: 8,
    alignItems: 'flex-end'

  },
  iconEye: {
    width: 30,
    height: 30
  }
});

export default connect(mapStateToProps, actions)(MaterialInput);