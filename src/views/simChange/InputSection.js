import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../config/colors';
import Styles from '../../config/styles';

class InputSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sectionClick: false,
      deafaultState: true
    };
  }

  render() {
    const {
      customStyle,
      label,
      validationStatus,
      onPress = Keyboard.dismiss
    } = this.props;
    let editIcon;
    if (validationStatus) {
      editIcon = (<Ionicons
        name='ios-checkmark-circle-outline'
        size={Styles.statusIconSize}
        color={Colors.statusIconColor.success}/>);
    } else {
      editIcon = (<Ionicons
        name='ios-checkmark-circle-outline'
        size={Styles.statusIconSize}
        color={Colors.statusIconColor.pending}/>);
    }

    return (
      <TouchableOpacity style={[styles.container, customStyle]} onPress={onPress}>
        <View style={[styles.innerImageContainer]}>
          {editIcon}
        </View>
        <View style={[styles.innerTextContainer]}>
          <Text style={styles.innerText}>{label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 2,
    marginTop: 3,
    marginRight: 2,
    // backgroundColor: Colors.green,
  },
  innerImageContainer: {
    flex: 0.7,
    paddingBottom: 8,
    paddingTop: 8,
    backgroundColor: Colors.appBackgroundColor,
    // backgroundColor: Colors.colorRed,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  innerTextContainer: {
    flex: 7,
    paddingBottom: 8,
    paddingTop: 8,
    paddingLeft: 1,
    backgroundColor: Colors.appBackgroundColor,
    // backgroundColor: Colors.colorBlue,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  innerText: {
    color: Colors.btnActiveTxtColor,
    fontSize: Styles.mobileAct.defaultBtnFontSize,
    fontWeight: '500'
  }

});

export default InputSection;