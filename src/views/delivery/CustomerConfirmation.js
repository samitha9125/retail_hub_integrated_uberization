import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Constants from '../../config/constants';
import Utills from '../../utills/Utills';
import strings from '../../Language/Delivery';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import Orientation from 'react-native-orientation';

const Utill = new Utills();

class CustomerConfirmation extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      apiLoading: false,
      locals: {
        screenTitle: 'IN-PROGESS WORK ORDER',
        customerConfirmation: 'Customer Confirmation',
        customerSignature: 'Customer Signature',
        backMessage: 'Do you want to cancel the operation & go back?',
        please_capture_the_signature: 'Please capture Customer Signature',
        idNumber: 'NIC No',
        continue: 'ACTIVATE',
        cancel: 'CANCEL'
      },
      workOrderDetails: []
    };
  }

  setIndicater = (loadingState = false) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });
  }

  setWorkorderDetails = (aditionalData) => {
    console.log('xxx setWorkorderDetails', aditionalData);
    let dup_array = [];
    for (var i = 0, len = aditionalData.length; i < len; ++i) {
      dup_array[i] = {
        name: aditionalData[i].name,
        value: aditionalData[i].value
      };
    }
    dup_array.push({ name: 'SIM Number', value: this.props.simNumber });
    console.log('xxx setWorkorderDetails', dup_array);
    this.setState({ workOrderDetails: dup_array });
  }

  componentWillMount() {
    Keyboard.dismiss();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setWorkorderDetails(this.props.wom_api_order_addionalDetails);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setIndicater(nextProps.showLoadingIndicater);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    this.props.getResetImagesMobileAct();
    this.props.genaralResetSimApiStatus();
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({
      title: 'IN-PROGESS WORK ORDER',
      screen: 'DialogRetailerApp.views.CustomerVerificationScreen',
      passProps: {
        workOrdrState: 2,
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId,
        workOrderDetails: this.props.wom_api_order_details,
        aditionalData: this.props.wom_api_order_addionalDetails,
        customerDetail: this.props.wom_api_order_customerDetails
      }
    });
  }

  exCb = (error) => {
    console.log('exCb', error);
    this.setIndicater(false);
    Utill.showAlertMsg('System Error');
  }

  finalActivationCxCb = (error) => {
    console.log('finalActivationCxCb', error);
    this.setIndicater(false);
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      title: '',
      screen: 'DialogRetailerApp.modals.ActivateAlertModal',
      overrideBackPress: true,
      passProps: {
        workOrderNo: this.props.currentWorkOrderId,
        job_id: this.props.currentWorkOrderJobId,
        error: error,
        reTryMethod: this.finalActivation
      }
    });
  }
  finalActivationSuccessCb = (response) => {
    console.log('finalActivationSuccessCb');
    this.setIndicater(false);
    console.log('xxx finalActivationSuccessCb', response.data);
    console.log('xxx finalActivationSuccessCb :: info', response.data.info);
    const navigatorOb = this.props.navigator;
    this.uploadImages(response.data.data.order_no);
    navigatorOb.showModal({
      title: 'General Alert Modal',
      screen: 'DialogRetailerApp.modals.GeneralAlertModal',
      overrideBackPress: true,
      passProps: {
        info: response.data.info,
        successText: 'Connection No : ' + this.props.currentWorkOrderBasicInfo.accountNo,
        response: response.data,
        messageType: 'success',
        onOkPress: this.showFeedbackScreen
      }
    });
  }

  showFeedbackScreen = () => {
    console.log('showFeedbackScreen');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.push({ title: 'FEEDBACK', screen: 'DialogRetailerApp.views.FeedbackScreen', passProps: {} });
  }

  //Common Failure callback for  getOrderDetails
  finalActivationFailureCb = (response) => {
    console.log('finalActivationFailureCb');
    this.setIndicater(false);
    console.log('xxx finalActivationFailureCb', response);
    const error = response.data.error;
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      title: '',
      screen: 'DialogRetailerApp.modals.ActivateAlertModal',
      overrideBackPress: true,
      passProps: {
        workOrderNo: this.props.currentWorkOrderId,
        job_id: this.props.currentWorkOrderJobId,
        error: error,
        reTryMethod: this.finalActivation
      }
    });
  }

  //Final Activation Call
  finalActivation = () => {
    console.log('xxx finalActivation');
    if (!this.props.signatureCaptured) {
      Utill.showAlertMsg(this.state.locals.please_capture_the_signature);
      return;
    }
    const { sim_serial_number ='', material_code = '' } = this.props.materialCodeOb;
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissAllModals({
      animationType: 'slide-down'  
    });
    this.setIndicater(true);
    const data = {
      order_id: this.props.currentWorkOrderId,
      job_id: this.props.currentWorkOrderJobId,
      sla_breached: this.props.currentWorkOrderSlaBreachStatus
        ? 1
        : 0,
      sim_serial_no: sim_serial_number,
      sap_material_code: material_code,
      pmsId: this.props.currentWorkOrderBasicInfo.pmsId,
      pos_invoice_no: this.props.currentWorkOrderBasicInfo.pos_invoice_no,
      order_line_item_no: this.props.currentWorkOrderBasicInfo.order_line_item_no,
      issued_user: this.props.currentWorkOrderBasicInfo.issued_user,
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id,
      wom_sap_material_code: this.props.currentWorkOrderBasicInfo.wom_sap_material_code,
    };
    Utill.apiRequestPost('updateCFoss', 'womDelivery', 'delivery', data, this.finalActivationSuccessCb, this.finalActivationFailureCb, this.finalActivationCxCb);
  }

  uploadImages = (uniqueId) => {
    console.log('xxx uploadImages');
    if (this.props.signature.pathName !== null) {
      this.addToCaptureDb(uniqueId, Constants.imageCaptureTypes.SIGNATURE, this.props.signature.signatureUri);
    } else {
      Utill.showAlertMsg('Error in Uploading Images');
    }
  }

  addToCaptureDb = async (txnId, imageId, imageUri) => {
    console.log('xxx addToCaptureDb');
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id1 = `${txnId}_${imageId}`;
      const url1 = Utill.createApiUrl('imageUploadEncResubmit', 'initAct', 'ccapp');
      const imageFile1 = imageUri.replace('file://', '');
      const uploadSelector1 = 'encrypt';

      const options = {
        id: id1,
        url: url1,
        imageFile: imageFile1,
        uploadSelector: uploadSelector1,
        deviceData: JSON.stringify(deviceData)
      };

      const errorCb = (msg) => {
        console.log('xxxx addToCaptureDb errorCb ');
        console.log(msg);
      };
      const successCb = (msg) => {
        console.log('xxxx addToCaptureDb successCb');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, successCb);
    } catch (e) {
      console.log('addToCaptureDb Image Uploading Failed', e.message);
      Utill.showAlertMsg(e.message);
    }
  }

  getTnCurl = () => {
    console.log('getTnCurl', this.props.currentWorkOrderBasicInfo.saleType);
    let returnUrl;
    if (this.props.currentWorkOrderBasicInfo.saleType == 'GSM_PREPAID') {
      returnUrl = 'https://www.dialog.lk/dialogdocroot/content/pdf/tc/pre-english.pdf';
      console.log('GSM_PREPAID');
    } else {
      returnUrl = 'https://www.dialog.lk/dialogdocroot/content/pdf/tc/postpaid-application-form-mob' +
          'ile.pdf';
      console.log('GSM_POSTPAID');
    }
    return returnUrl;
  }

  onCaptureSignature = () => {
    console.log('xxxx onCaptureSignature');
    Orientation.lockToLandscape();    

    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.SignaturePadCommonScreen',
      passProps: {
        onReturnView: 'DialogRetailerApp.views.CustomerConfirmationScreen',
        downlodUrl: this.getTnCurl()
      }
    });
  };

  render() {
    const workOrderDetailsArray = this.state.workOrderDetails;
    let loadingIndicator;
    if (this.state.apiLoading || this.props.showLoadingIndicater) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    let signatureContent;
    let activateButtonColor;
    if (this.props.signature) {
      signatureContent = (<Image
        style={styles.signatureImage}
        source={{
          uri: `data:image/jpg;base64,${this.props.signature.signatureBase}`
        }}
        resizeMode="stretch"/>);
      activateButtonColor = {
        backgroundColor: Colors.btnActive
      };
    } else {
      signatureContent = (
        <Text style={styles.signatureTxt}>
          {this.state.locals.customerSignature}
        </Text>
      );
      activateButtonColor = {
        backgroundColor: Colors.btnDeactive
      };
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <ElementTitle titleText={this.state.locals.customerConfirmation}/>
        <View style={styles.detailsContainer}>
          {workOrderDetailsArray.map((value, i) => <ElementItem key={i} leftTxt={value.name} rightTxt={value.value}/>)}
        </View>
        <View style={styles.signatureContainer}>
          <TouchableOpacity
            style={styles.signatureButtonContainer}
            onPress={() => this.onCaptureSignature()}>
            {signatureContent}
          </TouchableOpacity>
        </View>
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtonText={this.state.locals.continue}
            onClickOk={() => this.finalActivation()}
            activateColor={activateButtonColor}/>
        </View>
      </View>
    );
  }
}

const ElementTitle = ({ titleText }) => (
  <View style={styles.containerTitle}>
    <Text style={styles.titleText}>
      {titleText}
    </Text>
  </View>
);

const ElementItem = ({ leftTxt, rightTxt }) => (
  <View style={styles.detailElementContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementFooter = ({ okButtonText, onClickOk, activateColor }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={[styles.buttonContainer, activateColor]}
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('************************************************************************');
  console.log('****** REDUX STATE :: Customer Information => mobile\n', state.mobile);
  console.log('****** REDUX STATE :: Customer Information => delivery\n', state.delivery);
  let signatureCaptured = false;
  const Language = state.lang.current_lang;
  const signature = state.mobile.signature;
  const simNumber = state.genaral.simNumber;
  if (state.mobile.signature !== null) {
    signatureCaptured = true;
  }
  const materialCodeOb = state.genaral.material_code;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderSlaBreachStatus = state.delivery.currentWorkOrderSlaBreachStatus;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;
  const showLoadingIndicater = state.delivery.showLoadingIndicater;

  return {
    Language,
    materialCodeOb,
    simNumber,
    signature,
    signatureCaptured,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderSlaBreachStatus,
    currentWorkOrderBasicInfo,
    showLoadingIndicater
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerTitle: {
    flex: 1,
    paddingTop: 18
  },
  detailsContainer: {
    flex: 3.2,
    borderWidth: 0,
    padding: 15
  },
  signatureContainer: {
    flex: 2
  },
  containerBottom: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  titleText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 10,
    margin: 5
  },
  detailElementContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },

  elementLeft: {
    flex: 1,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '500'
  },
  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },
  //Signature
  signatureButtonContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginRight: 20,
    marginLeft: 20,
    borderWidth: 1,
    borderColor: Colors.btnDeactiveTxtColor,
    borderRadius: 3
  },
  signatureImage: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  },

  signatureTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  }
});

export default connect(mapStateToProps, actions)(CustomerConfirmation);
