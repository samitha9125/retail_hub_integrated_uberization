/*
 * File: FilterWorkOrders.js
 * Project: Dialog Sales App
 * File Created: Friday, 4th May 2018 1:09:57 pm
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:41:24 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  DatePickerAndroid,
  TimePickerAndroid,
  Dimensions
} from 'react-native';import Colors from '../../config/colors';
import { Header } from '../../components/others';
import Styles from '../../config/styles';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Constants from '../../config/constants';
import strings from '../../Language/Delivery';
import Utills from '../../utills/Utills';
import * as actions from '../../actions';
import { connect } from 'react-redux';

const Utill = new Utills();
class FilterWorkOrders extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locals: {
        screenTitle: '',
        continue: 'SUBMIT',
        backMessage: 'Do you want to cancel the operation & go back?',
        titleDispatched: 'DISPATCHED ORDERS',
        titlePending: 'PENDING ORDERS',
        titleInprogess: 'IN-PROGESS ORDERS',
        type: 'Request Type',
        startDate: 'Schedule Start Date',
        endDate: 'Schedule End Date',
        filter: 'FILTER',
      },
      selectedType: '',
      selectedStartDate: '',
      selectedEndDate: '',
      filterTypes: ['O2A_DELV'],
      disableFilter: false
    };
  }

  // componentDidMount(){
  //   if (this.state.filterTypes.length == 1){
  //     this.setState({ selectedType: this.state.filterTypes[0] });
  //   }
  // }
  componentWillMount(){
    if (this.state.filterTypes.length == 1){
      this.setState({ selectedType: this.state.filterTypes[0] });
    }
    this.props.getFilteredWorkOrders([]);
  }

  render() {

    let screenTitle;

    if (this.props.womListType == Constants.workOrdrState.PENDING) {
      screenTitle = this.state.locals.titlePending;
    } else if (this.props.womListType == Constants.workOrdrState.DISPATCHED) {
      screenTitle = this.state.locals.titleDispatched;
    } else if (this.props.womListType == Constants.workOrdrState.INPROGRESS) {
      screenTitle = this.state.locals.titleInprogess;
    }

    const ElementSelctionMenu = ({ dropDownData, onSelect }) => (
      <ModalDropdown
        options={dropDownData}
        onSelect={(idx, value) => onSelect(idx, value)}
        style={styles.modalDropdownStyles}
        disabled={this.state.filterTypes.length == 1}
        dropdownStyle={styles.dropdownStyle}
        dropdownTextStyle={styles.dropdownTextStyle}
        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
        <View style={styles.dropdownElementContainer}>
          <View style={styles.dropdownDataElemint}>
            <Text style={styles.dropdownDataElemintTxt}>{this.state.selectedType}</Text>
          </View>
          <View style={styles.dropdownArrow}>
            <Ionicons name='ios-arrow-down' size={20}/>
          </View>
        </View>
      </ModalDropdown>
    );

    const CalendarSelctionMenu = ({ placeHolderText, onSelect }) => (
      <View style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElemint}>
          <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
        </View>
        <View style={styles.dropdownArrow}>
          <TouchableOpacity onPress={() => onSelect()}>
            <View>
              <Ionicons name='md-calendar' size={30}/>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );

    const ElementFooter = ({ okButtontext, onClickOk, disabled }) => (
      <View style={styles.bottomContainer}>
        {/* <View style={styles.dummyView}/> */}
        <TouchableOpacity
          style={ okButtontext == 'CLEAR ALL'? styles.buttonClearContainer : styles.buttonContainer }
          onPress={onClickOk}
          activeOpacity={1}
          disabled={disabled}>
          <Text style={styles.buttonTxt}>{okButtontext}</Text>
        </TouchableOpacity>
      </View>
    );
    
    return (
      <View style={styles.mainContainer}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={screenTitle}/>
        <View style={styles.container}>


          {/* TYPE */}
          <View style={styles.filterSection}>
            <View style={styles.dropDownTextContainer}>
              <Text style={styles.dropDownText}>{this.state.locals.type}</Text>
            </View>
            <ElementSelctionMenu
              dropDownData={this.state.filterTypes}
              onSelect={(idx, value) => this.selectFilterType(idx, value)}/>
          </View>

          {/* START DATE */}
          <View style={styles.filterSection}>

            <View style={styles.dropDownTextContainer}>
              <Text style={styles.dropDownText}>{this.state.locals.startDate}</Text>
            </View>
            <CalendarSelctionMenu
              placeHolderText={this.state.selectedStartDate}
              onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'startDate')}/>
          </View>

          {/* END DATE */}
          <View style={styles.filterSection}>

            <View style={styles.dropDownTextContainer}>
              <Text style={styles.dropDownText}>{this.state.locals.endDate}</Text>
            </View>
            <CalendarSelctionMenu
              placeHolderText={this.state.selectedEndDate}
              onSelect={() => this.openDatePicker(this.state.selectedStartDate, this.state.selectedEndDate, 'endDate')}/>
          </View>

          <View style={styles.footerSection}>
            <View style={{ flex:0.9, flexDirection: 'row', justifyContent: 'flex-end', }}>
              <ElementFooter
                okButtontext={"CLEAR ALL"}
                onClickOk={() => this.clearAllButtonHandler()}/>

              <ElementFooter
                okButtontext={"FILTER"}
                onClickOk={() => this.filterButtonHandler()}
                disabled={this.state.disableFilter}/>
            </View>
          </View>
        </View>
      </View>
    );
  }

  clearAllButtonHandler(){
    this.setState({ selectedStartDate: '' });
    this.setState({ selectedEndDate: '' });
  }

  filterButtonHandler(){
    let self = this;

    this.setState({ disableFilter: true },() => {
      let workOrderStatus;
      let job_status;
      if (this.props.womListType == Constants.workOrdrState.PENDING) {
        workOrderStatus = Constants.workOrderStatus.PENDING;//workOrderStatus
        job_status = 'INITIAL';
      } else if (this.props.womListType == Constants.workOrdrState.DISPATCHED) {
        workOrderStatus = Constants.workOrderStatus.DISPATCHED;
        job_status = 'DISPATCHED';
      } else if (this.props.womListType == Constants.workOrdrState.INPROGRESS) {
        workOrderStatus = Constants.workOrderStatus.IN_PROGRESS;
        job_status = 'IN_PROGRESS';
      }

      if (this.state.selectedType == '') {
        Utill.showAlertMsg('Please select a filter type');
        self.setState({ disableFilter: false });
        return;
      }
      if (this.state.selectedStartDate == '') {
        Utill.showAlertMsg('Please select a Start Date and Time');
        self.setState({ disableFilter: false });
        return;
      }
      if (this.state.selectedEndDate == '') {
        Utill.showAlertMsg('Please select a End Date and Time');
        self.setState({ disableFilter: false });
        return;
      }


      let data = {
        fromDate: this.state.selectedStartDate,
        toDate: this.state.selectedEndDate,
        requestType: this.state.selectedType,
        job_status: job_status,
        workOrderStatus: workOrderStatus
      };

      Utill.apiRequestPost('filterWorkOrders', 'womDelivery', 'delivery', data, 
        (response)=> {
          self.props.getFilteredWorkOrders(response.data.info);
          const navigatorOb = this.props.navigator;
          navigatorOb.pop();
          self.setState({ disableFilter: false });
          console.log("Filter Success Response: ", JSON.stringify(response.data.info));

        } , 
        (response) => {
          console.log("Filter Fail Response: ", JSON.stringify(response));
          Utill.showAlertMsg(response.data.error);
          self.setState({ disableFilter: false });
        });
    });

    
  }



  // getFilterSuccessCb(response){
  //   let self = this;
  //   self.props.getFilteredWorkOrders(response.data.info);
  //   console.log("Filter Success Response: ", JSON.stringify(response.data.info));
  // }  

  // getFilterFailureCb(response){
  //   console.log("Filter Fail Response: ", JSON.stringify(response));
  //   Utill.showAlertMsg(response.data.error);
  // }

  selectFilterType(idx, value){
    this.setState({ selectedType: value });
  }

  async openDatePicker(selStartDate, selEndDate, dateType){
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'');

    var initialDate;


    console.log('this.state.selectedStartDate ' + selStartDate.slice(0,10));
    console.log('this.state.selectedEndDate', selEndDate.slice(0,10));

    let formatteStartdDate = selStartDate.slice(0,10); 
    let formatteEnddDate = selEndDate.slice(0,10); 

    console.log('formatteStartdDate ' + new Date(formatteStartdDate));
    console.log('formatteEnddDate ', new Date(formatteEnddDate));
    console.log('utc: ',utc);

    if (dateType == 'startDate'){
      let stDate = selStartDate.slice(0,10);
      let stYear = stDate.slice(0,4);
      let stMonth = parseInt(stDate.slice(5,7)) - 1;
      let stDay = stDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', stYear+stMonth+stDay);
      initialDate =  (selStartDate == ''? new Date(utc) : new Date(stYear,stMonth,stDay));
    } else {
      let enDate = selEndDate.slice(0,10);
      let enYear = enDate.slice(0,4);
      let enMonth = parseInt(enDate.slice(5,7)) - 1;
      let enDay = enDate.slice(8,10);
      console.log('new Date(stYear,stMonth,stDay): ', enYear+enMonth+enDay);
      initialDate =  (selEndDate == ''? new Date(utc) : new Date(enYear,enMonth,enDay));
    }

    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: initialDate,
        // minDate: new Date(),
        mode:'calendar'
      });
      
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        let selectedMonth = (month<10?'0'+(month+1):(month+1));
        let selectedDay = (day<10?'0'+day:day);

        let selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 00:00:00';
        // let unformattedSelectedDate = year+'-'+selectedMonth+'-'+selectedDay;
        if (dateType == 'startDate'){
          this.setState({ selectedStartDate: selectedDate });
        } else {
          // if (this.state.selectedStartDate === selectedDate){
          selectedDate = year+'-'+selectedMonth+'-'+selectedDay+' 23:59:59';

          // }
          this.setState({ selectedEndDate: selectedDate });
        }
        // this.openTimePicker(unformattedSelectedDate, dateType);
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  async openTimePicker(selectedDate, dateType){
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: 0,
        minute: 0,
        is24Hour: true, // Will display '2 PM'
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        let dateTime = selectedDate+ ' '+(hour<10?'0'+hour:hour)+':'+(minute<10?'0'+minute:minute)+':00';

        if (dateType == 'startDate'){
          this.setState({ selectedStartDate: dateTime });
        } else {
          this.setState({ selectedEndDate: dateTime });
        }
      }
    } catch ({ code, message }) {
      console.warn('Cannot open time picker', message);
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY ===> Filter Work Orders ', state.delivery);

  const pendingWorkOrders = state.delivery.pendingWorkOrders;
  return { pendingWorkOrders };
};


const styles = StyleSheet.create({
  mainContainer:{ 
    flex:1, 
    backgroundColor: Colors.appBackgroundColor 
  },
  container: {
    flex: 0.8,
    backgroundColor: Colors.appBackgroundColor,
    paddingBottom: 20
  },
  filterSection:{
    flex:1,
    flexDirection: 'column'
  },
  footerSection:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  dropDownTextContainer: {
    flex: 0.5,
    //backgroundColor: 'pink'
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 15,
    fontWeight: 'bold'

  },
  //button styles
  bottomContainer: {
    height: 55,
    flex: 1,

    backgroundColor: Colors.appBackgroundColor,
    // flexDirection: 'row',
    padding: 5,
    // paddingTop: 15,
    // paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    // flex: 1.6,
    // alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    // marginRight: 10,
    // alignSelf: 'flex-end'
  },
  buttonClearContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    height: 55,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    // marginRight: 10,
    // alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '400'
  },
  //modal dropdown styles
  modalDropdownStyles: {
    width: Dimensions.get('window').width 
  },
  dropdownStyle: {
    width: Dimensions.get('window').width - 30 ,
    height: 80,
    marginLeft: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 5
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  }
});

export default connect(mapStateToProps, actions)(FilterWorkOrders);
