import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/Delivery';
import { Header } from '../../components/others';


class NoWorkOrder extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      apiLoading: true,
      resumit: true,
      isDisibleButtons: false,
      locals: {
        screenTitle: '',
        titleOrders: 'WORK ORDER',
        titleDispatched: 'DISPATCHED WORK ORDER',
        titlePending: 'PENDING WORK ORDER',
        titleInprogess: 'IN-PROGESS WORK ORDER',
        backMessage: 'Do you want to cancel the operation & go back?',
        you_dont_have_any_dispatched_work_orders: 'You don\'t have any \nDispatched Work Order'
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
  }

  render() {
    let screenTitle;

    if (this.props.currentWorkOrderState == Constants.workOrdrState.PENDING) {
      screenTitle = this.state.locals.titlePending;
    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.DISPATCHED) {
      screenTitle = this.state.locals.titleDispatched;

    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.INPROGRESS) {
      screenTitle = this.state.locals.titleInprogess;
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={screenTitle}/>
        <View style={styles.detailsContainer}>
          <Text style={styles.errorMsgText}>
            {this.state.locals.you_dont_have_any_dispatched_work_orders}
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => WorkOrderDetails\n', state.delivery);
  const Language = state.lang.current_lang;
  const currentWorkOrderState = state.delivery.workOrderState;

  return { Language, currentWorkOrderState };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  detailsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  errorMsgText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  }
});

export default connect(mapStateToProps, actions)(NoWorkOrder);
