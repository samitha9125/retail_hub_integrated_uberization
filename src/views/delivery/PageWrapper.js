import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StyleSheet } from 'react-native';
import Colors from '../../config/colors';

class PageWrapper extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { style } = this.props;
    return (
      <KeyboardAwareScrollView
        style={[styles.keyboardAwareScrollViewContainer, style]}
        keyboardShouldPersistTaps="always">
        {this.props.children}
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  }
});

export default PageWrapper;
