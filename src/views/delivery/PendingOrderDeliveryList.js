/*
 * File: PendingOrderDeliveryList.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 10:54:05 am
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:41:40 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */

import React, { Component } from 'react';
import { StyleSheet, View, BackHandler, Alert, Text, TouchableHighlight, Image, TextInput, Linking } from 'react-native';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
// import Utills from '../../utills/Utills';
import Colors from '../../config/colors';
import * as actions from '../../actions';
import { FlatList } from 'react-native';
import { Dimensions } from 'react-native';
import ActIndicator from './ActIndicator';
import { Header } from './Header';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Constants from '../../config/constants';
import strings from '../../Language/Delivery';

// const Utill = new Utills();
const UNDERLAY_COLOR = 'rgba(0,1,1,0.7)';

class PendingOrderDeliveryList extends Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      isLoading: true,
      pendingWorkOrders: [],
      locals: {
        backMessage: 'Do you want to cancel the operation & go back ?',
        cannot_proceed_error_msg: 'Data is invalid, Can not Proceed the Order.!',
        continue: 'CONTINUE',
        cancel: 'CANCEL',
        titleOrders: 'WORK ORDER',
        titleDispatched: 'DISPATCHED WORK ORDER',
        titlePending: 'PENDING WORK ORDER',
        titleInprogess: 'IN-PROGESS WORK ORDER',
        btnDispatchTitle: 'DISPATCH',
        btnProceedJobTitle: 'PROCEED JOB',
        btnStartJobTitle: 'START',
        inProgressErrorDesc: 'You don\'t have any \nIn-Progress Work Order',
        pendingErrorDesc: 'You don\'t have any \nPending Work Order',
        dispatchedErrorDesc: 'You don\'t have any \nDispatched Work Order',
        noSearchResult: 'No search result available',
        googleMapsMessage: 'Do you want to proceed to Google Maps?',
      },
      sortTypes:["Assigned Time", "Business Unit", "Priority", "SLA Lapse"],
      selectedSortType:'',
      showSearchBar: false,
      searchResults: [],
      searchString: ''
    };
  }

  componentWillMount() {
    if (this.props.womListType == Constants.workOrdrState.PENDING) {
      this.props.getWorkOrders('INITIAL');
    } else if (this.props.womListType == Constants.workOrdrState.DISPATCHED) {
      this.props.getWorkOrders('DISPATCHED');
    } else if (this.props.womListType == Constants.workOrdrState.INPROGRESS) {
      this.props.getWorkOrders('IN_PROGRESS');
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    // this
    //   .props
    //   .getresetPendingOrders(); TODO: to fix reset issue
  }

  handleBackButtonClick = ()=> {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    console.log('xxx Work order list goBack');
    this.props.getresetPendingOrders();
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });

  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.pendingWorkOrders>0) {
  //     this.setState({ isLoading: false });
  //   } else {
  //     this.setState({ isLoading: true });
  //   }
  // }

onDetailsPress = (orderId, jobId, item) => {
  console.log('xxx onDetailsPress :: orderId : ', orderId);
  console.log('xxx onDetailsPress :: jobId : ', jobId);
  this
    .props
    .setCurrentWorkOrderId(parseInt(orderId));
  this
    .props
    .setCurrentWorkOrderJobId(parseInt(jobId));
  this
    .props
    .setCurrentWorkOrderJobStatus(this.getJobStatus());
  //Set SLA time laps and SLA breach ststus
  this.props.setCurrentWorkOrderSlaBreachStatus(item.breachTime, item.slaLapse);
  const navigatorOb = this.props.navigator;
  //this.props.currentWorkOrderSlaBreachStatus TODO: Requement change
  if (false){
    navigatorOb.push({
      title: 'Sla Breach Screen',
      screen: 'DialogRetailerApp.views.SlaBreachScreen',
      passProps: {}
    }); 

  } else if (this.getJobStatus() == 'INITIAL') {
    navigatorOb.push({
      title: 'PENDING WORK ORDER',
      screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen',
      passProps: {
        workOrderId: orderId,
        jobId: jobId,
        jobStatus: this.getJobStatus(),
        workOrdrState: Constants.workOrdrState.PENDING,
        womListType: Constants.workOrdrState.PENDING
      }
    });
  } else if (this.getJobStatus() == 'IN_PROGRESS' ){
    navigatorOb.push({
      title: 'PENDING WORK ORDER',
      screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen',
      passProps: {
        workOrderId: orderId,
        jobId: jobId,
        jobStatus: this.getJobStatus(),
        workOrdrState: Constants.workOrdrState.INPROGRESS,
        womListType: Constants.workOrdrState.INPROGRESS
      }
    });
  }
}

  getJobStatus= () =>{
    let jobStatus = 'INITIAL';
    if (this.props.womListType == Constants.workOrdrState.PENDING) {
      jobStatus = 'INITIAL';
    } else if (this.props.womListType == Constants.workOrdrState.DISPATCHED) {
      jobStatus = 'DISPATCHED';     
    } else if (this.props.womListType == Constants.workOrdrState.INPROGRESS) {
      jobStatus = 'IN_PROGRESS';
    }
    return jobStatus;
  }

  onDispatchPress = (orderId, jobId, item) => {
    console.log('xxx onDispatchPress :: orderId : ', orderId);
    console.log('xxx onDispatchPress :: jobId : ', jobId);
    this
      .props
      .setCurrentWorkOrderId(parseInt(orderId));
    this
      .props
      .setCurrentWorkOrderJobId(parseInt(jobId));
    this
      .props
      .setCurrentWorkOrderJobStatus(this.getJobStatus());
      
    this.props.setCcbsOrderId(item.ccbs_order_id);

    //Set SLA time laps and SLA breach ststus
    this.props.setCurrentWorkOrderSlaBreachStatus(item.breachTime, item.slaLapse);
    const navigatorOb = this.props.navigator;
    //this.props.currentWorkOrderSlaBreachStatus TODO: Requement change
    if (false){
      navigatorOb.push({
        title: 'Sla Breach Screen',
        screen: 'DialogRetailerApp.views.SlaBreachScreen',
        passProps: {}
      }); 

    } else if (this.getJobStatus() == 'INITIAL' ) {
      navigatorOb.showModal({ 
        title: '', 
        screen: 'DialogRetailerApp.modals.ConfirmAlertModal', 
        overrideBackPress: true ,
        passProps: {
          workOrderId: orderId,
          jobId: jobId,
          insideDetailView: false
        }
      });
    } else if (this.getJobStatus() == 'IN_PROGRESS'){

      const data = {
        order_id: orderId,
        job_id: jobId,
        job_status: this.getJobStatus(),
        cannot_proceed_error_msg: this.state.locals.cannot_proceed_error_msg
      };
      this.props.getWorkOrderDetails(data);
      navigatorOb.push({
        title: 'IN-PROGESS WORK ORDER',
        screen: 'DialogRetailerApp.views.CustomerVerificationScreen',
        passProps: {
          workOrdrState: this.props.womListType,
          workOrderId: orderId,
          jobId: jobId,      
        }
      });
    }
  }

  handleSearch(){
    this.setState({ showSearchBar: !this.state.showSearchBar });
    this.setState({ searchString: '' });
    console.log("Search tapped.");
  }

  search(text, myArray){
    let resultsArray = [];
    for (var i=0; i < myArray.length; i++) {
      if ((''+myArray[i].orderId).search(text) !== -1) {
        resultsArray.push(myArray[i]);
      }
    }
    return resultsArray;
  }

  searchTextChanged(text, workOrders){
    console.log("Search text: ", text);
    console.log("Props: ", workOrders);
    let resultsArray = this.search(text, workOrders);
    this.setState({ searchResults : resultsArray });
    this.setState({ searchString: text });
  }

  handleFilter(){
    const navigatorOb = this.props.navigator;

    let screenType = this.props.womListType;

    navigatorOb.push({
      title: 'FILTER WORK OREDERS', 
      screen: 'DialogRetailerApp.views.FilterWorkOrdersIndex',
      passProps: {
        workOrdrState: screenType,
        womListType: screenType
      }
    });
    console.log("Filter tapped.");
  }

  onMapsPress(address){
    Alert.alert('', this.state.locals.googleMapsMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          Linking.openURL('https://www.google.com/maps/dir/?api=1&destination='+ encodeURI(address));
        }
      }
    ], { cancelable: true });
    
  }

  sortTypeSelected(idx, value){
    this.setState({ selectedSortType: value });
    this.sortByAssignedTime(idx);
    // switch (idx){
    //   case 0:
    //     this.sortByAssignedTime();
    //     break;
    //   case 1: 
    //     break;
    //   case 2:
    //     break;
    //   default: 
    //     // break;
    // }
    
    // console.log("Sorted: ", this.sortByAssignedTime);
    console.log("idx: ", idx, "\nvalue: ", value);
    // this.setState({ selectedSortType: value });
  }

  sortByAssignedTime(idx){
    let pendingWorkOrders = [];
    pendingWorkOrders  = this.props.pendingWorkOrders.slice(0);
    // var date_sort_asc = function (date1, date2) {
    //   // This is a comparison function that will result in dates being sorted in
    //   // ASCENDING order. As you can see, JavaScript's native comparison operators
    //   // can be used to compare dates. This was news to me.
    //   if (date1.scheduledTime > date2.scheduledTime) return 1;
    //   if (date1.scheduledTime < date2.scheduledTime) return -1;
    //   return 0;
    // };
    if (parseInt(idx) == 0 ){
      let date_sort_desc = function (date1, date2) {
        console.log("In schedileTime");
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (date1.scheduledTime > date2.scheduledTime) return -1;
        if (date1.scheduledTime < date2.scheduledTime) return 1;
        return 0;
      };
      pendingWorkOrders.sort(date_sort_desc);
    } else if (parseInt(idx) == 1){
      let date_sort_desc = function (date1, date2) {
        console.log("In woBisUnit");
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (date1.woBisUnit > date2.woBisUnit) return -1;
        if (date1.woBisUnit < date2.woBisUnit) return 1;
        return 0;
        // return date1.woBisUnit.localeCompare(date2.woBisUnit);
      };
      pendingWorkOrders.sort(date_sort_desc);
    } else if (parseInt(idx) == 2){
      let date_sort_desc = function (date1, date2) {
        console.log("In priority");
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (date1.priority > date2.priority) return -1;
        if (date1.priority < date2.priority) return 1;
        return 0;
      };
      pendingWorkOrders.sort(date_sort_desc);
    } else {
      let date_sort_desc = function (date1, date2) {
        console.log("In slapse", date1, date2);
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (parseInt(date1.sla_in_min) < parseInt(date2.sla_in_min)) return -1;
        if (parseInt(date1.sla_in_min) > parseInt(date2.sla_in_min)) return 1;
        return 0;
      };
      pendingWorkOrders.sort(date_sort_desc);
      this.setState({ searchResults : pendingWorkOrders });
      
    }
    console.log("SORTED: ", pendingWorkOrders);
    this.setState({ searchResults : pendingWorkOrders });
    for (var i=0; i<pendingWorkOrders.length; i++){
      console.log('scheduledTime: ', pendingWorkOrders[i].scheduledTime, '-', pendingWorkOrders[i].orderId);
      console.log('slaLapse: ', pendingWorkOrders[i].sla_in_min, '-', pendingWorkOrders[i].orderId);
    }
  }

  //DialogRetailerApp.views.FilterWorkOrdersIndex
  render() {
    console.log('Delivery Home Printed'); 
    const pendingWorkOrders = this.props.pendingWorkOrders;
    const workOrderRetrieveStatus = this.props.workOrderRetrieveStatus;
    console.log("in render: ", pendingWorkOrders);

    let screenTitle;
    let errorDescription;
    // let dispatchBtnTitle;

    if (this.props.womListType == Constants.workOrdrState.PENDING) {
      screenTitle = this.state.locals.titlePending;
      errorDescription = this.state.locals.pendingErrorDesc;
      this.dispatchBtnTitle = this.state.locals.btnDispatchTitle;
    } else if (this.props.womListType == Constants.workOrdrState.DISPATCHED) {
      screenTitle = this.state.locals.titleDispatched;
      errorDescription = this.state.locals.dispatchedErrorDesc;
      this.dispatchBtnTitle = this.state.locals.btnStartJobTitle;
    } else if (this.props.womListType == Constants.workOrdrState.INPROGRESS) {
      screenTitle = this.state.locals.titleInprogess;
      errorDescription = this.state.locals.inProgressErrorDesc;
      this.dispatchBtnTitle = this.state.locals.btnProceedJobTitle;
    }

    if (this.state.searchResults.length == 0 && this.state.searchString !== '') {
      errorDescription = this.state.locals.noSearchResult;
    }

    return (
      <View style={styles.mainContainer}>
        {/* HEADER SECTION START */}
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          filterButtonPressed={() => this.handleFilter()}
          displaySearch = {workOrderRetrieveStatus}
          displayFilter = {workOrderRetrieveStatus}
          headerText={screenTitle}/>
        {/* HEADER SECTION END */}
        
        {/* SEARCH BAR VIEW START */}
        { this.state.showSearchBar ?
          <View style={styles.searchBarView}>
            <TextInput 
              style={styles.textInputStyleClass}
              onChangeText={(text) =>{ this.searchTextChanged(text, pendingWorkOrders); }}
              value={this.state.text}
              underlineColorAndroid='transparent'
              placeholder="Search"
              keyboardType={'numeric'}
            />
          </View>
          : true}

        {/* SEARCH BAR VIEW END */}

        {workOrderRetrieveStatus !== false  ? 
        
        //{/* SORTING VIEW START */}
      <View style={styles.sortingView}>
        {/* SORT BY LABEL START */}
        <View style={styles.sortByLabelContainer}>
          <Text style={styles.txtSortBy}>Sort By</Text>
        </View>
        {/* SORT BY LABEL END */}
        {/* DROP DOWN SECTION START */}
        <View style={styles.sortByDropDownContainer}>
          <ModalDropdown 
            options={this.state.sortTypes}  
            style={styles.dropDownMain} 
            dropdownStyle={styles.dropDownStyle} 
            dropdownTextStyle={styles.dropDownTextStyle} 
            dropdownTextHighlightStyle={styles.dropDownTextHighlightStyle} 
            onSelect={(idx, value) => this.sortTypeSelected(idx, value)}>
            <View style={styles.dropDownSelectedItemView}>

              <View style={styles.selectedItemTextContainer}>
                <Text style={styles.sortTypeText}>
                  {this.state.selectedSortType}
                </Text>
              </View>

              <View style={styles.dropDownIcon}>
                <Ionicons name='md-arrow-dropdown' size={20} /> 
              </View> 
            </View>
          </ModalDropdown>
        </View>
        {/* DROP DOWN SECTION END */}
      </View>
          //{/* SORTING VIEW END */}
          : true}


        <View style={styles.bottomContainer}>
          {/* MAIN TABLE VIEW */}
          <View style={styles.container}>
            {/* ACTIVITY INDICATOR */}
            {workOrderRetrieveStatus === null  ? <ActIndicator animating /> : true}

            {/* ERROR DESCRIPTION */}
            {workOrderRetrieveStatus === false || (this.state.searchResults.length == 0 && this.state.searchString !== '') ? 
              <View style={styles.errorDescView}> 
                <Text style={styles.errorDescText}> {errorDescription}</Text>
              </View> : true}

            <FlatList
              data={(this.props.workOrdersSearchResults.length > 0 ? 
                this.props.workOrdersSearchResults : 
                (
                  this.state.searchResults.length>0?
                    this.state.searchResults: 
                    (this.state.searchString === ''? pendingWorkOrders : this.state.searchResults) 
                )
              )}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={true}
            />
          </View>
        </View>
      </View>
    );
  }

    renderListItem = ({ item }) => (
      <View style={styles.card}>
        {/* CARD TOP SECTION */}
        <View style={styles.cardTop}>
          {/* SIM NUMBER AND PRIORITY LABEL START  */}
          <View style={styles.cardTopSection1}>
            <View style={styles.simNumberView}>
              <View style={styles.simNumber}>
                <Text style={styles.txtSimNumber}> {item.orderId} </Text>
                <Image
                  source={require('../../../images/delivery/phone-portrait.png')}
                  style={ styles.imageContentPhone }
                />
              </View>
            </View> 
            { item.priority ? 
              <View style={styles.priorityView}>
                <Text style={styles.txtPriority}> PRIORITY </Text>
              </View>
              : 
              <View style={styles.priorityViewTransparent}/>
            } 
          </View>
          {/* SIM NUMBER AND PRIORITY LABEL END */}
          {/* DELIVERY TRUCK AND LABEL START */}
          <View style={styles.cardTopSection2}> 
            <View style={styles.deliveryTruckView}>
              <View style={ styles.deliveryTruckImageContainer }>
                <Image
                  source={require('../../../images/delivery/delivery_truck.png')}
                  style={ styles.imageContent }
                />
              </View>
              <View style={ styles.txtDeliveryContainer }>
                <Text> Delivery </Text>
              </View>
            </View>
            <View style={styles.bottomSpaceView} />
            {/* DELIVERY TRUCK AND LABEL END */}
          </View>
          <View style={styles.cardTopSection3}> 
            <View style={styles.simNumberView}>
              <Text style={(item.breachTime? styles.txtDurationRed : styles.txtDuration )}     
                numberOfLines={1}> 
                {item.slaLapse} 
              </Text>
            </View>
            <View style={styles.bottomSpaceView} />
          </View>
        </View>

        {/* CARD BODY */}
        <View style={styles.cardBody}>
          <View style={styles.addressField}>
            <Text style={styles.txtAddress} numberOfLines={3}>{item.address}</Text>
          </View>
          <View style={styles.orderDetails}>
            <View style={styles.orderDetail}>
              <Text style={styles.txtOrderDetailLabel}> Team Target SLA </Text>
              <Text style={styles.txtOrderDetailValue}> {item.TTSLA} </Text>
            </View>
            {item.CXRT!=""? 
              <View style={styles.orderDetail}>
                <Text style={styles.txtOrderDetailLabel}> CX Requested Time </Text>
                <Text style={styles.txtOrderDetailValue}> {item.CXRT} </Text>
              </View>
              :
              true
            }

          </View>
        </View>
        {/* CARD BODY END */}
        {/* CARD BOTTOM SECTION START */}
        <View style={styles.cardBottom}>
          <View style={styles.cardBottomSection1}> 
            <View style={styles.directionsView}>
              <TouchableHighlight 
                key={1}
                underlayColor={Colors.colorTransparent}
                onPress={() => {
                  this.onMapsPress(item.address);
                }}
                style={styles.cardBottomSection2}> 
                <Image
                  source={require('../../../images/delivery/direction.png')}
                  style={ styles.directionsImageContent }
                />
                {/* <Text style={styles.txtDetails}>DETAILS</Text> */}
              </TouchableHighlight>
            </View>
          </View>
          <TouchableHighlight 
            key={1}
            underlayColor={UNDERLAY_COLOR}
            onPress={() => {
              this.onDetailsPress(item.orderId, item.job_id, item);
            }}
            style={styles.cardBottomSection2}> 
            <Text style={styles.txtDetails}>DETAILS</Text>
          </TouchableHighlight>
          {/* <View style={styles.cardBottomSection3}>  */}
          <TouchableHighlight
            key={2}
            underlayColor={UNDERLAY_COLOR}
            // disabled={item.breachTime}
            onPress={() => {
              this.onDispatchPress(item.orderId, item.job_id, item);
            }}
            style={[styles.cardBottomSection3]}
          >
            <Text style={styles.txtDispatch}> {this.dispatchBtnTitle} </Text>
          </TouchableHighlight>
          {/* </View> */}
        </View>
        {/* CARD BOTTOM SECTION END */}
      </View>)
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY ===> Pending Work Orders ', state.delivery);

  const pendingWorkOrders = state.delivery.pendingWorkOrders;
  const workOrdersSearchResults = state.delivery.workOrdersSearchResults;
  const Language = state.lang.current_lang;
  const workOrderRetrieveStatus = state.delivery.workOrderRetrieveStatus;
  // const isLoading = !state.delivery.length > 0;

  // if (state.delivery.initial){
  //   isLoading = true;
  // } else {
  //   isLoading = false;
  // }
  // if (state.delivery.length > 0){
  //   isLoading = false;
  // } 

  return { Language, pendingWorkOrders, workOrdersSearchResults, workOrderRetrieveStatus };
};

const styles = StyleSheet.create({
  bottomContainer:{ 
    flex: 1, 
    flexDirection: 'row',
    paddingTop : 10,
    backgroundColor: Colors.lightGrey

  },
  mainContainer:{
    flex:1, 
    flexDirection: 'column', 
    justifyContent: "center", 
    alignItems: "center", 
    alignSelf:'center' 
  },
  container:{
    flex:1,
    backgroundColor: Colors.lightGrey,
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf:'center',
    flexDirection: 'column'
  },
  errorDescView:{ 
    flex:1, 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  errorDescText:{ 
    textAlign: 'center', 
    fontSize: 17, 
    fontWeight:'bold', 
    color: Colors.black 
  },
  sortingView:{ 
    flex: 0.08,
    flexDirection: 'row',
    backgroundColor: Colors.lightGrey, 
    alignSelf: 'stretch', 
    paddingLeft: 10, 
    paddingRight:10,
    justifyContent:'center',
    alignItems: 'center',

  },
  searchBarView:{ 
    flex: 0.08,
    flexDirection: 'row',
    backgroundColor: Colors.lightGrey, 
    alignSelf: 'stretch', 
    paddingLeft: 10, 
    paddingRight:10,
    justifyContent:'center',
    alignItems: 'center', 
    paddingTop:5,
  },
  dropDownTextStyle:{ 
    color: Colors.black, 
    fontSize: 15,
    fontWeight: 'bold' 
  },
  dropDownMain:{ 
    width: Dimensions.get('window').width * 0.7 
  },
  dropDownStyle:{ 
    flex:1, 
    width: Dimensions.get('window').width * 0.7,
    height: 160 
  },
  dropDownTextHighlightStyle:{ 
    fontWeight: 'bold' 
  },
  dropDownSelectedItemView:{ 
    flexDirection: 'row', 
    backgroundColor:Colors.white, 
    alignItems: 'center',
    justifyContent: 'center', 
    height:'100%', 
    width:'100%' 
  },
  selectedItemTextContainer:{ 
    width:'90%',
    paddingLeft: 5,  
    justifyContent: 'center', 
    alignItems: 'flex-start' 
  },
  sortTypeText:{ 
    flex:1, 
    color: Colors.black, 
    fontSize: 15, 
    marginTop: 5,
    fontWeight: 'bold' 
  },
  dropDownIcon:{ 
    width: '10%', 
    paddingRight: 10,  
    justifyContent: 'center', 
    alignItems: 'flex-end' 
  },
  card: {
    flex: 1,
    backgroundColor: Colors.white,
    // height: 350,
    height: Dimensions
      .get('window')
      .height / 3,
    margin: 5,
    marginVertical:10,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: Colors.black,
    shadowOffset: { height: 2, width: 0 },
    elevation:4,
    width: Dimensions.get('window').width - 10
    // marginTop: 0
  },
  cardTop:{
    flex: 1,
    flexDirection: 'row',
    marginTop: 10
    // backgroundColor: 'lightpink'
  },
  cardBody:{
    flex: 1.6,
    flexDirection: 'column',
    // backgroundColor: 'yellow'
  },
  cardBottom:{
    flex: 0.5,
    flexDirection: 'row',
    borderTopWidth:1,
    borderColor: Colors.lightGrey
    // backgroundColor: 'red'
  },
  sortByLabelContainer:{
    flex: 1, 
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:10,
    marginLeft: 0
  },
  sortByDropDownContainer:{ 
    flex: 3, 
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:10,
    paddingBottom: 10,
    height:50,
  },
  txtDispatch: {
    fontWeight: 'bold',
    color: Colors.colorDarkOrange
  },
  txtDetails: {
    fontWeight: 'bold'
  },
  txtSortBy: {
    fontWeight: 'bold'  ,
    color: Colors.black,
    alignSelf: 'flex-start'
  },
  txtDuration: {
    fontWeight: '500',
    color: Colors.colorGreen  
  },
  txtDurationRed: {
    fontWeight: '500',
    color: Colors.colorRed  
  },
  txtPriority: {
    fontWeight: 'bold',
    color: Colors.white
  },
  txtSimNumber: {
    fontSize: 18,
    color: Colors.black
  },
  txtAddress: {
    flex:1,
    fontWeight: 'bold',
    color: Colors.black,
    flexWrap: 'wrap'
  },
  txtOrderDetailLabel: {
    width:135,
    color: Colors.black
  },
  txtOrderDetailValue: {
    flex:1,
    color: Colors.black
  },
  cardTopSection1:{
    flex:1,
    flexDirection: "column",
    // backgroundColor: "blue"
  },
  cardTopSection2:{
    flex:1.1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  cardTopSection3:{
    flex:1.5,
    flexDirection: "column",
    // backgroundColor: "lightyellow",
    justifyContent: "center",
    alignItems: "center"
  },
  cardBottomSection1:{
    flex:3,
    flexDirection: "row",
    alignContent: "flex-start",
    alignItems: "flex-start",
    // backgroundColor: "blue"
  },
  cardBottomSection2:{
    // flex:1,
    flexDirection: "column",
    marginRight: 30,
    // backgroundColor: "lightskyblue",
    justifyContent: "center",
    alignItems: "center"
  },
  cardBottomSection3:{
    // flex:2,
    marginRight: 10,
    flexDirection: "column",
    // backgroundColor: "lightgreen",
    justifyContent: "center",
    alignItems: "center"
  },
  simNumberView:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "mediumspringgreen"
  },
  deliveryTruckView:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "mediumspringgreen"
  },
  simNumber:{
    flex:1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  priorityView:{
    flex: 1,
    borderRadius:10,
    flexDirection: "row",
    backgroundColor: Colors.colorRed,
    justifyContent: "center",
    alignItems: "center",
    margin:5
  },
  priorityViewTransparent:{
    flex: 1,
    borderRadius:10,
    flexDirection: "row",
    backgroundColor: Colors.transparent,
    justifyContent: "center",
    alignItems: "center",
    margin:5
  },
  bottomSpaceView:{
    flex: 1
  },
  addressField:{
    flex:1.3,
    margin: 5
  },
  orderDetail:{
    flex:1,
    margin: 2,
    marginLeft: 0,
    marginBottom: 0,
    marginTop:0,
    flexDirection: 'row'
  },
  orderDetails:{
    flex:1,
    flexDirection: 'column'
  },
  directionsView:{
    // height:50,
    // width:50,
    flex:0.2,
    // backgroundColor: 'yellow',
    margin: 5,
    marginLeft: 10,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
  },
  imageContent: { 
    flex: 1, 
    width: 30, 
    height: 30, 
    justifyContent: 'center', 
    alignContent: 'center',
    alignSelf: 'center',
    resizeMode: 'contain' 
  },
  imageContentPhone: { 
    flex: 1, 
    width: 25, 
    height:25, 
    justifyContent: 'center', 
    alignContent: 'center',
    alignSelf: 'center',
    resizeMode: 'contain' ,
    marginBottom: 5

  },
  directionsImageContent: { 
    flex: 1, 
    alignSelf: 'flex-start',

    // margin:5,
    // width: 26, 
    // height: 26, 
    resizeMode: 'contain' 
  },
  txtDeliveryContainer: { 
    flex:1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    // marginBottom: 10,
    // paddingTop: 10
  },
  deliveryTruckImageContainer:{  
    flex: 1
  },
  textInputStyleClass:{    
    flex:1,
    textAlign: 'left',
    height: 35,
    borderWidth: 1,
    borderColor: Colors.navBarBackgroundColor,
    borderRadius: 7 ,
    backgroundColor : Colors.white,
    padding: 10
  }
});

export default connect(mapStateToProps, actions)(PendingOrderDeliveryList);
