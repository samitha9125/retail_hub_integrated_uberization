import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import strings from '../../Language/Delivery';
import _ from 'lodash';

const Utill = new Utills();

class Feedback extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      apiLoading: true,
      feedbackData: [],
      locals: {
        screenTitle: 'FEEDBACK',
        continue: 'SUBMIT',
        backMessage: 'Do you want to cancel the operation & go back?'
      }
    };

    this.feedbackAnswers = {};
  }

  componentDidMount() {
    let _this = this;
    BackHandler.addEventListener('hardwareBackPress', _this.handleBackButtonClick);
    this.props.deliveryGetFeedback(_this);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  componentDidUpdate(){
    console.log('componentDidUpdate :: state ', this.state);
    console.log('componentDidUpdate :: props ', this.props);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick => do nothing');
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
  }

  setIndicator = (loadingState = true) => {
    console.log('xxx setIndicator');
    this.setState({ apiLoading: loadingState });
  }

  //Common exception callback
  exCb = (error) => {
    console.log('exCb', error);
    this.setIndicator(false);
    Utill.showAlertMsg('System Error, Please try again');
  }

  //Common Failure callback for  getOrderDetails
  getFeedbackFailureCb = (response) => {
    console.log('getFeedbackFailureCb');
    this.setIndicator(false);
    console.log('xxx getFeedbackFailureCb', response);
    Utill.showAlertMsg(response.data.error);
  }

  getFeedbackSuccessCb = (responseData) => {
    console.log('createDynamicForm :: responseData ', responseData);
    let feedbackData = responseData.data.info;
    console.log('createDynamicForm :: feedbackData ', feedbackData);
    let _this = this;
    _this.setIndicator(false);
    _this.setState({ feedbackData: feedbackData, apiLoading: false });
    
  }

  getElementView = (index, elementObject) => {
    console.log('getElementView :: index, elementObject =>', index, elementObject);
    let { question_id, description, type, values } = elementObject;

    try {
      console.log('getElementView :: TRY');
      let dropdownValues = [];
      switch (type) {
        case 'DROPDOWN':
          dropdownValues = _.map(values, 'value');
          return (
            <ElementSelectionMenu
              key={index}
              index={question_id}
              labelText={description}
              dropDownData={dropdownValues}
              placeHolderText={this.state[`selected_dropdown_text_${question_id}`]}
              onSelect={(idx, value) =>  { this.onChangeValue(type, question_id, { idx, value });} }
              defaultIndex={this.state.userSelectedItemIndex}/>
          );
        case 'TEXT':
          return (
            <ElementInputField
              key={index}
              index={question_id}
              labelText={description}
              placeholder={values}
              onChangeText={(text) => { this.onChangeValue(type, question_id, { value: text });}}/>
          ); 
        default:
          return <View key={index}/>;
      }
      
    } catch (error) {
      console.log('getElementView :: CATCH :', error);
      return <View key={index}/>;
    }
  };

  onChangeValue = (type, question_id, valueOb) => {
    console.log('onChangeValue:: type =>', type);
    console.log('onChangeValue:: question_id =>', question_id);
    console.log('onChangeValue:: valueOb =>', valueOb);
    switch (type) {
      case 'DROPDOWN':
        this.setSelectedDropdown(question_id, valueOb.idx, valueOb.value);
        break;
      case 'TEXT':
        this.onTextInputChange(question_id, valueOb.value);
        break;     
      default:
        break;      
    }
  }

  setSelectedDropdown = (question_id, idx, value) => {
    console.log('setSelectedDropdown :: idx ', idx);
    console.log('setSelectedDropdown :: value ', value);
    let dropdownValues = _.find(this.state.feedbackData, { 'question_id': question_id }).values;
    let selectedDropdownValue = dropdownValues[parseInt(idx)];
    console.log('setSelectedDropdown :: dropdownValues :: ', dropdownValues);
    console.log('setSelectedDropdown :: selectedDropdownValue :: ', selectedDropdownValue);
    this.setState({ [`selected_dropdown_text_${question_id}`]: value , [`feedback_answer_question_${question_id}`]: selectedDropdownValue.id });
    this.feedbackAnswers[`question_${question_id}`] = selectedDropdownValue.id ;
    console.log('setSelectedDropdown :: feedbackAnswers =>', this.feedbackAnswers);

  }

  onTextInputChange = (question_id, text) => {
    console.log('onTextInputChange:: question_id =>', question_id);
    console.log('onTextInputChange:: text =>', text);
    this.setState({ [`feedback_answer_question_${question_id}`]: text, [`entered_text_${question_id}`]: text });
    this.feedbackAnswers[`question_${question_id}`] = text ;
    console.log('onTextInputChange :: feedbackAnswers =>', this.feedbackAnswers);
  }

  showOrderDetail = () => {
    console.log('xxx showOrderDetail');
    const navigatorOb = this.props.navigator;
    navigatorOb.push({ title: 'Pending Work Orders', screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen' });

  }

  submitFeedback = () => {
    console.log('xxx submitFeedback');
    if (this.validateMandatoryInputs().status) {
      Utill.showAlertMsg('Please select '+ this.validateMandatoryInputs().reason);
      return;
    }
    const data = {
      order_id: this.props.currentWorkOrderId,
      job_id: this.props.currentWorkOrderJobId,
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id,
      cx_answers: this.feedbackAnswers
    };

    this.setIndicator(true);
    Utill.apiRequestPost('sendCxFeedback', 'womDelivery', 'delivery', data, this.submitFeedbackSuccessCb, this.getFeedbackFailureCb, this.exCb);

  }

  validateMandatoryInputs = () => {
    console.log('validateMandatoryInputs');
    let _this = this;
    let status = false;
    let reason = 'NO ERROR';

    let mandatoryFeedbacks = _.filter(this.state.feedbackData, { 'validation': 'Y' } );
    console.log('validateMandatoryInputs :: mandatoryFeedbacks', mandatoryFeedbacks);

    _.map(mandatoryFeedbacks, (feedback, key) => {
      console.log('validateMandatoryInputs - MAP ', feedback, _this.feedbackAnswers[`question_${feedback.question_id}`]);
      if (_.isUndefined(_this.feedbackAnswers[`question_${feedback.question_id}`]) 
          || _this.feedbackAnswers[`question_${feedback.question_id}`] =='' ){
        console.log('validateMandatoryInputs - INVALID ', feedback.description);
        status = true;
        reason = feedback.description;
      }
    });
    return {
      status: status,
      reason: reason
    };
  }

  submitFeedbackSuccessCb = (response) => {
    console.log('submitFeedbackSuccessCb');
    console.log('submitFeedbackSuccessCb', response.data.info);
    this.setIndicator(false);
    this.props.getResetImagesMobileAct();
    const navigatorOb = this.props.navigator;
    if (this.props.currentWorkOrderSlaBreachStatus) {
      navigatorOb.push({ title: 'SLA Breach Screen', screen: 'DialogRetailerApp.views.SlaBreachScreen', passProps: {} });
    } else {
      navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
    }
  }


  render() {
    const feedbackData = this.state.feedbackData;
    let submitButtonColor;
    let loadingIndicator;
    if (this.validateMandatoryInputs().status) {
      console.log('xxx enable SubmitButtonColor :: false');
      submitButtonColor = {
        backgroundColor: Colors.btnDeactive 
      };
    } else {
      console.log('xxx enable SubmitButtonColor :: true');
      submitButtonColor = {
        backgroundColor: Colors.btnActive
      };
    }

    if (this.state.apiLoading) {
      loadingIndicator = <ActIndicator animating={this.state.apiLoading}/>;
    } else {
      loadingIndicator = true;
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}
          hideBackButton/>
        {/*loading indicator*/}
        {loadingIndicator}
        <View style={styles.containerDropDown}>
          <ScrollView>
            {feedbackData.map((value, i) => this.getElementView(i, value))}       
          </ScrollView>
        </View>
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtonText={this.state.locals.continue}
            onClickOk={() => this.submitFeedback()}
            activateColor={submitButtonColor}/>
        </View>
      </View>
    );
  }
}

const ElementSelectionMenu = ({ index, labelText, dropDownData, placeHolderText, onSelect, defaultIndex }) => (
  <View style={styles.elementStyle} key={index}>
    <View style={styles.dropDownTextContainer}>
      <Text style={styles.dropDownText}>{labelText}</Text>
    </View>
    <ModalDropdown
      index={index}
      options={dropDownData}
      onSelect={onSelect}
      defaultIndex={parseInt(defaultIndex)}
      style={styles.modalDropdownStyles}
      dropdownStyle={styles.dropdownStyle}
      dropdownTextStyle={styles.dropdownTextStyle}
      dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
      <View style={styles.dropdownElementContainer}>
        <View style={styles.dropdownDataElement}>
          <Text style={styles.dropdownDataElementTxt}>{placeHolderText}</Text>
        </View>
        <View style={styles.dropdownArrow}>
          <Ionicons name='md-arrow-dropdown' size={20}/>
        </View>
      </View>
    </ModalDropdown>
  </View>
);

const ElementInputField = ({ index, labelText, placeholder, onChangeText }) => (
  <View style={styles.elementStyle} key={index}>
    <View style={[styles.dropDownTextContainer, styles.dropDownTextContainerAdditional]}>
      <Text style={styles.dropDownText}>{labelText}</Text>
    </View>
    <View style={styles.inputFieldContainer}>
      <TextInput
        index={index}
        style={styles.textInput}
        placeholder={placeholder}
        onChangeText={onChangeText}/>
    </View>
  </View>
);

const ElementFooter = ({ okButtonText, onClickOk, activateColor }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={[styles.buttonContainer, activateColor]}
      onPress={onClickOk}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => Feedback\n', state.delivery);
  const Language = state.lang.current_lang;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderSlaBreachStatus = state.delivery.currentWorkOrderSlaBreachStatus;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;

  return {
    Language,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderSlaBreachStatus,
    currentWorkOrderBasicInfo
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerDropDown: {
    flex: 5,
    paddingTop: 10,
    marginTop: 15
  },
  containerBottom: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnDeactive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },

  elementStyle: {
    flex: 1, 
    flexDirection: 'column'
  },


  dropDownTextContainer: {
    flex: 0.6
  },

  dropDownTextContainerAdditional: {
    marginTop: 10
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18

  },
  //Input fields
  inputFieldContainer: {
    flex: 1
  },

  textInput: {
    height: 40,
    padding: 5,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 18
  },

  //modal dropdown styles
  modalDropdownStyles: {
    //flex: 1
    width: Dimensions
      .get('window')
      .width
  },
  dropdownStyle: {
    width: Dimensions
      .get('window')
      .width - 30,
    marginLeft: 15,
    height: 170,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElement: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElementTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  }
});

export default connect(mapStateToProps, actions)(Feedback);
