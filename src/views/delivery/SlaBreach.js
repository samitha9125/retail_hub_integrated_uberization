import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import Constants from '../../config/constants';
import strings from '../../Language/Delivery';

const Utill = new Utills();

class SlaBreach extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      apiLoading: false,
      userData: '',
      delayReason: '',
      delayCode: '',
      dropDownDataDelayReasonArray: [],
      dropDownDataDelayCodeArray: [],
      dropdownSelectedIndex: -1,
      dropDownSelectedText: '',
      locals: {
        titleDispatched: 'DISPATCHED WORK ORDER',
        titlePending: 'PENDING WORK ORDER',
        titleInprogess: 'IN-PROGESS WORK ORDER',
        slaBreach: 'You have breached the SLA by',
        selectionDescription: 'Select the reason of the breach from below list',
        please_select_reject_reason: 'Please select a reason of the breach from dropdown',
        continue: 'SUBMIT',
        backMessage: 'Do you want to cancel the operation & go back?'
      }
    };
  }

  componentWillMount() {
    const data = {
      some_data: 'some_data'
    };

    Utill.apiRequestPost('getJobDelayReasons', 'womDelivery', 'delivery', data, this.getDelayReasonSuccessCb, this.getDelayReasonFailureCb, this.exCb);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('xxx do nothing');
    return true;
  }

  setIndicater = (loadingState = true) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });
  }

  //Commmon exception callback
  exCb = (error) => {
    console.log('exCb', error);
    this.setIndicater(false);
    Utill.showAlertMsg('System Error');
  }

  //Commmon Failure callback for  getDelayReason
  getDelayReasonFailureCb = (response) => {
    console.log('getDelayReasonFailureCb');
    this.setIndicater(false);
    console.log('xxx getDelayReasonFailureCb', response);
    Utill.showAlertMsg(JSON.stringify(response.data.error));
  }

  getDelayReasonSuccessCb = (response) => {
    console.log('getDelayReasonSuccessCb');
    this.setIndicater(false);
    console.log('xxx getDelayReasonSuccessCb', response.data.info);
    const apiDataDelayReason = response.data.info;
    try {
      let dropDownDataDelayCodeArray = [];
      let dropDownDataDelayReasonArray = [];
      for (let delayReasonProps in apiDataDelayReason) {
        const delayReasonOb = apiDataDelayReason[delayReasonProps];
        console.log('xxxx delayReasonOb', delayReasonOb);
        console.log('xxxx delayReasonOb.delay_code', delayReasonOb.delay_code);
        console.log('xxxx delayReasonOb.delay_reason', delayReasonOb.delay_reason);
        dropDownDataDelayCodeArray.push(delayReasonOb.delay_code);
        dropDownDataDelayReasonArray.push(delayReasonOb.delay_reason);
      }
      console.log('xxxx dropDownDataDelayCodeArray', dropDownDataDelayCodeArray);
      console.log('xxxx dropDownDataDelayReasonArray', dropDownDataDelayReasonArray);
      this.setState({ dropDownDataDelayCodeArray: dropDownDataDelayCodeArray, dropDownDataDelayReasonArray: dropDownDataDelayReasonArray });
    } catch (error) {
      Utill.showAlertMsg('getDelayReasonSuccessCb :: System Error');
    }
  }

  submitDelayReason = () => {
    console.log('xxx submitDelayReason');
    if (this.state.dropdownSelectedIndex == -1 && this.state.delayReason == '') {
      Utill.showAlertMsg(this.state.locals.please_select_reject_reason);
      return;
    }
    this.updateWorkOrder(this.props.currentWorkOrderId, this.props.currentWorkOrderJobId, 'END', this.submitDelayReasonSuccessCb);
  }

  //updateWorkOrder
  updateWorkOrder = (order_id, job_id, updateType, sucessCb) => {
    console.log('updateStartWorkOrder');
    const data = {
      order_id: order_id,
      job_id: job_id,
      command: updateType,
      delayReason: this.state.delayReason,
      delayCode: this.state.delayCode,
      status: 'JOB_DONE',
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id
    };

    this.setIndicater(true);
    Utill.apiRequestPost('updateWorkOrder', 'womDelivery', 'delivery', data, sucessCb, this.getDelayReasonFailureCb, this.exCb);
  }

  submitDelayReasonSuccessCb = (response) => {
    console.log('submitDelayReasonSuccessCb');
    this.setIndicater(false);
    console.log('xxx submitDelayReasonSuccessCb', response.data.info);
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
  }

  setSelectedValue = (idx, value) => {
    console.log('xxx setSelectedValue', idx);
    console.log('xxx dropDownSelectedText', value);
    this.setState({ dropdownSelectedIndex: idx, dropDownSelectedText: value });
    const delayCode = this.state.dropDownDataDelayCodeArray[parseInt(idx)];
    this.setState({ delayReason: value, delayCode: delayCode });
  }

  render() {
    let screenTitle;
    if (this.props.currentWorkOrderState == Constants.workOrdrState.PENDING) {
      screenTitle = this.state.locals.titlePending;
    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.DISPATCHED) {
      screenTitle = this.state.locals.titleDispatched;
    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.INPROGRESS) {
      screenTitle = this.state.locals.titleInprogess;
    }

    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={screenTitle}
          hideBackButton/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <ElementTitle titleText={this.state.locals.slaBreach}/>
        <ElementSlaTime slaTime={this.props.slaBreachTimeDisplay}/>
        <View style={styles.containerDropDown}>
          <View style={styles.dropDownTextContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.selectionDescription}</Text>
          </View>
          <ElementSelctionMenu
            dropDownData={this.state.dropDownDataDelayReasonArray}
            placeHolderText={this.state.dropDownSelectedText}
            onSelect={(idx, value) => this.setSelectedValue(idx, value)}
            defaultIndex={this.state.dropdownSelectedIndex}/>
        </View>
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtontext={this.state.locals.continue}
            onClickOk={() => this.submitDelayReason()}/>
        </View>
      </View>
    );
  }
}

const ElementTitle = ({ titleText }) => (
  <View style={styles.containerTitle}>
    <Text style={styles.titleText}>
      {titleText}
    </Text>
  </View>
);

const ElementSlaTime = ({ slaTime }) => (
  <View style={styles.containerSlaTime}>
    <Text style={styles.slaTimeText}>
      {slaTime}
    </Text>
  </View>
);

const ElementSelctionMenu = ({ dropDownData, placeHolderText, onSelect, defaultIndex }) => (
  <ModalDropdown
    options={dropDownData}
    onSelect={onSelect}
    defaultIndex={parseInt(defaultIndex)}
    style={styles.modalDropdownStyles}
    dropdownStyle={styles.dropdownStyle}
    dropdownTextStyle={styles.dropdownTextStyle}
    dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
    <View style={styles.dropdownElementContainer}>
      <View style={styles.dropdownDataElemint}>
        <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
      </View>
      <View style={styles.dropdownArrow}>
        <Ionicons name='md-arrow-dropdown' size={20}/>
      </View>
    </View>
  </ModalDropdown>
);

const ElementFooter = ({ okButtontext, onClickOk }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{okButtontext}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => SLA Breach\n', state.delivery);
  const Language = state.lang.current_lang;
  const currentWorkOrderState = state.delivery.workOrderState;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const slaBreachTimeDisplay = state.delivery.currentWorkOrderSlaTime;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;

  return {
    Language,
    currentWorkOrderState,
    slaBreachTimeDisplay,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderBasicInfo
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerTitle: {
    flex: 1,
    paddingTop: 18,
  },
  containerSlaTime: {
    flex: 1,
    paddingTop: 0
  },
  containerDropDown: {
    flex: 2,
    paddingTop: 5,
    marginTop: 5,

  },
  containerBottom: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  titleText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 10,
    paddingBottom: 5,
    margin: 5
  },
  slaTimeText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 5,
    color: Colors.colorRed
  },

  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },

  dropDownTextContainer: {
    flex: 0.6,
  },
  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18

  },
  //modal dropdown styles
  modalDropdownStyles: {
    width: Dimensions
      .get('window')
      .width
  },
  dropdownStyle: {
    width: Dimensions
      .get('window')
      .width - 30,
    marginLeft: 15,
    height: 120,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  }
});

export default connect(mapStateToProps, actions)(SlaBreach);
