/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Friday, 2th Feb 2018 3:53:37 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 7:25:09 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity,
  Image,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import Utills from '../../utills/Utills';
import ActIndicator from './ActIndicator';
import strings from '../../Language/Delivery';
import { Header } from '../../components/others';
import img1 from '../../../images/delivery/pending.png';
import img2 from '../../../images/delivery/dispatched.png';
import img3 from '../../../images/delivery/in_progress.png';

const Utill = new Utills();

class DeliveryHome extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      workOrderCount: [],
      apiLoading: true,
      api_error: false,
      api_error_message: 'Error occured when loading the data',
      locals: {
        screenTitle: 'DELIVERY APP',
        network_error_message: 'Network Error, \nPlease check your intenet connection',
        backMessage: 'Do you want to cancel the operation & go back ?',
        api_error_message: 'Error occured while loading the data',
        continue: 'CONTINUE',
        cancel: 'CANCEL'
      }
    };
  }

  setIndicater = (loadingState = true) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });

  }

  getOrderCount = () => {
    console.log('xxx getOrderCount');

    const data = {
      dummy: 'dummy'
    };
    //reset old Wom API details
    this
      .props
      .resetWomApiOrderData();
    this
      .props
      .setWorrkOrderStatus(Constants.workOrdrState.PENDING);
    this
      .props
      .reSetWorkOrderCounts();
    this
      .props
      .setCurrentWorkOrderSlaBreachStatus(false, '');

    Utill.apiRequestPost('getWorkOrdersCounts', 'womDelivery', 'delivery', data, this.getOrderCountSuccessCb, this.getOrderCountFailureCb, this.getOrderCountExcb);

  }

  getOrderCountExcb = (error) => {
    console.log('getOrderCountExcb', error);
    if (error == 'Network Error') {
      error = this.state.locals.network_error_message;
    } else {
      error = this.state.locals.api_error_message;
    }
    this.setState({ api_error_message: error });
    setTimeout(() => {
      this.setIndicater(false);
      this.setState({ api_error: true });
    }, 1000);
  }

  getOrderCountSuccessCb = (response) => {
    console.log('getOrderCountSuccessCb');
    console.log('xxx getOrderCountSuccessCb', response.data.info);
    let apiData = response.data.info;
    let workOrderCount = [
      {
        key: 0,
        imageUri: img1,
        value: "Pending Work Order",
        count: apiData.pending.pending_count
      }, {
        key: 1,
        imageUri: img2,
        value: "Dispatched Work Order",
        count: apiData.dispatched.dispatched_count
      }, {
        key: 2,
        imageUri: img3,
        value: "In-progress Work Order",
        count: apiData.in_progress.in_progress_count
      }
    ];
    this.setIndicater(false);

    let totalWoCount = {
      PENDING: apiData.pending.pending_count,
      DISPATCHED: apiData.dispatched.dispatched_count,
      INPROGRESS: apiData.in_progress.in_progress_count
    };
    this
      .props
      .setWorkOrderCounts(totalWoCount);
    this.setState({ workOrderCount: workOrderCount });
  }

  getOrderCountFailureCb = (response) => {
    console.log('getOrderCountFailureCb');
    console.log('xxx getOrderCountFailureCb', response);
    this.setState({ api_error_message: response.data.error });
    this.setState({ api_error: true });
    this.setIndicater(false);
    //Utill.showAlertMsg(JSON.stringify(response.data.error));
  }

  componentWillMount() {
    this.getOrderCount();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setIndicater(false);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    const me = this.props;
    me.resetMobileActivationState();
  }

  getWorkorderDetails = (job_status = "DISPATCHED") => {
    console.log('getWorkorderDetails', job_status);
    this.setIndicater(true);
    const data = {
      job_status: job_status
    };

    Utill.apiRequestPost('listWorkOrdersByJobStatus', 'womDelivery', 'delivery', data, this.getWorkorderDetailsSucessCb, this.getWorkorderDetailsFailureCb, this.exCb);
  }

  exCb = (error) => {
    console.log('exCb', error);
    if (error == 'Network Error') {
      error = this.state.locals.network_error_message;
    } else {
      error = this.state.locals.api_error_message;
    }
    setTimeout(() => {
      this.setIndicater(false);
      //this.setState({api_error: false});
      Utill.showAlertMsg(error);
    }, 1000);
  }

  getWorkorderDetailsFailureCb = (response) => {
    console.log('getWorkorderDetailsFailureCb');
    console.log('xxx getWorkorderDetailsFailureCb', response.data);
    this.setIndicater(false);
    const navigatorOb = this.props.navigator;
    if (this.props.currentWorkOrderState == Constants.workOrdrState.DISPATCHED) {
      navigatorOb.resetTo({ title: "No Work Order Screen", screen: 'DialogRetailerApp.views.NoWorkOrderScreen' });
    } else {
      Utill.showAlertMsg(this.state.locals.api_error_message);
    }

  }

  getWorkorderDetailsSucessCb = (response) => {
    console.log('getWorkorderDetailsSucessCb');
    this.setIndicater(false);
    console.log('xxx getWorkorderDetailsSucessCb', response.data.info);
    const apiData = response.data.info;
    console.log('xxx workOrderId', apiData[0].orderId);
    console.log('xxx jobId', apiData[0].job_id);
    console.log('xxx SLAbreachStatus', apiData[0].breachTime);
    console.log('xxx slaLapse', apiData[0].slaLapse);
    console.log('xxx jobStatus', apiData[0].job_status);

    this
      .props
      .setCurrentWorkOrderId(apiData[0].orderId);
    this
      .props
      .setCurrentWorkOrderJobId(apiData[0].job_id);
    this
      .props
      .setCurrentWorkOrderJobStatus(apiData[0].job_status);
    this
      .props
      .setCurrentWorkOrderSlaBreachStatus(apiData[0].breachTime, apiData[0].slaLapse);
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'DISPATCHED WORK ORDER',
      screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen',
      passProps: {
        workOrderId: apiData[0].orderId,
        jobId: apiData[0].job_id,
        jobStatus: apiData[0].job_status,
        workOrdrState: Constants.workOrdrState.DISPATCHED,
        womListType: Constants.workOrdrState.DISPATCHED
      }
    });
  }

  showOrderDetail = (index) => {
    console.log('xxx showOrderDetail', index);
    const navigatorOb = this.props.navigator;
    if (index == 0) {
      this
        .props
        .setWorrkOrderStatus(Constants.workOrdrState.PENDING);

      if (this.props.totalWoCount.PENDING == 1) {
        this.getWorkorderDetails('INITIAL');
      } else {
        navigatorOb.push({
          title: 'PENDING WORK OREDERS',
          screen: 'DialogRetailerApp.views.PendingOrderDeliveryIndex',
          passProps: {
            workOrdrState: Constants.workOrdrState.PENDING,
            womListType: Constants.workOrdrState.PENDING
          }
        });
      }
    } else if (index == 1) {
      this
        .props
        .setWorrkOrderStatus(Constants.workOrdrState.DISPATCHED);
      this.getWorkorderDetails('DISPATCHED');

    } else {
      this
        .props
        .setWorrkOrderStatus(Constants.workOrdrState.INPROGRESS);
      if (this.props.totalWoCount.INPROGRESS == 1) {
        this.getWorkorderDetails('IN_PROGRESS');
      } else {
        navigatorOb.push({
          title: 'IN-PROGRESS WORK OREDERS',
          screen: 'DialogRetailerApp.views.PendingOrderDeliveryIndex',
          passProps: {
            workOrdrState: Constants.workOrdrState.INPROGRESS,
            womListType: Constants.workOrdrState.INPROGRESS
          }
        });
      }
    }
  }


getCircleStyles = (count) => {
  console.log('xxx getCircleStyles', count);
  let dimention = 56;
  if (count < 10) {
    dimention = 40;
  } else if (count >= 10 && count < 100) {
    dimention = 50;
  } else {
    dimention = 60;
  }
  //Not using
  dimention = 48;
  return { 
    height: dimention, 
    width: dimention, 
    borderRadius: dimention/2 
  };
}

  renderListItem = ({ item, index }) => (
    <View style={styles.container}>
      <TouchableOpacity
        key={index}
        style={styles.cardContainer}
        underlayColor={Colors.underlayColor}
        onPress={() => this.showOrderDetail(index)}>
        <View style={styles.leftImageContainer}>
          <Image source={item.imageUri} style={styles.orderImage} resizeMode="cover"/>
        </View>
        <View style={styles.middleTextContainer}>
          <Text style={styles.innerText}>{item.value}
          </Text>
        </View>
        <View style={styles.rightImageContainer}>
          {item.count !== 0
            ? <View style={[styles.orderCountContainer, this.getCircleStyles(item.count)]}>
              <Text style={styles.orderCountText}>
                {item.count}
              </Text>
            </View>
            : <View/>
          }
        </View>
      </TouchableOpacity>
    </View>
  );

  render() {
    console.log(this.state.workOrderCount);
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    let screenContent;
    if (!this.state.api_error) {
      screenContent = (<FlatList
        data={this.state.workOrderCount}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
        scrollEnabled={false}
        refreshing={false}
        onRefresh={this.getOrderCount}/>);
    } else {
      screenContent = (
        <View style={styles.errorDescView}>
          <Text style={styles.errorDescText}>
            {this.state.api_error_message}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <View style={styles.dummyMargin}/>
        {screenContent}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY ===> MainPage ', state.delivery);
  console.log('****** REDUX STATE :: DELIVERY => MainPage ', JSON.stringify(state.delivery));
  const Language = state.lang.current_lang;
  const workOrderState = state.delivery.workOrderState;
  const totalWoCount = state.delivery.totalWoCount;
  const currentWorkOrderState = state.delivery.workOrderState;
  return { Language, workOrderState, totalWoCount, currentWorkOrderState };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 4,
    marginLeft: 6,
    marginRight: 6,
    padding: 5,
    borderColor: Colors.borderLineColor,
    borderBottomWidth: 0.5,
    borderRadius: 7,
    backgroundColor: Colors.colorYellow,
    alignItems: 'center'
  },
  leftImageContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 5
  },
  dummyMargin: {
    height: 8
  },
  orderImage: {
    alignSelf: 'center',
    width: undefined,
    height: undefined,
    margin: 5,
    padding: 25,
    justifyContent: 'center',
    backgroundColor: Colors.colorTransparent

  },
  middleTextContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  rightImageContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    paddingRight: 5
  },

  orderCountContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    padding: 6,
    height: 56,
    width: 56,
    borderRadius: 28,
    backgroundColor: Colors.colorRed
  },

  orderCountText: {
    textAlign: 'center',
    fontSize: Styles.delivery.notificationFontSize,
    color: Colors.colorWhite,
    fontWeight: '500',
  },
  innerText: {
    textAlign: 'center',
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '400',
    color: Colors.colorBlack,
    padding: 8
  },
  errorDescView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorDescText: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: 'bold',
    color: Colors.black
  }
});

export default connect(mapStateToProps, actions)(DeliveryHome);
