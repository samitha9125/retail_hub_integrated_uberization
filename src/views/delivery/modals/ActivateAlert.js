import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
import Constants from '../../../config/constants';
import Utills from '../../../utills/Utills';
import imageErrorIcon from '../../../../images/common/cross.png';

const Utill = new Utills();
class ActivateAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      locals: {
        request_you_send_activation_failed: 'The request you sent for activation is failed',
        'revisit': 'REVISIT',
        'retry': 'RETRY'
      }
    };
  }

  exCb = (error) => {
    console.log('exCb', error);
    this.props.deliveryHideLoadingIndicator();
    Utill.showAlertMsg('System Error');
  }

  updateWorkOrderSuccessCb = (response) => {
    console.log('updateWorkOrderSuccessCb');
    this.props.deliveryHideLoadingIndicator();
    const navigatorOb = this.props.navigator;
    console.log('xxx updateWorkOrderSuccessCb', response.data.info);
    this
      .props
      .setWorrkOrderStatus(Constants.workOrdrState.PENDING);  
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
  }

  updateWorkOrderFailureCb = (response) => {
    console.log('updateWorkOrderFailureCb');
    this.props.deliveryHideLoadingIndicator();
    console.log('xxx updateWorkOrderFailureCb', response);
    Utill.showAlertMsg(JSON.stringify(response.data.error));
  }

  updateWorkOrder = (order_id, job_id, updateType, status) => {
    console.log('updateWorkOrder');
    const data = {
      order_id: order_id,
      job_id: job_id,
      command: updateType,
      status: status,
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id
    };

    this.props.deliveryShowLoadingIndicator();
    Utill.apiRequestPost('updateWorkOrder', 'womDelivery', 'delivery', data, this.updateWorkOrderSuccessCb, this.updateWorkOrderFailureCb, this.exCb);
  }

  onBnPressRevisit = () => {
    const navigatorOb = this.props.navigator;
    this.props.getResetImagesMobileAct();
    this.props.genaralResetSimApiStatus();
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this.updateWorkOrder(this.props.currentWorkOrderId, this.props.currentWorkOrderJobId, 'END', 'REVISIT_REQUIRED');
  }

  onBnPressReTry = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  render() {
    const { currentWorkOrderId, currentWorkOrderJobId, error } = this.props;
    console.log('workOrderId=>', currentWorkOrderId);
    console.log('jobId=>', currentWorkOrderJobId);
    console.log('error=>', error);
    return (
      <View style={styles.containerOverlay}>
        <View style={styles.topContainer}/>
        <View style={styles.modalContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.alertImageContainer}>
              <Image source={imageErrorIcon} style={styles.alertImageStyle}/>
            </View>
            <View style={styles.descriptionContainer}>
              <Text style={styles.workOrderId}>Work Order No : {currentWorkOrderId}</Text>
              <Text style={styles.descriptionText}>{error}</Text>
            </View>
            <View style={styles.bottomCantainerBtn}>
              <View style={styles.dummyView}/>
              <TouchableOpacity
                onPress={() => this.onBnPressRevisit()}
                style={styles.bottomBtnCancel}>
                <Text style={styles.bottomCantainerCancelBtnTxt}>
                  {this.state.locals.revisit}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onBnPressReTry()}
                style={styles.bottomBtn}>
                <Text style={styles.bottomCantainerBtnTxt}>
                  {this.state.locals.retry}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}/>
      </View>
    );

  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => Activate\n', state.delivery);
  const Language = state.lang.current_lang;
  const workOrderState = state.delivery.workOrderState;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderJobStatus = state.delivery.currentWorkOrderJobStatus;
  const currentWorkOrderSlaBreachStatus = state.delivery.currentWorkOrderSlaBreachStatus;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;

  return {
    Language,
    workOrderState,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderJobStatus,
    currentWorkOrderSlaBreachStatus,
    currentWorkOrderBasicInfo
  };
};

const styles = StyleSheet.create({

  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor
  },

  topContainer: {
    flex: 0.4
  },
  modalContainer: {
    flex: 1.5
  },

  bottomContainer: {
    flex: 0.6
  },

  innerContainer: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa,
    padding: 12,
    paddingBottom: 20,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 13,
    marginRight: 13
  },

  alertImageContainer: {
    flex: 0.7,
    alignItems: 'center',
    padding: 5
  },

  alertImageStyle: {
    width: 85,
    height: 85,
    marginTop: 5,
    marginBottom: 5
  },

  descriptionContainer: {
    flex: 1.5,
    flexDirection: 'column',
    paddingTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 5,
    marginTop: 10
  },
  descriptionText: {
    fontSize: Styles.delivery.modalTextFontSize,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontWeight: '100'

  },
  workOrderId: {
    alignItems: 'flex-start',
    fontSize: Styles.delivery.modalFontSize,
    textAlign: 'left',
    color: Colors.colorBlack,
    paddingTop: 5,
    marginLeft: 10,
    marginBottom: 15,
    paddingBottom: 5,
    marginRight: 10,
    marginTop: 20,
    fontWeight: 'bold'
  },

  bottomCantainerBtn: {
    flex: 0.7,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 10
  },

  dummyView: {
    flex: 1.5,
    justifyContent: 'center'
  },

  bottomBtnCancel: {
    // flex: 1,
    marginRight: 20,
    justifyContent: 'center'
  },
  bottomBtn: {
    // flex: 1,
    marginRight: 5,
    justifyContent: 'center'
  },
  bottomCantainerBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorRed
  },
  bottomCantainerCancelBtnTxt: {
    fontSize: Styles.delivery.modalFontSize,
    alignSelf: 'center',
    color: Colors.colorBlack
  }
});

export default connect(mapStateToProps, actions)(ActivateAlert);
