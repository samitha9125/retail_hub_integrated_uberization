import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  TouchableOpacity
} from 'react-native';
import CheckBox from 'react-native-check-box';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import strings from '../../Language/Delivery';

const Utill = new Utills();

class CustomerVerification extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      nicFaceMatched: false,
      nicNumberMatched: false,
      apiLoading: false,
      locals: {
        screenTitle: 'IN-PROGRESS WORK ORDER',
        customerVerification: 'Customer Verification',
        continue: 'PASS',
        cancel: 'FAIL',
        backMessage: 'Do you want to cancel the operation & go back?'
      }
    };
  }

  componentWillMount() {}

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setIndicator(nextProps.showLoadingIndicater);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    this
      .props
      .getResetImagesMobileAct();
    //TODO
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'PENDING WORK ORDER',
      screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen',
      passProps: {
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId
      }
    });
  }

  setIndicator = (loadingState = true) => {
    console.log('xxx setIndicator');
    this.setState({ apiLoading: loadingState });
  }

  goToConfirmationPage = () => {
    console.log('xxx goToConfirmationPage');
    if (!this.state.nicNumberMatched) {
      Utill.showAlertMsg('Please check ' + this.props.idName + ' number');
      return;
    }
    if (!this.state.nicFaceMatched) {
      Utill.showAlertMsg('Please check Customer Face');
      return;
    }

    const validateUrl = { 
      action: 'validateSimNumber', 
      controller : 'serialValidation', 
      module:  'delivery' 
    };

    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'SCAN SIM',
      screen: 'DialogRetailerApp.views.BarcodeScannerCommonScreen',
      passProps: {
        onReturnView: 'DialogRetailerApp.views.CustomerConfirmationScreen',
        onReturnViewTitle: 'IN-PROGRESS WORK ORDER',
        validateUrl:validateUrl, 
        hasNextScreen: true,
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId,
        dataProps: {
          workOrderDetails: this.props.wom_api_order_details,
          aditionalData: this.props.wom_api_order_addionalDetails,
          customerDetail: this.props.wom_api_order_customerDetails
        }
      }
    });
    navigatorOb.pop();
  }

  changeCheckbox1 = () => {
    console.log('xxx changeCheckbox1');
    this.setState({
      nicNumberMatched: !this.state.nicNumberMatched
    });
  }

  changeCheckbox2 = () => {
    console.log('xxx changeCheckbox2');
    this.setState({
      nicFaceMatched: !this.state.nicFaceMatched
    });

  }

  onPressFail = () => {
    console.log('onPressFail :: workOrderId', this.props.currentWorkOrderId);
    console.log('onPressFail :: jobId', this.props.currentWorkOrderJobId);
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      title: '',
      screen: 'DialogRetailerApp.modals.RejectAlertModal',
      overrideBackPress: true,
      passProps: {
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId,
        insideDetailView: true
      }
    });
  }

  render() {
    let activateButtonColor;
    if (this.state.nicNumberMatched && this.state.nicFaceMatched) {
      console.log('xxx enableActivateButton :: true');
      activateButtonColor = {
        backgroundColor: Colors.btnActive
      };
    } else {
      console.log('xxx enableActivateButton :: false');
      activateButtonColor = {
        backgroundColor: Colors.btnDeactive
      };
    }
    let customerIdValue = '';
    if (this.props.wom_api_order_customerDetails !== undefined && this.props.wom_api_order_customerDetails !== null) {
      customerIdValue = this.props.wom_api_order_customerDetails.value;
    }

    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    const checkboxTxt1 = 'Customer ' + this.props.idName + ' number matched';
    const checkboxTxt2 =  'Customer face matched with ' + this.props.idName + ' photo';

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <ElementTitle titleText={this.state.locals.customerVerification}/>
        <ElementItem leftTxt={this.props.idType} rightTxt={customerIdValue}/>
        <View style={styles.containerCheckbox}>
          <CheckBoxItem
            labelText={checkboxTxt1}
            onClick={() => this.changeCheckbox1()}
            isChecked={this.state.nicNumberMatched}/>
          <CheckBoxItem
            labelText={checkboxTxt2}
            onClick={() => this.changeCheckbox2()}
            isChecked={this.state.nicFaceMatched}/>
        </View>
        <View style={styles.containerBottom}>
          <ElementFooter
            cancelBtnText={this.state.locals.cancel}
            okButtonText={this.state.locals.continue}
            onClickCancel={() => this.onPressFail()}
            onClickOk={() => this.goToConfirmationPage()}
            activateColor={activateButtonColor}/>
        </View>
      </View>
    );
  }
}

const ElementTitle = ({ titleText }) => (
  <View style={styles.containerTitle}>
    <Text style={styles.titleText}>
      {titleText}
    </Text>
  </View>
);

const ElementItem = ({ leftTxt, rightTxt }) => (
  <View style={styles.containerNic}>
    <View style={styles.elementLeft}>
      <Text style={styles.nicLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.nicMiddleText}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.nicRightText}>
        {rightTxt}</Text>
    </View>
  </View>
);

const CheckBoxItem = ({ labelText, onClick, isChecked }) => (
  <View style={styles.checkView}>
    <View>
      <CheckBox style={styles.checkBox} onClick={onClick} isChecked={isChecked}/>
    </View>
    <View style={styles.checkViewLabelContainer}>
      <Text style={styles.checkViewLabelTxt}>
        {labelText}
      </Text>
    </View>
  </View>
);

const ElementFooter = ({ cancelBtnText, okButtonText, onClickCancel, onClickOk, activateColor }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonCancelContainer}
      onPress={onClickCancel}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{cancelBtnText}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={[styles.buttonContainer, activateColor]}
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{okButtonText}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => CustomerVerification\n', state.delivery);
  const Language = state.lang.current_lang;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;
  const showLoadingIndicater = state.delivery.showLoadingIndicater;
  const idType = currentWorkOrderBasicInfo.idNumber.type == 'PP' ? 'Passport No' : 'NIC No';
  const idName = currentWorkOrderBasicInfo.idNumber.type == 'PP' ? 'Passport' : 'NIC';

  return {
    Language,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderBasicInfo,
    showLoadingIndicater,
    idType,
    idName
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerTitle: {
    flex: 1,
    paddingTop: 18
  },
  containerNic: {
    flex: 0.7,
    flexDirection: 'row',
    alignItems: 'center',
    margin: 15,
    marginBottom: 20,
    padding: 5,
    borderWidth: 3,
    borderRadius: 2,
    borderColor: Colors.colorYellow
  },
  containerCheckbox: {
    flex: 2,
    paddingTop: 5,
    marginTop: 5
  },
  containerBottom: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  titleText: {
    fontSize: Styles.delivery.titleFontSize,
    textAlign: 'center',
    padding: 10,
    margin: 5
  },
  elementLeft: {
    flex: 1
  },
  elementMiddle: {
    flex: 0.2
  },
  elementRight: {
    flex: 1.8
  },
  nicLeftTxt: {
    fontSize: Styles.delivery.nicLableFontSize,
    color: Colors.colorBlackDelivery,
    textAlign: 'center',
    padding: 5
  },
  nicMiddleText: {
    fontSize: Styles.delivery.nicLableFontSize,
    textAlign: 'center',
    color: Colors.colorBlackDelivery,
    padding: 5
  },
  nicRightText: {
    fontSize: Styles.delivery.nicLableFontSize,
    textAlign: 'center',
    color: Colors.colorBlackDelivery,
    fontWeight: '500',
    padding: 5,
    marginRight: 10
  },
  checkView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10,
    marginBottom: 15
  },
  checkBox: {
    padding: 5,
    marginLeft: 5
  },
  checkViewLabelContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  checkViewLabelTxt: {
    fontSize: Styles.delivery.checkboxFontSize,
    padding: 5,
    marginRight: 10
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 0.3
  },

  buttonCancelContainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    margin: 5,
    marginRight: 30,
    alignSelf: 'flex-end'

  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    // marginLeft: 5,
    margin: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  }
});

export default connect(mapStateToProps, actions)(CustomerVerification);
