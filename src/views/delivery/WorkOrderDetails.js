import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  TouchableOpacity,
  ScrollView,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Constants from '../../config/constants';
import strings from '../../Language/Delivery';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';

const Utill = new Utills();

class ActivationStatusDetails extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      userData: '',
      apiLoading: true,
      resumit: true,
      isDisibleButtons: false,
      locals: {
        screenTitle: '',
        titleOrders: 'WORK ORDER',
        titleDispatched: 'DISPATCHED WORK ORDER',
        titlePending: 'PENDING WORK ORDER',
        titleInprogess: 'IN-PROGESS WORK ORDER',
        cancel: 'CANCEL',
        dispatch: 'DISPATCH',
        unDispatch: 'UNDISPATCH',
        start: 'START',
        reject: 'REJECT',
        proceed: 'PROCEED JOB',
        cannot_proceed_error_msg: 'Data is invalid, Can not Proceed the Order.!',
        network_error_message: 'Network Error, \nPlease check your intenet connection',
        backMessage: 'Do you want to cancel the operation & go back?',
        api_error_message: 'Error occured while loading the data',
        confirmUndispatch: 'Do you want to undispatch the work order?'
      }
    };
  }

  componentWillMount() {
    const currentWorkOrderState = this.props.currentWorkOrderState;
    console.log('currentWorkOrderState', currentWorkOrderState);
    this.props.resetWomApiOrderData();

    const data = {
      order_id: this.props.currentWorkOrderId,
      job_id: this.props.currentWorkOrderJobId,
      job_status: this.props.currentWorkOrderJobStatus,
      cannot_proceed_error_msg: this.state.locals.cannot_proceed_error_msg
    };

    this.props.getWorkOrderDetails(data);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setIndicater(nextProps.showLoadingIndicater);
    console.log('componentWillReceiveProps :: isDeliveryApiFail', nextProps.isDeliveryApiFail);
    if (nextProps.isDeliveryApiFail) {
      console.log('componentWillReceiveProps :: isDeliveryApiFail :: true');
      this.disableButton(true);
    } else {
      console.log('componentWillReceiveProps :: isDeliveryApiFail :: false');
      this.disableButton(false);
    }
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    if (!this.state.apiLoading) {
      console.log('xxx handleBackButtonClick - show go back alert');
      Alert.alert('', this.state.locals.backMessage, [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => this.okHandler()
        }
      ], { cancelable: true });
    } else {
      console.log('xxx handleBackButtonClick - still loading');
    } 
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const currentWorkOrderState = this.props.currentWorkOrderState;
    console.log('@@@@ currentWorkOrderState', currentWorkOrderState);
    const navigatorOb = this.props.navigator;
    if (currentWorkOrderState == Constants.workOrdrState.PENDING) {
      navigatorOb.resetTo({
        title: 'PENDING WORK OREDERS',
        screen: 'DialogRetailerApp.views.PendingOrderDeliveryIndex',
        passProps: {
          workOrdrState: Constants.workOrdrState.PENDING,
          womListType: Constants.workOrdrState.PENDING
        }
      });

    } else if (currentWorkOrderState == Constants.workOrdrState.INPROGRESS) {
      navigatorOb.resetTo({
        title: 'IN-PROGRESS WORK OREDERS',
        screen: 'DialogRetailerApp.views.PendingOrderDeliveryIndex',
        passProps: {
          workOrdrState: Constants.workOrdrState.INPROGRESS,
          womListType: Constants.workOrdrState.INPROGRESS
        }
      });
    } else {
      navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
    }
  }

  setIndicater = (loadingState = true) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });

  }

  disableButton = (disableButton = false) => {
    console.log('xxx disableButton', disableButton);
    this.setState({ isDisibleButtons: disableButton });

  }

  //Commmon exception callback
  exCb = (error) => {
    console.log('exCb');
    if (error == 'Network Error') {
      error = this.state.locals.network_error_message;
    } else {
      error = this.state.locals.api_error_message;
    }
    setTimeout(() => {
      this.setIndicater(false);
      this.setState({ api_error: true });
      this.setState({ isDisibleButtons: true });
      Utill.showAlertMsg(error);
    }, 1000);

  }

  //Commmon Failure callback for updateWorkOrder
  updateWorkOrderFailureCb = (response) => {
    console.log('updateWorkOrderFailureCb');
    console.log('xxx updateWorkOrderFailureCb', response.data);
    this.setIndicater(false);
    //Utill.showAlertMsg(JSON.stringify(response.data.error));
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      title: 'General Alert Modal',
      screen: 'DialogRetailerApp.modals.GeneralAlertModal',
      overrideBackPress: true,
      passProps: {
        error: response.data.error,
        response: response.data,
        messageType: 'error'
      }
    });
  }

  //updateWorkOrder
  updateWorkOrder = (order_id, job_id, updateType, sucessCb) => {
    console.log('updateStartWorkOrder');
    const data = {
      order_id: order_id,
      job_id: job_id,
      command: updateType,
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id
    };
    //TODO: delivery
    Utill.apiRequestPost('updateWorkOrder', 'womDelivery', 'delivery', data, sucessCb, this.updateWorkOrderFailureCb, this.exCb);
  }

  //Proceed work Order - SuccessCb
  updateWorkOrderProceedSuccessCb = (response) => {
    console.log('updateWorkOrderProceedSuccessCb');
    console.log('xxx updateWorkOrderProceedSuccessCb', response.data.info);
    this.setIndicater(false);
    const apiData = response.data.info;
    this.setState({ apiData: apiData });
    this.props.setWorrkOrderStatus(Constants.workOrdrState.INPROGRESS);
  }

  //Start work Order - SuccessCb
  updateWorkOrderStartSuccessCb = (response) => {
    console.log('updateWorkOrderStartSuccessCb');
    console.log('xxx updateWorkOrderStartSuccessCb', response.data.info);
    this.setIndicater(false);
    const apiData = response.data.info;
    this.setState({ apiData: apiData });
    this
      .props
      .setWorrkOrderStatus(Constants.workOrdrState.INPROGRESS);
  }

  //Undispatch work Order - SuccessCb
  updateWorkOrderUndispatchSuccessCb = (response) => {
    console.log('updateWorkOrderUndispatchSuccessCb');
    console.log('xxx updateWorkOrderUndispatchSuccessCb', response.data.info);
    this.setIndicater(false);
    const apiData = response.data.info;
    this.setState({ apiData: apiData });
    this
      .props
      .setWorrkOrderStatus(Constants.workOrdrState.PENDING);
    const navigatorOb = this.props.navigator;
    //navigatorOb.pop({animated: true, animationType: 'fade'});
    navigatorOb.push({
      title: 'PENDING WORK OREDERS',
      screen: 'DialogRetailerApp.views.PendingOrderDeliveryIndex',
      passProps: {
        workOrdrState: Constants.workOrdrState.PENDING,
        womListType: Constants.workOrdrState.PENDING
      }
    });
  }

  onPressDispatch = () => {
    console.log('onPressDispatch');
    const navigatorOb = this.props.navigator;
    navigatorOb.showModal({
      title: '',
      screen: 'DialogRetailerApp.modals.ConfirmAlertModal',
      overrideBackPress: true,
      passProps: {
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId,
        insideDetailView: true
      }
    });
  }
  onPressUnDispatch = () => {
    console.log('onPressUnDispatch');
    Alert.alert('', this.state.locals.confirmUndispatch, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          this.setIndicater(true);
          this.updateWorkOrder(this.props.currentWorkOrderId, this.props.currentWorkOrderJobId, 'UNDISPATCH', this.updateWorkOrderUndispatchSuccessCb);
        }
      }
    ], { cancelable: true });
    
  }
  onPressStart = () => {
    console.log('onPressStart');
    //this.props.currentWorkOrderSlaBreachStatus TODO: Requement change
    if (false) {
      const navigatorOb = this.props.navigator;
      navigatorOb.push({ title: 'Sla Breach Screen', screen: 'DialogRetailerApp.views.SlaBreachScreen', passProps: {} });
    } else {
      this.setIndicater(true);
      this.updateWorkOrder(this.props.currentWorkOrderId, this.props.currentWorkOrderJobId, 'START', this.updateWorkOrderStartSuccessCb);
    }
  }
  onReject = () => {
    console.log('onReject');
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'IN-PROGESS WORK ORDER',
      screen: 'DialogRetailerApp.views.RejectOrderScreen',
      passProps: {
        workOrdrState: this.props.currentWorkOrderState,
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId
      }
    });
  }
  onPressBack = () => {
    console.log('onPressBack');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop({ animated: true, animationType: 'fade' });
  }

  onPressProceed = () => {
    console.log('onPressProceed');
    console.log('onPressProceed :: workOrderId', this.props.currentWorkOrderId);
    console.log('onPressProceed :: jobId', this.props.currentWorkOrderJobId);
    const navigatorOb = this.props.navigator;
    navigatorOb.push({
      title: 'IN-PROGESS WORK ORDER',
      screen: 'DialogRetailerApp.views.CustomerVerificationScreen',
      passProps: {
        workOrdrState: this.props.currentWorkOrderState,
        workOrderId: this.props.currentWorkOrderId,
        jobId: this.props.currentWorkOrderJobId,
        workOrderDetails: this.props.wom_api_order_details,
        aditionalData: this.state.wom_api_order_addionalDetails,
        customerDetail: this.state.wom_api_order_customerDetails
      }
    });
  }

  render() {
    const workOrderDetailsArray = this.props.wom_api_order_details;
    let loadingIndicator;
    let setButton;
    let screenTitle;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }

    if (this.props.currentWorkOrderState == Constants.workOrdrState.PENDING) {
      screenTitle = this.state.locals.titlePending;
      setButton = (<ElementFooterOneButton
        okButtontext={this.state.locals.dispatch}
        onClickOk={() => this.onPressDispatch()}
        disabled={this.state.isDisibleButtons}/>);
    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.DISPATCHED) {
      screenTitle = this.state.locals.titleDispatched;
      setButton = (<ElementFooter
        cancelBtnText={this.state.locals.unDispatch}
        okButtontext={this.state.locals.start}
        onClickCancel={() => this.onPressUnDispatch()}
        onClickOk={() => this.onPressStart()}
        cancelBtnDisabled={this.state.isDisibleButtons}
        okBtnDisabled={this.state.isDisibleButtons}/>);
    } else if (this.props.currentWorkOrderState == Constants.workOrdrState.INPROGRESS) {
      screenTitle = this.state.locals.titleInprogess;
      setButton = (<ElementFooter
        cancelBtnText={this.state.locals.reject}
        okButtontext={this.state.locals.proceed}
        onClickCancel={() => this.onReject()}
        onClickOk={() => this.onPressProceed()}
        okBtnDisabled={this.state.isDisibleButtons}
        cancelBtnDisabled={this.state.isDisibleButtons}/>);
    }
    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={screenTitle}/> 
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <View style={styles.detailsContainer}>
          <ScrollView>
            {workOrderDetailsArray.map((value, i) => <ElementItem key={i} leftTxt={value.name} rightTxt={value.value}/>)}
          </ScrollView>
        </View>
        <View style={styles.containerBottom}>
          {setButton}
        </View>
      </View>
    );
  }
}

const ElementItem = ({ leftTxt, rightTxt }) => (
  <View style={styles.detailElimentContainer}>
    <View style={styles.elementLeft}>
      <Text style={styles.elementLeftTxt}>
        {leftTxt}</Text>
    </View>
    <View style={styles.elementMiddle}>
      <Text style={styles.elementTxt}>
        :</Text>
    </View>
    <View style={styles.elementRight}>
      <Text style={styles.elementTxt}>
        {rightTxt}</Text>
    </View>
  </View>
);

const ElementFooterOneButton = ({
  okButtontext,
  onClickOk,
  disabled = false
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView2}/>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={disabled}>
      <Text style={styles.buttonTxt}>{okButtontext}
      </Text>
    </TouchableOpacity>
  </View>
);

const ElementFooter = ({
  cancelBtnText,
  okButtontext,
  onClickCancel,
  onClickOk,
  cancelBtnDisabled = false,
  okBtnDisabled = false
}) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonCancelContainer}
      onPress={onClickCancel}
      activeOpacity={1}
      disabled={cancelBtnDisabled}>
      <Text style={styles.buttonTxt}>{cancelBtnText}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}
      disabled={okBtnDisabled}>
      <Text style={styles.buttonTxt}>{okButtontext}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => WorkOrderDetails\n', state.delivery);
  const Language = state.lang.current_lang;
  const currentWorkOrderState = state.delivery.workOrderState;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderJobStatus = state.delivery.currentWorkOrderJobStatus;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderSlaBreachStatus = state.delivery.currentWorkOrderSlaBreachStatus;
  const isDisibleButton = wom_api_order_addionalDetails.length == 0;
  const showLoadingIndicater = state.delivery.showLoadingIndicater;
  const isDeliveryApiFail = state.delivery.isDeliveryApiFail;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;

  return {
    Language,
    currentWorkOrderState,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderJobStatus,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderSlaBreachStatus,
    isDisibleButton,
    showLoadingIndicater,
    isDeliveryApiFail,
    currentWorkOrderBasicInfo
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhiteWithAlpa
  },

  detailsContainer: {
    flex: 6,
    borderWidth: 0,
    paddingLeft: 12,
    paddingRight: 12
  },
  containerBottom: {
    backgroundColor: Colors.colorTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },

  detailElimentContainer: {
    flexDirection: 'row',
    borderWidth: 0,
    marginBottom: 10
  },

  elementLeft: {
    flex: 1,
    borderWidth: 0
  },
  elementMiddle: {
    borderWidth: 0,
    marginRight: 10
  },
  elementRight: {
    flex: 1,
    borderWidth: 0
  },
  elementLeftTxt: {
    fontSize: Styles.delivery.defaultFontSize,
    fontWeight: '500'
  },
  elementTxt: {
    fontSize: Styles.delivery.defaultFontSize
  },

  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 0,
    paddingTop: 0,
    paddingBottom: 15,
    marginRight: 0,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderTopColor: Colors.borderLineColor
  },
  dummyView: {
    flex: 0.3
  },
  dummyView2: {
    flex: 1.6
  },

  buttonCancelContainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 48,
    borderRadius: 5,
    padding: 5,
    marginRight: 30,
    paddingTop: 10,
    paddingBottom: 10,
    alignSelf: 'flex-end'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 48,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500'
  }
});

export default connect(mapStateToProps, actions)(ActivationStatusDetails);
