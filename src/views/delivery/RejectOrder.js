import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Text,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as actions from '../../actions';
import Colors from '../../config/colors';
import Styles from '../../config/styles';
import Utills from '../../utills/Utills';
import { Header } from '../../components/others';
import ActIndicator from './ActIndicator';
import strings from '../../Language/Delivery';

const Utill = new Utills();

class RejectOrder extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      userLogged: true,
      apiLoading: true,
      dropDownDataArray: [],
      dropDownDataArrayId: [],
      dropDownIntermediateDataArray: [],
      dropDownIntermediateDataArrayId: [],
      dropDownSelectedReasonText: '',
      dropDownSelectedSubReasonText: '',
      userSelectedItemReasonIndex: -1,
      userSelectedItemSubReasonIndex: -1,
      question_1: -1,
      question_2: -1,
      locals: {
        screenTitle: 'IN-PROGESS WORK ORDER',
        selectionDescription: 'Select the rejection reason from below list',
        selectionSubReasonDescription: 'Select the sub rejection reason from below list',
        continue: 'SUBMIT',
        backMessage: 'Do you want to cancel the operation & go back?'

      }

    };
  }

  setIndicater = (loadingState = false) => {
    console.log('xxx setIndicater');
    this.setState({ apiLoading: loadingState });
  }

  componentWillMount() {
    const data = {
      some_data: 'some_data'
    };
    this.setIndicater(true);
    Utill.apiRequestPost('getRejectReason', 'womDelivery', 'delivery', data, this.getRejectReasonSuccessCb, this.getRejectReasonFailureCb, this.exCb);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  //Commmon exception callback
  exCb = (error) => {
    console.log('exCb', error);
    this.setIndicater(false);
    Utill.showAlertMsg('System Error');
  }

  //Commmon Failure callback for  get Reject Reason
  getRejectReasonFailureCb = (response) => {
    console.log('getRejectReasonFailureCb');
    this.setIndicater(false);
    console.log('xxx getRejectReasonFailureCb', response);
    Utill.showAlertMsg(JSON.stringify(response.data.error));
  }

  getRejectReasonSuccessCb = (response) => {
    console.log('getRejectReasonSuccessCb');
    this.setIndicater(false);
    console.log('xxx getRejectReasonSuccessCb', response.data.info);
    const rejectReason = response.data.info;
    try {
      let dropDownDataArray = [];
      let dropDownDataArrayId = [];
      for (let rejectReasonProps in rejectReason) {
        const frejectReasonOb = rejectReason[rejectReasonProps];
        console.log('xxxx frejectReasonOb', frejectReasonOb);
        console.log('xxxx frejectReasonOb.id', frejectReasonOb.id);
        console.log('xxxx frejectReasonOb.description', frejectReasonOb.description);
        dropDownDataArray.push(frejectReasonOb.description);
        dropDownDataArrayId.push(frejectReasonOb.id);
      }
      console.log('xxxx dropDownDataArray', dropDownDataArray);
      console.log('xxxx dropDownDataArrayId', dropDownDataArrayId);
      this.setState({ dropDownDataArray: dropDownDataArray, dropDownDataArrayId: dropDownDataArrayId });
    } catch (error) {
      Utill.showAlertMsg('getRejectReasonSuccessCb :: System Error');
    }

  }

  getRejectIntermediateReasonSuccessCb = (response) => {
    console.log('getRejectIntermediateReasonSuccessCb');
    this.setIndicater(false);
    console.log('xxx getRejectIntermediateReasonSuccessCb', response.data.info);
    const rejectReason = response.data.info;
    try {
      let dropDownIntermediateDataArray = [];
      let dropDownIntermediateDataArrayId = [];
      for (let rejectReasonProps in rejectReason) {
        const rejectReasonOb = rejectReason[rejectReasonProps];
        console.log('xxxx rejectReasonOb', rejectReasonOb);
        console.log('xxxx rejectReasonOb.id', rejectReasonOb.id);
        console.log('xxxx rejectReasonOb.description', rejectReasonOb.description);
        dropDownIntermediateDataArray.push(rejectReasonOb.description);
        dropDownIntermediateDataArrayId.push(rejectReasonOb.id);
      }
      console.log('xxxx dropDownIntermediateDataArray', dropDownIntermediateDataArray);
      console.log('xxxx dropDownIntermediateDataArrayId', dropDownIntermediateDataArrayId);
      this.setState({ dropDownIntermediateDataArray: dropDownIntermediateDataArray, dropDownIntermediateDataArrayId: dropDownIntermediateDataArrayId });
    } catch (error) {
      Utill.showAlertMsg('getRejectIntermediateReasonSuccessCb :: System Error');
    }

  }

  showOrderDetail = () => {
    console.log('xxx showOrderDetail');
    const navigatorOb = this.props.navigator;
    navigatorOb.push({ title: 'Pending Work Orders', screen: 'DialogRetailerApp.views.WorkOrderDetailsScreen' });
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.pop();
  }

  setSelectedReason = (idx, value) => {
    console.log('xxx setSelectedReason', idx);
    console.log('xxx dropDownSelectedReasonText', value);
    this.setState({ userSelectedItemReasonIndex: idx, dropDownSelectedReasonText: value });
    this.setState({
      question_1: this.state.dropDownDataArrayId[parseInt(idx)]
    });

    this.setState({ dropDownIntermediateDataArray: [], userSelectedItemSubReasonIndex: -1, dropDownSelectedSubReasonText: '', question_2: -1 });

    const data = {
      question_id: this.state.dropDownDataArrayId[parseInt(idx)]
    };

    this.setIndicater(true);
    Utill.apiRequestPost('getIntermediateRejectReasons', 'womDelivery', 'delivery', data, this.getRejectIntermediateReasonSuccessCb, this.getRejectReasonFailureCb, this.exCb);

  }
  setSelectedSubReason = (idx, value) => {
    console.log('xxx setSelectedSubReason', idx);
    console.log('xxx dropDownSelectedSubReasonText', value);
    this.setState({ userSelectedItemSubReasonIndex: idx, dropDownSelectedSubReasonText: value });
    this.setState({
      question_2: this.state.dropDownIntermediateDataArrayId[parseInt(idx)]
    });
  }

  submitRejectReason = () => {
    console.log('xxx submitRejectReason');
    if (this.state.question_1 == -1) {
      Utill.showAlertMsg('Please select a reject reason from dropdown');
      return;
    }

    if (this.state.question_2 == -1) {
      Utill.showAlertMsg('Please select a intermediate reject reason from dropdown');
      return;
    }

    const data = {
      order_id: this.props.currentWorkOrderId,
      job_id: this.props.currentWorkOrderJobId,
      reject_reason: this.state.question_1,
      sub_reject_reason: this.state.question_2,
      status: 'REJECTED',
      command: 'END',
      ccbs_order_id: this.props.currentWorkOrderBasicInfo.ccbs_order_id

    };
    this.setIndicater(true);
    Utill.apiRequestPost('updateWorkOrder', 'womDelivery', 'delivery', data, this.submitRejectReasonSuccessCb, this.getRejectReasonFailureCb, this.exCb);

  }

  submitRejectReasonSuccessCb = (response) => {
    console.log('submitRejectReasonSuccessCb');
    this.setIndicater(false);
    console.log('xxx submitRejectReasonSuccessCb', response.data.info);
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: "DELIVERY APP", screen: 'DialogRetailerApp.views.DeliveryMainScreen' });
  }

  render() {
    let loadingIndicator;
    if (this.state.apiLoading) {
      loadingIndicator = (<ActIndicator animating/>);
    } else {
      loadingIndicator = true;
    }
    let subRejectreason;
    let dropDownIntermediatHeight = 160;
    let dropDownHeight = 160;
    if (this.state.dropDownDataArray.length < 4) {
      dropDownIntermediatHeight = this.state.dropDownDataArray.length * 40;
    }

    if (this.state.dropDownIntermediateDataArray.length < 4) {
      dropDownIntermediatHeight = this.state.dropDownIntermediateDataArray.length * 40;
    }

    if (this.state.dropDownIntermediateDataArray.length !== 0) {
      subRejectreason = (
        <View style={styles.containerDropDown}>
          <View style={styles.dropDownTextContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.selectionSubReasonDescription}</Text>
          </View>
          <View style={styles.dropDownContainer}>
            <ElementSelctionMenu
              dynamicDropdownStyle={{
                height: dropDownIntermediatHeight
              }}
              dropDownData={this.state.dropDownIntermediateDataArray}
              placeHolderText={this.state.dropDownSelectedSubReasonText}
              defaultIndex={this.state.userSelectedItemSubReasonIndex}
              onSelect={(idx, value) => this.setSelectedSubReason(idx, value)}/>
          </View>
        </View>
      );
    } else {
      subRejectreason = (<View style={styles.containerDropDown}/>);
    }

    return (
      <View style={styles.container}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.screenTitle}/>
        {/* There may be issue occur in this place - aware evil space issue */}
        {loadingIndicator}
        <View style={styles.containerDropDown}>
          <View style={styles.dropDownTextContainer}>
            <Text style={styles.dropDownText}>{this.state.locals.selectionDescription}</Text>
          </View>
          <View style={styles.dropDownContainer}>
            <ElementSelctionMenu
              dropDownData={this.state.dropDownDataArray}
              dynamicDropdownStyle={{
                height: dropDownHeight
              }}
              placeHolderText={this.state.dropDownSelectedReasonText}
              defaultIndex={this.state.userSelectedItemReasonIndex}
              onSelect={(idx, value) => this.setSelectedReason(idx, value)}/>
          </View>
        </View>
        {subRejectreason}
        <View style={styles.containerBottom}>
          <ElementFooter
            okButtontext={this.state.locals.continue}
            onClickOk={() => this.submitRejectReason()}/>
        </View>
      </View>
    );
  }
}

const ElementSelctionMenu = ({ dropDownData, placeHolderText, onSelect, defaultIndex, dynamicDropdownStyle }) => (
  <ModalDropdown
    options={dropDownData}
    onSelect={(idx, value) => onSelect(idx, value)}
    defaultIndex={parseInt(defaultIndex)}
    style={styles.modalDropdownStyles}
    dropdownStyle={[styles.dropdownStyle, dynamicDropdownStyle]}
    dropdownTextStyle={styles.dropdownTextStyle}
    dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
    <View style={styles.dropdownElementContainer}>
      <View style={styles.dropdownDataElemint}>
        <Text style={styles.dropdownDataElemintTxt}>{placeHolderText}</Text>
      </View>
      <View style={styles.dropdownArrow}>
        <Ionicons name='md-arrow-dropdown' size={20}/>
      </View>
    </View>
  </ModalDropdown>
);

const ElementFooter = ({ okButtontext, onClickOk }) => (
  <View style={styles.bottomContainer}>
    <View style={styles.dummyView}/>
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={onClickOk}
      activeOpacity={1}>
      <Text style={styles.buttonTxt}>{okButtontext}
      </Text>
    </TouchableOpacity>
  </View>
);

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: DELIVERY => RejectOrder\n', state.delivery);
  const Language = state.lang.current_lang;
  const wom_api_order_details = state.delivery.wom_api_order_details;
  const wom_api_order_customerDetails = state.delivery.wom_api_order_customerDetails;
  const wom_api_order_addionalDetails = state.delivery.wom_api_order_addionalDetails;
  const currentWorkOrderId = state.delivery.currentWorkOrderId;
  const currentWorkOrderJobId = state.delivery.currentWorkOrderJobId;
  const currentWorkOrderBasicInfo = state.delivery.currentWorkOrderBasicInfo;

  return {
    Language,
    wom_api_order_details,
    wom_api_order_customerDetails,
    wom_api_order_addionalDetails,
    currentWorkOrderId,
    currentWorkOrderJobId,
    currentWorkOrderBasicInfo
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  containerDropDown: {
    flex: 1,
    paddingTop: 20,
    marginTop: 25
  },
  containerBottom: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  //button styles
  bottomContainer: {
    height: 80,
    alignItems: 'flex-end',
    backgroundColor: Colors.appBackgroundColor,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 0
  },

  dummyView: {
    flex: 1.6,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: Colors.appBackgroundColor,
    height: 45,
    borderRadius: 5,
    padding: 5,
    marginRight: 5,
    alignSelf: 'flex-end'
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnActive,
    height: 45,
    borderRadius: 5,
    marginLeft: 5,
    padding: 5,
    marginRight: 10,
    alignSelf: 'flex-end'
  },
  buttonTxt: {
    textAlign: 'center',
    color: Colors.colorBlack,
    backgroundColor: Colors.colorTransparent,
    fontSize: Styles.delivery.defaultBtnFontSize,
    fontWeight: '500',
  },

  dropDownTextContainer: {
    flex: 0.5
  },
  dropDownContainer: {
    flex: 0.5,
    marginTop: 15
  },

  dropDownText: {
    textAlign: 'left',
    marginLeft: 12,
    marginRight: 20,
    padding: 5,
    marginBottom: 1,
    color: Colors.colorBlack,
    fontSize: 18

  },
  //modal dropdown styles
  modalDropdownStyles: {
    width: Dimensions
      .get('window')
      .width
  },
  dropdownStyle: {
    width: Dimensions
      .get('window')
      .width - 30,
    height: 160,
    marginLeft: 15,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 15,
    marginLeft: 10
  },

  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginLeft: 15,
    marginRight: 18,
    borderWidth: 1,
    borderColor: Colors.colorTransparent,
    borderBottomColor: Colors.borderColorGray
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 15
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: 15
  }
});

export default connect(mapStateToProps, actions)(RejectOrder);
