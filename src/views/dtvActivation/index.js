/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Thursday, 4th October 2018 12:08:32 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 4th October 2018 12:08:39 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Alert,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import {  Colors, Constants  } from '../../config';
import { Header } from '../common/components/Header';
import ActivityIndicator from '../common/components/ActivityIndicator';
import TopSelectionContainer from './TopSelectionContainer';
import DtvMainViewContainer from './DtvMainViewContainer';
import strings from '../../Language/LteActivation';
import strings_dtv from '../../Language/DtvActivation';
import StaticFooter from '../dtvActivation/StaticBar';
import { Analytics } from '../../utills';

class DtvActivationMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locals: {
        title: strings_dtv.title,
        loading: strings.loading,
        validating: strings.validating,
        backMessage: strings.backMessage,
        ok: strings.btnOk,
        cancel: strings.btnCancel,
      }
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    Analytics.logFirebaseEvent('action_start', {
      action_name: 'dtv_tile_click',
      action_type: 'button_click',
      additional_details: 'DTV Tile click',
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    const navigatorOb = this.props.navigator;
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
  }

  /**
   * @description Cancel voucher code reservation 
   * @memberof LteMainViewContainer
   */
  cancelVoucherCodeReservation = (previousVoucherCode = '', callback) => {
    console.log('xxx cancelVoucherCodeReservation');

    let me = this;
    let endpoint = {
      action: 'cancelVoucherCode',
      controller: 'dtv',
      module: 'ccapp'
    };
    let releaseOnly = false;
    let voucherCode = previousVoucherCode == '' ? me.props.enteredDTVVoucherCode : previousVoucherCode;

    if (previousVoucherCode !== ''){
      releaseOnly = true;
    }


    if (this.props.is_voucher_cancelled == false && this.props.validatedVoucherCode !== '') {
      console.log('xxx cancelVoucherCodeReservation');
      if (me.props.voucher_code_enabled && me.props.isValidDTVVoucherCode && me.props.validatedVoucherCode !== '') {
        console.log('xxx cancelVoucherCodeReservation :: CANCEL_VOUCHER_CODE');
        let requestParams = {
          "conn_type": me.props.selected_connection_type,
          "lob": Constants.LOB_DTV,
          "cx_identity_no": me.props.idNumber_input_value,
          // "om_campaign_record_no": me.props.om_campaign_record_no,
          // "reservation_no": me.props.reservation_no,
          "reference_no": me.props.reference_no,
          "voucher_code": voucherCode,
        };
        Analytics.logEvent('dtv_cancel_voucher_code_reservation');
        me.props.voucherCodeCancellationApi(requestParams, me, endpoint,releaseOnly);
        me.props.resetValidateVoucherCodeStatus(Constants.LOB_DTV);
        if (callback) callback();
      } else {
        console.log('xxx cancelVoucherCodeReservation :: DO NOTHING');
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title}/>
        {/*loading indicator*/}
        <ActivityIndicator animating={this.props.apiLoading} text={this.props.api_call_indicator_msg}/>
        <View style={styles.container}>
          <KeyboardAwareScrollView
            style={[styles.keyboardAwareScrollViewContainer, styles.additionalStyle]}
            keyboardShouldPersistTaps="always">
            <TouchableWithoutFeedback
              onPress={Keyboard.dismiss}
              accessible={false}
              style={styles.container}>
              <View>
                <View style={styles.containerTop}>
                  <TopSelectionContainer 
                    cancelVoucherCodeReservation={this.cancelVoucherCodeReservation}

                  />
                </View>
                <View style={styles.bottomItemsContainer}>
                  {/* DTV activation view */}
                  <DtvMainViewContainer 
                    cancelVoucherCodeReservation={this.cancelVoucherCodeReservation} 
                    {...this.props}

                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAwareScrollView>
        </View>
        {(this.props.sectionId_validation_status && this.props.section_2_visible) ?  
          <StaticFooter/> 
          :
          <View />
        }
      
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log("#####====== DTV ACTIVATION INDEX =====######\n", JSON.stringify(state.lteActivation));
  console.log("#####====== DTV ACTIVATION INDEX =====######\n", state.dtvActivation);
  const apiLoading = state.dtvActivation.apiLoading;
  const api_call_indicator_msg = state.dtvActivation.api_call_indicator_msg;
  const sectionId_validation_status = state.dtvActivation.sectionId_validation_status;
  const section_2_visible = state.dtvActivation.section_2_visible;  

  const is_voucher_cancelled = state.dtvActivation.is_voucher_cancelled;
  
  const validatedVoucherCode = state.dtvActivation.validatedVoucherCode;
  const voucher_code_enabled = state.dtvActivation.voucher_code_enabled;
  const selected_connection_type = state.dtvActivation.selected_connection_type;
  const idNumber_input_value = state.dtvActivation.idNumber_input_value;
  const reference_no = state.dtvActivation.reference_no;
  const enteredDTVVoucherCode = state.dtvActivation.enteredDTVVoucherCode;
  const isValidDTVVoucherCode = state.dtvActivation.isValidDTVVoucherCode;

  return { 
    apiLoading, 
    api_call_indicator_msg, 
    sectionId_validation_status,
    section_2_visible,
    is_voucher_cancelled,
    validatedVoucherCode,
    voucher_code_enabled,
    selected_connection_type,
    isValidDTVVoucherCode,
    idNumber_input_value,
    reference_no,
    enteredDTVVoucherCode
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },
  keyboardAwareScrollViewContainer: {
    backgroundColor: Colors.appBackgroundColor,
    zIndex: 10000
  },
  bottomItemsContainer: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7
  }
});

export default connect(mapStateToProps, actions)(DtvActivationMain);
