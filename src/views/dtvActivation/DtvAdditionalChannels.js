import React from 'react';
import { Text, View, TouchableOpacity, BackHandler, Alert, StyleSheet, Image, Dimensions, ScrollView, FlatList } from 'react-native';
import RadioButton from 'radio-button-react-native';
import CheckBox from 'react-native-check-box';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Constants, Images, Colors, Styles } from '@Config';
import { Analytics, Screen, FuncUtils } from '@Utils';
import _ from 'lodash';
import { Header } from '../common/components/Header';
import EmptyFeed from '@Components/EmptyFeed';
import ActivityIndicator from '../common/components/ActivityIndicator';
import FilterViewComponent from './components/FilterViewComponent';
import GridSelectionViewComponent from './components/GridSelectionViewComponent';
import SearchableDropdown from './components/SearchableDropdown';
import strings_dtv from '../../Language/DtvActivation';
import Utills from '../../utills/Utills'; 

const Utill = new Utills();

class DtvAdditionalChannels extends React.Component {
  constructor(props) {
    super(props);
    strings_dtv.setLanguage(this.props.Language);
    this.state = {
      expandSwitch: false,
      arrowDown: 'md-arrow-dropdown',
      searchResultSelection:[
        { 
          name: '',
          url:'',
          channel_code:'-99'
        }
      ],
      locals: {
        title: 'DTV ACTIVATION',
        ok: 'OK',
        cancel: 'CANCEL',
        channels: 'Channels',
        packs: 'Packs',
        filterBy: 'Filter by',
        backMessage: 'Do you want to go back ?',
        searchPlaceHolder: 'Search individual channels',
        noChannelFound: 'No search results available',
        Continue: strings_dtv.continueButtonText,
        additional_rental_text: 'Additional daily rental',
        Additional_monthly_rental: 'Additional monthly rental',
        total_rental_text: 'Total monthly rental',
        total_daily_rental: 'Total daily rental',
        selected_pack_includes_channels: 'Selected pack includes channels you have choosen already.',
        we_have_removed_hem_from_the_individual_channel: 'We have removed them from the individual channel list.',
        rs_label: 'Rs',
        tax: 'Tax',
        pack: 'pack',
        no_channel_message: 'There is no additional channels ',
        no_pack_message: 'There is no additional packs',
      },
  
      searchResult:[],
      expandPackItem : new Array(FuncUtils.getArraySize(this.props.channel_pack_list_data))
    };

    this.additional_channel_list_data_ob = _.cloneDeep(this.props.additional_channel_list_data);
    this.selected_channel_n_packs_ob = _.cloneDeep(this.props.selected_channel_n_packs);

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.props.dtvSetChannelType(Constants.DTV_INDIVIDUAL_CHANNEL);
    this.props.dtvSetChannelGenre(this.getDefaultSelectedGenre());
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  updateState = (state) => {
    this.setState(state);
    this.props.dtvForceUpdateView();

  }

  handleBackButtonClick = () => {
    console.log('xxx handleBackButtonClick');
    console.log('**************************************************');
    console.log(JSON.stringify(this.props));
    console.log(JSON.stringify(this.state));
    console.log('**************************************************');
    Alert.alert('', this.state.locals.backMessage, [
      {
        text: this.state.locals.cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: this.state.locals.ok,
        onPress: () => this.okHandler()
      }
    ], { cancelable: true });
    return true;
  }

  okHandler = () => {
    console.log('xxx okHandler');
    let _this = this;
    _this.popView();
    _this.props.dtvSetChannelsAndPacksPreviousValues(this.additional_channel_list_data_ob, this.selected_channel_n_packs_ob);
    _this.props.dtvForceUpdateView();
  }

  popView = () => {
    console.log('xxx popView');
    let _this = this;
    const navigatorOb = _this.props.navigator;
    navigatorOb.pop();
  }

  onPressContinueButton = ( totalPaymentProps ) => { 
    console.log('xxx onPressContinueButton');
    let _this = this;
    _this.popView();
    _this.props.setSelectedChannelTotalPayment(totalPaymentProps);
    _this.props.dtvForceUpdateView();
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_press_continue_channel_selection',
      item_id: 'btn_continue_channel_selection' ,
      content_type: 'button',
      additional_details: 'DTV press continue button' ,
    }); 
    
  }

  /**
   * @description Show notices alert
   * @param {Object} message
   * @memberof DtvAdditionalChannels
   */
  showNoticesAlert = (packName, packId, channelList) => {
    console.log('xxx showNoticesAlert :: packName', packName, channelList );
    let _this = this;
    let message_top = _this.state.locals.selected_pack_includes_channels;
    let message_bottom = _this.state.locals.we_have_removed_hem_from_the_individual_channel;
    let channel_list_text='';
    _.forEach(channelList, function(channelObj){
      console.log('channelOb', channelObj);
      channel_list_text += channelObj.name + '\n';
       
    });

    let description = message_top + '\n\n'+ channel_list_text +'\n'+  message_bottom;
    let { additional_channel_list_data, selected_channel_n_packs } = this.props;
    let passProps = {
      primaryText: this.state.locals.ok,
      primaryPress:  () => { 
        _this.props.updateAdditionalPackSelection(additional_channel_list_data, packId, selected_channel_n_packs);
        Screen.dismissModal();
        _this.props.dtvForceUpdateView();
      },
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.AttentionAlert,
      description: description,
      descriptionTitle : packName + ' '+ this.state.locals.pack,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorYellow },
      
    };

    Screen.showGeneralModal(passProps);
  };

  setChannelType = (type) => {
    console.log('xxx setChannelType', type);  
    this.resetSearch();
    this.props.dtvSetChannelType(type);
    this.props.dtvForceUpdateView();
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_select_channel_type',
      item_id: type == Constants.DTV_INDIVIDUAL_CHANNEL ? 'INDIVIDUAL_CHANNEL' : 'CHANNEL_PACK' ,
      content_type: 'radio_button',
      additional_details: 'DTV select channel type',
    }); 
   
  }

  expandDetails = () => {
    console.log('xxx expandDetails');
    if (this.state.expandSwitch) {
      this.setState({ expandSwitch: false, arrowDown: 'md-arrow-dropdown' });
    } else {
      this.setState({ expandSwitch: true, arrowDown: 'md-arrow-dropup' });
    }
  }

  renderPackageRentalListItem = ({ item }) => {
    return (
      <View style= {styles.listViewContainer}>
        <View style ={styles.rental_with_dropdown_1_container}>
          <View style ={styles.additional_rental_container}>
            <View style = {styles.textContainer}>
              <Text style={styles.listViewLeftText}>{item.pack_name}</Text>
            </View>
            <View style = {styles.rsContainer}>
              <Text style={styles.dropDownTextStyle}>{this.state.locals.rs_label} </Text>
            </View>
            <View style ={styles.amountContainer}>
              <Text style={styles.dropDownTextStyle}>{Utill.numberWithCommas(Utill.checkNumber(item.pack_rental))}</Text>
            </View>
            <View style ={styles.taxContainer}>
              <Text style={styles.dropDownTextStyle}>{' + ' + this.state.locals.tax} </Text>
            </View>
          </View>
        </View>
        <View style ={styles.rental_with_dropdown_2_container}>
        </View>
      </View>
    );
  }

  renderSelectedPackageRentalListItem = ({ item }) => {
    console.log('renderSelectedPackageRentalListItem',item);
    return (
      <View style= {styles.listViewContainer}>
        <View style ={styles.rental_with_dropdown_1_container}>
          <View style ={styles.additional_rental_container}>
            <View style = {styles.textContainer}>
              <Text style={styles.listViewLeftText}>{item.name}</Text>
            </View>
            <View style = {styles.rsContainer}>
              <Text style={styles.dropDownTextStyle}>{this.state.locals.rs_label} </Text>
            </View>
            <View style ={styles.amountContainer}>
              <Text style={styles.dropDownTextStyle}>{Utill.numberWithCommas(Utill.checkNumber(item.value))}</Text>
            </View>
            <View style ={styles.taxContainer}>
              <Text style={styles.dropDownTextStyle}>{' + ' + this.state.locals.tax} </Text>
            </View>
          </View>
        </View>
        <View style ={styles.rental_with_dropdown_2_container}>
        </View>
      </View>
    );
  }
 
  renderChannelRentalListItem = ({ item }) => {
    return (
      <View style= {styles.listViewContainer}>
        <View style ={styles.rental_with_dropdown_1_container}>
          <View style ={styles.additional_rental_container}>
            <View style = {styles.textContainer}>
              <Text style={styles.listViewLeftText}>{item.name}</Text>
            </View>
            <View style = {styles.rsContainer}>
              <Text style={styles.dropDownTextStyle}>{this.state.locals.rs_label} </Text>
            </View>
            <View style ={styles.amountContainer}>
              <Text style={styles.dropDownTextStyle}>{Utill.numberWithCommas(Utill.checkNumber(item.rental))}</Text>
            </View>
            <View style ={styles.taxContainer}>
              <Text style={styles.dropDownTextStyle}>{' + ' + this.state.locals.tax} </Text>
            </View>
          </View>
        </View>
        <View style={styles.rental_with_dropdown_2_container}>
        </View>
      </View>
    );
  }

  render() {

    let _this = this; 
    let packageRental = 0;
    let channelRental = 0;
    let packsArray;
    let channelArray;
    let channelRentalTotalValue = 0;
    let packsTotalValue = 0;
    let additionalMonthlyRental = 0;
    let totalDailyRental = 0;
    let selectedPackageRental = parseFloat(Utill.checkNumber(this.props.availableDTVDataPackagesList.package_rental));

    packsArray = this.props.selected_channel_n_packs.packs ;
    channelArray = this.props.selected_channel_n_packs.channels;

    packsArray.forEach(function (item){   
      packageRental = Utill.checkNumber(item.pack_rental);
      packsTotalValue = (parseFloat(packsTotalValue) + parseFloat(packageRental));
    });

    channelArray.forEach(function (item) {   
      channelRental = Utill.checkNumber(item.rental);
      channelRentalTotalValue = (parseFloat(channelRentalTotalValue) + parseFloat(channelRental));
    });

    additionalMonthlyRental = channelRentalTotalValue + packsTotalValue;
    totalDailyRental = channelRentalTotalValue + packsTotalValue + selectedPackageRental;

    let totalPaymentProps = {
      additionalMonthlyRental : additionalMonthlyRental,
      totalDailyRental : totalDailyRental
    };

    console.log('&&&&&&& additionalMonthlyRental',additionalMonthlyRental);
    console.log('&&&&&&& totalDailyRental',totalDailyRental);
    console.log('&&&&&&& selectedPackageRental',selectedPackageRental);
    console.log('&&&&&&& packsTotalValue',packsTotalValue);

    const renderAdditionalChannelView = () => {
      const { no_channel_message = this.state.locals.no_channel_message } = this.props.additional_channel_list_data;
      return (
        <View style={styles.channelListViewContainer}>
          {/* DTV Channel filter view */}
          <FilterViewComponent 
            dropDownData = {_this.props.additional_channel_list_data.genre_name_list}
            defaultIndex = {-1}
            onDropdownSelect = {(idx, value) => this.onSelectGenre(idx, value)}
            dropDownLabel = {_this.state.locals.filterBy}
            selectedValue = {_this.props.selected_genre_name}/>   
          {/* DTV Channel list grid view */}
          <View style={styles.gridViewStyle}>
            <ScrollView>
              <GridSelectionViewComponent 
                gridItem = {
                  _this.state.searchResultSelection[0].channel_code !== '-99'?
                    _this.state.searchResultSelection : 
                    _this.props.selected_channel_list
                }
                renderListItem = {this.renderChannelItem}
                listEmptyComponent={() => {
                  if (this.props.apiLoading)
                    return true;
                  return (
                    <EmptyFeed
                      text={no_channel_message}
                      containerStyle={styles.errorDescContainer}
                      messageTxtStyle={styles.errorDescTextChannels}
                    />
                  );
                }
                }
              />
            </ScrollView>
          </View>
        </View>
      );
    };


    const renderChannelPackView = () => {
      const { no_pack_message = this.state.locals.no_pack_message } = this.props.additional_channel_list_data;
      return (
        /* DTV Channel pack list view */
        <View style={styles.channelViewContainer}>
          <ScrollView>
            <FlatList
              data={_this.props.channel_pack_list_data}
              renderItem={_this.renderPackListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={false}
              ListEmptyComponent={() => {
                if (this.props.apiLoading)
                  return true;
                return (
                  <EmptyFeed
                    text={no_pack_message}
                    containerStyle={styles.errorDescContainer}
                    messageTxtStyle={styles.errorDescTextPacks}
                  />
                );
              }
              }
            />
          </ScrollView>
        </View>
      );
    };
  

    return (
      <View style={styles.container} key={this.props.random_value}>
        <Header
          backButtonPressed={() => this.handleBackButtonClick()}
          searchButtonPressed={() => this.handleSearch()}
          displaySearch={_this.props.channel_selected_type == Constants.DTV_INDIVIDUAL_CHANNEL}  
          headerText={ this.state.locals.title }
        /> 
        <SearchableDropdown 
          placeholder={this.state.locals.searchPlaceHolder}
          textInputStyle={styles.textInputStyleClass}
          items={_this.props.selected_channel_list}
          ref={(ref) => this.searchBar = ref}
          hasLeftButton={true}
          noItemsFoundString={this.state.locals.noChannelFound}
          leftButtonProps={{ 
            icon: 'arrow-back', 
            action: ()=>{ this.onSearchBackButtonPress(); }
          }}
          additionalChannelListData={this.props.channel_pack_list_data}
          onChangeText={text=>this.onChangeSearchParam(text)}
          value={this.state.searchResultSelection[0].name}
          keyboardType={'default'}
          onItemSelect={(value) => this.onSelectChannel(value)}
          onPressClear={()=>{
            this.searchBar.hide();
          }
          }
        />
        <ActivityIndicator animating={this.props.apiLoading} text={this.props.api_call_indicator_msg}/>
        <View style={styles.innerContainer} >
          {/* DTV Channel and Pack selection view */}
          <View style={styles.containerRadioWrapper}>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.channel_selected_type}
                value={Constants.DTV_INDIVIDUAL_CHANNEL}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.channel_selected_type == Constants.DTV_INDIVIDUAL_CHANNEL ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setChannelType(Constants.DTV_INDIVIDUAL_CHANNEL)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.channels}</Text>
              </RadioButton>
            </View>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.channel_selected_type}
                value={Constants.DTV_CHANNEL_PACK}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.channel_selected_type == Constants.DTV_CHANNEL_PACK ? 
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setChannelType(Constants.DTV_CHANNEL_PACK)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.packs}</Text>
              </RadioButton>
            </View>
          </View>
          {/* DTV Channel view - individual or channel Pack*/}
          {_this.props.channel_selected_type == Constants.DTV_INDIVIDUAL_CHANNEL ? 
            renderAdditionalChannelView()
            :
            renderChannelPackView()
          }
        </View>
        {/* DTV Channel - rental info*/}
        <ScrollView style={styles.rental_container} >
          <View style={styles.section_rental_Container}>
            <View style ={styles.rental_with_dropdown_container}>
              <View style ={styles.rental_with_dropdown_1_container}>
                <View style ={styles.additional_rental_container}>
                  <View style = {styles.textContainer}>
                    <Text style={styles.textStyle}>{ this.props.selected_connection_type == Constants.PREPAID ? this.state.locals.additional_rental_text : this.state.locals.Additional_monthly_rental}</Text>
                  </View>
                  <View style = {styles.rsContainer}>
                    <Text style={styles.textStyle}>{this.state.locals.rs_label} </Text>
                  </View>
                  <View style ={styles.amountContainer}>
                    <Text style={styles.textStyle}>{Utill.numberWithCommas(Utill.checkNumber(additionalMonthlyRental))}</Text>
                  </View>
                  <View style ={styles.taxContainer}>
                    <Text style={styles.textStyle}>{' + ' + this.state.locals.tax} </Text>
                  </View>
                </View>
                <View style ={styles.total_rental_container}>
                  <View style = {styles.textContainer}>
                    <Text style={styles.totalPaymentValue}>{ this.props.selected_connection_type == Constants.PREPAID ? this.state.locals.total_daily_rental : this.state.locals.total_rental_text}</Text>
                  </View>
                  <View style = {styles.rsContainer}>
                    <Text style={styles.totalPaymentValue}>{this.state.locals.rs_label} </Text>
                  </View>
                  <View style ={styles.amountContainer}>
                    <Text style={styles.totalPaymentValue}>{Utill.numberWithCommas(Utill.checkNumber(totalDailyRental))}</Text>
                  </View>
                  <View style ={styles.taxContainer}>
                    <Text style={styles.totalPaymentValue}>{' + ' + this.state.locals.tax} </Text>
                  </View>
                </View>
              </View>
              {/* Expandable arrow   */}
              <TouchableOpacity style ={styles.rental_with_dropdown_2_container} onPress={()=>this.expandDetails()}>
                <Ionicons name={this.state.arrowDown} size={20}/>
              </TouchableOpacity>
            </View>
            {this.state.expandSwitch ? 
            //Expandable list view
              <View style = {styles.expandableViewContainer}>
                {/* Selected Additional Package ListView */}
                <FlatList
                  data={this.props.selectedPackageDetails}
                  renderItem={this.renderSelectedPackageRentalListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={false}
                />
                {/* Selected Additional channel ListView */}
                <FlatList
                  data={this.props.selected_channel_n_packs.packs}
                  renderItem={this.renderPackageRentalListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={false}
                />
                {/* Selected Additional Package ListView */}
                <FlatList
                  data={this.props.selected_channel_n_packs.channels}
                  renderItem={this.renderChannelRentalListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={false}
                />
              </View>
              : 
              <View/>
            }
          </View>
          {/*Button Starts*/}
          <View style={styles.button_Container}>
            <View style={styles.buttonSetStyle}>
              <TouchableOpacity
                onPress={() => this.onPressContinueButton(totalPaymentProps)}
                style={styles.confirmButtonStyle}>
                <Text style={styles.ConfirmAndCancelTextStyle}>
                  {this.state.locals.Continue}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/*Button Ends*/}
        </ScrollView>
      </View>
    );
  }

  resetSearch = () =>{
    this.setState({ 
      searchResultSelection: [
        { 
          name:'',
          url:'',
          channel_code: '-99'
        }
      ] 
    },
    ()=>{ 
      this.searchBar.hide();
    });
  }

  onSelectChannel = (value)=> {
    console.log('onSelectChannel ::', value);
    console.log('value: ',value);
    if (value.channel_code !== '-99') {
      this.setState({ searchResultSelection: [value] });
      Analytics.logFirebaseEvent('select_content', {
        context: 'dtv_channel_search_result_select',
        item_id:  value.code,
        content_type: 'dropdown',
        additional_details: 'DTV search channel : channel_code - ' + value.code,
      }); 
    }
   
  }

  getDefaultSelectedGenre = () => {
    let defaultSelected = (FuncUtils.getArraySize(this.props.additional_channel_list_data.genre_name_list) > 0) ? 
      FuncUtils.getValueSafe(this.props.additional_channel_list_data).genre_name_list[0] : "All channels";

    let defaultSelectedGenre_code = (FuncUtils.getArraySize(this.props.additional_channel_list_data.genre_code_list) > 0) ? 
      FuncUtils.getValueSafe(this.props.additional_channel_list_data).genre_code_list[0] : "ALL";

    console.log('getDefaultSelectedGenre :: defaultSelected ', defaultSelected);
    console.log('getDefaultSelectedGenre :: defaultSelectedGenre_code ', defaultSelectedGenre_code);

    return  {
      genre_code: defaultSelectedGenre_code,
      genre_name: defaultSelected
    };
  }

  onSelectGenre = (idx, value)=> {
    console.log('onSelectGenre :: idx, value - ', idx, value);
    this.resetSearch();
    let selected_genre = {
      genre_code: value,
      genre_name: value
    };

    this.props.dtvSetChannelGenre(selected_genre);
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_select_genre',
      item_id:  selected_genre.genre_code,
      content_type: 'check_box',
      additional_details: 'DTV select genre : genre_code - ' + selected_genre.genre_code,
    }); 
   
  }

  onSelectChannelCheckbox = (channel_code)=> {
    console.log('onSelectChannelCheckbox :: channel_code', channel_code);
    let { additional_channel_list_data, selected_channel_n_packs } = this.props;
    this.props.selectAdditionalChannel(additional_channel_list_data, channel_code, selected_channel_n_packs);
    // this.props.dtvForceUpdateView();
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_select_additional_channel',
      item_id:  channel_code,
      content_type: 'check_box',
      additional_details: 'DTV select additional channel : code - ' + channel_code,
    }); 
  }

  onSelectPackCheckbox = (pack_code)=> {
    console.log('onSelectPackCheckbox :: pack_code', pack_code);
    let _this = this;
    let { additional_channel_list_data, selected_channel_n_packs } = this.props;
    this.props.selectAdditionalPack(additional_channel_list_data, pack_code, selected_channel_n_packs, _this);
    this.props.dtvForceUpdateView();
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_select_additional_pack',
      item_id:  pack_code,
      content_type: 'check_box',
      additional_details: 'DTV select additional pack : pack_code - ' + pack_code,
    }); 
    
  }

  renderSeparator = ()=> {
    return (
      <View style={styles.displayNoneStyle}/>
    );
  }

  expandChannelPack = (index)=> {
    console.log('expandChannelPack :: index', index);
    let expandPackItemArray = new Array(FuncUtils.getArraySize(this.props.channel_pack_list_data));
    expandPackItemArray[index] = !this.state.expandPackItem[index];
    console.log('expandChannelPack :: expandPackItemArray', expandPackItemArray);
    this.updateState({ expandPackItem : expandPackItemArray });
 
  }

  renderListItem = ( item )=> {
    return (
      <View style={styles.dropDownRow}>
        <View style={styles.searchBarIcon}></View>
        <View style={styles.channelNameTextView}>
          <Text style={styles.textViewStyleClass}>{item.name}</Text>
        </View>
        <View style={styles.channelIconView}>
          <Image 
            source={{ uri: item.url }}
            resizeMode={'stretch'}
            style={styles.searchItemIcon}
          />
        </View>
      </View> 
    );
  }

  onSearchBackButtonPress = () =>{
    this.resetSearch();
    console.log('onSearchBackButtonPress');
    this.searchBar.hide();
  }

  /**
   * @description Show search bar
   * @memberof ActivationStatusMainView
   */
  handleSearch = () =>{
    console.log('handleSearch');
    this.searchBar.show();
  }

  renderChannelItem = ({ item }) => {
    //console.log('renderChannelItem', item);
    return (
      <View style={styles.gridContainer}>
        <View style={styles.gridChannelImageContainer}>
          <Image source={{ uri: item.url }} style={styles.imageItem} />
        </View>
        <View style={styles.gridChannelNameTextView}>
          <Text style={styles.gridChannelNameTextStyle}>
            {`${this.state.locals.rs_label} ${item.rental} + ${this.state.locals.tax}`}
          </Text>
        </View>
        <View style={styles.gridChannelCheckboxView}>
          <CheckBox
            checkBoxColor={Colors.yellow}
            uncheckedCheckBoxColor={Colors.colorGrey}
            style={styles.gridChannelCheckboxStyle}
            onClick={() => this.onSelectChannelCheckbox(item.code)} isChecked={item.is_selected} />
        </View>
      </View>
    );
    
  };

  renderPackListItem = ({ item, index }) => {
    let _this = this;
    //console.log('renderPackListItem :: item, index : ', item, index);
    return (     
      <View style={styles.packListContainer}>
        <View style={styles.packListItemContainer}>
          <View style={styles.elementCheckBox}>
            <CheckBox
              checkBoxColor={Colors.yellow}
              uncheckedCheckBoxColor={Colors.colorGrey}
              style={styles.gridPackCheckboxStyle}
              onClick={() => this.onSelectPackCheckbox(item.pack_code)} isChecked={item.is_selected} />
          </View>
          <TouchableOpacity style={styles.elementRightItem} disabled = { (item.channels.length > 0 && item.channels) ? false : true } onPress={()=> _this.expandChannelPack(index)}>
            <View style={styles.elementRightItemTextView}>
              <Text style={styles.elementRightItemTextStyle}>{item.pack_name}</Text>
            </View>
            <View style={styles.elementMiddleTextView}>
              <Text style={styles.elementMiddleItemTextStyle}>
                {`${this.state.locals.rs_label} ${item.pack_rental} + ${this.state.locals.tax}`}
              </Text>
            </View>
            { (item.channels.length > 0 && item.channels) ? 
              <View style={styles.elementRightItemExpandSwitchView}>
                <Ionicons name={_this.state.expandPackItem[index] ? 'md-arrow-dropup' : 'md-arrow-dropdown'} size={20} />
              </View> 
              :
              <View style={styles.elementRightItemExpandSwitchView}>
              </View>
            }
          </TouchableOpacity> 
        </View>
        {/*Expanding View*/}
        {_this.state.expandPackItem[index] ?
          <GridSelectionViewComponent 
            style={styles.channelPackGridView} 
            gridItem ={item.channels} 
            renderListItem = {_this.renderChannelPackItem} /> 
          : 
          <View/>
        }
      </View> );
  };

  renderChannelPackItem = ({ item }) => {
    console.log('renderChannelPackItem', item);
    return (
      <View style={styles.gridPackContainer}>
        <View style={styles.gridPackImageContainer}>
          <Image source={{ uri: item.image_url }} style={styles.imagePackItem} />
        </View>
      </View>
    );
  };

}

const mapStateToProps = (state) => {
  console.log("#####====== DTV ACTIVATION DtvAdditionalChannels =====######\n", state.dtvActivation);
  const Language = state.lang.current_lang;
  const apiLoading = state.dtvActivation.apiLoading;
  const random_value = state.dtvActivation.random_value; 
  const api_call_indicator_msg = state.dtvActivation.api_call_indicator_msg;
  const channel_selected_type = state.dtvActivation.channel_selected_type;

  const selected_connection_type = state.dtvActivation.selected_connection_type; 

  const additional_channel_list_data = state.dtvActivation.additional_channel_list_data;
  let defaultSelected = (additional_channel_list_data.genre_name_list && additional_channel_list_data.genre_name_list.length > 0) ? additional_channel_list_data.genre_name_list[0] : "All channels";
  const additional_channel_selected_genre = state.dtvActivation.additional_channel_selected_genre;
  const selected_genre_name = additional_channel_selected_genre.genre_name == ''? 
    defaultSelected : additional_channel_selected_genre.genre_name  ;

  let channel_pack_list_data = additional_channel_list_data.pack_list;

  const selected_channel_n_packs = state.dtvActivation.selected_channel_n_packs;
  const selected_channel_total_package_rental = state.dtvActivation.selected_channel_total_package_rental;
  const availableDTVDataPackagesList = state.dtvActivation.availableDTVDataPackagesList;

  let selectedPackageDetails = [
    {
      name : availableDTVDataPackagesList.selectedValueName,
      value : availableDTVDataPackagesList.package_rental
    }
  ];
  let selected_channel_list_filter;
  let selected_channel_list_before_filter = [];
  let selected_channel_list = [];

  try {
    selected_channel_list_filter =_.filter(additional_channel_list_data.channel_list, { 'genre_name': selected_genre_name });
    selected_channel_list_before_filter = selected_channel_list_filter[0].channels;
    selected_channel_list = _.filter(selected_channel_list_before_filter, { 'already_in_pack': false });  
  } catch (error) {
    console.log('DTV mapStateToProps ERROR : ', error);
  }  

  console.log('DTV mapStateToProps :: selected_channel_list_before_filter', selected_channel_list_before_filter);
  console.log('DTV mapStateToProps :: selected_channel_list\n', selected_channel_list);
  console.log('DTV mapStateToProps :: selected_channel_n_packs\n', selected_channel_n_packs);
  console.log('DTV mapStateToProps :: additional_channel_list_data\n', additional_channel_list_data);
  console.log('DTV mapStateToProps :: selected_channel_total_package_rental', selected_channel_total_package_rental);

  //console.log("#####====== DTV ACTIVATION LOG_JSON :: additional_channel_list_data =====######\n", JSON.stringify(state.dtvActivation.additional_channel_list_data));
  console.log("#####====== DTV ACTIVATION LOG_OBJ :: additional_channel_list_data =====######\n", state.dtvActivation.additional_channel_list_data);
  console.log("#####====== DTV ACTIVATION LOG_OBJ :: selected_channel_n_packs =====######\n", state.dtvActivation.selected_channel_n_packs);
  console.log('$$ SELECT_ADDITIONAL_PACK - selected_channel_n_packs\n', selected_channel_n_packs);
  return { 
    Language, 
    apiLoading,
    random_value, 
    api_call_indicator_msg,
    selected_connection_type,
    channel_selected_type,
    additional_channel_list_data,
    selected_channel_list,
    additional_channel_selected_genre,
    selected_genre_name,
    channel_pack_list_data,
    selected_channel_n_packs,
    selected_channel_total_package_rental,
    availableDTVDataPackagesList,
    selectedPackageDetails,
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.appBackgroundColor
  },

  innerContainer: {
    flex: 1,
    marginLeft: 8,
    marginRight: 8,
    marginTop: 10
  },

  displayNoneStyle: { 
    display:'none' 
  },

  channelListViewContainer: {
    flex: 1,
  },

  containerRadioWrapper: {
    flexDirection: 'row',
    padding: 8,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    height:55
  },
  containerRadioBtn: {
    flex: 1,
    padding: 6,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  radioBtnTxt: {
    marginLeft: 5,
    fontSize: 16,
    fontWeight: '400',
    color: Colors.colorGrey
  },

  //Additional channel related
  gridViewStyle : {
    flex: 1,
    marginTop: 8
  },

  gridContainer : {
    width: Dimensions.get('window').width / 3 - 12,
    height: 112,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderRadius: 1,
    marginRight: 10,
    marginBottom: 8,
  },

  gridChannelImageContainer : {
    height: 42,
  },

  imageItem : {
    height: 40,
    justifyContent: 'center',
    resizeMode:'cover',
    margin: 8,  
  },

  gridChannelNameTextView: {
    marginTop: 10,
    padding: 4,
    alignItems: 'center'
  },

  gridChannelNameTextStyle : {  
    fontSize: 14
  },

  gridChannelCheckboxView: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  gridChannelCheckboxStyle: {
    padding: 2,
    marginBottom: 2
  },

  //Channel pack related
  channelViewContainer: {
    flex: 1,
  },
  channelPackGridView: { 
    marginLeft: 44, 
    paddingTop: 8
  },

  gridPackContainer : {
    width: (Dimensions.get('window').width - 44) / 3  - 12,
    height: 58,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderRadius: 1,
    marginRight: 10,
    marginBottom: 8,
  },

  gridPackImageContainer : {
    height: 42,
  },
  imagePackItem : {
    height: 40,
    justifyContent: 'center',
    resizeMode:'cover',
    margin: 8,  
  },
  packListContainer : {
    flex: 1,
    flexDirection: 'column',
    marginTop: 10,
  },

  packListItemContainer : {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },

  elementCheckBox: {
    width: 44,
  },
  
  gridPackCheckboxStyle: {
    padding: 2,
    margin: 2,
    alignItems: 'center',
    justifyContent :  'center',
  },

  elementRightItem: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    padding: 4,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderRadius: 1,
  },
  
  elementRightItemTextView : {
    flex: 1.5,
    padding: 2,
  },

  elementMiddleTextView: {
    flex: 0.8,
    padding: 2,
  },

  elementRightItemExpandSwitchView : {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 2,
  },
  
  elementRightItemTextStyle: {
    fontSize: 13,
    color: Colors.colorBlack
  },

  elementMiddleItemTextStyle: {
    fontSize: 13,
    color: Colors.colorGrey,
  },
  // End 
  dropDownRow:{
    flex:1,
    flexDirection:'row',
    width: Dimensions
      .get('window')
      .width,
    height:55
  },
  searchBarIcon:{
    width:35,
    margin: 5
  },
  textViewStyleClass:{    
    flex:1,
    textAlign: 'left',
    color:Colors.black,
    justifyContent: 'center', 
    alignItems: 'center',
    lineHeight: 55,
    fontSize: 14,
  },
  channelNameTextView:{
    flex:9,
    flexDirection:'column',
    padding:0,
    margin:0,
  },
  channelIconView:{
    width:80,
    margin: 10,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  searchItemIcon:{
    flex:1,
    height:'100%',
    width:80
  },
  section_rental_Container: {
    height: 'auto',
    flexDirection: 'column',
    borderTopColor: Colors.grey,
    borderTopWidth:1,
  },
  rental_with_dropdown_container: {
    height: 70,
    //backgroundColor: Colors.colorBlack,
    flexDirection: 'row',
    marginTop: 10
  },
  rental_with_dropdown_1_container:{
    flex: 0.9,
    flexDirection: 'column',
  },
  rental_with_dropdown_2_container: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  expandableViewContainer: {
    marginTop: 15,
    height: 'auto'
  },
  button_Container: {
    height: 40,
    marginTop: 5,
    marginBottom: 10,
  },
  confirmButtonStyle: {
    backgroundColor: Colors.buttonColor,
    alignSelf: 'stretch',
    borderRadius: 2,
    marginRight: 20,
    padding: 10
  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  ConfirmAndCancelTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  rental_container: {
    flexGrow: 0,
    flexDirection: 'column',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    // marginLeft: 20,
    // marginRight: 7
  },
  additional_rental_container: {
    flex: 0.5,
    flexDirection: 'row',
  },
  total_rental_container: {
    flex: 0.5,
    flexDirection: 'row',
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    textAlign: 'right',
  },
  textContainer: {
    flex: 0.6,
    alignItems: 'flex-start',
    marginLeft: 10,
  },
  rsContainer:{
    flex: 0.08,
    alignItems: 'flex-end',
  },
  amountContainer:{
    flex: 0.27,
  },
  taxContainer:{
    flex: 0.15,
    alignItems: 'flex-end',
  },
  totalPaymentValue : {
    textAlign: 'right',
    fontWeight: 'bold',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  dropDownTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    textAlign: 'right',
  },
  listViewLeftText: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    textAlign: 'left',
  },
  listViewContainer: {
    flex: 1,
    flexDirection: 'row',
    height: 30,
    marginBottom: 7,
  },

  errorDescContainer: {
    alignItems: 'center',
    //backgroundColor: Colors.colorGreen
  },

  errorDescTextChannels:{ 
    textAlign: 'center', 
    fontSize: 18, 
    fontWeight:'bold', 
    alignSelf: 'center', 
    //marginTop: -40,
    marginTop: (Dimensions.get('window').height) / 6,
    color: Colors.colorGrey 
  },
  errorDescTextPacks:{ 
    textAlign: 'center', 
    fontSize: 18, 
    fontWeight:'bold', 
    alignSelf: 'center',
    //marginTop: -40, 
    marginTop: (Dimensions.get('window').height) / 5,
    color: Colors.colorGrey 
  }
});

export default connect(mapStateToProps, actions)(DtvAdditionalChannels);
