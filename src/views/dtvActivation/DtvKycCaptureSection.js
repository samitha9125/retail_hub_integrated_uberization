/*
 * File: DtvKycCaptureSection.js
 * Project: Dialog Sales App
 * File Created: Sunday, 2nd December 2018 9:31:47 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Sunday, 2nd December 2018 10:05:32 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import CheckBox from 'react-native-check-box';
import { Constants, Colors } from '@Config';
import { FormatUtils } from '../../utills';
import CameraInput from './components/CameraInput';
import SectionContainer from './components/SectionContainer';

//Image capture in Customer Information section Local customer or Foreign customer OTP verified
export const CustomerOtpVerified = ({ currentView }) => (
  <View>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.addressDifferentFromOtpVerifiedConnectionPre : currentView.state.locals.addressDifferentFromOtpVerifiedConnectionPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('NIC OR PP - POB capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer
      hideView={!currentView.props.enable_sameDayInstallation}>
      <CheckBoxInput
        checkBoxText={`${currentView.state.locals.sameDayInstallation} ${FormatUtils.getFormattedCurrency(currentView.props.sameDayInstallation_price)} \n${currentView.state.locals.activate_before} ${currentView.props.sameDayInstallation_cutoff_time}`
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.SAME_DAY_INSTALLATION_TYPE, !currentView.props.opt_sameDayInstallation);
        }}
        isChecked={currentView.props.opt_sameDayInstallation}
      />
    </SectionContainer>
    <SectionContainer
      hideView={!(!currentView.props.opt_sameDayInstallation && currentView.props.isCashAndDeliverySelected)}>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('NIC OR PP - Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//NIC image capture in Customer Information section
export const CustomerInfoNIC = ({ currentView }) => (
  <View>
    <SectionContainer>
      <CameraInput
        captureFront={() => {
          console.log('Capture NIC Front');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT);
        }}
        captureBack={() => {
          console.log('Capture NIC Back');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.NIC_BACK);
        }}
        titleLabel={currentView.state.locals.captureNIC}
        icon={(currentView.props.common.kyc_nic_front === null
          && currentView.props.common.kyc_nic_back === null) ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
        frontAndBack={currentView.props.common.kyc_nic_front != null && currentView.props.common.kyc_nic_back != null} />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.addressDifferentFromNIC : currentView.state.locals.addressDifferentFromNICPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('NIC POB capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.state.locals.customerFaceNotClearNIC}
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE, !currentView.props.opt_customerFaceNotClear);
        }}
        isChecked={currentView.props.opt_customerFaceNotClear}
        cameraCaptureLabel={currentView.state.locals.captureCustomerPhoto}
        onCapture={() => {
          console.log('NIC Customer photo capture tapped');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE);
        }}
        captureIcon={currentView.props.common.kyc_customer_image === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer
      hideView={!currentView.props.enable_sameDayInstallation}>
      <CheckBoxInput
        checkBoxText={`${currentView.state.locals.sameDayInstallation} ${FormatUtils.getFormattedCurrency(currentView.props.sameDayInstallation_price)} \n${currentView.state.locals.activate_before} ${currentView.props.sameDayInstallation_cutoff_time}`
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.SAME_DAY_INSTALLATION_TYPE, !currentView.props.opt_sameDayInstallation);
        }}
        isChecked={currentView.props.opt_sameDayInstallation}
      />
    </SectionContainer>
    <SectionContainer
      hideView={!(!currentView.props.opt_sameDayInstallation && currentView.props.isCashAndDeliverySelected)}>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('NIC Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//Driving License image capture in Customer Information section
export const CustomerInfoDrivingLicense = ({ currentView }) => (
  <View>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Driving Licence');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE);
        }}
        titleLabel={currentView.state.locals.captureDrivingLicense}
        icon={currentView.props.common.kyc_driving_licence === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.addressDifferentFromDL : currentView.state.locals.addressDifferentFromDLPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING, !currentView.props.opt_addressDifferent);
        }}
        isChecked={currentView.props.opt_addressDifferent}
        cameraCaptureLabel={currentView.state.locals.capturePOBPost}
        onCapture={() => {
          console.log('DL Capture POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        captureIcon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.state.locals.customerFaceNotClearDL}
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE, !currentView.props.opt_customerFaceNotClear);
        }}
        isChecked={currentView.props.opt_customerFaceNotClear}
        cameraCaptureLabel={currentView.state.locals.captureCustomerPhoto}
        onCapture={() => {
          console.log('DL Capture Customer Photo');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE);
        }}
        captureIcon={currentView.props.common.kyc_customer_image === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer
      hideView={!currentView.props.enable_sameDayInstallation}>
      <CheckBoxInput
        checkBoxText={`${currentView.state.locals.sameDayInstallation} ${FormatUtils.getFormattedCurrency(currentView.props.sameDayInstallation_price)} \n${currentView.state.locals.activate_before} ${currentView.props.sameDayInstallation_cutoff_time}`
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.SAME_DAY_INSTALLATION_TYPE, !currentView.props.opt_sameDayInstallation);
        }}
        isChecked={currentView.props.opt_sameDayInstallation}
      />
    </SectionContainer>
    <SectionContainer
      hideView={!(!currentView.props.opt_sameDayInstallation && currentView.props.isCashAndDeliverySelected)}>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.props.is_prepaid ?
          currentView.state.locals.installationAddressDifferentFromBillingProof : currentView.state.locals.installationAddressDifferentFromBillingProofPost
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOB}
        onCapture={() => {
          console.log('DL Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);

//Passport image capture in Customer Information section
export const CustomerInfoPassport = ({ currentView }) => (
  <View>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Passport Front');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PASSPORT);
        }}
        titleLabel={currentView.state.locals.capturePassport}
        icon={currentView.props.common.kyc_passport === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer >
      <CameraInput
        captureFront={() => {
          console.log('Capture Address - Passport');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING);
        }}
        titleLabel={currentView.props.is_prepaid ?
          currentView.state.locals.captureAddressPre : currentView.state.locals.captureAddress
        }
        icon={currentView.props.common.kyc_proof_of_billing === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
    <SectionContainer
      hideView={!currentView.props.enable_sameDayInstallation}>
      <CheckBoxInput
        checkBoxText={`${currentView.state.locals.sameDayInstallation} ${FormatUtils.getFormattedCurrency(currentView.props.sameDayInstallation_price)} \n${currentView.state.locals.activate_before} ${currentView.props.sameDayInstallation_cutoff_time}`
        }
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.SAME_DAY_INSTALLATION_TYPE, !currentView.props.opt_sameDayInstallation);
        }}
        isChecked={currentView.props.opt_sameDayInstallation}
      />
    </SectionContainer>
    <SectionContainer
      hideView={!(!currentView.props.opt_sameDayInstallation && currentView.props.isCashAndDeliverySelected)}>
      <CheckBoxWithCameraInput
        checkBoxText={currentView.state.locals.installationAddressDifferentFromBillingProofPost}
        onClick={() => {
          currentView.props.dtvSetKycCheckBoxField(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB, !currentView.props.opt_installationAddressDeferent);
        }}
        isChecked={currentView.props.opt_installationAddressDeferent}
        cameraCaptureLabel={currentView.state.locals.captureAdditionalPOBPassport}
        onCapture={() => {
          console.log('PP Capture Additional POB');
          currentView.onPressImageCapture(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB);
        }}
        captureIcon={currentView.props.common.kyc_additional_pob === null ?
          Constants.icon.camera : Constants.icon.thumbnail
        }
      />
    </SectionContainer>
  </View>
);


const CheckBoxInput = ({ checkBoxText, onClick, isChecked }) => {
  return (
    <View style={styles.cameraInputCheckBoxContainerMain}>
      <View style={styles.cameraInputBoxContainer}>
        <View style={styles.cameraInputCheckBoxView}>
          <CheckBox
            checkBoxColor={isChecked ? Colors.yellow : Colors.colorGrey}
            uncheckedCheckBoxColor={Colors.colorGrey}
            style={styles.cameraInputCheckBoxStyle}
            onClick={onClick} 
            isChecked={isChecked} />
        </View>
        <View style={styles.cameraInputCheckBoxTextView}>
          <Text style={styles.cameraInputCheckBoxText}>
            {checkBoxText}
          </Text>
        </View>
      </View>
    </View>
  );
};

const CheckBoxWithCameraInput = ({ checkBoxText, onClick, isChecked, cameraCaptureLabel, onCapture, captureIcon = Constants.icon.camera }) => (
  <View style={styles.cameraInputCheckBoxContainerMain}>
    <View style={styles.cameraInputBoxContainer}>
      <View style={styles.cameraInputCheckBoxView}>
        <CheckBox
          checkBoxColor={isChecked ? Colors.yellow : Colors.colorGrey}
          uncheckedCheckBoxColor={Colors.colorGrey}
          style={styles.cameraInputCheckBoxStyle}
          onClick={onClick} 
          isChecked={isChecked} />
      </View>
      <View style={styles.cameraInputCheckBoxTextView}>
        <Text style={styles.cameraInputCheckBoxText}>
          {checkBoxText}
        </Text>
      </View>
    </View>
    {isChecked ?
      <View style={styles.cameraInputBoxContainer}>
        <View style={styles.cameraInputCheckBoxView} />
        <View style={styles.cameraInputCheckBoxTextView}>
          <CameraInput
            captureFront={onCapture}
            titleLabel={cameraCaptureLabel}
            icon={captureIcon} />
        </View>
      </View>
      : true}
  </View>
);

const styles = StyleSheet.create({
  
  cameraInputBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 8,
  },
  cameraInputCheckBoxContainerMain: {
    flex: 1,
    marginBottom: 8,
    flexDirection: 'column'
  },
  cameraInputCheckBoxView: {
    flex: 0.1,
    alignItems: 'center',
  },
  cameraInputCheckBoxTextView: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },
  cameraInputCheckBoxStyle: {
    padding: 0,
    paddingRight: 5,
  },
  cameraInputCheckBoxText: {
    paddingLeft: 5
  },
  
});
