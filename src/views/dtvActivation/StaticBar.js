/*
 * File: customerPayments.js
 * Project: Dialog Sales App
 * File Created: Friday, 16th November 2018 4:40:57 Am
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * -----
 * Last Modified: 
 * Modified By: 
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/DtvActivation';
import Utills from '../../utills/Utills';
import PaymentCalculations from './PaymentCalculation';

const Utill = new Utills();
class staticBar extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      expandSwitch: false,
      locals: {
        total_monthly_rental: 'Total monthly rental',
        total_daily_rental: 'Total daily rental',
        customerPayment: strings.customerPayment, 
        rs: strings.rs,
        tax: 'Tax'
      }
    };
  }

  componentDidMount() {
    console.log('xxx TotalPayments :: componentDidMount');
    console.log(JSON.stringify(this.props));
  }

  checkNumber = (number) => {
    console.log('xxx TotalPayments :: checkNumber');
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00" && value !== 0.00);
  }

  render() {
    return (
      <View style={styles.container}>

        {this.props.selected_channel_total_package_rental.totalDailyRental ? 
          <View style ={styles.totalRentalContainer}>
            <View style = {styles.textContainer}>
              <Text style={styles.textStyle}>{ this.props.selected_connection_type == Constants.PREPAID ? this.state.locals.total_daily_rental : this.state.locals.total_monthly_rental}</Text>
            </View>
            <View style = {styles.rsContainer}>
              <Text style={styles.textStyle}>{this.state.locals.rs} </Text>
            </View>
            <View style ={styles.amountContainer}>
              <Text style={styles.textStyle}>{Utill.numberWithCommas(Utill.checkNumber(this.props.selected_channel_total_package_rental.totalDailyRental))}</Text>
            </View>
            <View style ={styles.taxContainer}>
              <Text style={styles.textStyle}>{' + ' + this.state.locals.tax} </Text>
            </View>
          </View>
          :
          <View/>
        }
      
        {this.props.data_totalPaymentProps.customerTotalPayment ? 
          <View style ={styles.customerPaymentContainer}>
            <View style = {styles.textContainer}>
              <Text style={styles.textStyle}>{this.state.locals.customerPayment}</Text>
            </View>
            <View style = {styles.rsContainer}>
              <Text style={styles.textStyle}>{this.state.locals.rs} </Text>
            </View>
            <View style ={styles.amountContainer}>
              <Text style={styles.textStyle}>{Utill.numberWithCommas(Utill.checkNumber(this.props.data_totalPaymentProps.customerTotalPayment))}</Text>
            </View>
            <View style ={styles.taxContainer}>
            </View>
          </View>
          :
          <View/>
        }
       
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TOTAL PAYMENT => DTV ACTIVATION', state.dtvActivation);
  const language = state.lang.current_lang;
  const selected_connection_type = state.dtvActivation.selected_connection_type;
  const selected_channel_total_package_rental = state.dtvActivation.selected_channel_total_package_rental;
  let allPaymentData = PaymentCalculations.getPaymentDataFromRedux(state.dtvActivation);
  const data_totalPaymentProps = allPaymentData.totalPaymentProps;

  console.log(':: TOTAL PAYMENT => DTV ACTIVATION selected_channel_total_package_rental', selected_channel_total_package_rental);
  console.log(':: TOTAL PAYMENT => DTV ACTIVATION data_totalPaymentProps', data_totalPaymentProps);
  return {
    language,
    selected_connection_type,
    selected_channel_total_package_rental,
    allPaymentData,
    data_totalPaymentProps
  };
};

const styles = StyleSheet.create({
  container: {
    height: 'auto',
    flexDirection: 'column',
    backgroundColor: Colors.buttonColor
  },
  textContainer: {
    flex: 0.6,
    alignItems: 'flex-start',
    marginLeft: 10,
  },
  rsContainer:{
    flex: 0.08,
    alignItems: 'flex-end',
  },
  amountContainer:{
    flex: 0.27,
  },
  taxContainer:{
    flex: 0.15,
    alignItems: 'flex-end',
    marginRight: 5
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    textAlign: 'right',
  },
  totalRentalContainer: {
    marginTop: 10,
    height: 25,
    flexDirection: 'row', 
    marginBottom:5
  },
  customerPaymentContainer: {
    height: 25,
    flexDirection: 'row',
    marginBottom:10,
    marginTop: 5
  }
});

export default connect(mapStateToProps, actions)(staticBar);