/*
 * File: dealerCollectionBreakdown.js
 * Project: Dialog Sales App
 * File Created: Friday, 23th November 2018 10:40:57 Am
 * Author: Sampath (sampath@omobio.net)
 * -----
 * Last Modified: 
 * Modified By: 
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Constants from '../../config/constants';
import strings from '../../Language/DtvActivation';
import Utills from '../../utills/Utills';

const Utill = new Utills();
class DealerCollectionBreakdown extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      expandSwitch: false,
      arrowDown: 'md-arrow-dropdown',
      locals: {
        dealerCollection: strings.dealerCollection,
        showData: strings.showData,
        outstanding: strings.outstanding,
        deliveryInstallation: strings.deliveryInstallation,
        unitPriceAdjustment: strings.unitPriceAdjustment,
        voucher: strings.voucher,
        warrantyExt: strings.warrantyExt,
        sameDayInstallation: strings.sameDayInstallation,
        reload: strings.reload,
        rs: strings.rs,
        purchased_price: strings.purchased_price,
        selling_price: strings.selling_price,
        deposit: strings.depositAmount
      }
    };
  }

  componentDidMount() {
    console.log('xxx TotalPayments :: componentDidMount');
    console.log('xxx TotalPayments :: componentDidMount',JSON.stringify(this.props.dealerCollectionPaymentProps));
  }

  checkNumber = (number) => {
    console.log('xxx TotalPayments :: checkNumber');
    if (number !== undefined && number !== null && number !== "" && !isNaN(number)) {
      return parseFloat(number).toFixed(2);
    } else {
      return 0.00;
    }
  }

  needToShowValue = (value) => {
    console.log('xxx needToShowValue : ', value);
    return (value !== undefined && value !== null && value !== "" && !isNaN(value) && value !== "0.00" && value !== 0.00);
  }

  expandDetails = () => {
    console.log('xxx expandDetails');
    if (this.state.expandSwitch) {
      this.setState({ expandSwitch: false, arrowDown: 'md-arrow-dropdown' });
    } else {
      this.setState({ expandSwitch: true, arrowDown: 'md-arrow-dropup' });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerRadioWrapper}>
          <View style={styles.mainRow}>
            <View style={styles.elementLeftView}>
              <Text style={styles.totalPayment}>{this.state.locals.dealerCollection}</Text>
            </View>
            <View style={styles.elementRightView1}>
              <View style={styles.elementRightInnerView3}></View>
              <View style={styles.elementRightInnerView1}>
                <Text style={styles.totalPaymentValue}>{this.state.locals.rs} </Text>
              </View>
              <View style={styles.elementRightInnerView2}>
                <Text style={styles.totalPaymentValue}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.dealerCollectionPayment))}</Text>
              </View>
            </View>
          </View>
          <View style={styles.mainRow}>
            <Text
              style={styles.showDetails}
              onPress={() =>this.expandDetails()}>{this.state.locals.showData}</Text>
            <Ionicons name={this.state.arrowDown} size={20} color={Colors.linkColor} />
          </View>
          {this.state.expandSwitch
            ? <View>

              {this.needToShowValue(this.props.dealerCollectionPaymentProps.outstandingFeeValue) ?
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={styles.cl2}>{this.state.locals.outstanding}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}></View>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.outstandingFeeValue))}</Text>
                    </View>
                  </View>
                </View>
                :
                <View />
              }


              {/*this.needToShowValue(this.props.dealerCollectionPaymentProps.unitPriceAdjustment)*/}
              <View>
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={styles.cl2}>{this.state.locals.unitPriceAdjustment}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}/>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.unitPriceAdjustment))}</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={[styles.subCatText, styles.lightTextClr]}>{this.state.locals.purchased_price}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}/>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={[styles.textStyleRight, styles.lightTextClr]}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={[styles.textStyleRight, styles.lightTextClr]}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.purchasedPrice))}</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={[styles.subCatText, styles.lightTextClr]}>{this.state.locals.selling_price}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}/>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={[styles.textStyleRight, styles.lightTextClr]}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={[styles.textStyleRight, styles.lightTextClr]}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.unitPriceAfterVoucerValue))}</Text>
                    </View>
                  </View>
                </View>
              </View>


              {this.props.selected_connection_type == Constants.POSTPAID ?
                <View>
                  {this.needToShowValue(this.props.dealerCollectionPaymentProps.depositAmount) ?
                    <View style={styles.subRow}>
                      <View style={styles.elementLeftView}>
                        <Text style={styles.cl2}>{this.state.locals.deposit}</Text>
                      </View>
                      <View style={styles.elementRightView1}>
                        <View style={styles.elementRightInnerView3}></View>
                        <View style={styles.elementRightInnerView1}>
                          <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                        </View>
                        <View style={styles.elementRightInnerView2}>
                          <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.depositAmount))}</Text>
                        </View>
                      </View>
                    </View>
                    :
                    <View />
                  }
                </View>
                :
                <View/>
              }
              
              {this.needToShowValue(this.props.dealerCollectionPaymentProps.deliveryInstallationCharge) ?
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={styles.cl2}>{this.state.locals.deliveryInstallation}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}></View>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.deliveryInstallationCharge))}</Text>
                    </View>
                  </View>
                </View>
                :
                <View />
              }           
              {this.needToShowValue(this.props.dealerCollectionPaymentProps.sameDayInstallationCharge) ?
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={styles.cl2}>{this.state.locals.sameDayInstallation}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}></View>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.sameDayInstallationCharge))}</Text>
                    </View>
                  </View>
                </View>
                :
                <View />
              }

              {this.needToShowValue(this.props.dealerCollectionPaymentProps.warrentyExtCharge) ?
                <View style={styles.subRow}>
                  <View style={styles.elementLeftView}>
                    <Text style={styles.cl2}>{this.state.locals.warrantyExt}</Text>
                  </View>
                  <View style={styles.elementRightView1}>
                    <View style={styles.elementRightInnerView3}></View>
                    <View style={styles.elementRightInnerView1}>
                      <Text style={styles.textStyleRight}>{this.state.locals.rs} </Text>
                    </View>
                    <View style={styles.elementRightInnerView2}>
                      <Text style={styles.textStyleRight}>{Utill.numberWithCommas(this.checkNumber(this.props.dealerCollectionPaymentProps.warrentyExtCharge))}</Text>
                    </View>
                  </View>
                </View>
                :
                <View />
              }
            </View>
              
            : 
            <View />
          }
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: TOTAL PAYMENT => LTE ACTIVATION', state.lteActivation);
  const language = state.lang.current_lang;
  const selected_connection_type = state.dtvActivation.selected_connection_type;

  return {
    language,
    selected_connection_type
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
    // marginLeft: 20,
    // marginRight: 7
  },
  containerRadioWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    // padding: 5,
    borderColor: Colors.borderLineColor
  },
  mainRow: {
    flexDirection: 'row',
    flex: 1
  },
  subRowWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  subRow: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,   
  },
  showDetails: {
    color: Colors.urlLinkColor,
    // textDecorationLine: 'underline',
    paddingRight: 9,
    // paddingLeft: 5,
    fontSize: Styles.otpEnterModalFontSize,
    fontWeight: 'normal'
  },
  cl2: {
    flexDirection: 'row',
    flex: 1.5,
    color: Colors.colorGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  subCatText: {
    flexDirection: 'row',
    marginLeft: 10,
    flex: 1.5,
    color: Colors.lightTextGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  lightTextClr:{
    color: Colors.lightTextGrey
  },
  totalPayment : {
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  totalPaymentValue : {
    textAlign: 'right',
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  elementLeftView : {

    flex: 0.6
  },
  elementRightView1 : {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  },
  elementRightInnerView1 : {
    flex: 0.2,
    alignItems: 'flex-start'
  },
  elementRightInnerView2 : {
    flex: 0.7,
    alignItems: 'flex-end'
  },
  elementRightInnerView3 : {
    flex: 0.1,
    alignItems: 'flex-start'
  },
  textStyleRight : {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },

});

export default connect(mapStateToProps, actions)(DealerCollectionBreakdown);