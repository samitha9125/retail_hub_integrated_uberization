/*
 * File: DtvMainViewContainer.js
 * Project: Dialog Sales App
 * File Created: Thursday, 4th October 2018 12:22:01 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 4th October 2018 2:45:01 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 *              Prasad Sampath Hewage (sampath@omobio.net)
 *              Manojkanth Rajendran (manojkanthan.rajendran@omobio.net)
 *              Devanshani Rasanjika (devanshani@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Keyboard, AppState } from 'react-native';
import Orientation from 'react-native-orientation';
import { Navigation } from 'react-native-navigation';
import RadioButton from 'radio-button-react-native';
import CheckBox from 'react-native-check-box';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import { Analytics, Screen, FuncUtils, MessageUtils } from '../../utills';
import { globalConfig, Constants, Images, Colors, Styles } from '../../config';
import Utills from '../../utills/Utills';
import MainView from './components/MainView';
import SectionContainer from './components/SectionContainer';
import MaterialInput from './components/MaterialInput';
import DropDownInput from './components/DropDownInput';
import InputSection from './components/InputSection';
import strings from '../../Language/LteActivation';
import strings_dtv from '../../Language/DtvActivation';
import _ from 'lodash';
import {
  CustomerOtpVerified,
  CustomerInfoNIC,
  CustomerInfoDrivingLicense,
  CustomerInfoPassport
} from './DtvKycCaptureSection';
import DealerCollectionBreakdown from '../dtvActivation/DealerCollectionBreakdown';
import CustomerPayment from '../dtvActivation/CustomerPayment';
import PaymentCalculations from './PaymentCalculation';
import EzCashBalance from './EzCashBalance';


const Utill = new Utills();

const DEBOUNCE_OPTIONS = { 'leading': false, 'trailing': true };

class DtvMainViewContainer extends React.Component {
  constructor(props) {
    super(props);
    strings.setLanguage(this.props.Language);
    this.state = {
      locals: {
        sectionNICValidation: strings_dtv.sectionNICValidation,
        sectionPPValidation: strings_dtv.sectionPPValidation,
        sectionCustomerInformation: strings.sectionCustomerInformation,
        sectionProductInformation: strings.sectionProductInformation,
        id_nic_mc: strings.id_nic_mc,
        id_passport_mc: strings.id_passport_mc,
        labelNIc: strings.labelNIc,
        labelPP: strings.labelPP,
        additional_channels: 'Additional channels (optional)',
        fulfillment_type: 'Fulfillment type',
        packType: 'Pack type',
        labelVoucherCode: strings.labelVoucherCode,
        labelScanSerialPack: strings.labelScanSerialPack,
        labelScanSimSerial: strings.labelScanSimSerial,
        labelIndividualSerial: strings.labelIndividualSerial,
        labelAnalogPhoneSerial: strings.labelAnalogPhoneSerial,
        serialType: strings.serialType,
        bundleSerial: strings.bundleSerial,
        individualSerial: strings.individualSerial,
        offer: strings.offer,
        voicePackages: strings.voicePackages,
        default_offer: strings.default_offer,
        radioBtnField_NIC: strings.radioBtnField_NIC,
        radioBtnField_Passport: strings.radioBtnField_Passport,
        radioBtnField_Driving_Licence: strings.radioBtnField_Driving_Licence,
        dataPackages: strings.dataPackages,
        area: strings.area,
        dataSachets: strings.dataSachets,
        depositAmount: strings.depositAmount,
        billingCycle: strings.billingCycle,
        captureNIC: strings.captureNIC,
        addressDifferentFromNIC: strings.addressDifferentFromNIC,
        addressDifferentFromNICPost: strings.addressDifferentFromNICPost,
        customerFaceNotClearNIC: strings.customerFaceNotClearNIC,
        customerFaceNotClearDL: strings.customerFaceNotClearDL,
        addressDifferentFromDL: strings.addressDifferentFromDL,
        addressDifferentFromDLPost: strings.addressDifferentFromDLPost,
        installationAddressDifferentFromBillingProof: strings_dtv.installationAddressDifferentFromBillingProof,
        installationAddressDifferentFromBillingProofPost: strings_dtv.installationAddressDifferentFromBillingProofPost,
        addressDifferentFromOtpVerifiedConnectionPre: strings.addressDifferentFromOtpVerifiedConnectionPre,
        addressDifferentFromOtpVerifiedConnectionPost: strings.addressDifferentFromOtpVerifiedConnectionPost,
        billingAddressDifferentFromPrevious: strings.billingAddressDifferentFromPrevious,
        sameDayInstallation: 'Same day installation Rs',
        activate_before: 'Activate before',
        enterAddress: 'Enter address',
        capturePOB: strings.capturePOB,
        capturePOBPost: strings.capturePOBPost,
        captureAdditionalPOB: strings.captureAdditionalPOB,
        captureAdditionalPOBPassport: strings.captureAdditionalPOBPassport,
        captureCustomerPhoto: strings.captureCustomerPhoto,
        capturePassport: strings.capturePassport,
        captureAddressPre: strings.captureAddressPre,
        captureAddress: strings.captureAddress,
        captureDrivingLicense: strings.captureDrivingLicense,
        labelCustomerEmail: strings.labelCustomerEmail,
        labelCustomerMobile: strings.labelCustomerMobile,
        labelCustomerLandline: strings.labelCustomerLandline,
        checkBoxTextAddVoice: strings.checkBoxTextAddVoice,
        checkBoxTextAnalogPhone: strings.checkBoxTextAnalogPhone,
        labelArea: strings.labelArea,
        labelConnectionNo: strings.labelConnectionNo,
        labelDataSachetReload: strings.labelDataSachetReload,
        rs_label: strings.rs_label,
        rs_label_with_dot: strings.rs_label_with_dot,
        ez_cash_account_balance: strings.ez_cash_account_balance,
        system_error_please_try_again: strings.system_error_please_try_again,
        do_uou_want_to_cancel_serial_scan_and_go_back: strings.do_uou_want_to_cancel_serial_scan_and_go_back,
        do_uou_want_to_cancel_image_capture_and_go_back: strings.do_uou_want_to_cancel_image_capture_and_go_back,
        enterSerial: strings.enterSerial,
        customerSignature: strings.customerSignature,
        ezCashPin: strings.ezCashPin,
        ez_cash_pin_title: strings.ez_cash_pin_title,
        txtActivate: strings.txtActivate,
        btnOk: strings.btnOk,
        btnCancel: strings.btnCancel,
        btnContinue: strings.btnContinue,
        btnYes: strings.btnYes,
        btnNo: strings.btnNo,
        btnRetry: strings.btnRetry,
        please_enter_valid_nic: strings.please_enter_valid_nic,
        please_enter_valid_pp: strings.please_enter_valid_pp,
        offerInfo: strings.offerInfo,
        packageInfo: strings.packageInfo,
        title_scan_serial: strings.title_scan_serial,
        please_enter_analog_phone_serial: strings.please_enter_analog_phone_serial,
        please_enter_valid_email: strings.please_enter_valid_email,
        please_enter_valid_mobile: strings.please_enter_valid_mobile,
        please_enter_valid_laneline: strings.please_enter_valid_laneline,
        please_enter_valid_voucher_code: strings.please_enter_valid_voucher_code,
        please_select_mobile_number: strings.please_select_mobile_number,
        please_capture_nic: strings.please_capture_nic,
        please_capture_passport: strings.please_capture_passport,
        please_capture_driving_licence: strings.please_capture_driving_licence,
        please_capture_pob: strings_dtv.please_capture_pob,
        please_enter_customer_face: strings.please_enter_customer_face,
        please_enter_poi: strings.please_enter_poi,
        please_enter_signature: strings.please_enter_signature,
        please_enter_ez_cash_pin: strings.please_enter_ez_cash_pin,
        offerPricePrefix: strings.offerPricePrefix,
        dataRentalPrefix: strings.dataRentalPrefix,
        tax: strings.tax,
        //DTV
        dtv_package: strings_dtv.package,
        dtvOffer: strings_dtv.dtvOffer,
        channels: 'channels',
        packs: 'packs',
        channel: 'channel',
        pack: 'pack',
        deliveryCharge: strings_dtv.deliveryCharge,
        labelStbPackSerial: strings_dtv.labelStbPackSerial,
        labelAccessoryPackSerial: strings_dtv.labelAccessoryPackSerial,
        labelBundleSerial: strings_dtv.labelBundleSerial,
        scan_accessories_serial: strings_dtv.scan_accessories_serial,
        scan_stbpack_serial: strings_dtv.scan_stbpack_serial,
        scan_bundle_serial: strings_dtv.scan_bundle_serial,
        checkBoxUpgradeToHybrid: strings_dtv.checkBoxUpgradeToHybrid,
        warrantyExtension: strings_dtv.warrantyExtension,
        firstReload: strings_dtv.firstReload,
        firstReloadTitleMin: strings_dtv.firstReloadTitleMin,
        firstReloadTitleMax: strings_dtv.firstReloadTitleMax,
        firstReloadErrorMax: strings_dtv.firstReloadErrorMax,
        firstReloadErrorMin: strings_dtv.firstReloadErrorMin,
        firstReloadErrorInvalidAmount: strings_dtv.firstReloadErrorInvalidAmount,
        dtvOafSerial: strings_dtv.dtvOafSerial,
        ok: strings_dtv.ok,
        customer_name: 'Name',
        customer_address_line1: 'Address line 1',
        customer_address_line2: 'Address line 2',
        customer_address_line3: 'Address line 3 (optional)',
        customer_city: 'City',
        customer_address_line_error: 'Only letters numbers #,.-_ allowed',
        customer_name_error: 'Only letters are allowed',
        customer_city_error: 'Enter valid city',
        confirmAndSign: strings_dtv.modalTitle,
        sameDayInstallationTitle: strings_dtv.sameDayInstallation,
        eoafSerialModalName: strings_dtv.eoafSerialModalName,
        transaction_id: 'Transaction ID',
        cir_no: 'CIR no.'
      },

      emailError: '',
      mobileError: '',
      landlineError: '',
      currentTime: '',
      appState: 'active'
    };

    this.signManditoryFields = [];
    this.debouncedCitySearch = _.debounce((requestParams, text) => this.dtvCitySearch(requestParams, text), 500, DEBOUNCE_OPTIONS);
    this.debouncedCheckboxSelect = _.debounce((checkBoxType) => this.checkStbUpgrade(checkBoxType), 5, DEBOUNCE_OPTIONS);
    this.debouncedFinalActivation = _.debounce(() => this.onPressActivate(), 150, DEBOUNCE_OPTIONS);
    this.debouncedonBlurReloadRapidEzCheck = _.debounce(() => this.onBlurReloadRapidEzCheck(), 5, DEBOUNCE_OPTIONS);
  }

  //-----------------------------LIFE CYCLE EVENTS------------------------//

  componentWillUnmount() {
    console.log('componentWillUnmount');
    //AppState.removeEventListener('change', this._handleAppStateChange);
    Orientation.lockToPortrait();
    this.props.resetDTVData(true);
    if (this.props.cancelVoucherCodeReservation && this.props.dtv_activation_status == false) {
      this.props.cancelVoucherCodeReservation();
    }
  }

  componentDidMount() {
    console.log('componentDidMount');
    //AppState.addEventListener('change', this._handleAppStateChange);
    Orientation.lockToPortrait();
    let me = this;
    me.props.resetDTVData(true);
    this.checkRapidEzcashModal();
  }

  componentDidUpdate() {
    console.log('componentDidUpdate this.props\n', this.props);
    console.log('componentDidUpdate this.state\n', this.state);
  }

  //-----------------------------LIFE CYCLE EVENTS END------------------------//

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({ appState: nextAppState });
  };

  goToHomePage = () => {
    console.log('xxx goToHomePage');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });
    let me = this;
    if (me.props.cancelVoucherCodeReservation && this.props.dtv_activation_status == false)
      me.props.cancelVoucherCodeReservation();
    setTimeout(() => {
      me.props.resetLTEData(true);
    }, 50);
  }

  checkRapidEzcashModal = () => {
    if (!this.props.getConfiguration.rapid_ez_account.rapid_ez_availability && this.props.enable_reload) {
      this.topViewGeneralModal();
    }
  }

  /**
   * @description Top View General modal
   * @param {Object} passProps
   * @memberof BottomItemsPre
   */
  topViewGeneralModal = () => {
    let passProps = {
      primaryText: this.state.locals.btnContinue,
      primaryPress: () => this.onPressContinue(),
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.AttentionAlert,
      description: Utill.getStringifyText(this.props.rapid_ez_error),
      primaryButtonStyle: { color: Colors.colorYellow },
    };

    Screen.showGeneralModal(passProps);
  };

  /**
   * @description dismiss modal
   * @memberof BottomItemsPre
   */
  onPressContinue = () => {
    console.log('xxx modalDismiss');
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  };


  onPressOtp = () => {
    console.log('xxx onPressOtp');
    let screen = {
      title: 'OtpNumberSelectionModal',
      id: 'DialogRetailerApp.modals.OtpNumberSelectionModal',
    };
    let passProps = {
      messageType: 'success'
    };
    Utill.showModalView(screen, passProps);
  }

  onConfirmModal = (navigator) => {

    if ( FuncUtils.isNullOrUndefined(this.props.tcUrls)){
      return;
    }
    console.log('xxx onConfirmModal');
    let screen = {
      title: this.state.locals.confirmAndSign,
      id: 'DialogRetailerApp.modals.DTVConfirmModal',
      overrideBackPress: true
    };
    let passProps = {
      messageType: 'success',
      local_foreign: this.props.customerType,
      idNumber: this.props.idNumber_input_value,
      connectionNo: this.props.connectionNo,
      pPlan: this.props.selected_connection_type,
      selectedDTVDataPackageDetails: this.props.selectedDTVDataPackageDetails,
      selected_channel_n_packs: this.props.selected_channel_n_packs,
      selected_channel_total_package_rental: this.props.selected_channel_total_package_rental,
      selectedFullFillmentType: this.props.dtv_fullfillment_type,
      dtv_pack_type: this.props.dtv_pack_type,
      availableDtvOffers: this.props.availableDtvOffers,
      enteredDTVVoucherCode: this.props.enteredDTVVoucherCode,
      stb_upgrade_checked: this.props.stb_upgrade_checked,
      dtv_warranty_extensions: this.props.dtv_warranty_extensions,
      data_totalPaymentProps: this.props.data_totalPaymentProps,
      opt_sameDayInstallation: this.props.opt_sameDayInstallation,
      tncUrl: this.getTermAndConditionUrl(),
      stb_upgrade_available: this.props.stb_data,
      // package: this.props.selectedDTVDataPackageDetails,
      // pRental: 'Rs 6.00 + Tax daily',
      // additionalChannels: 'BBC, Cinemax , Discovery &\nSports pack',
      // additionalRental: 'Rs 10.00 + Tax daily',
      // totalRental: 'Rs 16.00 + Tax daily',
      // fulfillment: 'Cash & carry',
      // packType: 'Full pack',
      // offer: 'Standard offer Rs 2990.00',
      // voucherCode: '538635679876',
      // freeVisitCount: '10',
      // stbType: 'Hybrid',
      // stbWarranty: '2 year & 6 months',
      // rcuWarranty: '6 months',
      // pcuWarranty: '6 months',
      // lnbWarranty: '1 year',
      // lblWarrantyExtension: '12 months',
      dtvMobileNumber: this.props.dtvMobileNumber,
      customerLandline: this.props.customerLandline,
      email_input_value: this.props.email_input_value,
      installationAddress: {
        customer_address_line1: this.props.customer_address_line1.value,
        customer_address_line2: this.props.customer_address_line2.value,
        customer_address_line3: this.props.customer_address_line3.value,
        customer_city: this.props.customer_city.city_name
      },
      // outstanding: 'Rs  2,990.00',
      // deliveryAndInstallation: 'Rs  1,000.00',
      // unitPrice: 'Rs  1,000.00',
      // voucher: 'Rs  1,000.00',
      // warrantyExtension: 'Rs  1,000.00',
      reloadAmountValue: this.props.reloadAmountValue,
      // sameDayInstallation: 'Rs  1,500.00',
      // totalPayment: 'Rs  10,000.00'
    };

    navigator.showModal({
      title: screen.title,
      screen: screen.id,
      passProps: passProps,
      // overrideBackPress: true
    });
    // Utill.showModalView(screen, passProps, true);
  }

  onPressInfo = (availableDataList, modalType) => {
    console.log('xxx onPressInfo', availableDataList);
    let selectedDTVDataPackageDetails = this.props.selectedDTVDataPackageDetails;
    console.log('xxx selectedDTVDataPackageDetails', selectedDTVDataPackageDetails);
    if (availableDataList == undefined || selectedDTVDataPackageDetails == undefined) {
      return;
    }

    let screen = {
      title: '',
      id: 'DialogRetailerApp.modals.PackageSelectionModal',
    };

    let passProps = {};

    switch (modalType) {
      case 'offers':
        passProps = {
          modalTitle: this.state.locals.offerInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onOfferInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType
        };
        break;
      case 'datapackages':
        passProps = {
          modalTitle: this.state.locals.packageInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onDTVPkgInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType,
          selectedDTVDataPackageDetails: this.props.selectedDTVDataPackageDetails,
          idx: availableDataList.defaultIndex,
          value: availableDataList.selectedValue
        };
        break;
      case 'voicepackages':
        passProps = {
          modalTitle: this.state.locals.packageInfo,
          dropDownData: availableDataList.dropDownData,
          defaultIndex: availableDataList.defaultIndex,
          onSelect: this.onPressVoicePkgInfoDropdownSelect,
          selectedValue: availableDataList.selectedValue,
          modalType: modalType
        };
        console.log(JSON.stringify(passProps));
        break;
      default:
        Utill.showAlert(this.state.locals.system_error_please_try_again);
    }
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Serial scanning functions
   * @param {String} serialType
   * @memberof DtvMainViewContainer
   */
  onPressSerialScan = (serialType) => {
    console.log('xxx dtv_pack_type  ::::', this.props.dtv_pack_type);
    console.log('xxx dtv_pack_type  ::::', this.props.availableDTVDataPackagesList.data_package_code);
    console.log('xxx availableDtvOffers  ::::', this.props.availableDtvOffers.offer_code);
    this.props.commonResetKycSignature();
    if (this.props.voucher_code_enabled && !this.props.isValidDTVVoucherCode && this.props.validatedVoucherCode == '') {

      return;
    }

    if (this.props.dtv_pack_type.packageCode == '' || this.props.dtv_pack_type.packageCode == undefined
      || this.props.availableDtvOffers.offer_code == '' || this.props.availableDtvOffers.offer_code == undefined) {
      return;
    }
    console.log('xxx onPressSerialScan', serialType);
    let me = this;

    let screen = {
      title: 'SCAN SERIALS',
      id: 'DialogRetailerApp.views.BarcodeScannerModule',
    };

    let serialMaxLength = Constants.DEFAULT_SERIAL_MAX_LENGTH;
    let screenTitle = me.state.locals.title_scan_serial;
    let displayMessage = '';
    let materialType = 'INDIVIDUAL';
    let inputValidateType = 'NUMERIC';
    let keyboardType = 'numeric';
    let setSerialValueRedux;
    let productFamily;
    let autoValidation = false;
    let sliceNumber = false;
    let sliceLength = 0;
    let additionalPramsForAPI = {
      pack_code: this.props.dtv_pack_type.packageCode,
      offer_code: this.props.availableDtvOffers.offer_code,
      conn_type: this.props.selected_connection_type,
      package_code: this.props.availableDTVDataPackagesList.data_package_code,
      stb_upgrade_checked: this.props.stb_upgrade_checked

    };

    switch (serialType) {
      case 'DTV_BUNDLE':
        console.log('xxx onPressSerialScan DTV_BUNDLE');
        materialType = 'BUNDLE';
        serialMaxLength = Constants.BUNDLE_SERIAL_MAX_LENGTH;
        displayMessage = this.state.locals.scan_bundle_serial;
        setSerialValueRedux = me.props.dtvSetBundleSerial;
        productFamily = Constants.PRODUCT_FAMILY_LTE_CPE;
        break;
      case 'DTV_STB':
        console.log('xxx onPressSerialScan DTV_STB');
        displayMessage = this.state.locals.scan_stbpack_serial;
        serialMaxLength = Constants.PACK_SERIAL_MAX_LENGTH;
        setSerialValueRedux = me.props.dtvSetSerialPack;
        productFamily = Constants.PRODUCT_FAMILY_LTE_CPE;
        break;
      case 'DTV_ACCESSORIES':
        console.log('xxx onPressSerialScan DTV_ACCESSORIES');
        displayMessage = this.state.locals.scan_accessories_serial;
        serialMaxLength = Constants.PACK_SERIAL_MAX_LENGTH;
        setSerialValueRedux = me.props.dtvSetAccessoriesSerial;
        productFamily = Constants.PRODUCT_FAMILY_LTE_SIM;
        break;
      default:
        console.log('xxx onPressSerialScan default');
        break;
    }

    let passProps = {
      screenTitle: screenTitle,
      backMessage: me.state.locals.do_uou_want_to_cancel_serial_scan_and_go_back,
      displayMessage: displayMessage,
      textInputLabel: me.state.locals.enterSerial,
      serialMaxLength: serialMaxLength,
      autoValidation: autoValidation,
      sliceNumber: sliceNumber,
      sliceLength: sliceLength,
      materialType: materialType,
      serialType: serialType,
      inputValidateType: inputValidateType,
      keyboardType: keyboardType,
      productFamily: productFamily,
      lob: Constants.LOB_DTV,
      setSerialValueRedux: setSerialValueRedux,
      additionalPramsForAPI: additionalPramsForAPI,
      resetSerialValidationStatus: () => this.resetSerialValidationStatus(),
      setSerialInputValue: (serialValue) => this.setSerialInputValue(serialValue),
      resetSerialFieldValues: (serialType) => this.resetSerialFieldValues(serialType),
      validateSerialNumberAPI: (requestParam, url, barcodeScreen) => this.props.dtvSerialValidationNumber(requestParam, url, barcodeScreen),
      hasNextScreen: false,
      additionalProps: {
        bundleSerialData: me.props.bundleSerialData,
        serialPackData: me.props.serialPackData,
        simSerialData: me.props.simSerialData,
        conn_type: me.props.selected_connection_type,
        lob: Constants.LOB_DTV,
        id_number: me.props.idNumber_input_value,
        id_type: me.props.customerInfoSectionIdType,
        customer_status: me.props.customerExistence == 'N' ? 'NEW' : 'EXIST'

      }
    };

    Screen.pushView(screen, me, passProps);
  }

  /**
   * @description Reset serial validation status
   * @memberof DtvMainViewContainer
   */
  resetSerialValidationStatus = () => {
    console.log('xxx resetSerialValidationStatus :: LTE');
    let me = this;
    me.props.dtvResetSerialValidationStatus();
  };

  /**
   * @description Set serial input serial value
   * @memberof DtvMainViewContainer
   */
  setSerialInputValue = (value) => {
    console.log('xxx setSerialInputValue :: LTE', value);
    let me = this;
    me.props.dtvSetSerialTextValue(value);
  };

  /**
   * @description Reset serial input fields
   * @param {String} serialType
   * @memberof DtvMainViewContainer
   */
  resetSerialFieldValues = (serialType) => {
    console.log('xxx resetSerialFieldValues :: LTE', serialType);
    this.props.dtvResetIndividualSerialFieldValues(serialType);

  };


  /**
   * @description dismiss modal
   * @memberof DtvMainViewContainer
   */
  modalDismiss = () => {
    console.log('xxx modalDismiss');
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  /**
   * @description Show inline validation error message in idNumber field
   * @param {Object} response
   * @param {String} description
   * @memberof DtvMainViewContainer
   */
  showInlineErrorMessageIdNumber = (response, description) => {
    console.log('xxx showInlineErrorMessageIdNumber :: response', response);
    console.log('xxx showInlineErrorMessageIdNumber :: response', description);
    //let description = 'You must be older than 18 years to get connection.';
    let me = this;
    me.props.dtvSetIdNumberError(description);
  };

  /**
   * @description Show Maximum Number Exceeded Modal or Blacklisted Customer Modal
   * @param {Object} response
   * @param {String} description
   * @memberof DtvMainViewContainer
   */
  showCustomModalTypeMessage = (response, description) => {
    console.log('xxx showCustomModalTypeMessage :: response', response);
    //let description_1 = 'Entered NIC has exceeded the maximum number (5) of connections. \n\nInform the customer to visit the nearest Dialog service centre to disconnect any connections not in use.';
    //let description_2 = 'Entered NIC is not eligible to get a new connection. \n\nInform the customer to visit the nearest Dialog service centre to disconnect any connections not in use.';
    let passProps = {
      primaryText: this.state.locals.btnOk,
      disabled: false,
      hideTopImageView: true,
      title: this.getCustomerIdTypeText() + ' : ' + this.props.idNumber_input_value,
      icon: Images.icons.SuccessAlert,
      description: description,
      additionalProps: response
    };

    this.dtvGeneralModal(passProps);

  };

  /**
   * @description Show Maximum Number Exceeded Modal or Blacklisted Customer Modal
   * @param {Object} response
   * @param {String} description
   * @memberof DtvMainViewContainer
   */
  showModalTypeMessage = (response, description) => {
    console.log('xxx showModalTypeMessage :: response', response);
    let okFn = null;
    let okText = this.state.locals.btnOk;
    let title = this.getCustomerIdTypeText() + ' : ' + this.props.idNumber_input_value;
    let message = description;

    Screen.showDialogBox(message, title, okFn, okText);

  };

  /**
   * @description Show General error modal
   * @param {Object} error
   * @memberof DtvMainViewContainer
   */
  showGeneralErrorModal = (error) => {
    console.log('xxx showGeneralErrorModal :: error', error);
    let description = MessageUtils.getFormatedErrorMessage(error, this.props.Language);
    let passProps = {
      primaryText: this.state.locals.btnOk,
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      icon: Images.icons.FailureAlert,
      description: description,
    };

    this.dtvGeneralModal(passProps);
  };

  /**
   * @description DTV General modal
   * @param {Object} passProps
   * @memberof DtvMainViewContainer
   */
  dtvGeneralModal = (passProps) => {
    console.log('xxx dtvGeneralModal :: passProps', passProps);
    Screen.showGeneralModal(passProps);
  }


  /**
   * @description Show notice alert
   * @param {Text} description
   * @memberof DtvMainViewContainer
   */
  showNoticeAlert = (description) => {
    console.log('showNoticeAlert :: description', description);
    let passProps = {
      primaryText: this.state.locals.ok,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.AttentionAlert,
      description: description,
      primaryButtonStyle: { color: Colors.colorYellow },

    };

    Screen.showGeneralModal(passProps);
  };

  /**
   * @description Show notices alert with title
   * @param {Object} message
   * @memberof DtvMainViewContainer
   */
  showAlertWithTitle = (description, title) => {
    console.log('xxx DTV showNoticesAlert :: description', description, 'title: ', title);

    let passProps = {
      primaryText: this.state.locals.ok,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.FailureAlert,
      description: description,
      descriptionTitle: title,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorRed },

    };

    Screen.showGeneralModal(passProps);
  };

  setCusInfoMode = (infoType) => {
    console.log('xxx setCusInfoMode :: infoType :', infoType);
    this.props.dtvSetCustomerInfoSectionIdType(infoType);
    this.props.dtvResetCustomerInfoSectionChange();
  }

  onDTVPkgInfoDropdownSelect = (idx, value, modalType) => {
    this.props.dtvForceUpdateView();
    console.log('onDTVPkgInfoDropdownSelect id,value', idx, value, modalType);
    console.log('availableDTVDataPackagesList', this.props.availableDTVDataPackagesList);
    let packageList = this.props.availableDTVDataPackagesList.packageList;
    console.log('Selected packageList', packageList);
    let selectedPackageCode = packageList[idx].package_code;
    console.log('Selected Package Code', selectedPackageCode);

    if (this.props.availableDTVDataPackagesList.data_package_code == selectedPackageCode) {
      return;
    }
    this.props.resetFullFillMentType();
    if (this.props.cancelVoucherCodeReservation) {
      this.props.cancelVoucherCodeReservation();
    }
    this.props.resetDtvPackType();
    this.props.dtvResetStbUpgradeData();
    this.props.dtvResetEoafSerialPack();
    this.props.dtvResetIndividualSerialFieldValues();
    this.props.dtvResetDepositAmount();
    this.props.dtvSetReloadAmound('');
    this.props.commonResetKycSignature();
    let selectedData = {
      dropDownData: _.toArray(_.mapValues(packageList, 'package_name')),
      data_package_code: selectedPackageCode,
      selectedValue: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name,
      defaultIndex: _.findIndex(packageList, ['package_code', selectedPackageCode]),
      packageList: packageList,
      default_fullfil_status: this.props.availableDTVDataPackagesList.default_fullfil_status,
      selectedFullFillmentType: _.filter(packageList, { 'package_code': selectedPackageCode })[0].fullfilment_type,
      package_rental: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_rental,
      idx: idx,
      value: value,
      selectedValueName: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name_wo_price,
    };

    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_package_select',
      item_id: selectedPackageCode,
      content_type: 'dropdown',
      additional_details: selectedPackageCode,
    });

    let paymentProps = {
      totalDailyRental: selectedData.package_rental
    };
    this.props.setSelectedChannelTotalPayment(paymentProps);


    let requestParams = {
      conn_type: this.props.selected_connection_type,
      cx_identity_no: this.props.idNumber_input_value,
      cx_identity_type: this.props.customerIdType,
      package_code: selectedPackageCode,
      txn_reference: this.props.txn_reference,
    };
    console.log('onDataPackageInfoDropdownSelect :: Selected Data Ob', selectedData);
    this.props.getDtvPackageDetails(requestParams, selectedData);

    this.props.clearChannelAndPacksData();
    this.props.getDtvFullFillmentType(selectedData, this, modalType);
    this.props.dtvResetOffers();

    let FullFillmentTypeArrayWithDefaultCode = _.filter(selectedData.selectedFullFillmentType, { 'code': selectedData.default_fullfil_status.code });

    try {
      let packTypeParams = {
        conn_type: this.props.selected_connection_type,
        fulfilment_type: FullFillmentTypeArrayWithDefaultCode.length !== 0 ? FullFillmentTypeArrayWithDefaultCode[0].code : '',
        package_code: selectedPackageCode,
        txn_reference: this.props.txn_reference,
      };
      this.props.getDtvPackType(packTypeParams);

    } catch (error) {
      console.log('fulfillment_type Exp', error);
      Screen.showAlert(this.state.locals.system_error_please_try_again);
    }
  } 
  

  onPressFulfillmentTypeDropdownSelect = (idx, value) => {
    console.log('onPressFulfillmentTypeDropdownSelect', idx, value);
    this.props.dtvForceUpdateView();
    let fulfillmentType = this.props.dtv_fullfillment_type.selectedFullFillmentType;
    console.log('packageTypeList :: fulfillmentType - ', fulfillmentType);
    let selectedPackageType = fulfillmentType[idx].code;
    console.log('Selecteddtv_fullfillment_type', selectedPackageType);
    console.log('Selecteddtv_fullfillment_type', this.props.dtv_fullfillment_type.package_code);

    if ( selectedPackageType == this.props.dtv_fullfillment_type.package_code ){

      return;
    }

    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.dtvResetIndividualSerialFieldValues();
    this.props.dtvResetStbUpgradeData();
    this.props.dtvResetVoucerValue();
    this.props.dtvResetEoafSerialPack();
    this.props.dtvSetReloadAmound('');
    this.props.commonResetKycSignature();
    
    const fulfillmentSelectedData = {
      dropDownData: _.toArray(_.mapValues(fulfillmentType, 'display_name')),
      // selectedValue: _.filter(fullFillmentType, { 'code': selectedPackageType })[0].display_name,
      selectedValue: fulfillmentType.selectedValue == "" ? fulfillmentType.default_fullfil_status.display_name : _.filter(fulfillmentType, { 'code': selectedPackageType })[0].display_name,
      package_code: selectedPackageType,
      defaultIndex: _.findIndex(fulfillmentType, ['code', selectedPackageType]),
      // defaultIndex: _.findIndex(selectedData.selectedFullFillmentType, ['code', selectedData.selectedFullFillmentType]),
      // packageList: response.data.packageList,
      selectedFullFillmentType: fulfillmentType
    };
    console.log('fulfillmentSelectedData API ::', fulfillmentSelectedData);
    this.props.setFullFillMentType(fulfillmentSelectedData);

    let packTypeParams = {
      conn_type: this.props.selected_connection_type,
      fulfilment_type: selectedPackageType,
      package_code: this.props.availableDTVDataPackagesList.data_package_code,
      txn_reference: this.props.txn_reference,

    };

    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_fulfillment_type_select',
      item_id: selectedPackageType,
      content_type: 'dropdown',
      additional_details: selectedPackageType,
    });

    console.log('onPressFulfillmentTypeDropdownSelect :: packTypeParams', packTypeParams);
    this.props.getDtvPackType(packTypeParams);
    this.props.dtvResetOffers();
    this.props.dtvResetDepositAmount();
    this.props.dtvResetCustomerInfoSection();
    this.props.dtvSetCustomerInfoValidationStatus(false);

    if (selectedPackageType == Constants.CASH_AND_DELIVERY) {
      console.log("onPressFulfillmentTypeDropdownSelectdtvOafSerialValidation", this.props.selected_connection_type);
      let oafParams = {
        lob: Constants.LOB_DTV,
        conn_type: this.props.selected_connection_type
      };
      this.props.dtvOafSerialValidation(oafParams, this);
    }
   
  }

  onPressPackTypeDropdownSelect = (idx, value) => {
    console.log('onPressPackTypeDropdownSelect', idx, value);
    this.props.dtvForceUpdateView();
    if (this.props.cancelVoucherCodeReservation) {
      this.props.cancelVoucherCodeReservation();
    }
    this.props.dtvResetStbUpgradeData();
    this.props.commonResetKycSignature();
    this.props.dtvResetVoucerValue();
    this.props.dtvResetIndividualSerialFieldValues();
    this.props.dtvResetDepositAmount();
    this.props.dtvSetReloadAmound('');
    let packageTypeList = this.props.dtv_pack_type.packageType;
    console.log('onPressPackTypeDropdownSelect :: packageTypeList -', packageTypeList);
    let selectedPackageType = packageTypeList[idx].key;
    console.log('onPressPackTypeDropdownSelect :: selectedPackageType - ', selectedPackageType);
    const selectedData = {
      dropDownData: _.toArray(_.mapValues(packageTypeList, 'pack_type')),
      selectedValue: _.filter(packageTypeList, { 'key': selectedPackageType })[0].pack_type,
      defaultIndex: _.findIndex(packageTypeList, ['key', selectedPackageType]),
      packageType: packageTypeList,
      packageCode: selectedPackageType,

    };

    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_pack_type_select',
      item_id: selectedPackageType,
      content_type: 'dropdown',
      additional_details: selectedPackageType,
    });
    console.log('onPressPackTypeDropdownSelect :: selectedData ', selectedData);
    this.props.setPackType(selectedData);
    this.props.dtvResetOffers();
    this.props.dtvResetCustomerInfoSection();
    this.props.dtvSetCustomerInfoValidationStatus(false);
  };

  /**
   * @description STB upgrade checkbox select
   * @param {String} checkBoxType
   * @memberof DtvMainViewContainer
   */
  checkStbUpgrade = (checkBoxType) => {
    console.log('## checkStbUpgrade :: checkBoxType ', checkBoxType);
    this.props.dtvCheckStbUpgrade(!this.props.stb_upgrade_checked);
    this.props.dtvResetIndividualSerialFieldValues();
    this.props.commonResetKycSignature();

  };

  /**
   * @description Warranty Extension DropdownSelect
   * @param {int} idx
   * @param {String} value
   * @memberof DtvMainViewContainer
   */
  onPressWarrantyExtensionDropdownSelect = (idx, value) => {
    console.log('onPressWarrantyExtensionDropdownSelect', idx, value);
    this.props.dtvForceUpdateView();
    this.props.commonResetKycSignature();
    let dtv_warranty_extensions_dropdown_data = this.props.dtv_warranty_extensions;
    let dtv_warranty_extensions_data = this.props.dtv_warranty_extensions_data;
    console.log('onPressWarrantyExtensionDropdownSelect :: dtv_warranty_extensions_data - ', dtv_warranty_extensions_data);
    console.log('onPressWarrantyExtensionDropdownSelect :: dtv_warranty_extensions_dropdown_data - ', dtv_warranty_extensions_dropdown_data);

    let selectedPackCode = dtv_warranty_extensions_data[idx].pack_code;
    let selectedPackPrice = dtv_warranty_extensions_data[idx].price;
    let time_period = dtv_warranty_extensions_data[idx].time_period;
    let selectedValueOb = _.find(dtv_warranty_extensions_data, { 'pack_code': selectedPackCode });

    console.log('onPressWarrantyExtensionDropdownSelect :: selectedPackCode - ', selectedPackCode);
    console.log('onPressWarrantyExtensionDropdownSelect :: selectedPackPrice - ', selectedPackPrice);
    console.log('onPressWarrantyExtensionDropdownSelect :: selectedValueOb - ', selectedValueOb);

    const selectedData = {
      dropDownData: _.toArray(_.mapValues(dtv_warranty_extensions_data, 'pack_name')),
      selectedValue: _.find(dtv_warranty_extensions_data, { 'pack_code': selectedPackCode }).pack_name,
      packCode: selectedPackCode,
      packPrice: selectedPackPrice,
      time_period: time_period
    };

    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_warranty_extensions_select',
      item_id: selectedPackCode,
      content_type: 'dropdown',
      additional_details: selectedPackCode,
    });

    console.log('onPressWarrantyExtensionDropdownSelect :: selectedData -', selectedData);
    this.props.dtvSetWarrantyExtensionsData(selectedData, dtv_warranty_extensions_data);

  }

  /**
   * @description NIC and Passport validation on text change event
   * @param {Number} nicNumber
   * @memberof DtvMainViewContainer
   */
  onIdNumberTextChange = (nicNumber) => {
    this.props.commonResetKycSignature();
    console.log("onIdNumberTextChange", nicNumber);
    this.props.dtvSetInputIdNumber(nicNumber);
    this.clearReduxValues('idValidation');
    if (this.props.cancelVoucherCodeReservation) {
      this.props.cancelVoucherCodeReservation();
    }
    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      this.props.dtvSetCustomerInfoSectionIdType(Constants.CX_INFO_TYPE_NIC);
      //Check wether message need to show or not
      if (Utill.nicNumberRealTimeValidate(nicNumber).showMessage) {
        console.log('onIdNumberTextChange :: Invalid ID number');
        this.props.dtvSetIdNumberError(this.state.locals.please_enter_valid_nic);
      } else {
        console.log('onIdNumberTextChange :: Valid ID number');
        this.props.dtvSetIdNumberError('');
        //Check NIC validity
        if (Utill.nicNumberRealTimeValidate(nicNumber).status) {
          //Call the NIC validation API
          console.log('onIdNumberTextChange :: Call the NIC validation API');
          const requestParams = {
            conn_type: this.props.selected_connection_type,
            lob: Constants.LOB_DTV,
            cx_identity_no: nicNumber,
            cx_identity_type: 'NIC'
          };

          Keyboard.dismiss();
          this.props.dtvCustomerValidation(requestParams, this.props.selected_connection_type, this);

          this.state.currentTime = new Date().toLocaleString();
          console.log('current time', this.state.currentTime);
        }
      }
    } else {
      this.props.dtvSetCustomerInfoSectionIdType(Constants.CX_INFO_TYPE_PASSPORT);
      if (Utill.passportNumberRealTimeValidate(nicNumber).showMessage) {
        console.log('onIdNumberTextChange :: Invalid ID number');
        this.props.dtvSetIdNumberError(this.state.locals.please_enter_valid_pp);
      } else {
        console.log('onIdNumberTextChange :: Valid ID number');
        this.props.dtvSetIdNumberError('');
      }
    }
  }

  /**
   * @description Passport validation backend API call
   * @memberof DtvMainViewContainer
   */
  onPressValidatePassport = () => {
    console.log('onPressValidatePassport');
    if (this.props.cancelVoucherCodeReservation) {
      this.props.cancelVoucherCodeReservation();
    }
    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      console.log('onPressValidatePassport :: PASSPORT - SKIP_API_CALL');
      return;
    }
    if (Utill.passportNumberRealTimeValidate(this.props.idNumber_input_value).status) {
      console.log('onPressValidatePassport :: PASSPORT - ALLOW_API_CALL');
      const requestParams = {
        conn_type: this.props.selected_connection_type,
        lob: Constants.LOB_DTV,
        cx_identity_no: this.props.idNumber_input_value,
        cx_identity_type: 'PP'

      };
      this.props.dtvSetIdNumberError('');
      Keyboard.dismiss();
      this.props.dtvCustomerValidation(requestParams, this.props.selected_connection_type, this);
    } else {
      this.props.dtvSetIdNumberError(this.state.locals.please_enter_valid_pp);
    }
  }

  /**
   * @description First Reload amount validation on text change event
   * @param {Number} reloadAmount
   * @memberof DtvMainViewContainer
   */
  onFirstReloadAmountTextChange = (reloadAmount = '') => {
    this.props.commonResetKycSignature();
    let first_reload_validation_error = '';
    console.log("on Reload amount", reloadAmount);
    var reg = /^\d+$/;

    if (!reg.test(reloadAmount)) {
      first_reload_validation_error = this.state.locals.firstReloadErrorInvalidAmount;
    }
    if (reloadAmount < 100) {
      console.log("Reload amount less than min");
      first_reload_validation_error = `${this.state.locals.firstReloadErrorMin}100`;
      this.props.dvtSetValidationError(first_reload_validation_error);
    } else {
      this.props.dvtSetValidationError('');
    }

    if (reloadAmount > 25000) {
      console.log("Reload amount greater than max");
      first_reload_validation_error = `${this.state.locals.firstReloadErrorMax}25,000`;
      this.props.dvtSetValidationError(first_reload_validation_error);
    } else {
      this.props.dvtSetValidationError('');
    }

    if (_.isEmpty(reloadAmount)) {
      first_reload_validation_error = '';
    }
    this.props.dvtSetValidationError(first_reload_validation_error);
    this.props.dtvSetReloadAmound(reloadAmount);
  }

  /**
   * @description First Reload amount RapidezErrorCheck
   * @param {Number} reloadAmount
   * @memberof DtvMainViewContainer
   */
  onBlurReloadRapidEzCheck = () => {
    if (this.props.reloadAmountValue == 0 || this.props.reloadAmountValue == ''){
      console.log('xxx onBlurReloadRapidEzCheck return', this.props.reloadAmountValue);
      return;
    }
    
    const requestParams = {
      amount: this.props.reloadAmountValue
    };
    this.props.commonRapidEzAccountCheck(requestParams, this.agentAccountCheckSuccess, this);
  }

  agentAccountCheckSuccess = () => {
    console.log('xxx agentAccountCheckSuccess');
  }


  /**
   * @description First Reload amount showRapidEzErrorModal
   * @param {Number} reloadAmount
   * @memberof DtvMainViewContainer
   */
  showRapidEzErrorModal = (response) => {
    console.log('xxx showRapidEzErrorModal');
    let passProps = {
      primaryText: this.state.locals.ok,
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      icon: Images.icons.FailureAlert,
      descriptionTitle: response.title,
      description: Utill.getStringifyText(response.error),
      descriptionBottomText: response.balance,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryPress: () => this.dismissModal()
    };

    Navigation.showModal({
      title: 'CommonModalAlert',
      screen: 'DialogRetailerApp.modals.CommonModalAlert',
      passProps: passProps
    });
  }

  dismissModal = () => {
    this.props.dvtSetValidationError('');
    Navigation.dismissModal({ animationType: 'slide-down' });
    const reloadAmount = '';
    this.props.dtvSetReloadAmound(reloadAmount);
  };

  /**
   * @description Method to handle section validation status
   * @param {*} key
   * @memberof DtvMainViewContainer
   */
  onPressSection = async (key) => {
    console.log('## onPressSection :', key);
    await this.props.dtvSetProductInfoValidationStatus(this.props.sectionProductInfo_validation_status_fromMapToStatus);
    let validation_status = {
      section_1: this.props.sectionId_validation_status,
      section_2: this.props.sectionProductInfo_validation_status,
      section_3: this.props.sectionCustomerInfo_validation_status
    };

    let current_View_status = {
      section_1: this.props.section_1_visible,
      section_2: this.props.section_2_visible,
      section_3: this.props.section_3_visible
    };

    await this.props.dtvSetSectionVisibility(key, validation_status, current_View_status);
  }

  /**
   * @description Show OTP number selection modal 
   * @param {Object} customerData
   * @memberof DtvMainViewContainer
   */
  showOtpNumberSelectionModal = (customerData) => {
    console.log('xxx showOtpNumberSelectionModal :: customerData', customerData);
    let screen = {
      title: 'OtpNumberSelectionModal',
      id: 'DialogRetailerApp.modals.OtpNumberSelectionModal',
    };
    let passProps = {
      modalTitle: 'OtpNumberSelectionModal',
      customerData: customerData,
      sendOtpMessage: this.sendOtpMessage,

    };
    Utill.showModalView(screen, passProps);
  }

  /**
   * @description Show OTP entering modal (Send OTP success callback)
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  showOtpEnteringModal = (response) => {
    console.log('xxx showOtpEnteringModal :: response', response);
    this.modalDismiss();
    let screen = {
      title: 'OtpEnterModal',
      id: 'DialogRetailerApp.modals.OtpEnterModal',
    };
    let passProps = {
      modalTitle: 'OtpEnterModal',
      otpInfo: response.info,
      response: response,
      reSendOtpMessage: this.reSendOtpMessage,
      verifyOtp: this.verifyOtp

    };
    Utill.showModalView(screen, passProps);
  }


  /**
   * @description Send OTP SMS to existing Prepaid and Postpaid customer 
   * @param {Number} otpNumber
   * @memberof DtvMainViewContainer
   */
  sendOtpMessage = (otpNumber) => {
    console.log('xxx sendOtpMessage :: otpNumber', otpNumber);
    if (otpNumber == '' || otpNumber == undefined) {
      Utill.showAlert(this.state.locals.please_select_mobile_number);
      return;
    }
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_ALL,
      otp_msisdn: otpNumber
    };

    this.props.dtvSendOtpMessage(requestParams, this);
  };

  /**
   * @description Re Send OTP SMS to existing Prepaid and Postpaid customer 
   * @param {Number} otpNumber
   * @param {Number} refId
   * @memberof DtvMainViewContainer
   */
  reSendOtpMessage = (otpNumber, refId) => {
    console.log('xxx checkCustomerOutstanding :: otpNumber, refId', otpNumber, refId);
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_DTV,
      otp_msisdn: otpNumber,
      ref_id: refId,
      reference_id: refId
    };

    this.props.dtvSendOtpMessage(requestParams, this);
  };

  otpResendSuccessfulMessage = (message) => {
    console.log(message);
    console.log('xxx otpResendSuccessfulMessage');
    Utill.showAlertMsg(message);
  }

  /**
   * @description Validate OTP SMS from backend
   * @param {Number} otpNumber
   * @param {Number} otpPin
   * @param {Number} refId
   * @memberof DtvMainViewContainer
   */
  verifyOtp = (otpNumber, otpPin, refId) => {
    console.log('xxx verifyOtp');
    let me = this;
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_DTV,
      otp_msisdn: otpNumber,
      otp_pin: otpPin,
      ref_id: refId,
      reference_id: refId

    };

    this.props.dtvVerifyOtp(requestParams, me);
  };

  /**
   * @description Check Existing postpaid customer Outstanding amount and Show Outstanding confirm modal
   * @param {Object} customerData
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  showOutstandingConfirmModal = (customerData, response) => {
    console.log('xxx showOutstandingConfirmModal :: customerData, response', customerData, response);
    let screen = {
      title: 'CustomerOutstandingModal',
      id: 'DialogRetailerApp.modals.CustomerOutstandingModal',
    };
    let passProps = {
      modalTitle: 'OutstandingModalDtv',
      outstandingData: response,
      customerData: customerData,
      outstandingConfirmContinue: this.outstandingConfirmContinue,
      outstandingConfirmCancel: this.outstandingConfirmCancel
    };
    Utill.showModalView(screen, passProps);
  }


  /**
   * @description Click Customer Outstanding continue
   * @param {Object} outstandingData
   * @param {Object} customerData
   * @memberof DtvMainViewContainer
   */
  outstandingConfirmContinue = (outstandingData, customerData) => {
    console.log('xxx outstandingConfirmContinue :: outstandingData, customerData :', outstandingData, customerData);
    this.modalDismiss();
    if (customerData.cxExistStatus == Constants.CX_EXIST_YES
      && customerData.documnetExistInDataScan == Constants.DOCUMENT_EXIST_YES
      && customerData.notificationNumberList.length > 0) {
      console.log('POSTPAID_AND_PREPAID_OTP_FLOW');
      this.showOtpNumberSelectionModal(customerData);
    } else {
      console.log('POSTPAID_PREPAID_NEW_CUSTOMER_NORMAL_FLOW');
    }
  };

  /**
   * @description Click Customer Outstanding no
   * @param {Object} response
   * param {Object} customerData
   * @memberof DtvMainViewContainer
   */
  outstandingConfirmCancel = () => {
    console.log('xxx outstandingConfirmCancel :: response, customerData :');
    Utill.showAlert('Do you want to cancel the operation');
  };

  /**
   * @description Existing postpaid customer without Outstanding amount
   * @param {Object} customerData
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  customerWithoutOutstanding = (customerData, response) => {
    console.log('xxx customerWithoutOutstanding :: customerData, response :', response, customerData);
    if (customerData.cxExistStatus == Constants.CX_EXIST_YES
      && customerData.documnetExistInDataScan == Constants.DOCUMENT_EXIST_YES
      && customerData.notificationNumberList.length > 0) {
      console.log('POSTPAID_OTP_FLOW');
      this.showOtpNumberSelectionModal(customerData);
    } else {
      console.log('POSTPAID_NEW_CUSTOMER_NORMAL_FLOW');
    }
  };

  /**
   * @description Check existing postpaid customer Outstanding amount
   * @param {Object} customerData
   * @memberof DtvMainViewContainer
   */
  checkCustomerOutstanding = (customerData) => {
    console.log('xxx checkCustomerOutstanding :: response', customerData);
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_DTV,
      cx_identity_no: customerData.id_number,
      cx_identity_type: customerData.id_type,
      identification_no: customerData.id_number,
      identification_type: customerData.id_type
    };

    this.props.dtvCustomerOutstandingCheck(requestParams, customerData, this);
  };

  /**
   * @description Voucher code validation
   * @memberof DtvMainViewContainer
   */
  onPressVoucherCodeValidate = () => {
    this.props.commonResetKycSignature();
    console.log('onPressVoucherCodeValidate');
    console.log("Voucher Code: ", this.props.enteredDTVVoucherCode);
    console.log("ID No: ", this.props.idNumber_input_value);
    let requestParams = {
      "conn_type": this.props.selected_connection_type,
      "lob": Constants.LOB_DTV,
      "cx_identity_no": this.props.idNumber_input_value,
      "voucher_code": this.props.enteredDTVVoucherCode
    };
    console.log("requestParams: ", requestParams);
    if (this.props.voucher_code_enabled && this.props.isValidDTVVoucherCode && this.props.validatedVoucherCode !== '') {
      if (this.props.cancelVoucherCodeReservation) {
        console.log("Cancelling old voucher code");
        this.props.cancelVoucherCodeReservation(this.props.validatedVoucherCode, () => {
          this.props.checkDTVVoucherCodeValidity(requestParams, this);
        });
      }
    } else {
      this.props.checkDTVVoucherCodeValidity(requestParams, this);
    }
  }

  /**
   * @description Call the backend API Open additional chanel selection screen
   * @memberof DtvMainViewContainer
   */
  loadAdditionalChannels = () => {
    this.props.commonResetKycSignature();
    console.log('#### loadAdditionalChannels');
    let _this = this;
    let successCb = (response) => _this.showAdditionalChannelsView(response);
    let requestParams = {
      lob: Constants.LOB_DTV,
      conn_type: _this.props.selected_connection_type,
      package_code: _this.props.selectedDTVDataPackageDetails.package_code,
      txn_reference: this.props.txn_reference,
    };

    if (FuncUtils.getArraySize(FuncUtils.getValueSafe(this.props.additional_channel_list_data).channel_list) == 0) {
      console.log('#### loadAdditionalChannels LOADING FROM API');
      _this.props.clearChannelAndPacksData();
      _this.props.getDtvAdditionalChannels(requestParams, successCb, _this);
    } else {
      console.log('#### loadAdditionalChannels LOADING FROM REDUX');
      _this.showAdditionalChannelsView(_this.props.additional_channel_list_data);
    }

  }

  /**
   * @description Call the backend API for packageslist
   * @param {Object} customerData
   * @memberof DtvMainViewContainer
   */
  checkPackageList = (customerData) => {
    console.log('#### checkPackageList', customerData);
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      cx_identity_no: this.props.idNumber_input_value,
      cx_identity_type: this.props.customerType,
      txn_reference: this.props.txn_reference,
      // package_code : "POWERPLAY"
    };

    console.log(requestParams);
    this.props.getDtvDataPackages(requestParams, customerData, this);
  };

  /**
   * @description Show additional chanel selection screen
   * @memberof DtvMainViewContainer
   */
  showAdditionalChannelsView = (response) => {
    console.log('#### showAdditionalChannelsView', response);
    let _this = this;
    let screen = {
      title: 'DTV Activation',
      id: 'DialogRetailerApp.views.DtvAdditionalChannelsScreen',
    };

    let passProps = {
      screenTitle: 'DTV Activation',
      backMessage: _this.state.locals.do_uou_want_to_cancel_image_capture_and_go_back,
      lob: Constants.LOB_DTV,
    };

    Screen.pushView(screen, _this, passProps);
  }

  /**
   * @description Open image capture camera module
   * @param {Number} captureType
   * @memberof DtvMainViewContainer
   */
  onPressImageCapture = (captureType) => {
    console.log('#### onPressImageCapture : captureType => ', captureType);
    let me = this;
    let screen = {
      title: 'Image Capture Module',
      id: 'DialogRetailerApp.views.ImageCaptureModule',
    };

    let passProps = {
      screenTitle: 'Image Capture Module',
      backMessage: me.state.locals.do_uou_want_to_cancel_image_capture_and_go_back,
      imageCaptureTypeNo: captureType,
      lob: Constants.LOB_DTV,
      reCapture: false
    };

    Analytics.logFirebaseEvent('action_progress', {
      action_name: 'capture_lyc_images',
      action_type: 'capture_images',
      additional_details: 'DTV KYC image capture  - captureType :' + captureType,
    });

    Screen.pushView(screen, me, passProps);
  }

  /**
   * @description Common method to hide individual items in both Prepaid and Postpaid activation
   * @param {String} refName
   * @memberof DtvMainViewContainer
   */
  getIndividualItemHideStatus = (refName) => {
    let _this = this;
    console.log('#### getIndividualItemHideStatus : refName => ', refName);
    console.log('#### getIndividualItemHideStatus : CONNECTION_TYPE ::' + this.props.selected_connection_type);
    if (this.props.selected_connection_type === Constants.PREPAID) {
      switch (refName) {
        case 'idNumber':
          console.log('## idNumber');
          return false;
        case 'connectionNo2':
          console.log('# connectionNo');
          return false;
        case 'fullfillmentType':
          console.log('# fullfillmentType');
          return false;
        case 'stbPackSerial':
          console.log('# stbPackSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == "CARRY" && (this.props.dtv_pack_type.packageCode == 'HP' || this.props.dtv_pack_type.packageCode == 'FP'));
        case 'accessoriesPackSerial':
          console.log('# accessoriesPackSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == "CARRY" && this.props.dtv_pack_type.packageCode == 'FP');
        case 'bundleSerial':
          console.log('# bundleSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_CARRY && this.props.dtv_pack_type.packageCode == 'FPB');
        case 'checkBox_stb_upgrade':
          console.log('# checkBox_stb_upgrade');
          return !_this.props.stb_data.stb_upgrade_available;
        case 'voucherCode':
          console.log('# voucherCode');
          return !this.props.voucher_code_enabled;
        case 'warrantyExtension':
          console.log('# warrantyExtension');
          return false;
        case 'firstReload':
          console.log('# firstReload');
          return !(this.props.enable_reload && this.props.getConfiguration.rapid_ez_account.rapid_ez_availability);
        case 'oafSerial':
          console.log('# oafSerial');
          return (this.props.dtv_fullfillment_type.package_code !== Constants.CASH_AND_DELIVERY || this.props.eoafSerial.material_serial_avaialable == false);
        case 'totalPayment':
          console.log('# totalPayment');
          return !this.props.sectionId_validation_status;
        case 'dealerCollection':
          console.log('# dealerCollection');
          return (!((this.props.sectionId_validation_status) ? this.props.enable_dealer_collection : false));
        case 'ezCashPin':
          console.log('# ezCashPin');
          return !(!_.isNull(this.props.common.kyc_signature) && this.shouldChargeEzCash());
        case 'ezCashBalance':
          console.log('# ezCashBalance');
          return !(!_.isNull(this.props.common.kyc_signature) && this.shouldChargeEzCash() && this.props.didEzCashAccountValidated);
        default:
          console.log('# default :: ' + refName);
          return false;
      }
    } else {

      switch (refName) {
        case 'idNumber':
          console.log('## idNumber');
          return false;
        case 'connectionNo':
          console.log('# connectionNo');
          return false;
        case 'fullfillmentType':
          console.log('# fullfillmentType');
          return false;
        case 'stbPackSerial':
          console.log('# stbPackSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == "CARRY" && (this.props.dtv_pack_type.packageCode == 'HP' || this.props.dtv_pack_type.packageCode == 'FP'));
        case 'accessoriesPackSerial':
          console.log('# accessoriesPackSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == "CARRY" && this.props.dtv_pack_type.packageCode == 'FP');
        case 'bundleSerial':
          console.log('# bundleSerial');
          return !(this.props.dtv_fullfillment_type.package_code != undefined && this.props.dtv_pack_type.packageCode != undefined && this.props.dtv_fullfillment_type.package_code == "CARRY" && this.props.dtv_pack_type.packageCode == 'FPB');
        case 'checkBox_stb_upgrade':
          console.log('# checkBox_stb_upgrade');
          return !_this.props.stb_data.stb_upgrade_available;
        case 'voucherCode':
          console.log('# voucherCode');
          return !this.props.voucher_code_enabled;
        case 'warrantyExtension':
          console.log('# warrantyExtension');
          return false;
        case 'firstReload':
          console.log('# firstReload');
          return true;
        case 'oafSerial':
          console.log('# oafSerial');
          return (this.props.dtv_fullfillment_type.package_code !== Constants.CASH_AND_DELIVERY || this.props.eoafSerial.material_serial_avaialable == false);
        case 'totalPayment':
          console.log('# totalPayment');
          return !this.props.sectionId_validation_status;
        case 'dealerCollection':
          console.log('# dealerCollection');
          return (!((this.props.sectionId_validation_status) ? this.props.enable_dealer_collection : false));
        case 'ezCashPin':
          console.log('# ezCashPin');
          return !(!_.isNull(this.props.common.kyc_signature) && this.shouldChargeEzCash());
        case 'ezCashBalance':
          console.log('# ezCashBalance');
          return !(!_.isNull(this.props.common.kyc_signature) && this.shouldChargeEzCash() && this.props.didEzCashAccountValidated);
        default:
          console.log('# default :: ' + refName);
          return false;
      }
    }
  }

  shouldChargeEzCash = () => {
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps ', this.props.data_dealerTotalPaymentProps);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - dealerCollectionPayment ', this.props.data_dealerTotalPaymentProps.dealerCollectionPayment);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isNil ', _.isNil(this.props.data_dealerTotalPaymentProps.dealerCollectionPayment));
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isEmpty ', this.props.data_dealerTotalPaymentProps.dealerCollectionPayment == '');
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isEqual ', parseInt(this.props.data_dealerTotalPaymentProps.dealerCollectionPayment) == 0);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - totalOsAmount ', Utill.checkNumber(this.props.totalOsAmount));
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - totalOsAmount ', Utill.checkNumber(this.props.totalOsAmount) !== 0);

    return (!_.isNil(this.props.data_dealerTotalPaymentProps.dealerCollectionPayment)
      && this.props.data_dealerTotalPaymentProps.dealerCollectionPayment != ''
      && parseInt(this.props.data_dealerTotalPaymentProps.dealerCollectionPayment) != 0 || Utill.checkNumber(this.props.totalOsAmount) !== 0 );

  }

  /**
   * @description Common method to clear redux values
   * @param {String} clearLevel
   * @memberof DtvMainViewContainer
   */
  clearReduxValues = async (clearLevel) => {
    console.log('#### clearReduxValues :: clearLevel :: ### ', clearLevel);
    switch (clearLevel) {
      case 'idValidation':
        await this.props.dtvResetSectionValidationStatus();
        break;
      case 'serialValidation':
        break;
      default:
        //Clear all data related to DTV
        await this.props.resetDTVData(true);
        break;
    }
  }

  getCustomerIdTypeText = () => {
    console.log('#### getCustomerIdTypeText');
    return this.props.customerIdType == Constants.CX_ID_TYPE_NIC ?
      this.state.locals.id_nic_mc : this.state.locals.id_passport_mc;
  }

  getTermAndConditionUrl = () => {
    const { tcUrls } = this.props;
    let returnUrl;
    if (this.props.selected_connection_type == Constants.PREPAID) {
      returnUrl = tcUrls.PREPAID;
    } else {
      returnUrl = tcUrls.POSTPAID;
    }
    return returnUrl;
  }

  signaturePressValidation = () => {
    console.log('## signaturePressValidation');
    let me = this;
    //Check image capture
    if (me.props.customerType === Constants.CX_TYPE_LOCAL
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {
      if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_NIC
        && (me.props.common.kyc_nic_front === null
          || me.props.common.kyc_nic_back === null)) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> NIC');
        return {
          status: false,
          reason: this.state.locals.please_capture_nic
        };
      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_PASSPORT) {
        if (me.props.common.kyc_passport === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - PASSPORT');
          return {
            status: false,
            reason: this.state.locals.please_capture_passport
          };
        }
        if (me.props.common.kyc_proof_of_billing === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - POB');
          return {
            status: false,
            reason: this.state.locals.please_capture_pob
          };
        }

      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_DRIVING_LICENCE
        && me.props.common.kyc_driving_licence === null) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> DRIVING_LICENCE');
        return {
          status: false,
          reason: this.state.locals.please_capture_driving_licence
        };
      }
    } else if (me.props.customerType === Constants.CX_TYPE_FOREIGNER
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {

      if (me.props.common.kyc_passport === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PASSPORT');
        return {
          status: false,
          reason: this.state.locals.please_capture_passport
        };
      }

      if (me.props.common.kyc_proof_of_billing === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PROOF_OF_BILLING');
        return {
          status: false,
          reason: this.state.locals.please_capture_pob
        };
      }
    }

    //Check optional KYC images
    if (me.props.opt_addressDifferent && me.props.common.kyc_proof_of_billing === null) {
      console.log('ANY >> CHECK_BOX >> PROOF_OF_BILLING');
      return {
        status: false,
        reason: this.state.locals.please_capture_pob
      };
    }

    if (me.props.opt_customerFaceNotClear && me.props.common.kyc_customer_image === null) {
      console.log('ANY >> CHECK_BOX >> CUSTOMER_IMAGE');
      return {
        status: false,
        reason: this.state.locals.please_enter_customer_face
      };
    }

    if (me.props.opt_installationAddressDeferent && me.props.common.kyc_additional_pob === null) {
      console.log('ANY >> CHECK_BOX >> ADDITIONAL_POB');
      return {
        status: false,
        reason: this.state.locals.please_enter_poi
      };
    }

    //Same day installation 
    if (me.props.opt_sameDayInstallation) {
      if (me.props.customer_name.value == "" || me.props.customer_name.error !== '') {
        return {
          status: false,
          reason: strings_dtv.customerName
        };
      }

      if (me.props.customer_address_line1.value == "" || me.props.customer_address_line1.error !== '') {
        return {
          status: false,
          reason: strings_dtv.customerAddressLineOne
        };
      }

      if (me.props.customer_address_line2.value == "" || me.props.customer_address_line2.error !== '') {
        return {
          status: false,
          reason: strings_dtv.customerAddressLineTwo
        };
      }

      if (me.props.customer_city.code == "") {
        return {
          status: false,
          reason: strings_dtv.customerCity
        };
      }
    }

    //Customer mobile number
    if (!Utill.mobileNumberValidateAny(me.props.dtvMobileNumber)) {
      return {
        status: false,
        reason: me.state.locals.please_enter_valid_mobile
      };
    }

    //Check customer landline number
    if (me.props.customerLandline !== '' && !Utill.landlineNumberValidation(me.props.customerLandline)) {
      return {
        status: false,
        reason: this.state.locals.please_enter_valid_laneline
      };
    }

    //Check customer email
    if (me.props.email_input_value !== '' && !Utill.emailValidation(me.props.email_input_value)) {
      return {
        status: false,
        reason: this.state.locals.please_enter_valid_email
      };
    }
    

    return {
      status: true,
      reason: ''
    };

  }

  onCaptureSignature = (totalPaymentProps, eZCashTotalPaymentProps) => {
    console.log('## onCaptureSignature');
    if (!this.signaturePressValidation().status) {
      console.log('onCaptureSignature :: VALIDATION ERROR');
      Utill.showAlert(this.signaturePressValidation().reason);
    } else {
      console.log('onCaptureSignature :: VALIDATION SUCCESS', totalPaymentProps, eZCashTotalPaymentProps);
      const navigatorOb = this.props.navigator;
      this.onConfirmModal(navigatorOb);
    }
  }

  /**
   * This will be invoked when user type in a voucher code
   *
   * @memberof DtvMainViewContainer
   */
  onVoucherCodeTextChange = async (text) => {
    console.log('xxx onDTVVoucherCodeTextChange', text);
    let me = this;

    if (!Utill.alphaNumericValidation(text) && text !== '') {
      console.log("Invalid Voucher Format");
      this.setState({ voucherCodeError: this.state.locals.please_enter_valid_voucher_code });
    } else {
      this.setState({ voucherCodeError: '' });
    }
    if (text === "") {
      this.setState({ voucherCodeError: '' });
    }
    await me.props.setDTVVoucherCodeText(text);

  };

  
  /**
   * @description Final activation API call implementation - TODO
   * @memberof DtvMainViewContainer
   */
  onPressActivate = () => {
    console.log('##### onPressActivate #####');
    let me = this;
    let activity_start_time = me.props.common_activity_start_time;
    let activity_end_time = Math.round((new Date()).getTime() / 1000);
    //me.props.commonSetActivityEndTime();
    console.log('activity_start_time', activity_start_time);
    console.log('activity_end_time', activity_end_time);
    console.log('data_totalPaymentProps', me.props.data_totalPaymentProps);

    if (!me.getActivateButtonStatus().status) {
      console.log('##### onPressActivate :: DISPLAY_ALERT');
      Utill.showAlert(me.getActivateButtonStatus().reason);
      return;
    }

    let additional_channels = [];

    _.forEach(this.props.selected_channel_n_packs.channels, (value) => {
      additional_channels.push({
        "code": value.pack_code,
        "name": value.name,
        "rental": value.rental
      });
    });

    _.forEach(this.props.selected_channel_n_packs.packs, (value) => {
      additional_channels.push({
        "code": value.pack_code,
        "name": value.pack_name,
        "rental": value.pack_rental
      });
    });

    let requestParams = {
      start_ts: activity_start_time,
      end_ts: activity_end_time,
      conn_type: this.props.selected_connection_type,
      lob: Constants.LOB_DTV,
      cx_type: this.props.customerType,
      cx_identity_type: this.props.customerInfoSectionIdType,
      cx_identity_no: this.props.idNumber_input_value,
      otp_verified_status: this.props.customerOtpStatus,
      otp_verified_number: this.props.cx_otp_number,
      id_not_clear: this.props.opt_customerFaceNotClear,
      address_different: this.props.opt_addressDifferent,
      install_address_different: this.props.opt_installationAddressDeferent,
      offer_code: this.props.availableDtvOffers.offer_code,
      same_day_installation: this.props.opt_sameDayInstallation,
      os_amount: this.props.totalOsAmount,
      adjustment_amount: this.props.data_dealerTotalPaymentProps.unitPriceAdjustment,
      same_day_installation_fee: this.props.data_totalPaymentProps.sameDayInstallationCharge,
      delivery_and_installation_fee: this.props.data_totalPaymentProps.deliveryInstallationCharge,
      extended_warranty_fee: this.props.data_totalPaymentProps.warrentyExtCharge,
      connection_os_details: this.props.customerOutstandingData,
      ezcash_pin: this.props.ez_cash_input_pin,
      oaf_serial: this.props.material_serial,
      service_type: this.props.dtv_fullfillment_type.package_code,
      voucher_code: this.props.validatedVoucherCode,
      reference_no: this.props.reference_no,
      image_type_list: this.getImageTypeList(),
      alt_contact_number: this.props.dtvMobileNumber,
      contact_number_land: this.props.customerLandline,
      contact_email: this.props.email_input_value,
      pack_type: this.props.dtv_pack_type.packageCode,
      package_code: this.props.availableDTVDataPackagesList.data_package_code,
      reload_amount: this.props.reloadAmountValue,
      om_campaign_record_no: this.props.stb_data.multiplay_reservation.om_campaign_record_no,
      reservation_no: this.props.stb_data.multiplay_reservation.reservation_no,
      total_amount: this.props.data_totalPaymentProps.customerTotalPayment,
      connection_fee: this.props.data_totalPaymentProps.deliveryInstallationCharge,
      device_selling_price: this.props.data_dealerTotalPaymentProps.unitPriceAfterVoucerValue,
      warranty_pack_code: this.props.dtv_warranty_extensions.packCode,
      // additional_channels: additional_channels,
      product_items: [],
      stb_upgrade: this.props.stb_upgrade_checked,
      additional_rental: this.props.selected_channel_total_package_rental.additionalMonthlyRental,
      package_name: this.props.selectedDTVDataPackageDetails.package_name,
      package_rental: this.props.selectedDTVDataPackageDetails.package_rental
    };

    if (this.props.enable_sameDayInstallation) {
      requestParams["address_details"] = {
        customer_name: this.props.customer_name.value,
        address_line1: this.props.customer_address_line1.value,
        address_line2: this.props.customer_address_line2.value,
        address_line3: this.props.customer_address_line3.value,
        city_name: this.props.customer_city.city_name,
        postal_code: this.props.customer_city.code
      };

      requestParams["accessory_serial"] = this.props.accessoriesSerialValue;

    }

    if (this.props.dtv_fullfillment_type.package_code === Constants.CASH_AND_CARRY) {
      const { accessoriesSerialData = null, serialPackData = null, bundleSerialData = null } = this.props;
      let product_items = [];
      console.log('validatedAccessoryData', accessoriesSerialData);
      console.log('validatedbundleSerialData', bundleSerialData);

      if (accessoriesSerialData !== null)
        product_items.push(accessoriesSerialData);

      if (serialPackData !== null)
        product_items.push(serialPackData);

      if (bundleSerialData !== null)
        product_items.push(bundleSerialData);

      requestParams.product_items = product_items;
    }

    if (this.props.dtv_fullfillment_type.package_code === Constants.CASH_AND_DELIVERY) {

      requestParams["delivery_charge"] = Utill.checkNumber(this.props.dtvDeliveryCharge.delivery_charge);
      requestParams["delivery_charge_wotax"] = Utill.checkNumber(this.props.dtvDeliveryCharge.delivery_charge_wotax);
    }

    if (this.props.bundleSerialValue) {
      requestParams["stb_serial"] = this.props.bundleSerialValue;
    } else {
      requestParams["stb_serial"] = this.props.serialPackValue;
    }
    

    if (!_.isNil(this.props.availableDtvOffers) && this.props.voucher_code_enabled == true) {
      requestParams["voucher_value"] = this.props.voucherValue;
      requestParams["voucher_category"] = this.props.voucherCategory;
      requestParams["voucher_sub_category"] = this.props.voucherSubCategory;
    }

    const { selected_channel_n_packs } = this.props;

    if (selected_channel_n_packs.channels.length > 0 ||
      selected_channel_n_packs.packs.length > 0) {
      requestParams["additional_channels"] = additional_channels;
      // requestParams["additional_packs"] = additional_packs;
    }
    console.log('## onPressActivateRequestParams : ', JSON.stringify(requestParams));
    me.props.dtvFinalActivationApi(requestParams, me);
  };


  getImageTypeList = () => {
    console.log('xxxx getImageTypeList');
    let imageTypeList = [];
    this.props.common.kyc_nic_front !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT) : true;
    this.props.common.kyc_nic_back !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.NIC_BACK) : true;

    this.props.common.kyc_driving_licence !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE) : true;

    this.props.common.kyc_passport !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.PASSPORT) : true;
    this.props.common.kyc_proof_of_billing !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING) : true;

    this.props.common.kyc_customer_image !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE) : true;

    this.props.common.kyc_signature !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.SIGNATURE) : true;

    this.props.common.kyc_additional_pob !== null ?
      imageTypeList.push(Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB) : true;

    console.log('xxxx imageTypeList', imageTypeList);
    return imageTypeList;
  };

  /**
   * @description Add to image upload SQLite db
   * @param {Number} txnId
   * @param {Number} imageId
   * @param {String} imageOb
   * @memberof DtvMainViewContainer
   */
  addToImageUploadQueue = async (txnId, imageId, imageOb) => {
    console.log('xxx DTV :: addToImageUploadQueue :: txnId, imageId, imageUri : ', txnId, imageId, imageOb);
    let deviceData = await Utill.getDeviceAndUserData();
    try {
      const id = `${txnId}_${imageId}`;
      const url = Utill.createApiUrl('imageUploadEnc', 'initAct', 'ccapp');
      const imageFile = imageOb.imageUri.replace('file://', '');
      const uploadSelector = 'encrypt';

      const options = {
        id: id,
        url: url,
        imageFile: imageFile,
        uploadSelector: uploadSelector,
        deviceData: JSON.stringify(deviceData)
      };

      console.log('xxx DTV :: addToImageUploadQueue :: options : ', options);

      const successCb = (msg) => {
        console.log('xxxx addToImageUploadQueue successCb');
        Analytics.logEvent('dsa_dtv_activation_sqlite_add_success');
        console.log(msg);
      };

      const errorCb = (msg) => {
        console.log('xxxx addToImageUploadQueue errorCb ');
        Analytics.logEvent('dsa_dtv_activation_sqlite_add_error');
        console.log(msg);
      };

      RNReactNativeImageuploader.addToDb(options, errorCb, successCb);

    } catch (e) {
      console.log('addToImageUploadQueue Image Uploading Failed :', e.message);
      Analytics.logEvent('dsa_dtv_activation_sqlite_exception');
      this.showAlert(e.message);
    }
  }

  /**
   * @description Show notices alert
   * @param {Object} message
   * @memberof DtvMainViewContainer
   */

  showNoticesAlert = (description) => {
    console.log("showNoticesAlert: ", description);
    let passProps = {
      secondaryPress: this.onEoafSerialModalCancelPress,
      primaryPress: () => this.onEoafSerialModalContinuePress(description),
      primaryText: this.state.locals.btnContinue,
      secondaryText: this.state.locals.btnCancel,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.NoticeAlert,
      descriptionTitle: this.state.locals.sameDayInstallationTitle,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorYellow },
      secondaryButtonStyle: { color: Colors.colorBlack },
      eoafSerialTitle: this.state.locals.eoafSerialModalName,
      eoafSerialvalue: description.info.oaf_serial,
      eoafSerialTitleTextStyle: { alignSelf: 'flex-start', fontSize: 14 },
      description: description.message,
      dealerCollectionPaymentProps: this.props.data_dealerTotalPaymentProps,
      locals: {
        unitPriceAdjustment: strings_dtv.unitPriceAdjustment,
        rs: strings_dtv.rs,
        purchased_price: strings_dtv.purchased_price,
        selling_price: strings_dtv.selling_price

      },

    };

    // Screen.showGeneralModal(passProps);
    let screen = {
      title: 'EoafConfirmModal',
      id: 'DialogRetailerApp.modals.EoafConfirmModal',
    };
    Utill.showModalView(screen, passProps);
  };


  /**
   * @description Show notices alert
   * @param {Object} message
   * @memberof DtvMainViewContainer
   */

  showModalError = (description) => {
    let passProps = {
      primaryText: this.state.locals.btnContinue,
      secondaryText: this.state.locals.btnCancel,
      disabled: false,
      hideTopImageView: false,
      icon: Images.icons.NoticeAlert,
      descriptionTitle: this.state.locals.sameDayInstallationTitle,
      descriptionTitleTextStyle: { alignSelf: 'flex-start' },
      primaryButtonStyle: { color: Colors.colorYellow },
      secondaryButtonStyle: { color: Colors.colorBlack },
      description: description,
      secondaryPress: this.onCancelPress,
      primaryPress: this.onContinuePress
    };

    Screen.showGeneralModal(passProps);

  };

  //this functions will be trigger doing activation with already purchased oeaf serial number
  onEoafSerialModalCancelPress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });

  }

  onEoafSerialModalContinuePress = (description) => {

    const eoafSerialvalue = description.info.oaf_serial;
    let eoafSerial = {
      material_serial_avaialable: true,
      material_serial: eoafSerialvalue,

    };
    this.props.dtvSeteOAFserialValue(eoafSerial);

    this.timeoutCheck = setTimeout(() => {
      const navigatorOb = this.props.navigator;
      navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
      this.onPressActivate();

    }, 100);

  }

  //this function will be occur when Same day installation error view Cancel button pressed
  onCancelPress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    navigatorOb.resetTo({ title: 'DIALOG SALES APP', screen: 'DialogRetailerApp.views.HomeTileScreen' });

  }

  onContinuePress = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
    this.props.setSameDayInstallationData(null);
    this.props.resetSameDayInstallationStatus();
  }

  /**
   * @description Get activate button status
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  getActivateButtonStatus = () => {
    console.log('xxx getActivateButtonStatus');
    let me = this;
    //Check image capture
    if (me.props.customerType === Constants.CX_TYPE_LOCAL
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {
      if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_NIC
        && (me.props.common.kyc_nic_front === null
          || me.props.common.kyc_nic_back === null)) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> NIC');
        return {
          status: false,
          reason: this.state.locals.please_capture_nic
        };
      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_PASSPORT) {
        if (me.props.common.kyc_passport === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - PASSPORT');
          return {
            status: false,
            reason: this.state.locals.please_capture_passport
          };
        }
        if (me.props.common.kyc_proof_of_billing === null) {
          console.log('OTP_NOT_VERIFIED >> LOCAL >> PASSPORT - POB');
          return {
            status: false,
            reason: this.state.locals.please_capture_pob
          };
        }

      } else if (me.props.customerInfoSectionIdType === Constants.CX_INFO_TYPE_DRIVING_LICENCE
        && me.props.common.kyc_driving_licence === null) {
        console.log('OTP_NOT_VERIFIED >> LOCAL >> DRIVING_LICENCE');
        return {
          status: false,
          reason: this.state.locals.please_capture_driving_licence
        };
      }
    } else if (me.props.customerType === Constants.CX_TYPE_FOREIGNER
      && (me.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
        || me.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {

      if (me.props.common.kyc_passport === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PASSPORT');
        return {
          status: false,
          reason: this.state.locals.please_capture_passport
        };
      }

      if (me.props.common.kyc_proof_of_billing === null) {
        console.log('OTP_NOT_VERIFIED >> FOREIGNER >> PROOF_OF_BILLING');
        return {
          status: false,
          reason: this.state.locals.please_capture_pob
        };
      }
    }

    //Check optional images
    if (me.props.opt_addressDifferent && me.props.common.kyc_proof_of_billing === null) {
      console.log('ANY >> CHECK_BOX >> PROOF_OF_BILLING');
      return {
        status: false,
        reason: this.state.locals.please_capture_pob
      };
    }

    if (me.props.opt_customerFaceNotClear && me.props.common.kyc_customer_image === null) {
      console.log('ANY >> CHECK_BOX >> CUSTOMER_IMAGE');
      return {
        status: false,
        reason: this.state.locals.please_enter_customer_face
      };
    }

    if (me.props.opt_installationAddressDeferent && me.props.common.kyc_additional_pob === null) {
      console.log('ANY >> CHECK_BOX >> ADDITIONAL_POB');
      return {
        status: false,
        reason: this.state.locals.please_enter_poi
      };
    }

    //Same day installation 
    if (me.props.opt_sameDayInstallation) {
      if (me.props.customer_name.value == "" || me.props.customer_name.error !=='') {
        return {
          status: false,
          reason: strings_dtv.customerName
        };
      }
    
      if (me.props.customer_address_line1.value == "" || me.props.customer_address_line1.error !=='') {
        return {
          status: false,
          reason: strings_dtv.customerAddressLineOne
        };
      }
    
      if (me.props.customer_address_line2.value == "" || me.props.customer_address_line2.error !=='') {
        return {
          status: false,
          reason: strings_dtv.customerAddressLineTwo
        };
      }
    
      if (me.props.customer_city.code == "") {
        return {
          status: false,
          reason: strings_dtv.customerCity
        };
      }
    }

    //Check customer mobile number
    if (!Utill.mobileNumberValidateAny(me.props.dtvMobileNumber)) {
      console.log('ANY >> CUSTOMER_MOBILE');
      return {
        status: false,
        reason: this.state.locals.please_enter_valid_mobile
      };
    }

    //Check customer landline number
    if (me.props.customerLandline !== '' && !Utill.landlineNumberValidation(me.props.customerLandline)) {
      console.log('ANY >> CUSTOMER_LAND_LINE');
      return {
        status: false,
        reason: this.state.locals.please_enter_valid_laneline
      };
    }

    //Check customer email
    if (me.props.email_input_value !== '' && !Utill.emailValidation(me.props.email_input_value)) {
      console.log('ANY >> CUSTOMER_EMAIL');
      return {
        status: false,
        reason: this.state.locals.please_enter_valid_email
      };
    }

    //Check signature image
    if (me.props.common.kyc_signature === null) {
      console.log('ANY >> SIGNATURE');
      return {
        status: false,
        reason: this.state.locals.please_enter_signature
      };
    }

    //Check eZ cash PIN
    if (this.shouldChargeEzCash() && (me.props.ez_cash_input_pin === '' || me.props.ez_cash_input_pin.length !== 4)) {
      console.log('ANY >> EZ_CASH_PIN');
      return {
        status: false,
        reason: this.state.locals.please_enter_ez_cash_pin
      };
    }

    return {
      status: true,
      reason: ''
    };
  }

  onChangeTextCustomerMobile = (text) => {
    this.props.commonResetKycSignature();
    console.log('xxx onChangeTextCustomerMobile :', text);
    this.setState({ customerMobile: text });
    this.props.dtvSetInputMobileNumber(text);
    if (text.length >= 3) {
      if (Utill.mobileNumberValidateAny(text)) {
        this.setState({ mobileError: '' });
      } else {
        this.setState({ mobileError: this.state.locals.please_enter_valid_mobile });
      }
    } else if (text.length < 3) {

      this.setState({ mobileError: '' });
    }
  }

  /**
   * @description Add all images to image upload thread (SQLite Db)
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  addToImageUploadProcess = (response) => {
    console.log('xxx addToImageUploadProcess :: response', response);
    let me = this;
    let transactionId = response.transaction_id;

    if (me.props.common.kyc_nic_front !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_FRONT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT,
        me.props.common.kyc_nic_front);
    }

    if (me.props.common.kyc_nic_back !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => NIC_BACK');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.NIC_BACK,
        me.props.common.kyc_nic_back);
    }

    if (me.props.common.kyc_driving_licence !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => DRIVING_LICENCE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE,
        me.props.common.kyc_driving_licence);
    }

    if (me.props.common.kyc_passport !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PASSPORT');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PASSPORT,
        me.props.common.kyc_passport);
    }

    if (me.props.common.kyc_proof_of_billing !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => PROOF_OF_BILLING');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING,
        me.props.common.kyc_proof_of_billing);
    }

    if (me.props.common.kyc_customer_image !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => CUSTOMER_IMAGE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE,
        me.props.common.kyc_customer_image);
    }

    if (me.props.common.kyc_signature !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => SIGNATURE');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.SIGNATURE,
        me.props.common.kyc_signature);
    }

    if (me.props.common.kyc_additional_pob !== null) {
      console.log('ADD_TO_IMAGE_UPLOAD_QUEUE => ADDITIONAL_POB');
      me.addToImageUploadQueue(
        transactionId,
        Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB,
        me.props.common.kyc_additional_pob);
    }

  }

  /**
   * @description Show Activate Success Modal
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  showActivateSuccessModal = (response) => {
    console.log('xxx showActivateSuccessModal :: response', response);
    this.addToImageUploadProcess(response); //Add to image upload process
    let description = response.description;
    let description_title = response.description_title;

    let justifiedList = [
      {
        label: this.state.locals.transaction_id,
        value: response.order_id
      },
      {
        label: this.state.locals.cir_no,
        value: response.cir_no
      },
    ];

    let passProps = {
      descriptionTitle: description_title,
      description: description,
      primaryText: this.state.locals.btnOk,
      primaryPress: this.goToHomePage,
      primaryPressPayload: response,
      icon: Images.icons.SuccessAlert,
      descriptionBottomTextJustified: justifiedList,
      disabled: false,
      hideTopImageView: false,
      descriptionTextStyle: { fontWeight: "bold", color: Colors.colorBlack }
    };

    this.dtvGeneralModal(passProps);
  };

  /**
   * @description Show data offer selection modal
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  showOfferSelectionModal = (response) => {
    console.log('xxx showOfferSelectionModal :: response', response);
    let screen = {
      title: 'OfferSelectionModal',
      id: 'DialogRetailerApp.modals.OfferSelectionModal',
    };

    let passProps = {
      modalTitle: 'OfferSelectionModal',
      offerDataList: response.data,
      cancelVoucherCodeReservation: () => this.props.cancelVoucherCodeReservation()
    };
    if (response.data.length > 0) {
      Utill.showModalView(screen, passProps, false);
    } else {
      this.showGeneralErrorModal(response.message);
      console.log("xxx showOfferSelectionModal :: Empty offers array.");
    }
  }


  /**
   * @description Load Data Dtv Offers from backend and show offer selection modal
   * @memberof DtvMainViewContainer
   */
  loadDataDtvOffers = () => {
    console.log('xxx loadDataDtvOffers');
    let me = this;

    let fulfilment_type = null;

    if (this.props.availableDTVDataPackagesList.dropDownData.length == 1) {
      fulfilment_type = this.props.availableDTVDataPackagesList.selectedFullFillmentType[0].code;
    } else {
      fulfilment_type = this.props.dtv_fullfillment_type.package_code;
    }
    const requestParams = {
      fulfilment_type: fulfilment_type,
      package_code: this.props.availableDTVDataPackagesList.data_package_code,
      txn_reference: this.props.txn_reference,
      pack_type: this.props.dtv_pack_type.packageCode
    };
    this.props.dtvLoadOfferList(requestParams, me);
  };

  /**
   * @description Handle eZ Cash PIN input tet field change
   * @param {Number} ezCashPin
   * @memberof DtvMainViewContainer
   */
  onExCashPinTextChange = (ezCashPin) => {
    console.log('onExCashPinTextChange :', ezCashPin);
    let _this = this;
    _this.props.dtvSetEzCashInputPin(ezCashPin);
    if (Utill.validateNumberFormat(ezCashPin) && ezCashPin.length === 4) {
      console.log('onExCashPinTextChange :: VALID EZ_CASH_PIN ', ezCashPin);
      const requestParams = {
        conn_type: _this.props.selected_connection_type,
        lob: Constants.LOB_DTV,
        subscriber_pin: ezCashPin,
        device_price: !_.isNil(this.props.data_dealerTotalPaymentProps) ? this.props.data_dealerTotalPaymentProps.purchased_price : 0,
        payment_amount: !_.isNil(this.props.data_dealerTotalPaymentProps) ? this.props.data_dealerTotalPaymentProps.dealerCollectionPayment : 0
      };
      _this.props.dtvEzCashAccountCheck(requestParams, _this);
    } else {
      console.log('onExCashPinTextChange :: INVALID EZ_CASH_PIN ', ezCashPin);
      _this.props.dtvResetEzCashBalance();
    }
  }

  onPressEzCashCancel = () => {
    console.log('xxx onPressEzCashCancel');
    this.goToHomePage();
  }

  onPressOk = () => {
    console.log('xxx onPressEzCashReTry');
    this.props.dtvSetEzCashInputPin('');
    this.modalDismiss();
  }

  onChangeTextEmail = (text) => {
    this.props.commonResetKycSignature();
    console.log('xxx onChangeTextEmail', text);
    this.setState({ email_input_value: text });
    this.props.dtvSetInputemail(text);
    if (text.length >= 3) {

      if (Utill.emailValidation(text)) {
        this.setState({ emailError: '' });
      } else {
        this.setState({ emailError: this.state.locals.please_enter_valid_email });
      }
    } else if (text.length < 3) {
      this.setState({ emailError: '' });
    }
  }

  onChangeTextCustomerLandline = (text) => {
    this.props.commonResetKycSignature();
    console.log('xxx onChangeTextCustomerLandline :', text);
    this.setState({ customerLandline: text });
    this.props.dtvSetInputLandlineNumber(text);
    if (text.length >= 3) {
      if (Utill.landlineNumberValidation(text)) {
        this.setState({ landlineError: '' });
      } else {
        this.setState({ landlineError: this.state.locals.please_enter_valid_laneline });
      }
    } else if (text.length < 3) {
      this.setState({ landlineError: '' });
    }
  }

  /**
   * @description Check agent's eZ Cash account and show error message
   * @param {Object} response
   * @memberof DtvMainViewContainer
   */
  showEzCashErrorModal = (response) => {
    console.log('xxx showEzCashErrorModal ::  response', response);
    let description = response.error;
    let description_title = response.title;
    let passProps;

    if (response.ez_cash_error_type == 'BLOCKED') {
      passProps = {
        primaryText: this.state.locals.ok,
        primaryPress: () => this.onPressEzCashCancel(),
        secondaryButtonStyle: { color: Colors.colorGrey },
        primaryButtonStyle: { color: Colors.colorRed },
        disabled: false,
        hideTopImageView: false,
        descriptionTitle: description_title,
        descriptionTitleTextStyle: { alignSelf: 'flex-start' },
        description: description,
        icon: Images.icons.FailureAlert,
      };
    } else {
      passProps = {
        primaryText: this.state.locals.btnOk,
        primaryPress: () => this.onPressOk(),
        primaryButtonStyle: { color: Colors.colorRed },
        disabled: false,
        hideTopImageView: false,
        descriptionTitle: description_title,
        descriptionTitleTextStyle: { alignSelf: 'flex-start' },
        description: description,
        icon: Images.icons.FailureAlert,
      };
    }

    this.dtvGeneralModal(passProps);
  }


  onChangeTextCustomerName = (text) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onChangeTextCustomerName : ', text);
    if (FuncUtils.nameValidation(text)) {
      console.log('onChangeTextCustomerName - TRUE');
      _this.props.dtvSetCustomerName(text);
    } else {
      _this.props.dtvSetCustomerName(text, _this.state.locals.customer_name_error);
    }
  }

  onChangeTextCustomerAddressLine1 = (text) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onChangeTextCustomerAddressLine1 : ', text);
    if (FuncUtils.addressValidation(text)) {
      console.log('onChangeTextCustomerAddressLine1 - TRUE');
      _this.props.dtvSetCustomerAddressLine1(text);
    } else {
      _this.props.dtvSetCustomerAddressLine1(text, _this.state.locals.customer_address_line_error);
    }
  }

  onChangeTextCustomerAddressLine2 = (text) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onChangeTextCustomerAddressLine2 : ', text);
    if (FuncUtils.addressValidation(text)) {
      console.log('onChangeTextCustomerAddressLine2 - TRUE');
      _this.props.dtvSetCustomerAddressLine2(text);
    } else {
      _this.props.dtvSetCustomerAddressLine2(text, _this.state.locals.customer_address_line_error);
    }
  }

  onChangeTextCustomerAddressLine3 = (text) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onChangeTextCustomerAddressLine3 : ', text);
    if (FuncUtils.addressValidation(text)) {
      console.log('onChangeTextCustomerAddressLine2 - TRUE');
      _this.props.dtvSetCustomerAddressLine3(text);
    } else {
      _this.props.dtvSetCustomerAddressLine3(text, _this.state.locals.customer_address_line_error);
    }
  }

  onChangeTextCustomerCity = (text) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onChangeTextCustomerCity : ', text);
    let requestParams = {
      search_text: text
    };
    _this.props.dtvSetCustomerCity({ city_name: text, code: '' });
    if (FuncUtils.alphaValidation(text) && text.length >= Constants.AUTO_SEARCH_MIN_TEXT_LENGTH) {
      console.log('onChangeTextCustomerCity - TRUE');
      _this.debouncedCitySearch(requestParams, _this);
    } else if (text === '') {
      console.log('onChangeTextCustomerCity - EMPTY');
      _this.props.dtvSetCustomerCityData({
        all_data: [],
        city_names: [],
        error: ''
      });
    } else {
      console.log('onChangeTextCustomerCity - FALSE');
      _this.props.dtvSetCustomerCityData({
        all_data: [],
        city_names: [],
        error: _this.state.locals.customer_city_error
      });
    }
  }

  dtvCitySearch = (requestParams, text) => {
    let _this = this;
    console.log('dtvCitySearch : ', text);
    _this.props.dtvCitySearch(requestParams, _this);

  }

  onCustomerCityDropdownSelect = (idx, value) => {
    this.props.commonResetKycSignature();
    let _this = this;
    console.log('onCustomerCityDropdownSelect', idx, value);
    _this.props.dtvSetCustomerCity(this.props.customer_city_data.all_data[parseInt(idx)]);

  }

  onCitySelectionDropdownWillHide = () => {
    console.log('onCitySelectionDropdownWillHide - city');
    _.delay((_this) => {
      if (_this.props.customer_city.code === '') {
        console.log('onCitySelectionDropdownWillHide :: empty city code - city');
        // _this.props.dtvSetCustomerCityData({
        //   all_data: [],
        //   city_names: [],
        //   error: ''
        // });
        // _this.props.dtvSetCustomerCity({ 
        //   city_name : '', 
        //   code : '' 
        // });
      } else {
        console.log('onCitySelectionDropdownWillHide - city :: city code available - city', _this.props.customer_city.code);
      }
    }, 100, this);

  }

  onBlurCitySearchInput = () => {
    console.log('onBlurCitySearchInput - city');
    _.delay((_this) => {
      if (_this.props.customer_city.code === '') {
        console.log('onBlurCitySearchInput :: empty city code - city');
        // _this.props.dtvSetCustomerCityData({
        //   all_data: [], 
        //   city_names: [],
        //   error: ''
        // });

      } else if (_this.props.customer_city.city_name.length < Constants.AUTO_SEARCH_MIN_TEXT_LENGTH) {
        console.log('onBlurCitySearchInput :: city_name_length < min length - city');
        // _this.props.dtvSetCustomerCityData({
        //   all_data: [], 
        //   city_names: [],
        //   error: 'Enter atleast 3 letters to search'
        // });
      } else {
        console.log('onBlurCitySearchInput :: city code available - city', _this.props.customer_city.code);
      }
    }, 100, this);
  }


  /**
   * @description load Same day installation address capture view 
   * @memberof DtvMainViewContainer
   */

  RenderAddressCaptureView = () => {
    let _this = this;
    if (_this.props.opt_sameDayInstallation) {
      return (
        <View key={this.props.random_value}>
          {_this.RenderInputCustomerName()}
          {_this.RenderInputAddressLine1()}
          {_this.RenderInputAddressLine2()}
          {_this.RenderInputAddressLine3()}
          {_this.RenderInputCitySearch()}
        </View>
      );
    } else {
      return true;
    }
  }


  RenderInputCustomerName = () => {
    return (
      <SectionContainer refName={'customerName'}>
        <MaterialInput
          label={this.state.locals.customer_name}
          index={'customerName'}
          ref={'customerName'}
          value={this.props.customer_name.value}
          error={!_.isEmpty(this.props.customer_name.value) ? this.props.customer_name.error : ''}
          customStyle={styles.materialInputCustomStyle}
          onChangeText={(text) => this.onChangeTextCustomerName(text)}
          onIconPress={() => {
            console.log('## onIconPress #customerName');
          }}
          icon={Constants.icon.edit}
          hideRightIcon={this.props.customer_name.value == ''} />
      </SectionContainer>
    );
  }

  RenderInputAddressLine1 = () => {
    return (
      <SectionContainer refName={'customerAddressLine1'}>
        <MaterialInput
          label={this.state.locals.customer_address_line1}
          index={'customerAddressLine1'}
          ref={'customerAddressLine1'}
          value={this.props.customer_address_line1.value}
          error={!_.isEmpty(this.props.customer_address_line1.value) ? this.props.customer_address_line1.error : ''}
          customStyle={styles.materialInputCustomStyle}
          onChangeText={(text) => this.onChangeTextCustomerAddressLine1(text)}
          onIconPress={() => {
            console.log('## onIconPress #customerAddressLine1');
          }}
          icon={Constants.icon.edit}
          hideRightIcon={this.props.customer_address_line1.value == ''} />
      </SectionContainer>
    );
  }

  RenderInputAddressLine2 = () => {
    return (
      <SectionContainer refName={'customerAddressLine2'}>
        <MaterialInput
          label={this.state.locals.customer_address_line2}
          index={'customerAddressLine2'}
          ref={'customerAddressLine2'}
          value={this.props.customer_address_line2.value}
          error={!_.isEmpty(this.props.customer_address_line2.value) ? this.props.customer_address_line2.error : ''}
          customStyle={styles.materialInputCustomStyle}
          onChangeText={(text) => this.onChangeTextCustomerAddressLine2(text)}
          onIconPress={() => {
            console.log('## onIconPress #customerAddressLine2');
          }}
          icon={Constants.icon.edit}
          hideRightIcon={this.props.customer_address_line2.value == ''} />
      </SectionContainer>
    );
  }

  RenderInputAddressLine3 = () => {
    return (
      <SectionContainer refName={'customerAddressLine3'}>
        <MaterialInput
          label={this.state.locals.customer_address_line3}
          index={'customerAddressLine3'}
          ref={'customerAddressLine3'}
          value={this.props.customer_address_line3.value}
          error={!_.isEmpty(this.props.customer_address_line3.value) ? this.props.customer_address_line3.error : ''}
          customStyle={styles.materialInputCustomStyle}
          onChangeText={(text) => this.onChangeTextCustomerAddressLine3(text)}
          onIconPress={() => {
            console.log('## onIconPress #customerAddressLine3');
          }}
          icon={Constants.icon.edit}
          hideRightIcon={this.props.customer_address_line3.value == ''} />
      </SectionContainer>
    );
  }

  RenderInputCitySearch = () => {
    return (
      <SectionContainer refName={'customerCity'}>
        <DropDownInput
          index={'customerCity'}
          ref={'customerCity'}
          modalRef={(ref) => this.customerCitySearch = ref}
          dropDownTitle={this.state.locals.customer_city}
          onSelect={this.onCustomerCityDropdownSelect}
          onTextInputChange={this.onChangeTextCustomerCity}
          dropDownData={this.props.customer_city_data.city_names}
          selectedValue={this.props.customer_city.city_name}
          defaultIndex={-1}
          onDropdownWillHide={this.onCitySelectionDropdownWillHide}
          onBlur={this.onBlurCitySearchInput}
          error={this.props.customer_city_data.error}
          isTextInput={true}
          hideRightIcon
          customStyle={styles.dropDownInputCustomStyle}
        // disabled={!this.props.didSerialValidated}
        // disableDropdown={!this.props.didSerialValidated}
        //disableIconTap={!this.props.didSerialValidated} 
        />
      </SectionContainer>
    );
  }


  render() {

    const _this = this;
    let CustomertotalPaymentProps = _this.props.data_totalPaymentProps;
    let data_dealerTotalPaymentProps = _this.props.data_dealerTotalPaymentProps;

    console.log('##CustomertotalPaymentProps', data_dealerTotalPaymentProps);

    const { packs, channels } = _this.props.selected_channel_n_packs;
    let channel_text = (channels.length > 0) ? `${channels.length} ${channels.length == 1 ? _this.state.locals.channel : _this.state.locals.channels} ` : '';
    let pack_text = (packs.length > 0) ? `${packs.length} ${ packs.length == 1 ? _this.state.locals.pack : _this.state.locals.packs} ` : '';
    let additional_channel_rental = FuncUtils.getRsValue(_this.props.selected_channel_total_package_rental.additionalMonthlyRental);
    let additional_channel_rental_text = (channel_text !== '' || pack_text !== '') ? `(${_this.state.locals.rs_label} ${additional_channel_rental} + ${_this.state.locals.tax})` : '';
    additional_channel_rental_text = (channels.length > 0 && packs.length > 0) ? '\n' + additional_channel_rental_text : additional_channel_rental_text;

    pack_text = (channels.length == 0 || packs.length == 0) ? pack_text : ' & ' + pack_text;

    let additional_channel_label_text = channel_text + pack_text + additional_channel_rental_text;

    let idLabel;
    let idSectionLabel;
    let idNumberRightIconHide = false;
    let idNumberIcon;
    let idIconSize;

    if (this.props.customerType === Constants.CX_TYPE_LOCAL) {
      idLabel = this.state.locals.labelNIc;
      idSectionLabel = this.state.locals.sectionNICValidation;
      idNumberRightIconHide = this.props.idNumber_input_value === '';
      idNumberIcon = Constants.icon.edit;
      idIconSize = 26;
    } else {
      idLabel = this.state.locals.labelPP;
      idSectionLabel = this.state.locals.sectionPPValidation;
      idNumberIcon = Constants.icon.next;
      idIconSize = Constants.icon.next.defaultFontSize;
    }

    const renderCxInfoImageCaptureFields = () => {
      
      if (this.props.customerType === Constants.CX_TYPE_LOCAL
        && (this.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
          || this.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {

        switch (this.props.customerInfoSectionIdType) {
          case Constants.CX_INFO_TYPE_NIC:
            return (<CustomerInfoNIC currentView={this} />);
          case Constants.CX_INFO_TYPE_PASSPORT:
            return (<CustomerInfoPassport currentView={this} />);
          case Constants.CX_INFO_TYPE_DRIVING_LICENCE:
            return (<CustomerInfoDrivingLicense currentView={this} />);
          default:
            return (<CustomerInfoNIC currentView={this} />);
        }
      } else if ((this.props.customerType === Constants.CX_TYPE_LOCAL
        || this.props.customerType === Constants.CX_TYPE_FOREIGNER)
        && this.props.customerOtpStatus == Constants.CX_OTP_VERIFIED) {
        return (<CustomerOtpVerified currentView={this} />);
      } else {
        return (<CustomerInfoPassport currentView={this} />);
      }
    };

    const renderCxInfoRadioButtonSection = () => {

      if (this.props.customerType === Constants.CX_TYPE_LOCAL
        && (this.props.customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED ||
          this.props.customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE)) {
        return (<SectionContainer hideView={false}>
          <View style={styles.containerRadioWrapper}>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_NIC}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_NIC ?
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_NIC)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_NIC}
                </Text>
              </RadioButton>
            </View>
            <View style={[styles.containerRadioBtn, styles.containerRadioBtnMiddle]}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_PP}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_PP ?
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_PASSPORT)}>
                <Text style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_Passport}
                </Text>
              </RadioButton>
            </View>
            <View style={styles.containerRadioBtn}>
              <RadioButton
                currentValue={this.props.customerInfoSectionIdTypeCode}
                value={Constants.ID_TYPE_DL}
                outerCircleSize={20}
                innerCircleColor={Colors.radioBtn.innerCircleColor}
                outerCircleColor={this.props.customerInfoSectionIdTypeCode == Constants.ID_TYPE_DL ?
                  Colors.radioBtn.innerCircleColor : Colors.radioBtn.outerCircleColor
                }
                onPress={() => this.setCusInfoMode(Constants.CX_INFO_TYPE_DRIVING_LICENCE)}>
                <Text numberOfLines={2} style={styles.radioBtnTxt}>{this.state.locals.radioBtnField_Driving_Licence}
                </Text>
              </RadioButton>
            </View>
          </View>
        </SectionContainer>);
      } else {
        return true;
      }
    };

    return (

      <View style={styles.container} key={this.props.random_value}>
        {/* NIC INPUT SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.NIC_VALIDATION}
          sectionId={Constants.MAIN_SECTION.NIC_VALIDATION}
          label={idSectionLabel}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.NIC_VALIDATION)}
          validationStatus={this.props.sectionId_validation_status}
          isFocused={this.props.section_1_visible} >
          <MainView
            formData={this.props}
            key={this.state + 1}
            style={styles.mainSectionContainer}
            visible={this.props.section_1_visible}
            removeWhenHidden
            noAnimation>
            <SectionContainer
              refName={'idNumber'}
              hideView={this.getIndividualItemHideStatus('idNumber')}>
              <MaterialInput
                label={idLabel}
                value={this.props.idNumber_input_value}
                index={'idNumber'}
                ref={'idNumber'}
                error={this.props.idNumber_input_value === '' ? '' : this.props.id_input_error}
                onChangeText={this.onIdNumberTextChange}
                onIconPress={() => this.onPressValidatePassport()}
                customStyle={styles.materialInputCustomStyle}
                icon={idNumberIcon}
                iconSize={idIconSize}
                disableIconTap={false}
                tapToFocus={this.props.customerType === Constants.CX_TYPE_LOCAL}
                hideRightIcon={idNumberRightIconHide}
              />
            </SectionContainer>
          </MainView>
        </InputSection>
        {/* NIC INPUT SECTION END */}
        {/* PRODUCT INFORMATION SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.PRODUCT_INFORMATION}
          sectionId={Constants.MAIN_SECTION.PRODUCT_INFORMATION}
          label={this.state.locals.sectionProductInformation}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.PRODUCT_INFORMATION)}
          validationStatus={this.props.sectionProductInfo_validation_status}
          isFocused={this.props.section_2_visible} >
          <MainView
            formData={this.props}
            key={this.state + 2}
            style={styles.mainSectionContainer}
            visible={this.props.section_2_visible}
            removeWhenHidden
            noAnimation>
            <SectionContainer
              refName={'dtvPackages'}
              hideView={this.getIndividualItemHideStatus('dtvPackages')}>
              <DropDownInput
                index={'dtvPackages'}
                ref={'dtvPackages'}
                dropDownTitle={this.state.locals.dtv_package}
                onSelect={this.onDTVPkgInfoDropdownSelect}
                dropDownData={this.props.availableDTVDataPackagesList.dropDownData}
                selectedValue={this.props.availableDTVDataPackagesList.dropDownData.length > 1 ? this.props.availableDTVDataPackagesList.selectedValue : this.props.availableDTVDataPackagesList.dropDownData[0]}
                defaultIndex={this.props.availableDTVDataPackagesList.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo(this.props.availableDTVDataPackagesList, 'datapackages');
                }}
                //dropdownStyle={this.state.appState !== 'active' ? {  height: 0} : {}}
                showArrow={FuncUtils.getArraySize(this.props.availableDTVDataPackagesList.dropDownData) != 1}
                customStyle={styles.dropDownInputCustomStyle}
                disabled={FuncUtils.getArraySize(this.props.availableDTVDataPackagesList.dropDownData) < 2}
                disableDropdown={FuncUtils.getArraySize(this.props.availableDTVDataPackagesList.dropDownData) < 2}
              />
            </SectionContainer>
            <SectionContainer
              refName={'additionalChannels'}
              hideView={this.getIndividualItemHideStatus('additionalChannels')}>
              <MaterialInput
                label={this.state.locals.additional_channels}
                index={'additionalChannels'}
                ref={'additionalChannels'}
                value={additional_channel_label_text}
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => this.loadAdditionalChannels()}
                icon={Constants.icon.edit}
                hideRightIcon={false}
                editable={false}
                multiline={true}
                numberOfLines={(additional_channel_label_text == '') ? 1 : 2}
                tapToFocus={false}
                disableIconTap={_this.props.selectedDTVDataPackageDetails.package_code === ''
                  || _this.props.selectedDTVDataPackageDetails.package_code === undefined || (this.props.eoafSerial.material_serial_avaialable === false && this.props.availableDTVDataPackagesList.default_fullfil_status === Constants.CASH_AND_DELIVERY )}
              />
            </SectionContainer>
            <SectionContainer
              refName={'fulfillmentType'}
              hideView={this.getIndividualItemHideStatus('fulfillmentType')}>
              <DropDownInput
                index={'fulfillmentType'}
                ref={'fulfillmentType'}
                dropDownTitle={this.state.locals.fulfillment_type}
                onSelect={this.onPressFulfillmentTypeDropdownSelect}
                dropDownData={this.props.dtv_fullfillment_type.dropDownData}
                selectedValue={this.props.availableDTVDataPackagesList.dropDownData.length > 0 ? this.props.dtv_fullfillment_type.selectedValue : this.props.availableDTVDataPackagesList.default_fullfil_status.display_name}
                defaultIndex={this.props.dtv_fullfillment_type.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo();
                }}
                customStyle={styles.dropDownInputCustomStyleFulfillment}
                hideRightIcon={true}
                title={this.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY ?
                  this.state.locals.deliveryCharge + Utill.numberWithCommas(this.props.dtvDeliveryCharge.delivery_charge) : ''}
              />
            </SectionContainer>
            <SectionContainer
              refName={'packType'}
              hideView={this.getIndividualItemHideStatus('packType')}>
              <DropDownInput
                index={'packType'}
                ref={'packType'}
                dropDownTitle={this.state.locals.packType}
                onSelect={this.onPressPackTypeDropdownSelect}
                dropDownData={this.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY && this.props.eoafSerial.material_serial_avaialable == false ? [] : this.props.dtv_pack_type.dropDownData}
                selectedValue={this.props.dtv_pack_type.selectedValue}
                defaultIndex={this.props.dtv_pack_type.defaultIndex}
                icon={Constants.icon.info}
                onPress={() => {
                  this.onPressInfo();
                }}
                customStyle={styles.dropDownInputCustomStylePackType}
                hideRightIcon={true}
                disabled={this.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY && this.props.eoafSerial.material_serial_avaialable == false}
                disableDropdown={this.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY && this.props.eoafSerial.material_serial_avaialable == false}
              />
            </SectionContainer>
            <SectionContainer
              refName={'dtvOffer'}
              hideView={this.getIndividualItemHideStatus('dtvOffer')}>
              <MaterialInput
                label={this.state.locals.dtvOffer}
                index={'dtvOffer'}
                ref={'dtvOffer'}
                value={this.props.availableDtvOffers.offer_name}
                error={this.state.mobileError}
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => this.loadDataDtvOffers()}
                editable={false}
                multiline={true}
                icon={Constants.icon.edit}
                hideRightIcon={_.isNil(this.props.dtv_pack_type.packageCode)}
                disabled={_.isNil(this.props.dtv_pack_type.packageCode)} />
            </SectionContainer>
            <SectionContainer
              refName={'voucherCode'}
              hideView={this.getIndividualItemHideStatus('voucherCode')}>
              <MaterialInput
                label={this.state.locals.labelVoucherCode}
                index={'voucherCode'}
                ref={'voucherCode'}
                error={this.state.voucherCodeError}
                value={this.props.enteredDTVVoucherCode}
                customStyle={styles.materialInputCustomStyle}
                onChangeText={this.onVoucherCodeTextChange}
                onIconPress={() => {
                  this.onPressVoucherCodeValidate();
                }}
                icon={Constants.icon.next}
                iconSize={Constants.icon.next.defaultFontSize}
                disableIconTap={
                  this.props.enteredDTVVoucherCode === '' ||
                  this.state.voucherCodeError !== '' ||
                  (this.props.validatedVoucherCode === this.props.enteredDTVVoucherCode)}
              />
            </SectionContainer>
            <SectionContainer
              refName={'checkBox_stb_upgrade'}
              customStyle={styles.checkBoxStbUpgradeStyle}
              hideView={_this.getIndividualItemHideStatus('checkBox_stb_upgrade')}>
              <ElementCheckBox
                checkBoxText={_this.props.stb_data.stb_upgrade_text}
                onClick={() => _this.debouncedCheckboxSelect('stb_upgrade')}
                disabled={false}
                isChecked={_this.props.stb_upgrade_checked} />
            </SectionContainer>
            <SectionContainer
              refName={'warrantyExtension'}
              hideView={this.getIndividualItemHideStatus('warrantyExtension')}>
              <DropDownInput
                index={'warrantyExtension'}
                ref={'warrantyExtension'}
                dropDownTitle={this.state.locals.warrantyExtension}
                onSelect={this.onPressWarrantyExtensionDropdownSelect}
                dropDownData={this.props.dtv_warranty_extensions.dropDownData}
                selectedValue={this.props.dtv_warranty_extensions.selectedValue}
                defaultIndex={this.props.dtv_warranty_extensions.defaultIndex}
                icon={Constants.icon.info}
                customStyle={styles.dropDownInputCustomStyle}
                hideRightIcon={true}
              // disabled={this.props.disableField_VoucherCode}
              // disableDropdown={this.props.disableField_VoucherCode}
              // disableIconTap={this.props.disableField_VoucherCode}
              />
            </SectionContainer>
            <SectionContainer
              refName={'firstReload'}
              hideView={this.getIndividualItemHideStatus('firstReload')}>
              <MaterialInput
                label={this.state.locals.firstReload}
                value={this.props.reloadAmountValue}
                title={`${this.state.locals.firstReloadTitleMin}100${this.state.locals.firstReloadTitleMax}25,000`}
                index={'firstReload'}
                ref={'firstReload'}
                keyboardType={'numeric'}
                onBlur={this.debouncedonBlurReloadRapidEzCheck}
                error={this.props.first_reload_validation_error === '' ? '' : this.props.first_reload_validation_error}
                onChangeText={(text) => this.onFirstReloadAmountTextChange(text)}
                customStyle={styles.materialInputCustomStyle}
                hideRightIcon={true}
                editable = {FuncUtils.getValueSafe(this.props.availableDtvOffers).offer_name ? true : false}
              />
            </SectionContainer>
            <SectionContainer
              refName={'stbPackSerial'}
              hideView={this.getIndividualItemHideStatus('stbPackSerial')}>
              <MaterialInput
                label={this.state.locals.labelStbPackSerial}
                index={'stbPackSerial'}
                ref={'stbPackSerial'}
                editable={false}
                value={this.props.serialPackValue}
                onIconPress={() => this.onPressSerialScan('DTV_STB')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle} />
            </SectionContainer>
            <SectionContainer
              refName={'accessoriesPackSerial'}
              hideView={this.getIndividualItemHideStatus('accessoriesPackSerial')}>
              <MaterialInput
                label={this.state.locals.labelAccessoryPackSerial}
                index={'accessoriesPackSerial'}
                ref={'accessoriesPackSerial'}
                editable={false}
                value={this.props.accessoriesSerialValue}
                onIconPress={() => this.onPressSerialScan('DTV_ACCESSORIES')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle} />
            </SectionContainer>
            <SectionContainer
              refName={'bundleSerial'}
              hideView={this.getIndividualItemHideStatus('bundleSerial')}>
              <MaterialInput
                label={this.state.locals.labelBundleSerial}
                index={'bundleSerial'}
                ref={'bundleSerial'}
                editable={false}
                value={this.props.bundleSerialValue}
                onIconPress={() => this.onPressSerialScan('DTV_BUNDLE')}
                icon={Constants.icon.barcode}
                disableIconTap={false}
                customStyle={styles.materialInputCustomStyle} />
            </SectionContainer>
            <SectionContainer
              refName={'oafSerial'}
              hideView={this.getIndividualItemHideStatus('oafSerial')}>
              <MaterialInput
                label={this.state.locals.dtvOafSerial}
                index={'oafSerial'}
                ref={'oafSerial'}
                value={this.props.material_serial}
                error={this.state.mobileError}
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => this.onPressInfo()}
                editable={false}
                icon={Constants.icon.edit}
                hideRightIcon={true} />
            </SectionContainer>
          </MainView>
        </InputSection>
        {/* PRODUCT INFORMATION SECTION END */}
        {/* CUSTOMER INFORMATION SECTION START */}
        <InputSection
          key={Constants.MAIN_SECTION.CUSTOMER_INFORMATION}
          sectionId={Constants.MAIN_SECTION.CUSTOMER_INFORMATION}
          customStyle={styles.customerInformationInputSection}
          label={this.state.locals.sectionCustomerInformation}
          onPress={() => this.onPressSection(Constants.MAIN_SECTION.CUSTOMER_INFORMATION)}
          validationStatus={this.props.sectionCustomerInfo_validation_status}
          isFocused={this.props.section_3_visible} >
          <MainView
            formData={this.props}
            key={this.state + 3}
            style={styles.mainSectionContainer}
            visible={this.props.section_3_visible}
            removeWhenHidden
            noAnimation>
            {/* CUSTOMER INFORMATION RADIO BUTTONS START */}
            {renderCxInfoRadioButtonSection()}
            {/* CUSTOMER INFORMATION RADIO BUTTONS END */}
            {/* CUSTOMER INFORMATION IMAGE CAPTURE FIELDS START */}
            {renderCxInfoImageCaptureFields()}
            {/* CUSTOMER INFORMATION IMAGE CAPTURE FIELDS END */}
            {_this.RenderAddressCaptureView()}
            <SectionContainer
              refName={'customerMobile'}
              hideView={this.getIndividualItemHideStatus('customerMobile')}>
              <MaterialInput
                label={this.state.locals.labelCustomerMobile}
                index={'customerMobile'}
                ref={'customerMobile'}
                value={this.props.dtvMobileNumber}
                keyboardType={'numeric'}
                error={this.state.mobileError}
                maxLength={globalConfig.MOBILE_NUMBER_MAX_LENGTH}
                customStyle={styles.materialInputCustomStyle}
                onChangeText={(text) => this.onChangeTextCustomerMobile(text)}
                onIconPress={() => {
                  console.log('## onIconPress #customerMobile');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.dtvMobileNumber == ''} />
            </SectionContainer>
            <SectionContainer
              refName={'customerLandline'}
              hideView={this.getIndividualItemHideStatus('customerLandline')}>
              <MaterialInput
                label={this.state.locals.labelCustomerLandline}
                index={'customerLandline'}
                ref={'customerLandline'}
                keyboardType={'numeric'}
                value={this.props.customerLandline}
                error={this.state.landlineError}
                customStyle={styles.materialInputCustomStyle}
                maxLength={globalConfig.MOBILE_NUMBER_MAX_LENGTH}
                onChangeText={(text) => this.onChangeTextCustomerLandline(text)}
                onIconPress={() => {
                  console.log('## onIconPress #customerLandline');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.customerLandline == ''} />
            </SectionContainer>
            <SectionContainer
              refName={'email'}
              hideView={this.getIndividualItemHideStatus('email')}>
              <MaterialInput
                label={this.state.locals.labelCustomerEmail}
                index={'email'}
                value={this.props.email_input_value}
                ref={'email'}
                onChangeText={(text) => this.onChangeTextEmail(text)}
                error={this.state.emailError}
                customStyle={styles.materialInputCustomStyle}
                onIconPress={() => {
                  console.log('## onIconPress #email');
                }}
                icon={Constants.icon.edit}
                hideRightIcon={this.props.email_input_value == ''} />
            </SectionContainer>

            {/* SIGNATURE BUTTON START */}
            <SectionContainer>
              <TouchableOpacity
                style={styles.signatureContainer}
                onPress={() => this.onCaptureSignature(CustomertotalPaymentProps, data_dealerTotalPaymentProps)}
              >
                {this.props.common.kyc_signature !== null
                  ? <Image
                    style={styles.signatureImage}
                    key={this.props.common.kyc_signature.signatureRand}
                    source={{
                      uri: `data:image/jpg;base64,${this.props.common.kyc_signature.signatureBase64}`
                    }}
                    resizeMode="stretch"
                  />
                  :
                  <Text style={styles.signatureTxt}>
                    {this.state.locals.customerSignature}
                  </Text>
                }
              </TouchableOpacity>
            </SectionContainer>
            {/* SIGNATURE BUTTON END */}
          </MainView>
        </InputSection>
        {/* TOTAL PAYMENTS SECTION START */}
        <View>
          <SectionContainer
            refName={'totalPayment'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('totalPayment')}>
            <CustomerPayment totalPaymentProps={CustomertotalPaymentProps} />
          </SectionContainer>
        </View>
        <View>
          <SectionContainer
            refName={'dealerCollection'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('dealerCollection')}>
            <DealerCollectionBreakdown dealerCollectionPaymentProps={data_dealerTotalPaymentProps} />
          </SectionContainer>
        </View>
        {/* TOTAL PAYMENTS SECTION END */}
        {/* EZ CASH INPUT START */}
        <SectionContainer
          refName={'ezCashPin'}
          customMarginLeft={{ marginLeft: 50 }}
          hideView={this.getIndividualItemHideStatus('ezCashPin')}
          customStyle={styles.ezSectionCustomStyle}>
          <MaterialInput
            label={this.state.locals.ezCashPin}
            index={'ezCashPin'}
            ref={'ezCashPin'}
            title={this.props.configuration.default_ez_cash_account === '' ?
              '' : `${this.props.configuration.default_ez_cash_account} ${this.state.locals.ez_cash_pin_title}`}
            secureTextEntry
            maxLength={Constants.EZ_CASH_PIN_MAX_LENGTH}
            keyboardType={'numeric'}
            value={this.props.ez_cash_input_pin}
            icon={Constants.icon.edit}
            hideRightIcon={this.props.ez_cash_input_pin == ''}
            onChangeText={this.onExCashPinTextChange}
            customStyle={styles.materialInputCustomStyle}
            onIconPress={() => {
              console.log('## onIconPress #ezCashPin');
            }}
          />
        </SectionContainer>
        {/* EZ CASH INPUT END */}
        {/* EZ CASH BALANCE */}
        <View>
          <SectionContainer
            refName={'ezCashBalance'}
            customMarginLeft={{ marginLeft: 50 }}
            hideView={this.getIndividualItemHideStatus('ezCashBalance')}>
            <EzCashBalance
              ezCashAccountBalance={this.props.ezCashAccountBalance}
              rs_label={this.state.locals.rs_label}
              title={this.state.locals.ez_cash_account_balance} />
          </SectionContainer>
        </View>
        {/* EZ CASH BALANCE END*/}
        {/* FINAL ACTIVATION BUTTON */}
        <View style={styles.activateBtnContainer}>
          <View style={styles.activateDummy} />
          <TouchableOpacity
            style={[styles.activateBtn, {
              backgroundColor: this.props.activateButtonColorStatus ?
                Colors.btnActive : Colors.btnDisable
            }]}
            onPress={() => this.debouncedFinalActivation()}
            disabled={!(this.props.sectionId_validation_status
              && this.props.sectionProductInfo_validation_status_fromMapToStatus)
            }
          >
            <Text style={[styles.activateBtnTxt, {
              color: this.props.activateButtonColorStatus ?
                Colors.btnActiveTxtColor : Colors.btnDisableTxtColor
            }]} >
              {this.state.locals.txtActivate}
            </Text>
          </TouchableOpacity>
        </View>
        {/* FINAL ACTIVATION BUTTON END */}
      </View>
    );
  }
}


const ElementCheckBox = ({ checkBoxText, onClick, isChecked, isHidden = false, disabled = false }) => {
  if (isHidden == true) {
    return (<View />);
  } else {
    return (
      <TouchableOpacity style={styles.checkBoxContainer} onPress={onClick} disabled={disabled}>
        <View style={styles.checkBoxView}>
          <CheckBox
            checkBoxColor={isChecked ? Colors.yellow : Colors.colorGrey}
            uncheckedCheckBoxColor={Colors.colorGrey}
            style={styles.checkBoxStyle}
            onClick={onClick}
            disabled={disabled}
            isChecked={isChecked} />
        </View>
        <View style={styles.checkBoxTextView}>
          <Text style={styles.checkBoxText}>
            {checkBoxText}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
};


const mapStateToProps = state => {
  console.log('#############3 START MAP_TO_STATE_PROPS  ###########################');
  console.log('****** REDUX STATE :: DTV => DtvMainViewContainer :: dtvActivation \n', state.dtvActivation);
  console.log('****** REDUX STATE :: DTV => DtvMainViewContainer :: common\n', state.common);
  console.log('****** REDUX STATE :: DTV => DtvMainViewContainer :: configuration\n', state.configuration);
  const common = state.common;
  const configuration = state.configuration;
  const Language = state.lang.current_lang;

  const common_activity_start_time = state.common.common_activity_start_time;
  const txn_reference = state.dtvActivation.txn_reference;
  const random_value = state.dtvActivation.random_value;

  const section_1_visible = state.dtvActivation.section_1_visible;
  const section_2_visible = state.dtvActivation.section_2_visible;
  const section_3_visible = state.dtvActivation.section_3_visible;

  const selected_connection_type = state.dtvActivation.selected_connection_type;
  const customerType = state.dtvActivation.customerType;
  const is_prepaid = selected_connection_type === Constants.PREPAID;

  const customerExistence = state.dtvActivation.customerExistence; //TODO
  const customerOtpStatus = state.dtvActivation.customerOtpStatus; //TODO
  const customerIdType = state.dtvActivation.customerIdType;
  const customerInfoSectionIdType = state.dtvActivation.customerInfoSectionIdType;
  const customerInfoSectionIdTypeCode = state.dtvActivation.customerInfoSectionIdTypeCode;

  const idNumber_input_value = state.dtvActivation.idNumber_input_value;
  const id_input_error = state.dtvActivation.id_input_error;
  const idValidationData = state.dtvActivation.idValidationData; //TODO
  const idValidationStatus = state.dtvActivation.idValidationStatus; //TODO

  const customer_id_info = state.dtvActivation.customer_id_info; //TODO
  const customerOutstandingData = state.dtvActivation.customerOutstandingData; //TODO

  const eoafSerial = state.dtvActivation.eoafSerial;
  const material_serial = eoafSerial.material_serial_avaialable == true ? eoafSerial.material_serial : '';

  const reloadAmountValue = state.dtvActivation.reloadAmountValue;

  //KYC related
  const opt_addressDifferent = state.dtvActivation.opt_addressDifferent;
  const opt_customerFaceNotClear = state.dtvActivation.opt_customerFaceNotClear;
  const opt_installationAddressDeferent = state.dtvActivation.opt_installationAddressDeferent;
  const opt_sameDayInstallation = state.dtvActivation.opt_sameDayInstallation;

  const availableDTVDataPackagesList = state.dtvActivation.availableDTVDataPackagesList;
  const selectedDTVDataPackageDetails = state.dtvActivation.selectedDTVDataPackageDetails;
  const dtvDeliveryCharge = state.dtvActivation.dtvDeliveryCharge;

  //OTP validation related
  const otpValidationData = state.dtvActivation.otpValidationData;
  const otpValidationStatus = state.dtvActivation.otpValidationStatus;
  const cx_otp_number = state.dtvActivation.cx_otp_number;

  const selected_connection_number = state.dtvActivation.selected_connection_number;
  const dtv_pack_type = state.dtvActivation.dtv_pack_type;
  const dtv_fullfillment_type = state.dtvActivation.dtv_fullfillment_type;
  const availableDtvOffers = state.dtvActivation.availableDtvOffers;

  //Additional channels and packs
  const selected_channel_n_packs = state.dtvActivation.selected_channel_n_packs;
  const additional_channel_list_data = state.dtvActivation.additional_channel_list_data;
  const selected_channel_total_package_rental = state.dtvActivation.selected_channel_total_package_rental;

  console.log('##additional_channel_list_data', additional_channel_list_data);
  console.log('##selected_channel_n_packs', selected_channel_n_packs);

  //Warranty extensions
  const dtv_warranty_extensions = state.dtvActivation.dtv_warranty_extensions;
  const dtv_warranty_extensions_data = state.dtvActivation.dtv_warranty_extensions_data;

  console.log('##warranty_pack_code', dtv_warranty_extensions_data);

  //STB upgrade
  const stb_data = state.dtvActivation.stb_data;
  const stb_upgrade_checked = state.dtvActivation.stb_upgrade_checked;

  //Voucher code
  const enteredDTVVoucherCode = state.dtvActivation.enteredDTVVoucherCode;
  const first_reload_validation_error = state.dtvActivation.first_reload_validation_error;
  const voucher_code_enabled = state.dtvActivation.voucher_code_enabled;
  const isValidDTVVoucherCode = state.dtvActivation.isValidDTVVoucherCode;
  console.log('##isValidDTVVoucherCode', isValidDTVVoucherCode);

  //Serial validation
  const serialPackValue = state.dtvActivation.serialPackValue;
  const accessoriesSerialValue = state.dtvActivation.accessoriesSerialValue;
  const bundleSerialValue = state.dtvActivation.bundleSerialValue;

  //Same day installation
  const enable_sameDayInstallation = state.dtvActivation.same_day_installation_data.enabled
    && dtv_fullfillment_type.package_code === Constants.CASH_AND_DELIVERY;
  const isCashAndDeliverySelected = dtv_fullfillment_type.package_code === Constants.CASH_AND_DELIVERY;
  const sameDayInstallation_price = state.dtvActivation.same_day_installation_data.price;
  const sameDayInstallation_cutoff_time = state.dtvActivation.same_day_installation_data.cut_off_time;

  console.log('DTV mapStateToProps :: enable_sameDayInstallation', enable_sameDayInstallation);
  console.log('DTV mapStateToProps :: opt_sameDayInstallation', opt_sameDayInstallation);
  console.log('DTV mapStateToProps :: sameDayInstallation_cutoff_time', sameDayInstallation_cutoff_time);

  const customer_name = state.dtvActivation.customer_name;
  const customer_address_line1 = state.dtvActivation.customer_address_line1;
  const customer_address_line2 = state.dtvActivation.customer_address_line2;
  const customer_address_line3 = state.dtvActivation.customer_address_line3;
  const customer_city = state.dtvActivation.customer_city;
  const customer_city_data = state.dtvActivation.customer_city_data;

  //Customer contact details
  const dtvMobileNumber = state.dtvActivation.input_MobileNumber;
  const customerLandline = state.dtvActivation.dtv_set_input_landLineNumber;
  const email_input_value = state.dtvActivation.email_input_value;

  //eZ cash
  const ez_cash_input_pin = state.dtvActivation.ez_cash_input_pin;
  const didEzCashAccountValidated = state.dtvActivation.didEzCashAccountValidated;
  const ezCashAccountBalance = state.dtvActivation.ezCashAccountBalance;

  //Payment calculation
  let allPaymentData = PaymentCalculations.getPaymentDataFromRedux(state.dtvActivation);
  const data_totalPaymentProps = allPaymentData.totalPaymentProps;
  const data_dealerTotalPaymentProps = allPaymentData.dealerCollectionPaymentProps;
  const totalOsAmount = state.dtvActivation.totalOsAmount;
  console.log('##allPaymentData', allPaymentData);

  const getConfiguration = state.auth.getConfiguration;
  const { rapid_ez_error = '' } = getConfiguration.rapid_ez_account;

  const { dtv_activation_status = false } = configuration;

  const validatedVoucherCode = state.dtvActivation.validatedVoucherCode;
  const voucherValue = state.dtvActivation.voucherValue;
  const voucherCategory = state.dtvActivation.voucherCategory;
  const voucherSubCategory = state.dtvActivation.voucherSubCategory;

  console.log('##validatedVoucherCode', validatedVoucherCode);

  // serial
  const validatedSerialData = state.dtvActivation.serialValidation;
  console.log('##validatedSerialData', validatedSerialData);
  const accessoriesSerialData = state.dtvActivation.accessoriesSerialData;
  const serialPackData = state.dtvActivation.serialPackData;
  const bundleSerialData = state.dtvActivation.bundleSerialData;

  const tcUrls = FuncUtils.getValueSafe(state.auth.getConfiguration).agreement_document;

  let { enable_reload = false, enable_dealer_collection = false } = getConfiguration.config;

  console.log('######################## Activate button status ####################');
  //Activate button status - start logic
  let activateButtonStatus;
  let activateButtonColorStatus;

  //Check image capture
  if (customerOtpStatus == Constants.CX_OTP_NOT_VERIFIED
    || customerOtpStatus == Constants.CX_OTP_NOT_APPLICABLE) {
    console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE');
    if (customerType === Constants.CX_TYPE_LOCAL) {
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL');
      if (customerInfoSectionIdType === Constants.CX_INFO_TYPE_NIC) {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_NIC');
        activateButtonStatus = common.kyc_nic_front !== null && common.kyc_nic_back !== null;
        activateButtonColorStatus = activateButtonStatus;
      } else if (customerInfoSectionIdType === Constants.CX_INFO_TYPE_DRIVING_LICENCE) {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_DRIVING_LICENCE');
        activateButtonStatus = common.kyc_driving_licence !== null;
        activateButtonColorStatus = activateButtonStatus;
      } else {
        console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_LOCAL >> CX_INFO_TYPE_PASSPORT');
        activateButtonStatus = common.kyc_passport !== null && common.kyc_proof_of_billing !== null;
        activateButtonColorStatus = activateButtonStatus;
      }
    } else {
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_FOREIGNER');
      console.log('CX_OTP_NOT_VERIFIED_OR_NOT_APPLICABLE >> CX_TYPE_FOREIGNER >> CX_INFO_TYPE_PASSPORT');
      activateButtonStatus = common.kyc_passport !== null && common.kyc_proof_of_billing !== null;
      activateButtonColorStatus = activateButtonStatus;
    }

  } else if (customerOtpStatus == Constants.CX_OTP_VERIFIED) {
    console.log('CX_TYPE_FOREIGNER_OR_LOCAL >> CX_OTP_VERIFIED');
    activateButtonStatus = true;
    activateButtonColorStatus = true;
  } else {
    console.log('CX_INVALID');
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check additional images
  if (opt_addressDifferent && common.kyc_proof_of_billing === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }
  if (opt_customerFaceNotClear && common.kyc_customer_image === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }
  if (opt_installationAddressDeferent && common.kyc_additional_pob === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check customer landline number
  if (customerLandline !== '' && !Utill.landlineNumberValidation(customerLandline)) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check customer email
  if (email_input_value !== '' && !Utill.emailValidation(email_input_value)) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Same day installation 
  if (opt_sameDayInstallation) {
    if (customer_name.value == "" || customer_name.error !=='') {
      activateButtonStatus = false;
      activateButtonColorStatus = false;
    }

    if (customer_address_line1.value == "" || customer_address_line1.error !=='') {
      activateButtonStatus = false;
      activateButtonColorStatus = false;
    }

    if (customer_address_line2.value == "" || customer_address_line2.error !=='') {
      activateButtonStatus = false;
      activateButtonColorStatus = false;
    }

    if (customer_city.code == "") {
      activateButtonStatus = false;
      activateButtonColorStatus = false;
    }
  }

  //Check signature capture
  if (common.kyc_signature === null) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //Check eZ cash PIN
  const shouldChargeEzCash = () => {
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps ', data_dealerTotalPaymentProps);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - dealerCollectionPayment ', data_dealerTotalPaymentProps.dealerCollectionPayment);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isNil ', _.isNil(data_dealerTotalPaymentProps.dealerCollectionPayment));
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isEmpty ', data_dealerTotalPaymentProps.dealerCollectionPayment == '');
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - isEqual ', parseInt(data_dealerTotalPaymentProps.dealerCollectionPayment) == 0);
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - totalOsAmount ', Utill.checkNumber(totalOsAmount));
    console.log('#### shouldChargeEzCash :: data_dealerTotalPaymentProps - totalOsAmount ', Utill.checkNumber(totalOsAmount) !== 0);

    return (!_.isNil(data_dealerTotalPaymentProps.dealerCollectionPayment)
      && data_dealerTotalPaymentProps.dealerCollectionPayment != ''
      && parseInt(data_dealerTotalPaymentProps.dealerCollectionPayment) != 0 || Utill.checkNumber(totalOsAmount) !== 0);
  };
  if (shouldChargeEzCash() && ez_cash_input_pin.length != 4) {
    activateButtonStatus = false;
    activateButtonColorStatus = false;
  }

  //get Product Info Validation Status
  const getProductInfoValidationStatus = () => {
    console.log('## getProductInfoValidationStatus');
    let did_serial_validated = true;
    let did_reload_validated = true;
    let did_offer_validated = false;
    let did_voucher_validated = true;

    let require_reload = enable_reload && FuncUtils.getValueSafe(getConfiguration.rapid_ez_account).rapid_ez_availability;
    let fulfillment_type_code = FuncUtils.getValueSafe(dtv_fullfillment_type.package_code);

    if (fulfillment_type_code == Constants.CASH_AND_CARRY) {
      let pack_type_package_code = FuncUtils.getValueSafe(dtv_pack_type.packageCode);
      switch (pack_type_package_code) {
        case 'HP':
          console.log('# stbPackSerial');
          did_serial_validated = serialPackValue != '';
          break;
        case 'FP':
          console.log('# accessoriesPackSerial');
          did_serial_validated = accessoriesSerialValue != '' && serialPackValue != '';
          break;
        case 'FPB':
          console.log('# bundleSerial');
          did_serial_validated = bundleSerialValue != '';
          break;
        default:
          console.log('# do_nothing');
          did_serial_validated = false;
      }
    }

    if (require_reload) {
      did_reload_validated = first_reload_validation_error == '';
    }

    let offer_code = FuncUtils.getValueSafe(availableDtvOffers).offer_code;

    if (offer_code != '') {
      did_offer_validated = true;
    }

    if (voucher_code_enabled) {
      did_voucher_validated = validatedVoucherCode !== '';
    }


    return did_reload_validated && did_serial_validated && did_offer_validated && did_voucher_validated;
  };

  let sectionProductInfo_validation_status_fromMapToStatus = getProductInfoValidationStatus();
  const sectionId_validation_status = state.dtvActivation.sectionId_validation_status; //Recheck
  const sectionProductInfo_validation_status = sectionProductInfo_validation_status_fromMapToStatus;
  const sectionCustomerInfo_validation_status = state.dtvActivation.sectionCustomerInfo_validation_status;

  //Activate button status - end logic
  console.log('#####-----------------------------------------------------------###');

  console.log('############## END OF MAP_TO_STATE_PROPS  #########################');

  return {
    common_activity_start_time,
    common,
    configuration,
    Language,
    txn_reference,
    random_value,
    activateButtonStatus,
    activateButtonColorStatus,
    section_1_visible,
    section_2_visible,
    section_3_visible,
    customerType,
    customerExistence,
    customerOtpStatus,
    customerIdType,
    customerInfoSectionIdType,
    customerInfoSectionIdTypeCode,
    selected_connection_type,
    is_prepaid,
    customer_id_info,
    idNumber_input_value,
    id_input_error,
    idValidationData,
    idValidationStatus,
    otpValidationData,
    otpValidationStatus,
    cx_otp_number,
    sectionId_validation_status,
    sectionProductInfo_validation_status,
    sectionCustomerInfo_validation_status,
    sectionProductInfo_validation_status_fromMapToStatus,
    customerOutstandingData,
    opt_addressDifferent,
    opt_customerFaceNotClear,
    opt_installationAddressDeferent,
    opt_sameDayInstallation,
    enable_sameDayInstallation,
    isCashAndDeliverySelected,
    sameDayInstallation_price,
    sameDayInstallation_cutoff_time,
    availableDTVDataPackagesList,
    selectedDTVDataPackageDetails,
    dtvDeliveryCharge,
    selected_connection_number,
    dtv_pack_type,
    dtv_fullfillment_type,
    availableDtvOffers,
    selected_channel_n_packs,
    additional_channel_list_data,
    selected_channel_total_package_rental,
    dtv_warranty_extensions,
    dtv_warranty_extensions_data,
    stb_data,
    stb_upgrade_checked,
    first_reload_validation_error,
    voucher_code_enabled,
    enteredDTVVoucherCode,
    isValidDTVVoucherCode,
    serialPackValue,
    bundleSerialValue,
    accessoriesSerialValue,
    eoafSerial,
    material_serial,
    dtvMobileNumber,
    customerLandline,
    email_input_value,
    customer_name,
    customer_address_line1,
    customer_address_line2,
    customer_address_line3,
    customer_city,
    customer_city_data,
    allPaymentData,
    reloadAmountValue,
    data_totalPaymentProps,
    data_dealerTotalPaymentProps,
    ez_cash_input_pin,
    ezCashAccountBalance,
    didEzCashAccountValidated,
    getConfiguration,
    enable_dealer_collection,
    enable_reload,
    tcUrls,
    dtv_activation_status,
    rapid_ez_error,
    validatedVoucherCode,
    voucherCategory,
    voucherSubCategory,
    voucherValue,
    validatedSerialData,
    accessoriesSerialData,
    serialPackData,
    bundleSerialData,
    totalOsAmount
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    marginLeft: 7,
    marginRight: 7,
    backgroundColor: Colors.appBackgroundColor
  },

  //main section container
  mainSectionContainer: {
    backgroundColor: Colors.appBackgroundColor
  },


  signatureContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    // margin: 10,
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: 3,
    //backgroundColor: '#808080'
  },

  signatureImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined,
  },

  signatureTxt: {
    color: Colors.btnDeactiveTxtColor,
    fontSize: Styles.defaultBtnFontSize
  },
  customerInformationInputSection: {
    marginBottom: 10
  },
  containerRadioWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 1
  },

  materialInputCustomStyle: {
    marginTop: -12
  },

  dropDownInputCustomStyle: {
    marginTop: -12
  },

  dropDownInputCustomStyleFulfillment: {
    marginTop: -12
  },

  dropDownInputCustomStylePackType: {
    marginTop: -12
  },

  checkBoxStbUpgradeStyle: {
    marginBottom: -4,
    // marginTop: -2,
  },

  ezSectionCustomStyle: {
    marginTop: 8
  },

  containerRadioBtn: {
    flex: 1,
    alignItems: 'flex-start',
    padding: 0,
    //justifyContent: 'flex-start',

  },
  containerRadioBtnMiddle: {
    marginRight: 25
  },
  radioBtnTxt: {
    paddingLeft: 5,
    fontWeight: 'bold',
    color: Colors.black
  },
  //Check box
  checkBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 8,
    marginTop: 8,
    // marginLeft:5
  },
  checkBoxView: {
    width: 25,
    alignItems: 'center',
  },
  checkBoxTextView: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    // marginRight: 5,
    //backgroundColor: 'green'
  },
  checkBoxStyle: {
    padding: 0,
    paddingBottom: 3,
    // paddingRight: 5
  },
  checkBoxText: {
    fontSize: 15,
    paddingBottom: 3,
    paddingLeft: 5
  },
  activateBtnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 10,
    marginTop: 37,
    height: 50,
    //backgroundColor: '#155'
  },
  activateDummy: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
    //backgroundColor: '#185',
  },
  activateBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // padding: 10,
    borderRadius: 5,
    backgroundColor: Colors.btnDisable

  },
  activateBtnTxt: {
    fontSize: Styles.defaultBtnFontSize,
    fontWeight: '500',
    color: Colors.btnDisableTxtColor,
  }
});


export default connect(mapStateToProps, actions)(DtvMainViewContainer);
