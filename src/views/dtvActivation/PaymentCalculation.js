import Constants from '../../config/constants';


let customerTotalPayment;
let outstandingFeeValue;
let deliveryInstallationCharge;
let unitPriceValue;
let voucherValue;
let warrentyExtCharge;
let reloadAmount;
let sameDayInstallationCharge;
let stbPackPrice;
let offerPriceValue;
let voucherValuefromRedux;
let unitPriceAfterVoucerValue;
let dealerCollectionPayment;
let unitPriceAdjustment;
let bundleSerialPrice;
let accessoriesSerialPrice;
let eoafSerialPrice;
let purchasedPrice;
let checkSelectedPackType;
let checkSelectedFullFillment;
let stpSerialValidationPackPrice;
let depositAmount;
let offerValueWithoutDeposit;

const getPaymentDataFromRedux = (dtvActState) => {
  console.log('### getPaymentDataFromRedux :: getPaymentData', dtvActState);

  let outstandingFeeValueRedux = dtvActState.totalOsAmount !== null
  && dtvActState.totalOsAmount !== undefined ? 
    dtvActState.totalOsAmount : 0.00;

  outstandingFeeValue = checkNumber(outstandingFeeValueRedux);
  offerValueWithoutDeposit = checkNumber(dtvActState.availableDtvOffers.price);
  voucherValuefromRedux = checkNumber(dtvActState.voucherValue);
  warrentyExtCharge = checkNumber(dtvActState.dtv_warranty_extensions.packPrice);
  reloadAmount = checkNumber(dtvActState.reloadAmountValue);
  checkSelectedPackType = dtvActState.dtv_pack_type.packageCode;
  checkSelectedFullFillment = dtvActState.dtv_fullfillment_type.package_code;
  depositAmount = checkNumber(dtvActState.deposit_amount);


  if (dtvActState.selected_connection_type == Constants.POSTPAID) {
    offerPriceValue = (parseFloat(offerValueWithoutDeposit) - parseFloat(depositAmount));
  } else {
    offerPriceValue = (parseFloat(offerValueWithoutDeposit).toFixed(2));
  }


  if (dtvActState.serialPackData !== undefined 
    && dtvActState.serialPackData  !== null) {
    console.log('### getPaymentDataFromRedux :: serialPackData : ', dtvActState.serialPackData.price);
    stpSerialValidationPackPrice =  checkNumber(dtvActState.serialPackData.price);  
  } else {
    stpSerialValidationPackPrice = 0;
  }

  if (dtvActState.bundleSerialData !== undefined 
    && dtvActState.bundleSerialData  !== null) {
    console.log('### getPaymentDataFromRedux :: bundleSerialData : ', dtvActState.bundleSerialData.price);
    bundleSerialPrice =  checkNumber(dtvActState.bundleSerialData.price);  
  } else {
    bundleSerialPrice = 0;
  }

  if (dtvActState.accessoriesSerialData !== undefined 
    && dtvActState.accessoriesSerialData  !== null) {
    console.log('### getPaymentDataFromRedux :: accessoriesSerialData : ', dtvActState.accessoriesSerialData.price);
    accessoriesSerialPrice =  checkNumber(dtvActState.accessoriesSerialData.price);  
  } else {
    accessoriesSerialPrice = 0;
  }


  if (dtvActState.eoafSerial !== undefined 
    && dtvActState.eoafSerial  !== null) {
    console.log('### getPaymentDataFromRedux :: eoafSerial : ', dtvActState.eoafSerial.price);
    eoafSerialPrice =  checkNumber(dtvActState.eoafSerial.price);  
  } else {
    eoafSerialPrice = 0;
  }

  
  //delivery installation charge validate
  if (checkSelectedFullFillment == "DELIVERY" ) {
    console.log('### getPaymentDataFromRedux :: deliveryInstallationCharge : ', dtvActState.dtvDeliveryCharge.delivery_charge);
    deliveryInstallationCharge = checkNumber(dtvActState.dtvDeliveryCharge.delivery_charge);
  } else {
    deliveryInstallationCharge = 0;
  }

  //checking Stb column updated or not
  if (dtvActState.stb_upgrade_checked ){

    stbPackPrice =  checkNumber(dtvActState.stb_data.stb_price);
  } else {
    
    stbPackPrice = 0.00;
  }


  //if sameday installation checked only this will add the price
  if (dtvActState.opt_sameDayInstallation){
    console.log('### getPaymentDataFromRedux :: sameDayInstallation_price : ', dtvActState.same_day_installation_data.price);
    sameDayInstallationCharge = checkNumber(dtvActState.same_day_installation_data.price);
  } else {

    sameDayInstallationCharge = 0.00;
  }



  // calculating PurchasePrice According To selected pack Type
  if ( checkSelectedFullFillment == "CARRY" && checkSelectedPackType == 'FP'){

    purchasedPrice = (parseFloat(stpSerialValidationPackPrice) + parseFloat(accessoriesSerialPrice));
  } else if ( checkSelectedFullFillment == "CARRY" && checkSelectedPackType == 'FPB' ) {
  
    purchasedPrice = bundleSerialPrice;
  } else if ( checkSelectedFullFillment == "CARRY" && checkSelectedPackType == 'HP' ) {
  
    purchasedPrice = stpSerialValidationPackPrice;
  } else if (checkSelectedFullFillment == "DELIVERY" ){
  
    purchasedPrice = eoafSerialPrice;
  } else {
  
    purchasedPrice = 0.00;
  }


  //add minus to voucher
  voucherValue = voucherValuefromRedux * (-1);

  // Calculating unit price
  unitPriceValue = (parseFloat(offerPriceValue) + parseFloat(stbPackPrice));

  //Calculating unitPrice with VoucherValue Deduction
  unitPriceAfterVoucerValue = (parseFloat(offerPriceValue) + parseFloat(stbPackPrice) + parseFloat(voucherValue));

  //Calculating unitPrice Adjustment
  unitPriceAdjustment = (parseFloat(unitPriceAfterVoucerValue) - parseFloat(purchasedPrice));

  // Calculating Total Amount
  if (dtvActState.selected_connection_type == Constants.PREPAID) {

    customerTotalPayment = parseFloat(outstandingFeeValue) + parseFloat(deliveryInstallationCharge) + parseFloat(unitPriceValue)
                          +parseFloat(voucherValue) + parseFloat(warrentyExtCharge) + parseFloat(reloadAmount)
                          +parseFloat(sameDayInstallationCharge);

    dealerCollectionPayment = parseFloat(outstandingFeeValue) + parseFloat(deliveryInstallationCharge) + parseFloat(unitPriceAdjustment)
                          + parseFloat(warrentyExtCharge) + parseFloat(sameDayInstallationCharge);
  } else {

    customerTotalPayment = parseFloat(outstandingFeeValue) + parseFloat(deliveryInstallationCharge) + parseFloat(unitPriceValue)
                          +parseFloat(voucherValue) + parseFloat(warrentyExtCharge) +parseFloat(sameDayInstallationCharge) + parseFloat(depositAmount); 

    dealerCollectionPayment = parseFloat(outstandingFeeValue) + parseFloat(deliveryInstallationCharge) + parseFloat(unitPriceAdjustment)
                            + parseFloat(warrentyExtCharge) + parseFloat(sameDayInstallationCharge) + parseFloat(depositAmount);
  }



  outstandingFeeValue = parseFloat(outstandingFeeValue).toFixed(2);
  deliveryInstallationCharge = parseFloat(deliveryInstallationCharge).toFixed(2);
  voucherValue = parseFloat(voucherValue).toFixed(2);
  unitPriceValue = parseFloat(unitPriceValue).toFixed(2);
  warrentyExtCharge = parseFloat(warrentyExtCharge).toFixed(2);
  reloadAmount = parseFloat(reloadAmount).toFixed(2);
  sameDayInstallationCharge = parseFloat(sameDayInstallationCharge).toFixed(2);

  console.log('### getPaymentDataFromRedux :: customerTotalPayment : ', customerTotalPayment);
  console.log('### getPaymentDataFromRedux :: outstandingFeeValue : ', outstandingFeeValue);
  console.log('### getPaymentDataFromRedux :: deliveryInstallationCharge : ', deliveryInstallationCharge);
  console.log('### getPaymentDataFromRedux :: voucherValue : ', voucherValue);
  console.log('### getPaymentDataFromRedux :: warrentyExtCharge : ', warrentyExtCharge);
  console.log('### getPaymentDataFromRedux :: reloadAmount : ', reloadAmount);
  console.log('### getPaymentDataFromRedux :: sameDayInstallationCharge : ', sameDayInstallationCharge);
  console.log('### getPaymentDataFromRedux :: stbPackPrice : ', stbPackPrice);
  console.log('### getPaymentDataFromRedux :: offerPriceValue : ', offerPriceValue);
  console.log('### getPaymentDataFromRedux :: unitPriceValue : ', unitPriceValue);
  console.log('### getPaymentDataFromRedux :: unitPriceAfterVoucerValue : ', unitPriceAfterVoucerValue);
  console.log('### getPaymentDataFromRedux :: dealerCollectionPayment : ', dealerCollectionPayment);
  console.log('### getPaymentDataFromRedux :: unitPriceAdjustment : ', unitPriceAdjustment);
  console.log('### getPaymentDataFromRedux :: purchasedPrice : ', purchasedPrice);
  console.log('### getPaymentDataFromRedux :: depositAmount : ', depositAmount);
  console.log('### =============================================================== ##');


  let totalPaymentProps = {
    customerTotalPayment : customerTotalPayment,
    outstandingFeeValue: outstandingFeeValue,
    deliveryInstallationCharge: deliveryInstallationCharge,
    unitPriceValue: unitPriceValue,
    voucherValue: voucherValue,
    warrentyExtCharge: warrentyExtCharge,
    reloadAmount: reloadAmount,
    sameDayInstallationCharge: sameDayInstallationCharge,
    depositAmount : depositAmount
  };


  let dealerCollectionPaymentProps = {

    dealerCollectionPayment : dealerCollectionPayment,
    outstandingFeeValue: outstandingFeeValue,
    unitPriceAdjustment: unitPriceAdjustment,
    offerPriceValue: offerPriceValue,
    purchasedPrice: purchasedPrice,
    unitPriceAfterVoucerValue: unitPriceAfterVoucerValue,
    deliveryInstallationCharge: deliveryInstallationCharge,
    sameDayInstallationCharge: sameDayInstallationCharge,
    warrentyExtCharge: warrentyExtCharge,
    depositAmount : depositAmount
  };
  
  console.log('### getPaymentDataFromRedux :: totalPaymentProps', totalPaymentProps);
  console.log('### getPaymentDataFromRedux :: dealerCollectionPaymentProps', dealerCollectionPaymentProps);

  return { totalPaymentProps , dealerCollectionPaymentProps };

};

const checkNumber = (number) => {
  console.log('xxx checkNumber :: number', number);
  if (number !== undefined && number !== null) {
    let numberString = number.toString().replace(/,/g, '');
    if (numberString !== "" && !isNaN(numberString)) {
      return parseFloat(numberString).toFixed(2);
    } else {
      return 0.00;
    }
   
  } else {
    return 0.00;
  }
};

export default {
  getPaymentDataFromRedux,
  checkNumber
};

