/*
 * File: EzCashBalance.js
 * Project: Dialog Sales App
 * File Created: Monday, 26th November 2018 7:14:18 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 26th November 2018 7:14:57 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Styles from '../../config/styles';
import Colors from '../../config/colors';
import Utills from '../../utills/Utills';

const Utill = new Utills();

class EzCashBalance extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>   
        <View style={styles.containerWrapper}>
          <View style={styles.row_main}>
            <View style={styles.elementLeftView}>
              <Text style={styles.totalPayment}>{this.props.title}</Text>
            </View>
            <View style={styles.elementRightView1}>
              <View style={styles.elementRightInnerView3}></View>
              <View style={styles.elementRightInnerView1}>
                <Text style={styles.balanceValue}>{this.props.rs_label} </Text>
              </View>
              <View style={styles.elementRightInnerView2}>
                <Text style={styles.balanceValue}>{Utill.numberWithCommas(Utill.getCorrectedNumber(this.props.ezCashAccountBalance))}</Text>
              </View>
            </View>
          </View>         
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.appBackgroundColor,
    marginBottom: 7,
    marginTop: 10,
  },
  containerWrapper: {
    flex: 1,
    marginRight: 2,
    flexDirection: 'column',
    borderColor: Colors.borderLineColor
  },
  row_main: {
    flexDirection: 'row',
    flex: 1
  },
  totalPayment: {
    fontWeight: '400',
    color: Colors.colorGrey,
    fontSize: Styles.otpEnterModalFontSize
  },
  balanceValue: {
    textAlign: 'right',
    fontWeight: '400',
    color: Colors.colorBlack,
    fontSize: Styles.otpEnterModalFontSize
  },
  elementLeftView: {
    flex: 0.6
  },
  elementRightView1: {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  }, elementRightInnerView1: {
    flex: 0.2,
    alignItems: 'flex-start',
  },
  elementRightInnerView2: {
    flex: 0.7,
    alignItems: 'flex-end',
  },
  elementRightInnerView3: {
    flex: 0.1,
    alignItems: 'flex-start',
  }
});

export default EzCashBalance;