/*
 * File: GeneralModalAlert.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 31st October 2018 3:21:24 pm
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import Button from '@Components/others/Button';
import Colors from '../../../config/colors';
import Styles from '../../../config/styles';
const { width, height } = Dimensions.get('window');
import Utills from '../../../utills/Utills';
import strings from '../../../Language/DtvActivation';


const Utill = new Utills();

export default class GeneralModalAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expandSwitch: false,
      arrowDown: 'md-arrow-dropdown',
     
    };
  }

  render() {
    const { 
      modalContainerCustomStyle = {},
      disabled= false,
      hideTopImageView = false,  
      title,
      descriptionTitle = 'TEst title',
      description = '', 
      customDescriptionTextView = false,   
      primaryPress,
      primaryPressPayload ={} ,
      primaryText,
      secondaryPress,
      secondaryPressPayload = {},
      secondaryText,
      additionalProps,
      titleTextStyle = {}, 
      topTitleContainerStyle = {}, 
      descriptionTextStyle = {}, 
      descriptionTitleTextStyle = {},
      descriptionTitleStyle = {},
      eoafSerialTitleTextStyle = {},
      eoafSerialTitle = {},
      eoafSerialvalue = {},
      primaryButtonStyle = {}, 
      secondaryButtonStyle = {} ,
    } = this.props;

    const TopImageView = () => {
      if (!hideTopImageView) {
        return (<View style={styles.topImageContainerStyle}>
          <Image
            resizeMode="contain"
            resizeMethod="scale"
            style={styles.titleImageStyle}
            source={this.props.icon}
          />
        </View>);
      } else if (title) {
        return (<View style={[styles.topTitleContainer, topTitleContainerStyle]} >
          <Text style={[styles.titleTextStyle, titleTextStyle]}> {title} </Text>
        </View>);
      }  else {
        return true;
      }  
    };

    const DescriptionTitleView = () => {
      if (descriptionTitle) {
        return ( <View style={[ styles.descriptionTitleContainer, descriptionTitleStyle] }>
          <Text textAlign="center" style={[styles.descriptionTitleTextStyle, descriptionTitleTextStyle]}>{description}</Text>
        </View>);
      } else { 
        return true;
      }
    };

    const DescriptionTextView = () => {
      if (customDescriptionTextView) {
        return true;
      } else {
        return (
          <View style= {styles.serialContainerView}>
            <View style={[ styles.serialContainer_first_column, descriptionTitleStyle] }>
              <Text textAlign="left" style={[styles.eoafSerialTitleTextStyle, eoafSerialTitleTextStyle]}>{eoafSerialTitle}</Text>
            </View>
        
            <View style={[ styles.serialContainer_second_column, descriptionTitleStyle] }>
              <Text textAlign="left" style={[styles.descriptionTitleTextStyle, descriptionTitleTextStyle]}>{eoafSerialvalue}</Text>
            </View>
          </View>
        );
      }   
    };

    const UnitPriceAdjustmentTextView = () => {
      if (customDescriptionTextView) {
        return true;
      } else {
        return (
          <View style= {styles.descriptionContainer}>
            {/*Description view*/}
            {/* {UnitPriceAdjustmentTextView()} */}
            <View style={styles.subRow}>
              <View style={styles.elementLeftView}>
                <Text style={styles.cl2}>{this.props.locals.unitPriceAdjustment}</Text>
              </View>
              <View style={styles.elementRightView1}>
                <View style={styles.elementRightInnerView3}/>
                <View style={styles.elementRightInnerView1}>
                  <Text style={styles.textStyleRight}>{this.props.locals.rs} </Text>
                </View>
                <View style={styles.elementRightInnerView2}>
                  <Text style={styles.textStyleRight}>{Utill.numberWithCommas(Utill.checkNumber(this.props.dealerCollectionPaymentProps.unitPriceAdjustment))}</Text>
                </View>
              </View>
            </View>
            <View style={styles.subRow}>
              <View style={styles.elementLeftView}>
                <Text style={[styles.subCatText, styles.lightTextClr]}>{this.props.locals.purchased_price}</Text>
              </View>
              <View style={styles.elementRightView1}>
                <View style={styles.elementRightInnerView3}/>
                <View style={styles.elementRightInnerView1}>
                  <Text style={[styles.textStyleRight, styles.lightTextClr]}>{this.props.locals.rs} </Text>
                </View>
                <View style={styles.elementRightInnerView2}>
                  <Text style={[styles.textStyleRight, styles.lightTextClr]}>{Utill.numberWithCommas(Utill.checkNumber(this.props.dealerCollectionPaymentProps.purchasedPrice))}</Text>
                </View>
              </View>
            </View>
            <View style={styles.subRow}>
              <View style={styles.elementLeftView}>
                <Text style={[styles.subCatText, styles.lightTextClr]}>{this.props.locals.selling_price}</Text>
              </View>
              <View style={styles.elementRightView1}>
                <View style={styles.elementRightInnerView3}/>
                <View style={styles.elementRightInnerView1}>
                  <Text style={[styles.textStyleRight, styles.lightTextClr]}>{this.props.locals.rs} </Text>
                </View>
                <View style={styles.elementRightInnerView2}>
                  <Text style={[styles.textStyleRight, styles.lightTextClr]}>{Utill.numberWithCommas(Utill.checkNumber(this.props.dealerCollectionPaymentProps.unitPriceAfterVoucerValue))}</Text>
                </View>
              </View>
            </View>
          </View>
        );
      }   
    };

    return (
      <View style={styles.screenContainer}>
        <View style={[styles.modalContainer, modalContainerCustomStyle]}>
          {/*Top image view*/}
          {TopImageView()}
          {/*Description Title view*/}
          {DescriptionTitleView()}
          <View style={styles.descriptionContainer}>
            {/*Description view*/}
            {DescriptionTextView()}
          </View>
         
          {UnitPriceAdjustmentTextView()}

          <View style={styles.buttonContainer}>
            {secondaryText ? <Button children={secondaryText}
              childStyle={[styles.leftButtonStyle, secondaryButtonStyle]}
              ContainerStyle={styles.leftButtonContainerStyle}
              onPress={() =>secondaryPress(secondaryPressPayload)}
            /> : null}
            <Button children={primaryText}
              childStyle={[styles.rightButtonStyle, primaryButtonStyle]}
              ContainerStyle={styles.rightButtonContainerStyle}
              disabled={disabled}
              onPress={() =>primaryPress(primaryPressPayload)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.modalOverlayColorLow,
   
  },
  modalContainer: {
    height: 'auto',
    backgroundColor: Colors.colorWhite,
    width: width * 0.9,
    shadowColor: Colors.black,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 2 },
    elevation: 2,
    paddingHorizontal: 10,
  },

  topImageContainerStyle: {
    height: 64,
    marginBottom : 5,
    marginTop: 5,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  topTitleContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 5,  
  },

  descriptionContainer: {
    height: 'auto',
    justifyContent: 'center',
    paddingHorizontal: 15,
    // paddingTop: 16,
    paddingBottom: 16,
  },

  serialContainerView: {
    // flex : 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  serialContainer_first_column: {
    // color: Colors.generalModalTextColor,
    flex: 0.4
  },
  serialContainer_second_column: {
    // color: Colors.generalModalTextColor,
    flex: 0.6
  },

  descriptionTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    padding: 5,
    marginBottom: 10
  },

  buttonContainer: {
    height: 'auto',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 16,
    paddingBottom: 10,
    paddingRight: 10, 
  },

  titleImageStyle: {
    width: 54,
    height: 54
  },

  titleTextStyle: {
    fontSize: Styles.generalModalTitleTextSize,
    fontWeight:'500',
    color: Colors.colorBlack,
  },

  descriptionTitleTextStyle: {
    fontSize: Styles.otpModalFontSize,
    color: Colors.colorBlack,
  },

  eoafSerialTitleTextStyle: {
    fontSize: Styles.thumbnailTxtFontSize,
    color: Colors.colorGrey,
  },

  leftButtonStyle: {
    color: Colors.colorDarkOrange,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  },
  leftButtonContainerStyle: {
    paddingRight: 20,
    padding: 5
  },
  rightButtonContainerStyle : {
    paddingVertical: 5
  },
  rightButtonStyle: {
    color: Colors.colorGreen,
    fontSize: Styles.generalModalButtonTextSize,
    fontWeight:'500'
  },
  subRow: {
    // flex: 1,
    flexDirection: 'row',
    marginTop: 10
  },
  cl2: {
    flexDirection: 'row',
    flex: 1.5,
    color: Colors.colorBlack,
    fontSize: Styles.otpModalFontSize,
    alignItems: 'center',
    justifyContent: 'center',
  },
  elementLeftView : {

    flex: 0.6
  },
  elementRightView1 : {
    flex: 0.4,
    alignItems: 'flex-start',
    flexDirection: 'row'

  },
  elementRightInnerView1 : {
    flex: 0.2,
    alignItems: 'flex-start'
  },
  elementRightInnerView2 : {
    flex: 0.7,
    alignItems: 'flex-end'
  },
  elementRightInnerView3 : {
    flex: 0.1,
    alignItems: 'flex-start'
  },
  textStyleRight : {
    fontSize: Styles.otpModalFontSize,
    color: Colors.colorGrey
  },
  lightTextClr:{
    color: Colors.lightTextGrey
  },
  subCatText: {
    flexDirection: 'row',
    marginLeft: 10,
    flex: 1.5,
    color: Colors.lightTextGrey,
    fontSize: Styles.otpModalFontSize
  }
});
