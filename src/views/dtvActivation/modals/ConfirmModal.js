import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Styles from '../../../config/styles';
import Colors from '../../../config/colors';
import strings from '../../../Language/DtvActivation.js';
import Orientation from 'react-native-orientation';
import { Header } from '../../common/components/Header';
import { Constants } from '../../../config';
import Utills from '../../../utills/Utills';
import { BackHandler } from 'react-native';

const Utill = new Utills();

class ConfirmModal extends React.Component {

  constructor(props) {
    super(props);
    strings.setLanguage(this.props.language);
    this.state = {
      tncDownloadUrl: 'https://www.dialog.lk/dialogdocroot/content/pdf/tc/postpaid-application-form-mob' +
        'ile.pdf',

      locals: {
        confirmButtonTxt: strings.confirmButtonTxt,
        cancelButtonTxt: strings.cancelButtonTxt,
        modalTitle: strings.modalTitle,
        totalPayment: strings.totalPayment,
        byclicking: strings.byclicking,
        pPlan:strings.pPlan,
        pRental:strings.pRental,
        additionalChannels:strings.additionalChannels,
        additionalRental:strings.additionalRental,
        totalRental:strings.totalRental,
        fulfillment:strings.fulfillment,
        packType:strings.packType,
        offer:strings.offer,
        voucherCode:strings.voucherCode,
        freeVisitCount:strings.freeVisitCount,
        stbType:strings.stbType,
        stbWarranty:strings.stbWarranty,
        rcuWarranty:strings.rcuWarranty,
        pcuWarranty:strings.pcuWarranty,
        lnbWarranty:strings.lnbWarranty,
        lblWarrantyExtension:strings.lblWarrantyExtension,
        mobileContactNo:strings.mobileContactNo,
        landlineContactNo:strings.landlineContactNo,
        email:strings.email,
        installationAddress:strings.installationAddress,
        deliveryAndInstallation:strings.deliveryAndInstallation,
        outstanding: strings.outstanding,
        unitPrice: strings.unitPrice,
        depositAmount: strings.depositAmount,
        voucher: strings.voucher,
        warrantyExtension: strings.warrantyExtension,
        reload: strings.reload,
        sameDayInstallation: strings.sameDayInstallation,
        title: strings.title,
        package: strings.package,
        upgradeEligibility: strings.upgradeEligibility,
        lblPrePaid: strings.lblPrePaid,
        lblPostPaid: strings.lblPostPaid
      }
    };
  }

  componentWillMount(){
    Orientation.lockToPortrait();
  }

  getAdditionalChannelsValue = () =>{
    const { selected_channel_n_packs = {} } = this.props;
    let additionalChannelsValue = '';

    try {
      const channels = selected_channel_n_packs.channels;
      const packs = selected_channel_n_packs.packs;

      const hasBothPacksAndChannels = channels.length > 0 && packs.length > 0;
      const hasChannels = channels.length > 0;
      const hasOnlyPacks = channels.length == 0 && packs.length > 0;

      if (hasOnlyPacks){
        console.log("Confirm Modal::: In hasOnlyPacks");
        console.log("Confirm Modal::: packs:", packs);
        for (let i=0; i<packs.length; i++){
          if (i == packs.length-1){
            if (i == 0){
              console.log("Confirm Modal::: 1");
              additionalChannelsValue = additionalChannelsValue + packs[i].pack_name;
            } else {
              console.log("Confirm Modal::: 2");
              additionalChannelsValue = additionalChannelsValue + ' & ' + packs[i].pack_name;
            }
          } else {
            if (i == 0){
              console.log("Confirm Modal::: 3");
              additionalChannelsValue = additionalChannelsValue + packs[i].pack_name;
            } else {
              console.log("Confirm Modal::: 4");
              additionalChannelsValue = additionalChannelsValue + ' , ' + packs[i].pack_name;
            }
          }
        }
      } 
      else {
        if (hasChannels) { 
          console.log("Confirm Modal::: In hasOnlyChannels");
          console.log("Confirm Modal::: channels", channels);
          for (let i=0; i<channels.length; i++){
            if (i == channels.length-1){
              if (i == 0){
                console.log("Confirm Modal::: 5");
                additionalChannelsValue = additionalChannelsValue + channels[i].name;
              } else {
                console.log("Confirm Modal::: 6");
                if (hasBothPacksAndChannels)
                  additionalChannelsValue = additionalChannelsValue + ' , ' + channels[i].name;
                else {
                  console.log("Confirm Modal::: 7");
                  if (hasBothPacksAndChannels)
                    additionalChannelsValue = additionalChannelsValue + ' , ' + channels[i].name;
                  else
                    console.log("Confirm Modal::: 8");
                  additionalChannelsValue = additionalChannelsValue + ' & ' + channels[i].name;
                }
              }
            } else {
              if (i == 0){
                console.log("Confirm Modal::: 9");
                additionalChannelsValue = additionalChannelsValue + channels[i].name;
              } else {
                console.log("Confirm Modal::: 10");
                additionalChannelsValue = additionalChannelsValue + ' , ' + channels[i].name;
              }
            }
          }
        // additionalChannelsValue = channels.map
        }
        if (hasBothPacksAndChannels){
          console.log("Confirm Modal::: In hasBothPacksAndChannels");
          console.log("Confirm Modal::: packs:", packs);
          for (let i=0; i<packs.length; i++){
            if (i == packs.length-1){
              if (i == 0){
                console.log("Confirm Modal::: 11");
                additionalChannelsValue = additionalChannelsValue + ' & ' +packs[i].pack_name;
              } else {
                console.log("Confirm Modal::: 12");
                additionalChannelsValue = additionalChannelsValue + ' & ' + packs[i].pack_name;
              }
            } else {
              if (i == 0){
                console.log("Confirm Modal::: 13");
                additionalChannelsValue = additionalChannelsValue + ' , ' +packs[i].pack_name;
              } else {
                console.log("Confirm Modal::: 14");
                additionalChannelsValue = additionalChannelsValue + ' , ' + packs[i].pack_name;
              }
            }
          }
        }
      }

    } catch (e){
      console.log(e);
    }

    return additionalChannelsValue;
  }

  handleBack = () => {
    console.log("handleBack called");
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }

  addRupeesMask(value){
    try {
      if (Number.isNaN(Number.parseFloat(value)))
        return '';
      if (Number.parseInt(value) == 0)
        return '';
      return 'Rs '+ Utill.getCorrectedNumber(value);
    } catch (e){
      console.log('addRupeesMask exception: ' + e);
    }
  }

  addTaxMask(value, paymentPlan){
    try {
      if (Number.isNaN(Number.parseFloat(value)))
        return '';
      if (Number.parseInt(value) == 0)
        return '';
      if (paymentPlan == Constants.PREPAID)
        return this.addRupeesMask(value) + ' + Tax daily';
      return this.addRupeesMask(value) + ' + Tax monthly';
    } catch (e){
      console.log('addRupeesMask exception: ' + e);
    }
  }

  formatAddress (installationAddress, opt_sameDayInstallation){
    console.log("In formatAddress: ", installationAddress);
    console.log("installationAddress.customer_address_line1: ", installationAddress.customer_address_line1);
    console.log("In formatAddress opt_sameDayInstallation: ", opt_sameDayInstallation);
    let address = '';
    let customer_address_line3 = '';
    if (installationAddress.customer_address_line3){
      customer_address_line3 = '\n'+installationAddress.customer_address_line3;
    }

    try {
      address = opt_sameDayInstallation == true ? 
        installationAddress.customer_address_line1+
      '\n'+
      installationAddress.customer_address_line2+
      customer_address_line3 +
      '\n'+
      Utill.toFirstLetterUpperCase(installationAddress.customer_city) : '';
    } catch (e){
      console.log("formatAddress exception: ", e);
    }
    return address;

  }

  getLocalizedPaymentPlanString(paymentPlan){
    if (paymentPlan == Constants.PREPAID){
      return this.state.locals.lblPrePaid;
    }
    return this.state.locals.lblPostPaid;
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);

    try {
      console.log('Fulfilment type', this.props.selectedFullFillmentType);
      console.log('Props', this.props);
      const { 
        selectedDTVDataPackageDetails: { 
          package_name = '', 
          package_rental = '' },
        selected_channel_total_package_rental: {
          additionalMonthlyRental = 0,
          totalDailyRental = 0
        },
        selectedFullFillmentType:{
          selectedValue:ffType=''
        },
        dtv_pack_type:{
          selectedValue: selectedPackType=''
        },
        availableDtvOffers:{
          offer_name=''
        },
        enteredDTVVoucherCode = '',
        dtv_warranty_extensions:{
          time_period: warrantyExtension = '',
          packPrice: warrantyExtCharge = '',
        },
        installationAddress = {
          customer_address_line1: '',
          customer_address_line2: '',
          customer_address_line3: '',
          customer_city: ''
        },
        dtvMobileNumber ='',
        customerLandline = '',
        email_input_value = '',
        reloadAmountValue = '',
        data_totalPaymentProps:{
          customerTotalPayment = '',
          outstandingFeeValue = '',
          deliveryInstallationCharge = '',
          unitPriceValue = '',
          voucherValue = '',
          sameDayInstallationCharge = '',
          depositAmount = ''
        },
        opt_sameDayInstallation = false,
        stb_upgrade_available:{
          stb_type = '',
          free_visits_count = '',
          rcu_warranty = '',
          pcu_warranty ='',
          stb_warranty = '',
          lnb_warranty = '',
          stb_upgrade_available=false,
          default_stb_type = ''
        },
        stb_upgrade_checked = false
      } = this.props;

      let stbType = stb_type;

      if (stb_upgrade_checked == false && stb_upgrade_available == true){
        stbType = default_stb_type;
      }

      // let stbType = stb_upgrade_checked == true ? "Hybrid" : '';

      this.setState({ orderDetails: [{
        data: this.props.local_foreign == Constants.CX_TYPE_LOCAL ?
          strings.nicNo : strings.ppNo,
        value: this.props.idNumber
      }, 
      {
        value: this.getLocalizedPaymentPlanString(this.props.pPlan),
        data:  this.state.locals.pPlan
      },
      {
        value: package_name,
        data: this.state.locals.package
      },
      {
        value: this.addTaxMask(package_rental, this.props.pPlan),
        data: this.state.locals.pRental
      },
      {
        value: this.getAdditionalChannelsValue(),
        data: this.state.locals.additionalChannels
      },
      {
        value: this.addTaxMask(additionalMonthlyRental, this.props.pPlan),
        data: this.state.locals.additionalRental
      },
      {
        value: this.addTaxMask(totalDailyRental, this.props.pPlan),
        data: this.state.locals.totalRental
      },
      {
        value: ffType,
        data: this.state.locals.fulfillment
      },
      {
        value: selectedPackType,
        data: this.state.locals.packType
      },
      {
        value: offer_name,
        data: this.state.locals.offer
      },
      {
        value: enteredDTVVoucherCode,
        data: this.state.locals.voucherCode
      },
      {
        value: free_visits_count,
        data: this.state.locals.freeVisitCount
      },
      {
        value: stbType,
        data: this.state.locals.stbType
      },
      {
        value: stb_warranty,
        data: this.state.locals.stbWarranty
      },
      {
        value: rcu_warranty,
        data: this.state.locals.rcuWarranty
      },
      {
        value: pcu_warranty,
        data: this.state.locals.pcuWarranty
      },
      {
        value: lnb_warranty,
        data: this.state.locals.lnbWarranty
      },
      {
        value: warrantyExtension,
        data: this.state.locals.lblWarrantyExtension
      },
      {
        value: dtvMobileNumber,
        data: this.state.locals.mobileContactNo
      },
      {
        value: customerLandline,
        data: this.state.locals.landlineContactNo
      },
      {
        value: email_input_value,
        data: this.state.locals.email
      },
      {
        value: this.formatAddress(installationAddress, opt_sameDayInstallation),
        data: this.state.locals.installationAddress
      },
      {
        value: this.addRupeesMask(outstandingFeeValue),
        data: this.state.locals.outstanding
      },
      {
        value: this.addRupeesMask(deliveryInstallationCharge),
        data: this.state.locals.deliveryAndInstallation
      },
      {
        value: this.addRupeesMask(unitPriceValue),
        data: this.state.locals.unitPrice
      },
      {
        value: this.addRupeesMask(depositAmount),
        data: this.state.locals.depositAmount
      },
      {
        value: this.addRupeesMask(voucherValue),
        data: this.state.locals.voucher
      },
      {
        value: this.addRupeesMask(warrantyExtCharge),
        data: this.state.locals.lblWarrantyExtension
      },
      {
        value: this.addRupeesMask(reloadAmountValue),
        data: this.state.locals.reload
      },
      {
        value: this.addRupeesMask(sameDayInstallationCharge),
        data: this.state.locals.sameDayInstallation
      },
      {
        value: this.addRupeesMask(customerTotalPayment),
        data: this.state.locals.totalPayment
      }
      ] });
    } catch (e){
      console.log('Confirm modal didmount exception', e);
    }
  }

  dismissModal = () => {
    const navigator = this.props.navigator;
    navigator.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  handleBackButtonClick = ()=>{
    this.dismissModal();
  }

  openSignature = () => {
    console.log('Open Signature');
    Orientation.lockToLandscape();
    const navigatorOb = this.props.navigator;

    navigatorOb.push({
      title: '',
      screen: 'DialogRetailerApp.views.SignaturePadModule',
      passProps: {
        onReturnView: 'DialogRetailerApp.views.DtvActScreen',
        tncUrl: this.props.tncUrl,
        confirmModal: ()=>this.dismissModal()
      },
      overrideBackPress: true
    });
  }

  buttonSet = () => {
    return (
      <View style={styles.buttonSetStyle}>
        <TouchableOpacity
          style={styles.cancelButtonStyle}
          onPress={() => this.dismissModal()}>
          <Text style={styles.textStyle}>
            {this.state.locals.cancelButtonTxt}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.confirmButtonStyle}
          onPress={() => this.openSignature()}>
          <Text style={styles.textStyle}>
            {this.state.locals.confirmButtonTxt}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  setTnCtext = () => {
    if (this.props.isExsistingCustomer) {
      return (
        <View style={styles.tncView}>
          <Text style={styles.desTncTxt}>{this.state.locals.byclicking}
          </Text>
          <Text
            style={styles.desTncTxtLink}
            onPress={() => Linking.openURL(this.state.tncDownloadUrl)}>
            {this.state.locals.tns}
          </Text>
        </View>
      );
    } else {
      return (<View/>);
    }
  }

  renderListItem = ({ item }) => (<ElementItem data={item.data} value={item.value}/>)

  render() {
    const {
      stb_upgrade_available:{
        lte_unit_eligibility = false
      },
      stb_upgrade_checked = false
    } = this.props;
    const finalPakageDetails = this.state.orderDetails;
    // const paymentData = this.state.paymentData;
    return (
      <View style={styles.containerOverlay}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title}/>
        <View style={styles.containerStyle}>
          <View style={styles.section_1_Container}>
            <Text style={styles.titleStyle}>{this.state.locals.modalTitle}</Text>
          </View>
          <View style={styles.section_2_Container}>
            <FlatList
              data={finalPakageDetails}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={true}/>
          </View>
          {stb_upgrade_checked == true || lte_unit_eligibility == true ? 
            <View style={styles.section_2_1_Container}>
              <Text style={styles.lteEligibilityLabel}>{this.state.locals.upgradeEligibility}</Text>
            </View>
            : null}
          {/* <View style={styles.section_3_Container}>
            <FlatList
              data={paymentData}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index}
              scrollEnabled={false}/>
          </View> */}
          {/* {this.setTnCtext()} */}
          <View style={styles.section_4_Container}>
            {this.buttonSet()}
          </View>
        </View>
      </View>
    );
  }
}

const ElementItem = ({ data, value }) => {
  if (value)
    return (
      <View style={styles.viewStyle}>
        <View style={styles.elementLeftItem}>
          <Text style={[styles.textStyle, styles.textStyleLeft]}>{data}</Text>
        </View>
        <View style={styles.elementrightItem}>
          <Text style={styles.textStyleRight}>{value}</Text>
        </View>
      </View>
    );
  return null;
};

const styles = {
  containerOverlay: {
    flex: 1,
    backgroundColor: Colors.modalOverlayColor,
    // padding: 5,
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    // marginBottom: 50,
    paddingLeft: 14,
    paddingRight: 14,
    // paddingTop: 20,
    // paddingBottom: 20,
    // marginTop: 20,
    backgroundColor: Colors.colorWhite
  },

  section_1_Container: {
    height:55,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  section_2_Container: {
    flex: 6
  },
  section_2_1_Container: {
    height:35,
    marginTop:12
  },
  section_3_Container: {
    flex: 3,
    marginTop: 10
  },
  section_4_Container: {
    flex: 1
  },

  elementLeftItem: {
    flex: 0.9,
    flexDirection:'column',
    justifyContent: 'flex-start',
    alignItems:'flex-start',
  },
  elementrightItem: {
    flex: 1,
    paddingLeft:10
  },
  viewStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 12
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack
  },
  textStyleButton: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    textAlignVertical: 'center'
  },
  textStyleRight: {
    fontSize: 16,
    color: Colors.colorBlack
  },
  textStyleLeft: {
    // fontWeight: 'bold',
    color: Colors.borderColorGray,
    fontSize:16,
    textAlignVertical: 'top'
  },
  titleStyle: {
    fontSize: 20,
    color: Colors.colorBlack,
    fontWeight: 'bold',
    textAlignVertical: 'center'
  },
  lteEligibilityLabel: {
    fontSize: 16,
    color: Colors.colorBlack,
    // fontWeight: 'bold',
    // textAlignVertical: 'center'
  },
  confirmButtonStyle: {
    backgroundColor: '#edc92c',
    alignSelf: 'stretch',
    justifyContent: 'center',
    borderRadius: 3,
    marginLeft: 5,
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
    height:45,
  },
  cancelButtonStyle: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    justifyContent: 'center',
    height:45,
    borderRadius: 3,
    marginLeft: 5,
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: 55,
    marginRight: 58
  },
  tncView: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5
  },
  desTncTxt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: Styles.otpEnterModalFontSize
  },
  desTncTxtLink: {
    color: Colors.urlLinkColor,
    fontSize: Styles.otpEnterModalFontSize,
    textDecorationLine: 'underline'
  }
};

const mapStateToProps = (state) => {
  console.log('****** REDUX STATE :: CONFIRM => MOBILE ACTIVATION', state.mobile);
  const language = state.lang.current_lang;
  // const local_foreign = 'local';
  // const idNumber= '9135585555v';
  // const connectionNo = '033123456';
  // const simNumber = '5855255855555';
  // const offer = 'Standard';
  // const dataPacks = 'Lite Plus';
  // const pkgRental = 'Rs 599';
  // const contactNumber = '0777123456';
  // const email = 'email';
  // const connectionFee = 'Rs 5000';
  // const depositAmount = 'Rs 300';
  // const totalPayment = 'RS 5300';


  return {
    language,
    // local_foreign,
    // idNumber,
    // connectionNo,
    // simNumber,
    // offer,
    // dataPacks,
    // pkgRental,
    // contactNumber,
    // email,
    // connectionFee,
    // depositAmount,
    // totalPayment
  };
};

export default connect(mapStateToProps, actions)(ConfirmModal);