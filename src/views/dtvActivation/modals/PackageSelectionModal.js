/*
 * Created on Fri Oct 05 2018
 *
* Copyright 2018 Omobio (PVT) Limited
 * File: PackageSelectionModal.js
 * Project: Dialog Sales App
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 */


import React from 'react';
import { View, Text, TouchableOpacity, ScrollView,Image, Dimensions, FlatList } from 'react-native';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import ActivityIndicator from '../../common/components/ActivityIndicator';
import { Header } from '../../common/components/Header';
import strings from '../../../Language/LteActivation';
import Styles from '../../../config/styles';
import _ from 'lodash';
import GridViewComponent from "../components/GridViewComponent";

class PackageSelectionModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      expandSwitch: [],
      arrowDown: 'md-arrow-dropdown',
      activeSections: [],
      idx: '',
      value: '',
      locals: {
        Continue: strings.btnContinue,
        title: 'DTV ACTIVATION',
        pakage: 'Package'
      }
    };
  }


  componentDidMount(){
    console.log('selected idx', this.props.idx);
    console.log('selected value', this.props.value);
    {/* Adding dropdown arrow as per listItem */}
    console.log("In didmount. selectedDTVDataPackageDetails: ", JSON.stringify(this.props.selectedDTVDataPackageDetails));
    let selectedDTVDataPackageDetails = this.props.selectedDTVDataPackageDetails;
    if (selectedDTVDataPackageDetails == undefined || selectedDTVDataPackageDetails == null || selectedDTVDataPackageDetails == '' ) {
      return;
    }

    let { genres=[] } = selectedDTVDataPackageDetails;
    if (genres.length >0){
      let expandSwitch = [];
      for (var i=0;i<genres.length;i++){
        expandSwitch.push({ id:i, isHidden: false });
      }
      this.setState({ expandSwitch:expandSwitch });
    }

    this.setState({ idx: this.props.idx });
    this.setState({ value: this.props.value });
  }

  componentWillReceiveProps(nextprops){
    {/* Adding dropdown arrow as per listItem */}
    let selectedDTVDataPackageDetails = nextprops.selectedDTVDataPackageDetails;
    if (selectedDTVDataPackageDetails == undefined || selectedDTVDataPackageDetails == null || selectedDTVDataPackageDetails == '' ) {
      return;
    }
    console.log("In willreceiveprops. selectedDTVDataPackageDetails: ", JSON.stringify(nextprops.selectedDTVDataPackageDetails));
    let { genres=[] } = selectedDTVDataPackageDetails;
    if (genres.length >0){
      let expandSwitch = [];
      for (var i=0;i<genres.length;i++){
        expandSwitch.push({ id:i, isHidden: false });
      }
      this.setState({ expandSwitch:expandSwitch });
    }
  }


  expandDetails = (index) => {
    console.log("this.state.expandSwitch", this.state.expandSwitch);
    let expandSwitch = this.state.expandSwitch;
    for (var i = 0; i < expandSwitch.length; i++){
      if (i == index) {
        continue;
      }
      expandSwitch[i].isHidden = false;
    }
    
    expandSwitch[index].isHidden = !expandSwitch[index].isHidden;
    if (this.state.expandSwitch) {
      this.setState({ expandSwitch: expandSwitch });
    
    }
  }

  handleBackButtonClick = () => {

    let { idx=undefined } = this.props;
    
    if (idx !== -1) {
      
      this.dismissModal();
      this.props.onSelect(this.state.idx, this.state.value, this.props.modalType);
    } else {
      this.dismissModal();
    }
    
  }

  dismissModal = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  
  renderListItem = ({ item, index }) => {
    console.log("this.state.expandSwitch.length", this.state.expandSwitch.length);
    console.log("this.state.expandSwitch.length", this.state.expandSwitch);
    {/*Package List View Starts*/}
    if (this.state.expandSwitch.length < 1) return;
    return (
      <View style={styles.listContainer}>
        <TouchableOpacity style={styles.viewStyle} onPress={()=>this.expandDetails(index)}>
          <View style={styles.elementLeftItem}>
            <Text style={[styles.leftTextFont]}>{item.genre_name}</Text>
          </View>
          <View style={styles.elementRightItem}>
            <Text style={styles.textStyle}>{item.noOfChannels}</Text>
          </View>
          <View style={styles.elementRightItemExpandSwitch}>

            <Ionicons name={this.state.expandSwitch[index].isHidden ? 'md-arrow-dropup' : 'md-arrow-dropdown'} size={20}></Ionicons> 
          </View>
     
        </TouchableOpacity>
        {/*Expanding View Starts*/}
        {this.state.expandSwitch[index].isHidden ?  
        
          <GridViewComponent gridItem ={item.channels} renderListItem = { renderListItem2} />
          :
          <View />  
        }
        {/*Expanding View Ends*/}
     
      </View>
    );
    // Package List View Starts
  }


  //if package drop down selected this function will trigger
  onSelect = (idx, value) => {
    console.log('onDataPackageInfoDropdownSelect', idx, value);
    // console.log('Selected packageList',  this.props.availableDTVDataPackagesList);
    // let packageList = this.props.availableDTVDataPackagesList.packageList;
    // console.log('Selected packageList', packageList);
    // let selectedPackageCode = packageList[idx].package_code;
    // console.log('Selected Package Code', selectedPackageCode);

    // let selectedData = {
    //   dropDownData: _.toArray(_.mapValues(packageList, 'package_name')),
    //   data_package_code: selectedPackageCode,
    //   selectedValue: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name,
    //   defaultIndex: _.findIndex(packageList, ['package_code', selectedPackageCode]),
    //   packageList: packageList
    // };
    // let requestParams = {

    //   "conn_type": "POSTPAID",
    //   "cx_identity_no": "868153211V",
    //   "cx_identity_type": "NIC",
    //   "package_code":selectedPackageCode

    // };
    // console.log(requestParams);
    // console.log('onDataPackageInfoDropdownSelect :: Selected Data Ob', selectedData);
    // this.props.getDtvPackageDetails(requestParams, selectedData);

  }


  render() {

    const {
      dropDownData = [],
      defaultIndex = -1,
      onSelect=this.onSelect,
      selectedValue = ''
    } = this.props;
    
    let selectedVal = selectedValue;
    let error = undefined;
    let selectedDTVDataPackageDetails  = this.props.selectedDTVDataPackageDetails;
    selectedVal = this.props.availableDTVDataPackagesList.selectedValue;
  
    if (selectedDTVDataPackageDetails !== undefined){
      console.log('finalOfferDetails',selectedDTVDataPackageDetails);

      error = selectedDTVDataPackageDetails.error !== undefined ? selectedDTVDataPackageDetails.error : undefined;
    }
    return (
  
      <View style={styles.containerOverlay}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title} />
        {/*loading indicator*/}
        <ActivityIndicator animating={this.props.apiLoading}/>
        <View style={styles.section_1_Container}>
        
          <View style={styles.containerMainView}>
            <ScrollView>
              <View style={styles.containerMainView}>
                <View style={styles.firstColumnView}>
                  <View style={styles.firstColumnView_1_Container}>
                    <Text style = {styles.leftTextFont}> {this.state.locals.pakage}</Text>
                  </View>
                  <View style={styles.firstColumnView_2_Container}>
                    <View style={styles.modalDropdownView}>
                      <ModalDropdown
                        options={dropDownData}
                        onSelect={(idx,value)=>{ onSelect(idx,value, this.props.modalType); }}
                        defaultIndex={parseInt(defaultIndex)}
                        style={styles.modalDropdownStyles}
                        dropdownStyle={dropDownData.length<5 ? [styles.dropdownStyle, { height: 'auto' }] : styles.dropdownStyle}
                        // dropdownStyle={ styles.dropdownStyle}
                        dropdownTextStyle={styles.dropdownTextStyle}
                        dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
                        <View style={styles.dropdownElementContainer}>
                          <View style={styles.dropdownDataElemint}>
                            <Text ellipsizeMode ='tail' numberOfLines={3} style={styles.dropdownDataElemintTxt}>{selectedVal}</Text>
                          </View>
                          <View style={styles.dropdownArrow}>
                            <Ionicons name='md-arrow-dropdown' size={20}/>
                          </View>
                        </View>
                      </ModalDropdown>
                    </View>
                  </View>
                </View>
                {selectedVal !== '' ? 
                  <View style={error !== undefined? styles.errorViewContainer :   styles.SecondColumnView}>
                    { error ? 
                    //Error View
                      // <View style={ styles.errorView }>
                      //   <Text style={ styles.errorText }>{ error}</Text>
                      // </View> 
                      this.dismissModal()
                      :
                    // If error is undefined, show details list
                      <FlatList
                        data={selectedDTVDataPackageDetails.genres}
                        renderItem={this.renderListItem}
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled={false}
                        extraData={this.state}
                      />
                    }
            
                  </View>
                  :
                  <View/>
                } 
              </View>
            </ScrollView>
          </View>
            
        </View>
        {/*Button Starts*/}
        <View style={styles.section_2_Container}>
          <View style={styles.buttonSetStyle}>
            <TouchableOpacity
              onPress={() => this.dismissModal()}
              style={styles.confirmButtonStyle}>
              <Text style={styles.ConfirmAndCancelTextStyle}>
                {this.state.locals.Continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/*Button Ends*/}
      </View>

    );
  }
}

const renderListItem2 = ({ item }) => {
  console.log('renderListItem2', item);
  return (
    <View style={styles.gridContainer}>
      <View style={styles.gridChannelImageContainer}>
        <Image source={{ uri: item }} style={styles.item}></Image>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const { availableDTVDataPackagesList =[] } = state.dtvActivation;
  const { selectedDTVDataPackageDetails = [] } = state.dtvActivation;
  const apiLoading = state.dtvActivation.apiLoading;
  
  return { 
    Language,
    availableDTVDataPackagesList,
    selectedDTVDataPackageDetails,
    apiLoading
  };
};



const styles = {
  containerOverlay: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white
  },
  containerMainView: {
    flex: 1,
    backgroundColor: Colors.white
  },
  firstColumnView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  SecondColumnView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 5,
  },
  firstColumnView_1_Container: {
    flex: 0.22,
    marginLeft: 10,
    justifyContent: 'center',
  },
  firstColumnView_2_Container: {
    flex: 0.78
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor,
    height: 60
  },
  gridviewviewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12,
    height: 'auto'
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },
  leftTextFont:{
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
  },
  modalDropdownView: {
    flexDirection: 'row',
  },

  modalDropdownStyles: {
    width: '100%',
  },

  dropdownStyle: {
    width: (Dimensions
      .get('window')
      .width - (Dimensions
      .get('window')
      .width * 0.3 + 15)),
    height: 160,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },

  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint,
    marginLeft: 10
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    // marginLeft: 15,
    //marginRight: 15,
    padding: 5,
    borderWidth: 1,
    borderColor: Colors.borderColor
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
    // backgroundColor: 'red'
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 5,
    // marginLeft: 10,
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint
  },
  elementLeftItem: {
    flex: 0.5,
    marginLeft: 10
  },
  elementRightItem: {
    flex: 0.4,
    alignItems: 'flex-end',
    //backgroundColor:'red'

  },
  elementRightItemExpandSwitch:{
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'

  },
  listContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  gridContainer: {

    width: Dimensions.get('window').width / 3 - 12,
    height: 70,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderRadius: 1,
    marginRight: 10,
    marginBottom: 8,
  },

  gridChannelImageContainer : {
    height: 60,
  },
  item: {
    height: 46,
    justifyContent: 'center',
    resizeMode:'cover',
    margin: 8,  
    
  },
  section_2_Container: {
    // flex: 1,
    marginTop: 10,
    // backgroundColor: 'red'
    height: 56

  }, 
  section_1_Container: {
    flex: 1,
    // marginBottom: 5,
    // backgroundColor: 'red'

  }, 
  confirmButtonStyle: {
    backgroundColor: '#FFC400',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginRight: 20,
    padding: 10

  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  ConfirmAndCancelTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold'
  },
  errorViewContainer:  { 
    flex:10,
    paddingTop: 10,
    flexDirection: 'column',
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center'
  },
  errorView:{ 
    flex:1, 
    justifyContent:'center', 
    alignItems:'center' 
  },
  errorText:{ 
    fontSize: 15, 
    fontWeight: '500',
    marginBottom: 15 
  }


};

export default connect(mapStateToProps, actions)(PackageSelectionModal);
