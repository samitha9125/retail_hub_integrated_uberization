/*
 * Created on Fri Oct 05 2018
 *
* Copyright 2018 Omobio (PVT) Limited
 * File: OfferSelectionModal.js
 * Project: Dialog Sales App
 * Author: Prasad Jayahanka
 */

import React from 'react';
import { View, Text, TouchableOpacity, ScrollView, Dimensions, FlatList } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import Colors from '../../../config/colors';
import ActivityIndicator from '../../common/components/ActivityIndicator';
import { Header } from '../../common/components/Header';
import strings from '../../../Language/LteActivation';
import Styles from '../../../config/styles';
import Utills from '../../../utills/Utills';
import { Analytics } from '@Utils';

const Utill = new Utills();

class OfferSelectionModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModel: false,
      selectedItem:{},
      selectedIndex:-1,
      expandSwitch: [],
      arrowDown: 'md-arrow-dropdown',
      activeSections: [],
      locals: {
        Continue: strings.btnContinue,
        offer_price: 'Offer price',
        title: 'OFFERS',
        pakage: 'Package',
        rs_label: 'Rs',
        best_offer: 'BEST OFFER'
      }
    };
  }

  componentWillMount(){
    console.log("componentWillMount :: offerList: ", JSON.stringify(this.props.offerDataList));
    let offerList = this.props.offerDataList;
    if (offerList == undefined || offerList == null || offerList == '' ) {
      return;
    }
    if (offerList.length >0){
      let expandSwitch = [];
      for (var i=0;i<offerList.length;i++){
        expandSwitch.push({ id:i, isHidden: false });
      }
      this.setState({ expandSwitch:expandSwitch });
    }
  }

  componentDidMount(){
    let offerList = this.props.offerDataList;
    let selectedOffer = this.props.availableDtvOffers;
    if (selectedOffer.offer_code == ''){
      this.setState({ selectedIndex : 0 });
    } else {
      for (var x=0; x<offerList.length; x++){
        (offerList[x].offer_code == selectedOffer.offer_code ? this.setState({ selectedIndex: x }) : 0);
      }
    }
  }

  componentWillReceiveProps(nextProps){
    let offerList = nextProps.offerDataList;
    if (offerList == undefined || offerList == null || offerList == '' ) {
      return;
    }
    console.log("componentWillReceiveProps :: offerList: ", JSON.stringify(nextProps.offerDataList));
    if (offerList.length >0){
      let expandSwitch = [];
      for (var i=0;i<offerList.length;i++){
        expandSwitch.push({ id:i, isHidden: false });
      }
      this.setState({ expandSwitch: expandSwitch });
    }
  }


  expandDetails = (index) => {
    console.log("this.state.expandSwitch", this.state.expandSwitch);
    console.log(this.state.expandSwitch);
    let expandSwitch = this.state.expandSwitch;
    expandSwitch[index].isHidden = !expandSwitch[index].isHidden;
    console.log('xxx expandDetails', index );
    if (this.state.expandSwitch) {
      this.setState({ expandSwitch: expandSwitch });
    }
  }

  handleBackButtonClick = () => {
    this.dismissModal();
  }

  handleContinueButtonClick = () => {
    console.log("handleContinueButtonClick");
    if (this.props.cancelVoucherCodeReservation)
      this.props.cancelVoucherCodeReservation();
    this.props.dtvResetVoucerValue();
    this.props.dtvResetStbUpgradeData();
    this.props.dtvResetDepositAmount();
    this.props.dtvResetCustomerInfoSection();
    this.props.dtvSetCustomerInfoValidationStatus(false);
    this.props.dtvSetReloadAmound('');
    this.props.commonResetKycSignature();
    this.props.dtvResetIndividualSerialFieldValues();
    //call soft reserve and dismiss modal
    const requestParams = {
      conn_type: this.props.selected_connection_type,
      cx_identity_no: this.props.idNumber_input_value,
      txn_reference: this.props.txn_reference,
      offer_code: this.props.offerDataList[this.state.selectedIndex].offer_code,
      offer_type: this.props.offerDataList[this.state.selectedIndex].offer_type,
      connection_type: this.props.selected_connection_type,
      package_code:this.props.availableDTVDataPackagesList.data_package_code,
      fulfilment_type: this.props.dtv_fullfillment_type.package_code,   
    };
    this.props.dtvGetOfferDetails(requestParams, this, (response) => {
      console.log("In dtvGetOfferDetails success callback", response);
      // let { voucher_code_enabled = false } = response;
      // if (voucher_code_enabled == false){
      console.log("In voucher_code_enabled == false");
      console.log("this.props: ",JSON.stringify(this.props));
      // }
    });

    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_offer_continue',
      item_id: this.props.offerDataList[this.state.selectedIndex].offer_code,
      content_type: 'button',
      additional_details: this.props.offerDataList[this.state.selectedIndex].offer_type,
    });


    if (this.state.selectedIndex >=0 ){
      this.props.dtvGetSelectedOffer(this.props.offerDataList[this.state.selectedIndex]);
    }

  }

  dismissModal = () => {
    const navigatorOb = this.props.navigator;
    navigatorOb.dismissModal({ animated: true, animationType: 'slide-down' });
  }

  onListItemPress = (item, index) => {
    this.setState({ selectedIndex: index });
    Analytics.logFirebaseEvent('select_content', {
      context: 'dtv_select_offer',
      item_id: item.offer_name,
      content_type: 'list',
      additional_details: item.offer_type,
    });
  }

  renderListItem = ({ item, index }) => {
    return (
      <View style={styles.listContainer}>
        <TouchableOpacity style={this.state.selectedIndex == index ? styles.viewStyleSelected : styles.viewStyle} onPress={()=>this.onListItemPress(item, index)}>
          {this.state.expandSwitch[index].isHidden ? 
            <View style={styles.elementLeftItem}>
              <Text style={styles.leftTextStyle}>{item.offer_description}</Text>
            </View> : 
            <View style={styles.elementLeftItem}>
              <Text style={[styles.textStyle]}>{item.offer_name}</Text>
            </View> }
          <View style={styles.elementRightItem}>
            {item.offer_type == this.props.multi_play_offer_type ? 
              <View style={styles.offerTagStyle}>
                <Text style={styles.offerTagTextStyle}>{this.state.locals.best_offer}</Text>
              </View> : true }
            <View style={styles.priceViewStyle}>
              <Text style={styles.textStyleRupees}>Rs </Text>
              <Text style={styles.textStyleBoldBlack}>{Utill.numberWithCommas(Utill.checkNumber(item.price))}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    let offerList = this.props.offerDataList;

    return (
      <View style={styles.containerOverlay}>
        <Header
          style={styles.header}
          backButtonPressed={() => this.handleBackButtonClick()}
          headerText={this.state.locals.title} />
        {/*loading indicator*/}
        <ActivityIndicator animating={this.props.apiLoading} text={this.props.api_call_indicator_msg}/>
        <View style={styles.section_1_Container}>   
          <View style={styles.containerMainView}>
            <ScrollView>
              <View style={styles.containerMainView}>
                <FlatList
                  data={offerList}
                  renderItem={this.renderListItem}
                  keyExtractor={(item, index) => index.toString()}
                  scrollEnabled={false}
                  extraData={this.state}
                />
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.totalPriceContainerStyle}>
          <Text style={styles.totalPriceTitleStyle}>{this.state.locals.offer_price}</Text>
          { this.state.selectedIndex >=0 ?
            <View style={styles.priceViewStyle}>
              <Text style={styles.textStyleRupees}>Rs </Text>
              <Text style={styles.textStyleFooterPriceBlack}>{Utill.numberWithCommas(Utill.checkNumber(offerList[this.state.selectedIndex].price))}</Text>
            </View> 
            :
            <View style={styles.priceViewStyle}>
              <Text style={styles.textStyleRupees}>Rs </Text>
              <Text style={styles.textStyleFooterPriceBlack}>0.00</Text>
            </View> 
          }
        </View>
        {/*Button Starts*/}
        <View style={styles.section_2_Container}>
          <View style={styles.buttonSetStyle}>
            <TouchableOpacity
              style={styles.confirmButtonStyle}
			        onPress={()=>this.handleContinueButtonClick()}>
              <Text style={styles.ConfirmAndCancelTextStyle}>
                {this.state.locals.Continue}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/*Button Ends*/}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const Language = state.lang.current_lang;
  const apiLoading = state.dtvActivation.apiLoading;
  const api_call_indicator_msg = state.dtvActivation.api_call_indicator_msg;
  const availableDtvOffers = state.dtvActivation.availableDtvOffers;
  const selected_connection_type = state.dtvActivation.selected_connection_type;
  const availableDTVDataPackagesList = state.dtvActivation.availableDTVDataPackagesList;
  const dtv_fullfillment_type = state.dtvActivation.dtv_fullfillment_type;
  const txn_reference = state.dtvActivation.txn_reference;
  const idNumber_input_value = state.dtvActivation.idNumber_input_value;
  const multi_play_offer_type = state.dtvActivation.multi_play_offer_type;

  return { 
    Language,
    apiLoading,
    txn_reference,
    idNumber_input_value,
    api_call_indicator_msg,
    availableDtvOffers,
    selected_connection_type,
    availableDTVDataPackagesList,
    dtv_fullfillment_type,
    multi_play_offer_type
  };
};



const styles = {
  containerOverlay: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white
  },
  containerMainView: {
    flex: 1,
    backgroundColor: Colors.white
  },
  firstColumnView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },
  SecondColumnView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 5,
  },
  firstColumnView_1_Container: {
    flex: 0.3,
    marginLeft: 10,
    justifyContent: 'center',
  },
  firstColumnView_2_Container: {
    flex: 0.7
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '95%',
    marginTop: 5,
    marginLeft: 10,
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 20,
    paddingBottom: 20,
    marginBottom: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.borderColor,
    height: 'auto',
    shadowColor: Colors.grey,
    shadowOffset: { width: 0, height: -3 },
    shadowOpacity: 0.3,
    shadowRadius: 0.5,  
    elevation: 1
  },
  viewStyleSelected: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '95%',
    marginTop: 5,
    marginLeft: 10,
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom: 5,
    paddingTop: 20,
    paddingBottom: 20,
    borderWidth: 1.5,
    borderRadius: 5,
    borderColor: Colors.colorYellow,
    height: 'auto',
  },
  gridviewviewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12,
    height: 'auto'
  },

  offerTagStyle: {
    //flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    marginBottom: 5,
    borderRadius: 5,
    backgroundColor: Colors.navBarBackgroundColor,
  },

  priceViewStyle: {
    flex:0.4,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
    //width: '100%',
    //marginBottom: 12,
    //height: 'auto'
  },
  textStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },

  leftTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    alignSelf: 'flex-start'
  },

  offerTagTextStyle : {
    fontSize: Styles.otpEnterModalFontSize,
    fontWeight: '500',
    textAlign: 'center',
    color: Colors.colorWhite
  },
  
  textStyleRupees: {
    flex: 0.4,
    textAlign: 'right',
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey
  },
  textStyleBoldBlack: {
    flex: 0.6,
    //right:0,
    textAlign: 'right',
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight:'bold'
  },
  textStyleFooterPriceBlack: {
    flex: 0.6,
    //right:0,
    textAlign: 'right',
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorGrey,
    paddingRight:12
  },
  modalDropdownView: {
    flexDirection: 'row',
  },

  modalDropdownStyles: {
    width: '100%',
  },

  dropdownStyle: {
    width: (Dimensions
      .get('window')
      .width - (Dimensions
      .get('window')
      .width * 0.3 + 15)),
    height: 160,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor
  },

  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint,
    marginLeft: 10
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    // marginLeft: 15,
    //marginRight: 15,
    padding: 5,
    borderWidth: 1,
    borderColor: Colors.borderColor
  },
  dropdownDataElemint: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
    // backgroundColor: 'red'
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 5,
    // marginLeft: 10,
  },
  dropdownDataElemintTxt: {
    color: Colors.colorBlack,
    fontSize: Styles.dropDownModal.dropdownDataElemint
  },
  elementLeftItem: {
    flex: 0.6,
    marginLeft: 5,
    alignSelf: 'flex-start',
    justifyContent:'flex-start',
    alignItems: 'flex-start',
    //backgroundColor: Colors.colorGreen
  },
  elementRightItem: {
    flex: 0.4,
    marginRight: 5,
    alignSelf: 'flex-start',
    flexDirection: 'column',
    alignItems: 'flex-start',
    //backgroundColor: Colors.colorPureYellow
  },
  elementRightItemExpandSwitch:{
    flex: 0.1,
    marginLeft: 5,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  elementLeftText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack
  },
  elementRightText: {
    fontSize: Styles.defaultBtnFontSize,
    color: Colors.colorBlack,
    textAlign: 'left'

  },
  listContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  gridContainer: {
    width: Dimensions
      .get('window')
      .width / 3 - 10,
    // height: Dimensions
    //   .get('window')
    //   .width / 3,
    // backgroundColor: 'red',
    // width : 100,    
    height: 80,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor,
    marginRight: 10,
    marginBottom: 10,
    
  },
  item: {
    flex: 1,
    resizeMode:'cover',
    // width : 72,
    // height: 36,
    margin: 10,
    justifyContent: 'center'
    
  },
  section_2_Container: {
    // flex: 1,
    marginTop: 10,
    // backgroundColor: 'red'
    height: 56

  }, 
  section_1_Container: {
    flex: 1,
    // marginBottom: 5,
    // backgroundColor: 'red'

  }, 
  confirmButtonStyle: {
    backgroundColor: '#FFC400',
    alignSelf: 'stretch',
    borderRadius: 2,
    marginRight: 20,
    padding: 10

  },
  buttonSetStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  ConfirmAndCancelTextStyle: {
    fontSize: Styles.otpEnterModalFontSize,
    color: Colors.colorBlack,
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10
  },
  errorViewContainer:  { 
    flex:10,
    paddingTop: 10,
    flexDirection: 'column',
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center'
  },
  errorView:{ 
    flex:1, 
    justifyContent:'center', 
    alignItems:'center' 
  },
  errorText:{ 
    fontSize: 15, 
    fontWeight: '500',
    marginBottom: 15 
  },
  totalPriceTitleStyle:{
    flex:0.7, 
    marginLeft:10
  },
  totalPriceContainerStyle:{
    flexDirection:'row', 
    borderTopColor: Colors.grey, 
    borderTopWidth:1, 
    paddingTop:5
  },
  totalPriceSummaryStyle:{
    flex:0.25, 
    marginRight:5, 
    alignItems:'flex-end'
  }
};

export default connect(mapStateToProps, actions)(OfferSelectionModal);