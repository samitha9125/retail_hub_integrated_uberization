import React from 'react';
import { StyleSheet, View } from 'react-native';
import Colors from '../../../config/colors';

class SectionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  render() {
    const { customStyle, children, hideView, customMarginLeft } = this.props;
    if (hideView) {
      return null;
    }
    return (
      <View style={[styles.container, customStyle, customMarginLeft]}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 4,
    marginTop: 4,
    marginLeft: 25,
    marginRight: 5,
    backgroundColor: Colors.appBackgroundColor
  }
});

export default SectionContainer;
