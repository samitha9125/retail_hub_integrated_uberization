/*
 * File: GridSelectionViewComponent.js
 * Project: Dialog Sales App
 * File Created: Monday, 22nd October 2018 6:45:57 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 22nd October 2018 6:47:12 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { View, Dimensions, FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors, Styles } from '@Config';

class GridSelectionViewComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    const {
      gridItem = [],
      renderListItem = [],
      numColumns = 3,
      style = {},
      listEmptyComponent = true
    } = this.props;

    return (
      <View style={[styles.gridViewStyle, style]}>
        <FlatList
          data={gridItem}
          renderItem={renderListItem}
          keyExtractor={(item, index) => index.toString()}
          scrollEnabled={false}
          numColumns={numColumns}
          ListEmptyComponent={listEmptyComponent}
          extraData={this.state}/>
      </View>
    );
  }
}

const styles = {
  gridViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12,
    height: 'auto'
  }
};

export default GridSelectionViewComponent;
