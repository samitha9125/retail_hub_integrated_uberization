import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Keyboard,
  Dimensions
} from 'react-native';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import { TextField } from 'react-native-material-textfield';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import { FuncUtils } from '../../../utills';
import {  Colors } from '../../../config';
import _ from 'lodash';

class DropDownInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: undefined,
      searchResults: []
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log('DropDownInput willReceiveProps\n', JSON.stringify(nextProps));
  }

  render() {
    const {
      customStyle,
      onSelect,
      ref,
      dropDownTitle,
      selectedValue = '',
      dropDownData = [],
      defaultIndex,
      hideRightIcon,
      icon,
      onPress,
      disabled,
      disableDropdown,
      showArrow= true,
      disableIconTap,
      isTextInput = false,
      onTextInputChange,
      modalRef,
      onBlur,
      multiline = true,
      numberOfLines = 2,
      onDropdownWillHide,
      error = '',
      title = '',
      dropdownStyle = {}
    } = this.props;

    const keyboardAwareOnPress = function () {
      Keyboard.dismiss();
      onPress.apply(this, arguments);
    };

    let rightIconView;
    let dropDownDataArray = _.isNil(dropDownData) ? [] : dropDownData;

    if (!hideRightIcon) {
      let rightIcon;
      if (icon.iconType === 'MaterialCommunityIcons') {
        rightIcon = (<MaterialCommunityIcons
          name={icon.id}
          size={24}
          color={Colors.defaultIconColorBlack} />);
      } else if (icon.iconType === 'Ionicons') {
        rightIcon = (<Ionicons name={icon.id} size={22} color={Colors.defaultIconColorBlack} />);
      } else if (icon.iconType === 'FontAwesome') {
        rightIcon = (<FontAwesome name={icon.id} size={26} color={Colors.defaultIconColorBlack} />);
      } else if (icon.iconType === 'Entypo') {
        rightIcon = (<Entypo name={icon.id} size={20} color={Colors.defaultIconColorBlack} />);
      } else {
        rightIcon = (<MaterialIcons name={icon.id} size={18} color={Colors.defaultIconColorBlack} />);
      }

      rightIconView = (
        <View>
          <TouchableOpacity
            style={styles.imageStyle}
            onPress={keyboardAwareOnPress}
            disabled={disableIconTap || FuncUtils.getArraySize(dropDownDataArray) < 1}>
            {FuncUtils.getArraySize(dropDownDataArray) < 1 ? <View style={styles.iconCustomStyle} /> : rightIcon}
          </TouchableOpacity>
        </View>
      );
    } else {
      rightIconView = (
        <View>
          <View style={styles.imageStyle}>
            <View style={styles.iconCustomStyle} />
          </View>
        </View>);
    }

    let containerStyle;

    if (FuncUtils.getArraySize(dropDownDataArray) == 0) {
      containerStyle = [styles.containerNoData];
    } else {
      containerStyle = [styles.container, customStyle];
    }

    return (
      <View style={containerStyle} ref={ref}>
        <View style={styles.innerContainer}>
          <View style={styles.mainViewStyle}>
            <View style={styles.dropDownViewContainer}>
              <ModalDropdown
                ref={modalRef}
                options={dropDownDataArray}
                disabled={disableDropdown || FuncUtils.getArraySize(dropDownDataArray) < 1}
                defaultIndex={defaultIndex}
                onSelect={onSelect}
                style={styles.modalDropdownStyles}
                dropDownColorData={this.props.offerList}
                dropdownStyle={FuncUtils.getArraySize(dropDownDataArray) < 5 ? [styles.dropdownStyle, dropdownStyle, { height: 'auto' }] : [styles.dropdownStyle, dropdownStyle]}
                dropdownTextStyle={[styles.dropdownTextStyle]}
                dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
                onDropdownWillHide={onDropdownWillHide}>
                <View style={styles.insideDropdownStyle}>
                  <View style={styles.leftViewStyle}>
                    <TextField
                      editable={isTextInput == true && !disabled}
                      label={dropDownTitle}
                      numberOfLines={numberOfLines}
                      inputContainerStyle={styles.textFieldStyle}
                      onChangeText={onTextInputChange}
                      style={[styles.dropTextStyle]}
                      value={selectedValue}
                      multiline={multiline}
                      onBlur={onBlur}
                      error={error}
                      title={title}
                    />
                  </View>
                  {isTextInput == true ? <View style={styles.textInputViewStyle} /> :
                    <View style={styles.rightViewStyle}>
                      {(FuncUtils.getArraySize(dropDownDataArray) < 1 || !showArrow )? <View /> :
                        <View>
                          <Ionicons
                            name='md-arrow-dropdown'
                            size={24}
                            color={Colors.defaultIconColorBlack} />
                          {title ? <View style={styles.dropDownStyleWithTitle}></View> : <View />}
                        </View>
                      }
                    </View>
                  }
                </View>
              </ModalDropdown>
              {rightIconView}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerNoData: {
    flex: 1,
    top: -20,
    marginBottom: -20
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  mainViewStyle: {
    flex: 1,
    flexDirection: 'column'
  },

  dropDownViewContainer: {
    flex: 0.5,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  iconCustomStyle: {
    width: 20
  },

  imageStyle: {
    flex: 3,
    justifyContent: 'center',
    marginTop: 25,
    alignItems: 'center'
  },
  modalDropdownStyles: {
    flex: 9
  },
  dropdownStyle: {
    width: Dimensions
      .get('window')
      .width * 0.71,
    marginTop: -8
  },
  dropdownTextStyle: {
    // color: Colors.black,
    marginLeft: 5,
    fontSize: 15
  },
  dropdownTextHighlightStyle: {
    fontWeight: 'bold'
  },
  insideDropdownStyle: {
    flex: 0.5,
    flexDirection: 'row',
    height: '100%',
    // backgroundColor: 'yellow'
  },
  leftViewStyle: {
    flex: 9
  },
  dropTextStyle: {
    // color: Colors.black,
    fontSize: 15,
    paddingRight: 15,
    // backgroundColor:'red'
  },
  rightViewStyle: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 0,
    marginTop: 12,
    height: '100%',
    // backgroundColor: 'red',
    position: 'absolute',
    right: -4,
    marginRight: 15
  },
  dropDownStyleWithTitle: {
    marginBottom: 20,
  },
  textInputViewStyle: {
    flex: 0,
    alignItems: 'flex-end',
    paddingRight: 0,
    height: '100%'
  },
  textFieldStyle: { paddingRight: 10 }
});

export default connect(null, actions)(DropDownInput);