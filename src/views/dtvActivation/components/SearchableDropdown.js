/*
 * File: SearchableDropdown.js
 * Project: Dialog Retail Hub
 * File Created: Thursday, 25th October 2018 2:17:44 pm
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Monday, 29th October 2018 5:25:34 pm
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */


import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ListView,
  TextInput,
  View,
  TouchableOpacity,
  Animated,
  Keyboard,
  Dimensions,
  Image
} from 'react-native';
import Colors from '../../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Analytics } from '@Utils';
import _ from 'lodash';

const INITIAL_TOP = -60;
var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
export default class SearchableDropDown extends Component{
  constructor(props) {
    super(props);
    this.state = {
      item: {
        name: '',
        url:'',
        code:'-99'
      },
      listItems: [],
      focus: false,
      show: props.showOnLoad,
      top: new Animated.Value(
        INITIAL_TOP
      ),
      text:''
    };
    this.navigator = this.props.navigator;
    this.renderList = this.renderList.bind(this);
  }

  /**
   * This function will display the searchbar with an animation.
   *
   * @memberof SearchableDropDown
   */
  show = () => {
    const { animate = true, animationDuration = 0 } = this.props;
    this.setState({ show: true, 
      item: {
        name: '',
        url:'',
        code:'-99'
      } 
    });
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: 0,
        duration: animationDuration
      }).start();
      this.input.focus();
    } else {
      this.setState({ top: new Animated.Value(0) });
    }
  };

  /**
   ** This function will hide the searchbar with an animation.
   *
   * @memberof SearchableDropDown
   */
  hide = () => {
    this.setState({ text: '', focus: false });
    const { animate = true, animationDuration = 0 } = this.props;
    if (animate) {
      Animated.timing(this.state.top, {
        toValue: INITIAL_TOP,
        duration: animationDuration
      }).start();
    }
    Keyboard.dismiss();
  };

  /**
   *This function will be called when user taps away from the search field.
   *
   * @memberof SearchableDropDown
   */
  _handleBlur = () => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur();
    }
  };

  /**
   *This function will render the search result
   *
   * @returns
   * @memberof SearchableDropDown
   */
  renderList(){
    if (this.state.focus){
      return (
        <ListView
          style={ styles.itemsContainerStyle }
          keyboardShouldPersistTaps="always"
          dataSource={ds.cloneWithRows(this.state.listItems)}
          renderRow={this.renderItems} 
          enableEmptySections={true}
        />
      );
    }
  }

  componentDidMount(){
    const listItems = this.props.items;
    const defaultIndex = this.props.defaultIndex;
    if (defaultIndex && listItems.length > defaultIndex) {
      this.setState({
        listItems,
        item: listItems[defaultIndex]
      });
    }
    else {
      this.setState({ listItems : [] });
    }
  }

/**
 * This function will be called when the user enters a string into the search bar. This will filter out the item list
 * for the entered string and set the result accordingly to the state.
 *
 * @param {String} searchedText
 * @memberof SearchableDropDown
 */
searchedItems = (searchedText) => {
  const { noItemsFoundString = 'No items found', additionalChannelListData=[] } = this.props;
  if (searchedText.length > 2) {
    Analytics.logFirebaseEvent('search', {
      search_context: 'dtv_search_additional_channels',
      search_string: searchedText,
      additional_details: 'DTV select additional channel search',
    });
  }
 
  let item = {
    name: searchedText,
    url:null,
    code:'-99'
  };

  let placeholderItem = {
    name: noItemsFoundString,
    url:null,
    code:'-99'
  };
  let selectedPacks = _.filter(additionalChannelListData,{ is_selected:true }) || [];
  let channelsInPacks = [];

  // Extract the channels from selected channel packs into one array
  _.forEach(selectedPacks, function(packObj){
    channelsInPacks = [ ...channelsInPacks, ...packObj.channels ];
  });

  console.log('Selected packs: ', selectedPacks);
  console.log('channelsInPacks: ', channelsInPacks);
  console.log('additionalChannelListData: ', additionalChannelListData);

  // Search results should be displayed after the 2nd character.
  if (searchedText.length < 3){
    this.setState({ item: item });
    if (searchedText.length == 0){
      this.setState({ item: item, listItems:[] });
      return true;
    }
    return true;
  }

  // Search the channel list for the entered text.
  var ac = this.props.items.filter(function(item) {
    return item.name.toLowerCase().indexOf(searchedText.toLowerCase()) > -1;
  });

  // Filter out the channels that are not included in the selected packs.
  let filteredFromSelectedPacks = _.differenceBy(ac,channelsInPacks,'code');

  // If the filter result is empty, show "No channel found".
  let searchResult = filteredFromSelectedPacks.length > 0 ? filteredFromSelectedPacks : [placeholderItem];

  // Set the filtered result into search dropdown.
  this.setState({ listItems: searchResult, item: item });

  // If the onTextChange event is handled manually, invoke callback.
  const onTextChange = this.props.onTextChange;
  if (onTextChange && typeof onTextChange === 'function') {
    setTimeout(() => {
      onTextChange(searchedText);
    }, 0);
  }
};

  renderItems = (item) => {

    return (
      <TouchableOpacity
        onPress={() => {
          if (item.code === "-99"){
            this.setState({ 
              item: {
                name: '',
                url:'',
                code:'-99'
              }, focus: false });
          } else {
            this.setState({ 
              item: item, focus: false });
          }
          Keyboard.dismiss();
          setTimeout(() => {
            this.props.onItemSelect(item);
          }, 0);
        }}
        style={styles.dropDownRow}>
        <View style={styles.searchBarIcon}></View>
        <View style={styles.channelNameTextView}>
          <Text style={styles.textViewStyleClass}>{item.name}</Text>
        </View>
        <View style={styles.channelIconView}>
          <Image 
            source={{ uri: item.url }}
            resizeMode={'stretch'}
            style={styles.searchItemIcon}
          />
        </View>
      </TouchableOpacity> 
    );
  };

  render() {
    const { 
      placeholder = 'Search', 
      keyboardType = 'default',
      leftButtonProps = { icon: 'search', action: ()=>{} },
      rightButtonProps = { icon: 'clear', action: ()=>{} },
      hasLeftButton = false,
      hasRightButton = false,
    } = this.props;
    
    return (
      <Animated.View keyboardShouldpersist='always' style={[{ ...this.props.containerStyle },{ position:'absolute', backgroundColor:Colors.white, top: this.state.top, width:'100%', zIndex:9999 }]}>
        <View style={styles.searchBarContainer}>
          {hasLeftButton?this.renderSearchLeftButton(leftButtonProps):true}
          <TextInput
            // underlineColorAndroid={this.props.underlineColorAndroid}
            onFocus={() => {
              this.setState({
                focus: true,
                // item: {
                //   name: '',
                //   url:'',
                //   code:'-99'
                // },
                listItems: []
              });
            }}
            onBlur={() => {
              this.setState({ focus: false });
            }}
            underlineColorAndroid={Colors.transparent}
            ref={(e) => this.input = e}
            onChangeText={(text) => {
              this.searchedItems(text);}
            }
            keyboardType={keyboardType}
            value={this.state.item.name}
            style={styles.textInputStyle}
            placeholderTextColor={this.props.placeholderTextColor}
            placeholder={placeholder} />
          {hasRightButton?this.renderSearchRightButton(rightButtonProps):true}  
        </View>
        { this.renderList() }
      </Animated.View>
    );
  }
  renderSearchLeftButton = (leftButtonProps) => {
    return (
      <TouchableOpacity style={styles.searchBarIcon} onPress={leftButtonProps.action}>
        <MaterialIcons
          name={leftButtonProps.icon}
          size={styles.inputFieldIconSize}
          color={Colors.grey}
        />
      </TouchableOpacity>
    );
  }

  renderSearchRightButton = (rightButtonProps) => {
    return (
      <TouchableOpacity style={styles.clearIcon} onPress={rightButtonProps.action}>
        <MaterialIcons
          name={rightButtonProps.icon}
          size={styles.inputFieldIconSize}
          color={Colors.grey}
        />
      </TouchableOpacity>
    );
  }
}

const styles = {
  inputFieldIconSize: 30, 
  searchBarIcon:{
    width:35,
    margin: 5
  },
  clearIcon:{
    flex:1,
    margin: 5
  },
  dropDownRow:{
    flex:1,
    flexDirection:'row',
    width: Dimensions
      .get('window')
      .width,
    height:55
  },
  searchBarContainer:{
    flexDirection:'row', 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  textViewStyleClass:{    
    flex:1,
    textAlign: 'left',
    color:Colors.black,
    justifyContent: 'center', 
    alignItems: 'center',
    lineHeight: 55,
    fontSize: 14,
  },
  channelNameTextView:{
    flex:9,
    flexDirection:'column',
    padding:0,
    margin:0,
  },
  channelIconView:{
    width:80,
    margin: 10,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  searchItemIcon:{
    flex:1,
    height:'100%',
    width:80
  },
  itemsContainerStyle:{ 
    maxHeight: 220, 
    backgroundColor: Colors.white
  },
  textInputStyle:{ 
    flex:9 
  }
};