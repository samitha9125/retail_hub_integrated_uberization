
/*
 * Created on Fri Oct 05 2018
 *
* Copyright 2018 Omobio (PVT) Limited
 * File: PackageSelectionModal.js
 * Project: Dialog Sales App
 * Author: Manoj Kanth (manojkanthan.rajendran@omobio.net)
 */


 
import React from 'react';
import { View, Dimensions, FlatList } from 'react-native';
import Colors from '../../../config/colors';


class GridViewComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.navigator = this.props.navigator;
  }

  render() {
      
    const {
      gridItem = [],
      renderListItem = []
    } = this.props;

    return (

      <View style={styles.gridviewviewStyle}> 
        <FlatList
          data={gridItem}
          renderItem={renderListItem}
          keyExtractor={(item, index) => index.toString()}
          scrollEnabled={false}
          numColumns = {3}
          extraData={this.state}
        />
      </View>
    );
  }
}

const styles = {
  gridContainer: {
    width: Dimensions
      .get('window')
      .width / 3 - 10,
    // height: Dimensions
    //   .get('window')
    //   .width / 3,
    // backgroundColor: 'red',
    // width : 100,    
    height: 80,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: Colors.borderColor,
    marginRight: 10,
    marginBottom: 10,
        
  },
  itemView: {
    flex: 1,
    resizeMode:'cover',
    // width : 72,
    // height: 36,
    margin: 10,
    justifyContent: 'center'
        
  },
  gridviewviewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    marginBottom: 12,
    height: 'auto'
  }
};

export default GridViewComponent;
