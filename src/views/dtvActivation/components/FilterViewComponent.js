/*
 * File: FilterViewComponent.js
 * Project: Dialog Sales App
 * File Created: Monday, 22nd October 2018 12:32:44 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 22nd October 2018 2:14:46 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { View, Dimensions, Text } from 'react-native';
import ModalDropdown from 'react-native-rn-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors, Styles } from '@Config';

class FilterViewComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      dropDownData = [],
      defaultIndex = -1,
      onDropdownSelect,
      dropDownLabel = '',
      selectedValue = '',
      disableDropdown = false,
    } = this.props;

    console.log('FilterViewComponent :: dropDownData', dropDownData);

    return (
      <View style={styles.container}>
        <View style={styles.dropdown_label_Container}>
          <Text style={styles.textStyle}>
            {dropDownLabel}</Text>
        </View>
        <View style={styles.dropdown_Container}>
          <View style={styles.modalDropdownView}>
            <ModalDropdown
              options={dropDownData}
              onSelect={onDropdownSelect}
              disabled={disableDropdown || dropDownData.length <= 1}
              defaultIndex={parseInt(defaultIndex)}
              style={[styles.modalDropdownStyles]}
              dropdownStyle={[styles.dropdownStyle, dropDownData.length > 3 ? { height: 160 } : { height: 'auto' }]}
              dropdownTextStyle={styles.dropdownTextStyle}
              dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}>
              <View style={styles.dropdownElementContainer}>
                <View style={styles.dropdownDataElement}>
                  <Text
                    ellipsizeMode='tail'
                    numberOfLines={3}
                    style={styles.dropdownDataElementTxt}>{selectedValue}</Text>
                </View>
                <View style={styles.dropdownArrow}>
                  <Ionicons name='md-arrow-dropdown' size={20}/>
                </View>
              </View>
            </ModalDropdown>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {

  container: {
    flexDirection: 'row',
    marginTop: 10,
    borderColor: Colors.borderColor,
    borderWidth: 1,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    height: 55
    //backgroundColor: 'green'
  },

  dropdown_label_Container: {
    flex: 0.3,
    justifyContent: 'center'
  },
  dropdown_Container: {
    flex: 0.7,
    borderColor: Colors.borderColor,
    borderLeftWidth: 1
  },
  modalDropdownView: {
    flexDirection: 'row'
  },

  modalDropdownStyles: {
    width: '100%'
  },

  dropdownStyle: {
    width: (Dimensions.get('window').width - Dimensions.get('window').width * 0.3) - 12,
    height: 160,
    //backgroundColor: 'green'
  },

  dropdownTextStyle: {
    color: Colors.colorBlack,
    fontSize: 18,
    marginLeft: 5
  },
  dropdownTextHighlightStyle: {
    fontWeight: '500'
  },

  dropdownElementContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    padding: 5
  },

  dropdownDataElement: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    //backgroundColor: 'red'
  },
  dropdownArrow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 8
  },
  dropdownDataElementTxt: {
    color: Colors.colorGrey,
    fontSize: 18,
    marginLeft: 5
  },

  textStyle: {
    fontSize: 18,
    color: Colors.colorGrey,
    marginLeft: 10
  }
};

export default FilterViewComponent;
