import { Keyboard } from 'react-native';
import Constants from '../config/constants';
import { Analytics, ApiRequestUtils, Screen, FuncUtils } from '../utills';
import _ from 'lodash';

import {
  LTE_API_CALL, SET_LTE_VALIDATED_VOUCHER_CODE,
  LTE_SET_VOUCHER_CANCELLATION_STATUS,
  DTV_SET_API_LOADING, DTV_SET_VOUCHER_CANCELLATION_STATUS,
  SET_DTV_VOUCHER_CODE, RESET_DTV_VOUCHER_CODE_DATA,
  COMMON_SET_ACTIVITY_START_TIME,
  COMMON_SET_ACTIVITY_END_TIME,
  COMMON_SET_BANNER_IMAGES,
} from "./types";

/**
 *  LTE, DTV Voucher code cancellation
 */
export const voucherCodeCancellationApi = (requestParams, screen, endpoint, releaseOnly = false) => {
  console.log('xxxx voucherCodeCancellationApi API');
  console.log('xxx voucherCodeCancellationApi :: requestParams ', requestParams);

  return (dispatch) => {
    if (requestParams.lob == Constants.LOB_LTE)
      dispatch({ type: LTE_API_CALL, payLoad: false });
    dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    const successCb = function (response) {
      console.log('xxx voucherCodeCancellationApi:: successCb ', response);

      if (response.success) {
        console.log('VOUCHER_CANCELLATION_SUCCESS');
        if (releaseOnly == false) {
          if (requestParams.lob == Constants.LOB_LTE) {
            dispatch({ type: SET_LTE_VALIDATED_VOUCHER_CODE, payLoad: '' });
            dispatch({ type: LTE_SET_VOUCHER_CANCELLATION_STATUS, payLoad: true });
          } else {
            // dispatch({ type: SET_DTV_VALIDATED_VOUCHER_CODE, payLoad: '' });
            dispatch({ type: DTV_SET_VOUCHER_CANCELLATION_STATUS, payLoad: true });
            dispatch({ type: SET_DTV_VOUCHER_CODE, payLoad: '' });
            dispatch({ type: RESET_DTV_VOUCHER_CODE_DATA, payLoad: true });
          }
        }
        if (requestParams.lob == Constants.LOB_LTE)
          dispatch({ type: LTE_API_CALL, payLoad: false });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        Analytics.logEvent('lte_cancel_voucher_code_reservation_success');
      }

      else {
        if (requestParams.lob == Constants.LOB_LTE)
          dispatch({ type: LTE_API_CALL, payLoad: false });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        console.log('xxx voucherCodeCancellationApi :: NEGATIVE');
        Analytics.logEvent('lte_cancel_voucher_code_reservation_failed');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('VOUCHER_CANCELLATION_NO_ERROR');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('VOUCHER_CANCELLATION_FAIL');
          screen.showGeneralErrorModal(response.error);

        }
      }

    };

    const failureCb = function (response) {
      console.log('xxx voucherCodeCancellationApi :: failureCb ', response);
      console.log('VOUCHER_CANCELLATION_ERROR');
      if (requestParams.lob == Constants.LOB_LTE)
        dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    };

    let handleFalseResponse = true;
    ApiRequestUtils.apiPost(endpoint.action, endpoint.controller, endpoint.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

export const commonSetActivityStartTime = () => {
  let timestamp = Math.round((new Date()).getTime() / 1000);
  console.log('Redux :: commonSetActivityStartTime -', timestamp);
  return (dispatch) => {
    dispatch({ type: COMMON_SET_ACTIVITY_START_TIME, payLoad: timestamp });
  };
};

export const commonSetActivityEndTime = () => {
  let timestamp = Math.round((new Date()).getTime() / 1000);
  console.log('Redux :: commonSetActivityEndTime -', timestamp);
  return (dispatch) => {
    dispatch({ type: COMMON_SET_ACTIVITY_END_TIME, payLoad: timestamp });
  };
};

/**
 * This function will get the dashboard banner URLs
 * 
 */
export const commonGetBannerImages = (successCallback) => {
  console.log('Redux :: commonGetBannerImages :: API');

  let url = {
    action: 'getHomeBanners',
    controller: 'homeBanner',
    module: 'ccapp'
  };

  let requestParams = {};

  return (dispatch) => {

    try {
      const successCb = function (response) {
        console.log('Redux :: commonGetBannerImages :: successCb', response);
        if (response.success) {
          console.log('Redux :: commonGetBannerImages :: successCb', response.data);
          let bannerImageArray = response.data;
          dispatch(commonSetBannerImages(bannerImageArray));
          successCallback(bannerImageArray);
        } else {
          console.log('Redux :: commonGetBannerImages :: failureCb', response);
          dispatch(commonSetBannerImages(null));

        }
      };

      const exCb = function (response) {
        console.log('Redux :: commonGetBannerImages :: exCb', response.error);
        dispatch(commonSetBannerImages(null));
      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, exCb, handleFalseResponse);

    } catch (e) {
      dispatch(commonSetBannerImages(null));
      console.log('Redux :: commonGetBannerImages :: EXCEPTION ', e);
    }
  };
};

export const commonSetBannerImages = (data) => {
  console.log('Redux :: commonSetBannerImages :: ', data);
  return (dispatch) => {

    dispatch({ type: COMMON_SET_BANNER_IMAGES, payLoad: data });
  };
};
