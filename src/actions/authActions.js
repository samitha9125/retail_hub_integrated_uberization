/*
 * File: authActions.js
 * Authentication related redux actions
 * Project: Dialog Sales App
 * File Created: Wednesday, 28th November 2018 8:48:32 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 28th November 2018 8:49:30 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { Keyboard } from 'react-native';
import Constants from '../config/constants';
import { Analytics, ApiRequestUtils, LocalStorageUtils, Screen, MessageUtils } from '../utills/';
import Utills from '../utills/Utills';
import _ from 'lodash';

const Utill = new Utills();

import {
  AUTH_SET_API_LOADING,
  AUTH_GETS_START_API_SUCCESS,
  AUTH_GETS_START_API_FAIL,
  GET_CURRENT_LANGUAGE,

} from './types';

/**
 * @description This method is used to handle user On-boarding
 * @param {Object} requestData 
 * @param {fn} successCbCustom 
 * @param {fn} failureCbCustom 
 */
export const authGetStartInApi = (requestData, successCbCustom = null, failureCbCustom = null) => {
  console.log('Redux :: authGetStartInApi :: API');
  console.log('Redux :: authGetStartInApi :: requestData '.requestData);
  return (dispatch) => {
    dispatch({ type: AUTH_SET_API_LOADING, payLoad: true });
    Keyboard.dismiss();
    try {
      const successCb = function (response) {
        console.log('Redux :: authGetStartInApi :: successCb', response.data);
        dispatch({ type: AUTH_SET_API_LOADING, payLoad: false });

        //RECHECK - Save login persistance data
        /*
        if (response.data.action === 'SUCCESS_LOGIN' && requestData.action === 'login') {
          LocalStorageUtils.saveUserDataAfterLogin(response);       
        } 
        */
        try {
          if (!_.isNil(response.data.pre_data) && !_.isNil(response.data.pre_data.lang)
            && !_.isNil(response.data.pre_data.langIndex)) {
            let lang = response.data.pre_data.lang;
            let langIndex = response.data.pre_data.langIndex;
            dispatch(setLanguage(lang, langIndex));
            console.log('Redux :: authGetStartInApi :: setLanguage - SUCCESS');
          } else {
            console.log('Redux :: authGetStartInApi :: setLanguage - FAIL');
          }
        } catch (e) {
          console.log('Redux :: authGetStartInApi :: setLanguage - EXCEPTION : ', e);
        }

        dispatch({ type: AUTH_GETS_START_API_SUCCESS, payLoad: { nextScreen: response.data.action, data: response.data } });
        if (successCbCustom != null) {
          console.log('Redux :: authGetStartInApi :: successCbCustom');
          successCbCustom.apply(this, arguments);
        }
        Analytics.logEvent('dsa_on_boarding_success');
        Analytics.logEvent('dsa_on_boarding_action_' + response.data.action);
      };

      const failureCb = function (response) {
        console.log('Redux :: authGetStartInApi :: failureCb ', response.data.error);
        dispatch({ type: AUTH_SET_API_LOADING, payLoad: false });
        dispatch({ type: AUTH_GETS_START_API_FAIL, payLoad: [] });
        if (failureCbCustom != null) {
          console.log('Redux :: authGetStartInApi :: failureCbCustom');
          failureCbCustom.apply(this, arguments);
        } else {
          Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));

        }
        Analytics.logEvent('dsa_on_boarding_failure');
      };

      const exCb = function (error) {
        console.log('Redux :: authGetStartInApi :: exCb', error);
        dispatch({ type: AUTH_SET_API_LOADING, payLoad: false });
        dispatch({ type: AUTH_GETS_START_API_FAIL, payLoad: [] });
        if (failureCbCustom != null) {
          console.log('xxxx authGetStartInApi :: exCb => failureCbCustom');
          failureCbCustom.apply(this, arguments);
        } else {
          Utill.showAlert(MessageUtils.getExceptionMessage(error));
        }
        Analytics.logEvent('dsa_on_boarding_exception');
      };
      let withHttps = true;
      if (requestData.action == 'getStart' || requestData.action == 'login') {
        withHttps = false;
      }

      let module = 'ccapp';
      if (requestData.action == 'login') {
        module = '';
      }

      ApiRequestUtils.apiPostLegacy(requestData.action, requestData.controller, module, requestData, successCb, failureCb, exCb, withHttps);

    } catch (e) {
      console.log('Redux :: authGetStartInApi :: EXCEPTION ', e);
      dispatch({ type: AUTH_SET_API_LOADING, payLoad: false });
      dispatch({ type: AUTH_GETS_START_API_FAIL, payLoad: [] });
      Analytics.logEvent('dsa_on_boarding_exception');
    }
  };
};

export const setLanguage = (lang, langIndex) => {
  console.log('Redux :: setLanguage : lang, langIndex - ', lang, langIndex);
  return {
    type: GET_CURRENT_LANGUAGE,
    payLoad: {
      value: lang,
      index: langIndex
    }
  };
};