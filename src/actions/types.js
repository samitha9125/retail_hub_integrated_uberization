/*
 * File: types.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author:  Parthipan Kugadoss (parthipan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:22:11 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Parthipan Kugadoss (parthipan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 *              Arafath Misree (arafath@omobio.net)
 *              Manojkanth Rajendran (manojkanthan.rajendran@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

export const GET_PIN = 'get_pin';
export const GET_CONNECTION_TYPE_BILL = 'get_connection_type_bill';
export const GET_CONNECTION_NUMBER_BILL = 'get_connection_number_bill';
export const GET_AMOUNT_BILL = 'get_amount_bill';
export const GET_EZ_CASH_PIN_BILL = 'get_ez_cash_pin_bill';
export const GET_ID_NUMBER_SIM = 'get_id_number_sim';
export const GET_SIM_NUMBER_SIM = 'get_sim_number_sim';
export const GET_MOBILE_NUMBER_SIM = 'get_mobile_number_sim';
export const GET_PRE_POST_MOBILE_ACT = 'get_pre_post_mobile_act';
export const GET_LOCAL_FOREIGN_MOBILE_ACT = 'get_local_foreign_mobile_act';
export const GET_SIM_NUMBER_MOBILE_ACT = 'get_sim_number_mobile_act';
export const GET_ID_NUMBER_MOBILE_ACT = 'get_id_number_mobile_act';
export const GET_MOBILE_NUMBER_MOBILE_ACT = 'get_mobile_number_mobile_act';
export const GET_OTP_STATUS_MOBILE_ACT = 'get_otp_status_mobile_act';
export const GET_ALTERNATE_CONTACT_NUMBER_MOBILE_ACT = 'get_alternate_contact_number_mobile_act';
export const GET_EMAIL_ADDRESS_MOBILE_ACT = 'get_email_address_mobile_act';
export const GET_FIRST_RELOAD_MOBILE_ACT = 'get_first_reload_mobile_act';
export const GET_INFORMATION_ID_TYPE_MOBILE_ACT = 'get_information_id_type_mobile_act';
export const GET_SKIP_VALIDATION_MOBILE_ACT = 'get_skip_validation_mobile_act';
export const GET_NAME_MOBILE_ACT = 'get_name_mobile_act';
export const GET_INFORMATION_NOT_CLEAR_MOBILE_ACT = 'get_information_not_clear_mobile_act';
export const GET_BLOCKED_ID_MOBILE_ACT = 'get_blocked_id_mobile_act';
export const GET_RELOAD_OFFER_CODE_MOBILE_ACT = 'get_reload_offer_code_mobile_act';
export const GET_ADDRESS_DIFFERENT_MOBILE_ACT = 'get_address_different_mobile_act';
export const SIM_VALIDATION_SUCCESS = 'sim_validation_success';
export const SIM_VALIDATION_FAIL = 'sim_validation_fail';
export const SIM_VALIDATION_FAIL_RESET = 'sim_validation_fail_reset';
export const SIM_API_FAIL_RESET = 'sim_api_fail_reset';
export const NEW_CONN_ACTIVATION_SUCCESS = 'new_conn_activation_success';
export const NEW_CONN_ACTIVATION_FAIL = 'new_conn_activation_fail';
export const MOBILE_ACT_NIC_VALIDATION_SUCCESS = 'mobile_act_validation_success';
export const MOBILE_ACT_NIC_VALIDATION_FAIL = 'mobile_act_validation_fail';
export const MOBILE_ACT_MODEL_POPPED = 'mobile_act_model_popped';
export const MOBILE_ACT_OTP_POPPED = 'mobile_act_otp_popped';
export const MOBILE_ACT_M_CONNECT_POPPED = 'mobile_act_m_connect_popped';
export const GET_NAME = 'get_name';
export const GET_OTP_NUMBER = 'get_otp_number';
export const MOBILE_ACT_OTP_VALIDATION_FAIL = 'get_otp_validation_fail';
export const MOBILE_ACT_OTP_VALIDATION_SUCCESS = 'get_otp_validation_success';
export const MOBILE_ACT_M_CONNECT_VALIDATION_FAIL = 'get_m_connect_validation_fail';
export const MOBILE_ACT_M_CONNECT_VALIDATION_SUCCESS = 'get_m_connect_validation_success';
export const GET_MOBILE_ACT_SIGNATURE = 'get_signature_mobile_act';
export const GET_MOBILE_ACT_KYC_CAPTURE = 'get_kyc_mobile_act';
export const GET_MOBILE_ACT_KYC_NIC_1 = 'get_kyc_nic_1_mobile_act';
export const GET_MOBILE_ACT_KYC_NIC_2 = 'get_kyc_nic_2_mobile_act';
export const GET_MOBILE_ACT_POB_KYC_CAPTURE = 'get_kyc_pob_mobile_act';
export const GET_MOBILE_ACT_CUSTOMER_KYC_CAPTURE = 'get_kyc_customer_mobile_act';
export const GET_MOBILE_ACT_CAPTURE_TYPE_NO = 'get_capture_id_type_no_mobile_act';
export const GET_MOBILE_ACT_RE_CAPTURE_TYPE_NO = 'get_re_capture_id_type_no_mobile_act';
export const GET_MOBILE_ACT_UNIQUE_TX_ID = 'get_unique_id_mobile_act';
export const GET_MOBILE_ACT_RESET_IMAGES = 'get_reset_images_mobile_act';
export const GET_API_RESPONSE_MOBILE_ACT = 'get_api_response_mobile_act';
export const GET_IS_CAPTURED_MOBILE_ACT = 'get_is_captured_mobile_act';
export const GET_API_LOADING = 'get_api_loading';
export const GET_CURRENT_LANGUAGE = 'get_current_language';
export const GET_CURRENT_PIN = 'get_current_pin';
export const GET_NEW_PIN = 'get_new_pin';
export const GET_CONFIRMED_PIN = 'get_confirmed_pin';
export const FOCUS_EZ_CASH_PIN = 'focus_ez_cash_pin';
export const RESET_PIN = 'reset_pin';
export const RESET = 'RESET';
export const GET_SELECTED_PACKAGE = 'get_selected_package';
export const IS_PACKAGE_LOADED = 'is_package_loaded';
export const GET_PACKAGE_DEPOSITS_SUCCESS = 'get_package_deposits_success';
export const GET_PACKAGE_DEPOSITS_FAIL = 'get_package_deposits_fail';
export const GET_EZ_CASH_PIN_MOBILE_ACT = 'get_ez_cash_pin_mobile_act';
export const GET_TOTAL_PAYMENT = 'get_total_payment_value';
export const SELECTED_PACKAGE_RESET = "secected_package_reset";
export const LOCAL_CALL_DEPOSITS_RESET = "local_call_deposits_reset";
export const GET_SELECTED_PACKAGE_INDEX = "get_selected_package_index";
export const USER_SELECTED_PACKAGE_INDEX_RESET = "user_selected_package_index_reset";
export const RESET_EZ_CASH_PIN = "reset_ez_cash_pin";
export const GET_MOBILE_ACT_LOADING = "get_mobile_act_loading";
export const GET_MOBILE_ACT_API_LOADING = "get_mobile_act_api_loading";
export const GET_DOC_99_URL = "get_doc_99_url";
export const GET_ALLOWED_SERVICES = 'get_allowed_services';
export const SET_RETAILER = 'SET_RETAILER';
export const SET_IS_CFSS = 'SET_IS_CFSS';
export const SET_TOTAL_PAYMENT_DATA = 'set_total_payment_data';
export const SET_PAYMENT_INFO = 'set_payment_info';
export const RESET_PAYMENT_INFO = 'reset_payment_info';
export const GET_TILE_DATA = 'get_tile_data';
export const GET_EZCASH_URL = 'GET_EZCASH_URL';
export const GET_EZCASH_SCHEME_URL = 'GET_EZCASH_SCHEME_URL';

// Delivery App Const Types
export const DELIVERY_SET_API_CALL = 'DELIVERY_SET_API_CALL';
export const GET_PENDING_WORK_ORDERS_SUCCESS = 'get_pending_work_orders_success';
export const GET_PENDING_WORK_ORDERS_FAIL = 'get_pending_work_orders_fail';
export const GET_WORK_ORDER_STATUS = 'get_work_order_status';
export const API_CALL_SUCCESS = 'API_CALL_SUCCESS';
export const API_CALL_FAIL = 'API_CALL_FAIL';
export const RESET_DELIVERY = 'reset_delivery';
export const GET_FILTER_WORK_ORDERS = 'get_filter_work_orders';
export const GET_CURRENT_WO_ID = 'get_current_wo_id';
export const GET_CURRENT_WO_JOBID = 'get_current_wo_jobid';
export const GET_CURRENT_WO_JOB_STATUS = 'get_current_wo_job_status';
export const GET_CURRENT_WO_SLA_BREACH_STATUS = 'get_current_wo_sla_breach_status';
export const GET_WOM_API_ORDER = 'get_wom_api_order';
export const DELIVERY_SHOW_LOADING_INDICATER = 'delivery_show_loading_indicator';
export const DELIVERY_HIDE_LOADING_INDICATER = 'delivery_hide_loading_indicator';
export const DELIVERY_API_CALL_FAIL = 'delivery_api_call_fail';
export const GET_CURRENT_WO_BASIC_INFO = 'delivery_get_current_order_basic_info';
export const DELIVERY_GET_WO_COUNT = 'delivery_get_wo_count';
export const DELIVERY_CCBS_ORDER_ID = 'delivery_ccbs_order_id';

//Transaction History Const Types
export const TRN_LOADING_INDICATOR = 'TRN_LOADING_INDICATOR';
export const RESET_ALL_TRANS_HISTORY = 'RESET_ALL_TRANS_HISTORY';
export const TRN_SIM_CHANGE_HISTORY_SUCCESS = 'TRN_SIM_CHANGE_HISTORY_SUCCESS';
export const TRN_SIM_CHANGE_HISTORY_FAIL = 'TRN_SIM_CHANGE_HISTORY_FAIL';
export const TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS = 'TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS';
export const TRN_SIM_CHANGE_HISTORY_PENDING_FAIL = 'TRN_SIM_CHANGE_HISTORY_PENDING_FAIL';
export const TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS = 'TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS';
export const TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL = 'TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL';
export const TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM = 'TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM';
export const TRN_BILL_PAYMENT_HISTORY_SUCCESS = 'TRN_BILL_PAYMENT_HISTORY_SUCCESS';
export const TRN_BILL_PAYMENT_HISTORY_FAIL = 'TRN_BILL_PAYMENT_HISTORY_FAIL';
export const TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM = 'TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM';
export const TRN_ACTIVATION_STATUS_ALL_SUCCESS = 'TRN_ACTIVATION_STATUS_ALL_SUCCESS';
export const TRN_ACTIVATION_RESET_ALL_LIST = 'TRN_ACTIVATION_RESET_ALL_LIST';
export const TRN_ACTIVATION_STATUS_ALL_FAIL = 'TRN_ACTIVATION_STATUS_ALL_FAIL';
export const TRN_ACTIVATION_STATUS_REJECTED_SUCCESS = 'TRN_ACTIVATION_STATUS_REJECTED_SUCCESS';
export const TRN_ACTIVATION_STATUS_REJECTED_FAIL = 'TRN_ACTIVATION_STATUS_REJECTED_FAIL';
export const TRN_ACTIVATION_STATUS_SELECTED_ITEM = 'TRN_ACTIVATION_STATUS_SELECTED_ITEM';
export const TRN_ACTIVATION_STATUS_FILTERS = 'TRN_ACTIVATION_STATUS_FILTERS';
export const TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING = 'TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING';
export const TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING = 'TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING';
export const TRN_SIM_CHANGE_HISTORY_GET_RESULTS = 'TRN_SIM_CHANGE_HISTORY_GET_RESULTS';
export const TRN_SIM_CHANGE_HISTORY_RESET_RESULTS = 'TRN_SIM_CHANGE_HISTORY_RESET_RESULTS';
export const TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING = 'TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING';
export const TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS = 'TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS';
export const TRN_BILL_PAYMENT_HISTORY_GET_RESULTS = 'TRN_BILL_PAYMENT_HISTORY_GET_RESULTS';
export const TRN_SIM_CHANGE_HISTORY_RESET_LISTS = 'TRN_SIM_CHANGE_HISTORY_RESET_LISTS';
export const TRN_ACTIVATION_RESET_REJECTED_LIST = 'TRN_ACTIVATION_RESET_REJECTED_LIST';
export const TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING = 'TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING';
export const TRN_SET_ACTIVATION_STATUS_SEARCH_STRING = 'TRN_SET_ACTIVATION_STATUS_SEARCH_STRING';
export const TRN_ACTIVATION_CAPTURED_IMAGE_STATUS='TRN_ACTIVATION_CAPTURED_IMAGE_STATUS';
export const TRN_ACTIVATION_GET_REJECTED_IMAGE='TRN_ACTIVATION_GET_REJECTED_IMAGE';
export const GET_ACTIVATION_STATUS_DETAILS = "GET_ACTIVATION_STATUS_DETAILS";

//Authentication App Constants 
export const AUTH_LOADING_INDICATOR = 'AUTH_LOADING_INDICATOR';
export const AUTH_SET_API_LOADING = 'AUTH_SET_API_LOADING';
export const AUTH_GETS_START_API_SUCCESS = 'AUTH_GETS_START_API_SUCCESS';
export const AUTH_GETS_START_API_FAIL = 'AUTH_GETS_START_API_FAIL';
export const AUTH_RESET_ALL = 'AUTH_RESET_ALL';

// General App Const Types
export const GET_GENARAL_SIM_NUMBER = 'genaral_get_sim_number';
export const GET_GENARAL_API_LOADING = 'genaral_api_loading';
export const GET_GENARAL_SIM_VALIDATION_STATUS = 'genaral_sim_validation_satus';
export const GET_GENARAL_SIM_VALIDATION_SUCCESS = 'genaral_sim_validation_api_success';
export const GET_GENARAL_SIM_VALIDATION_FAIL = 'genaral_sim_validation_api_fail';
export const RESET_SIM_VALIDATION_STATUS = 'genaral_reset_sim_validation_status';
export const SET_NUMBER_CHECK_STATUS = 'set_number_check_status';
export const RESET_GENARAL = 'genaral_reset_values';

//Common Const Types
export const COMMON_SET_API_LOADING = 'COMMON_SET_API_LOADING';
export const RESET_COMMON = 'RESET_COMMON';
export const COMMON_SET_KYC_NIC_FRONT = 'COMMON_SET_KYC_NIC_FRONT';
export const COMMON_SET_KYC_NIC_BACK = 'COMMON_SET_KYC_NIC_BACK';
export const COMMON_SET_KYC_PASSPORT = 'COMMON_SET_KYC_PASSPORT';
export const COMMON_SET_KYC_DRIVING_LICENCE = 'COMMON_SET_KYC_DRIVING_LICENCE';
export const COMMON_SET_KYC_PROOF_OF_BILLING = 'COMMON_SET_KYC_PROOF_OF_BILLING';
export const COMMON_SET_KYC_CUSTOMER_IMAGE = 'COMMON_SET_KYC_CUSTOMER_IMAGE';
export const COMMON_SET_KYC_SIGNATURE = 'COMMON_SET_KYC_SIGNATURE';
export const COMMON_SET_KYC_ADDITIONAL_POB = 'COMMON_SET_KYC_ADDITIONAL_POB';
export const COMMON_SET_ACTIVITY_START_TIME = 'COMMON_SET_ACTIVITY_START_TIME';
export const COMMON_SET_ACTIVITY_END_TIME = 'COMMON_SET_ACTIVITY_END_TIME';
export const RESET_KYC_IMAGES = 'RESET_KYC_IMAGES';
export const COMMON_RESET_KYC_SIGNATURE = 'COMMON_RESET_KYC_SIGNATURE';
export const COMMON_SET_BANNER_IMAGES = 'COMMON_SET_BANNER_IMAGES';

//Configuration Const Types
export const RESET_CONFIGURATION = 'RESET_CONFIGURATION';
export const CONFIG_SET_ACTIVITY_START_TIME = 'CONFIG_SET_ACTIVITY_START_TIME';
export const CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT = 'CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT';
export const GET_CONFIGURATION = "GET_CONFIGURATION";
export const CONFIG_LTE_SET_CONFIGURATION = "CONFIG_LTE_SET_CONFIGURATION";
export const CONFIG_LTE_SET_ACTIVATION_STATUS = "CONFIG_LTE_SET_ACTIVATION_STATUS";
export const CONFIG_DTV_SET_ACTIVATION_STATUS = "CONFIG_DTV_SET_ACTIVATION_STATUS";

/********************* LTE ACTIVATION TYPES***********************/
export const LTE_API_CALL = "lte_api_call";
export const RESET_LTE_DATA = "reset_lte_data";
export const LTE_SELECTED_CONNECTION_TYPE = "LTE_SELECTED_CONNECTION_TYPE";
export const LTE_SET_CUSTOMER_INFO_ID_TYPE = "LTE_SET_CUSTOMER_INFO_ID_TYPE";
export const LTE_SET_ID_NUMBER_VALIDATION_STATUS = "lte_set_id_number_validation_status";
export const LTE_SET_PRODUCT_INFO_VALIDATION_STATUS = "lte_set_product_info_validation_status";
export const LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS = "lte_set_customer_info_validation_status";
export const LTE_VISIBLE_SECTION_1 ='LTE_VISIBLE_SECTION_1';
export const LTE_VISIBLE_SECTION_2 ='LTE_VISIBLE_SECTION_2';
export const LTE_VISIBLE_SECTION_3 ='LTE_VISIBLE_SECTION_3';
export const LTE_SET_SECTIONS_VISIBLE = "LTE_SET_SECTIONS_VISIBLE";
export const LTE_INPUT_ID_NUMBER ='LTE_INPUT_ID_NUMBER';
export const LTE_SET_ID_NUMBER_ERROR ='LTE_SET_ID_NUMBER_ERROR';
export const LTE_INPUT_EZ_CASH_PIN ='LTE_INPUT_EZ_CASH_PIN';

export const LTE_SET_CUSTOMER_TYPE = "lte_set_customer_type";
export const LTE_SET_CUSTOMER_ID_INFO = "lte_set_customer_id_info";
export const LTE_SET_ID_TYPE = "lte_set_id_type";
export const LTE_SERIAL_SCAN_MODE = "LTE_SERIAL_SCAN_MODE";
export const LTE_ID_VALIDATION_SUCCESS = "LTE_ID_VALIDATION_SUCCESS";
export const LTE_ID_VALIDATION_FAIL = "LTE_ID_VALIDATION_FAIL";
export const LTE_OUTSTANDING_CHECK_SUCCESS = "LTE_OUTSTANDING_CHECK_SUCCESS";
export const LTE_OUTSTANDING_CHECK_FAIL = "LTE_OUTSTANDING_CHECK_FAIL";
export const LTE_OTP_VALIDATION_SUCCESS = "lte_otp_validation_success";
export const LTE_OTP_VALIDATION_FAIL = "lte_otp_validation_fail";
export const LTE_OTP_NUMBER = "lte_otp_number";
export const LTE_SERIAL_VALIDATION = "LTE_SERIAL_VALIDATION";
export const LTE_SERIAL_TEXT_VALUE = "LTE_SERIAL_TEXT_VALUE";
export const LTE_BUNDLE_SERIAL = "LTE_BUNDLE_SERIAL";
export const LTE_SERIAL_PACK = "LTE_SERIAL_PACK";
export const LTE_SIM_SERIAL = "LTE_SIM_SERIAL";
export const LTE_INPUT_ANALOG_PHONE_SERIAL = "LTE_INPUT_ANALOG_PHONE_SERIAL";
export const LTE_SET_SERIAL_TYPE_BUNDLE = "LTE_SET_SERIAL_TYPE_BUNDLE";
export const LTE_SET_SERIAL_TYPE_PACK = "LTE_SET_SERIAL_TYPE_PACK";
export const LTE_SET_SERIAL_TYPE_SIM = "LTE_SET_SERIAL_TYPE_SIM";
export const LTE_SET_SERIAL_TYPE_ANALOG_PHONE = "LTE_SET_SERIAL_TYPE_ANALOG_PHONE";
export const LTE_REST_ANALOG_PHONE_SERIAL_DATA = "LTE_REST_ANALOG_PHONE_SERIAL_DATA";
export const LTE_RESET_SERIALS = "LTE_RESET_SERIALS";
export const LTE_RESET_INDIVIDUAL_SERIAL_DATA = "LTE_RESET_INDIVIDUAL_SERIAL_DATA";
export const LTE_RESET_SERIALS_DATA = "LTE_RESET_SERIALS_DATA";
export const LTE_EZ_CASH_BALANCE = "LTE_EZ_CASH_BALANCE";
export const LTE_SET_RELOAD = "LTE_SET_RELOAD";
export const LTE_FORCE_UPDATE_PROPS = "LTE_FORCE_UPDATE_PROPS";
export const LTE_RESET_EZCASH_BALANCE = "LTE_RESET_EZCASH_BALANCE";
export const LTE_RESET_VOUCHER_VALUE = "LTE_RESET_VOUCHER_VALUE";
export const LTE_SET_DATA_PACKAGE_CODE = "LTE_SET_DATA_PACKAGE_CODE";
export const LTE_SET_VOICE_PACKAGE_CODE = "LTE_SET_VOICE_PACKAGE_CODE";

export const LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT = "LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT";
export const LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT = "LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT";
export const LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT = "LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT";
export const LTE_RESET_KYC_CHECK_BOX_ALL = "LTE_RESET_KYC_CHECK_BOX_ALL";
export const SET_LTE_VOUCHER_CODE_GROUP_NAME ="SET_LTE_VOUCHER_CODE_GROUP_NAME";
export const SET_LTE_VOUCHER_OFFER_CODE ="SET_LTE_VOUCHER_OFFER_CODE";

export const GET_LTE_OFFERS_LIST = "get_lte_offers_list";
export const GET_LTE_OFFER_DETAIL = 'get_lte_offer_detail';
export const SET_LTE_MULTIPLAY_OFFER_DATA = 'SET_LTE_MULTIPLAY_OFFER_DATA';
export const GET_LTE_VOUCHER_CODE_STATUS = 'get_lte_voucher_code_status'; //Sets if a voucher code is available for selected offer.
export const SET_LTE_VOUCHER_CODE = 'set_lte_voucher_code'; //Set user's input in voucher code field
export const SET_LTE_VOUCHER_CODE_VALIDITY = 'set_lte_voucher_code_validity'; //Sets if the user's entered voucher code is valid after validationg using the API call
export const SET_LTE_ANALOG_PHONE_VISIBILITY = 'set_lte_analog_phone_visibility'; // sets analog phone visibility depending on getLTEOfferDetails action resp

// export const VALIDATE_VOUCHER_CODE_TRUE = 'SET_LTE_VOUCHER_CODE_VALIDITY';
export const GET_LTE_DATA_PACKAGES_LIST = "get_lte_data_packages_list";
export const GET_LTE_DATA_PACKAGE_DETAIL = "get_lte_data_package_detail";
export const RESET_LTE_DATA_PACKAGES_DATA_RENTAL = "reset_lte_data_packages_data_rental";
export const GET_LTE_VOICE_PACKAGES_LIST = "get_lte_voice_packages_list";
export const GET_LTE_VOICE_PACKAGE_DETAIL = "get_lte_voice_package_detail";

export const SET_LTE_TELCO_AREA_DATA = "set_lte_telco_area_data";
export const SET_LTE_SELECTED_AREA = "set_lte_selected_area";

export const GET_LTE_PACKAGE_DEPOSITS_FAIL = 'get_lte_package_deposits_fail';
export const GET_LTE_PACKAGE_DEPOSITS_SUCCESS = 'get_lte_package_deposits_success';
export const SET_LTE_VOICE_CHECKBOX_DATA = "set_lte_voice_checkbox_data";

export const GET_EZCASH_CHECK_BALANCE_SUCCESS = "get_ezcash_check_balance_success";
export const GET_EZCASH_CHECK_BALANCE_FAIL = "get_ezcash_check_balance_fail";
export const GET_EZCASH_REVERSE_TRANSACTION_SUCCESS = "get_ezcash_reverse_transaction_success";
export const GET_EZCASH_REVERSE_TRANSACTION_FAIL = "get_ezcash_reverse_transaction_fail";
export const GET_EZCASH_SUBMIT_TRANSACTION_SUCCESS = "get_ezcash_submit_transaction_success";
export const GET_EZCASH_SUBMIT_TRANSACTION_FAIL = "get_ezcash_submit_transaction_fail";
export const LTE_INPUT_EMAIL = 'LTE_INPUT_EMAIL';
export const GET_LTE_DEVICE_SELLING_PRICE_SUCCESS = 'get_lte_device_selling_price_success';
export const GET_LTE_DEVICE_SELLING_PRICE_FAIL = 'get_lte_device_selling_price_fail';
export const LTE_SET_INPUT_LANLINE_NUMBER = 'lte_set_input_landline_number';
export const LTE_SET_INPUT_MOBILE_NUMBER = 'lte_set_input_mobile_Number';

export const LTE_RESERVE_NUMBER_POOL = "LTE_RESERVE_NUMBER_POOL";
export const LTE_BILLING_CYCLE_NUMBER = "LTE_BILLING_CYCLE_NUMBER";
export const SET_SELECTED_CONNECTION_NUMBER = "SET_SELECTED_CONNECTION_NUMBER";
export const RESET_SELECTED_CONNECTION_NUMBER = "RESET_SELECTED_CONNECTION_NUMBER";
export const LTE_SET_TOTAL_PAYMENT = "LTE_SET_TOTAL_PAYMENT";
export const LTE_SET_EZCASH_TOTAL_PAYMENT = "LTE_SET_EZCASH_TOTAL_PAYMENT";
export const SET_LTE_VALIDATED_VOUCHER_CODE = "SET_LTE_VALIDATED_VOUCHER_CODE";
export const LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS = "LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS";
export const LTE_SET_REFERENCE_ID = "LTE_SET_REFERENCE_ID";
export const LTE_SET_VOUCHER_CANCELLATION_STATUS = "LTE_SET_VOUCHER_CANCELLATION_STATUS";

/********************* DTV ACTIVATION TYPES***********************/
export const DTV_SET_API_LOADING = "DTV_SET_API_LOADING";
export const RESET_DTV_DATA = "RESET_DTV_DATA";
export const DTV_FORCE_UPDATE_VIEW = "DTV_FORCE_UPDATE_VIEW";
export const DTV_SET_CUSTOMER_TYPE = "DTV_SET_CUSTOMER_TYPE";
export const DTV_SELECTED_CONNECTION_TYPE = "DTV_SELECTED_CONNECTION_TYPE";
export const DTV_INPUT_ID_NUMBER = "DTV_INPUT_ID_NUMBER";
export const DTV_SET_ID_NUMBER_ERROR = "DTV_SET_ID_NUMBER_ERROR";
export const DTV_ID_VALIDATION_SUCCESS = "DTV_ID_VALIDATION_SUCCESS";
export const DTV_SET_TXN_REFERENCE = "DTV_SET_TXN_REFERENCE";
export const DTV_ID_VALIDATION_FAIL = "DTV_ID_VALIDATION_FAIL";
export const GET_DTV_DATA_PACKAGES_LIST = "GET_DTV_DATA_PACKAGES_LIST";
export const GET_DTV_DATA_PACKAGE_DETAIL = "GET_DTV_DATA_PACKAGE_DETAIL";
export const SET_DTV_DROPDOWN_INDEX = "SET_DTV_DROPDOWN_INDEX";
export const RESET_DTV_DROPDOWN_INDEX = "RESET_DTV_DROPDOWN_INDEX";
export const DTV_SET_SECTIONS_VISIBLE = "DTV_SET_SECTIONS_VISIBLE";
export const DTV_SET_ID_NUMBER_VALIDATION_STATUS = "DTV_SET_ID_NUMBER_VALIDATION_STATUS";
export const DTV_SET_PRODUCT_INFO_VALIDATION_STATUS = "DTV_SET_PRODUCT_INFO_VALIDATION_STATUS";
export const DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS = "DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS";
export const DTV_OTP_VALIDATION_SUCCESS = "DTV_OTP_VALIDATION_SUCCESS";
export const DTV_OTP_VALIDATION_FAIL = "DTV_OTP_VALIDATION_FAIL";
export const DTV_OUTSTANDING_CHECK_SUCCESS = "DTV_OUTSTANDING_CHECK_SUCCESS";
export const DTV_SET_TOTAL_OUTSTANDING_AMOUNT = "DTV_SET_TOTAL_OUTSTANDING_AMOUNT";
export const DTV_OUTSTANDING_CHECK_FAIL = "DTV_OUTSTANDING_CHECK_FAIL";
export const DTV_PACK_TYPE_NUMBER = "DTV_PACK_TYPE_NUMBER";
export const DTV_PACK_TYPE = "DTV_PACK_TYPE";
export const DTV_SAME_DAY_INSTALLATION_DATA = "DTV_SAME_DAY_INSTALLATION_DATA";
export const DTV_FULLFILLMENT_TYPE = "DTV_FULLFILLMENT_TYPE";
export const RESET_DTV_FULLFILLMENT_TYPE = "RESET_DTV_FULLFILLMENT_TYPE";
export const DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA = "DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA";
export const DTV_SET_CHANNEL_TYPE = "DTV_SET_CHANNEL_TYPE";
export const DTV_SET_CHANNEL_GENRE = "DTV_SET_CHANNEL_GENRE";
export const DTV_SELECTED_CHANNELS_N_PACKS = "DTV_SELECTED_CHANNELS_N_PACKS";
export const DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE = "DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE";
export const DTV_SERIAL_VALIDATION = "DTV_SERIAL_VALIDATION";
export const DTV_SERIAL_TEXT_VALUE = "DTV_SERIAL_TEXT_VALUE";
export const DTV_SET_SERIAL_TYPE_BUNDLE = "DTV_SET_SERIAL_TYPE_BUNDLE";
export const DTV_SET_SERIAL_TYPE_PACK = "DTV_SET_SERIAL_TYPE_PACK";
export const DTV_SET_SERIAL_TYPE_ACCESSORIES = "DTV_SET_SERIAL_TYPE_ACCESSORIES";
export const DTV_ACCESSORIES_SERIAL = "DTV_ACCESSORIES_SERIAL";
export const DTV_SERIAL_PACK = "DTV_SERIAL_PACK";
export const DTV_BUNDLE_SERIAL = "DTV_BUNDLE_SERIAL";
export const DTV_RESET_INDIVIDUAL_SERIAL_DATA = "DTV_RESET_INDIVIDUAL_SERIAL_DATA";
export const DTV_RESET_SERIALS_DATA = "DTV_RESET_SERIALS_DATA";
export const DTV_GET_OFFERS = "DTV_GET_OFFERS";
export const DTV_RESET_OFFERS = "DTV_RESET_OFFERS";
export const DTV_SET_STB_UPGRADE_DATA = "DTV_SET_STB_UPGRADE_DATA";
export const DTV_STB_UPGRADE_CHECKBOX = "DTV_STB_UPGRADE_CHECKBOX";
export const DTV_SET_WARRANTY_EXTENSION_DATA = "DTV_SET_WARRANTY_EXTENSION_DATA";
export const DTV_SET_FIRST_RELOAD_VALIDATION_ERROR = "DTV_SET_FIRST_RELOAD_VALIDATION_ERROR";
export const DTV_SET_SHOW_VOUCHER_CODE_FIELD = "DTV_SET_SHOW_VOUCHER_CODE_FIELD";
export const DTV_SET_CUSTOMER_INFO_ID_TYPE = "DTV_SET_CUSTOMER_INFO_ID_TYPE";
export const DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT = "DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT";
export const DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT = "DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT";
export const DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION = "DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION";
export const DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT = "DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT";
export const DTV_RESET_KYC_CHECK_BOX_ALL = "DTV_RESET_KYC_CHECK_BOX_ALL";
export const SET_DTV_VOUCHER_CODE = "SET_DTV_VOUCHER_CODE";
export const SET_DTV_VOUCHER_CODE_VALIDITY = "SET_DTV_VOUCHER_CODE_VALIDITY";
export const DTV_EOAF_SERIAL = "DTV_EOAF_SERIAL";
export const DTV_RESET_EOAF_SERIAL = "DTV_RESET_EOAF_SERIAL";
export const DTV_SET_INPUT_MOBILE_NUMBER = "DTV_SET_INPUT_MOBILE_NUMBER";
export const DTV_SET_CUSTOMER_NAME = "DTV_SET_CUSTOMER_NAME";
export const DTV_SET_CUSTOMER_ADDRESS_LINE1 = "DTV_SET_CUSTOMER_ADDRESS_LINE1";
export const DTV_SET_CUSTOMER_ADDRESS_LINE2 = "DTV_SET_CUSTOMER_ADDRESS_LINE2";
export const DTV_SET_CUSTOMER_ADDRESS_LINE3 = "DTV_SET_CUSTOMER_ADDRESS_LINE3";
export const DTV_SET_CUSTOMER_CITY = "DTV_SET_CUSTOMER_CITY";
export const DTV_CITY_SEARCH_DATA = "DTV_CITY_SEARCH_DATA";
export const DTV_SET_RELOAD_AMOUNT = "DTV_SET_RELOAD_AMOUNT";
export const DTV_RESET_VOUCHER_VALUE = "DTV_RESET_VOUCHER_VALUE";
export const DTV_CUSTOMER_TOTAL_PAYMENT = "DTV_CUSTOMER_TOTAL_PAYMENT";
export const DTV_EZ_CASH_BALANCE = "DTV_EZ_CASH_BALANCE";
export const DTV_RESET_EZCASH_BALANCE = "DTV_RESET_EZCASH_BALANCE";
export const DTV_SET_INPUT_EZ_CASH_PIN = "DTV_SET_INPUT_EZ_CASH_PIN";
export const DTV_RESET_SAMEDAY_INSTALLATION_STATUS = "DTV_RESET_SAMEDAY_INSTALLATION_STATUS";
export const DTV_INPUT_EMAIL = "DTV_INPUT_EMAIL";
export const DTV_SET_INPUT_LANLINE_NUMBER = "DTV_SET_INPUT_LANLINE_NUMBER";
export const DTV_SET_VOUCHER_CANCELLATION_STATUS = "DTV_SET_VOUCHER_CANCELLATION_STATUS";
export const DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS = "DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS";
export const SET_DTV_VOUCHER_CODE_ONLY = "SET_DTV_VOUCHER_CODE_ONLY";
export const RESET_DTV_VOUCHER_CODE_DATA = "RESET_DTV_VOUCHER_CODE_DATA";
export const DTV_OTP_NUMBER = "dtv_otp_number";
//=========================== CFSS related =====================================================
//Stock Acceptance
export const GET_STOCK_ACCEPTANCE_API = 'stock_acceptance_get_api';
export const GET_STOCK_ACCEPTANCE_DETAILS_API = 'stock_acceptance_get_api_details';
export const GET_STOCK_ACCEPTANCE_ELEMENT_LIST = 'stock_acceptance_element_list';
export const GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_LIST = 'stock_acceptance_issued_serial_list';
export const GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_LIST = 'stock_acceptance_scanned_serial_list';
export const GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_STATUS = 'stock_acceptance_issued_serial_status';
export const GET_STOCK_ACCEPTANCE_SESSION_SERIALS = 'stock_acceptance_session_serials';
export const GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_CHECK = 'stock_acceptance_scanned_serial_check';
export const GET_STOCK_ACCEPTANCE_MISMATCHED_SERIALS = 'stock_acceptance_mismatched_serials';
export const GET_STOCK_ACCEPTANCE_SELECTED_STO_ID = 'stock_acceptance_selected_sto_id';

//Accessory Sales
export const ACCESSORY_SALE_DETAIL = 'accessory_sale_detail';
export const ACCESSORY_SALES_EZ_CASH_PIN = 'accessory_sales_get_ez_cash_pin';
export const ACCESSORY_SALES_RESET = 'accessory_sales_reset';

//Warranty Replacement
export const WARRANTY_REPLACEMENT_DETAILS = 'warranty_replacement_details';
export const WARRANTY_REPLACEMENT_GET_ITEM_TYPE = 'warranty_replacement_get_item_type';
export const WARRANTY_REPLACEMENT_ITEM_CODE = 'warranty_replacement_get_item_code';
export const WARRANTY_REPLACEMENT_CONNECTION_TYPE = 'warranty_replacement_get_connection_type';
export const WARRANTY_REPLACEMENT_RESET = 'warranty_replacement_reset';

//Pending Work Order
export const WORK_ORDER_DETAIL = 'work_order_detail';
export const WORK_ORDER_CUSTOMER_SIGNATURE = 'work_order_customer_signature';
export const WORK_ORDER_DTV_PROVISIONING = 'work_order_dtv_provisioning';
export const WORK_ORDER_PROVISIONING_STATUS = 'work_order_provisioning_status';
export const WORK_ORDER_INSTALLED_ITEMS = 'work_order_installed_items';
export const WORK_ORDER_SUMMARY_RELOAD = 'work_order_summary_reload';
export const WORK_ORDER_RESET = 'work_order_reset';
export const WORK_ORDER_CUSTOMER_FEEDBACK = 'work_order_customer_feedback';
export const WORK_ORDER_CUSTOMER_ANSWERS = 'work_order_customer_answers';
export const WORK_ODER_DTV_INSTALLATION = 'work_oder_dtv_installation';
export const WORK_ORDER_SERIAL_LIST = 'work_order_serial_list';
export const WORK_ORDER_SIGNATURE_DATA = 'work_order_signature_data';
export const WORK_ORDER_ROOT_CAUSES = 'work_order_root_causes';
export const WORK_ORDER_APP_SCREEN = 'work_order_app_screen';
export const WORK_ORDER_APP_FLOW = 'work_order_app_flow';
export const WORK_ORDER_TRB_ACCESSORY_SALE = 'work_order_trb_accessory_sale';
export const CLEAR_STATES = 'clear_wo_states'; 
export const CLEAR_ROOT_CAUSE_DATA = 'clear_root_cause_data';
export const WORK_ORDER_REFIX_WARRANTY_DETAILS = 'work_order_refix_warranty_details';
export const WORK_ORDER_JOB_STATUS = 'WORK_ORDER_JOB_STATUS';
export const PAYMENT_DETAILS = 'PAYMENT_DETAILS';
export const ADD_ADDITIONAL_HYBRID_PROD = 'work_order_add_additional_hybrid_prod';
export const CLEAR_TRB_WO = 'clear_trb_work_order';
export const UPDATE_WORK_ORDER_ROOT_CAUSES = 'UPDATE_WORK_ORDER_ROOT_CAUSES';
export const GET_DTV_DELIVERY_CHARGE = 'GET_DTV_DELIVERY_CHARGE';
export const DTV_DEPOSIT_AMOUNT = 'DTV_DEPOSIT_AMOUNT';
export const DTV_RESET_DEPOSIT_AMOUNT = 'DTV_RESET_DEPOSIT_AMOUNT';
export const JOB_HISTORY_DETAILS = 'JOB_HISTORY_DETAILS';

// for SIM STB CHANGE
export const SIM_STB_CHANGE_COUNT = 'sim_stb_change_count';

//=========================== CFSS related =====================================================

