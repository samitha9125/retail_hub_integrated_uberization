/*
 * File: index.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author:  Parthipan Kugadoss (parthipan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:22:11 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Parthipan Kugadoss (parthipan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 *              Arafath Misree (arafath@omobio.net)
 *              Manojkanth Rajendran (manojkanthan.rajendran@omobio.net)
 *              Devanshani Rasanjika (devanshani@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import { Keyboard } from 'react-native';
import Utills from '../utills/Utills';
import { Constants } from '../config';
import { Analytics, ApiRequestUtils, Screen, FuncUtils, FormatUtils, MessageUtils } from '../utills/';
import Global from '../config/globalConfig';
import _ from 'lodash';

import * as AUTH_ACTION from './authActions';
import * as DTV_ACTION from './dtvActions';
import * as COMMON_ACTION from './commonActions';

const Utill = new Utills();

import {

  GET_PIN,
  GET_CONNECTION_TYPE_BILL,
  GET_CONNECTION_NUMBER_BILL,
  GET_AMOUNT_BILL,
  GET_EZ_CASH_PIN_BILL,
  GET_SIM_NUMBER_SIM,
  GET_MOBILE_NUMBER_SIM,
  GET_ID_NUMBER_SIM,
  GET_PRE_POST_MOBILE_ACT,
  GET_LOCAL_FOREIGN_MOBILE_ACT,
  GET_INFORMATION_ID_TYPE_MOBILE_ACT,
  GET_NAME_MOBILE_ACT,
  GET_SIM_NUMBER_MOBILE_ACT,
  GET_ID_NUMBER_MOBILE_ACT,
  GET_MOBILE_NUMBER_MOBILE_ACT,
  GET_FIRST_RELOAD_MOBILE_ACT,
  GET_ALTERNATE_CONTACT_NUMBER_MOBILE_ACT,
  GET_EMAIL_ADDRESS_MOBILE_ACT,
  SIM_VALIDATION_SUCCESS,
  SIM_VALIDATION_FAIL,
  SIM_VALIDATION_FAIL_RESET,
  SIM_API_FAIL_RESET,
  NEW_CONN_ACTIVATION_SUCCESS,
  NEW_CONN_ACTIVATION_FAIL,
  MOBILE_ACT_NIC_VALIDATION_SUCCESS,
  MOBILE_ACT_NIC_VALIDATION_FAIL,
  MOBILE_ACT_OTP_VALIDATION_SUCCESS,
  MOBILE_ACT_OTP_VALIDATION_FAIL,
  MOBILE_ACT_M_CONNECT_VALIDATION_SUCCESS,
  MOBILE_ACT_M_CONNECT_VALIDATION_FAIL,
  MOBILE_ACT_MODEL_POPPED,
  MOBILE_ACT_OTP_POPPED,
  MOBILE_ACT_M_CONNECT_POPPED,
  GET_RELOAD_OFFER_CODE_MOBILE_ACT,
  GET_OTP_NUMBER,
  GET_OTP_STATUS_MOBILE_ACT,
  GET_INFORMATION_NOT_CLEAR_MOBILE_ACT,
  GET_ADDRESS_DIFFERENT_MOBILE_ACT,
  GET_SKIP_VALIDATION_MOBILE_ACT,
  GET_BLOCKED_ID_MOBILE_ACT,
  GET_MOBILE_ACT_SIGNATURE,
  GET_MOBILE_ACT_KYC_CAPTURE,
  GET_MOBILE_ACT_KYC_NIC_1,
  GET_MOBILE_ACT_KYC_NIC_2,
  GET_MOBILE_ACT_POB_KYC_CAPTURE,
  GET_MOBILE_ACT_CUSTOMER_KYC_CAPTURE,
  GET_MOBILE_ACT_RESET_IMAGES,
  GET_MOBILE_ACT_CAPTURE_TYPE_NO,
  GET_MOBILE_ACT_RE_CAPTURE_TYPE_NO,
  GET_MOBILE_ACT_UNIQUE_TX_ID,
  GET_API_RESPONSE_MOBILE_ACT,
  GET_API_LOADING,
  GET_CURRENT_LANGUAGE,
  GET_IS_CAPTURED_MOBILE_ACT,
  GET_CURRENT_PIN,
  GET_NEW_PIN,
  GET_CONFIRMED_PIN,
  RESET_PIN,
  FOCUS_EZ_CASH_PIN,
  SET_TOTAL_PAYMENT_DATA,
  SET_PAYMENT_INFO,
  RESET_PAYMENT_INFO,
  //RESET,
  GET_SELECTED_PACKAGE,
  GET_PACKAGE_DEPOSITS_SUCCESS,
  GET_PACKAGE_DEPOSITS_FAIL,
  GET_EZ_CASH_PIN_MOBILE_ACT,
  GET_TOTAL_PAYMENT,
  IS_PACKAGE_LOADED,
  SELECTED_PACKAGE_RESET,
  LOCAL_CALL_DEPOSITS_RESET,
  GET_SELECTED_PACKAGE_INDEX,
  USER_SELECTED_PACKAGE_INDEX_RESET,
  RESET_EZ_CASH_PIN,
  GET_MOBILE_ACT_LOADING,
  GET_MOBILE_ACT_API_LOADING,
  GET_DOC_99_URL,
  GET_EZCASH_SCHEME_URL,
  GET_EZCASH_URL,
  GET_TILE_DATA,
  //Delivery App Constants
  DELIVERY_SET_API_CALL,
  GET_PENDING_WORK_ORDERS_SUCCESS,
  GET_PENDING_WORK_ORDERS_FAIL,
  GET_WORK_ORDER_STATUS,
  API_CALL_SUCCESS,
  API_CALL_FAIL,
  RESET_DELIVERY,
  GET_ALLOWED_SERVICES,
  SET_RETAILER,
  GET_FILTER_WORK_ORDERS,
  GET_CURRENT_WO_ID,
  GET_CURRENT_WO_JOBID,
  GET_CURRENT_WO_JOB_STATUS,
  GET_CURRENT_WO_SLA_BREACH_STATUS,
  GET_WOM_API_ORDER,
  DELIVERY_SHOW_LOADING_INDICATER,
  DELIVERY_HIDE_LOADING_INDICATER,
  DELIVERY_API_CALL_FAIL,
  GET_CURRENT_WO_BASIC_INFO,
  DELIVERY_GET_WO_COUNT,
  DELIVERY_CCBS_ORDER_ID,

  //Transaction History Const Types
  TRN_LOADING_INDICATOR,
  RESET_ALL_TRANS_HISTORY,
  TRN_SIM_CHANGE_HISTORY_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_FAIL,
  TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_PENDING_FAIL,
  TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL,
  TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM,
  TRN_BILL_PAYMENT_HISTORY_SUCCESS,
  TRN_BILL_PAYMENT_HISTORY_FAIL,
  TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM,
  TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING,
  TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS,
  TRN_ACTIVATION_CAPTURED_IMAGE_STATUS,
  // TRN_ACTIVATION_GET_REJECTED_IMAGE,
  // TRN_BILL_PAYMENT_HISTORY_GET_RESULTS,
  TRN_ACTIVATION_STATUS_ALL_SUCCESS,
  TRN_ACTIVATION_RESET_ALL_LIST,
  TRN_ACTIVATION_STATUS_ALL_FAIL,
  TRN_ACTIVATION_STATUS_REJECTED_SUCCESS,
  TRN_ACTIVATION_STATUS_REJECTED_FAIL,
  TRN_ACTIVATION_STATUS_SELECTED_ITEM,
  TRN_ACTIVATION_STATUS_FILTERS,
  TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING,
  // TRN_SIM_CHANGE_HISTORY_GET_RESULTS,
  TRN_SIM_CHANGE_HISTORY_RESET_LISTS,
  TRN_ACTIVATION_RESET_REJECTED_LIST,
  TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING,
  TRN_SET_ACTIVATION_STATUS_SEARCH_STRING,
  GET_ACTIVATION_STATUS_DETAILS,

  //Authentication App Constants 
  AUTH_LOADING_INDICATOR,
  AUTH_RESET_ALL,

  //Genaral App Constants
  GET_GENARAL_SIM_NUMBER,
  GET_GENARAL_API_LOADING,
  GET_GENARAL_SIM_VALIDATION_STATUS,
  GET_GENARAL_SIM_VALIDATION_SUCCESS,
  GET_GENARAL_SIM_VALIDATION_FAIL,
  RESET_SIM_VALIDATION_STATUS,
  SET_NUMBER_CHECK_STATUS,
  RESET_GENARAL,
  TRN_SIM_CHANGE_HISTORY_RESET_RESULTS,
  TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING,

  //Common App Constants
  COMMON_SET_API_LOADING,
  RESET_COMMON,
  COMMON_SET_KYC_NIC_FRONT,
  COMMON_SET_KYC_NIC_BACK,
  COMMON_SET_KYC_PASSPORT,
  COMMON_SET_KYC_DRIVING_LICENCE,
  COMMON_SET_KYC_PROOF_OF_BILLING,
  COMMON_SET_KYC_CUSTOMER_IMAGE,
  COMMON_SET_KYC_SIGNATURE,
  COMMON_SET_KYC_ADDITIONAL_POB,
  RESET_KYC_IMAGES,
  COMMON_RESET_KYC_SIGNATURE,

  //LTE App Constants
  RESET_LTE_DATA,
  LTE_API_CALL,
  LTE_FORCE_UPDATE_PROPS,
  LTE_SET_CUSTOMER_TYPE,
  LTE_SELECTED_CONNECTION_TYPE,
  LTE_SET_ID_TYPE,
  LTE_SET_CUSTOMER_INFO_ID_TYPE,
  LTE_SET_ID_NUMBER_VALIDATION_STATUS,
  LTE_SET_PRODUCT_INFO_VALIDATION_STATUS,
  LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS,
  LTE_SET_SECTIONS_VISIBLE,
  LTE_VISIBLE_SECTION_1,
  LTE_VISIBLE_SECTION_2,
  LTE_VISIBLE_SECTION_3,
  LTE_INPUT_ID_NUMBER,
  LTE_SET_ID_NUMBER_ERROR,
  LTE_INPUT_EZ_CASH_PIN,
  LTE_SET_CUSTOMER_ID_INFO,
  LTE_ID_VALIDATION_SUCCESS,
  LTE_ID_VALIDATION_FAIL,
  LTE_OUTSTANDING_CHECK_SUCCESS,
  LTE_OUTSTANDING_CHECK_FAIL,
  LTE_OTP_VALIDATION_SUCCESS,
  LTE_OTP_VALIDATION_FAIL,
  LTE_OTP_NUMBER,
  LTE_SERIAL_SCAN_MODE,
  LTE_SERIAL_VALIDATION,
  LTE_SERIAL_TEXT_VALUE,
  LTE_BUNDLE_SERIAL,
  LTE_SERIAL_PACK,
  LTE_SIM_SERIAL,
  LTE_INPUT_ANALOG_PHONE_SERIAL,
  LTE_SET_SERIAL_TYPE_BUNDLE,
  LTE_SET_SERIAL_TYPE_PACK,
  LTE_SET_SERIAL_TYPE_SIM,
  LTE_SET_SERIAL_TYPE_ANALOG_PHONE,
  LTE_REST_ANALOG_PHONE_SERIAL_DATA,
  LTE_RESET_SERIALS,
  LTE_RESET_SERIALS_DATA,
  LTE_RESET_INDIVIDUAL_SERIAL_DATA,
  LTE_EZ_CASH_BALANCE,
  LTE_SET_RELOAD,
  GET_LTE_OFFERS_LIST,
  GET_LTE_OFFER_DETAIL,
  SET_LTE_MULTIPLAY_OFFER_DATA,
  GET_LTE_DATA_PACKAGES_LIST,
  GET_LTE_DATA_PACKAGE_DETAIL,
  GET_LTE_VOICE_PACKAGES_LIST,
  GET_LTE_VOICE_PACKAGE_DETAIL,
  GET_LTE_VOUCHER_CODE_STATUS,
  SET_LTE_ANALOG_PHONE_VISIBILITY,
  SET_LTE_VOUCHER_CODE,
  SET_LTE_VALIDATED_VOUCHER_CODE,
  SET_LTE_VOUCHER_CODE_VALIDITY,
  // VALIDATE_VOUCHER_CODE_TRUE,
  SET_LTE_TELCO_AREA_DATA,
  SET_LTE_SELECTED_AREA,
  GET_LTE_PACKAGE_DEPOSITS_FAIL,
  GET_LTE_PACKAGE_DEPOSITS_SUCCESS,
  LTE_SET_DATA_PACKAGE_CODE,
  LTE_SET_VOICE_PACKAGE_CODE,

  LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT,
  LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
  LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT,
  LTE_RESET_KYC_CHECK_BOX_ALL,

  SET_LTE_VOICE_CHECKBOX_DATA,
  GET_EZCASH_CHECK_BALANCE_SUCCESS,
  GET_EZCASH_CHECK_BALANCE_FAIL,
  GET_EZCASH_REVERSE_TRANSACTION_SUCCESS,
  GET_EZCASH_REVERSE_TRANSACTION_FAIL,
  GET_EZCASH_SUBMIT_TRANSACTION_SUCCESS,
  GET_EZCASH_SUBMIT_TRANSACTION_FAIL,
  LTE_INPUT_EMAIL,
  GET_LTE_DEVICE_SELLING_PRICE_SUCCESS,
  GET_LTE_DEVICE_SELLING_PRICE_FAIL,
  LTE_SET_INPUT_LANLINE_NUMBER,
  LTE_SET_INPUT_MOBILE_NUMBER,
  LTE_RESERVE_NUMBER_POOL,
  LTE_BILLING_CYCLE_NUMBER,
  SET_SELECTED_CONNECTION_NUMBER,
  RESET_SELECTED_CONNECTION_NUMBER,
  LTE_SET_TOTAL_PAYMENT,
  LTE_SET_REFERENCE_ID,
  SET_LTE_VOUCHER_CODE_GROUP_NAME,
  SET_LTE_VOUCHER_OFFER_CODE,
  LTE_SET_EZCASH_TOTAL_PAYMENT,
  LTE_RESET_EZCASH_BALANCE,
  LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS,
  LTE_RESET_VOUCHER_VALUE,
  RESET_LTE_DATA_PACKAGES_DATA_RENTAL,
  LTE_SET_VOUCHER_CANCELLATION_STATUS,

  //Config App Constants
  CONFIG_SET_ACTIVITY_START_TIME,
  CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT,
  CONFIG_LTE_SET_CONFIGURATION,
  GET_CONFIGURATION,
  CONFIG_LTE_SET_ACTIVATION_STATUS,
  CONFIG_DTV_SET_ACTIVATION_STATUS,

  //DTV App constants
  DTV_SET_API_LOADING,
  RESET_DTV_DATA,
  DTV_SET_CUSTOMER_TYPE,
  DTV_SELECTED_CONNECTION_TYPE,
  DTV_INPUT_ID_NUMBER,
  DTV_SET_ID_NUMBER_ERROR,
  DTV_ID_VALIDATION_FAIL,
  DTV_ID_VALIDATION_SUCCESS,
  DTV_SET_TXN_REFERENCE,
  DTV_SET_ID_NUMBER_VALIDATION_STATUS,
  DTV_SET_PRODUCT_INFO_VALIDATION_STATUS,
  DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS,
  DTV_SET_SECTIONS_VISIBLE,
  GET_DTV_DATA_PACKAGES_LIST,
  GET_DTV_DATA_PACKAGE_DETAIL,
  GET_DTV_DELIVERY_CHARGE,
  SET_DTV_DROPDOWN_INDEX,
  DTV_OTP_VALIDATION_SUCCESS,
  DTV_OTP_VALIDATION_FAIL,
  DTV_OUTSTANDING_CHECK_SUCCESS,
  DTV_OUTSTANDING_CHECK_FAIL,
  DTV_PACK_TYPE,
  DTV_FULLFILLMENT_TYPE,
  RESET_DTV_FULLFILLMENT_TYPE,
  DTV_SAME_DAY_INSTALLATION_DATA,
  DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA,
  DTV_SET_CHANNEL_TYPE,
  DTV_SET_CHANNEL_GENRE,
  DTV_SELECTED_CHANNELS_N_PACKS,
  DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE,
  DTV_SERIAL_VALIDATION,
  DTV_SERIAL_TEXT_VALUE,
  DTV_SET_SERIAL_TYPE_BUNDLE,
  DTV_SET_SERIAL_TYPE_PACK,
  DTV_SET_SERIAL_TYPE_ACCESSORIES,
  DTV_GET_OFFERS,
  DTV_RESET_OFFERS,
  DTV_STB_UPGRADE_CHECKBOX,
  DTV_SET_STB_UPGRADE_DATA,
  DTV_SET_WARRANTY_EXTENSION_DATA,
  DTV_SET_FIRST_RELOAD_VALIDATION_ERROR,
  DTV_SET_SHOW_VOUCHER_CODE_FIELD,
  DTV_BUNDLE_SERIAL,
  DTV_SERIAL_PACK,
  DTV_ACCESSORIES_SERIAL,
  DTV_RESET_INDIVIDUAL_SERIAL_DATA,
  DTV_RESET_SERIALS_DATA,
  DTV_SET_CUSTOMER_INFO_ID_TYPE,
  DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION,
  DTV_RESET_KYC_CHECK_BOX_ALL,
  SET_DTV_VOUCHER_CODE,
  SET_DTV_VOUCHER_CODE_VALIDITY,
  DTV_EOAF_SERIAL,
  DTV_RESET_EOAF_SERIAL,
  DTV_SET_INPUT_MOBILE_NUMBER,
  DTV_SET_RELOAD_AMOUNT,
  DTV_RESET_VOUCHER_VALUE,
  DTV_CUSTOMER_TOTAL_PAYMENT,
  DTV_INPUT_EMAIL,
  DTV_SET_INPUT_LANLINE_NUMBER,
  DTV_SET_VOUCHER_CANCELLATION_STATUS,
  // ======================= CFSS related ============================
  //Stock Acceptance Constants
  GET_STOCK_ACCEPTANCE_API,
  GET_STOCK_ACCEPTANCE_DETAILS_API,
  GET_STOCK_ACCEPTANCE_ELEMENT_LIST,
  GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_LIST,
  GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_LIST,
  GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_STATUS,
  GET_STOCK_ACCEPTANCE_SESSION_SERIALS,
  GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_CHECK,
  GET_STOCK_ACCEPTANCE_MISMATCHED_SERIALS,
  GET_STOCK_ACCEPTANCE_SELECTED_STO_ID,

  //AccessorySales Constants
  ACCESSORY_SALE_DETAIL,
  ACCESSORY_SALES_EZ_CASH_PIN,
  ACCESSORY_SALES_RESET,

  //Warranty Replacement Constants
  WARRANTY_REPLACEMENT_DETAILS,
  WARRANTY_REPLACEMENT_GET_ITEM_TYPE,
  WARRANTY_REPLACEMENT_OFFER_CODE,
  WARRANTY_REPLACEMENT_ITEM_CODE,
  WARRANTY_REPLACEMENT_CONNECTION_TYPE,
  WARRANTY_REPLACEMENT_RESET,

  //Pending Work Orders
  WORK_ORDER_DETAIL,
  WORK_ORDER_CUSTOMER_SIGNATURE,
  WORK_ORDER_SIGNATURE_DATA,
  WORK_ORDER_DTV_PROVISIONING,
  WORK_ORDER_PROVISIONING_STATUS,
  WORK_ORDER_INSTALLED_ITEMS,
  WORK_ORDER_SERIAL_LIST,
  WORK_ORDER_SUMMARY_RELOAD,
  WORK_ORDER_RESET,
  WORK_ORDER_CUSTOMER_FEEDBACK,
  WORK_ORDER_CUSTOMER_ANSWERS,
  WORK_ODER_DTV_INSTALLATION,
  WORK_ORDER_ROOT_CAUSES,
  WORK_ORDER_APP_SCREEN,
  WORK_ORDER_APP_FLOW,
  WORK_ORDER_TRB_ACCESSORY_SALE,
  CLEAR_STATES,
  CLEAR_ROOT_CAUSE_DATA,
  WORK_ORDER_REFIX_WARRANTY_DETAILS,
  WORK_ORDER_JOB_STATUS,
  PAYMENT_DETAILS,
  ADD_ADDITIONAL_HYBRID_PROD,
  CLEAR_TRB_WO,
  UPDATE_WORK_ORDER_ROOT_CAUSES,
  JOB_HISTORY_DETAILS,

  //STB SIM Change
  SIM_STB_CHANGE_COUNT,
  SET_IS_CFSS,
  DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS,
  SET_DTV_VOUCHER_CODE_ONLY,
  DTV_SET_TOTAL_OUTSTANDING_AMOUNT,

  DTV_OTP_NUMBER,

  //DTV_DEPOSIT_AMOUNT
  DTV_DEPOSIT_AMOUNT,
  DTV_RESET_DEPOSIT_AMOUNT
  // =======================END CFSS related ============================

} from '../actions/types';

export const getPin = (input) => {

  return { type: GET_PIN, payLoad: input };

};

export const lteSetTotalPayment = (input) => {

  return { type: LTE_SET_TOTAL_PAYMENT, payLoad: input };

};

export const lteSetEzCashTotalPayment = (input) => {

  return { type: LTE_SET_EZCASH_TOTAL_PAYMENT, payLoad: input };

};

//Set activity start time
export const configSetActivityStartTime = () => {
  let timestamp = Math.round((new Date()).getTime() / 1000);
  console.log('Redux :: configSetActivityStartTime -', timestamp);
  return { type: CONFIG_SET_ACTIVITY_START_TIME, payLoad: timestamp };

};

export const configSetLteActivationStatus = (status) => {
  console.log('Redux :: CONFIG_LTE_SET_ACTIVATION_STATUS -');
  return { type: CONFIG_LTE_SET_ACTIVATION_STATUS, payLoad: status };

};

export const configSetDtvActivationStatus = (status) => {
  console.log('Redux :: CONFIG_DTV_SET_ACTIVATION_STATUS -');
  return { type: CONFIG_DTV_SET_ACTIVATION_STATUS, payLoad: status };

};

export const resetVoucerValue = () => {

  return { type: LTE_RESET_VOUCHER_VALUE, payLoad: null };

};

export const resetDataPackageRental = () => {

  return { type: RESET_LTE_DATA_PACKAGES_DATA_RENTAL, payload: null };

};

export const configSetDefaultEzCashAccount = (value) => {
  console.log('xxx configSetDefaultEzCashAccount');
  return { type: CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT, payLoad: value };

};

export const getApiLoading = (input) => {

  return { type: GET_API_LOADING, payLoad: input };

};

export const getConnectionTypeBill = (input) => {

  return { type: GET_CONNECTION_TYPE_BILL, payLoad: input };

};

export const getConnectionNumberBill = (input) => {

  return { type: GET_CONNECTION_NUMBER_BILL, payLoad: input };

};

export const getEzCashPinBill = (input) => {

  return { type: GET_EZ_CASH_PIN_BILL, payLoad: input };

};

export const getEzCashPinMobileAct = (input) => {

  return { type: GET_EZ_CASH_PIN_MOBILE_ACT, payLoad: input };

};

export const resetEzCashPin = () => {

  return { type: RESET_EZ_CASH_PIN, payLoad: '' };

};

export const getTotalPayment = (input) => {

  return { type: GET_TOTAL_PAYMENT, payLoad: input };

};

export const getAmountBill = (input) => {

  return { type: GET_AMOUNT_BILL, payLoad: input };

};

export const getSimNumberSimChange = (input) => {

  return { type: GET_SIM_NUMBER_SIM, payLoad: input };

};

export const getIdNumberSimChange = (input) => {

  return { type: GET_ID_NUMBER_SIM, payLoad: input };

};

export const getMobileNumberSimChange = (input) => {

  return { type: GET_MOBILE_NUMBER_SIM, payLoad: input };

};

export const getPrePostMobileAct = (input) => {

  return { type: GET_PRE_POST_MOBILE_ACT, payLoad: input };

};

export const getLocalForeignMobileAct = (input) => {

  return { type: GET_LOCAL_FOREIGN_MOBILE_ACT, payLoad: input };

};

export const setTotalPaymentData = (input) => {
  return { type: SET_TOTAL_PAYMENT_DATA, payLoad: input };
};

export const getIdNumberMobileAct = (input) => {

  return { type: GET_ID_NUMBER_MOBILE_ACT, payLoad: input };

};

export const getSimNumberMobileAct = (input) => {

  return { type: GET_SIM_NUMBER_MOBILE_ACT, payLoad: input };

};

export const getInformationIdTypeMobileAct = (input) => {

  return { type: GET_INFORMATION_ID_TYPE_MOBILE_ACT, payLoad: input };

};

export const getNameMobileAct = (input) => {

  return { type: GET_NAME_MOBILE_ACT, payLoad: input };

};

export const getMobileNumberMobileAct = (input) => {

  return { type: GET_MOBILE_NUMBER_MOBILE_ACT, payLoad: input };

};

export const getSelectedPostpaidPackage = (input) => {

  return { type: GET_SELECTED_PACKAGE, payLoad: input };

};

export const getFirstReloadMobileAct = (input) => {

  return { type: GET_FIRST_RELOAD_MOBILE_ACT, payLoad: input };

};

export const getAlternateContactNumberMobileAct = (input) => {

  return { type: GET_ALTERNATE_CONTACT_NUMBER_MOBILE_ACT, payLoad: input };

};

export const getEmailAdressMobileAct = (input) => {

  return { type: GET_EMAIL_ADDRESS_MOBILE_ACT, payLoad: input };

};

export const getIsCapturedMobileAct = (input) => {

  return { type: GET_IS_CAPTURED_MOBILE_ACT, payLoad: input };

};

export const getBlockedIdMobileAct = (input) => {

  return { type: GET_BLOCKED_ID_MOBILE_ACT, payLoad: input };

};

export const getSignatureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_SIGNATURE, payLoad: input };

};

export const getKycCaptureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_KYC_CAPTURE, payLoad: input };

};

export const getKycNic1CaptureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_KYC_NIC_1, payLoad: input };

};

export const setAllowedServices = (input) => {

  return { type: GET_ALLOWED_SERVICES, payLoad: input };

};

export const setRetailer = (isretailer) => {
  return { type: SET_RETAILER, payLoad: isretailer };
};

/**
 * This function will set if the agent is a CFSS agent or an O2A agent.
 * @param {*} isCFSSAgent 
 */
export const setAgentType = (isCFSSAgent) => {
  return { type: SET_IS_CFSS, payLoad: { isCFSSAgent } };
};

export const setTileData = (input) => {

  return { type: GET_TILE_DATA, payLoad: input };

};

//setAllowedServices

export const getKycNic2CaptureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_KYC_NIC_2, payLoad: input };

};

export const getKycPobCaptureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_POB_KYC_CAPTURE, payLoad: input };

};

export const getKycCustomerCaptureMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_CUSTOMER_KYC_CAPTURE, payLoad: input };

};

export const getResetImagesMobileAct = () => {

  console.log('GET_MOBILE_ACT_RESET_IMAGES');
  return { type: GET_MOBILE_ACT_RESET_IMAGES };

};

export const getCaptureIdTypeNoMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_CAPTURE_TYPE_NO, payLoad: input };

};

export const getReCaptureIdTypeNoMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_RE_CAPTURE_TYPE_NO, payLoad: input };

};

export const getUniqueTxIdMobileAct = (input) => {

  return { type: GET_MOBILE_ACT_UNIQUE_TX_ID, payLoad: input };

};

export const setUserState = (bool) => {

  return { type: 'set_user_state', payLoad: bool };

};

export const getOTPNumberMobileAct = (input) => {

  return { type: GET_OTP_NUMBER, payLoad: input };

};

export const setNumberCheckPerformed = (input) => {
  console.log("set number check performed", input);
  return { type: SET_NUMBER_CHECK_STATUS, payLoad: input };

};

export const getSkipValidationMobileAct = (input) => {

  return { type: GET_SKIP_VALIDATION_MOBILE_ACT, payLoad: input };

};

export const getOtpStatusMobileAct = (input) => {

  return { type: GET_OTP_STATUS_MOBILE_ACT, payLoad: input };

};

export const getInformationNotClearMobileAct = (input) => {

  return { type: GET_INFORMATION_NOT_CLEAR_MOBILE_ACT, payLoad: input };

};

export const getDifferentAddressMobileAct = (input) => {

  return { type: GET_ADDRESS_DIFFERENT_MOBILE_ACT, payLoad: input };

};

export const getSelectedPackageIndex = (input) => {

  return { type: GET_SELECTED_PACKAGE_INDEX, payLoad: input };

};

export const getOfferCodeMobileAct = (input) => {

  return { type: GET_RELOAD_OFFER_CODE_MOBILE_ACT, payLoad: input };

};

export const validateSimNumber = (data) => {
  console.log('validateSimNumber API :: data ', data);
  return (dispatch) => {
    dispatch({ type: GET_MOBILE_ACT_LOADING, payLoad: true });

    var successCb = function (response) {
      console.log('validateSimNumber RESPONSE: ', JSON.stringify(response));
      console.log('validateSimNumber RESPONSE: ', JSON.stringify(response));
      console.log('validateSimNumber RESPONSE: ', response);
      console.log('validateSimNumber RESPONSE: ', response);

      dispatch({ type: SIM_VALIDATION_SUCCESS, payLoad: response.data, sim_api_fail: false });
      // dispatch({ type: GET_MOBILE_ACT_LOADING, payLoad: false });
      Keyboard.dismiss();
    };

    var failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: SIM_VALIDATION_FAIL, payLoad: null, sim_api_fail: true });
      dispatch({ type: GET_MOBILE_ACT_LOADING, payLoad: false });
      Keyboard.dismiss();
    };

    var exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: SIM_VALIDATION_FAIL, payLoad: null, sim_api_fail: true });
      dispatch({ type: GET_MOBILE_ACT_LOADING, payLoad: false });
      Keyboard.dismiss();
    };

    Utill.apiRequestPost('validateSimNumber', 'sim', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const newConnActivation = (data) => {

  return (dispatch) => {

    var successCb = function (response) {

      if (response.data.success) {

        dispatch({ type: NEW_CONN_ACTIVATION_SUCCESS, payLoad: response.data });
      } else {
        dispatch({ type: NEW_CONN_ACTIVATION_FAIL, payLoad: null });
      }
    };

    Utill.apiRequestPost('newConnActivation', 'gsmConnection', 'ccapp', data, successCb);
  };
};

//GET_DOC_99_URL
export const getTileUrls = () => {
  const data = {
    service: 'doc990'
  };

  return (dispatch) => {
    dispatch(getApiLoading(true));
    var successCb = function (response) {
      dispatch(getApiLoading(false));
      console.log("====>>>> DOC URL RESPONSE", JSON.stringify(response));
      console.log("====>>>> DOC URL RESPONSE", JSON.stringify(response.data));

      if (response.data.success) {
        dispatch({ type: GET_DOC_99_URL, payLoad: response.data.svsUrl });
        dispatch({ type: GET_EZCASH_URL, payLoad: response.data.ezCashUrl });
        dispatch({ type: GET_EZCASH_SCHEME_URL, payLoad: response.data.ezCashSchemaUrl });
      } else {
        dispatch({ type: GET_DOC_99_URL, payLoad: 'http://partner.doc.lk/' });
        dispatch({ type: GET_EZCASH_URL, payLoad: 'market://details?id=dialog.com.ezcash.merchant' });
        dispatch({ type: GET_EZCASH_SCHEME_URL, payLoad: 'dialog://dialog.com.ezcash.merchants' });
      }
    };

    var failureCb = function (response) {
      dispatch(getApiLoading(false));
      console.log("getService URL fail", response);
    };

    var exCb = function (response) {
      dispatch(getApiLoading(false));
      console.log("getService URL exception", response);
    };

    Utill.apiRequestGet('getServiceUrl', 'configuration', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const setMobActApiLoading = (status) => {
  console.log('xxxx setMobActApiLoading');
  return { type: GET_MOBILE_ACT_API_LOADING, payLoad: status };
};

export const getPackageDeposits = (data) => {
  console.log('xxxx getPackageDeposits :: API');
  return (dispatch) => {

    dispatch({ type: GET_MOBILE_ACT_API_LOADING, payLoad: true });

    const successCb = function (response) {
      console.log('xxxx getPackageDeposits :: successCb', response.data);
      dispatch({ type: GET_PACKAGE_DEPOSITS_SUCCESS, payLoad: response.data.info });
      dispatch({ type: GET_MOBILE_ACT_API_LOADING, payLoad: false });
    };

    const failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      console.log('xxxx getPackageDeposits :: failureCb', response.data.error);
      dispatch({ type: GET_PACKAGE_DEPOSITS_FAIL, payLoad: null });
      dispatch({ type: GET_MOBILE_ACT_API_LOADING, payLoad: false });

    };

    const exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      console.log('xxxx getPackageDeposits :: exceptionCb', error);
      dispatch({ type: GET_PACKAGE_DEPOSITS_FAIL, payLoad: null });
      dispatch({ type: GET_MOBILE_ACT_API_LOADING, payLoad: false });
    };

    Utill.apiRequestPost('o2APackageDeposits', 'gsmPostpaidConnection', 'ccapp', data, successCb, failureCb, exCb);
  };
};


/**
 * This function accept the NIC in the format of 'xxxxxxxxxV' or 12 digits(new ID format) and return the validation status.
 * @param {JSON} data ==> {id_number: "197610104340", id_type: "NIC"}
 * @param {String} controller ==> 'gsmConnection' - prepaid(default), 'gsmPostpaidConnection' - postpaid
 * @returns {JSON} ==>
 * {"success":true,
 * "info":"Customter already exist",
 * "cxExistStatus":"Y",
 * "notificationNumberList":["779025482","771892197"],
 * "documnetExistInDataScan":"N",
 * "id_type":"NIC",
 * "id_number":"197610104340"}
 */
export const mobileActNicValidation = (data, controller = 'gsmConnection') => {
  console.log('In mobileActNicValidation: ', data);
  return (dispatch) => {
    dispatch(resetMobileActivationContactData());
    var successCb = function (response) {
      dispatch({ type: MOBILE_ACT_NIC_VALIDATION_SUCCESS, payLoad: response.data, nic_api_fail: false });
    };

    var failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: MOBILE_ACT_NIC_VALIDATION_FAIL, payLoad: null, nic_api_fail: true });

    };
    var exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: MOBILE_ACT_NIC_VALIDATION_FAIL, payLoad: null, nic_api_fail: true });

    };

    Utill.apiRequestPost('newCustomerNicValidation', controller, 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const mobileActOTPValidation = (data) => {

  return (dispatch) => {

    var successCb = function (response) {
      dispatch({ type: MOBILE_ACT_OTP_VALIDATION_SUCCESS, payLoad: response.data });
    };

    var failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: MOBILE_ACT_OTP_VALIDATION_FAIL, payLoad: null });

    };
    var exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: MOBILE_ACT_OTP_VALIDATION_FAIL, payLoad: null });
    };

    Utill.apiRequestPost('sendOtp', 'account', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const mobileActMConnectValidation = (data) => {

  return (dispatch) => {

    var successCb = function (response) {
      dispatch({ type: MOBILE_ACT_M_CONNECT_VALIDATION_SUCCESS, payLoad: response.data });
    };

    var failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: MOBILE_ACT_M_CONNECT_VALIDATION_FAIL, payLoad: null });
    };
    var exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: MOBILE_ACT_M_CONNECT_VALIDATION_FAIL, payLoad: null });
    };

    Utill.apiRequestPost('mConnectRequest', 'account', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const resetMobileActOTPValidation = () => {

  return { type: MOBILE_ACT_OTP_VALIDATION_FAIL, payLoad: null };

};

export const resetMobileActMConnectValidation = () => {

  return { type: MOBILE_ACT_M_CONNECT_VALIDATION_FAIL, payLoad: null };

};

export const resetMobileActNicValidation = () => {

  return { type: MOBILE_ACT_NIC_VALIDATION_FAIL, payLoad: null };

};

export const resetMobileActModelPopped = () => {

  return { type: MOBILE_ACT_MODEL_POPPED, payLoad: false };

};

export const mobileActModelPopped = (data) => {

  return { type: MOBILE_ACT_MODEL_POPPED, payLoad: data };

};

export const mobileActOtpPopped = (data) => {

  return { type: MOBILE_ACT_OTP_POPPED, payLoad: data };

};

export const mobileActMConnectPopped = (data) => {

  return { type: MOBILE_ACT_M_CONNECT_POPPED, payLoad: data };

};

export const resetNewConnActivation = () => {

  return { type: NEW_CONN_ACTIVATION_FAIL, payLoad: null };

};

export const resetValidateSimNumber = () => {

  return { type: SIM_VALIDATION_FAIL_RESET, payLoad: null };

};

export const setPaymentInfo = (data) => {

  return { type: SET_PAYMENT_INFO, payLoad: data };

};

export const resetPaymentInfo = () => {

  return { type: RESET_PAYMENT_INFO, payLoad: null };

};

export const isPackageLoaded = (input) => {

  return { type: IS_PACKAGE_LOADED, payLoad: input };

};

export const resetLocalCallDeposit = () => {

  const lcdResetParam = {
    depositType: "LCD",
    depositAmount: ''
  };

  return { type: LOCAL_CALL_DEPOSITS_RESET, payLoad: lcdResetParam };

};
//USER_SELECTED_PACKAGE_INDEX_RESET
export const resetSelectedPackageIndex = () => {

  const selectedIndex = -1;

  return { type: USER_SELECTED_PACKAGE_INDEX_RESET, payLoad: selectedIndex };

};

export const resetSelectedPackage = () => {

  const resetParams = {
    pkg_CODE: '',
    pkg_NAME: '',
    pkg_RENTAL: "0",
    connection_fee: "",
    deposit_AMOUNT: ""
  };

  return { type: SELECTED_PACKAGE_RESET, payLoad: resetParams };

};

export const resetSimApiFail = (payload = false) => {

  return { type: SIM_API_FAIL_RESET, payLoad: payload };

};

//get_mobile_act_loading
export const getApiResponseMobileAct = (input) => {

  return { type: GET_API_RESPONSE_MOBILE_ACT, payLoad: input };

};

export const getMobileActLoading = (input) => {

  return { type: GET_MOBILE_ACT_LOADING, payLoad: input };

};

export const getCurrentPin = (input) => {
  return { type: GET_CURRENT_PIN, payLoad: input };
};

export const getNewPin = (input) => {
  console.log(input);
  return { type: GET_NEW_PIN, payLoad: input };
};

export const getConfirmedPin = (input) => {

  return { type: GET_CONFIRMED_PIN, payLoad: input };
};

export const resetPinState = () => {
  return { type: RESET_PIN, payLoad: '' };
};

export const focusEzCashPin = (status) => {
  return { type: FOCUS_EZ_CASH_PIN, payLoad: status };
};

export const resetBillPaymentState = () => {

  return { type: 'RESET_BILL', payLoad: '' };
};

export const resetSimChangeState = () => {

  return { type: 'RESET_SIM', payLoad: '' };
};

export const resetMobileActivationState = () => {

  return { type: 'RESET_MOBILE_ACT', payLoad: '' };
};

//NEW - LTE
export const resetMobileActivationContactData = () => {
  console.log('Redux :: resetMobileActivationContactData');
  return (dispatch) => {
    dispatch(getEmailAdressMobileAct(''));
    dispatch(getAlternateContactNumberMobileAct(''));
  };
};

export const resetAuth = () => {

  return { type: 'RESET_AUTH', payLoad: '' };
};

// DELIVERY APP FUNCTIONS
/**
 * This function will return the set of pending work orders.
 * @param {String} job_status ==>
 * 'INITIAL' - Pending Work Orders(default),
 * 'DISPATCHED' - Dispatched Work Orders
 * 'IN_PROGRESS' - In Progress Work Orders,
 * @returns {JSON} ==>
 * [{ "simNumber": 28997,
 * "priority": true,
 * "estimatedTime": "1day : 03hrs : 20mins",
 * "address": "17/10B, Dudley Senanayake Road, Sri Gnanendra Mawatha, Rajagiriya",
 * "TTSLA":"22/02/2018 03:01 PM",
 * "CXRT":"22/02/2018 10:01 AM" }]
 */
export const getWorkOrders = (job_status = 'INITIAL') => {

  const data = {
    job_status: job_status
  };

  return (dispatch) => {
    const successCb = function (response) {
      console.log("In get pending work orders success.\n", response);
      if (response.data.success) {
        dispatch({ type: GET_PENDING_WORK_ORDERS_SUCCESS, payLoad: response.data.info });
      } else {
        dispatch({ type: GET_PENDING_WORK_ORDERS_FAIL, payLoad: [] });
      }
    };

    const failureCb = function (response) {
      console.log("In get pending work orders failure.\n", response);
      if (!response.data.error == 'No records founds') {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }
      dispatch({ type: GET_PENDING_WORK_ORDERS_FAIL, payLoad: [] });
    };

    const exCb = function (error) {
      console.log("In get pending work orders exCb.\n", error);
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: GET_PENDING_WORK_ORDERS_FAIL, payLoad: [] });
    };
    Utill.apiRequestPost('listWorkOrdersByJobStatus', 'womDelivery', 'delivery', data, successCb, failureCb, exCb);
  };
};

/**
 * This function will search work orders according to the data object that is passed as params.
 * @param {JSON} data ==>
 * [{ "simNumber": 28997,
 * "priority": true,
 * "estimatedTime": "1day : 03hrs : 20mins",
 * "address": "17/10B, Dudley Senanayake Road, Sri Gnanendra Mawatha, Rajagiriya",
 * "TTSLA":"22/02/2018 03:01 PM",
 * "CXRT":"22/02/2018 10:01 AM" }]
 */
export const getFilteredWorkOrders = (data) => {
  return { type: GET_FILTER_WORK_ORDERS, payLoad: data };
};

export const apiCall = (action, controller, module = 'ccapp') => {
  let data = {};
  return (dispatch) => {
    let successCb = function (response) {
      console.log('apiCall :: success.\n', response);
      if (response.data.success) {
        dispatch({ type: API_CALL_SUCCESS, payLoad: response.data.info });
      } else {
        dispatch({ type: API_CALL_FAIL, payLoad: [] });
      }
    };
    let failureCb = function (response) {
      console.log('apiCall :: failure.\n', response);
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: API_CALL_FAIL, payLoad: [] });
    };
    let exCb = function (error) {
      console.log('apiCall :: exCb.\n', error);
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      dispatch({ type: API_CALL_FAIL, payLoad: [] });
    };

    Utill.apiRequestPost(action, controller, module, data, successCb, failureCb, exCb);
  };
};

export const setWorrkOrderStatus = (status) => {

  return { type: GET_WORK_ORDER_STATUS, payLoad: status };
};

export const setCurrentWorkOrderId = (status) => {

  return { type: GET_CURRENT_WO_ID, payLoad: status };
};

export const setCurrentWorkOrderJobId = (status) => {

  return { type: GET_CURRENT_WO_JOBID, payLoad: status };
};

export const setCurrentWorkOrderJobStatus = (status) => {

  return { type: GET_CURRENT_WO_JOB_STATUS, payLoad: status };
};

export const setCurrentWorkOrderSlaBreachStatus = (slaStatus, slaTime) => {
  const data = {
    slaStatus,
    slaTime
  };
  return { type: GET_CURRENT_WO_SLA_BREACH_STATUS, payLoad: data };
};

export const setCurrentWorkOrderBasicInfo = (data) => {

  return { type: GET_CURRENT_WO_BASIC_INFO, payLoad: data };
};

export const setWomApiOrderData = (data) => {

  return { type: GET_WOM_API_ORDER, payLoad: data };

};
export const setWorkOrderCounts = (data) => {
  return { type: DELIVERY_GET_WO_COUNT, payLoad: data };
};

export const reSetWorkOrderCounts = () => {
  const totalWoCount = {
    PENDING: 0,
    DISPATCHED: 0,
    INPROGRESS: 0
  };
  return { type: DELIVERY_GET_WO_COUNT, payLoad: totalWoCount };
};

export const resetWomApiOrderData = () => {
  const constReduxData = {
    details: [],
    customerDetails: {},
    addionalDetails: []
  };
  return { type: GET_WOM_API_ORDER, payLoad: constReduxData };

};

export const getresetPendingOrders = () => {
  return { type: RESET_DELIVERY, payLoad: null };
};

export const setCcbsOrderId = (orderId) => {
  return { type: DELIVERY_CCBS_ORDER_ID, payLoad: orderId };
};

/**
  * order_id,
  * job_id,
  * job_status,
  * cannot_proceed_error_msg
  *
**/
export const getWorkOrderDetails = (data) => {
  console.log('@@@@@@@@@@ getWorkOrderDetails @@@@@@@@@@@@@@');
  return (dispatch) => {
    const successCb = function (response) {
      dispatch({ type: DELIVERY_HIDE_LOADING_INDICATER, payLoad: null });
      console.log('getWorkOrderDetails successCb: ', response.data.info);
      let constReduxData = {
        details: [],
        customerDetails: {},
        addionalDetails: []
      };

      let currentWorkOrderBasicInfo = {
        accountNo: '',
        saleType: '',
        woBisUnit: '',
        idNumber: {
          displayName: '',
          key: '',
          type: '',
          value: ''
        },
        pmsId: '',
        pos_invoice_no: '',
        order_line_item_no: '',
        issued_user: '',
        ccbs_order_id: '',
        wom_sap_material_code: ''
      };

      if (response.data.success) {
        const apiData = response.data.info;
        for (let apiDataProps in apiData) {
          if (!apiData.hasOwnProperty(apiDataProps)) {
            continue;
          }
          const workOrderApiData = apiData[apiDataProps];
          console.log('@@@@@@@@@@@@@@@@ workOrderApiData', workOrderApiData);
          if (data.order_id == workOrderApiData.orderId && data.job_id == workOrderApiData.job_id) {
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => orderId', workOrderApiData.orderId);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => job_id', workOrderApiData.job_id);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => SlaBreachStatus', workOrderApiData.breachTime);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => slaLapse', workOrderApiData.slaLapse);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => accountNo', workOrderApiData.accountNo);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => saleType', workOrderApiData.saleType);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => woBisUnit', workOrderApiData.woBisUnit);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => pmsId', workOrderApiData.pmsId);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => pos_invoice_no', workOrderApiData.pos_invoice_no);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => order_line_item_no', workOrderApiData.order_line_item_no);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => issued_user', workOrderApiData.issued_user);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => ccbs_order_id', workOrderApiData.ccbs_order_id);
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => wom_sap_material_code', workOrderApiData.wom_sap_material_code);

            const moreData = workOrderApiData.orderDetails;
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => moreData', moreData);
            const orderDetails = moreData.orderDetails;
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => moreData =>orderDetails', orderDetails);
            const customerDetail = moreData.customerDetail;
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => moreData =>customerDetail', customerDetail);
            const aditionalData = moreData.additionalData;
            console.log('@@@@@@@@@@@@@@@@ workOrderApiData => moreData =>aditionalData', aditionalData);

            if (FuncUtils.getArraySize(aditionalData) == 0) {
              Utill.showAlertMsg(data.cannot_proceed_error_msg);
              dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: true });
              return;
            }
            //Original Implementation goes here
            console.log('xxxx customerDetail :: idNumber', customerDetail.idNumber);

            currentWorkOrderBasicInfo = {
              accountNo: workOrderApiData.accountNo,
              saleType: workOrderApiData.saleType,
              woBisUnit: workOrderApiData.woBisUnit,
              idNumber: customerDetail.idNumber,
              pmsId: workOrderApiData.pmsId,
              pos_invoice_no: workOrderApiData.pos_invoice_no,
              order_line_item_no: workOrderApiData.order_line_item_no,
              issued_user: workOrderApiData.issued_user,
              ccbs_order_id: workOrderApiData.ccbs_order_id,
              wom_sap_material_code: workOrderApiData.wom_sap_material_code
            };

            let workOrderDetails = [];
            for (let prop in orderDetails) {
              if (!orderDetails.hasOwnProperty(prop)) {
                continue;
              }
              const newOb = {
                name: orderDetails[prop].displayName,
                value: orderDetails[prop].value
              };
              workOrderDetails.push(newOb);
            }

            console.log('xxxx workOrderDetails', workOrderDetails);

            let aditionalDataArray = [];
            for (let prop in aditionalData) {
              if (!aditionalData.hasOwnProperty(prop)) {
                continue;
              }
              const newOb = {
                name: aditionalData[prop].displayName,
                value: aditionalData[prop].value
              };
              aditionalDataArray.push(newOb);
            }

            console.log('xxxx aditionalData', aditionalDataArray);

            constReduxData = {
              details: workOrderDetails,
              customerDetails: customerDetail.idNumber,
              addionalDetails: aditionalDataArray
            };

          } else {
            console.log('Work order data invalid');
          }
        }

        dispatch({ type: GET_WOM_API_ORDER, payLoad: constReduxData });
        dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: false });
        dispatch({ type: GET_CURRENT_WO_BASIC_INFO, payLoad: currentWorkOrderBasicInfo });
      } else {
        dispatch({ type: GET_WOM_API_ORDER, payLoad: constReduxData });
        dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: true });
      }

    };

    const failureCb = function (response) {
      console.log('getWorkOrderDetails failureCb: ', response.data.error);
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      const constReduxData = {
        details: [],
        customerDetails: {},
        addionalDetails: []
      };
      dispatch({ type: DELIVERY_HIDE_LOADING_INDICATER, payLoad: null });
      dispatch({ type: GET_WOM_API_ORDER, payLoad: constReduxData });
      dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: true });
    };

    const exCb = function (error) {
      if (error == 'Network Error') {
        error = 'Network Error, \nPlease check your internet connection';
      } else {
        error = 'Error occured while loading the data';
      }
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      console.log('getWorkOrderDetails exCb: ', error);
      const constReduxData = {
        details: [],
        customerDetails: {},
        addionalDetails: []
      };
      dispatch({ type: DELIVERY_HIDE_LOADING_INDICATER, payLoad: null });
      dispatch({ type: GET_WOM_API_ORDER, payLoad: constReduxData });
      dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: true });
    };
    dispatch({ type: DELIVERY_SHOW_LOADING_INDICATER, payLoad: null });
    dispatch({ type: DELIVERY_API_CALL_FAIL, payLoad: true });

    Utill.apiRequestPost('listWorkOrdersByJobStatus', 'womDelivery', 'delivery', data, successCb, failureCb, exCb);
  };
};


/**
 * This function will get feedback list from backend
 * 
 */
export const deliveryGetFeedback = (screen) => {
  console.log('Redux :: deliveryGetFeedback :: API', screen);
  let requestParams = {
    some_data: 'some_data'
  };
  return (dispatch) => {
    try {
      dispatch({ type: DELIVERY_SET_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('Redux :: deliveryGetFeedback :: successCb', response.data);
        screen.getFeedbackSuccessCb(response);
        screen.setIndicator(false);
        dispatch({ type: DELIVERY_SET_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        console.log('Redux :: deliveryGetFeedback :: failureCb', response.data.error);
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        screen.setIndicator(false);
        dispatch({ type: DELIVERY_SET_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        console.log('Redux :: deliveryGetFeedback :: exceptionCb', error);
        Utill.showAlert(MessageUtils.getSystemErrorMessage());
        screen.setIndicator(false);
        dispatch({ type: DELIVERY_SET_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost2('getCxFeedbackQuestions', 'womDelivery', 'delivery', requestParams, successCb, failureCb, exCb);
    }
    catch (e) {
      console.log('Redux :: deliveryGetFeedback :: EXP', e);
      Utill.showAlert(MessageUtils.getSystemErrorMessage());
      screen.setIndicator(false);
      dispatch({ type: DELIVERY_SET_API_CALL, payLoad: false });
    }
  };
};

export const deliveryApiCallFail = (status) => {
  return { type: DELIVERY_API_CALL_FAIL, payLoad: status };
};

export const deliveryShowLoadingIndicator = () => {
  return { type: DELIVERY_SHOW_LOADING_INDICATER, payLoad: null };
};

export const deliveryHideLoadingIndicator = () => {
  return { type: DELIVERY_HIDE_LOADING_INDICATER, payLoad: null };
};

/**
 * Transaction History methods
 *
 * */

export const transShowApiLoading = () => {
  return { type: TRN_LOADING_INDICATOR, payLoad: true };
};

export const transResetAll = () => {
  return { type: RESET_ALL_TRANS_HISTORY, payLoad: null };
};

export const transHideApiLoading = () => {
  return { type: TRN_LOADING_INDICATOR, payLoad: false };
};

// export const setSIMChangeSearchString = (data) => {
//   return { type: TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING, payLoad: data };
// };

export const resetSIMChangeSearchResults = () => {

  return { type: TRN_SIM_CHANGE_HISTORY_RESET_RESULTS, payLoad: true };
};

export const resetSIMChangeHistoryLists = () => {
  return { type: TRN_SIM_CHANGE_HISTORY_RESET_LISTS, payLoad: true };
};

export const resetBillPaymentSearchResults = () => {
  return { type: TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS, payLoad: true };
};

export const resetActivationStatusAllList = () => {
  console.log('xxxx RESET ActivationStatusAllList');
  return (dispatch) => {
    dispatch({ type: TRN_ACTIVATION_RESET_ALL_LIST, payLoad: null });
  };
};

export const resetActivationStatusSearchString = () => {
  console.log('xxxx RESET ActivationStatus Search String');
  return (dispatch) => {
    dispatch({ type: TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING, payLoad: '' });
    dispatch({ type: TRN_SET_ACTIVATION_STATUS_SEARCH_STRING, payload: '' });
  };
};

export const setImageCaptureData = (imgArray) => {
  return { type: TRN_ACTIVATION_CAPTURED_IMAGE_STATUS, payLoad: imgArray };
};

export const getRejectedImageData = (data, successCallBack, failureCallBack) => {
  console.log('xxxx getSimChangeRejectedImage :: API');
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });

    const successCb = function (response) {
      console.log('xxxx getSimChangeRejectedImage :: successCb', response.data);
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      successCallBack(response);
    };

    const failureCb = function (response) {
      console.log('xxxx getSimChangeRejectedImage :: failureCb', response.data.error);
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      failureCallBack(response);

    };

    const exCb = function (error) {
      console.log('xxxx getSimChangeRejectedImage :: exceptionCb', error);
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
    };

    Utill.apiRequestPost('getRejectedImage', 'InitAct', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const resetActivationStatusRejectedList = () => {
  console.log('xxxx RESET ActivationStatusRejectedList');
  return (dispatch) => {
    dispatch({ type: TRN_ACTIVATION_RESET_REJECTED_LIST, payLoad: null });
  };
};

export const transBillPaymentHistorySelectedItem = (item) => {
  console.log('xxxx transBillPaymentHistorySelectedItem :: REDUX');
  return { type: TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM, payLoad: item };
};

export const transSimChangeHistorySelectedItem = (item) => {
  console.log('xxxx transSimChangeHistorySelectedItem :: REDUX');
  return { type: TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM, payLoad: item };
};

export const transActivationHistorySelectedItem = (item) => {
  console.log('xxxx transActivationHistorySelectedItem :: REDUX');
  return { type: TRN_ACTIVATION_STATUS_SELECTED_ITEM, payLoad: item };
};

export const transSetActivationStatusFilter = (data) => {
  console.log('xxxx transSetActivationStatusFilter :: REDUX');
  return { type: TRN_ACTIVATION_STATUS_FILTERS, payLoad: data };
};

export const getSimChangeHistoryList = (data, successCbCustom = null, failureCbCustom = null) => {
  console.log('xxxx getSimChangeHistoryList :: API');
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });
    if (data.customer_msisdn !== undefined) {
      dispatch({
        type: TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING, payLoad: {
          customer_msisdn: data.customer_msisdn, limit: data.limit, start: data.start
        }
      });
    }

    if (data.start_date !== undefined && data.end_date !== undefined) {
      dispatch({
        type: TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING, payLoad:
          { start_date: data.start_date, end_date: data.end_date, start: data.start, limit: data.limit }
      });
    }

    const successCb = function (response) {
      console.log('xxxx getSimChangeHistoryList :: successCb', response.data);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_SUCCESS, payLoad: response.data });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (successCbCustom != null) {
        console.log('xxxx getSimChangeHistoryList :: successCbCustom');
        successCbCustom.apply(this, arguments);
      }
    };

    const failureCb = function (response) {
      console.log('xxxx getSimChangeHistoryList :: failureCb', response.data.error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getSimChangeHistoryList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }
    };

    const exCb = function (error) {
      console.log('xxxx getSimChangeHistoryList :: exceptionCb', error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getSimChangeHistoryList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
      }
    };

    Utill.apiRequestPost('getSimChangesHistory', 'sim', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const getSimChangeHistoryPendingList = (data, successCbCustom = null, failureCbCustom = null) => {
  console.log('xxxx getSimChangeHistoryPendingList :: API');
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });

    if (data.customer_msisdn !== undefined) {
      dispatch({
        type: TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING, payLoad: {
          customer_msisdn: data.customer_msisdn, limit: data.limit, start: data.start
        }
      });
    }

    if (data.start_date !== undefined && data.end_date !== undefined) {
      dispatch({
        type: TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING, payLoad:
          { start_date: data.start_date, end_date: data.end_date, start: data.start, limit: data.limit }
      });
    }

    const successCb = function (response) {
      console.log('xxxx getSimChangeHistoryPendingList :: successCb', response.data);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS, payLoad: response.data });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (successCbCustom != null) {
        console.log('xxxx getSimChangeHistoryPendingList :: successCbCustom');
        successCbCustom.apply(this, arguments);
      }
    };

    const failureCb = function (response) {
      console.log('xxxx getSimChangeHistoryPendingList :: failureCb', response.data.error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_PENDING_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getSimChangeHistoryPendingList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }

    };

    const exCb = function (error) {
      console.log('xxxx getSimChangeHistoryPendingList :: exceptionCb', error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_PENDING_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getSimChangeHistoryPendingList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
      }
    };

    Utill.apiRequestPost('getSimChangesPendingHistory', 'sim', 'ccapp', data, successCb, failureCb, exCb);
  };
};


export const getSimChangeHistoryDetails = (data) => {
  console.log('xxxx getSimChangeHistoryDetails :: API');
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });

    const successCb = function (response) {
      console.log('xxxx getSimChangeHistoryDetails :: successCb', response.data);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS, payLoad: response.data.info });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
    };

    const failureCb = function (response) {
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      console.log('xxxx getSimChangeHistoryDetails :: failureCb', response.data.error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });

    };

    const exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      console.log('xxxx getSimChangeHistoryDetails :: exceptionCb', error);
      dispatch({ type: TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
    };

    Utill.apiRequestPost('getSimDetailsById', 'sim', 'ccapp', data, successCb, failureCb, exCb);
  };
};


export const getBillPaymentHistoryList = (data, successCbCustom = null, failureCbCustom = null) => {
  console.log('xxxx getBillPaymentHistoryList :: API');
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });
    if (data.account_no !== undefined) {
      dispatch({ type: TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING, payLoad: { account_no: data.account_no, start: data.start, limit: data.limit } });
    }

    if (data.start_date !== undefined && data.end_date !== undefined) {
      dispatch({
        type: TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING, payLoad:
          { start_date: data.start_date, end_date: data.end_date, start: data.start, limit: data.limit }
      });
    }

    const successCb = function (response) {
      console.log('xxxx getBillPaymentHistoryList :: successCb', response.data);
      dispatch({ type: TRN_BILL_PAYMENT_HISTORY_SUCCESS, payLoad: response.data });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (successCbCustom != null) {
        console.log('xxxx getBillPaymentHistoryList :: successCbCustom');
        successCbCustom.apply(this, arguments);
      }
    };

    const failureCb = function (response) {
      console.log('xxxx getBillPaymentHistoryList :: failureCb', response.data.error);
      dispatch({ type: TRN_BILL_PAYMENT_HISTORY_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getBillPaymentHistoryList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }

    };

    const exCb = function (error) {
      console.log('xxxx getBillPaymentHistoryList :: exceptionCb', error);
      dispatch({ type: TRN_BILL_PAYMENT_HISTORY_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getBillPaymentHistoryList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
      }
    };

    Utill.apiRequestPost('getBillPaymentHistory', 'bill', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const getActivationStatusAllList = (data, successCbCustom = null, failureCbCustom = null, isFirstLoading) => {
  console.log('xxxx getActivationStatusAllList :: API', isFirstLoading);
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });
    if (data.search !== undefined) {
      dispatch({
        type: TRN_SET_ACTIVATION_STATUS_SEARCH_STRING, payLoad: {
          search: data.search,
          limit: data.limit,
          start: data.start
        }
      });
    }
    if (data.filter !== undefined) {
      dispatch({
        type: TRN_SET_ACTIVATION_STATUS_SEARCH_STRING, payLoad: {
          limit: data.limit,
          start: data.start,
          filter: data.filter
        }
      });
    }

    const successCb = function (response) {
      console.log('xxxx getActivationStatusAllList :: successCb', response.data);
      dispatch({ type: TRN_ACTIVATION_STATUS_ALL_SUCCESS, payLoad: response.data, isFirstLoading });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (successCbCustom != null) {
        console.log('xxxx getActivationStatusAllList :: successCbCustom');
        successCbCustom.apply(this, arguments);
      }
    };

    const failureCb = function (response) {
      console.log('xxxx getActivationStatusAllList :: failureCb', response.data.error);
      dispatch({ type: TRN_ACTIVATION_STATUS_ALL_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getActivationStatusAllList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }

    };

    const exCb = function (error) {
      console.log('xxxx getActivationStatusAllList :: exceptionCb', error);
      dispatch({ type: TRN_ACTIVATION_STATUS_ALL_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getActivationStatusAllList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
      }
    };

    Utill.apiRequestPost('getAllStatus', 'activationStatus', 'ccapp', data, successCb, failureCb, exCb);
  };
};

export const getActivationStatusRejectedList = (data, successCbCustom = null, failureCbCustom = null, isFirstLoading) => {
  console.log('xxxx getActivationStatusRejectedList :: API', isFirstLoading);
  return (dispatch) => {

    dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });

    if (data.search !== undefined) {
      dispatch({
        type: TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING, payLoad: {
          search: data.search,
          limit: data.limit,
          start: data.start
        }
      });
    }

    if (data.filter !== undefined) {
      dispatch({
        type: TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING, payLoad: {
          limit: data.limit,
          start: data.start,
          filter: data.filter
        }
      });
    }

    const successCb = function (response) {
      console.log('xxxx getActivationStatusRejectedList :: successCb', response.data);
      dispatch({ type: TRN_ACTIVATION_STATUS_REJECTED_SUCCESS, payLoad: response.data , isFirstLoading });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (successCbCustom != null) {
        console.log('xxxx getActivationStatusRejectedList :: successCbCustom');
        successCbCustom.apply(this, arguments);
      }
    };

    const failureCb = function (response) {
      console.log('xxxx getActivationStatusRejectedList :: failureCb', response.data.error);
      dispatch({ type: TRN_ACTIVATION_STATUS_REJECTED_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getActivationStatusRejectedList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      }

    };

    const exCb = function (error) {
      console.log('xxxx getActivationStatusRejectedList :: exceptionCb', error);
      dispatch({ type: TRN_ACTIVATION_STATUS_REJECTED_FAIL, payLoad: null });
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      if (failureCbCustom != null) {
        console.log('xxxx getActivationStatusRejectedList :: failureCbCustom');
        failureCbCustom.apply(this, arguments);
      } else {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
      }
    };

    Utill.apiRequestPost('getRejectedStatus', 'activationStatus', 'ccapp', data, successCb, failureCb, exCb);
  };
};



/**
 *  This function will get activation status detail 
 * 
 */
export const getActivationStatusDetailView = (requestParams, screen) => {
  console.log('Redux :: getActivationStatusDetailView API');
  console.log('Redux :: getActivationStatusDetailView :: requestParams ', requestParams);

  let url = {
    action: 'getDetails',
    controller: 'activationStatus',
    module: 'ccapp'
  };

  return (dispatch) => {

    try {
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });
      const successCb = function (response) {
        console.log('Redux :: getActivationStatusDetailView :: successCb ', response);
        dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
        if (response.success) {
          console.log('GET_ACTIVATION_STATUS_DETAILS_SUCCESS');
          dispatch({ type: GET_ACTIVATION_STATUS_DETAILS, payLoad: response });
          screen.showDetailsScreen(response.info);
        } else {
          console.log('GET_ACTIVATION_STATUS_DETAILS_FALSE');
          dispatch({ type: GET_ACTIVATION_STATUS_DETAILS, payLoad: null });
          Screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      };

      const failureCb = function (response) {
        console.log('Redux :: getActivationStatusDetailView :: failureCb ', response);
        console.log('GET_ACTIVATION_STATUS_DETAILS_ERROR');
        Utill.showAlert(MessageUtils.getExceptionMessage(response));
        dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
        dispatch({ type: GET_ACTIVATION_STATUS_DETAILS, payLoad: null });
      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      Utill.showAlert(MessageUtils.getSystemErrorMessage());
      console.log('Redux :: getActivationStatusDetailView :: EXCEPTION ', e);
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      dispatch({ type: GET_ACTIVATION_STATUS_DETAILS, payLoad: null });
    }
  };
};

/**
 *  This function will update image resubmit details 
 * 
 */
export const transImageResubmitProcess = (requestParams, screen) => {
  console.log('Redux :: transImageResubmitProcess API');
  console.log('Redux :: transImageResubmitProcess :: requestParams ', requestParams);

  let url = {
    action: 'updateResubmitProcess',
    controller: 'initAct',
    module: 'ccapp'
  };

  return (dispatch) => {
    try {
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: true });
      const successCb = function (response) {
        console.log('Redux :: transImageResubmitProcess :: successCb ', response);
        dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
        if (response.success) {
          console.log('IMAGE_RESUBMIT_PROCESS_SUCCESS');
          screen.updateProcessSuccessCb(response);
        } else {
          console.log('IMAGE_RESUBMIT_PROCESS_SUCCESS_FALSE');
          Screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      };

      const failureCb = function (response) {
        console.log('Redux :: transImageResubmitProcess :: failureCb ', response);
        console.log('IMAGE_RESUBMIT_PROCESS_SUCCESS_ERROR');
        Screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
        dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      Screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage());
      console.log('Redux :: transImageResubmitProcess :: EXCEPTION ', e);
      dispatch({ type: TRN_LOADING_INDICATOR, payLoad: false });
    }
  };
};



/**
 * Authentication methods
 *
 * */

export const authResetAll = () => {
  console.log('AUTH_RESET_ALL :: authResetAll :: REDUX');
  return { type: AUTH_RESET_ALL, payLoad: null };
};

export const authShowApiLoading = () => {
  console.log('AUTH_LOADING_INDICATOR :: authShowApiLoading :: REDUX');
  return { type: AUTH_LOADING_INDICATOR, payLoad: true };
};


export const appVersionCheck = (versionCheckSuccessCb, showVersionUpdateAlert, tokenValue = null) => {
  console.log('## Redux :: appVersionCheck');
  const token = tokenValue == null ? "first_time" : tokenValue;
  const version = Global.appVersion;
  const mode = Global.mode;

  let url = {
    action: "startupCheck",
    controller: "account",
    module: "ccapp",
  };

  const requestPayload = {
    token,
    version,
    mode
  };

  return (dispatch) => {
    try {
      dispatch({ type: AUTH_LOADING_INDICATOR, payLoad: true });
      let successCb = function (response) {
        console.log('## Redux :: appVersionCheck :: successCb ', response);
        console.log('## Redux :: appVersionCheck :: successCb => downloadUrl', response.data.downloadUrl);
        console.log('## Redux :: appVersionCheck :: successCb => version', response.data.version);
        dispatch({ type: AUTH_LOADING_INDICATOR, payLoad: false });
        versionCheckSuccessCb.apply(this, arguments);

      };

      const failureCb = function (response) {
        console.log('## Redux :: appVersionCheck :: failureCb ', response);
        console.log('## Redux :: appVersionCheck :: failureCb => downloadUrl', response.data.downloadUrl);
        console.log('## Redux :: appVersionCheck :: failureCb => version', response.data.version);
        dispatch({ type: AUTH_LOADING_INDICATOR, payLoad: false });
        if (!_.isNil(response.data.downloadUrl) || !_.isNil(response.data.version)) {
          console.log('## Redux :: appVersionCheck :: show update alert');
          showVersionUpdateAlert(response.data.downloadUrl, response.data.version);
        } else {
          console.log('## Redux :: appVersionCheck :: failureCb - Failed to check version of Retailer App');
          Utill.showAlert(MessageUtils.getSystemErrorMessage());
          Analytics.logEvent('dsa_version_check_failure');
        }
      };

      const expCb = function (response) {
        console.log('## Redux :: appVersionCheck :: expCb ', response);
        dispatch({ type: AUTH_LOADING_INDICATOR, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(response));
        Analytics.logEvent('dsa_version_check_exception');
      };

      let withHttps = false;
      Utill.apiRequestPost2(url.action, url.controller, url.module, requestPayload, successCb, failureCb, expCb, withHttps);

    } catch (e) {
      console.log('## Redux :: appVersionCheck :: EXCEPTION ', e);
      dispatch({ type: AUTH_LOADING_INDICATOR, payLoad: false });
      Analytics.logEvent('dsa_version_check_exception');
    }
  };
};


/**
 * Genaral methods
 * 
 * */
export const genaralSetApiLoading = (status) => {
  return { type: GET_GENARAL_API_LOADING, payLoad: status };
};

export const genaralValidateSimNumber = (data, url = { action: 'validateSimNumber', controller: 'sim', module: 'ccapp' }) => {
  console.log('xxxx genaralValidateSimNumber API');
  return (dispatch) => {
    const successCb = function (response) {
      console.log('genaralValidateSimNumber successCb: ', response.data);
      dispatch({ type: GET_GENARAL_SIM_VALIDATION_SUCCESS, payLoad: response.data.data });
    };

    const failureCb = function (response) {
      console.log('genaralValidateSimNumber failureCb: ', response.data.error);
      Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
      dispatch({ type: GET_GENARAL_SIM_VALIDATION_FAIL, payLoad: null });
    };

    const exCb = function (error) {
      Utill.showAlert(MessageUtils.getExceptionMessage(error));
      console.log('genaralValidateSimNumber exCb: ', error);
      dispatch({ type: GET_GENARAL_SIM_VALIDATION_FAIL, payLoad: null });
    };

    Utill.apiRequestPost(url.action, url.controller, url.module, data, successCb, failureCb, exCb);
  };
};

export const genaralSetSimNumber = (simNumber) => {
  return { type: GET_GENARAL_SIM_NUMBER, payLoad: simNumber };
};

export const genaralSetSimValidationStatus = (status) => {
  return { type: GET_GENARAL_SIM_VALIDATION_STATUS, payLoad: status };
};

export const genaralSetSimValidationApiFail = () => {
  return { type: GET_GENARAL_SIM_VALIDATION_FAIL, payLoad: null };
};

export const genaralResetSimApiStatus = () => {
  return { type: RESET_SIM_VALIDATION_STATUS, payLoad: null };
};

export const genaralResetAll = () => {
  return { type: RESET_GENARAL, payLoad: null };
};

/********************* COMMON TYPES***********************/

export const commonResetAll = () => {
  console.log('RESET_COMMON');
  return { type: RESET_COMMON, payLoad: null };
};

export const commonSetApiLoading = (status) => {
  console.log('COMMON_SET_API_LOADING');
  return { type: COMMON_SET_API_LOADING, payLoad: status };
};

export const commonResetKycImages = () => {
  console.log('Redux :: RESET_KYC_IMAGES');
  return { type: RESET_KYC_IMAGES, payLoad: null };
};

export const commonResetKycSignature = () => {
  console.log('Redux :: COMMON_RESET_KYC_SIGNATURE');
  return { type: COMMON_RESET_KYC_SIGNATURE, payLoad: null };
};

export const commonSetKycImage = (captureOb) => {
  console.log('Redux :: commonSetKycImage :: captureOb', captureOb);
  let imageData = captureOb;
  if (_.isNil(captureOb.imageUri)) {
    imageData = null;
  }
  switch (captureOb.captureType) {
    case Constants.IMAGE_CAPTURE_TYPES.NIC_FRONT:
      return {
        type: COMMON_SET_KYC_NIC_FRONT,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.NIC_BACK:
      return {
        type: COMMON_SET_KYC_NIC_BACK,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.PASSPORT:
      return {
        type: COMMON_SET_KYC_PASSPORT,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.DRIVING_LICENCE:
      return {
        type: COMMON_SET_KYC_DRIVING_LICENCE,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING:
      return {
        type: COMMON_SET_KYC_PROOF_OF_BILLING,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE:
      return {
        type: COMMON_SET_KYC_CUSTOMER_IMAGE,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.SIGNATURE:
      return {
        type: COMMON_SET_KYC_SIGNATURE,
        payLoad: imageData
      };
    case Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB:
      return {
        type: COMMON_SET_KYC_ADDITIONAL_POB,
        payLoad: imageData
      };
    default:
      console.log('xxx commonSetKycImage :: default :: error');
      return {
        type: COMMON_SET_KYC_NIC_FRONT,
        payLoad: null
      };
  }
};


/********************* LTE ACTIVATION TYPES***********************/

//LTE_API_CALL
export const lteApiCall = (status) => {
  console.log('xxx lteApiCall');
  return { type: LTE_API_CALL, payLoad: status };
};

//LTE_SELECTED_CONNECTION_TYPE
export const setLTEConnectionType = (input) => {
  return { type: LTE_SELECTED_CONNECTION_TYPE, payLoad: input };
};
//RESET_LTE_DATA
export const resetLTEData = (input) => {
  return { type: RESET_LTE_DATA, payLoad: input };
};
//LTE_SERIAL_SCAN_MODE
export const setSerialScanMode = (input) => {
  console.log('LTE_SERIAL_SCAN_MODE');
  return { type: LTE_SERIAL_SCAN_MODE, payLoad: parseInt(input) };
};

export const setBillingCycle = (input) => {
  return { type: LTE_BILLING_CYCLE_NUMBER, payLoad: input };
};

//LTE_SET_CUSTOMER_TYPE
export const lteSetCustomerType = (input) => {
  return { type: LTE_SET_CUSTOMER_TYPE, payLoad: input };
};

//LTE_SET_ID_TYPE
export const lteSetIdType = (input) => {
  return { type: LTE_SET_ID_TYPE, payLoad: input };
};

//LTE_SET_CUSTOMER_INFO_ID_TYPE
export const lteSetCustomerInfoSectionIdType = (infoIdType) => {
  let code;
  console.log('LTE_SET_CUSTOMER_INFO_ID_TYPE', infoIdType);
  switch (infoIdType) {
    case Constants.CX_INFO_TYPE_NIC:
      code = Constants.ID_TYPE_NIC;
      break;
    case Constants.CX_INFO_TYPE_PASSPORT:
      code = Constants.ID_TYPE_PP;
      break;
    case Constants.CX_INFO_TYPE_DRIVING_LICENCE:
      code = Constants.ID_TYPE_DL;
      break;
    default:
      code = Constants.ID_TYPE_NIC;
      infoIdType = Constants.CX_INFO_TYPE_NIC;
      console.log('LTE_SET_CUSTOMER_INFO_ID_TYPE - default');
      break;
  }
  return { type: LTE_SET_CUSTOMER_INFO_ID_TYPE, payLoad: { infoIdType, code } };
};

//LTE_SET_CUSTOMER_ID_INFO
export const lteSetCustomerIdInfo = (input) => {
  return { type: LTE_SET_CUSTOMER_ID_INFO, payLoad: input };
};

//LTE_SET_ID_NUMBER_VALIDATION_STATUS
export const lteSetIdNumberValidationStatus = (input) => {
  return { type: LTE_SET_ID_NUMBER_VALIDATION_STATUS, payLoad: input };
};

//LTE_SET_PRODUCT_INFO_VALIDATION_STATUS
export const lteSetProductInfoValidationStatus = (input) => {
  return { type: LTE_SET_PRODUCT_INFO_VALIDATION_STATUS, payLoad: input };
};
//LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS
export const lteSetCustomerInfoValidationStatus = (input) => {
  return { type: LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS, payLoad: input };
};


export const lteResetSectionValidationStatus = () => {
  console.log('REDUX :: lteResetSectionValidationStatus');

  return (dispatch) => {

    dispatch({ type: LTE_SET_ID_NUMBER_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: LTE_SET_PRODUCT_INFO_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: RESET_KYC_IMAGES, payLoad: null });
    dispatch({ type: LTE_ID_VALIDATION_FAIL, payLoad: null });
    dispatch({ type: LTE_OUTSTANDING_CHECK_FAIL, payLoad: null });
    dispatch({ type: LTE_OTP_VALIDATION_FAIL, payLoad: null });
    dispatch({ type: LTE_SERIAL_VALIDATION, payLoad: null });
    dispatch({ type: LTE_RESET_SERIALS, payLoad: null });


  };

};

//KYC related
export const lteSetKycCheckBoxField = (checkBoxType = 'RESET', status = false) => {
  console.log('xxx lteSetKycCheckBoxField', checkBoxType);
  switch (checkBoxType) {
    case Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING:
      console.log('LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT');
      return {
        type: LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT,
        payLoad: status
      };
    case Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE:
      console.log('LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT');
      return {
        type: LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
        payLoad: status
      };
    case Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB:
      console.log('LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT');
      return {
        type: LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT,
        payLoad: status
      };
    default:
      console.log('LTE_RESET_KYC_CHECK_BOX_ALL');
      return {
        type: LTE_RESET_KYC_CHECK_BOX_ALL,
        payLoad: null
      };
  }
};

export const lteOnResetImageCapture = () => {
  console.log('REDUX :: lteOnResetImageCapture');

  return (dispatch) => {
    dispatch({ type: LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: RESET_KYC_IMAGES, payLoad: null });

  };

};

//LTE_SET_SECTIONS_VISIBLE
export const lteSetSectionVisibility = (key, validation_status, current_View_status) => {
  let data = {
    section_1: current_View_status.section_1,
    section_2: current_View_status.section_2,
    section_3: current_View_status.section_3,
  };

  switch (key) {
    case Constants.MAIN_SECTION.NIC_VALIDATION:
      data = {
        section_1: true,
        section_2: false,
        section_3: false,
      };
      break;
    case Constants.MAIN_SECTION.PRODUCT_INFORMATION:
      if (validation_status.section_1 || validation_status.section_2 || validation_status.section_3) {
        data = {
          section_1: false,
          section_2: true,
          section_3: false,
        };
      }
      break;
    case Constants.MAIN_SECTION.CUSTOMER_INFORMATION:
      if ((validation_status.section_1 && validation_status.section_2) || validation_status.section_3) {
        data = {
          section_1: false,
          section_2: false,
          section_3: true,
        };
      }
      break;
    default:
      break;

  }

  return { type: LTE_SET_SECTIONS_VISIBLE, payLoad: data };
};

//LTE_VISIBLE_SECTION_1
export const lteSetSection_1_visibility = (status) => {
  return { type: LTE_VISIBLE_SECTION_1, payLoad: status };
};
//LTE_VISIBLE_SECTION_2
export const lteSetSection_2_visibility = (status) => {
  return { type: LTE_VISIBLE_SECTION_2, payLoad: status };
};
//LTE_VISIBLE_SECTION_3
export const lteSetSection_3_visibility = (status) => {
  return { type: LTE_VISIBLE_SECTION_3, payLoad: status };
};

//LTE_INPUT_ID_NUMBER
export const lteSetInputIdNumber = (input) => {
  return { type: LTE_INPUT_ID_NUMBER, payLoad: input };
};

//LTE_SET_ID_NUMBER_ERROR
export const lteSetIdNumberError = (error) => {
  return { type: LTE_SET_ID_NUMBER_ERROR, payLoad: error };
};

//LTE_INPUT_EZ_CASH_PIN
export const lteSetEzCashPin = (input) => {
  return { type: LTE_INPUT_EZ_CASH_PIN, payLoad: input };
};

//LTE_OTP_NUMBER
export const lteSetOtpNumber = (number) => {
  console.log('xxx lteSetOtpNumber');
  return { type: LTE_OTP_NUMBER, payLoad: number };
};

//LTE_SERIAL_TEXT_VALUE
export const lteSetSerialTextValue = (number) => {
  console.log('xxx lteSetSerialTextValue');
  return { type: LTE_SERIAL_TEXT_VALUE, payLoad: number };
};

//LTE_SERIAL_VALIDATION
export const lteResetSerialValidationStatus = () => {
  console.log('xxx lteResetSerialValidationStatus');
  return { type: LTE_SERIAL_VALIDATION, payLoad: null };
};

//LTE_BUNDLE_SERIAL
export const lteSetBundleSerial = (number) => {
  console.log('xxx lteSetBundleSerial');
  return { type: LTE_BUNDLE_SERIAL, payLoad: number };
};

//LTE_SERIAL_PACK
export const lteSetSerialPack = (number) => {
  console.log('xxx lteSetBundleSerial');
  return { type: LTE_SERIAL_PACK, payLoad: number };
};

//LTE_SIM_SERIAL
export const lteSetSimSerial = (number) => {
  console.log('xxx lteSetBundleSerial');
  return { type: LTE_SIM_SERIAL, payLoad: number };
};

//LTE_INPUT_ANALOG_PHONE_SERIAL
export const lteSetAnalogPhoneSerial = (number) => {
  console.log('xxx lteSetAnalogPhoneSerial');
  return { type: LTE_INPUT_ANALOG_PHONE_SERIAL, payLoad: number };
};

//LTE_REST_ANALOG_PHONE_SERIAL_DATA
export const lteResetAnalogPhoneSerialData = () => {
  console.log('xxx lteResetAnalogPhoneSerialData');
  return { type: LTE_REST_ANALOG_PHONE_SERIAL_DATA, payLoad: null };
};

//LTE_SET_RELOAD
export const lteSetReload = (input) => {
  console.log('xxx lteSetReload');
  if (input.offer_val == undefined || input.offer_val == null) {
    input = {
      offer_key: null,
      offer_val: {
        CHARGE: ''
      }
    };
  }
  return { type: LTE_SET_RELOAD, payLoad: input };
};

//LTE_RESET_SERIALS
export const lteResetSerialValues = () => {
  console.log('xxx lteResetSerialValues');
  return { type: LTE_RESET_SERIALS, payLoad: null };
};

//LTE_INPUT_EMAIL
export const lteSetInputemail = (input) => {
  return { type: LTE_INPUT_EMAIL, payLoad: input };
};

//LTE_INPUT_LANLINENUMBER
export const lteSetInputLandlineNumber = (input) => {
  return { type: LTE_SET_INPUT_LANLINE_NUMBER, payLoad: input };
};

//LTE_SET_INPUT_MOBILE_NUMBER
export const lteSetInputMobileNumber = (input) => {
  return { type: LTE_SET_INPUT_MOBILE_NUMBER, payLoad: input };
};

//LTE_SET_DATA_PACKAGE_CODE
export const lteSetDataPackageCode = (code) => {
  console.log('xxx lteSetDataPackageCode');
  return { type: LTE_SET_DATA_PACKAGE_CODE, payLoad: code };
};

//LTE_SET_VOICE_PACKAGE_CODE
export const lteSetVoicePackageCode = (code) => {
  console.log('xxx lteSetDataPackageCode');
  return { type: LTE_SET_VOICE_PACKAGE_CODE, payLoad: code };
};

//LTE_RESET_INDIVIDUAL_SERIAL_DATA
export const lteResetIndividualSerialFieldValues = (serialType) => {
  console.log('xxx lteResetIndividualSerialFieldValues :: serialType :', serialType);
  return (dispatch) => {
    dispatch({ type: LTE_RESET_INDIVIDUAL_SERIAL_DATA, payLoad: serialType });
    dispatch({ type: LTE_RESET_SERIALS_DATA, payLoad: null });
  };
};

//LTE_FORCE_UPDATE_PROPS
export const lteForceUpdateProps = () => {
  console.log('xxx lteForceUpdateProps');
  console.log('################ FORCE_UPDATE_PROPS #######################');
  let randValue = Math.floor(Math.random() * 255);
  //let randValue = Math.round((new Date()).getTime() / 1000);
  return { type: LTE_FORCE_UPDATE_PROPS, payLoad: randValue };
};

//DTV_PACKTYPE_DATA
export const setPackType = (input) => {
  return { type: DTV_PACK_TYPE, payLoad: input };
};

//DTV_FULLFILLMENTTYPE_DATA
export const setFullFillMentType = (input) => {
  return (dispatch) => {
    dispatch({ type: DTV_FULLFILLMENT_TYPE, payLoad: input });
    dispatch({ type: DTV_SET_SERIAL_TYPE_ACCESSORIES, payLoad: null });
  };

};

//RESET_DTV_FULLFILLMENT_TYPE

export const resetFullFillMentType = (input) => {
  console.log('resetFullFillMentType', input);
  return (dispatch) => {
    dispatch({ type: RESET_DTV_FULLFILLMENT_TYPE, payLoad: input });
  };

};

/**
 *  This function will check Customer validation API (NIC & Passport) 
 *  API 1
 */
export const lteCustomerValidation = (data, connType, screen, customUrl = null) => {
  console.log('xxxx lteCustomerValidation API');
  console.log('xxx lteCustomerValidation :: connType ', connType);
  let url = {
    action: 'customerValidation',
    controller: 'lte',
    module: 'ccapp'
  };

  if (customUrl !== null) {
    url = customUrl;
  }

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true, message: 'Validating' });
    const successCb = function (response) {
      console.log('xxx lteCustomerValidation :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      Keyboard.dismiss();
      if (connType == Constants.PREPAID && response.success) {
        console.log('xxx lteCustomerValidation :: PREPAID ################');
        dispatch({ type: LTE_ID_VALIDATION_SUCCESS, payLoad: response });
        if (response.cxExistStatus == Constants.CX_EXIST_YES
          && response.documnetExistInDataScan == Constants.DOCUMENT_EXIST_YES
          && FuncUtils.getArraySize(response.notificationNumberList) > 0) {
          console.log('PREPAID_OTP_FLOW');
          screen.showOtpNumberSelectionModal(response);
        } else {
          console.log('PREPAID_NEW_CUSTOMER_NORMAL_FLOW');
        }
      } else if (connType == Constants.POSTPAID && response.success) {
        console.log('xxx lteCustomerValidation :: POSTPAID ################');
        dispatch({ type: LTE_ID_VALIDATION_SUCCESS, payLoad: response });
        if (response.cxExistStatus == Constants.CX_EXIST_YES) {
          console.log('POSTPAID_OUTSTANDING_CHECK');
          screen.checkCustomerOutstanding(response);
        } else {
          console.log('POSTPAID_NEW_CUSTOMER_NORMAL_FLOW');
        }
      }
      else {
        dispatch({ type: LTE_ID_VALIDATION_FAIL, payLoad: null });
        console.log('xxx lteCustomerValidation :: NEGATIVE => connType -' + connType);
        if (response.message_type == Constants.MSG_TYPE_MODAL) {
          console.log('MSG_TYPE_MODAL');
          screen.showModalTypeMessage(response, response.error);
        } else if (response.message_type == Constants.MSG_TYPE_INLINE) {
          console.log('MSG_TYPE_INLINE');
          screen.showInlineErrorMessageIdNumber(response, response.error);
        } else if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
        } else {
          console.log('MSG_TYPE_ALERT');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteCustomerValidation :: failureCb ', response);
      dispatch({ type: LTE_ID_VALIDATION_FAIL, payLoad: null });
      dispatch({ type: LTE_API_CALL, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, data, successCb, failureCb, handleFalseResponse);
  };
};

/**
 *  This function will check postpaid Customer Outstanding 
 *  API 2
 */
export const lteCustomerOutstandingCheck = (data, customerData, screen, customUrl = null) => {
  console.log('xxxx lteCustomerOutstandingCheck API');
  console.log('xxx lteCustomerOutstandingCheck :: customerData ', customerData);
  let url = {
    action: 'getO2AConnectionOsDetails',
    controller: 'lte',
    module: 'ccapp'
  };

  if (customUrl !== null) {
    url = customUrl;
  }

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx lteCustomerOutstandingCheck :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: LTE_OUTSTANDING_CHECK_SUCCESS, payLoad: response });
      if (response.success) {
        console.log('OUTSTANDING_CHECK_SUCCESS');
        screen.showOutstandingConfirmModal(customerData, response);
      }
      else {
        console.log('xxx lteCustomerOutstandingCheck :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('OUTSTANDING_CHECK_SUCCESS_NO_OUTSTANDING');
          screen.customerWithoutOutstanding(customerData, response);
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('OUTSTANDING_CHECK_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteCustomerOutstandingCheck :: failureCb ', response);
      console.log('OUTSTANDING_CHECK_ERROR');
      dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: LTE_OUTSTANDING_CHECK_FAIL, payLoad: null });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, data, successCb, failureCb, handleFalseResponse);
  };
};

/**
 *  This function will send OTP SMS message from backend to Customer
 *  API 3
 */
export const lteSendOtpMessage = (requestParams, screen) => {
  console.log('xxxx lteSendOtpMessage API');
  Analytics.logEvent('dsa_lte_activation_otp_send');
  console.log('xxx lteSendOtpMessage :: requestParams :', requestParams);
  let url = {
    action: 'otpSend',
    controller: 'common',
    module: 'ccapp'
  };

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx lteSendOtpMessage :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('OTP_SEND_CHECK_SUCCESS');
        if (requestParams.ref_id) {
          console.log('OTP_RESEND');
          screen.otpResendSuccessfulMessage();
        } else {
          console.log('OTP_SEND');
          screen.showOtpEnteringModal(response);
        }
      }
      else {
        console.log('xxx lteSendOtpMessage :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('OTP_SEND_CHECK_FAIL_ALERT');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('OTP_SEND_CHECK_FAIL_ALERT');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteSendOtpMessage :: failureCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

/**
 *  This function will validate OTP SMS from backend
 *  API 4
 */
export const lteVerifyOtp = (requestParams, screen) => {
  console.log('xxxx lteVerifyOtp API');
  console.log('xxx lteVerifyOtp :: requestParams : ', requestParams);
  let url = {
    action: 'otpVerify',
    controller: 'common',
    module: 'ccapp'
  };

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx lteVerifyOtp :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('VERIFY_OTP_SUCCESS');
        dispatch({ type: LTE_OTP_VALIDATION_SUCCESS, payLoad: response });
        screen.modalDismiss();
        Analytics.logEvent('dsa_lte_activation_otp_success', { customerNumber: requestParams.otp_msisdn });
      }
      else {
        console.log('xxx lteVerifyOtp :: NEGATIVE');
        dispatch({ type: LTE_OTP_VALIDATION_FAIL, payLoad: null });
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('VERIFY_OTP_FAIL');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('VERIFY_OTP_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteVerifyOtp :: failureCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: LTE_OTP_VALIDATION_FAIL, payLoad: null });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

/**
 *  This function will validate serial number from backend
 *  API 5
 */
export const lteValidateSerialNumber = (requestParam, url, screen) => {
  console.log('xxxx lteValidateSerialNumber API');
  console.log('xxx lteValidateSerialNumber : requestParam', requestParam);
  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx lteValidateSerialNumber :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('SERIAL_VALIDATION_SUCCESS');
        dispatch({ type: LTE_SERIAL_VALIDATION, payLoad: response.info });
        dispatch({ type: LTE_SERIAL_TEXT_VALUE, payLoad: requestParam.materialSerial });
        Keyboard.dismiss();
        try {
          let additionalProps = screen.props.additionalProps;
          console.log('xxx lteValidateSerialNumber :: additionalProps : ', additionalProps);
          screen.serialValidationSuccessCb(response);
          console.log('SERIAL_VALIDATION_SUCCESS TRY');
          let requestParamsOffers = {
            conn_type: additionalProps.conn_type,
            lob: additionalProps.lob,
            cx_identity_no: additionalProps.id_number,
            cx_identity_type: additionalProps.id_type,
            scanned_serials: []
          };

          // let requestParamsDataPackage = {
          //   conn_type: additionalProps.conn_type,
          //   lob: additionalProps.lob,
          //   cx_identity_no: additionalProps.id_number,
          //   cx_identity_type: additionalProps.id_type,
          //   package_type: 'DATA'

          // };

          // let billingCycleParams = {
          //   conn_type: additionalProps.conn_type,
          //   lob: Constants.LOB_LTE,
          //   cx_identity_no: additionalProps.id_number,
          //   cx_identity_type: additionalProps.id_type,
          //   customer_status: additionalProps.customer_status
          // };

          let telcoAreaSearchParams = {
            conn_type: additionalProps.conn_type,
            lob: Constants.LOB_LTE,
            cx_identity_no: additionalProps.id_number,
            cx_identity_type: additionalProps.id_type,
            city_name: ''
          };

          let scanned_serials = [];
          switch (requestParam.serialType) {
            case 'bundleSerial':
              console.log('xxx lteValidateSerialNumber bundleSerial');
              dispatch({ type: LTE_SET_SERIAL_TYPE_BUNDLE, payLoad: response.info });
              scanned_serials.push(response.info);
              requestParamsOffers.scanned_serials = scanned_serials;
              dispatch(getLTEOffersList(requestParamsOffers, additionalProps));
              // dispatch(getLTEDataPackagesList(requestParamsDataPackage, billingCycleParams, additionalProps));
              if (additionalProps.selectedArea == '')
                dispatch(searchTelcoArea(telcoAreaSearchParams));
              break;
            case 'serialPack':
              console.log('xxx lteValidateSerialNumber serialPack');
              dispatch({ type: LTE_SET_SERIAL_TYPE_PACK, payLoad: response.info });
              if (additionalProps.simSerialData !== null) {
                scanned_serials = [];
                scanned_serials.push(additionalProps.simSerialData);
                scanned_serials.push(response.info);
                requestParamsOffers.scanned_serials = scanned_serials;
                dispatch(getLTEOffersList(requestParamsOffers, additionalProps));
                // dispatch(getLTEDataPackagesList(requestParamsDataPackage, billingCycleParams, additionalProps));
                if (additionalProps.selectedArea == '')
                  dispatch(searchTelcoArea(telcoAreaSearchParams));
              }
              break;
            case 'simSerial':
              console.log('xxx lteValidateSerialNumber simSerial');
              dispatch({ type: LTE_SET_SERIAL_TYPE_SIM, payLoad: response.info });
              if (additionalProps.serialPackData !== null) {
                scanned_serials = [];
                scanned_serials.push(response.info);
                scanned_serials.push(additionalProps.serialPackData);
                requestParamsOffers.scanned_serials = scanned_serials;
                dispatch(getLTEOffersList(requestParamsOffers, additionalProps));
                // dispatch(getLTEDataPackagesList(requestParamsDataPackage, billingCycleParams, additionalProps)); 
                if (additionalProps.selectedArea == '')
                  dispatch(searchTelcoArea(telcoAreaSearchParams));
              }
              break;
            case 'analogPhoneSerial':
              console.log('xxx lteValidateSerialNumber analogPhoneSerial');
              dispatch({ type: LTE_SET_SERIAL_TYPE_ANALOG_PHONE, payLoad: response.info });
              break;
            default:
              console.log('xxx lteValidateSerialNumber default');
              break;
          }
        } catch (error) {
          console.log('SERIAL_VALIDATION_SUCCESS CATCH', error);
        }
      }
      else {
        console.log('xxx lteValidateSerialNumber :: NEGATIVE');
        dispatch({ type: LTE_SERIAL_VALIDATION, payLoad: null });
        dispatch({ type: LTE_SERIAL_TEXT_VALUE, payLoad: '' });
        try {
          Utill.showAlert(MessageUtils.getFormatedErrorMessage(response));
          screen.serialValidationFailureCb(response);
          console.log('SERIAL_VALIDATION_FAIL TRY');
        } catch (error) {
          console.log('SERIAL_VALIDATION_FAIL CATCH', error);
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteValidateSerialNumber :: failureCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: LTE_SERIAL_VALIDATION, payLoad: null });
      dispatch({ type: LTE_SERIAL_TEXT_VALUE, payLoad: '' });
      try {
        Utill.showAlert(MessageUtils.getExceptionMessage(response));
        screen.serialValidationFailureCb(response);
        console.log('SERIAL_VALIDATION_FAIL_EX TRY');
      } catch (error) {
        console.log('SERIAL_VALIDATION_FAIL_EX CATCH', error);
      }

    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParam, successCb, failureCb, handleFalseResponse);
  };
};


/**
 *  This function will check eZ Cash account details 
 *  API 6
 */
export const lteEzCashAccountCheck = (requestParams, screen) => {
  console.log('xxxx lteEzCashAccountCheck API');
  console.log('xxx lteEzCashAccountCheck :: requestParams ', requestParams);
  let url = {
    action: 'checkBalance',
    controller: 'ezCash',
    module: 'ccapp'
  };

  //TODO :
  // Analytics.logFirebaseEvent('action_progress', {
  //   action_name: 'lte_ez_cash_account_check',
  //   action_type: 'api_request',
  //   additional_details: 'eZ cash account check',
  // });

  //TODO : mock API
  // url = {
  //   action: 'testEzCash',
  //   controller: 'apiTest',
  //   module: 'ccapp'
  // };

  return (dispatch) => {
    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });
      Keyboard.dismiss();
      const successCb = function (response) {
        console.log('xxx lteEzCashAccountCheck :: successCb ', response);
        dispatch({ type: LTE_API_CALL, payLoad: false });
        if (response.success) {
          console.log('EZ_CASH_ACCOUNT_CHECK_SUCCESS');
          dispatch({ type: LTE_EZ_CASH_BALANCE, payLoad: response, balance: response.info.availableAmount, account_no: response.info.msisdn });
        }
        else {
          dispatch({ type: LTE_EZ_CASH_BALANCE, payLoad: null, balance: 0, account_no: '' });
          console.log('xxx lteEzCashAccountCheck :: NEGATIVE');
          if (response.message_type == Constants.MSG_TYPE_NA) {
            console.log('MSG_TYPE_NA');
            console.log('EZ_CASH_ACCOUNT_CHECK_NO_ERROR');
          } else {
            console.log('MSG_TYPE_ALERT');
            console.log('EZ_CASH_ACCOUNT_CHECK_FAIL');
            screen.showEzCashErrorModal(response);
          }
        }
      };

      const failureCb = function (response) {
        console.log('xxx lteSendOtpMessage :: failureCb ', response);
        console.log('EZ_CASH_ACCOUNT_CHECK_ERROR');
        dispatch({ type: LTE_API_CALL, payLoad: false });
        dispatch({ type: LTE_EZ_CASH_BALANCE, payLoad: null, balance: 0, account_no: '' });
        screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
      };

      let handleFalseResponse = true;
      Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);

    } catch (e) {
      console.log('Redux :: lteEzCashAccountCheck :: EXCEPTION ', e);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      dispatch({ type: LTE_EZ_CASH_BALANCE, payLoad: null, balance: 0, account_no: '' });
      screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage());

    }
  };
};

// LTE_RESET_EZCASH_BALANCE
export const lteResetEzcashBalance = () => {
  console.log('xxx LTE_RESET_EZCASH_BALANCE');
  return { type: LTE_RESET_EZCASH_BALANCE, payLoad: null };
};

/**
 *  This function will load data sachet list from backend
 *  API 7
 */
/** Mock API :: lte_sachetPackageList **/
//import  lte_sachetPackageList from '../mockData/lte_sachetPackageList';
export const lteLoadDataSachetList = (requestParams, screen, customUrl = null) => {
  console.log('xxxx lteLoadDataSachetList API');
  console.log('xxx lteLoadDataSachetList :: requestParams ', requestParams);
  let url = {
    action: 'sachetPackageList',
    controller: 'lte',
    module: 'ccapp'
  };

  if (customUrl !== null) {
    url = customUrl;
  }

  //Mock API :: lte_sachetPackageList
  //let response = lte_sachetPackageList;

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx lteLoadDataSachetList :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('LOAD_DATA_SACHET_SUCCESS');
        screen.showDataSachetSelectionModal(response);
      }
      else {
        console.log('xxx lteLoadDataSachetList :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('LOAD_DATA_SACHET_NO_ERROR');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('LOAD_DATA_SACHET_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteLoadDataSachetList :: failureCb ', response);
      console.log('LOAD_DATA_SACHET_ERROR');
      dispatch({ type: LTE_API_CALL, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

/**
 *  This function will check agent bucket account details 
 *  API 8
 */
export const commonRapidEzAccountCheck = (requestParams, successCallBack, screen) => {
  console.log('xxxx commonRapidEzAccountCheck API');
  console.log('xxx commonRapidEzAccountCheck :: requestParams, screen ', requestParams, screen);
  let url = {
    action: 'checkBalance',
    controller: 'rapidEz',
    module: 'ccapp'
  };

  return (dispatch) => {

    dispatch({ type: LTE_API_CALL, payLoad: true });
    const successCb = function (response) {
      console.log('xxx commonRapidEzAccountCheck :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('AGENT_RAPID_EZ_ACCOUNT_CHECK_SUCCESS');
        successCallBack(response);
      }
      else {
        console.log('xxx commonRapidEzAccountCheck :: NEGATIVE');
        console.log('MSG_TYPE_ALERT');
        console.log('AGENT_RAPID_EZ_ACCOUNT_CHECK_FAIL');
        screen.showRapidEzErrorModal(response);
      }
    };

    const failureCb = function (response) {
      console.log('xxx commonRapidEzAccountCheck :: failureCb ', response);
      console.log('AGENT_RAPID_EZ_ACCOUNT_CHECK__ERROR');
      dispatch({ type: LTE_API_CALL, payLoad: false });
      Utill.showAlert(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};


/**
 *  This function will send final activation data to backend 
 *  API 9
 */
export const lteFinalActivationApi = (requestParams, screen) => {
  console.log('xxxx lteFinalActivationApi API');
  console.log('xxx lteFinalActivationApi :: requestParams ', requestParams);
  let url = {
    action: 'activation',
    controller: 'lteActivation',
    module: 'ccapp'
  };

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });

    const successCb = function (response) {
      console.log('xxx lteFinalActivationApi :: successCb ', response);
      dispatch({ type: LTE_API_CALL, payLoad: false });
      if (response.success) {
        console.log('FINAL_ACTIVATION_SUCCESS');
        dispatch({ type: CONFIG_LTE_SET_ACTIVATION_STATUS, payLoad: true });
        screen.showActivateSuccessModal(response);

      }
      else {
        console.log('xxx lteFinalActivationApi :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('FINAL_ACTIVATION_NO_ERROR');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('FINAL_ACTIVATION_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx lteFinalActivationApi :: failureCb ', response);
      console.log('FINAL_ACTIVATION_ERROR');
      dispatch({ type: LTE_API_CALL, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

/**
 * This function get available Offers from backend and update availableLTEOffersList
 * 
 */
export const getLTEOffersList = (requestParams, additionalProps) => {
  console.log('xxxx getLTEOffersList API :: requestParams :', requestParams);
  let reqParams = requestParams;
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      await Utill.apiRequestPost2('GetOfferDetails', 'lte', 'ccapp', reqParams, (res) => {
        console.log("Success Data Obj:", JSON.stringify(res.data.info));
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
          selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
          offer_Code: res.data.info.defaultSelection,
          offerPrice: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Price,
          defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
          offerList: res.data.info.OfferList,
          offer_type: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_type,
          additionalInfo: {
            audience_group_name: res.data.info.audience_group_name,
            om_campaign_record_no: res.data.info.om_campaign_record_no,
            reservation_no: res.data.info.reservation_no,
          }

        };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });

        reqParams.offer_code = res.data.info.defaultSelection;
        reqParams.offer_type = selectedData.offer_type;
        //Aditional optional parameters
        if (res.data.info.audience_group_name !== null) {
          reqParams.audience_group_name = res.data.info.audience_group_name;
        }

        if (res.data.info.om_campaign_record_no !== null) {
          reqParams.om_campaign_record_no = res.data.info.om_campaign_record_no;
        }

        if (res.data.info.reservation_no !== null) {
          reqParams.reservation_no = res.data.info.reservation_no;
        }


        let requestParamsDataPackage = {
          conn_type: additionalProps.conn_type,
          lob: additionalProps.lob,
          cx_identity_no: additionalProps.id_number,
          cx_identity_type: additionalProps.id_type,
          package_type: 'DATA',
          offer_code: res.data.info.defaultSelection
        };

        let billingCycleParams = {
          conn_type: additionalProps.conn_type,
          lob: Constants.LOB_LTE,
          cx_identity_no: additionalProps.id_number,
          cx_identity_type: additionalProps.customerType,
          customer_status: additionalProps.customer_status
        };

        let dataPackageParams = {
          requestParamsDataPackage,
          billingCycleParams,
          additionalProps
        };

        // dispatch(getLTEDataPackagesList(requestParamsDataPackage, billingCycleParams, additionalProps));
        dispatch(getLTEOfferDetails(reqParams, selectedData, dataPackageParams));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEOffersList Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      }, (errorEx) => {
        console.log("getLTEOffersList Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
 * This function get available Offers from backend and update availableLTEOffersList
 * 
 */
export const getLTEOfferDetails = (requestParams, selectedDataOb, dataPackageParams) => {

  console.log('xxxx getLTEOfferDetails API :: requestParams, selectedDataOb :', requestParams, requestParams);
  return async (dispatch) => {
    dispatch(getLTEDataPackagesList(dataPackageParams.requestParamsDataPackage, dataPackageParams.billingCycleParams, dataPackageParams.additionalProps));

    try {
      dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedDataOb });
      dispatch({ type: LTE_API_CALL, payLoad: true });
      await Utill.apiRequestPost2('getOfferCodeDetails', 'lte', 'ccapp', requestParams, (res) => {

        console.log("LTE Offer Details Success Data Obj:", JSON.stringify(res.data.info));
        console.log("Selected Data Obj:", JSON.stringify(selectedDataOb));
        console.log("Voucher code available:", res.data.info.voucherCodeAvailable);

        if (res.data.info !== undefined) {
          let offerInformation = {
            details: _.map(res.data.info.details, 'offerItemName'),
            detailsObject: res.data.info.details
          };

          dispatch({ type: GET_LTE_OFFER_DETAIL, payLoad: offerInformation });
          dispatch({ type: GET_LTE_VOUCHER_CODE_STATUS, payLoad: res.data.info.voucherCodeAvailable });
          dispatch({ type: SET_LTE_VOUCHER_CODE_GROUP_NAME, payLoad: res.data.info.audience_group_name });
          dispatch({ type: SET_LTE_ANALOG_PHONE_VISIBILITY, payLoad: res.data.info.analog_phone_enabled });
          dispatch({ type: SET_LTE_VOUCHER_OFFER_CODE, payLoad: res.data.info.offerCode });
          dispatch({
            type: SET_LTE_MULTIPLAY_OFFER_DATA,
            payLoad: {
              audience_group_name: res.data.info.audience_group_name ? res.data.info.audience_group_name : '',
              om_campaign_record_no: res.data.info.om_campaign_record_no == 0 || res.data.info.om_campaign_record_no ? res.data.info.om_campaign_record_no : '',
              reservation_no: res.data.info.reservation_no == 0 || res.data.info.reservation_no ? res.data.info.reservation_no : ''
            }
          });

        }
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEOfferDetails Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: GET_LTE_OFFER_DETAIL, payLoad: { error: errorResponse.data.error } });
        dispatch({
          type: SET_LTE_MULTIPLAY_OFFER_DATA,
          payLoad: {
            audience_group_name: '',
            om_campaign_record_no: '',
            reservation_no: '',
          }
        });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // Utill.showAlert(errorResponse.data.error);
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      }, (errorEx) => {
        console.log("getLTEOfferDetails Exception Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: GET_LTE_OFFER_DETAIL, payLoad: [] });
        dispatch({
          type: SET_LTE_MULTIPLAY_OFFER_DATA,
          payLoad: {
            audience_group_name: '',
            om_campaign_record_no: '',
            reservation_no: '',
          }
        });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
 * This function get available Offers from backend and update availableLTEDataPackagesList
 * 
 */
export const getLTEDataPackagesList = (requestParams, billingCycleParams, additionalProps) => {
  let reqParams = requestParams;
  let billingParams = billingCycleParams;
  console.log("Success Data Obj getLTEDataPackagesList:", requestParams);
  console.log("Success Data Obj getLTEDataPackagesList:", billingCycleParams);
  console.log("Success Data Obj getLTEDataPackagesList:", additionalProps);
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      await Utill.apiRequestPost2('LteDataPackage', 'lte', 'ccapp', requestParams, (res) => {
        console.log("Success Data Obj getLTEDataPackagesList:", JSON.stringify(res.data.info));
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(res.data.info.packageList, 'package_name')),
          selectedValue: _.filter(res.data.info.packageList, { 'package_code': res.data.info.defaultSelection })[0].package_name,
          dataRental: _.filter(res.data.info.packageList, { 'package_code': res.data.info.defaultSelection })[0].package_rental,
          data_package_code: res.data.info.defaultSelection,
          defaultIndex: _.findIndex(res.data.info.packageList, ['package_code', res.data.info.defaultSelection]),
          packageList: res.data.info.packageList,

        };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
        reqParams.package_code = res.data.info.defaultSelection;
        billingParams.package_code = res.data.info.defaultSelection;
        let depositParams = {
          package_code: reqParams.package_code,
          cx_identity_type: additionalProps.customerType
        };

        if (reqParams.conn_type === Constants.POSTPAID) {
          console.log('billingParams parameters', billingParams);
          console.log('depositParams parameters', depositParams);
          dispatch(getLtePackageDeposits(depositParams));
          dispatch(getLTEbillingCycleDropdownDetails(billingParams));
        }

        dispatch(getLTEDataPakageDetails(reqParams, selectedData));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEDataPackagesList Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      }, (errorEx) => {
        console.log("getLTEDataPackagesList Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
 * This function get available Offers from backend and update getLTEDataPakageDetails
 * 
 */
export const getLTEDataPakageDetails = (requestParams, selectedDataOb) => {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_LTE_DATA_PACKAGES_LIST, payLoad: selectedDataOb });
      dispatch({ type: LTE_API_CALL, payLoad: true });
      await Utill.apiRequestPost2('LtePackageDetails', 'lte', 'ccapp', requestParams, (res) => {
        console.log("getLTEDataPakageDetails Success Data Obj:", JSON.stringify(res.data.info));
        console.log("Selected Data Obj:", JSON.stringify(selectedDataOb));
        if (_.isObject(res.data.info)) {
          dispatch({ type: GET_LTE_DATA_PACKAGE_DETAIL, payLoad: res.data.info.details });
        }
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEDataPakageDetails Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: GET_LTE_DATA_PACKAGE_DETAIL, payLoad: { error: errorResponse.data.error } });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("getLTEDataPakageDetails Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: GET_LTE_DATA_PACKAGE_DETAIL, payLoad: [] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};


/**
 * This function get available Offers list from backend service getLTEVoicePackagesList
 * 
 */
export const getLTEVoicePackagesList = (requestParams) => {
  let reqParams = requestParams;
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      await Utill.apiRequestPost2('LteVoicePackage', 'lte', 'ccapp', requestParams, (res) => {
        console.log("Success Data Obj:", JSON.stringify(res.data.info));
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(res.data.info.packageList, 'package_name')),
          selectedValue: _.filter(res.data.info.packageList, { 'package_code': res.data.info.defaultSelection })[0].package_name,
          voice_package_code: res.data.info.defaultSelection,
          defaultIndex: _.findIndex(res.data.info.packageList, ['package_code', res.data.info.defaultSelection]),
          packageList: res.data.info.packageList
        };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
        reqParams.package_code = res.data.info.defaultSelection;

        dispatch(getLTEVoicePakageDetails(reqParams, selectedData));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEVoicePackagesList Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      }, (errorEx) => {
        console.log("getLTEVoicePackagesList Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        // const selectedData = {
        //   dropDownData: _.toArray(_.mapValues(res.data.info.OfferList, 'offer_Name')),
        //   selectedValue: _.filter(res.data.info.OfferList, { 'offer_Code': res.data.info.defaultSelection })[0].offer_Name,
        //   defaultIndex: _.findIndex(res.data.info.OfferList, ['offer_Code', res.data.info.defaultSelection]),
        //   offerList: res.data.info.OfferList
        // };
        // console.log("Selected Data Obj:",JSON.stringify(selectedData));
        // dispatch({ type: GET_LTE_OFFERS_LIST, payLoad: selectedData });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
      dispatch({ type: LTE_API_CALL, payLoad: false });
    }
  };
};

/**
 * This function get available voice package details from getLTEVoicePakageDetails
 * 
 */
export const getLTEVoicePakageDetails = (requestParams, selectedDataOb) => {
  return async (dispatch) => {

    try {
      dispatch({ type: GET_LTE_VOICE_PACKAGES_LIST, payLoad: selectedDataOb });
      dispatch({ type: LTE_API_CALL, payLoad: true });
      await Utill.apiRequestPost2('LtePackageDetails', 'lte', 'ccapp', requestParams, (res) => {
        console.log("getLTEVoicePakageDetails Success Data Obj:", JSON.stringify(res.data.info));
        console.log("Selected Data Obj:", JSON.stringify(selectedDataOb));
        if (_.isObject(res.data.info) && res.data.info !== undefined)
          dispatch({ type: GET_LTE_VOICE_PACKAGE_DETAIL, payLoad: res.data.info.details });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEVoicePakageDetails Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: GET_LTE_VOICE_PACKAGE_DETAIL, payLoad: { error: errorResponse.data.error } });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("getLTEVoicePakageDetails Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: GET_LTE_VOICE_PACKAGE_DETAIL, payLoad: [] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
 * Set user enter voucher code into redux store
 * @param {String} text 
 */
export const setVoucherCodeText = (text) => {
  return (dispatch) => {
    dispatch({ type: SET_LTE_VOUCHER_CODE, payLoad: text });
  };
};


/**
 * Set user enter voucher code into redux store
 * @param {String} text 
 */
export const setValidatedVoucherCodeValue = (text) => {
  console.log('xxx setVoucherCodeGroupName');
  return (dispatch) => {
    dispatch({ type: SET_LTE_VALIDATED_VOUCHER_CODE, payLoad: text });
  };
};


//Set Voucher code group name
export const setVoucherCodeGroupName = (text) => {
  console.log('xxx setVoucherCodeGroupName');
  return (dispatch) => {
    dispatch({ type: SET_LTE_VOUCHER_CODE_GROUP_NAME, payLoad: text });
  };
};

//SET_LTE_VOUCHER_OFFER_CODE
export const setVoucherCodeValidityOfferCode = (text) => {
  console.log('xxx setVoucherCodeValidityOfferCode');
  return (dispatch) => {
    dispatch({ type: SET_LTE_VOUCHER_OFFER_CODE, payLoad: text });
  };
};


/**
 * This function will if the entered voucher code is valid
 * 
 */
export const checkVoucherCodeValidity = (data = {}, successCb, voucher_code) => {
  let enteredVoucherCode = (data) ? data.voucher_code : "";
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true, message: 'Validating' });
    try {
      Utill.apiRequestPost('validateVoucherCode', 'lte', 'ccapp', data, (res) => {
        // if (res.data.success == true){
        //   dispatch({ type: VALIDATE_VOUCHER_CODE_TRUE, payLoad: voucher_code });
        // }
        console.log("xxx checkVoucherCodeValidity xxx");
        console.log("checkVoucherCodeValidity Success Data Obj:", JSON.stringify(res.data));
        dispatch({ type: SET_LTE_VOUCHER_CODE_VALIDITY, payLoad: res.data });
        dispatch({ type: SET_LTE_VALIDATED_VOUCHER_CODE, payLoad: voucher_code });
        dispatch({ type: LTE_SET_VOUCHER_CANCELLATION_STATUS, payLoad: false });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        successCb(res.data);
      }, (errorResponse) => {
        console.log("checkVoucherCodeValidity Error Data Obj:", JSON.stringify(errorResponse));
        successCb(errorResponse);
        Utill.showAlertModal(errorResponse.data, enteredVoucherCode);
        dispatch({ type: SET_LTE_VOUCHER_CODE_VALIDITY, payLoad: false });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("checkVoucherCodeValidity Exceptoin Data Obj:", JSON.stringify(errorEx));
        successCb(errorEx);
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
        dispatch({ type: SET_LTE_VOUCHER_CODE_VALIDITY, payLoad: false });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};


//LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS

export const resetValidateVoucherCodeStatus = (lob) => {
  if (lob == Constants.LOB_LTE)
    return { type: LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS, payLoad: '' };
  return { type: DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS, payLoad: '' };
};

/**
 * This function will search telco area user entered
 * 
 */
export const searchTelcoArea = (data = {}) => {

  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    dispatch({ type: SET_LTE_SELECTED_AREA, payLoad: '' });
    try {
      Utill.apiRequestPost('getO2ATelcoAreaCode', 'telcoArea', 'ccapp', data, (res) => {
        console.log("telcoAreaData Success Data Obj:", JSON.stringify(res.data.info));


        let responseData = _.isArray(res.data.info) ? res.data.info : [];
        let cityNames = _.map(responseData, 'city_name');
        let cityCodes = _.map(responseData, 'area_code');
        let error = !_.isArray(res.data.info) ? res.data.info : '';

        console.log('CityNames: ', cityNames, '\nCityCodes', cityCodes);
        let payLoadData = {
          cityNames: cityNames,
          areaCodes: cityCodes,
          searchResult: responseData,
          error: error
        };
        dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad: payLoadData });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("telcoAreaData Error Data Obj:", JSON.stringify(errorResponse));
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));
        dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad: [] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("telcoAreaData Exceptoin Data Obj:", JSON.stringify(errorEx));
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
        dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad: [] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
   * This function will search telco area user entered
   * 
   */
export const setLTESelectedArea = (data = {}, requestParams) => {
  return (dispatch) => {
    dispatch({ type: SET_LTE_SELECTED_AREA, payLoad: data });
    dispatch({ type: LTE_API_CALL, payLoad: false });

    if (_.isObject(requestParams)) {
      dispatch(reserveLTENumberPool(requestParams));
    }
  };
};


/**
 * This function get available dates from backend and update billingCycleDropdown
 * 
 */
export const getLTEbillingCycleDropdownDetails = (billingParams) => {
  console.log('getLTEbillingCycleDropdownDetails API :: billingParams', billingParams);
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      await Utill.apiRequestPost2('getPostpaidBillCycle', 'lte', 'ccapp', billingParams, (res) => {
        console.log("Success Data Obj getPostpaidBillCycle:", JSON.stringify(res.data.info));
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(res.data.info.dates, 'value')),
          selectedValue: _.filter(res.data.info.dates, { 'value': res.data.info.default_date.value })[0].value,
          defaultIndex: _.findIndex(res.data.info.dates, ['value', res.data.info.default_date.value]),
          dates: res.data.info.dates,
          selectedBillingCycleCode: res.data.info.default_date.key
        };

        dispatch(setBillingCycle(selectedData));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("getLTEbillingCycleDropdownDetails Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));
      }, (errorEx) => {
        console.log("getLTEbillingCycleDropdownDetails Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};


/**
 * This function will set the deposit Amount
 * 
 */
export const getLtePackageDeposits = (depositParams) => {
  console.log('xxxx getLtePackageDeposits :: API');
  return (dispatch) => {

    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx getLtePackageDeposits :: successCb', response.data);
        dispatch({ type: GET_LTE_PACKAGE_DEPOSITS_SUCCESS, payLoad: response.data.info });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('xxxx getLtePackageDeposits :: failureCb', response.data.error);
        dispatch({ type: GET_LTE_PACKAGE_DEPOSITS_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('xxxx getLtePackageDeposits :: exceptionCb', error);
        dispatch({ type: GET_LTE_PACKAGE_DEPOSITS_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost('PackageDeposits', 'lte', 'ccapp', depositParams, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
 * This function will set the redux values when user checks/unchecks Add Voice and Analog Phone checkboxes
 * 
 */
export const setLTEVoiceCheckboxData = (data = { addVoiceChecked: false, analogPhoneChecked: false }) => {
  return ({ type: SET_LTE_VOICE_CHECKBOX_DATA, payLoad: data });
};

/**
 * This function will reset Voice packages list and its details reducers
 * 
 */
export const resetLTEVoicePackageData = () => {
  let availableLTEVoicePackagesList = {
    dropDownData: [],
    selectedValue: '',
    defaultIndex: -1
  };

  let selectedLTEVoicePackageDetails = [];

  return (dispatch) => {
    dispatch({ type: GET_LTE_VOICE_PACKAGES_LIST, payLoad: availableLTEVoicePackagesList });
    dispatch({ type: GET_LTE_VOICE_PACKAGE_DETAIL, payLoad: selectedLTEVoicePackageDetails });
  };
};


/**
   * This function will get the EZCashCheckBalance
   * 
   */

export const getEZCashCheckBalance = (ezCashsubmitParams) => {
  console.log('xxxx getLteEZCashCheckBalance :: API');
  return (dispatch) => {

    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx getLteEZCashCheckBalance :: successCb', response.data);
        dispatch({ type: GET_EZCASH_CHECK_BALANCE_SUCCESS, payLoad: response.data.info });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('xxxx getLteEZCashCheckBalance :: failureCb', response.data.error);
        dispatch({ type: GET_EZCASH_CHECK_BALANCE_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('xxxx getLteEZCashCheckBalance :: exceptionCb', error);
        dispatch({ type: GET_EZCASH_CHECK_BALANCE_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost('CheckBlance', 'EzCash', 'ccapp', ezCashsubmitParams, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
   * This function will get the EZCashCheckBalance
   * 
   */

export const getEZCashreverseTransaction = (ezCashreverseTransactionParams) => {
  console.log('xxxx getEZCashreverseTransaction :: API');
  return (dispatch) => {

    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx getEZCashreverseTransaction :: successCb', response.data);
        dispatch({ type: GET_EZCASH_REVERSE_TRANSACTION_SUCCESS, payLoad: response.data.info });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('xxxx getEZCashreverseTransaction :: failureCb', response.data.error);
        dispatch({ type: GET_EZCASH_REVERSE_TRANSACTION_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('xxxx getEZCashreverseTransaction :: exceptionCb', error);
        dispatch({ type: GET_EZCASH_REVERSE_TRANSACTION_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost('reverseTransaction', 'EzCash', 'ccapp', ezCashreverseTransactionParams, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
   * This function will do the submitEZCashTransaction
   * 
   */

export const submitEZCashTransaction = (ezCashSubmitTransactionParams) => {
  console.log('xxxx submitEZCashTransaction :: API');
  return (dispatch) => {

    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx submitEZCashTransaction :: successCb', response.data);
        dispatch({ type: GET_EZCASH_SUBMIT_TRANSACTION_SUCCESS, payLoad: response.data.info });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('xxxx submitEZCashTransaction :: failureCb', response.data.error);
        dispatch({ type: GET_EZCASH_SUBMIT_TRANSACTION_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('xxxx submitEZCashTransaction :: exceptionCb', error);
        dispatch({ type: GET_EZCASH_SUBMIT_TRANSACTION_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost('submitTransaction', 'EzCash', 'ccapp', ezCashSubmitTransactionParams, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};


/**
 * This function will do the GetDeviceSellingPrice
 * 
 */
export const LteGetDeviceSellingPrice = (DeviceSellingPrams) => {
  console.log('xxxx LteGetDeviceSellingPrice :: API');
  return (dispatch) => {

    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx LteGetDeviceSellingPrice :: successCb', response.data);
        dispatch({ type: GET_LTE_DEVICE_SELLING_PRICE_SUCCESS, payLoad: response.data.info });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('xxxx LteGetDeviceSellingPrice :: failureCb', response.data.error);
        dispatch({ type: GET_LTE_DEVICE_SELLING_PRICE_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('xxxx LteGetDeviceSellingPrice :: exceptionCb', error);
        dispatch({ type: GET_LTE_DEVICE_SELLING_PRICE_FAIL, payLoad: null });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost('GetDeviceSellingPrice', 'lte', 'ccapp', DeviceSellingPrams, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
   * This function will reserve a block of numbers from backend this
   * 
   * {
   *    "area_code":"011", // Mandatory
   *    "number_count":10, // Optional - Default 1
   *    "conn_type":"PREPAID", // Mandatory
   *    "start_with":254, // Optional
   *    "end_with":584, // Optional
   *    "includes":8855 // Optional
   * }
   * 
   * 
   */
export const reserveLTENumberPool = (data = {}) => {
  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      Utill.apiRequestPost('numberPoolReserve', 'lte', 'ccapp', data, (res) => {

        console.log("Reserved Number List: ", JSON.stringify(res.data.reserved_number_list));
        console.log("data: ", JSON.stringify(data));

        let payloadData = {
          reserved_number_list: res.data.reserved_number_list,
          number_list: [] // This will be assigned in below if else block
        };

        if (res.data.reserved_number_list.single === undefined) {
          if (_.isObject(res.data.reserved_number_list.cdma_series_4)) {
            payloadData.number_list = [...payloadData.number_list, ...res.data.reserved_number_list.cdma_series_4.number_list];
          }
          if (_.isObject(res.data.reserved_number_list.cdma_series_7)) {
            payloadData.number_list = [...payloadData.number_list, ...res.data.reserved_number_list.cdma_series_7.number_list];
          }
        } else {
          payloadData.number_list = res.data.reserved_number_list.single.number_list;
          let autoSelectedNumber = payloadData.number_list !== null ? payloadData.number_list[0] : '';
          if (_.isObject(autoSelectedNumber) && data.search === undefined)
            dispatch(setSelectedConnectionNumber({ selected_number: autoSelectedNumber.value, blocked_id: payloadData.blocked_id }));
          dispatch(lteSetProductInfoValidationStatus(true));
        }

        dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad: payloadData });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorResponse) => {
        console.log("telcoAreaData Error Data Obj:", JSON.stringify(errorResponse));
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));
        // dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad:[] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        dispatch(lteSetProductInfoValidationStatus(false));
      }, (errorEx) => {
        console.log("telcoAreaData Exceptoin Data Obj:", JSON.stringify(errorEx));
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
        // dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad:[] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        dispatch(lteSetProductInfoValidationStatus(false));
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

/**
   * This function will relese a block of numbers from backend 
   * 
   */
export const releaseLTENumberPool = (data = {}, type = { type: Constants.RELEASE_TYPES.RELEASE }) => {

  console.log('releaseLTENumberPool => data => ', data);
  console.log('releaseLTENumberPool => type => ', type);

  let reserved_number_list = {
    reserved_number_list: {},
    number_list: []
  };

  return async (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    // dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad:reserved_number_list });

    try {
      Utill.apiRequestPost('numberPoolRelease', 'lte', 'ccapp', data, (res) => {

        console.log("releaseLTENumberPool Number List: ", JSON.stringify(res.data));
        dispatch({ type: LTE_API_CALL, payLoad: false });

        if (res.data.selected_number !== undefined && res.data.selected_number !== "") {
          let selected_number = res.data.selected_number;
          let blocked_id = res.data.blocked_id;
          dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad: reserved_number_list });
          dispatch({
            type: LTE_RESERVE_NUMBER_POOL, payLoad: {
              reserved_number_list: {
                reserved: {
                  blocked_id: blocked_id,
                  number_list: [{ value: selected_number, label: selected_number }]
                }
              }
            }
          });
          dispatch(setSelectedConnectionNumber({ selected_number, blocked_id }));
        }

        if (type.type == Constants.RELEASE_TYPES.REFRESH) {
          // dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad:reserved_number_list });
          dispatch(reserveLTENumberPool(type.requestParams));
        }

        if (type.type == Constants.RELEASE_TYPES.SEARCH) {
          // dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad: reserved_number_list });
          dispatch(reserveLTENumberPool(type.requestParams));
        }

        if (type.type == Constants.RELEASE_TYPES.RELEASE) {
          // dispatch(setSelectedConnectionNumber(''));

          dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad: data.release });
        }

        if (type.type == Constants.RELEASE_TYPES.RELEASE_ALL) {
          console.log('in reserved_number_list1', data.release);

          // dispatch(setSelectedConnectionNumber(''));
          dispatch({ type: LTE_RESERVE_NUMBER_POOL, payLoad: reserved_number_list });
        }

      }, (errorResponse) => {
        console.log("releaseLTENumberPool Error Data Obj:", JSON.stringify(errorResponse));
        // Utill.showAlert(errorResponse.data.error);
        // dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad:[] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("releaseLTENumberPool Exceptoin Data Obj:", JSON.stringify(errorEx));
        // Utill.showAlert(errorEx);
        // dispatch({ type: SET_LTE_TELCO_AREA_DATA, payLoad:[] });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

export const setSelectedConnectionNumber = (data = {}) => {
  console.log('selected number data', data);
  return ({ type: SET_SELECTED_CONNECTION_NUMBER, payLoad: data });
};

export const resetSelectedConnectionNumber = () => {
  return ({ type: RESET_SELECTED_CONNECTION_NUMBER, payLoad: null });
};

export const numberPoolRelease = (type = {}, searchParams = {}, thisContext) => { // Types: 'refresh', 'release'
  return (dispatch) => {
    let releaseNumbers = [];
    let me = thisContext;
    let releaseType;
    let req = {
      "release": [],
      "selected_number": ""
    };

    let selected_number;

    console.log("reserved_number_list.reserved_number_list", me.props.reserved_number_list.reserved_number_list);

    // reserved_number_list is set when a batch of numbers are reserved.
    _.forEach(me.props.reserved_number_list.reserved_number_list, function (value, key) {
      // get blocked_id from each number type
      let blocked_id = value.blocked_id;
      // extract values list from each number type
      let number_list = _.map(value.number_list, "value");
      // Current selected number

      console.log("blocked_id: ", blocked_id);
      console.log("number_list: ", number_list);
      console.log("key: ", key);

      // if (type.type !== Constants.RELEASE_TYPES.RELEASE_ALL){
      //   let index = number_list.indexOf(selected_number);
      //   if (index > -1){
      //     console.log("Selected number exists..", selected_number);
      //     number_list.splice(index,1);
      //   }
      // }
      // for each number type in reserved_number_list, an object with blocked_id and it's set of numbers will be pushed
      // this will be sent to backend to be released to number pool. 
      releaseNumbers.push({
        blocked_id,
        number_list
      });
    });

    console.log('in numberPoolRelease ==>> releaseNumbers', releaseNumbers);
    // if the user has selected a number, find the selected number in the number list and remove it. Otherwise the 
    // selected number will also be released.

    if (me.state.selectedNumber == '') {
      console.log("selected_number: ", me.props.selected_connection_number.selected_number);
      if (me.props.selected_connection_number.selected_number == '') {
        selected_number = '';
      } else {
        selected_number = me.props.selected_connection_number.selected_number;
      }
    } else {
      console.log("selected_number: ", me.state.selectedNumber);
      selected_number = me.state.selectedNumber;
    }

    switch (type.type) {
      case Constants.RELEASE_TYPES.RELEASE_ALL:
        console.log("Release Mode: Release All");
        req.release = releaseNumbers;
        req.selected_number = "";
        console.log("Req: ", JSON.stringify(req));

        dispatch(releaseLTENumberPool(req, type));
        break;
      case Constants.RELEASE_TYPES.RELEASE:
        console.log("Release Mode: Release");
        req.release = releaseNumbers;
        req.selected_number = selected_number;
        console.log("Req: ", JSON.stringify(req));
        dispatch(releaseLTENumberPool(req, releaseType));

        break;

      case Constants.RELEASE_TYPES.REFRESH:
        console.log("Release Mode: Refresh");
        req.release = releaseNumbers;
        req.selected_number = selected_number;
        console.log("Req: ", JSON.stringify(req));

        releaseType = type;

        releaseType.requestParams = {
          "area_code": me.props.lteSelectedArea.area_code,
          "number_count": 10,
          "conn_type": me.props.selected_connection_type,
        };
        console.log("releseType: ", releaseType);
        // requestParams - params needed to release the numbers
        // releaseType - used to decide if a new set of numbers should be reserved after release. 
        //               This has a signature of { type:'', requestParams:{area_code:'',number_count:'',conn_type:''} }
        dispatch(releaseLTENumberPool(req, releaseType));

        break;
      case Constants.RELEASE_TYPES.SEARCH:
        console.log("Release Mode: Search");
        req.release = releaseNumbers;
        req.selected_number = selected_number;
        console.log("Req: ", JSON.stringify(req));

        releaseType = type;

        releaseType.requestParams = {
          "area_code": me.props.lteSelectedArea.area_code,
          "number_count": 10,
          "conn_type": me.props.selected_connection_type,
        };
        // if release type is search, append keys of searchParams to releaseType.requestParams
        releaseType.requestParams = { ...releaseType.requestParams, ...searchParams };
        if (releaseType.requestParams.search !== undefined) {
          releaseType.requestParams.number_count = 1; // As requested by backend, count should be 1 only when searching. 
        }
        dispatch(releaseLTENumberPool(req, releaseType));
        break;
      default:
        console.log("type.type: ", type.type);
    }
  };
};

export const coverageMapStatusCheck = (successCb, data = {}) => {
  console.log('coverageMapStatusCheck => data => ', data);

  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });
    try {
      Utill.apiRequestPost('getLocationStatus', 'networkCoverage', 'ccapp', data, (res) => {
        const { info = undefined } = res.data;
        dispatch({ type: LTE_API_CALL, payLoad: false });
        successCb(info);
      }, (errorResponse) => {
        console.log("coverageMapStatusCheck Error Data Obj:", JSON.stringify(errorResponse));
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("coverageMapStatusCheck Exceptoin Data Obj:", JSON.stringify(errorEx));
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};

export const requestLocationPermission = (successCb, data = {}) => {
  console.log('requestLocationPermission => data => ', data);
  return (dispatch) => {
    dispatch({ type: LTE_API_CALL, payLoad: true });

    try {
      Utill.apiRequestPost('getCurrentLocation', 'networkCoverage', 'ccapp', data, (res) => {
        const { info = undefined } = res.data;
        dispatch({ type: LTE_API_CALL, payLoad: false });
        if (info) {
          const { refId = '' } = info;
          dispatch({ type: LTE_SET_REFERENCE_ID, payLoad: refId });
        }
        successCb(info);
      }, (errorResponse) => {
        console.log("requestLocationPermission Error Data Obj:", JSON.stringify(errorResponse));
        //Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));
        dispatch({ type: LTE_API_CALL, payLoad: false });
      }, (errorEx) => {
        console.log("requestLocationPermission Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: LTE_API_CALL, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
      });
    } catch (e) {
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Err: ', e);
    }
  };
};


/**
 * This function will do the GetConfigurations
 * 
 */
export const getConfigurations = (screen, key) => {
  console.log('Redux :: getConfigurations :: API ', requestParams);
  let lobType;
  switch (key) {
    case 3:
      lobType = Constants.LOB_GSM;
      break;
    case 4:
      lobType = Constants.LOB_DTV;
      break;
    case 9:
      lobType = Constants.LOB_LTE;
      break;
    default:
      lobType = Constants.LOB_ALL;
      break;
  }

  let requestParams = {
    type: 'numberCheck',
    lob: lobType
  };

  return (dispatch) => {
    try {
      dispatch(getApiLoading(true));

      const successCb = function (response) {
        console.log('Redux :: getConfigurations :: successCb', response.data);
        dispatch({ type: GET_CONFIGURATION, payload: response.data, status: true });
        dispatch(getApiLoading(false));
        screen.checkDefaultAccountAndPush(key);
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('Redux :: getConfigurations :: failureCb', response.data.error);
        dispatch({ type: GET_CONFIGURATION, payload: {}, status: false });
        dispatch(getApiLoading(false));
      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        console.log('Redux :: getConfigurations :: exceptionCb', error);
        dispatch({ type: GET_CONFIGURATION, payload: {}, status: false });
        dispatch(getApiLoading(false));
      };

      Utill.apiRequestPost2('getConfigurations', 'configuration', 'ccapp', requestParams, successCb, failureCb, exCb);
    } catch (e) {
      Utill.showAlert(MessageUtils.getSystemErrorMessage());
      dispatch(getApiLoading(false));
      console.log('Redux :: getConfigurations :: Err: ', e);
    }
  };
};

/**
 * This function will get the LTE configuration Data
 * 
 */
export const LteGetConfiguration = () => {
  console.log('Redux :: LteGetConfiguration :: API');
  return (dispatch) => {
    try {
      dispatch({ type: LTE_API_CALL, payLoad: true });

      const successCb = function (response) {
        console.log('Redux :: LteGetConfiguration :: successCb', response.data);
        dispatch({ type: CONFIG_LTE_SET_CONFIGURATION, payLoad: response.data.data, status: true });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      const failureCb = function (response) {
        console.log('Redux :: LteGetConfiguration :: failureCb', response.data.error);
        dispatch({ type: CONFIG_LTE_SET_CONFIGURATION, payLoad: null, status: false });
        dispatch({ type: LTE_API_CALL, payLoad: false });
        //dispatch(LteGetConfiguration());

      };

      const exCb = function (error) {
        console.log('Redux :: LteGetConfiguration :: exceptionCb', error);
        dispatch({ type: CONFIG_LTE_SET_CONFIGURATION, payLoad: null, status: false });
        dispatch({ type: LTE_API_CALL, payLoad: false });
      };

      Utill.apiRequestPost2('getConfigurations', 'lte', 'ccapp', { lob: Constants.LOB_LTE }, successCb, failureCb, exCb);
    }
    catch (e) {
      dispatch({ type: CONFIG_LTE_SET_CONFIGURATION, payLoad: null, status: false });
      dispatch({ type: LTE_API_CALL, payLoad: false });
      console.log('Redux :: LteGetConfiguration :: Err: ', e);
    }
  };
};


//==================== CFSS related =======================================================
//STOCK ACCEPTANCE
export const stockAcceptanceInfo = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_API, payLoad: input };
};

export const stockAcceptanceDetailInfo = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_DETAILS_API, payLoad: input };
};

export const stockAcceptanceElementsList = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_ELEMENT_LIST, payLoad: input };
};

export const stockAcceptanceIssuedSerialList = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_LIST, payLoad: input };
};

export const stockAcceptanceScannedSerialList = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_LIST, payLoad: input };
};

export const stockAcceptanceIssuedSerialStatus = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_STATUS, payLoad: input };
};

export const stockAcceptanceSessionSerials = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_SESSION_SERIALS, payLoad: input };
};

export const stockAcceptanceScannedSerialCheck = (input) => {
  console.log("ACCEPT_STOCK>>>>>" + this.props.accept_stock);
  return { type: GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_CHECK, payLoad: input };
};

export const stockAcceptanceMismatchedSerials = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_MISMATCHED_SERIALS, payLoad: input };
};

export const stockAcceptanceSelectedSTOID = (input) => {
  return { type: GET_STOCK_ACCEPTANCE_SELECTED_STO_ID, payLoad: input };
};

//Accessory Sales
export const getAccessorySaleDetail = (input) => {
  return { type: ACCESSORY_SALE_DETAIL, payLoad: input };
};

export const accessorySalesEzCashPIN = (input) => {
  return { type: ACCESSORY_SALES_EZ_CASH_PIN, payLoad: input };
};

export const accessorySalesReset = () => {
  return { type: ACCESSORY_SALES_RESET };
};

//Warranty Replacement
export const warrantyReplacementDetails = (input) => {
  return { type: WARRANTY_REPLACEMENT_DETAILS, payLoad: input };
};

export const warratyReplacementGetItemType = (input) => {
  return { type: WARRANTY_REPLACEMENT_GET_ITEM_TYPE, payLoad: input };
};

export const warratyReplacementGetOfferCode = (input) => {
  return { type: WARRANTY_REPLACEMENT_OFFER_CODE, payLoad: input };
};

export const warratyReplacementGetItemCode = (input) => {
  return { type: WARRANTY_REPLACEMENT_ITEM_CODE, payLoad: input };
};

export const warratyReplacementGetConnectionType = (input) => {
  return { type: WARRANTY_REPLACEMENT_CONNECTION_TYPE, payLoad: input };
};

export const warratyReplacementReset = (input) => {
  return { type: WARRANTY_REPLACEMENT_RESET, payLoad: input };
};

//Work Order Management
export const clearWOStates = () => {
  return { type: CLEAR_STATES };
};

export const workOrderGetDetail = (input) => {
  return { type: WORK_ORDER_DETAIL, payLoad: input };
};

export const workOrderGetCustomerSignature = (input) => {
  return { type: WORK_ORDER_CUSTOMER_SIGNATURE, payLoad: input };
};

export const workOrderGetDTVProvisioning = (input) => {
  return { type: WORK_ORDER_DTV_PROVISIONING, payLoad: input };
};

export const workOrderGetProvisioningStatus = (input) => {
  return { type: WORK_ORDER_PROVISIONING_STATUS, payLoad: input };
};

export const workOrderGetInstalledItems = (input) => {
  return { type: WORK_ORDER_INSTALLED_ITEMS, payLoad: input };
};

export const workOrderReload = (input) => {
  return { type: WORK_ORDER_SUMMARY_RELOAD, payLoad: input };
};

export const workOrderReset = () => {
  return { type: WORK_ORDER_RESET };
};

export const workOrderCustomerFeedback = (input) => {
  return { type: WORK_ORDER_CUSTOMER_FEEDBACK, payLoad: input };
};

export const workOrderCustomerAnswers = (input) => {
  return { type: WORK_ORDER_CUSTOMER_ANSWERS, payLoad: input };
};

export const workOrderdtvinstallation = (input) => {
  return { type: WORK_ODER_DTV_INSTALLATION, payLoad: input };
};

export const workOrderSerialList = (input) => {
  return { type: WORK_ORDER_SERIAL_LIST, payLoad: input };
};

export const workOrderSignatureData = (input) => {
  return { type: WORK_ORDER_SIGNATURE_DATA, payLoad: input };
};

export const workOrderRootCauseData = (input) => {
  return { type: WORK_ORDER_ROOT_CAUSES, payLoad: input };
};

export const workOrderRootCauseDataReset = () => {
  return { type: CLEAR_ROOT_CAUSE_DATA };
};

export const workOrderAppScreen = (input) => {
  return { type: WORK_ORDER_APP_SCREEN, payLoad: input };
};

export const workOrderAppFlow = (input) => {
  return { type: WORK_ORDER_APP_FLOW, payLoad: input };
};

export const workOrderTRBSales = (input) => {
  return { type: WORK_ORDER_TRB_ACCESSORY_SALE, payLoad: input };
};

export const updateRootCauseList = (inputArray) => {
  return { type: UPDATE_WORK_ORDER_ROOT_CAUSES, payLoad: inputArray };
};

export const workOrderRefixWarrantyDetails = (inputArray) => {
  return { type: WORK_ORDER_REFIX_WARRANTY_DETAILS, payLoad: inputArray };
};

export const setWOJobStatus = (status) => {
  return { type: WORK_ORDER_JOB_STATUS, payLoad: status };
};

export const setPaymentDetails = (values) => {
  return { type: PAYMENT_DETAILS, payLoad: values };
};

export const addAdditionalHybridProducts = (values) => {
  return { type: ADD_ADDITIONAL_HYBRID_PROD, payLoad: values };
};

export const clearTRBWO = () => {
  return { type: CLEAR_TRB_WO };
};

export const getSIMSTBCount = (count) => {
  return { type: SIM_STB_CHANGE_COUNT, payLoad: count };
};

export const setJobHistoryDetails = (values) => {
return { type: JOB_HISTORY_DETAILS, payLoad: values}
}

//==================== END CFSS related =======================================================

/********************************** DTV Related **********************/

//RESET_DTV_DATA_PACKAGES
export const resetDTVData = () => {
  return (dispatch) => {
    dispatch({ type: RESET_DTV_DATA, payLoad: '' });
  };
};

/**
 * This function will validate customer NIC number
 * DTV - API 1
 */
export const dtvCustomerValidation = (data, connType, screen) => {
  console.log('Redux :: dtvCustomerValidation API');
  console.log('Redux :: dtvCustomerValidation :: connType ', connType);

  let url = {
    action: 'customerValidation',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'customer_validation',
    action_type: 'api_request',
    additional_details: 'DTV customer validation',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true, message: 'Validating' });
    dispatch(commonSetActivityStartTime());
    const successCb = function (response) {
      console.log('Redux :: dtvCustomerValidation :: successCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      Keyboard.dismiss();
      if (connType == Constants.PREPAID && response.success) {
        console.log('Redux :: dtvCustomerValidation :: PREPAID ################');
        dispatch({ type: DTV_ID_VALIDATION_SUCCESS, payLoad: response });
        dispatch(dtvSetTnxReference(response.txn_reference));
        screen.checkPackageList(response);
        if (response.cxExistStatus == Constants.CX_EXIST_YES) {
          console.log('PREPAID_OTP_FLOW');
          screen.checkCustomerOutstanding(response);
        } else {
          console.log('PREPAID_NEW_CUSTOMER_NORMAL_FLOW');
        }
      } else if (connType == Constants.POSTPAID && response.success) {
        console.log('Redux :: dtvCustomerValidation :: POSTPAID ################');
        dispatch({ type: DTV_ID_VALIDATION_SUCCESS, payLoad: response });
        dispatch(dtvSetTnxReference(response.txn_reference));
        screen.checkPackageList(response);
        if (response.cxExistStatus == Constants.CX_EXIST_YES) {
          console.log('POSTPAID_OUTSTANDING_CHECK');
          screen.checkCustomerOutstanding(response);
        } else {
          console.log('POSTPAID_NEW_CUSTOMER_NORMAL_FLOW');
        }
      }
      else {
        dispatch({ type: DTV_ID_VALIDATION_FAIL, payLoad: null });
        console.log('Redux :: dtvCustomerValidation :: NEGATIVE => connType -' + connType);
        if (response.message_type == Constants.MSG_TYPE_MODAL) {
          console.log('MSG_TYPE_MODAL');
          screen.showModalTypeMessage(response, response.error);
        } else if (response.message_type == Constants.MSG_TYPE_INLINE) {
          console.log('MSG_TYPE_INLINE');
          screen.showInlineErrorMessageIdNumber(response, response.error);
        } else if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
        } else {
          console.log('MSG_TYPE_ALERT');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('Redux :: dtvCustomerValidation :: failureCb ', response);
      dispatch({ type: DTV_ID_VALIDATION_FAIL, payLoad: null });
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, data, successCb, failureCb, handleFalseResponse);
  };
};

//DTV_SET_TXN_REFERENCE
export const dtvSetTnxReference = (txn_reference) => {
  return { type: DTV_SET_TXN_REFERENCE, payLoad: txn_reference };
};

//DTV_SET_ID_NUMBER_VALIDATION_STATUS
export const dtvSetIdNumberValidationStatus = (input) => {
  return { type: DTV_SET_ID_NUMBER_VALIDATION_STATUS, payLoad: input };
};

//DTV_SET_PRODUCT_INFO_VALIDATION_STATUS
export const dtvSetProductInfoValidationStatus = (input) => {
  return { type: DTV_SET_PRODUCT_INFO_VALIDATION_STATUS, payLoad: input };
};

//DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS
export const dtvSetCustomerInfoValidationStatus = (input) => {
  return { type: DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS, payLoad: input };
};

export const dtvSetSectionVisibility = (key, validation_status, current_View_status) => {
  let data = {
    section_1: current_View_status.section_1,
    section_2: current_View_status.section_2,
    section_3: current_View_status.section_3,
  };

  switch (key) {
    case Constants.MAIN_SECTION.NIC_VALIDATION:
      data = {
        section_1: true,
        section_2: false,
        section_3: false,
      };
      break;
    case Constants.MAIN_SECTION.PRODUCT_INFORMATION:
      if (validation_status.section_1 || validation_status.section_2 || validation_status.section_3) {
        data = {
          section_1: false,
          section_2: true,
          section_3: false,
        };
      }
      break;
    case Constants.MAIN_SECTION.CUSTOMER_INFORMATION:
      if ((validation_status.section_1 && validation_status.section_2) || validation_status.section_3) {
        data = {
          section_1: false,
          section_2: false,
          section_3: true,
        };
      }
      break;
    default:
      break;
  }

  return { type: DTV_SET_SECTIONS_VISIBLE, payLoad: data };
};


//DTV_INPUT_ID_NUMBER
export const dtvSetInputIdNumber = (input) => {
  return { type: DTV_INPUT_ID_NUMBER, payLoad: input };
};

//DTV_SET_ID_NUMBER_ERROR
export const dtvSetIdNumberError = (error) => {
  return { type: DTV_SET_ID_NUMBER_ERROR, payLoad: error };
};


//DTV_SELECTED_CONNECTION_TYPE
export const setDTVConnectionType = (input) => {
  return { type: DTV_SELECTED_CONNECTION_TYPE, payLoad: input };
};

//DTV_SET_CUSTOMER_TYPE
export const dtvSetCustomerType = (input) => {
  return { type: DTV_SET_CUSTOMER_TYPE, payLoad: input };
};

//SET DTV PACKAGES DROP DOWN
export const setDtvdropdown = (data = {}) => {
  return (dispatch) => {
    dispatch({ type: SET_DTV_DROPDOWN_INDEX, payLoad: data });
  };
};


//DTV send OTP PIN
export const dtvSendOtpMessage = (requestParams, screen) => {
  console.log('Redux :: dtvSendOtpMessage API');
  Analytics.logEvent('dsa_dtv_activation_otp_send');
  console.log('Redux :: dtvSendOtpMessage :: requestParams :', requestParams);

  let url = {
    action: 'otpSend',
    controller: 'common',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'send_otp_message',
    action_type: 'api_request',
    additional_details: 'DTV send OTP',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    const successCb = function (response) {
      console.log('Redux :: dtvSendOtpMessage :: successCb ', response);
      console.log('Redux :: dtvSendOtpMessage :: successCb ', response.message);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      if (response.success) {
        console.log('OTP_SEND_CHECK_SUCCESS');
        if (requestParams.ref_id) {
          console.log('OTP_RESEND');
          screen.otpResendSuccessfulMessage(response.message);
        } else {
          console.log('OTP_SEND');
          screen.showOtpEnteringModal(response);
        }
      }
      else {
        console.log('Redux :: dtvSendOtpMessage  :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('OTP_SEND_CHECK_FAIL_NO_ALERT');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('OTP_SEND_CHECK_FAIL_ALERT');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('Redux :: dtvSendOtpMessage :: failureCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

//DTV verify OTP
export const dtvVerifyOtp = (requestParams, screen) => {
  console.log('Redux :: dtvVerifyOtp API');
  console.log('Redux :: dtvVerifyOtp :: requestParams : ', requestParams);

  let url = {
    action: 'otpVerify',
    controller: 'common',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'otp_verify',
    action_type: 'api_request',
    additional_details: 'DTV verify OTP',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    const successCb = function (response) {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      console.log('Redux :: dtvVerifyOtp :: successCb ', response);
      if (response.success) {
        console.log('VERIFY_OTP_SUCCESS');
        dispatch({ type: DTV_OTP_VALIDATION_SUCCESS, payLoad: response });
        screen.modalDismiss();
        Analytics.logEvent('dsa_dtv_activation_otp_success', { customerNumber: requestParams.otp_msisdn });
      }
      else {
        console.log('Redux :: dtvVerifyOtp :: NEGATIVE');
        dispatch({ type: DTV_OTP_VALIDATION_FAIL, payLoad: null });
        Analytics.logEvent('dsa_dtv_activation_otp_failed', { customerNumber: requestParams.otp_msisdn });
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('VERIFY_OTP_FAIL');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('VERIFY_OTP_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('Redux :: dtvVerifyOtp :: failureCb ', response);
      Analytics.logEvent('dsa_dtv_activation_otp_failed', { customerNumber: requestParams.otp_msisdn });
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      dispatch({ type: DTV_OTP_VALIDATION_FAIL, payLoad: null });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

//DTV customer outstanding check
export const dtvCustomerOutstandingCheck = (data, customerData, screen) => {
  console.log('Redux :: dtvCustomerOutstandingCheck API');
  console.log('Redux :: dtvCustomerOutstandingCheck :: customerData ', customerData);
  let url = {
    action: 'getO2AConnectionOsDetails',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'outstanding_check',
    action_type: 'api_request',
    additional_details: 'DTV Outstanding Check',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    const successCb = function (response) {
      console.log('Redux :: dtvCustomerOutstandingCheck :: successCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      if (response.success) {
        dispatch({ type: DTV_OUTSTANDING_CHECK_SUCCESS, payLoad: response.info });
        dispatch({ type: DTV_SET_TOTAL_OUTSTANDING_AMOUNT, payLoad: response.totalOsAmount });

        console.log('OUTSTANDING_CHECK_SUCCESS');
        screen.showOutstandingConfirmModal(customerData, response);
        Analytics.logEvent('dsa_dtv_outstanding_modal_appear');
      }
      else {
        console.log('Redux :: dtvCustomerOutstandingCheck :: NEGATIVE');
        dispatch({ type: DTV_OUTSTANDING_CHECK_SUCCESS, payLoad: [] });

        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('OUTSTANDING_CHECK_SUCCESS_NO_OUTSTANDING');
          screen.customerWithoutOutstanding(customerData, response);
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('OUTSTANDING_CHECK_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('Redux :: dtvCustomerOutstandingCheck :: failureCb ', response);
      console.log('OUTSTANDING_CHECK_ERROR');
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      dispatch({ type: DTV_OUTSTANDING_CHECK_FAIL, payLoad: null });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, data, successCb, failureCb, handleFalseResponse);
  };
};


/**
 * This function will get package details
 * DTV - API 2
 */
export const getDtvDataPackages = (requestParams) => {
  console.log('Redux :: getDtvDataPackages :: API');

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_packages',
    action_type: 'api_request',
    additional_details: 'DTV get packages',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    try {
      const successCb = function (response) {
        console.log('Redux :: getDtvDataPackages:: successCb', response.data);
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(response.data.packageList, 'package_name')),
          selectedValue: response.data.package_code ? _.filter(response.data.packageList, { 'package_code': response.data.package_code })[0].package_name : '',
          data_package_code: response.data.package_code,
          defaultIndex: _.findIndex(response.data.packageList, ['package_code', response.data.defaultSelection]),
          packageList: response.data.packageList,
          selectedFullFillmentType: response.data.fullfilment_type,
          default_fullfil_status: response.data.default_fullfil_status,
          package_rental: response.data.package_rental,
          selectedValueName : response.data.package_code ? _.filter(response.data.packageList, { 'package_code': response.data.package_code })[0].package_name_wo_price : '',

        };

        // dispatch(getDtvFullFillmentType(selectedData)); //TODO
        console.log('Redux :: getDtvDataPackages :: successCb', selectedData);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        dispatch({ type: GET_DTV_DATA_PACKAGES_LIST, payLoad: selectedData });

        if (response.data.defaultIndex === 0) {
          console.log("Redux :: getDtvDataPackages :: received one package");
          let packageList = response.data.packageList;
          let selectedPackageCode = response.data.packageList[0].package_code;
          let requestParamsPack = requestParams;

          requestParamsPack.package_code = selectedPackageCode;

          let selectedData = {
            dropDownData: _.toArray(_.mapValues(packageList, 'package_name')),
            data_package_code: selectedPackageCode,
            selectedValue: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name,
            defaultIndex: _.findIndex(packageList, ['package_code', selectedPackageCode]),
            packageList: packageList,
            default_fullfil_status: response.data.default_fullfil_status,
            selectedFullFillmentType: _.filter(packageList, { 'package_code': selectedPackageCode })[0].fullfilment_type,
            package_rental: _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_rental,
            conn_type:requestParams.conn_type,
            selectedValueName : _.filter(packageList, { 'package_code': selectedPackageCode })[0].package_name_wo_price
          };

          let packTypeParams = requestParamsPack;
          let modalType = "datapackages";
          packTypeParams.fulfilment_type = response.data.fullfilment_type[0].code;
        
          dispatch(getDtvPackageDetails(requestParamsPack, selectedData));
          dispatch(getDtvFullFillmentType(selectedData,this, modalType));
          dispatch(getDtvPackType(packTypeParams));

          let paymentProps = {
            totalDailyRental: response.data.packageList[0].package_rental
          };
          dispatch(setSelectedChannelTotalPayment(paymentProps));
        }

      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        console.log('Redux :: getDtvDataPackages :: failureCb', response.data.error);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      };

      const expCb = function (response) {
        Utill.showAlert(MessageUtils.getExceptionMessage(response));
        console.log('Redux :: getDtvDataPackages :: expCb', response);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      };

      Utill.apiRequestPost2('getPackageList', 'dtv', 'ccapp', requestParams, successCb, failureCb, expCb);
    }
    catch (e) {
      console.log('Redux :: getDtvDataPackages :: EXCEPTION ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    }
  };

};

/**
 * This function will set the getDtvPackageDetails for packageSelectionModal
 * 
 */
export const getDtvPackageDetails = (requestParams, selectedData) => {
  console.log('xxxx getDtvPackageDetails :: API', selectedData);
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_package_details',
    action_type: 'api_request',
    additional_details: 'DTV get package details',
  });

  return (dispatch) => {
    try {

      dispatch({ type: GET_DTV_DATA_PACKAGES_LIST, payLoad: selectedData });
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });

      const successCb = function (response) {
        console.log('xxxx getDtvPackageDetails :: successCb', response.data);
        dispatch({ type: GET_DTV_DATA_PACKAGE_DETAIL, payLoad: response.data.package_details_list });
        dispatch({ type: GET_DTV_DELIVERY_CHARGE, payLoad: response.data.delivery_charge });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      };

      const failureCb = function (response) {
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response.data));
        let dropDownData1 = selectedData;
        dropDownData1.selectedValue = '';
        console.log('xxxx getDtvPackageDetails :: failureCb', response.data.error);
        dispatch({ type: GET_DTV_DATA_PACKAGE_DETAIL, payLoad: { error: response.data.error } });
        dispatch({ type: GET_DTV_DELIVERY_CHARGE, payLoad: 0 });
        dispatch({ type: GET_DTV_DATA_PACKAGES_LIST, payLoad: dropDownData1 });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });

      };

      const exCb = function (error) {
        Utill.showAlert(MessageUtils.getExceptionMessage(error));
        let dropDownData2 = selectedData;
        dropDownData2.selectedValue = '';
        console.log('xxxx getDtvPackageDetails :: exceptionCb', error);
        dispatch({ type: GET_DTV_DATA_PACKAGE_DETAIL, payLoad: { error: error } });
        dispatch({ type: GET_DTV_DELIVERY_CHARGE, payLoad: 0 });
        dispatch({ type: GET_DTV_DATA_PACKAGES_LIST, payLoad: dropDownData2 });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });

      };

      Utill.apiRequestPost2('getPackageDetails', 'dtv', 'ccapp', requestParams, successCb, failureCb, exCb);
    }
    catch (e) {
      console.log('Err: ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    }
  };
};


/**
 * This function will get the Aditional channel list
 * 
 */
export const getDtvAdditionalChannels = (requestParams, successCallback, screen) => {
  console.log('Redux :: getDtvAdditionalChannels :: API', requestParams);

  let url = {
    action: 'getPackageAdditionals',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_additional_channels',
    action_type: 'api_request',
    additional_details: 'DTV get addtional channels',
  });


  return (dispatch) => {

    try {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });

      const successCb = function (response) {
        console.log('Redux :: getDtvAdditionalChannels :: successCb', response);
        if (response.success) {
          console.log('getDtvAdditionalChannels :: success');
          successCallback(response);
          let modifiedChannelList = _.forEach(response.package_details_list.channel_list, function (item) {
            _.forEach(item.channels, function (channelObj) {
              channelObj.already_in_pack = false;
            });
          });

          let channelData = {
            package_name: response.package_details_list.package_name,
            package_code: response.package_details_list.package_code,
            package_rental: response.package_details_list.package_rental,
            genre_name_list: _.map(response.package_details_list.channel_list, 'genre_name'),
            genre_code_list: _.map(response.package_details_list.channel_list, 'genre_code'),
            channel_list: modifiedChannelList,
            pack_list: response.package_details_list.pack_list,
            no_channel_message: FuncUtils.getValueSafe(response.channel_availability).message,
            no_pack_message: FuncUtils.getValueSafe(response.pack_availability).message,
          };

          dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: channelData });
          dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        }
        else {
          console.log('getDtvAdditionalChannels :: false');
          dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
          if (response.message_type == Constants.MSG_TYPE_MODAL) {
            console.log('MSG_TYPE_MODAL');
            screen.showNoticeAlert(MessageUtils.getFormatedErrorMessage(response));
          } else {
            console.log('MSG_TYPE_ALERT');
            screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
          }
          console.log('Redux :: getDtvAdditionalChannels :: failureCb', response.error);
          dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: null });
        }

      };

      const failureCb = function (response) {
        console.log('Redux :: getDtvAdditionalChannels :: failureCb', response.error);
        dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: null });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));

      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      console.log('Redux :: getDtvAdditionalChannels EXCEPTION : ', e);
      dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: null });
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage());
    }
  };
};

//DTV_SET_CHANNEL_GENRE
export const dtvSetChannelGenre = (data) => {
  return (dispatch) => {
    dispatch({ type: DTV_SET_CHANNEL_GENRE, payLoad: data });
  };
};

//DTV_SET_CHANNEL_TYPE
export const dtvSetChannelType = (data) => {
  return (dispatch) => {
    dispatch({ type: DTV_SET_CHANNEL_TYPE, payLoad: data });
  };
};


//DTV_SELECT_ADDITIONAL_CHANNEL
export const selectAdditionalChannel = (channelDataOb, channelID, selectedChannelAndPacks) => {
  console.log('Redux :: selectAdditionalChannel :: channelDataOb', channelDataOb);
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'select_additional_channel',
    action_type: 'chek_box_selection',
    additional_details: 'DTV select addtion channel',
  });

  return (dispatch) => {
    let selectedChannelsArray = [];
    let channelData = channelDataOb;

    let selectedChannels = _.forEach(channelData.channel_list, function (item) {
      _.forEach(item.channels, function (channelObj) {
        channelObj.is_selected = (channelObj.code == channelID) ? !channelObj.is_selected : channelObj.is_selected;
        if (channelObj.is_selected) {
          selectedChannelsArray.push(channelObj);
        }
      });
    });

    channelData.channel_list = selectedChannels;

    selectedChannelAndPacks.channels = _.uniqBy(selectedChannelsArray, 'code');
    console.log('$$ SELECT_CHANNEL - channel_list\n', channelData.channel_list);
    dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: channelData });
    dispatch(dtvSetSelectedChannelsAndPacks(selectedChannelAndPacks));

  };
};

//DTV_SELECT_ADDITIONAL_CHANNEL
export const selectAdditionalPack = (channelDataOB, packID, selectedChannelAndPacksOb, screen) => {
  console.log('Redux :: selectAdditionalPack :: channelDataOB ', channelDataOB);
  console.log('Redux :: selectAdditionalPack :: selectedChannelAndPacksOb ', selectedChannelAndPacksOb);
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'select_additional_pack',
    action_type: 'chek_box_selection',
    additional_details: 'DTV select addtion pack',
  });

  let channelData = _.cloneDeep(channelDataOB);
  let selectedChannelAndPacks = _.cloneDeep(selectedChannelAndPacksOb);

  return (dispatch) => {
    let selected_pack_list = _.find(channelData.pack_list, { 'pack_code': packID });
    let selected_channels_in_pack_list = _.cloneDeep(selected_pack_list.channels);
    let common_channels = _.intersectionBy(selectedChannelAndPacks.channels, selected_channels_in_pack_list, 'code');

    console.log('$$ SELECT_ADDITIONAL_PACK - selectedChannelAndPacksOb\n', selectedChannelAndPacksOb);
    console.log('$$ SELECT_ADDITIONAL_PACK - selected_pack_list\n', selected_pack_list);
    console.log('$$ SELECT_ADDITIONAL_PACK - selected_channel_list\n', selectedChannelAndPacks.channels);
    console.log('$$ SELECT_ADDITIONAL_PACK - selected_channels_in_pack_list\n', selected_channels_in_pack_list);
    console.log('$$ SELECT_ADDITIONAL_PACK - common_channels\n', common_channels);

    if (FuncUtils.getArraySize(common_channels) > 0) {
      screen.showNoticesAlert(selected_pack_list.pack_name, selected_pack_list.pack_code, common_channels);
    } else {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
      dispatch(updateAdditionalPackSelection(channelData, packID, selectedChannelAndPacks));
    }
  };
};

//DTV_SELECT_ADDITIONAL_CHANNEL
export const updateAdditionalPackSelection = (channelDataOb, packID, selectedChannelAndPacks) => {
  console.log('Redux :: updateAdditionalPackSelection :: channelDataOb ADCH', channelDataOb);
  console.log('$$ updateAdditionalPackSelection :: selectedChannelAndPacks - Old', selectedChannelAndPacks);
  return (dispatch) => {
    let channelData = _.cloneDeep(channelDataOb);
    let selectedChannelsArray = [];
    let selectedChannelsInPacksArray = [];
    let selected_channel_n_packs = {
      channels: [],
      channels_in_pack: [],
      packs: []
    };

    let selectedChannelList = [];
    let previousSelectedPackList = [];
    let currentSelectedPackList = [];
    let selectedChannelListInPack = [];

    let updatedPackList = _.forEach(channelData.pack_list, function (packItemObj) {
      //Add previous selected pack list to array
      if (packItemObj.is_selected) {
        previousSelectedPackList.push(packItemObj);
      }
      if (packItemObj.pack_code == packID) {
        packItemObj.is_selected = !packItemObj.is_selected;
      }
      //Add current selected pack list to array
      if (packItemObj.is_selected) {
        currentSelectedPackList.push(packItemObj);
      }
    });

    channelData.pack_list = updatedPackList;

    currentSelectedPackList = _.uniqBy(currentSelectedPackList, 'pack_code');
    previousSelectedPackList = _.uniqBy(previousSelectedPackList, 'pack_code');

    console.log('$$ PREVIOUS_SELECTED_PAK_LIST : ', previousSelectedPackList);
    console.log('$$ CURRENT_SELECTED_PAK_LIST : ', currentSelectedPackList);
    console.log('$$ CH_PACK - CURRENT_SELECTED_PAK_LIST\n', currentSelectedPackList); //TODO
    console.log('$$ CH_PACK - PREVIOUS_SELECTED_PAK_LIST\n', previousSelectedPackList); //TODO

    selected_channel_n_packs.packs = currentSelectedPackList;

    _.forEach(currentSelectedPackList, function (packItem) {
      console.log('$$ L1 SELECTED_PACKS - packItem : ', packItem);
      _.forEach(packItem.channels, function (channelOb_in_pack) {
        console.log('$$ L2 SELECTED_PACKS => CHANNEL : ', channelOb_in_pack);
        selectedChannelsInPacksArray.push(channelOb_in_pack);
      });
    });


    console.log('$$ CH_PACK - selectedChannelsInPacksArray\n', selectedChannelsInPacksArray); //TODO
    selectedChannelListInPack = _.uniqBy(selectedChannelsInPacksArray, 'code');
    console.log('$$ SELECTED_CHANNELS_IN_PACKS : ', selectedChannelListInPack);
    console.log('$$ CH_PACK - selectedChannelListInPack\n', selectedChannelListInPack); //TODO
    selected_channel_n_packs.channels_in_pack = selectedChannelListInPack;

    console.log('$$ ALL_CHANNEL_LIST - BEFORE: ', channelData.channel_list);

    let modifiedChannelList = _.forEach(channelData.channel_list, function (genreItem) {
      console.log('$$ L0 CHANNEL - genre_code : ', genreItem.genre_code);
      console.log('$$ SELECT_ADDITIONAL_PACK - genreItem.channels\n', genreItem.channels);//TODO
      console.log('$$ CH_PACK - genreItem.channels\n', genreItem.channels);//TODO
      console.log('$$ CH_PACK L1 - genreItem.channels : => ',genreItem.channels);
      _.forEach(genreItem.channels, function (channelObj) {
        let channel_code = channelObj.code;
        let already_in_pack_previous = channelObj.already_in_pack;
        channelObj.already_in_pack = false;
        console.log('$$ SELECT_ADDITIONAL_PACK - channelObj\n', channelObj);
        console.log('$$ CH_PACK - channelObj\n', channelObj);
        console.log('$$ CH_PACK L2 - channelObj : => ',channelObj);
        console.log('$$ CH_PACK L2 - selectedChannelListInPack : => ',selectedChannelListInPack);
        console.log(`$$ L1 CHANNEL - channel_code : ${channel_code} , already_in_pack_previous : ${already_in_pack_previous}`);
        _.forEach(selectedChannelListInPack, function (channelObjInPack) {
          console.log(`$$ L3 CHANNEL_IN_PACK - channel_code : ${channel_code} , already_in_pack_previous : ${already_in_pack_previous}`);
          console.log('$$ CH_PACK L3 - channelObjInPack : => ',channelObjInPack);
          if (channelObjInPack.code == channel_code) {
            console.log(`$$ L3 CHANNEL_IN_PACK :: TRUE - channel_code : ${channel_code} , already_in_pack_previous : ${already_in_pack_previous}`);
            channelObj.already_in_pack = true;
            channelObj.is_selected = false;
          } else {
            console.log(`$$ L3 CHANNEL_IN_PACK :: FALSE : ${channel_code}`);
          }
        });
        if (channelObj.is_selected) {
          console.log(`$$ ** L1 SELECTED_CHANNEL :: channel_code - is_selected ${channel_code}`);
          selectedChannelsArray.push(channelObj);
        } else {
          console.log(`$$ ** L1 SELECTED_CHANNEL :: channel_code -not_selected ${channel_code}`);
        }
      });
    });

    console.log('$$ ALL_CHANNEL_LIST  - AFTER : ', modifiedChannelList);
    channelData.channel_list = modifiedChannelList;
    console.log('$$ SELECT_ADDITIONAL_PACK1 - selectedChannelsArray\n', selectedChannelsArray);//TODO
    selectedChannelList = _.uniqBy(selectedChannelsArray, 'code');
    console.log('$$ SELECTED_CHANNEL_LIST - before filter : ', selectedChannelList);
    console.log('$$ SELECT_ADDITIONAL_PACK - selectedChannelList\n', selectedChannelList);//TODO
    console.log('$$ CH_PACK - selectedChannelList\n', selectedChannelList);//TODO
    selectedChannelList = _.filter(selectedChannelList, { 'already_in_pack': false });
    console.log('$$ SELECT_ADDITIONAL_PACK - selectedChannelList-filter\n', selectedChannelList);//TODO
    console.log('$$ SELECTED_CHANNEL_LIST : ', selectedChannelList);
    selected_channel_n_packs.channels = selectedChannelList;

    dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: channelData });
    dispatch(dtvSetSelectedChannelsAndPacks(selected_channel_n_packs));//TODO
    dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
  };
};


//DTV_SELECTED_CHANNELS_N_PACKS
export const dtvSetSelectedChannelsAndPacks = (data) => {
  console.log('Redux :: dtvSetSelectedChannelsAndPacks ADCH', data);
  return (dispatch) => {
    dispatch({ type: DTV_SELECTED_CHANNELS_N_PACKS, payLoad: data });
  };
};

export const dtvSetChannelsAndPacksPreviousValues = (channels, pack_n_channels) => {
  console.log('Redux :: dtvSetChannelsAndPacksPreviousValues ADCH', channels, pack_n_channels);
  return (dispatch) => {
    dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: channels });
    dispatch(dtvSetSelectedChannelsAndPacks(pack_n_channels));
  };
};

export const clearChannelAndPacksData = () => {
  console.log('Redux :: clearChannelAndPacksData ADCH');
  let additional_channel_list_data = {
    package_name: '',
    package_code: '',
    package_rental: '',
    genre_name_list: [],
    genre_code_list: [],
    channel_list: [],
    pack_list: [],
  };

  let selected_channel_n_packs = {
    channels: [],
    channels_in_pack: [],
    packs: []
  };

  return (dispatch) => {
    dispatch({ type: DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA, payLoad: additional_channel_list_data });
    dispatch(dtvSetSelectedChannelsAndPacks(selected_channel_n_packs));
  };

};

/**
 * This function get available fullfillment Type
 * 
 */
export const getDtvFullFillmentType = (selectedData, screen, modalType) => {
  console.log('getDtvFullFillmentType API ::', modalType);
  console.log(selectedData);
  // const test = selectedData.selectedFullFillmentType;
  //  console.log('Filter_selectedPackageType', selectedData.selectedValue ==  "" ? '' :_.filter(selectedData.selectedFullFillmentType, { 'code': selectedData.selectedFullFillmentType.code })[0].display_name);
  // console.log('Filter_selectedPackageTValue', _.filter(test, { true : test.is_selected }));
  return async (dispatch) => {

    let FullFillmentTypeArrayWithDefaultCode = _.filter(selectedData.selectedFullFillmentType, { 'code': selectedData.default_fullfil_status.code });
    let FullFillmentTypeArrayWithDefaultDisplayName = _.filter(selectedData.selectedFullFillmentType, { 'display_name': selectedData.default_fullfil_status.display_name });

    const fullfillmentselectedData = {
      dropDownData: _.toArray(_.mapValues(selectedData.selectedFullFillmentType, 'display_name')),
      defaultIndex: FuncUtils.getArraySize(FullFillmentTypeArrayWithDefaultCode) !== 0 ? _.findIndex(selectedData.selectedFullFillmentType, ['code', selectedData.default_fullfil_status.code]) : -1,
      package_code: FuncUtils.getArraySize(FullFillmentTypeArrayWithDefaultCode) !== 0 ? FullFillmentTypeArrayWithDefaultCode[0].code : '',
      selectedFullFillmentType: selectedData.selectedFullFillmentType,
      default_fullfil_status: selectedData.default_fullfil_status,
      selectedValue: FuncUtils.getArraySize(FullFillmentTypeArrayWithDefaultDisplayName) !== 0 ? FullFillmentTypeArrayWithDefaultDisplayName[0].display_name : '',
    };

    console.log('getDtvFullFillmentType selectedData ::', fullfillmentselectedData);
    dispatch({ type: DTV_FULLFILLMENT_TYPE, payLoad: fullfillmentselectedData });
    dispatch({ type: DTV_SET_API_LOADING, payLoad: false });

    if (selectedData.default_fullfil_status.code == Constants.CASH_AND_DELIVERY) {
      let connection_type = null;
    
      if (selectedData.packageList.length == 1){
        connection_type = selectedData.conn_type;
      } else {
        connection_type = screen.props.selected_connection_type;
      }
      let oafParams = {
        lob: Constants.LOB_DTV,
        conn_type: connection_type
      };
      dispatch(dtvOafSerialValidation(oafParams, screen, modalType));
    }

  };
};


/**
 * This function get available packtypes
 * 
 */
export const getDtvPackType = (packTypeParams) => {
  console.log('getDtvPackType API ::', packTypeParams);
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_pack_types',
    action_type: 'api_request',
    additional_details: 'DTV get Dtv pack types',
  });

  return async (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    try {
      await Utill.apiRequestPost2('getPackType', 'dtv', 'ccapp', packTypeParams, (res) => {
        console.log("Success Data Obj getDtvPackType:", JSON.stringify(res.data));
        const selectedData = {
          dropDownData: _.toArray(_.mapValues(res.data.data, 'pack_type')),
          selectedValue: '',
          defaultIndex: res.data.data.defaultIndex,
          packageType: res.data.data
        };

        dispatch(setPackType(selectedData));
        console.log("selectedData getDtvPackType:", selectedData);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      }, (errorResponse) => {
        console.log("getDtvPackType Error Data Obj:", JSON.stringify(errorResponse));
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(errorResponse.data));

      }, (errorEx) => {
        console.log("getDtvPackType Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      });
    } catch (e) {
      console.log('Err: ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    }
  };
};

export const resetDtvPackType = () => {
  return (dispatch) => {
    dispatch({ type: DTV_PACK_TYPE, payLoad: '', reset: true });
  };
};

//DTV_SAME_DAY_INSTALATION_DATA
export const setSameDayInstallationData = (data) => {
  let installationData = data;
  if (_.isNil(data)) {
    installationData = {
      enabled: false,
      cut_off_time: '',
      price: 0,
    };
  }
  return { type: DTV_SAME_DAY_INSTALLATION_DATA, payLoad: installationData };
};


//DTV set total payment
export const setSelectedChannelTotalPayment = (input) => {
  return { type: DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE, payLoad: input };
};

/**
 *  This function will load offer data list from backend
 *  API 77
 */
export const dtvLoadOfferList = (requestParams, screen) => {
  console.log('xxxx dtvLoadOfferList API');
  console.log('xxx dtvLoadOfferList :: requestParams ', requestParams);
  let url = {
    action: 'getOffers',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_offer_list',
    action_type: 'api_request',
    additional_details: 'DTV get Dtv offer list',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    // dispatch(dtvResetVoucerValue());
    const successCb = function (response) {
      console.log('xxx dtvLoadOfferList :: successCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      if (response.success) {
        console.log('LOAD_OFFER_DATA_SUCCESS');
        screen.showOfferSelectionModal(response);
      }
      else {
        console.log('xxx dtvLoadOfferList :: NEGATIVE');
        if (response.message_type == Constants.MSG_TYPE_NA) {
          console.log('MSG_TYPE_NA');
          console.log('LOAD_OFFER_DATA_FAIL');
        } else {
          console.log('MSG_TYPE_ALERT');
          console.log('LOAD_OFFER_DATA_FAIL');
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
        }
      }
    };

    const failureCb = function (response) {
      console.log('xxx dtvLoadOfferList :: failureCb ', response);
      console.log('LOAD_OFFER_DATA_FAIL');
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

//DTV_GET_SELECTED_OFFER
export const dtvGetSelectedOffer = (data) => {
  return (dispatch) => {
    dispatch({ type: DTV_GET_OFFERS, payLoad: data });
  };
};

//DTV_RESET_OFFER_FIELD
export const dtvResetOffers = () => {
  return (dispatch) => {
    dispatch({ type: DTV_RESET_OFFERS, payLoad: null });
    dispatch({ type: DTV_SET_SHOW_VOUCHER_CODE_FIELD, payLoad: false });
  };
};

/**
 *  This function will call get offer details API and multiplay soft reserve
 */
export const dtvGetOfferDetails = (requestParams, screen, successCallback) => {
  console.log('xxxx dtvGetOfferDetails API');
  console.log('xxx dtvGetOfferDetails :: requestParams ', requestParams, screen);

  let url = {
    action: 'getOfferDetails',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'get_dtv_offer_details',
    action_type: 'api_request',
    additional_details: 'DTV get Dtv offer details',
  });

  return (dispatch) => {
    try {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
      const successCb = function (response) {
        console.log('xxx dtvGetOfferDetails :: successCb ', response);
        if (response.success) {
          console.log('OFFER_DETAILS_N_MULTI_PLAY_SOFT_RESERVE_SUCCESS');
          console.log("xxx dtvGetOfferDetails response: ", JSON.stringify(response));
          let { voucher_code_enabled = false } = response;
          let extended_warranty_packs = response.extended_warranty_packs;
          let same_day_installation_data = response.same_day_installation;

          successCallback(response);

          console.log('xxx dtvGetOfferDetails :: data ', response);
          console.log('xxx dtvGetOfferDetails :: extended_warranty_packs ', extended_warranty_packs);
          console.log('xxx dtvGetOfferDetails :: same_day_installation_data ', same_day_installation_data);
          dispatch({ type: DTV_SET_SHOW_VOUCHER_CODE_FIELD, payLoad: voucher_code_enabled });

          let { lte_unit_eligibility = false, default_stb_type = '' } = response.stb_data;

          let stbData = {
            stb_upgrade_available: response.stb_data.stb_upgrade_availability,
            stb_price: response.stb_data.stb_price,
            stb_upgrade_text: response.stb_data.stb_upgrade_text,
            additional_data: null,
            stb_type: response.stb_data.stb_type,
            free_visits_count: response.free_visits_count,
            rcu_warranty: response.rcu_warranty,
            pcu_warranty: response.pcu_warranty,
            stb_warranty: response.stb_warranty,
            lnb_warranty: response.lnb_warranty,
            lte_unit_eligibility: lte_unit_eligibility,
            default_stb_type: default_stb_type,
            multiplay_reservation: response.multiplay_reservation,
          };
          let warrantyData = {
            dropDownData: _.toArray(_.mapValues(extended_warranty_packs, 'pack_name')),
            selectedValue: '',
            packCode: '',
            packPrice: 0,
            time_period: ''
          };

          console.log('xxx dtvGetOfferDetails :: stb_data ', stbData);
          console.log('xxx dtvGetOfferDetails :: warrantyData ', warrantyData);
          console.log('xxx dtvGetOfferDetails :: same_day_installation_data ', same_day_installation_data);

          dispatch(dtvSetWarrantyExtensionsData(warrantyData, extended_warranty_packs));
          dispatch({ type: DTV_SET_STB_UPGRADE_DATA, payLoad: stbData });

          if ( screen.props.selected_connection_type == Constants.POSTPAID) {
            dispatch({ type: DTV_DEPOSIT_AMOUNT, payLoad: response.deposit_amount });
            console.log('xxx DTV_DEPOSIT_AMOUNT', response.deposit_amount);
          }
          
          dispatch(setSameDayInstallationData(same_day_installation_data));
          Screen.dismissModal();//Dismiss Offer selection modal

        }
        else {
          console.log('xxx dtvGetOfferDetails :: NEGATIVE');
          if (response.message_type == Constants.MSG_TYPE_NA) {
            console.log('MSG_TYPE_NA');
            console.log('OFFER_DETAILS_N_MULTI_PLAY_SOFT_RESERVE_FAIL');
          } else {
            console.log('MSG_TYPE_ALERT');
            console.log('OFFER_DETAILS_N_MULTI_PLAY_SOFT_RESERVE_FAIL');
            Screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
          }
        }
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      };

      const failureCb = function (response) {
        console.log('xxx dtvGetOfferDetails :: failureCb ', response);
        console.log('OFFER_DETAILS_N_MULTI_PLAY_SOFT_RESERVE_FAIL');
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        Screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));
      };

      let handleFalseResponse = true;
      Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    } catch (e) {
      console.log('xxx dtvGetOfferDetails :: EXCEPTION ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      Screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage());
    }
  };
};



//DTV_STB_UPGRADE_CHECKBOX
export const dtvCheckStbUpgrade = (input) => {
  return (dispatch) => {
    dispatch({ type: DTV_STB_UPGRADE_CHECKBOX, payLoad: input });
  };
};

//DTV_RESET_DEPOSIT_AMOUNT
export const dtvResetDepositAmount = (input) => {
  return (dispatch) => {
    dispatch({ type: DTV_RESET_DEPOSIT_AMOUNT, payLoad: input });
  };
};

//Set Warranty Extensions Data
export const dtvSetWarrantyExtensionsData = (data, extended_warranty_packs) => {
  console.log('REDUX :: dtvSetWarrantyExtensionsData :: ', data);
  return (dispatch) => {
    dispatch({ type: DTV_SET_WARRANTY_EXTENSION_DATA, payLoad: data, apiData: extended_warranty_packs });
  };
};


//Reset STB data
export const dtvResetStbUpgradeData = () => {
  console.log('REDUX :: dtvResetStbUpgradeData');
  return (dispatch) => {
    dispatch({ type: DTV_STB_UPGRADE_CHECKBOX, payLoad: false });
    dispatch({
      type: DTV_SET_STB_UPGRADE_DATA, payLoad: {
        stb_upgrade_available: false,
        stb_price: 0,
        stb_upgrade_text: 'Upgrade to hybrid box',
        additional_data: null,
        stb_type: '',
        free_visits_count: '',
        rcu_warranty: '',
        pcu_warranty: '',
        stb_warranty: '',
        lnb_warranty: ''
      },
    });
    dispatch(dtvSetWarrantyExtensionsData({
      dropDownData: [],
      selectedValue: '',
      defaultIndex: -1,
      packCode: '',
      packPrice: 0,
      time_period: ''
    }, {}));
  };

};


export const dvtSetValidationError = (payload) => {
  return (dispatch) => {
    dispatch({ type: DTV_SET_FIRST_RELOAD_VALIDATION_ERROR, payLoad: payload });
  };
};


//DTV_SET_CUSTOMER_INFO_ID_TYPE
export const dtvSetCustomerInfoSectionIdType = (infoIdType) => {
  let code;
  console.log('DTV_SET_CUSTOMER_INFO_ID_TYPE - ', infoIdType);
  switch (infoIdType) {
    case Constants.CX_INFO_TYPE_NIC:
      code = Constants.ID_TYPE_NIC;
      break;
    case Constants.CX_INFO_TYPE_PASSPORT:
      code = Constants.ID_TYPE_PP;
      break;
    case Constants.CX_INFO_TYPE_DRIVING_LICENCE:
      code = Constants.ID_TYPE_DL;
      break;
    default:
      code = Constants.ID_TYPE_NIC;
      infoIdType = Constants.CX_INFO_TYPE_NIC;
      console.log('DTV_SET_CUSTOMER_INFO_ID_TYPE - default');
      break;
  }
  return { type: DTV_SET_CUSTOMER_INFO_ID_TYPE, payLoad: { infoIdType, code } };
};

//KYC related
export const dtvSetKycCheckBoxField = (checkBoxType = 'RESET', status = false) => {
  console.log('Redux :: dtvSetKycCheckBoxField - ', checkBoxType);
  if (checkBoxType != 'RESET') {
    Analytics.logFirebaseEvent('action_progress', {
      action_name: 'select_kyc_check_box',
      action_type: 'check_box_select',
      additional_details: 'Click KYC checkbox : type -' + checkBoxType,
    });
  }
  return (dispatch) => {
    switch (checkBoxType) {
      case Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING:
        console.log('DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT');
        if (!status) {
          console.log('DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT - UNCHECK');
          dispatch(commonSetKycImage({
            captureType: Constants.IMAGE_CAPTURE_TYPES.PROOF_OF_BILLING,
            imageUri: null
          }));
        }
        dispatch({
          type: DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT,
          payLoad: status
        });
        break;
      case Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE:
        console.log('DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT');
        if (!status) {
          console.log('DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT - UNCHECK');
          dispatch(commonSetKycImage({
            captureType: Constants.IMAGE_CAPTURE_TYPES.CUSTOMER_IMAGE,
            imageUri: null
          }));
        }
        dispatch({
          type: DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
          payLoad: status
        });
        break;
      case Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB:
        console.log('DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT');
        if (!status) {
          console.log('DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT - UNCHECK');
          dispatch(commonSetKycImage({
            captureType: Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB,
            imageUri: null
          }));
        }
        dispatch({
          type: DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT,
          payLoad: status
        });
        break;
      case Constants.SAME_DAY_INSTALLATION_TYPE:
        console.log('DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION');
        if (status) {
          console.log('DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION - CHECK');
          dispatch({
            type: DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT,
            payLoad: false
          });

          dispatch(commonSetKycImage({
            captureType: Constants.IMAGE_CAPTURE_TYPES.ADDITIONAL_POB,
            imageUri: null
          }));

        } else {
          console.log('DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION - UNCHECK');
          dispatch(dtvResetSameDayInstallationInputData());
        }
        dispatch({
          type: DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION,
          payLoad: status
        });
        break;
      default:
        console.log('DTV_RESET_KYC_CHECK_BOX_ALL');
        dispatch({
          type: DTV_RESET_KYC_CHECK_BOX_ALL,
          payLoad: null
        });
        break;
    }
  };
};

//LTE_SET_INPUT_MOBILE_NUMBER
export const dtvSetInputMobileNumber = (input) => {
  return { type: DTV_SET_INPUT_MOBILE_NUMBER, payLoad: input };
};

/**
 * Set user enter voucher code into redux store
 * @param {String} text 
 */
export const setDTVVoucherCodeText = (text) => {
  return (dispatch) => {
    dispatch({ type: SET_DTV_VOUCHER_CODE_ONLY, payLoad: text });
  };
};

/**
 * This function will if the entered voucher code is valid
 * 
 */
export const checkDTVVoucherCodeValidity = (data = {}, screen) => {
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'dtv_validate_voucher_code',
    action_type: 'api_request',
    additional_details: 'DTV validate voucher codes',
  });
  return async (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true, message: 'Validating' });
    try {
      Utill.apiRequestPost2('validateVoucherCode', 'dtv', 'ccapp', data, async (res) => {
        console.log("xxx checkDTVVoucherCodeValidity xxx");
        console.log("checkDTVVoucherCodeValidity Success Data Obj:", JSON.stringify(res.data));
        dispatch({ type: SET_DTV_VOUCHER_CODE_VALIDITY, payLoad: res.data });
        dispatch({ type: DTV_SET_VOUCHER_CANCELLATION_STATUS, payLoad: false });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      }, (errorResponse) => {
        console.log("checkDTVVoucherCodeValidity Error Data Obj:", JSON.stringify(errorResponse));
        screen.showAlertWithTitle(errorResponse.data.error, errorResponse.data.error_title);
        dispatch({ type: SET_DTV_VOUCHER_CODE_VALIDITY, payLoad: false });
        dispatch({ type: SET_DTV_VOUCHER_CODE, payLoad: '' });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      }, (errorEx) => {
        console.log("checkDTVVoucherCodeValidity Exceptoin Data Obj:", JSON.stringify(errorEx));
        dispatch({ type: SET_DTV_VOUCHER_CODE_VALIDITY, payLoad: false });
        dispatch({ type: SET_DTV_VOUCHER_CODE, payLoad: '' });
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        Utill.showAlert(MessageUtils.getExceptionMessage(errorEx));
      });
    } catch (e) {
      console.log('Err: ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
    }
  };
};

/**
 *  This function will validate serials
 */
export const dtvSerialValidationNumber = (requestParam, newUrl, screen) => {
  console.log('Redux :: dtvSerialValidationNumber API');
  console.log('Redux :: dtvSerialValidationNumber  : requestParam', requestParam);

  let url = {
    action: 'validateMaterialSerial',
    controller: 'dtv',
    module: 'ccapp'
  };
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'dtv_validate_serial',
    action_type: 'api_request',
    additional_details: 'DTV validate vserials',
  });
  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    const successCb = function (response) {
      console.log('Redux :: dtvSerialValidationNumber :: successCb ', response);
      console.log('Redux :: dtvSerialValidationNumber :: additionalProps : ', screen);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      if (response.success) {
        console.log('SERIAL_VALIDATION_SUCCESS', response);
        dispatch({ type: DTV_SERIAL_VALIDATION, payLoad: response.info });
        dispatch({ type: DTV_SERIAL_TEXT_VALUE, payLoad: requestParam.materialSerial });
        Keyboard.dismiss();

        try {
          let additionalProps = screen.props.additionalProps;
          console.log('Redux :: dtvSerialValidationNumber :: additionalProps : ', additionalProps);
          screen.serialValidationSuccessCb(response);
          console.log('SERIAL_VALIDATION_SUCCESS TRY');

          switch (requestParam.serialType) {
            case 'DTV_BUNDLE':
              console.log('Redux :: dtvSerialValidationNumber => bundleSerial');
              dispatch({ type: DTV_SET_SERIAL_TYPE_BUNDLE, payLoad: response.info });
              break;
            case 'DTV_STB':
              console.log('Redux :: dtvSerialValidationNumber => stbPackSerial');
              dispatch({ type: DTV_SET_SERIAL_TYPE_PACK, payLoad: response.info });
              break;
            case 'DTV_ACCESSORIES':
              console.log('Redux :: dtvSerialValidationNumber => accessoriesPackSerial');
              dispatch({ type: DTV_SET_SERIAL_TYPE_ACCESSORIES, payLoad: response.info });
              break;
            default:
              console.log('Redux :: dtvSerialValidationNumber => default');
              break;
          }
        } catch (error) {
          console.log('SERIAL_VALIDATION_SUCCESS CATCH', error);
          dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        }
      }
      else {
        console.log('Redux :: dtvSerialValidationNumber :: NEGATIVE');
        dispatch({ type: DTV_SERIAL_VALIDATION, payLoad: null });
        dispatch({ type: DTV_SERIAL_TEXT_VALUE, payLoad: '' });
        Utill.showAlert(MessageUtils.getFormatedErrorMessage(response));
        try {
          screen.serialValidationFailureCb(response);
          console.log('SERIAL_VALIDATION_FAIL TRY');
        } catch (error) {
          console.log('SERIAL_VALIDATION_FAIL CATCH', error);
        }
      }
    };

    const failureCb = function (response) {
      console.log('Redux :: dtvSerialValidationNumber :: failureCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      dispatch({ type: DTV_SERIAL_VALIDATION, payLoad: null });
      dispatch({ type: DTV_SERIAL_TEXT_VALUE, payLoad: '' });
      Utill.showAlert(MessageUtils.getExceptionMessage(response));
      try {
        screen.serialValidationFailureCb(response);
        console.log('SERIAL_VALIDATION_FAIL_EX TRY');
      } catch (error) {
        console.log('SERIAL_VALIDATION_FAIL_EX CATCH', error);
      }

    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParam, successCb, failureCb, handleFalseResponse);
  };
};



//DTV_SERIAL_TEXT_VALUE
export const dtvSetSerialTextValue = (number) => {
  console.log('xxx dtvSetSerialTextValue');
  return { type: DTV_SERIAL_TEXT_VALUE, payLoad: number };
};

//DTV_SERIAL_VALIDATION
export const dtvResetSerialValidationStatus = () => {
  console.log('xxx dtvResetSerialValidationStatus');
  return { type: DTV_SERIAL_VALIDATION, payLoad: null };
};

//DTV_BUNDLE_SERIAL
export const dtvSetBundleSerial = (number) => {
  console.log('xxx dtvSetBundleSerial');
  return { type: DTV_BUNDLE_SERIAL, payLoad: number };
};

//DTV_SERIAL_PACK
export const dtvSetSerialPack = (number) => {
  console.log('xxx dtvSetSerialPack');
  return { type: DTV_SERIAL_PACK, payLoad: number };
};

//DTV_ACCESSORIES_SERIAL
export const dtvSetAccessoriesSerial = (number) => {
  console.log('xxx dtvSetAccessoriesSerial');
  return { type: DTV_ACCESSORIES_SERIAL, payLoad: number };
};

//DTV_RESET_INDIVIDUAL_SERIAL_DATA
export const dtvResetIndividualSerialFieldValues = (serialType) => {
  console.log('xxx dtvResetIndividualSerialFieldValues :: serialType :', serialType);
  return (dispatch) => {
    dispatch({ type: DTV_RESET_INDIVIDUAL_SERIAL_DATA, payLoad: serialType });
    dispatch({ type: DTV_RESET_SERIALS_DATA, payLoad: null });
  };
};

//DTV OAF SERIAL VALIDATION
export const dtvOafSerialValidation = (requestParams, screen, modalType) => {
  console.log('xxxx dtvOafSerialValdiation API', modalType);
  console.log('xxx dtvOafSerialValdiation :: requestParams ', requestParams);

  let url = {
    action: 'getMaterialAvailability',
    controller: 'dtv',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'dtv_oaf_serial_alidation',
    action_type: 'api_request',
    additional_details: 'DTV Oaf Serial Validation',
  });

  return (dispatch) => {
    dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
    const successCb = function (response) {
      console.log('xxx dtvOafSerialValdiation :: successCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });

      if (response.success) {
        dispatch({ type: DTV_EOAF_SERIAL, payLoad: response });

      } else {
        if (modalType !== "datapackages") {
          screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
          dispatch({ type: DTV_EOAF_SERIAL, payLoad: response });
        }
        console.log('xxx dtvOafSerialValdiation :: Failure ', response);
      }

    };

    const failureCb = function (response) {
      console.log('xxx dtvOafSerialValdiation :: failureCb ', response);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      try {
        Utill.showAlert(MessageUtils.getExceptionMessage(response));
        console.log('DTV_SET_API_LOADING TRY');
      } catch (error) {
        console.log('DTV_SET_API_LOADING CATCH', error);
        Utill.showAlert(MessageUtils.getSystemErrorMessage());
      }
    };

    let handleFalseResponse = true;
    Utill.apiRequestPostGeneral(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
  };
};

//set DTV_EOAF_SERIAL
export const dtvSeteOAFserialValue = (input) => {
  return { type: DTV_EOAF_SERIAL, payLoad: input };
};

//LTE_INPUT_EMAIL
export const dtvSetInputemail = (input) => {
  return { type: DTV_INPUT_EMAIL, payLoad: input };
};

//LTE_INPUT_LANLINENUMBER
export const dtvSetInputLandlineNumber = (input) => {
  return { type: DTV_SET_INPUT_LANLINE_NUMBER, payLoad: input };
};

//DTV_SERIAL_PACK
export const dtvResetEoafSerialPack = (number) => {
  console.log('xxx dtvSetSerialPack');
  return { type: DTV_RESET_EOAF_SERIAL, payLoad: number };
};

//DTV_SET_RELOAD_AMOUNT
export const dtvSetReloadAmound = (number) => {
  return { type: DTV_SET_RELOAD_AMOUNT, payLoad: number };
};

//DTV_RESET_VOUCHER_VALUE
export const dtvResetVoucerValue = () => {
  return { type: DTV_RESET_VOUCHER_VALUE, payLoad: null };
};

//DTV_CUSTOMER_TOTAL_PAYMENT
export const dtvSetCustomerTotalPaymentValue = (number) => {
  console.log('dtvSetCustomerTotalPaymentValue', number);
  return { type: DTV_CUSTOMER_TOTAL_PAYMENT, payLoad: number };
};

//DTV_OTP_NUMBER
export const dtvSetOtpNumber = (number) => {
  console.log('xxx dtvSetOtpNumber');
  return { type: DTV_OTP_NUMBER, payLoad: number };
};

export const dtvResetSectionValidationStatus = () => {
  console.log('REDUX :: dtvResetSectionValidationStatus');

  return (dispatch) => {

    dispatch({ type: DTV_SET_ID_NUMBER_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: DTV_SET_PRODUCT_INFO_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS, payLoad: false });
    dispatch({ type: RESET_KYC_IMAGES, payLoad: null });
    dispatch({ type: DTV_ID_VALIDATION_FAIL, payLoad: null });
    dispatch({ type: DTV_OUTSTANDING_CHECK_FAIL, payLoad: null });
    dispatch({ type: DTV_OTP_VALIDATION_FAIL, payLoad: null });
    dispatch({ type: DTV_SERIAL_VALIDATION, payLoad: null });
    dispatch({ type: DTV_RESET_SERIALS_DATA, payLoad: null });


  };

};

/** 
 * @description Common method to clear customer info section when change
 * 1. Pack type change
 * 2. Fulfilment type
 * 3. Offer change
 * 
 * 
*/
export const dtvResetCustomerInfoSection = () => {
  console.log('REDUX :: dtvResetCustomerInfoSection');
  return (dispatch) => {
    dispatch(commonResetKycImages());
    dispatch(dtvSetKycCheckBoxField('RESET'));
    dispatch(dtvResetSameDayInstallationInputData());
    dispatch(setSameDayInstallationData(null));

  };
};

/** 
 * @description Common method to clear customer info section when change id type
 *
 * 
*/
export const dtvResetCustomerInfoSectionChange = () => {
  console.log('REDUX :: dtvResetCustomerInfoSectionChange');
  return (dispatch) => {
    dispatch(commonResetKycImages());
    dispatch(dtvSetKycCheckBoxField('RESET'));
    dispatch(dtvResetSameDayInstallationInputData());
    //TODO: Add other methods

  };
};

/********************************** Common Actions **********************/

export const voucherCodeCancellationApi = COMMON_ACTION.voucherCodeCancellationApi;
export const commonSetActivityStartTime = COMMON_ACTION.commonSetActivityStartTime;
export const commonSetActivityEndTime = COMMON_ACTION.commonSetActivityEndTime;
export const commonGetBannerImages = COMMON_ACTION.commonGetBannerImages;
export const commonSetBannerImages = COMMON_ACTION.commonSetBannerImages;


/********************************** Authondication Related New redux methods **********************/

export const authGetStartInApi = AUTH_ACTION.authGetStartInApi;
export const setLanguage = AUTH_ACTION.setLanguage;


/********************************** DTV Activation Related New redux methods **********************/

export const dtvForceUpdateView = DTV_ACTION.dtvForceUpdateView;
export const dtvSetCustomerName = DTV_ACTION.dtvSetCustomerName;
export const dtvSetCustomerAddressLine1 = DTV_ACTION.dtvSetCustomerAddressLine1;
export const dtvSetCustomerAddressLine2 = DTV_ACTION.dtvSetCustomerAddressLine2;
export const dtvSetCustomerAddressLine3 = DTV_ACTION.dtvSetCustomerAddressLine3;
export const dtvSetCustomerCity = DTV_ACTION.dtvSetCustomerCity;
export const dtvCitySearch = DTV_ACTION.dtvCitySearch;
export const dtvResetSameDayInstallationInputData = DTV_ACTION.dtvResetSameDayInstallationInputData;
export const dtvSetCustomerCityData = DTV_ACTION.dtvSetCustomerCityData;
export const dtvSetEzCashInputPin = DTV_ACTION.dtvSetEzCashInputPin;
export const dtvEzCashAccountCheck = DTV_ACTION.dtvEzCashAccountCheck;
export const dtvResetEzCashBalance = DTV_ACTION.dtvResetEzCashBalance;
export const dtvFinalActivationApi = DTV_ACTION.dtvFinalActivationApi;
export const resetSameDayInstallationStatus = DTV_ACTION.resetSameDayInstallationStatus;






