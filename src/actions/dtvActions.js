/*
 * File: dtvActions.js
 * Project: Dialog Sales App
 * File Created: Friday, 16th November 2018 2:06:48 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 16th November 2018 2:10:00 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { Alert, Keyboard } from 'react-native';
import Constants from '../config/constants';
import { Analytics, ApiRequestUtils, Screen, FuncUtils, MessageUtils } from '../utills';
import _ from 'lodash';

import {
  DTV_FORCE_UPDATE_VIEW,
  DTV_SET_CUSTOMER_NAME,
  DTV_SET_CUSTOMER_ADDRESS_LINE1,
  DTV_SET_CUSTOMER_ADDRESS_LINE2,
  DTV_SET_CUSTOMER_ADDRESS_LINE3,
  DTV_SET_CUSTOMER_CITY,
  DTV_CITY_SEARCH_DATA,
  DTV_EZ_CASH_BALANCE,
  DTV_RESET_EZCASH_BALANCE,
  DTV_SET_INPUT_EZ_CASH_PIN,
  DTV_RESET_SAMEDAY_INSTALLATION_STATUS,
  DTV_SET_API_LOADING,
  CONFIG_DTV_SET_ACTIVATION_STATUS

} from './types';
import Utills from '../utills/Utills';

import * as ALL_ACTION from './index';


//DTV_FORCE_UPDATE_VIEW
export const dtvForceUpdateView = () => {
  console.log('################ DTV_FORCE_UPDATE_VIEW #######################');
  let randValue = Math.floor(Math.random() * 255);
  return { type: DTV_FORCE_UPDATE_VIEW, payLoad: randValue };
};

//DTV_SET_CUSTOMER_NAME
export const dtvSetCustomerName = (text, error ='') => {
  console.log('Redux :: dtvSetCustomerName');
  return (dispatch) => {
    dispatch({ type: DTV_SET_CUSTOMER_NAME, payLoad: { 
      value : text, 
      error :  error
    } });
  };
};

//DTV_SET_CUSTOMER_ADDRESS_LINE1
export const dtvSetCustomerAddressLine1 = (text, error ='') => {
  console.log('Redux :: dtvSetCustomerAddressLine1');
  return (dispatch) => {
    dispatch({ type: DTV_SET_CUSTOMER_ADDRESS_LINE1, payLoad: { 
      value : text, 
      error :  error
    } });
  };
};

//DTV_SET_CUSTOMER_ADDRESS_LINE2
export const dtvSetCustomerAddressLine2 = (text, error ='') => {
  console.log('Redux :: dtvSetCustomerAddressLine2');
  return (dispatch) => {
    dispatch({ type: DTV_SET_CUSTOMER_ADDRESS_LINE2, payLoad: { 
      value : text, 
      error :  error
    } });
  };
};

//DTV_SET_CUSTOMER_ADDRESS_LINE3
export const dtvSetCustomerAddressLine3 = (text, error ='') => {
  console.log('Redux :: dtvSetCustomerAddressLine3');
  return (dispatch) => {
    dispatch({ type: DTV_SET_CUSTOMER_ADDRESS_LINE3, payLoad: { 
      value : text, 
      error :  error
    } });
  };
};

//DTV_CITY_SEARCH_DATA
export const dtvSetCustomerCityData = (customer_city_data) => {
  console.log('Redux :: dtvSetCustomerCityData - customer_city_data ',  customer_city_data);
  return (dispatch) => {
    dispatch({ type: DTV_CITY_SEARCH_DATA, payLoad: customer_city_data });
  };
};

//DTV_SET_CUSTOMER_CITY
export const dtvSetCustomerCity = (city_data) => {
  console.log('Redux :: dtvSetCustomerCity : ', city_data);
  let selected_city_data = { 
    city_name : '', 
    code : '' 
  };

  if (city_data != null && city_data.city_name !== undefined ) {
    selected_city_data = { 
      city_name : city_data.city_name , 
      code :  city_data.code,      
    };
  }

  let  customer_city_data = {
    all_data: [],
    city_names: [],
    error: ''
  };
 
  console.log('Redux :: dtvSetCustomerCity : selected_city_data ', selected_city_data);
  return (dispatch) => {
    dispatch({ type: DTV_SET_CUSTOMER_CITY, payLoad: { 
      city_name : selected_city_data.city_name , 
      code :  selected_city_data.code, 
    } });
    dispatch(dtvSetCustomerCityData(customer_city_data));
  };
};

export const dtvResetSameDayInstallationInputData = () => {
  console.log('Redux :: dtvResetSameDayInstallationInputData');
  return (dispatch) => {
    dispatch(dtvSetCustomerName(''));
    dispatch(dtvSetCustomerAddressLine1(''));
    dispatch(dtvSetCustomerAddressLine2(''));
    dispatch(dtvSetCustomerAddressLine3(''));
    dispatch(dtvSetCustomerCity({ 
      city_name : '', 
      code : '' 
    }));
  };
};


/**
 * This function will search city from API
 * 
 */
export const dtvCitySearch = (requestParams, screen) => {
  console.log('Redux :: dtvCitySearch :: API - requestParams ', requestParams, screen);

  let url = {
    action: 'getPostalCodes',
    controller: 'common',
    module: 'ccapp'
  };

  
  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'dtv_city_search',
    action_type: 'api_request',
    additional_details: 'DTV city search',
  });

  return (dispatch) => {
    try {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });

      const successCb = function (response) {
        console.log('Redux :: dtvCitySearch :: successCb ', response);

        let  customer_city_data = {
          all_data: response.info,
          city_names: _.map(response.info, 'city_name'),
          error: response.info.length == 0 ? 'No search result(s) found' : ''
        };
        Keyboard.dismiss();
        dispatch(dtvSetCustomerCityData(customer_city_data));
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        if (response.info.length > 0 ) {
          screen.customerCitySearch.show();
        } else {
          console.log('Redux :: dtvCitySearch :: NO_SEARCH_RESULTS');
        }  
      };

      const failureCb = function (response) {
        console.log('Redux :: dtvCitySearch :: failureCb ',  response.error);
        let  customer_city_data = {
          all_data: [],
          city_names: [],
          error: response.error
        };
        Keyboard.dismiss();
        dispatch(dtvSetCustomerCityData(customer_city_data));
        dispatch(dtvSetCustomerCity(null));
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });

      };

      let handleFalseResponse = false;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      console.log('Redux :: dtvCitySearch :: EXCEPTION ', e);
      Keyboard.dismiss();
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      let  customer_city_data = {
        all_data: [],
        city_names: [],
        error: 'System error, please try again'
      };
      dispatch(dtvSetCustomerCityData(customer_city_data));
      dispatch(dtvSetCustomerCity(null));
    }
  };
};

// DTV_RESET_SAMEDAY_INSTALLATION_STATUS
export const resetSameDayInstallationStatus = () => {
  console.log('xxx DTV_RESET_SAMEDAY_INSTALLATION_STATUS');
  return { type: DTV_RESET_SAMEDAY_INSTALLATION_STATUS, payLoad: null };
};

/**
 *  This function will check eZ Cash account details 
 *  API 
 */
export const dtvEzCashAccountCheck = (requestParams, screen) => {
  console.log('Redux :: dtvEzCashAccountCheck API');
  console.log('Redux :: dtvEzCashAccountCheck :: requestParams ', requestParams);

  let url = {
    action: 'checkBalance',
    controller: 'ezCash',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_progress', {
    action_name: 'ez_cash_account_check',
    action_type: 'api_request',
    additional_details: 'DTV eZ cash account check',
  });

  return (dispatch) => {
    try {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
      Keyboard.dismiss();
      const successCb = function (response) {
        console.log('Redux :: dtvEzCashAccountCheck :: successCb ', response);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        if (response.success) {
          console.log('EZ_CASH_ACCOUNT_CHECK_SUCCESS');
          dispatch({ type: DTV_EZ_CASH_BALANCE, payLoad: response, validated: true, balance: response.info.availableAmount, accountNo: response.info.msisdn });     
        }
        else {
          dispatch({ type: DTV_EZ_CASH_BALANCE, payLoad: null, validated: false, balance: 0, account_no: '' });
          console.log('Redux :: dtvEzCashAccountCheck :: NEGATIVE');
          if (response.message_type == Constants.MSG_TYPE_NA) {
            console.log('MSG_TYPE_NA');
            console.log('EZ_CASH_ACCOUNT_CHECK_NO_ERROR');
          } else {
            console.log('MSG_TYPE_ALERT');
            console.log('EZ_CASH_ACCOUNT_CHECK_FAIL');
            screen.showEzCashErrorModal(response);
          }
        }
      };

      const failureCb = function (response) {
        console.log('Redux :: dtvEzCashAccountCheck :: failureCb ', response);
        console.log('EZ_CASH_ACCOUNT_CHECK_ERROR');
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        dispatch({ type: DTV_EZ_CASH_BALANCE, payLoad: null, validated: false, balance: 0, account_no: '' });
        Screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response), screen.onPressOk);
      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      console.log('Redux :: dtvEzCashAccountCheck :: EXCEPTION ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      dispatch({ type: DTV_EZ_CASH_BALANCE, payLoad: null, validated: false, balance: 0, account_no: '' });
      Screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage(), screen.onPressOk);
    }
  };
};

//DTV_SET_EZ_CASH_PIN
export const dtvSetEzCashInputPin = (pin) => {
  console.log('Redux :: dtvSetEzCashInputPin');
  return { type: DTV_SET_INPUT_EZ_CASH_PIN, payLoad: pin };
};


//DTV_RESET_EZCASH_BALANCE
export const dtvResetEzCashBalance = () => {
  console.log('Redux :: dtvResetEzCashBalance');
  return { type: DTV_RESET_EZCASH_BALANCE, payLoad: null };
};

/**
 *  This function will send final activation data to backend 
 *  API 9
 */
export const dtvFinalActivationApi = (requestParams, screen) => {
  console.log('Redux :: dtvFinalActivationApi :: API - requestParams ', requestParams, screen);
  console.log('xxx dtvFinalActivationApi');
  let url = {
    action: 'activate',
    controller: 'ConnectionSale',
    module: 'ccapp'
  };

  Analytics.logFirebaseEvent('action_end', {
    action_name: 'dtv_final_activation',
    action_type: 'api_request',
    additional_details: 'DTV final activation',
  });

  return (dispatch) => {
    try {
      dispatch({ type: DTV_SET_API_LOADING, payLoad: true });
      dispatch(ALL_ACTION.commonSetActivityEndTime());
      const successCb = function (response) {
        console.log('DTV success resp', response);
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        dispatch({ type: CONFIG_DTV_SET_ACTIVATION_STATUS, payLoad: true });

        if (response.success) {
          console.log('FINAL_ACTIVATION_SUCCESS');
          if (!response.serial_available && screen.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY && !response.transaction_id){
            screen.showNoticesAlert(response);
          } else {
            screen.showActivateSuccessModal(response);
          }
         
        }
        else {
          console.log('FINAL_ACTIVATION_FAIL');
          console.log('xxx dtvFinalActivationApi :: Positive');
          console.log('xxx dtvFinalActivationApi :: NEGATIVE');
          dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
          if (response.message_type == Constants.MSG_TYPE_MODAL && screen.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY) {
            console.log('MSG_TYPE_MODAL');
            console.log('FINAL_ACTIVATION_NO_ERROR');
            screen.showModalError(response.description);
          } else if (response.message_type == Constants.MSG_TYPE_ALERT && screen.props.dtv_fullfillment_type.package_code == Constants.CASH_AND_DELIVERY){
            console.log('MSG_TYPE_ALERT');
            console.log('FINAL_ACTIVATION_FAIL');
            screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
          } else {
            screen.showGeneralErrorModal(MessageUtils.getFormatedErrorMessage(response));
          }
        }
      };

      const failureCb = function (response) {
        console.log('Redux :: dtvFinalActivationApi :: failureCb ',  response);  
        dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
        screen.showGeneralErrorModal(MessageUtils.getExceptionMessage(response));

      };

      let handleFalseResponse = true;
      ApiRequestUtils.apiPost(url.action, url.controller, url.module, requestParams, successCb, failureCb, handleFalseResponse);
    }
    catch (e) {
      console.log('Redux :: dtvFinalActivationApi :: EXCEPTION ', e);
      dispatch({ type: DTV_SET_API_LOADING, payLoad: false });
      screen.showGeneralErrorModal(MessageUtils.getSystemErrorMessage());
     
    }
  };
};


