import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  en: {
    nicValidate: 'NIC validation',
    passValidate: 'Passport validation',
    productInfo: 'Product information',
    custoInfo: 'Customer information',
    buttonTxt: 'ACTIVATE',
    nic: 'NIC',
    simNo: 'SIM no',
    mobileNo: 'Mobile no',
    firstRel: 'First reload',
    alternateContact: 'Alternative contact no',
    otherContact: 'Other Contact no',
    lcd: 'Local call deposit (LCD)',
    email: 'Email',
    ezcash: '(0771234567 eZ cash account PIN)',
    ezCashPin: 'eZ cash PIN',
    prepaid: 'Prepaid',
    postpaid: 'Postpaid',
    srilankan: 'Sri Lankan',
    foreigner: 'Foreigner',

    idLabelInptNicEng: 'NIC',
    idLabelInputNic: 'NIC',
    idLabelInputPp: 'Passport',

    captureNicLabelTxt: 'Capture NIC',
    captureDlLabelTxt: 'Capture driving license',
    capturePpLabelTxt: 'Capture passport',
    captureForeignAddress: 'Capture address',

    capturedNicLabelTxt: 'NIC front & back',
    capturedDlLabelTxt: 'Driving license',
    capturedPpLabelTxt: 'Passport',
    capturedForeignAddress: 'Address',

    backMessage: 'Do you want to cancel the activation and go back to home page?',
    notClearNicLabelTxt: 'Customer Face is not clear on NIC',
    notClearDlLabelTxt: 'Customer Face is not clear on driving license',
    notClearPpLabelTxt: 'Customer Face is not clear on passport',

    captureCustomerPhotoSubTxt: 'Capture customer photo',
    captureProofOfDeliverySubTxt: 'Capture proof of billing',
    capturedCustomerPhotoSubTxt: 'Customer photo',
    capturedProofOfDeliverySubTxt: 'Proof of billing',

    pobNicLabelTxt: 'Current Address is different/ NIC address is not clear',
    pobDlLabelTxt: 'Current Address is different/ Driving license address is not clear',
    pobPpLabelTxt: 'Current Address is different/ Passport address is not clear',

    pobNicLabelTxtPost: 'Billing Address is different/ NIC address is not clear',
    pobDlLabelTxtPost: 'Billing Address is different/ Driving license address is not clear',
    pobPpLabelTxtPost: 'Billing Address is different/ Passport address is not clear',

    customerSignature: 'Customer Signature',
    customerSignatureUC: 'CUSTOMER SIGNATURE',
    placeYourSignature: 'Place your signature here',
    bySigining: 'By signing, you agree to our',
    byclicking: 'By clicking, you agree to our',
    enterCustomerSignature: 'Please enter customer signature',
    tns: 'Terms & Conditions',
    tncUc: 'TERMS & CONDITIONS',
    sigReset: 'Reset',
    sigOk: 'Ok',
    scanSim: 'Scan SIM',
    numberSelection: 'Number selection',
    numberRandom: 'Random',
    numberSearch: 'Search',
    numberInclude: 'Include',
    numberStart: 'Starts with',
    numberEnd: 'Ends with',
    numberCancel: 'Cancel',

    enter_valid_amount: 'Please enter a valid amount',
    reload_cant_empty: 'Reload value can\'t be empty!',
    select_first_reload: 'Select first reload',
    continue: 'CONTINUE',
    more_offers: 'More offers',

    reloadAmount: 'Reload amount',
    totalPay: 'Total payment',
    showData: 'Show details',
    localCall: 'Local call deposit',
    conFee: 'Connection Fee',
    packageInfo: 'Package information',
    nicNo: 'NIC no',
    ppNo: 'Passport no',
    type: 'Type',
    connetionType: 'Postpaid',
    connectionNo: 'Connection no',
    offer: 'Offer',
    package: 'Package',
    rental: 'Rental',
    otherContactNo: 'Other contact no',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'Continue to make this payment and get the new connection?',
    confirmText: 'Are you sure you want to cancel?',
    outstandingLeftText: 'Customer has an outstanding of',
    outstandingRightText: 'from disconnected connection(s).',
    //New
    customer_already_with_dialog: 'Customer details are already with Dialog.',
    select_mobile_number: 'Select a mobile number to validate details',
    send_validation: 'SEND VALIDATION',
    skip_validation: 'Skip validation',
    msg_please_select_no: 'Please select a number from list',
    five_pin_number_sent_to: '5 digit PIN number is sent to ',
    enter_pin: 'Enter PIN',
    receive_five_digit_pin: 'Didn\'t receive the 5 digit PIN?',
    recive_five_digit_pin: 'Didn\'t receive the 5 digit PIN?',
    resend_pin: 'RESEND PIN',
    validate_pin: 'VALIDATE',
    otp_validation_success: 'OTP validation successful',
    otp_validation_failed: 'OTP validation failed !',
    otp_resent: 'OTP Resent',
    otp_select_a_number: 'Select a Number',
    outstandingFee: 'Outstanding Fee',
    please_enter_ezcash_code: 'Please Enter the eZ Cash PIN',
    ezcash_pin_must_only_numbers: 'eZ Cash PIN must only contain numbers',
    ezcash_pin_must_contain_4_numbers: 'eZ Cash PIN must contain 4 numbers',
    //TODO
    btnOk: 'OK',
    btnCancel: 'CANCEL',
    btnYes: 'YES',
    btnNo: 'NO',
    confirmButtonTxt: 'CONFIRM',
    cancelButtonTxt: 'CANCEL',
    retryButtonText: 'RETRY',
    continueButtonText: 'CONTINUE',
    modalTitle: 'Confirm & Sign',
    modalTitleOtpVerified: 'Confirm & Activate',
    lcdValue: 'Local call deposit',
    totalPayment: 'Total payment',
    system_error: 'System error',

    activation_success: 'Activation Success',
    invalid_sim_serieal: 'Invalid SIM Serial',
    customer_information_image_not_captured: 'Image not captured in customer information',
    please_enter_nic: 'Please complete the NIC image capture',
    passport_image_not_captured: 'Passport image not captured',
    address_image_not_captured: 'Address image not captured',
    pob_image_not_captured: 'POB Image not captured',
    customer_image_not_captured: 'Customer image not captured',
    invalid_altenate_contact_no: 'Invalid alternate contact number',
    signature_not_captured: 'Customer signature not captured',
    submit_valid_nic: 'Please submit a valid NIC',
    submit_valid_passport: 'Please submit a valid passport',
    ezCashAccountLabel: 'eZ Cash account',
    please_enter_valid_email: 'Please enter valid email',
    tnsUc: 'TERMS & CONDITIONS',
    conn_Type: 'Connection type',
    mobileActivationTitle: 'MOBILE ACTIVATION',

  },
  si: {
    nicValidate: 'ජාතික හැදුනුම්පත් වලංගුතාවය',
    passValidate: 'විදේශ ගමන් බලපත්‍රය වලංගුතාවය',
    productInfo: 'නිෂ්පාදන තොරතුරු',
    custoInfo: 'පාරිභෝගික තොරතුරු',
    buttonTxt: 'සක්‍රීය කරන්න',
    nic: 'ජාතික හැදුනුම්පත් අංකය',
    simNo: 'සිම්පත් අංකය',
    mobileNo: 'දුරකථන අංකය',
    firstRel: 'පළමු රීලෝඩය',
    alternateContact: 'විකල්ප දුරකථන අංකය',
    prepaid: 'පෙරගෙවුම්',
    postpaid: 'පසුගෙවුම්',
    srilankan: 'ශ්‍රී ලාංකික',
    foreigner: 'විදේශික',
    otherContact: 'විකල්ප දුරකථන අංකය',
    lcd: 'දේශීය ඇමතුම් තැන්පතු',
    email: 'ඊ-මේල්',
    ezcash: '(0771234567 eZ Cash කේතය)',
    ezCashPin: 'eZ Cash කේතය',

    idLabelInptNicEng: 'NIC',
    idLabelInputNic: 'ජාතික හැදුනුම්පත් අංකය',
    idLabelInputPp: 'පාස්පෝර්ට්',

    captureNicLabelTxt: 'NIC ඡායාරූපය',
    captureDlLabelTxt: 'රියදුරු බලපත්‍රය ඡායාරූපය',
    capturePpLabelTxt: 'පාස්පෝර්ට් ඡායාරූපය',
    captureForeignAddress: 'ලිපිනය ඡායාරූපය',

    capturedNicLabelTxt: 'NIC ඉදිරිපස සහ පසුපස',
    capturedDlLabelTxt: 'රියදුරු බලපත්‍රය',
    capturedPpLabelTxt: 'පාස්පෝර්ට්',
    capturedForeignAddress: 'ලිපිනය',

    backMessage: 'සක්‍රීය කිරිම අවලංගු කර නැවත මුල් පිටුව වෙත ?',
    notClearNicLabelTxt: 'පාරිභෝගිකයාගේ මුහුණේ ඡායාරූපය පැහැදිලි නැත',
    notClearDlLabelTxt: 'පාරිභෝගිකයාගේ මුහුණේ ඡායාරූපය පැහැදිලි නැත',
    notClearPpLabelTxt: 'පාරිභෝගිකයාගේ මුහුණේ ඡායාරූපය පැහැදිලි නැත',

    captureCustomerPhotoSubTxt: 'පාරිභෝගිකයාගේ ඡායාරූපය ලබාගන්න',
    captureProofOfDeliverySubTxt: 'බිල් ගෙවීම් පිටපත ලබාගන්න',
    capturedCustomerPhotoSubTxt: 'පාරිභෝගිකයාගේ ඡායාරූපය',
    capturedProofOfDeliverySubTxt: 'බිල් ගෙවීම් පිටපත',

    pobNicLabelTxt: 'වර්තමාන ලිපිනය වෙනස්/ NIC ලිපිනය පැහැදිලි නැත',
    pobDlLabelTxt: 'වර්තමාන ලිපිනය වෙනස්/ රියදුරු බලපත්‍ර ලිපිනය පැහැදිලි නැත',
    pobPpLabelTxt: 'වර්තමාන ලිපිනය වෙනස්/ පාස්පෝර්ට් ලිපිනය පැහැදිලි නැත',

    pobNicLabelTxtPost: 'බිල්ගත ලිපිනය වෙනස්/ NIC ලිපිනය පැහැදිලි නැත',
    pobDlLabelTxtPost: 'බිල්ගත ලිපිනය වෙනස්/ රියදුරු බලපත්‍ර ලිපිනය පැහැදිලි නැත',
    pobPpLabelTxtPost: 'බිල්ගත ලිපිනය වෙනස්/ පාස්පෝර්ට් ලිපිනය පැහැදිලි නැත',

    customerSignature: 'පාරිභෝගික අත්සන',
    customerSignatureUC: 'පාරිභෝගික අත්සන',
    placeYourSignature: 'පාරිභෝගික අත්සන මෙහි යොදන්න',
    bySigining: 'අත්සන් කිරීමෙන් ඔබ අපගේ',
    byclicking: 'මෙය සනාථ කිරීමෙන් ඔබ අපගේ ',
    enterCustomerSignature: 'කරුණාකර පාරිභෝගික අත්සන ඇතුලත් කරන්න',
    tns: 'වගන්ති හා කොන්දේසි',
    tnsUc: 'වගන්ති හා කොන්දේසි',
    sigReset: 'Reset',
    sigOk: 'Ok',
    scanSim: 'සිම්පත ස්කෑන් කරන්න',
    numberSelection: 'සම්බන්ධතා අංකය තෝරාගන්න',
    numberRandom: 'අහඹු තෝරාගැනීමක්',
    numberSearch: 'සෙවුම',
    numberInclude: 'අන්තර්ගත',
    numberStart: 'ආරම්භක අංක',
    numberEnd: 'අවසාන අංක',
    numberCancel: 'අවලංගු කරන්න',

    reloadAmount: 'රීලෝඩය වටිනාකම',
    totalPay: 'මුළු ගෙවීම්',
    showData: 'විස්තර',
    localCall: 'දේශීය ඇමතුම් තැන්පතු',
    conFee: 'සම්බන්ධතා ගාස්තු',
    packageInfo: 'පැකේජයේ විස්තර',
    nicNo: 'NIC අංකය',
    ppNo: 'පාස්පෝර්ට් අංකය',
    type: 'වර්ගය',
    connetionType: 'පසුගෙවුම්',
    connectionNo: 'සම්බන්ධතා අංකය',
    offer: 'දීමනා',
    package: 'පැකේජය',
    rental: 'මාසික ගාස්තුව',
    otherContactNo: 'විකල්ප දුරකථන අංකය',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'ගෙවීම සිදු කර නව සම්බන්ධතාවය ලබා ගන්න',
    confirmText: 'අවලංගු කිරිම තහවුරු කරන්න',
    outstandingLeftText: 'පාරිබෝගිකයාගේ ගෙවිය යුතු හිග මුදල',
    outstandingRightText: 'විසන්ධි වූ සම්බන්ධතාවයන්',
    //New
    customer_already_with_dialog: 'පාරිභෝගික තොරතුරු ඩයලොග් සතුව පවතී',
    select_mobile_number: 'පාරිභෝගික වලංගුතාවය මැන බැලීම සදහා සම්බන්ධිත අංකයක් තෝරා ගන්න',
    send_validation: 'වලංගුකරණය',
    skip_validation: 'වලංගුකරණය මඟහැරීම',
    msg_please_select_no: 'කරුණාකර ලැයිස්තුවෙන් අංකයක් තෝරන්න..!',
    five_pin_number_sent_to: '5 අංකිත කේත අංකය යවනු ලැබේ',
    enter_pin: 'කේතය ඇතුළත් කරන්න',
    recive_five_digit_pin: '5 අංකිත කේත අංකය නොලැබුණිද ?',
    receive_five_digit_pin: '5 අංකිත කේත අංකය නොලැබුණිද ?',
    resend_pin: 'කේතය නැවත යැවිම',
    validate_pin: 'කේත වලංගුකරණය',
    otp_validation_success: 'OTP වලංගුකරණය සාර්ථකයි !',
    otp_validation_failed: 'OTP වලංගුකරණය අසාර්ථකයි !',
    otp_resent: 'OTP නැවත යැවිම',
    otp_select_a_number: 'අංකයක් තෝරන්න',
    outstandingFee: 'හිග මුදල',
    please_enter_ezcash_code: 'කරුණාකර eZ Cash කේතය ඇතුලත් කරන්න',
    ezcash_pin_must_only_numbers: 'eZ Cash කේතය පමණක් අංක අඩංගු විය යුතුය',
    ezcash_pin_must_contain_4_numbers: 'eZ Cash කේතයෙහි අංකය 4 ක් අඩංගු විය යුතුය',
    //TODO
    btnOk: 'හරි',
    btnCancel: 'අවලංගු කරන්න',
    btnYes: 'ඔව් ',
    btnNo: 'නැත',
    confirmButtonTxt: 'තහවුරු කරන්න',
    cancelButtonTxt: 'අවලංගු කරන්න',
    retryButtonText: 'නැවත උත්සාහ කරන්න',
    continueButtonText: 'ඉදිරියට',
    modalTitle: 'තහවුරු සහ අත්සන කරන්න',
    modalTitleOtpVerified: 'තහවුරු සහ සක්‍රීය කරන්න',
    lcdValue: 'දේශීය ඇමතුම් තැන්පතු',
    totalPayment: 'මුළු ගෙවීම්',
    system_error: 'පද්ධති දෝෂයකි',

    enter_valid_amount: 'කරුණාකර වලංගු වටිනාකම ඇතුලත් කරන්න',
    reload_cant_empty: 'රීලෝඩය වටිනාකම හිස් විය නොහැක',
    select_first_reload: 'පළමු රීලෝඩය තෝරන්න',
    continue: 'ඉදිරියට',
    more_offers: 'තවත් දීමනා',

    activation_success: 'සාර්ථකව සක්‍රීය කරන ලදී',
    invalid_sim_serieal: 'වැරදි SIM අංකයකි',
    customer_information_image_not_captured: 'පාරිභෝගිකයාගේ මුහුණේ ඡායාරූපය ලබාගෙන නොමැත',
    please_enter_nic: 'කරුණාකර ජාතික හැදුනුම්පතෙහි ඡායාරූපය ලබා දෙන්න',
    passport_image_not_captured: 'පාස්පෝටයේ ඡායාරූපය ලබාගෙන නොමැත',
    address_image_not_captured: 'ලිපිනය ඡායාරූපගත කර නැත',
    pob_image_not_captured: 'POB ඡායාරූපගත කර නැත',
    customer_image_not_captured: 'පාරිභෝගික පින්තූරය ඡායාරූපගත කර නැත',
    invalid_altenate_contact_no: 'වලංගු නොවන විකල්ප දුරකථන අංකය',
    signature_not_captured: 'පාරිභෝගික අත්සන ලබාගන්න',
    submit_valid_nic: 'කරුණාකර වලංගු NIC අංකය ඇතුලත් කරන්න',
    submit_valid_passport: 'කරුණාකර වලංගු Passport අංකය ඇතුලත් කරන්න',
    ezCashAccountLabel: 'eZ Cash ගිණුම',
    please_enter_valid_email: 'කරුණාකර වලංගු විද්යුත් තැපැල් ඇතුලත් කරන්න',
    conn_Type: 'සම්බන්ධතා වර්ගය', 
    mobileActivationTitle: 'මොබයිල් සක්‍රීය කිරී',

  },
  ta: {
    nicValidate: 'NIC உறுதிப்படுத்தல்',
    passValidate: 'கடவுச்சீட்டு உறுதிப்படுத்தல்',
    productInfo: 'தகவல்',
    custoInfo: 'வாடிக்கையாளர் விபரம்',
    buttonTxt: 'செயற்படுத்த',
    nic: 'தேசிய அடையாள அட்டை',
    simNo: 'சிம் இலக்கம்',
    mobileNo: 'மொபைல் இலக்கம்',
    firstRel: 'முதலாவது கட்டணம்',
    alternateContact: 'மாற்று தொலைபேசி இலக்கம்',
    otherContact: 'மாற்று தொலைபேசி இலக்கம்',
    prepaid: 'முற்கொடுப்பனவு',
    postpaid: 'பிற்கொடுப்பனவு',
    srilankan: 'இலங்கையர்',
    foreigner: 'வெளிநாட்டவர்',
    lcd: 'Local Call Deposit (LCD)',
    email: 'Email',
    ezcash: '(0771234567 eZ Cash பின்)',
    ezCashPin: 'eZ Cash பின்',

    idLabelInptNicEng: 'NIC',
    idLabelInputNic: 'தேசிய அடையாள அட்டை',
    idLabelInputPp: 'கடவுச்சீட்டு',

    captureNicLabelTxt: 'NIC புகைப்படம் எடுக்க',
    captureDlLabelTxt: 'ஓட்டுனர் உரிமம் புகைப்படம் எடுக்க',
    capturePpLabelTxt: 'கடவுச்சீட்டு புகைப்படம் எடுக்க',
    captureForeignAddress: 'முகவரி புகைப்படம் எடுக்க',

    capturedNicLabelTxt: 'NIC முற்பக்கம் மற்றும் பின்பக்கம்',
    capturedDlLabelTxt: 'ஓட்டுனர் உரிமம்',
    capturedPpLabelTxt: 'கடவுச்சீட்டு',
    capturedForeignAddress: 'முகவரி',

    backMessage: 'இயக்குதலை ரத்து செய்து முதன்மை பக்கத்துக்கு செல்ல விரும்புகிறீர்களா?',
    notClearNicLabelTxt: 'வாடிக்கையாளரின் முகம் NIC இல் தெளிவில்லை',
    notClearDlLabelTxt: 'வாடிக்கையாளரின் முகம் ஓட்டுநர் உரிமத்தில்  தெளிவில்லை',
    notClearPpLabelTxt: 'வாடிக்கையாளரின் முகம் கடவுசீட்டில் தெளிவில்லை',

    captureCustomerPhotoSubTxt: 'வாடிக்கையாளரை புகைப்படம் எடுக்க',
    captureProofOfDeliverySubTxt: 'பில் ஆதாத்தை புகைப்படம் எடுக்க',
    capturedCustomerPhotoSubTxt: 'வாடிக்கையாளர் புகைப்படம் எடுக்க',
    capturedProofOfDeliverySubTxt: 'பில் ஆதார புகைப்படம் எடுக்க',

    pobNicLabelTxt: 'தற்போதைய முகவரி வேறு/ NIC முகவரி தெளிவில்லை',
    pobDlLabelTxt: 'தற்போதைய முகவரி வேறு/ ஓட்டுனர் உரிமம் முகவரி தெளிவில்லை',
    pobPpLabelTxt: 'தற்போதைய முகவரி வேறு/ கடவுச்சீட்டு முகவரி  தெளிவில்லை',

    pobNicLabelTxtPost: 'பில் ஆதார முகவரி வேறு/ NIC முகவரி தெளிவில்லை',
    pobDlLabelTxtPost: 'பில் ஆதார முகவரி வேறு/ ஓட்டுனர் உரிம முகவரி தெளிவில்லை',
    pobPpLabelTxtPost: 'பில் ஆதார முகவரி வேறு/ கடவுச்சீட்டு முகவரி தெளிவில்லை',

    customerSignature: 'வாடிக்கையாளர் கையொப்பம்',
    customerSignatureUC: 'வாடிக்கையாளர் கையொப்பம்',
    placeYourSignature: 'வாடிக்கையாளர் கையொப்பத்தை இங்கே உள்ளிடுக',
    bySigining: 'கையொப்பம் மூலம், நீங்கள் ஒப்புக்கொள்கிறீர்கள்  எங்களின்',
    byclicking: 'கிளிக் செய்வதன் மூலம், நீங்கள் ஒப்புக்கொள்கிறீர்கள் எங்களின்',
    enterCustomerSignature: 'தயவுசெய்து வாடிக்கையாளர் கையொப்பத்தை உள்ளிடவும்',
    tns: 'விதிமுறைகள் மற்றும் நிபந்தனைகளை',
    tnsUc: 'விதிமுறைகள் மற்றும் நிபந்தனைகளை',
    scanSim: 'சிம் ஸ்கேன்',
    numberSelection: 'தொடர்பு எண் தேர்வு செய்யவும்',
    numberRandom: 'தேர்வு செய்க',
    numberSearch: 'தேடல்',
    numberInclude: 'உள்ளடக்க',
    numberStart: 'தொடங்கும் இலக்கம்',
    numberEnd: 'இறுதி இலக்கம்',
    numberCancel: 'ரத்து செய்க',

    reloadAmount: 'Reload தொகை',
    totalPay: 'செலுத்த வேண்டிய மொத்த தொகை',
    showData: 'குறித்த தகவலைக் காட்டு',
    localCall: 'Local call Deposit',
    conFee: 'இணைப்பு கட்டணம்',
    packageInfo: 'பேகேஜ் தகவல்',
    nicNo: 'தேசிய அடையாள அட்டை எண்',
    ppNo: 'கடவுச்சீட்டு எண்',
    type: 'Type',
    connetionType: 'போஸ்யிட்',
    connectionNo: 'தொடர்பு எண்',
    offer: 'சலுகை',
    package: 'பேகேஜ்',
    rental: 'தவணை',
    otherContactNo: 'இணைப்பு எண்',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'இந்த கட்டணத்தைச் செய்து புதிய இணைப்பைப் பெறவும்.',
    confirmText: 'நிச்சயமாக நீங்கள் ரத்து செய்ய விரும்புகிறீர்களா ?',
    outstandingLeftText: 'வாடிக்கையாளர் செலுத்த வேண்டிய மொத்த தொகை',
    outstandingRightText: 'துண்டிக்கப்பட்ட இணைப்புக்கள்',
    //New
    customer_already_with_dialog: 'வாடிக்கையாளர் விவரங்கள் ஏற்கனவே டயலொக் இல் உள்ளன',
    select_mobile_number: 'விவரங்களை சரிபார்க்க மொபைல் எண்ணைத் தேர்ந்தெடுக்கவும்',
    send_validation: 'மதிப்பிட அனுப்புக',
    skip_validation: 'சரிபார்த்தல் தவிர்க்க',
    msg_please_select_no: 'பட்டியலில் இருந்து எண்ணைத் தேர்ந்தெடுக்கவும்',
    five_pin_number_sent_to: '5 இலக்க PIN எண் அனுப்பப்படுகிறது',
    enter_pin: 'பின் உள்ளிடவும்',
    receive_five_digit_pin: '5 இலக்க PIN ஐப் பெறவில்லையா ?',
    recive_five_digit_pin: '5 இலக்க PIN ஐப் பெறவில்லையா ?',
    resend_pin: 'மீண்டும் PIN ஐ அனுப்புக',
    validate_pin: 'PIN சரிபார்த்தல்',
    otp_validation_success: 'OTP சரிபார்த்தல் வெற்றி',
    otp_validation_failed: 'OTP சரிபார்த்தல் தோல்வி !',
    //Need review
    otp_resent: 'OTP மீண்டும் அனுப்பப்பட்டுள்ளது',
    otp_select_a_number: 'ஓர் இலக்கத்தை தெரிவு செய்யவும் ',
    outstandingFee: 'நிலுவை கட்டணம்',
    please_enter_ezcash_code: 'தயவுசெய்து eZ Cash PIN ஐ  எழுதவும்',
    ezcash_pin_must_only_numbers: 'eZ Cash PIN இலக்கங்களை மட்டுமே கொண்டது ',
    ezcash_pin_must_contain_4_numbers: 'eZ Cash PIN 4 இலக்கங்களை மட்டுமே கொண்டது',

    //TODO
    btnOk: 'சரி',
    btnCancel: 'අවලංගු කරන්න',
    btnYes: 'ඔව් ',
    btnNo: 'නැත',
    confirmButtonTxt: 'உறுதிப்படுத்தவும்',
    cancelButtonTxt: 'ரத்து செய்யவும்',
    retryButtonText: 'மீண்டும் முயற்சி செய்',
    continueButtonText: 'தொடர்ந்து',
    modalTitle: 'உறுதிப்படுத்தி கையொப்பமிடுக',
    modalTitleOtpVerified: 'உறுதிப்படுத்தி செயல்படுத்தவும்',
    lcdValue: 'Local Call Deposit',
    totalPayment: 'செலுத்த வேண்டிய மொத்த கொடுப்பனவு',
    system_error: 'கணினி பிழை',

    enter_valid_amount: 'சரியானத் தொகையை உள்ளடக்க',
    reload_cant_empty: 'Reload கட்டணம் காலியாக இருக்கக்கூடாது',
    select_first_reload: 'முதலாவது Reload கட்டணத்தை தெரிக',
    continue: 'தொடர்ந்து செல்ல',
    more_offers: 'மேலும் சலுகைகள்',

    activation_success: 'சேவை வெற்றிகரமாக செயற்ப்படுத்தப்பட்டது',
    invalid_sim_serieal: 'தவறான சிம் சீரியல்',
    customer_information_image_not_captured: 'வாடிக்கையாளரின் தகவல்களில் புகைப்படம் பிடிக்கப்படவில்லை',
    please_enter_nic: 'தயவுசெய்து NIC புகைபடப்பிடிப்பை பூர்த்தி செய்க',
    passport_image_not_captured: 'பாஸ்போர்ட் புகைபடம் பிடிக்கப்படவில்லை',
    address_image_not_captured: 'விலாச புகைபடம் பிடிக்கபடவில்லை',
    pob_image_not_captured: 'POB புகைபடம் பிடிக்கபடவில்லை',
    customer_image_not_captured: 'வாடிக்கையாளரின் புகைபடம் பிடிக்கப்படவில்லை',
    invalid_altenate_contact_no: 'தவறான மாற்று தொடர்பு இலக்கம்',
    signature_not_captured: 'வாடிக்கையாளரின் கையொப்ப புகைபடம் பிடிக்கப்படவில்லை',
    submit_valid_nic: 'சரியான NIC இனை சமர்ப்பிக்க',
    submit_valid_passport: 'சரியான கடவுசீட்டினை சமர்ப்பிக்க',
    ezCashAccountLabel: 'eZ Cash கணக்கு',
    please_enter_valid_email: 'சரியான மின்னஞ்சல் முகவரியை உள்ளிடுக',
    conn_Type: 'இணைப்பு வகை',    
    mobileActivationTitle: 'மொபைல் சேவை செயற்படுத்தல்',
  }

});

export default strings;
