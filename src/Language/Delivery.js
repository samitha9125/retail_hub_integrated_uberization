import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  en: {
    backMessage: 'Do you want to cancel the operation & go back ?',
    continue: 'CONTINUE',
    cancel: 'CANCEL',
    titleOrders: 'WORK ORDER',
    titleDispatched: 'DISPATCHED WORK ORDER',
    titlePending: 'PENDING WORK ORDER',
    titleInprogess: 'IN-PROGESS WORK ORDER',
  },
  si: {
    backMessage: 'ක්‍රියාකාරීත්වය අවලංගු කිරීමට හා ආපසු යාමට අවශ්‍යද?',
    continue: 'CONTINUE',
    cancel: 'CANCEL',
    titleOrders: 'වැඩ යෙදුම',
    titleDispatched: 'DISPATCHED WORK ORDER',
    titlePending: 'PENDING WORK ORDER',
    titleInprogess: 'IN-PROGESS WORK ORDER',
  },
  ta: {
    backMessage: 'செயல்பாட்டை ரத்து செய்து  மீண்டும் முன்பக்கம் செல்ல விரும்புகிறீர்களா?',
    continue: 'CONTINUE',
    cancel: 'CANCEL',
    titleOrders: 'வேலைக்கட்டளை',
    titleDispatched: 'DISPATCHED WORK ORDER',
    titlePending: 'PENDING WORK ORDER',
    titleInprogess: 'IN-PROGESS WORK ORDER',
  }

});

export default strings;
