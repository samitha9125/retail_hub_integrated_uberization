import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        continueCaps: 'CONTINUE',
        lblReplacement: 'Warranty Replacement',
        lblOldSTBSerial: 'old STB',
        lblOldSIMSerial: 'old SIM',
        lbloldRCUSerial: 'old RCU',
        lblOldPowerSupplySerial: 'old power supply',
        lblOldLNBSerial: 'old LNB serial',
        lblOldRemoteSerial: 'old remote serial',

        lblNewSTBSerial: 'new STB serial',
        lblNewSIMSerial: 'new SIM serial',
        lblNewRCUSerial: 'new RCU serial',
        lblNewPowerSupplySerial: 'new power supply serial',
        lblNewLNBSerial: 'new LNB serial',
        lblNewRemoteSerial: 'new remote serial',
        newSerial: 'new serial',
        lblNewCable: 'new Cable serial',

        scanOldSerial: 'Scan old serial',
        scanNewSerial: 'Scan new serial',
        scanNewSTBSerial: 'Scan new STB serial',
        powerSupplySerial: 'Power supply serial',
        rcuSerial: 'RCU serial',
        rcuWarranty: 'RCU warranty',
        powerSupplyWarranty: 'Power supply warranty',

        serial: 'Serial Number',

        oldSerial: 'old serial',
        oldSTBSerial: 'old STB serial',
        oldSIMSerial: 'old SIM serial',
        lblOldCable: 'old Cable serial',

        warrantyReplacementCaps: 'WARRANTY REPLACEMENT',
        connectionNo: 'Connection no',
        warrantyItem: 'Warranty item',
        newSTBType: 'New STB type',
        contactNo: 'Contact no',
        contactNoAlt: 'Alternative contact no',
        confirmation: 'Customer confirmation',
        accessoryType: 'Item type',
        newSerial: 'New Serial',
        newWarrantyPeriod: 'warranty',
        confirmCaps: 'CONFIRM',
        itemType: 'Item type',
        warrentyText1: 'Remaining warranty period :',
        warrentyText2: 'Scanned item is eligible for a replacement under warranty.',
        invalidNIC: 'Invalid NIC number',
        invalidConnectionNo: 'Invalid DTV number',
        invalidContactNo: 'Invalid contact number.',
        noRegContactNo: 'No registered contact number was found.Please enter an alternative mobile number.',
        error: 'Error!',
        title: 'WARRANTY REPLACEMENT',
        notEligibleWarranty: 'Scanned item is not eligible for a replacement under warranty.',

        network_error_message: 'Network Error, \nPlease check your intenet connection',
        api_error_message: 'Error occured while loading the data',
        warranty_replacement_successful: 'Warranty replacement & Provision successful.',
        lblNewCable: 'new Cable serial',
        lblOldCable: 'old Cable serial',
        lblWarrantyPeriodExpired: 'Warranty period expired.',
        Serialnumber: 'Serial number :',
        systemError: 'System error when updating user provision status.',
        lblNew: 'New',
        lblSerial: 'serial',
        continue: 'CONTINUE',
    },
    si: {
        continueCaps: 'CONTINUE',
        rcuWarranty: 'RCU warranty',
        powerSupplyWarranty: 'Power supply warranty',
        lblReplacement: 'Warranty Replacement',

        lblOldSTBSerial: 'old STB',
        lblOldSIMSerial: 'old SIM',
        lbloldRCUSerial: 'old RCU',
        lblOldPowerSupplySerial: 'old power supply',
        lblOldLNBSerial: 'old LNB serial',
        lblOldRemoteSerial: 'old remote serial',
        lblOldCable: 'old Cable serial',

        lblNewSTBSerial: 'new STB serial',
        lblNewSIMSerial: 'new SIM serial',
        lblNewRCUSerial: 'new RCU serial',
        lblNewPowerSupplySerial: 'new power supply serial',
        lblNewLNBSerial: 'new LNB serial',
        lblNewRemoteSerial: 'new remote serial',
        lblNewCable: 'new Cable serial',

        warrantyReplacement: 'ඇප ආදේශනය',
        warrantyReplacementCaps: 'ඇප ආදේශනය',
        connectionNo: 'සබඳතා අංකය',
        warrantyItem: 'වගකීම් ලද උපාංග',
        scanOldSerial: 'පරණ ආකෘතිය ස්කෑන් කිර්‍රම',
        contactNo: 'දුරකථන අංකය',
        confirmation: 'තහවුරු කිරීම',
        accessoryType: 'උපාංග වර්ගය',
        itemType: 'උපාංග වර්ගය',
        newSerial: 'නව අනුක්‍රමික අංකය',
        newWarrantyPeriod: 'නව වගකීම් කාලය', //to do
        confirmCaps: 'තහවුරු කරන්න',
        invalidNIC: 'වලංගු නොවන හැඳුනුම්පත් අංකයකි',
        invalidConnectionNo: 'වලංගු නොවන DTV අංකයකි',
        invalidContactNo: 'දුරකථන අංකය 07xxxxxxxx ලෙස ඇතුලත් කරන්න.',
        contactNoAlt: 'අතිරේක දුරකථන අංකය.',
        noRegContactNo: 'පෙර සටහන් ගත දුරකථන අංක නොමැත.විකල්ප දුර අංක ඇතුලත් කරන්න.',
        error: 'වැරදියි!',
        serial: 'කේත අංක',
        notEligibleWarranty: 'Scan කල අංකය සඳහා වගකීම් කල් ඉකුත් වී ඇත.',
        eligibleWarranty: 'වගකීම් කාල සීමාව තුල ප්‍රතිස්ථාපනය කිරීමකට වලංගු වේ.',
        network_error_message: 'ජාල දෝෂයකි, ඔබගේ අන්තර්ජාල සම්බන්ධතාව පරීක්ෂා කරන්න',
        api_error_message: 'දත්ත හුවමාරුවේදී දෝෂයක් සිදු විය',
        connectionNoNotEmpty: 'සම්බන්ධතා අංකය සහ වගකීම් අයිතමය හිස්ව තැබිය නොහැක',
        warranty_replacement_successful: 'Warranty replacement & Provision successful.',
        lblNewCable: 'new Cable serial',
        lblOldCable: 'old Cable serial',
        warrentyText1: 'Remaining warranty period :',
        warrentyText2: 'Scanned item is eligible for a replacement under warranty.',
        lblWarrantyPeriodExpired: 'Warranty period expired.',
        Serialnumber: 'අනුක්‍රමික අංකය :',
        systemError: 'System error when updating user provision status.',
        lblNew: 'New',
        lblSerial: 'serial',
        continue: 'ඉදිරියට',
        powerSupplySerial: 'Power supply serial',
        rcuSerial: 'RCU serial',
    },
    ta: {
        continueCaps: 'CONTINUE',

        rcuWarranty: 'RCU warranty',
        powerSupplyWarranty: 'Power supply warranty',
        lblReplacement: 'Warranty Replacement',

        lblOldSTBSerial: 'old STB',
        lblOldSIMSerial: 'old SIM',
        lbloldRCUSerial: 'old RCU',
        lblOldPowerSupplySerial: 'old power supply',
        lblOldLNBSerial: 'old LNB serial',
        lblOldRemoteSerial: 'old remote serial',

        lblNewSTBSerial: 'new STB serial',
        lblNewSIMSerial: 'new SIM serial',
        lblNewRCUSerial: 'new RCU serial',
        lblNewPowerSupplySerial: 'new power supply serial',
        lblNewLNBSerial: 'new LNB serial',
        lblNewRemoteSerial: 'new remote serial',

        lblSTBSerial: 'New STB serial',
        newSTBWarrentyPeriod: 'New STB warrenty period',

        warrantyReplacement: 'உத்தரவாதத்திற்குட்பட்ட மாற்றம்',
        warrantyReplacementCaps: 'உத்தரவாதத்திற்குட்பட்ட மாற்றம்',
        connectionNo: 'இணைப்பு இலக்கம்',
        warrantyItem: 'உத்தரவாதத்திற்குட்பட்ட பொருள்',
        scanOldSerial: 'பழைய தொடர் எண்ணை ஸ்கேன் செய்க',
        contactNo: 'Contact no',
        confirmation: 'உறுதிப்படுத்தல்',
        accessoryType: 'உதிரிப்பாக வகை',
        itemType: 'உதிரிப்பாக வகை',
        newSerial: 'புதிய தொடர் எண்',
        newWarrantyPeriod: 'புதிய உத்தரவாத காலம்',
        confirmCaps: 'CONFIRM',

        invalidNIC: 'தவறான NIC இலக்கம்',
        invalidConnectionNo: 'தவறான DTV இலக்கம்',
        invalidContactNo: 'Invalid contact number.',
        contactNoAlt: 'மாற்று தொலைபேசி இலக்கம்',
        noRegContactNo: 'No registered contact number was found.Please enter an alternative mobile number.',
        error: 'Error!',
        serial: 'Serial Number',
        notEligibleWarranty: 'Scanned item is not eligible for a replacement under warranty.',
        eligibleWarranty: 'Scanned item is eligible for a replacement under warranty.',
        network_error_message: 'இணைப்பில் பிழை, உங்கள் இணைய இணைப்பை சரிபார்க்கவும்',
        api_error_message: 'தரவை ஏற்றும்போது பிழை ஏற்பட்டுள்ளது',
        connectionNoNotEmpty: 'இணைப்பு எண் மற்றும் உத்தரவாத பொருள் வெற்றிடமாக இருக்க முடியாது',
        warranty_replacement_successful: 'Warranty replacement & Provision successful.',
        lblNewCable: 'new Cable serial',
        lblOldCable: 'old Cable serial',
        warrentyText1: 'Remaining warranty period :',
        warrentyText2: 'Scanned item is eligible for a replacement under warranty.',
        lblWarrantyPeriodExpired: 'Warranty period expired.',
        Serialnumber: 'Serial number :',
        systemError: 'System error when updating user provision status.',
        lblNew: 'New',
        lblSerial: 'serial',
        continue: 'தொடர்க',
        powerSupplySerial: 'Power supply serial',
        rcuSerial: 'RCU serial',
    }
});

export default strings;
