import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        activationDetails: 'Activation Details',
        details: 'DETAILS',
        accept: 'ACCEPT',
        reject: 'REJECT',
        approved_date: 'Approved Date',
        approved_quantity: 'Approved qty',
        approved_quantity_full: 'Approved Quantity',
        requested_date: 'Requested Date',
        issued_quantity: 'Issued qty',
        issued_quantity_full: 'Issued Quantity',
        checked_quantity: 'Checked qty',
        continue: "CONTINUE",
        screenTitle: 'STOCK ACCEPTANCE',
        network_error_message: 'Network Error, \nPlease check your intenet connection',
        backMessage: 'Do you want to cancel the operation & go back ?',
        api_error_message: 'Error occured while loading the data',
        disconected: 'disconnected',
        connected: 'connected',
        network_conected: 'Network connection is established'
    },
    si: {
        activationDetails: 'Activation Details',
        details: 'විස්තර',
        accept: 'අනුමත කරන්න',
        reject: 'REJECT',
        approved_date: 'අනුමත දිනය',
        approved_quantity: 'අනුමත ප්‍රමාණය',
        approved_quantity_full: 'අනුමත ප්‍රමාණය',
        requested_date: 'ඉල්ලූ දිනය',
        issued_quantity: 'නිකුත් කළ ප්‍රමාණය',
        issued_quantity_full: 'නිකුත් කළ ප්‍රමාණය',
        checked_quantity: 'පරීක්ෂා කළ ප්‍රමාණය',
        continue: "CONTINUE",
        screenTitle: 'අනුමත ප්‍රමාණය',
        api_error_message: 'දත්ත හුවමාරුවේදී දෝෂයක් සිදු විය',
        backMessage: 'ක්‍රියාකාරීත්වය අවලංගු කිරීමට හා ආපසු යාමට අවශ්‍යද?',
        network_error_message: 'ජාල දෝෂයකි, ඔබගේ අන්තර්ජාල සම්බන්ධතාව පරීක්ෂා කරන්න',
        disconected: 'disconnected',
        connected: 'connected',
        network_conected: 'Network connection is established'
    },
    ta: {
        activationDetails: 'Activation Details',
        details: 'விவரங்கள்',
        accept: 'ஏற்றுக்கொள்',
        reject: 'REJECT',
        approved_date: 'அங்கீகரிக்கப்பட்ட திகதி',
        approved_quantity: 'அங்கீகரிக்கப்பட்ட அளவு',
        approved_quantity_full: 'அங்கீகரிக்கப்பட்ட அளவு',
        requested_date: 'கோரிக்கை திகதி',
        issued_quantity: 'வழங்கப்பட்ட அளவு',
        issued_quantity_full: 'வழங்கப்பட்ட அளவு',
        checked_quantity: 'பரிசோதித்த அளவு',
        continue: "CONTINUE",
        screenTitle: 'பொருட்களை ஏற்றுக்கொள்ளல் ',
        api_error_message: 'தரவை ஏற்றும்போது பிழை ஏற்பட்டுள்ளது',
        backMessage: 'செயல்பாட்டை ரத்து செய்து  மீண்டும் முன்பக்கம் செல்ல விரும்புகிறீர்களா?',
        network_error_message: 'இணைப்பில் பிழை, உங்கள் இணைய இணைப்பை சரிபார்க்கவும்',
        disconected: 'disconnected',
        connected: 'connected',
        network_conected: 'Network connection is established'
    }
});

export default strings;
