import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  en: {
    ok: "OK",
    cancel: 'CANCEL',
    yes: 'YES',
    no: 'NO',
    confirmButtonTxt: 'CONFIRM',
    cancelButtonTxt: 'CANCEL',
    retryButtonText: 'RETRY',
    continueButtonText: 'CONTINUE',
    rs_text: 'Rs',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'Continue to make this payment and get the new connection.',
    confirmText: 'Are you sure you want to cancel?',
    outstandingLeftText: 'Customer has an outstanding of',
    outstandingRightText: 'from disconnected connection(s).',
    do_uou_want_to_cancel_image_capture_and_go_back: 'Do you want to cancel the image capture and go back ?',
  },
  si: {
    ok: "හරි",
    cancel: 'අවලංගු කරන්න',
    yes: 'YES',
    no: 'NO',
    confirmButtonTxt: 'තහවුරු කරන්න',
    cancelButtonTxt: 'අවලංගු කරන්න',
    retryButtonText: 'නැවත උත්සාහ කරන්න',
    continueButtonText: 'ඉදිරියට',
    rs_text: 'Rs',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'ගෙවීම සිදු කර නව සම්බන්ධතාවය ලබා ගන්න',
    confirmText: 'අවලංගු කිරිම තහවුරු කරන්න',
    outstandingLeftText: 'පාරිබෝගිකයාගේ ගෙවිය යුතු හිග මුදල',
    outstandingRightText: 'විසන්ධි වූ සම්බන්ධතාවයන්',
    do_uou_want_to_cancel_image_capture_and_go_back: 'ඡායාරූප ගැනීම අවලංගු කර මුල් පිටුව වෙත ?',
  },
  ta: {
    ok: "சரி",
    cancel: 'ரத்து செய்க',
    yes: 'YES',
    no: 'NO',
    confirmButtonTxt: 'உறுதிப்படுத்தவும்',
    cancelButtonTxt: 'ரத்து செய்யவும்',
    retryButtonText: 'மீண்டும் முயற்சி செய்',
    continueButtonText: 'தொடர்ந்து',
    rs_text: 'Rs',
    passportUC: 'PASSPORT',
    nicUC: 'NIC',
    last: 'இந்த கட்டணத்தைச் செய்து புதிய இணைப்பைப் பெறவும்.',
    confirmText: 'நிச்சயமாக நீங்கள் ரத்து செய்ய விரும்புகிறீர்களா ?',
    outstandingLeftText: 'வாடிக்கையாளர் செலுத்த வேண்டிய மொத்த தொகை',
    outstandingRightText: 'துண்டிக்கப்பட்ட இணைப்புக்கள்'
  }
});

export default strings;
