import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        error: 'Error!',
        continueButtonText: 'CONTINUE',
        cancel: 'CANCEL',
        api_error_message: 'Error occured while loading the data',
        backMessage: 'Do you want to cancel the operation & go back ?',
        network_error_message: 'Network Error, \nPlease check your intenet connection',
        HomeMenu: 'DIALOG SALSE APP',
        screenTitle: 'STOCK AVAILABILITY',
        googleMapsMessage: 'Do you want to view directions to this destination?',
        dispatchConfirm: 'Do you want to dispatch the work order ?',
        messageType: 'alert',
        headerTitle: 'STOCK AVAILABILITY',
        startDate: 'Schedule Start Date',
        endDate: 'Schedule End Date',
        date: 'Date',
        filterBy: 'Filter By',
        dateRange: 'Date Range',
        disconected: 'disconected',
        connected: 'connected',
        network_conected: 'Network connection is established'
    },
    si: {
        error: 'වැරදියි!',
        cancel: 'අතිහිටුවන්න',
        api_error_message: 'දත්ත හුවමාරුවේදී දෝෂයක් සිදු විය',
        backMessage: 'ක්‍රියාකාරීත්වය අවලංගු කිරීමට හා ආපසු යාමට අවශ්‍යද?',
        network_error_message: 'ජාල දෝෂයකි, ඔබගේ අන්තර්ජාල සම්බන්ධතාව පරීක්ෂා කරන්න',
        HomeMenu: 'DIALOG SALSE APP',
        screenTitle: 'පවතින තොග',
        googleMapsMessage: 'Do you want to view directions to this destination?',
        dispatchConfirm: 'Do you want to dispatch the work order ?',
        messageType: 'alert',
        headerTitle: 'STOCK AVAILABILITY',
        startDate: 'Schedule Start Date',
        endDate: 'Schedule End Date',
        date: 'දිනය',
        filterBy: 'Filter By',
        dateRange: 'Date Range',
        disconected: 'disconected',
        connected: 'connected',
        network_conected: 'Network connection is established'
    },
    ta: {
        error: 'Error!',
        continueButtonText: 'தொடர்ந்து',
        continue: 'CONTINUE',
        cancel: 'CANCEL',
        api_error_message: 'தரவை ஏற்றும்போது பிழை ஏற்பட்டுள்ளது',
        backMessage: 'செயல்பாட்டை ரத்து செய்து  மீண்டும் முன்பக்கம் செல்ல விரும்புகிறீர்களா?',
        network_error_message: 'இணைப்பில் பிழை, உங்கள் இணைய இணைப்பை சரிபார்க்கவும்',
        HomeMenu: 'DIALOG SALSE APP',
        screenTitle: 'கையிருப்பிலுள்ள பொருட்கள்',
        googleMapsMessage: 'Do you want to view directions to this destination?',
        dispatchConfirm: 'Do you want to dispatch the work order ?',
        messageType: 'alert',
        headerTitle: 'STOCK AVAILABILITY',
        startDate: 'Schedule Start Date',
        endDate: 'Schedule End Date',
        date: 'திகதி',
        filterBy: 'Filter By',
        dateRange: 'Date Range',
        disconected: 'disconected',
        connected: 'connected',
        network_conected: 'Network connection is established'

    }
});

export default strings;
