import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  en: {
    connction: 'Connection type',
    mobile: 'Mobile',
    number: 'Connection no',
    amount: 'Amount',
    btnTxt: 'PAY BILL',
    ezTitle: '(0777123456) eZ Cash account PIN',
    ezPin: 'eZ Cash PIN',
    amountTitle: 'Eg: 199 or 199.00',
    invalidDtv: 'Invalid DTV number',
    invalidMobile: 'Invalid mobile number',
    invalidLte: 'Invalid LTE number',
    invalidAmount: 'Invalid amount',
    minimumAmmountValue: 'Minimum amount is Rs.50',
    maximumAmmountValue: 'Maximum value is Rs.25000',
    backMessage: 'Do you want to cancel the bill payment and go back to home page ?',

  },
  si: {
    connction: 'සම්බන්ධතා වර්ගය',
    mobile: 'Mobile',
    number: 'සම්බන්ධතා අංකය',
    amount: 'ප්‍රමාණය',
    btnTxt: 'බිල්පත් ගෙවීම',
    ezPin: 'eZ Cash PIN',
    ezTitle: '(0777123456) eZ Cash ගිණුම් කේතය',
    amountTitle: 'උදා: 199 or 199.00',
    invalidDtv: 'වැරදි DTV අංකයකි',
    invalidMobile: 'වැරදි දුරකථන අංකයකි',
    invalidLte: 'වැරදි LTE අංකයකි',
    invalidAmount: 'වැරදි ප්‍රමාණයකි',
    minimumAmmountValue: 'අවම වටිනාකම Rs.50',
    maximumAmmountValue: 'උපරිම වටිනාකම Rs.25000',
    backMessage:  'බිල්පත් ගෙවීම අවලංගු කර නැවත මුල් පිටුව වෙත ?',


  },
  ta: {
    connction: 'இணைப்பு வகை',
    mobile: 'Mobile',
    number: 'இணைப்பு இலக்கம்',
    amount: 'தொகை',
    btnTxt: 'பணம் செலுத்து',
    ezPin: 'eZ Cash PIN',
    ezTitle: '(0777123456) eZ Cash கணக்கு பின்',
    amountTitle: 'எ.கா. 199 or 199.00',
    nvalidDtv: 'தவறான DTV இலக்கம் ',
    invalidMobile: 'தவறான மொபைல் இலக்கம்',
    invalidLte: 'தவறான LTE இலக்கம் ',
    invalidAmount: 'தவறான தொகை',
    minimumAmmountValue: 'குறைந்தபட்ச தொகை Rs.50',
    maximumAmmountValue: 'குறைந்தபட்ச தொகை RS.25000',
    backMessage: 'பில் செலுத்துதலை ரத்து செய்து, வீட்டுக்குச் செல்லுங்கள் ?',

  }
});

export default strings;
