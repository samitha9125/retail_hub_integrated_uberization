/*
 * File: app.js
 * Project: Dialog Sales App
 * File Created: Monday, 30th Oct 2017 3:53:37 pm
 * Author:  Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 10:22:11 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 *              Parthipan Kugadoss (parthipan@omobio.net)
 *              Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { Navigation } from 'react-native-navigation';
import { registerScreens, registerScreenVisibilityListener } from './screens';
import DtawerConfig from './screens/drawer/DrawerConfig';

import './config/global';
registerScreens();
registerScreenVisibilityListener();

Navigation.startSingleScreenApp({
  screen: {
    screen: 'DialogRetailerApp.StartupScreen',
    title: 'Dialog Sales App'
  },
  animationType: 'none',
  drawer: DtawerConfig,
  appStyle: {
    navBarHeight: 55
  }
});
