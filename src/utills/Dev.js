/*
 * File: Dev.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 15th May 2018 10:30:45 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 11:53:13 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { AsyncStorage } from 'react-native';
import Global from '../config/globalConfig';

const asyncForEach = async(array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

const getAsyncStorageDump = async() => {
  console.log('xxx Dev :: getAsyncStorageDump');
  let asyncStorageArray = [];
  const asyncStorageAllKeys = await AsyncStorage.getAllKeys();
  await asyncForEach(asyncStorageAllKeys, async(asyncKey) => {
    let elementValue = await AsyncStorage.getItem(asyncKey);
    asyncStorageArray.push(elementValue);
    console.log('xxx Dev :: getAsyncStorageDump :: elementValue =>', elementValue);
  });

  console.log('xxx Dev :: getAsyncStorageDump => asyncStorageArray =>', asyncStorageArray);
  return asyncStorageArray;
};

export default {
  asyncForEach,
  getAsyncStorageDump
};
