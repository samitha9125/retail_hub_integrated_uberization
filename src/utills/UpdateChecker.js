/*
 * File: UpdateChecker.js
 * Project: Dialog Sales App
 * File Created: Tuesday, 12th June 2018 12:22:26 pm
 * Author: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Last Modified: Wednesday, 13th June 2018 8:39:05 am
 * Modified By: Nipuna H Herath (nipuna@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Limited
 */


import { Alert, BackHandler, Linking } from 'react-native';
import Utills from './Utills';
import Global from './../config/globalConfig';

const Utill = new Utills();

const updateCheck = (sucessCb, tokenValue = null) => {
  console.log("@@ updateCheck");
  const token = tokenValue == null ? "first_time" : tokenValue;
  const version = Global.appVersion;
  const mode = Global.mode;

  const payloadData = {
    token,
    version,
    mode
  };
  Utill.apiStartupCheck(
    "startupCheck",
    "account",
    "ccapp",
    payloadData,
    sucessCb,
    failureCb
  );
};

const failureCb = (reponse) => {
  console.log('## startupCheck :: failureCb', reponse);
  if (reponse.downloadUrl !== undefined || reponse.version !== undefined) {
    showDownloadMsg(reponse.downloadUrl, reponse.version);
  } else {
    console.log('Failed to check version of Retailer App');
  }
};

const showDownloadMsg = (downloadUrl, version) => {
  console.log("@@@ showDownloadMsg");
  Alert.alert(
    "",
    `A new version of Retailer App ${version} is available. Do you want to download ?`,
    [
      {
        text: "Cancel",
        onPress: () => {
          console.log("## showDownloadMsg :: Cancel Pressed");
          BackHandler.exitApp();
        },
        style: "cancel"
      },
      {
        text: "Yes",
        onPress: () => {
          console.log("## showDownloadMsg :: Install APK");
          // BackHandler.exitApp();
          Linking .openURL(downloadUrl);
          // BackHandler.exitApp();
          // if (Utill.requestPermission("WRITE_EXTERNAL_STORAGE")) {
          //   FileUtils.downloadFile(downloadUrl, version);
          // }
        },
        style: "default"
      }
    ],
    { cancelable: false }
  );
};

export default {
  updateCheck
};
