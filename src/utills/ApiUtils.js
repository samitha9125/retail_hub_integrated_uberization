import { Alert, BackHandler, Linking } from 'react-native';
import Utills from './Utills';
import FileUtils from './FileUtils';
import Global from './../config/globalConfig';

const Utill = new Utills();

const startupCheck = (tokenValue) => {
  console.log('@@ startupCheck');
  const token = tokenValue == null
    ? 'first_time'
    : tokenValue;
  const version = Global.appVersion;
  const mode = Global.mode;
  const sucessCb = (reponse) => {
    console.log('## startupCheck :: sucessCb', reponse);
  };
  const failureCb = (reponse) => {
    console.log('## startupCheck :: failureCb', reponse);
    if (reponse.downloadUrl !== undefined || reponse.version !== undefined) {
      showDownloadMsg(reponse.downloadUrl, reponse.version);
    } else {
      console.log('Failed to check version of Retailer App');
    }
  };

  const payloadData = {
    token,
    version,
    mode
  };
  Utill.apiStartupCheck('startupCheck', 'account', 'ccapp', payloadData, sucessCb, failureCb);
};

const showDownloadMsg = (downloadUrl, version) => {
  console.log('@@@ showDownloadMsg');
  Alert.alert('', `A new version of Retailer App ${version} is available. Do you want to download ?`, [
    {
      text: 'Cancel',
      onPress: () => {
        console.log('## showDownloadMsg :: Cancel Pressed');
        BackHandler.exitApp();
      },
      style: 'cancel'
    }, {
      text: 'Yes',
      onPress: () => {
        console.log('## showDownloadMsg :: Install APK');
        Linking.openURL(downloadUrl);

        // if (Utill.requestPermission('WRITE_EXTERNAL_STORAGE')) {
        //   FileUtils.downloadFile(downloadUrl, version);
        // }
      }
    }
  ], { cancelable: false });
};

export default {
  startupCheck
};
