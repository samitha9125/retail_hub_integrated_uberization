
export const validateNIC = (nic) => {
  let imnicsi;
  if (typeof imnicsi !== 'string') {
    imnicsi = new String(nic);
    if (nic.length === 10 || nic.length === 12) {
      const reg = /^([1-9][0-9]{8}[vVxX])|([1-9][0-9]{6}[0-9]{5})|([2][0-9]{11})$/;

      if (reg.test(nic)) {
        console.log(nic.length);
        if (nic.length === 12) {
          let digits = imnicsi.substr(4, 3);
          let myInt = parseInt(digits);
          if (imnicsi.substr(0, 1) == 1 || imnicsi.substr(0, 1) == 2) {
            if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
              console.log('xxx validNIC');
              return nic.toLocaleUpperCase();
            }
          }
          return false;
        }
        if (nic.length === 10) {
          let digits = imnicsi.substr(2, 3);
          let myInt = parseInt(digits);
          if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
            console.log('xxx validNIC');
            return nic.toLocaleUpperCase();
          }
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

};

/**
 * Checks if passed EMAIL Address is valid or not.
 * @param {string} email
 */
export const validateEmail = (email) => {
  const re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
};

export const gsmDialogNumvalidate = (num) => {
  console.log('xxx gsmDialogNumvalidate', num.toString()[0]);
  const compairNo = num
    .toString()
    .length == 10 && num.toString()[0] == 0
    ? (num.slice(-9)).toString()
    : (num).toString();
  console.log('xxx gsmDialogNumvalidate', compairNo);
  return /^[0]?[7](7|6)[0-9]{7}$/.test(compairNo);
};

export const gsmNumbervalidate = (num) => {
  console.log('xxx gsmDialogNumvalidate', num.toString()[0]);
  const compairNo = num
    .toString()
    .length == 10 && num.toString()[0] == 0
    ? (num.slice(-9)).toString()
    : (num).toString();
  console.log('xxx gsmDialogNumvalidate', compairNo);
  // return /^[0]?[7](7|6)[0-9]{7}$/.test(compairNo);
  return /^[0]?[7][0-9]{8}$/.test(compairNo);
}

export const dtvNumValidate = (connRef) => {
  if (typeof connRef !== 'string') {
    connRef = new String(connRef);
  }
  
  if ((connRef.length === 8) && ((connRef.substr(0, 1) === '6') || (connRef.substr(0, 1) === '8'))) {
    return connRef;
  }
  return false;
}


export const dtvDirectSaleNumValidate = (connRef) => {
  if (typeof connRef !== 'string') {
    connRef = new String(connRef);
  }

  let reg;
  const conSub = connRef;
  if (conSub.substr(0, 1) === '6') {
    reg = new RegExp('^6[5-9][0-9]{6}')
  } else {
    reg = new RegExp('^[78][0-9]{7}')
  }

  if (reg.test(connRef)) {

    return connRef;
  }
  return false;
};

export const lteNumValidate = (num) => {
  if (typeof num !== 'string') {
    num = new String(num);
  }
  const reg = new RegExp('^(94|0)?[12]14[0-9]{6}');
  if (reg.test(num)) {
    return num.slice(-9);
  }
  console.log('lte_validation settings not valid');
  return false;
};

export const numericValidate = (num_val) => {
  let nu_value = new String(num_val);
  let pattern = /^\d+$/;
  if (pattern.test(nu_value)) {
    return true;
  }
  else {
    return false;
  }
};

export const alphaNumericValidate = (alpha_serial) => {
  let alnu_value = new String(alpha_serial);
  if (alnu_value.length > 0) {
    let pattern = /^[0-9a-zA-Z]+$/;
    if (pattern.test(alnu_value)) {
      console.log('true returned from alphanumeric');
      return true;
    }
    else {
      console.log('false returned due to non alphanumeric');
      return false;
    }
  }
  else {
    console.log('false returned due to length is not 22');
    return false;
  }

};

export const serialValidate = (serial) => {
  let alnu_value = new String(serial);
  if (alnu_value.length > 0) {
    let pattern = /^[0-9a-zA-Z]+$/;
    if (pattern.test(alnu_value)) {
      console.log('true returned from alphanumeric');
      return true;
    }
    else {
      console.log('false returned due to non alphanumeric');
      return false;
    }
  }
  else {
    console.log('false returned due to length is not 22');
    return false;
  }
};
export const accountNumFormatValidate = (num) => {
  if (!/^8[0-9]{6}[0-9]$/.test(num)) {
    return false;
  } else {
    return true;
  }
};

