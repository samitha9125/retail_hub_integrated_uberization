/*
 * File: AuthenticationUtils.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 10th October 2018 6:16:41 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 10th October 2018 6:22:31 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { Alert, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import DrawerConfig from '../screens/drawer/DrawerConfig';
import { ApiRequestUtils, Analytics, Screen } from './index';

/**
 * login function
 * @memberof AuthenticationUtils
 * @function login
 *
 */
const login = async() => {
  console.log('### AuthenticationUtils ::  login');

};

/**
 * logout function
 * @memberof AuthenticationUtils
 * @function LogOut
 *
 */
const logOut = async() => {
  console.log('### AuthenticationUtils ::  logOut');
  try {
    let isFirstTime = await AsyncStorage.getItem('isFirstTime');
    await AsyncStorage.removeItem('isFirstTime');
    await AsyncStorage.removeItem('api_key');
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('conn');
    await AsyncStorage.clear();
    await AsyncStorage.setItem('isFirstTime', isFirstTime);
    console.log(AsyncStorage.getItem('isFirstTime'));
    console.log(AsyncStorage.getItem('api_key'));
    console.log(AsyncStorage.getItem('token'));
    console.log(AsyncStorage.getItem('conn'));
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'DialogRetailerApp.views.LoginScreen',
        title: 'Dialog Sales App'
      },
      animationType: 'none',
      drawer: DrawerConfig,
      appStyle: {
        navBarHeight: 55
      }
    });
  } catch (error) {
    console.log(`AsyncStorage error: ${error.message}`);
  }

  const logoutSuccessCb = async() => {
    console.log('### AuthenticationUtils ::  logoutSuccessCb');
    Analytics.logEvent('dsa_logout_api_call_success');

  };

  const logoutFailureCb = async() => {
    console.log('### AuthenticationUtils ::  logoutFailureCb');
    Analytics.logEvent('dsa_logout_api_call_failed');

  };

  try {
    ApiRequestUtils.apiPostLegacy('logout', 'account', 'ccapp', {}, logoutSuccessCb, logoutFailureCb, logoutFailureCb);
  } catch (error) {
    console.log(`ApiRequestUtils :: logOut :: error: ${error.message}`);
  }
};

/**
 * Force logout alert
 * @memberof AuthenticationUtils
 * @param msg [String]
 * @param title [String]
 * @function LogOut
 *
 */
const forceLogoutAlert = (msg, title = '') => {
  console.log('### AuthenticationUtils ::  forceLogoutAlert');
  Alert.alert(
    title,
    Screen.getStringifyText(msg),
    [
      { text: 'OK', onPress: () => logOut() }, 
    ],
    { cancelable: false }
  );
};

export default {
  login,
  logOut,
  forceLogoutAlert
};
