/*
 * File: AccountUtils.js
 * Project: Dialog Sales App
 * File Created: Thursday, 17th January 2019 2:23:33 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 17th January 2019 2:26:40 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2019 Omobio (PVT) Ltd
 */

import { LocalStorageUtils } from './index';


/**
 * @description Common method get user logged status
 * @memberof AccountUtils
 */
const didUserLogout = async () => {
  console.log('### AccountUtils :: didUserLogout');
  const token = await LocalStorageUtils.getToken();
  const allowedServices = await LocalStorageUtils.getAllowedServices();
  if (token == null || allowedServices == null) {
    console.log('### AccountUtils :: didUserLogout TRUE');
    return true;
  } else {
    console.log('### AccountUtils :: didUserLogout FALSE');
    return false;
  }
};

/**
 * @description Get agent is CFSS or Retailer
 * @memberof AccountUtils
 */
const isCFSSAgent = async () => {
  console.log('### AccountUtils :: isCFSSAgent');
  let isCFSSAgent = await LocalStorageUtils.getIsCFSSAgent();
  if (isCFSSAgent == 'true' || isCFSSAgent == true) {
    console.log('### AccountUtils :: isCFSSAgent TRUE');
    return true;
  } else {
    console.log('### AccountUtils :: isCFSSAgent FALSE');
    return false;
  }
};


export default {
  didUserLogout,
  isCFSSAgent,
};