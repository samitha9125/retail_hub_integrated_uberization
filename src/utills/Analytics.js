/*
 * File: Analytics.js
 * Project: Dialog Sales App
 * File Created: Monday, 16th April 2018 10:21:38 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 15th June 2018 11:53:41 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */
import firebase from 'react-native-firebase';
import update from 'immutability-helper';

const logEvent = (event = 'dsa_default', params = {}) => {
  console.log('@@ logEvent', event);
  const prefix = {
    timestamp : Math.floor(Date.now() / 1000),
    imei : global.imei, 
    device_model: global.deviceModel, 
    app_version : global.appVersion,
    app_version_code : global.versionCode,
    retailer_msisdn : global.agentMssdn,
    sub_retailer_msisdn : global.subAgentMsisdn,
  };
  const payLoadData = update(prefix, { $merge: params });
  console.log('@@ logEvent :: event, payLoadData =>', event, payLoadData);
  firebase.analytics().logEvent(event, payLoadData);
};

const logFirebaseEvent = (event = 'dra_firebase_default', params = {}) => {
  console.log('@@ logFirebaseEvent', event);
  const prefix = {
    timestamp : Math.floor(Date.now() / 1000),
    imei : global.imei, 
    device_model: global.deviceModel, 
    app_version : global.appVersion,
    app_version_code : global.versionCode,
    retailer_msisdn : global.agentMssdn,
    sub_retailer_msisdn : global.subAgentMsisdn,
  };
  const payLoadData = update(prefix, { $merge: params });
  console.log('@@ logFirebaseEvent :: event, payLoadData =>', event, payLoadData);
  firebase.analytics().logEvent(event, payLoadData);
};

const logScreen = () => {
  console.log('@@ logScreen');
};

const setAnalyticsCollectionEnabled = (enabled = true) => {
  console.log('@@ setAnalyticsCollectionEnabled');
  firebase.analytics().setAnalyticsCollectionEnabled(enabled);
};

export default {
  setAnalyticsCollectionEnabled,
  logEvent,
  logFirebaseEvent,
  logScreen
};
