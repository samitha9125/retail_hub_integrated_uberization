/*
 * File: Screen.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 1st August 2018 11:33:55 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 11th October 2018 7:52:24 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */


import { Navigation } from  'react-native-navigation';
import { Alert } from 'react-native';
import _ from 'lodash';
import { Images, Colors } from '@Config';
import { Analytics, MessageUtils } from './index';

/**
 * @description Common method to show Modal
 * @param {Object} screen
 * @param {Object} [passProps={}]
 * @param {boolean} [overrideBackPress=true]
 * @memberof Screen
 */
const showModalView = (screen, passProps = {}, overrideBackPress = true ) => {
  console.log('### Screen :: showModalView :', screen);
  console.log('# screen title :', screen.title);
  console.log('# screen id :', screen.id);
  console.log('# screen passProps :', passProps);
  Navigation.showModal({
    title: screen.title,
    screen: screen.id,
    overrideBackPress: overrideBackPress,
    passProps: passProps
  });
  Analytics.logEvent('dsa_screen_show_modal', { screen_id: getScreenId(screen.id) } );
  Analytics.logEvent('dsa_screen_mount', { screen_id: getScreenId(screen.id) } );
};

/**
 * @description Common method to push View
 * @param {Object} screen
 * @param {Object} currentView
 * @param {Object} [passProps={}]
 * @param {boolean} [overrideBackPress=true]
 * @memberof Screen
 */
const pushView = (screen, currentView, passProps = {}) => {
  console.log('### Screen :: pushView :', screen);
  console.log('#screen title :', screen.title);
  console.log('#screen id :', screen.id);
  console.log('#screen passProps :', passProps);
  const navigator = currentView.props.navigator;
  navigator.push({
    title: screen.title,
    screen: screen.id,
    passProps: passProps
  });
  Analytics.logEvent('dsa_screen_mount', { screen_id: getScreenId(screen.id) } );

};

/**
 * @description Common method to show Alert
 * @param {String} [message]
 * @param {String} [title = ''}
 * @memberof Screen
 */
const showAlert = (message, title = '') => {
  console.log('### Screen :: showAlert :', title);
  console.log('# screen message :', message);
  Alert.alert(title, MessageUtils.getStringifyText(message));
  Analytics.logEvent('dsa_screen_show_alert');
};

/**
 * @description Common method to show custom dialog box 
 * @param {String} [message]
 * @param {String} [title = ''}
 * @param {okFn} [okFn = null}
 * @param {okText} [okText = 'OK}
 * @param {cancelFn} [cancelFn = null}
 * @param {cancelText} [okText = 'CANCEL}
 * @memberof Screen
 */
const showDialogBox = (message, title = '', okFn = null,  okText = 'OK', cancelFn = null, cancelText = 'CANCEL') => {
  console.log('### Screen :: showDialogBox :', title);
  console.log('# screen message :', message);

  let cancelButton = {
    text: cancelText,
    onPress: () => { 
      console.log('CANCEL Pressed'); 
      if (cancelFn !== null) cancelFn();
    },
    style: 'cancel'
  };

  let buttonArray  = [{
    text: okText,
    onPress: () => { 
      console.log('OK Pressed'); 
      if (okFn !== null) okFn();
    }
  }];

  if (cancelFn !== null) {
    buttonArray.push(cancelButton);
  }
  
  Alert.alert(title, MessageUtils.getStringifyText(message), buttonArray, { cancelable: true });
  Analytics.logEvent('dsa_screen_show_dialog_box');
};

/**
 * @description common method to show General modal
 * @param {Object} passProps
 * @memberof Screen
 */
const showGeneralModal = (passProps) => {
  console.log('### Screen :: showGeneralModal :: passProps - ', passProps);
  let screen = {
    title: 'CommonModalAlert',
    id: 'DialogRetailerApp.modals.CommonModalAlert',
  };

  if (_.isNil(passProps.primaryPress) && !_.isFunction(passProps.primaryPress)){ 
    passProps.primaryPress = () => dismissModal();
  }


  showModalView(screen, passProps);
};

  /**
   * @description common method to show general error modal
   * @param {String} error
   * @param {String} language
   * @memberof Screen
   */
const showGeneralErrorModal = (error, primaryPress= null, primary_text= null) => {
  console.log('### Screen :: showGeneralErrorModal :: error - ', error);
  let description = MessageUtils.getFormatedErrorMessage(error);
  let passProps = {
    primaryText: primary_text,
    disabled: false,
    hideTopImageView: false,
    primaryButtonStyle: { color: Colors.colorRed },
    icon: Images.icons.FailureAlert,
    description: description,
  };
  if (!_.isNil(primaryPress) && _.isFunction(primaryPress)){ 
    passProps.primaryPress = () => primaryPress();
  } else {
    passProps.primaryPress = () => dismissModal();
  }

  if (!_.isNil(primary_text)){ 
    passProps.primaryText = primary_text;
  } else {
    passProps.primaryText = MessageUtils.getOkText();
  }

  showGeneralModal(passProps);
};

/**
 * @description Common method to dismiss modal
 * @memberof Screen
 */
const dismissModal = () => {
  console.log('xxx modalDismiss');
  console.log('### Screen :: dismissModal');
  Analytics.logFirebaseEvent('select_content', {
    context: 'common_alert',
    item_id: 'common_alert',
    content_type: 'button',
    additional_details: 'Common method to dismiss alert',
  });
  Navigation.dismissModal({ animationType: 'slide-down' });
};

/**
 * @description Common method to get screen id
 * @param {String} [fullName}
 * @memberof Screen
 */
const getScreenId = (fullName) => {
  console.log('### Screen :: getScreenId :', fullName);
  let res = fullName.split('.');
  return res[res.length - 1];
};

/**
 * @description Common method to get stringify text
 * @param {Object} [text}
 * @memberof Screen
 */
const getStringifyText = (text) => {
  console.log('### Screen :: getStringifyText => text :', text);
  let stringifiedText = text;
  if (text !== undefined && text !== null && (typeof text == Object || typeof text == Array)) {
    stringifiedText = JSON.stringify(text);
  }
  console.log('### Screen :: getStringifyText => text :: after format =>', stringifiedText);
  return stringifiedText;
};

export default {
  showGeneralErrorModal,
  showGeneralModal,
  dismissModal,
  showModalView,
  pushView,
  showAlert,
  showDialogBox,
  getScreenId,
  getStringifyText
};
