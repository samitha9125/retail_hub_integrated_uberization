import React from 'react';
import { Alert, 
  AsyncStorage, 
  PermissionsAndroid, 
  NativeModules, 
  NetInfo, 
  Linking } from 'react-native';
import axios from 'axios';
import { Navigation } from 'react-native-navigation';
import update from 'immutability-helper';
import DeviceInfo from 'react-native-device-info';
import RNSimData from 'react-native-sim-data';
import Permissions from 'react-native-permissions';
import Global from './../config/globalConfig';
import { Colors, Images, Constants }  from './../config/';
import { Analytics, FormatUtils } from './index';
import Dev from './Dev';
import MessageUtils from './MessageUtils';
import DtawerConfig from '../screens/drawer/DrawerConfig';
import NetworkConnectivity from '../views/wom/components/NetworkComponent';
import _ from 'lodash';

const baseUrl = Global.baseUrl;
const baseUrlLogin = Global.baseUrlLogin;

const ERROR_MESSAGES = {
  en: {
    SYSTEM_ERROR: 'System error. Please try again',
    NETWORK_ERROR: 'Network error'
  },
  si: {
    SYSTEM_ERROR: 'පද්ධති දෝෂයකි. කරුණාකර නැවත උත්සාහ කරන්න',
    NETWORK_ERROR: 'ජාල දෝෂයකි'
  },
  ta: {
    SYSTEM_ERROR: 'கணினி பிழை. தயவு செய்து மீண்டும் முயற்சிக்கவும்',
    NETWORK_ERROR: 'நெட்வொர்க் பிழை'
  }
};

const LOCATION_ALERT_MESSAGES = (lang) => {
  let messages;
  if (lang == 'en') {
    messages = {
      askLocationPermission: 'Please grant location permission to continue.',
      askHighAccuracyLocation: 'Please enable improved location accuracy / high accuracy from your location settings.',
      enableLocationService: 'Please enable location services from your settings.'
    };
  } else if (lang == 'si') {
    messages = {
      askLocationPermission: 'Please grant location permission to continue.',
      askHighAccuracyLocation: 'Please enable improved location accuracy / high accuracy from your location settings.',
      enableLocationService: 'Please enable location services from your settings.'
    };
  } else if (lang == 'ta') {
    messages = {
      askLocationPermission: 'Please grant location permission to continue.',
      askHighAccuracyLocation: 'Please enable improved location accuracy / high accuracy from your location settings.',
      enableLocationService: 'Please enable location services from your settings.'
    };
  }

  return messages;
};

const TIME_OUT = 60000;
const NetConnection = new NetworkConnectivity();

class Utills extends React.Component {
  //get SIM details
  getSimInfo() {
    console.log('@@@ getSimInfo', RNSimData.getSimInfo());
    return RNSimData.getSimInfo();
  }
  //get device details
  async getDeviceInfo() {
    console.log('@@@ getDeviceInfo');
    let uniqueID = '';
    let isUserLogged = true;
    let deviceRef = '';
    let imsi = '';
    let imei = '';
    let imeiNumber = '';
    let appVersion = '';
    let versionNumber ='';
    let versionCode ='';
    let systemName = '';
    let systemVersion = '';
    let deviceName = '';
    let deviceModel = '';
    let manufacturer = '';
    let deviceMemory = '';
    let diskCapacity = '';
    let cameraResolution = '';
    let freeDiskCapacity = '';
    let carrier = '';
    let connectionType = '';
    let cellularType = '';
    let simSerialNumber = '';
    let simInfo = '';
  
    try {
      isUserLogged = await !this.didUserLogout();
      await NativeModules.IMEI.getIMEI().then(result => {
        if (result && result.length > 0) {
          imei = result;
          deviceRef = imei;
          imeiNumber = imei;
          console.log('imei  success:', imei);
        }
      });

      await NativeModules.IMEI.getIMSI().then(result => {
        if (result && result.length > 0) {
          imsi = result;
          console.log('imsi  success:', imei);

        }
      });

      await NativeModules.IMEI.getSimSerialNumber().then(result => {
        if (result && result.length > 0) {
          simSerialNumber = result;
          console.log('simSerialNumber  success:', imei);

        }
      });

      await NativeModules.IMEI.getCameraRes().then(result => {
        console.log('getCameraRes');
        console.log('getCameraRes', result);
        if (result) {
          cameraResolution = result;
          console.log('CameraRes  success:', result);

        } else {
          cameraResolution = '';
        }
      });
    } catch (error) {
      console.log(`imei, imsi, simSerialNumber => error: ${error.message}`);
    }

    await NetInfo.getConnectionInfo().then((connectionInfo) => {
      console.log('connectionType: ' + connectionInfo.type + ', EffectiveConnectionType: ' + connectionInfo.effectiveType);
      connectionType = connectionInfo.type;
      cellularType = connectionInfo.effectiveType;
    });

    try {

      const info = DeviceInfo;
      const simInfoOb = this.getSimInfo();
      uniqueID = info.getUniqueID();
      //conn = info.getPhoneNumber();
      // imsi = simInfoOb.subscriptionId0;  
      // deviceRef = simInfoOb.deviceId0;
      appVersion = info.getVersion();
      versionNumber = Global.versionNumber;
      versionCode = Global.versionCode;
      systemName = info.getSystemName();
      systemVersion = info.getSystemVersion();
      deviceName = DeviceInfo.getBrand();
      deviceModel = DeviceInfo.getModel();
      manufacturer = info.getManufacturer();
      deviceMemory = DeviceInfo.getTotalMemory();
      diskCapacity = DeviceInfo.getTotalDiskCapacity();
      freeDiskCapacity = DeviceInfo.getFreeDiskStorage();
      carrier = info.getCarrier();
      //simSerialNumber = simInfoOb.simSerialNumber0;
      simInfo = simInfoOb;
    } catch (error) {
      console.log(`getDeviceInfo error: ${error.message}`);
    }
    return {
      uniqueID,
      isUserLogged,
      deviceRef,
      imsi,
      imei,
      imeiNumber,
      appVersion,
      versionNumber,
      versionCode,
      systemName,
      systemVersion,
      deviceName,
      deviceModel,
      manufacturer,
      diskCapacity,
      deviceMemory,
      freeDiskCapacity,
      carrier,
      connectionType,
      cellularType,
      simSerialNumber,
      cameraResolution,
      simInfo,  
    };
  }

  async setLang(lang, langIndex) {
    console.log('@@@ setLang :', lang, langIndex);
    try {
      await AsyncStorage.setItem('lang', lang);
      await AsyncStorage.setItem('langIndex', langIndex);
      global.lang = lang;
      console.log(`getLang: ${AsyncStorage.getItem('lang')}`);
      console.log(`getLang: ${AsyncStorage.getItem('langIndex')}`);
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async storeItem(key, item) {
    try {
      var item1 = await AsyncStorage.setItem(key, JSON.stringify(item));
      return item1;
    } catch (error) {
      console.log(error.message);
    }
  }

  async retrieveItem(key) {
    try {
      const retrievedItem = await AsyncStorage.getItem(key);
      const item = JSON.parse(retrievedItem);
      return item;
    } catch (error) {
      console.log(error.message);
    }
    return;
  }

  async getCompatibaleKey() {
    try {
      const isMsisdn = AsyncStorage.getItem('isMsisdn');
      const msisdn = AsyncStorage.getItem('msisdn');
      return { isMsisdn, msisdn };
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async getLang() {
    console.log('@@@ getLang');
    let lang = '';
    try {
      lang = await AsyncStorage.getItem('lang');
      console.log(`getLang: ${lang}`);
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    return lang.toString();
  }

  async getConnNumber() {
    console.log('@@@ getConnNumber');
    let conn = '';
    let subAgentMsisdn = '';
    let imei = '';
    let device_model = '';
    try {
      conn = await AsyncStorage.getItem('conn');
      subAgentMsisdn = await AsyncStorage.getItem('subAgentMsisdn');
      imei = await AsyncStorage.getItem('imei');
      device_model = await AsyncStorage.getItem('device_model');
      console.log('conn               :: ', conn);
      console.log('subAgentMsisdn     :: ', subAgentMsisdn);
      console.log('imei               :: ', imei);
      console.log('device_model       :: ', device_model);
      global.agentMssdn = conn;
      global.subAgentMsisdn = subAgentMsisdn;
      global.imei = imei;
      global.deviceModel = device_model;
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    return (conn == null?  '' : conn.toString());
  }

  async getPersistenceData() {
    console.log('@@@ getPersistenceData');
    let persistenceData = '';
    try {
      persistenceData = await AsyncStorage.getItem('persistenceData');
      if (persistenceData !== null) {
        persistenceData = await JSON.parse(persistenceData);
        console.log(persistenceData);
      }
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    return persistenceData;
  }

  async getRetailer() {
    console.log('@@@ getPersistenceData');
    let retailer = '';
    try {
      retailer = await AsyncStorage.getItem('isRetailer');
      if (retailer !== null) {
        retailer = await JSON.parse(retailer);
        console.log(retailer);
      }
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    return retailer;
  }

  async getAgentType() {
    console.log('@@@ getPersistenceData');
    let isCFSSAgent = '';
    try {
      isCFSSAgent = await AsyncStorage.getItem('isCFSSAgent');
      if (isCFSSAgent !== null) {
        isCFSSAgent = await JSON.parse(isCFSSAgent);
        console.log(isCFSSAgent);
      }
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }

    return isCFSSAgent;
  }

  async savePersistenceData(data) {
    console.log('@@@ savePersistenceData :', JSON.stringify(data));
    try {
      await AsyncStorage.setItem('persistenceData', JSON.stringify(data));
      console.log(AsyncStorage.getItem('persistenceData'));
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async saveAllowedServices(data) {
    console.log('@@@ allowed_services :', JSON.stringify(data));
    try {
      await AsyncStorage.setItem('allowedServices', JSON.stringify(data));
      console.log(AsyncStorage.getItem('allowedServices'));
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async saveTileData(data) {
    console.log('@@@ tileData :', JSON.stringify(data));
    try {
      await AsyncStorage.setItem('tileData', JSON.stringify(data));
      console.log(AsyncStorage.getItem('tileData'));
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  async saveSessionData(data) {
    console.log('@@@ saveSessionData :', JSON.stringify(data));
    try {
      await AsyncStorage.setItem('isFirstTime', 'yes');
      await AsyncStorage.setItem('app_key', data.app_key);
      await AsyncStorage.setItem('token', data.token);
      await AsyncStorage.setItem('conn', data.pre_data.conn);
      await AsyncStorage.setItem('subAgentMsisdn', data.pre_data.sub_agent_msisdn);
      await AsyncStorage.setItem('imei', data.pre_data.imei);
      await AsyncStorage.setItem('device_model', data.pre_data.deviceModel);
      global.agentMssdn = data.pre_data.conn;
      global.subAgentMsisdn = data.pre_data.sub_agent_msisdn;
      global.imei = data.pre_data.imei;
      global.deviceModel = data.pre_data.deviceModel;
      console.log('tileData :: ', await AsyncStorage.getItem('isFirstTime'));
      console.log('app_key  :: ', await AsyncStorage.getItem('app_key'));
      console.log('token    :: ', await AsyncStorage.getItem('token'));
      console.log('conn     :: ', await AsyncStorage.getItem('conn'));
      console.log('subAgentMsisdn :: ', await AsyncStorage.getItem('subAgentMsisdn'));
      console.log('imei     :: ', await AsyncStorage.getItem('imei'));
      console.log('deviceModel :: ', await AsyncStorage.getItem('device_model'));
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  //check app file permission
  async requestFilePermission() {
    console.log('************** Request WRITE_EXTERNAL_STORAGE Permission');
    try {
      let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      console.log('************** Request WRITE_EXTERNAL_STORAGE Permission 1');

      if (!granted) {
        console.log('************** WRITE_EXTERNAL_STORAGE Permission Not Granted');
        granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('************** You can use the WRITE_EXTERNAL_STORAGE');
        } else {
          console.log('************** WRITE_EXTERNAL_STORAGE permission denied');
        }
      } else {
        console.log('************** WRITE_EXTERNAL_STORAGE permission is already granted');
      }
    } catch (err) {
      console.log('************** Request Permission error');
      //console.warn(err);
    }
  }

  //check app permission
  async requestPermission(permissionName) {
    console.log('********* requestPermission ********* ');
    let permissionType;
    switch (permissionName) {
      case 'READ_PHONE_STATE':
        permissionType = PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE;
        break;
      case 'WRITE_EXTERNAL_STORAGE':
        permissionType = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
        break;
      case 'CAMERA':
        permissionType = PermissionsAndroid.PERMISSIONS.CAMERA;
        break;
      case 'LOCATION':
        permissionType = PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
        break;
      default:
        permissionType = PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE;
        break;
    }
    try {
      let granted = await PermissionsAndroid.check(permissionType);
      console.log(`*** try Request ${permissionName} Permission`);
      if (!granted) {
        console.log(`*** ${permissionName} Permission Not Granted`);
        granted = await PermissionsAndroid.request(permissionType);
        if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
          console.log(`*** You can use the ${permissionName}`);
          return granted;
        }
        console.log(`*** ${permissionName} Permission denied`);
        return granted;
      }
      console.log(`*** ${permissionName} Permission is already granted`);
      return granted;
    } catch (err) {
      console.log(`*** ${permissionName} Request Permission error`);
      return false;
    }
  }

  /**
 * @memberof UTILS
 * @function LogOut
 * @param Navigator
 * logout function
 */
  async onLogout() {
    console.log('xxx onLogout');
    try {
      let isFirstTime = await AsyncStorage.getItem('isFirstTime');
      this.stopHeartBeatService();
      await AsyncStorage.removeItem('isFirstTime');
      await AsyncStorage.removeItem('api_key');
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('conn');
      await AsyncStorage.clear();
      await AsyncStorage.setItem('isFirstTime', isFirstTime);
      console.log(AsyncStorage.getItem('isFirstTime'));
      console.log(AsyncStorage.getItem('api_key'));
      console.log(AsyncStorage.getItem('token'));
      console.log(AsyncStorage.getItem('conn'));
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'DialogRetailerApp.views.LoginScreen',
          title: 'Dialog Sales App'
        },
        animationType: 'none',
        drawer: DtawerConfig,
        appStyle: {
          navBarHeight: 55
        }
      });
    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  //Check version and token at app startup
  async apiStartupCheck(actionName, controllerName, moduleName, payLoadData = {}, successCb, failureCb = null) {
    console.log('@@@@@ apiStartupCheck');
    console.log('@@@@@  Request : ', payLoadData);
    console.log('#@@@@@@@ Utills::apiStartupCheck => payLoadData', payLoadData);
    console.log(`#@@@@@@@ Utills::apiStartupCheck :: actionName : ${actionName} => Request PayLoad : \n`, JSON.stringify(payLoadData));
    console.log('##### URL :: apiStartupCheck ####', this.createUrl(moduleName, controllerName, actionName));
    await axios({
      method: 'post',
      timeout: TIME_OUT,
      url: this.createUrl(moduleName, controllerName, actionName),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      },

      data: payLoadData
    }).then((response) => {
      console.log('Success :: apiStartupCheck');
      console.log(response);
      console.log('#@@@@@@@================== response.data :: stringify :: apiStartupCheck =====#');
      console.log(`#@@@@@@@ apiStartupCheck :: actionName : ${actionName} => response.data : \n\n`, JSON.stringify(response.data));
      console.log('#@@@@@@@================== response.data :: apiStartupCheck ==================#');
      if (response.data.success) {
        successCb(response);
      } else if (failureCb == null) {
        this.showAlert(response.data.error ? response.data.error : this.getSystemError(payLoadData.lang));
      } else {
        failureCb(response.data);
      }
    }).catch((error) => {
      console.log('Fail :: apiStartupCheck :: actionName :', actionName);
      console.log(`#@@@@@@@ Utills::apiStartupCheck :: actionName : ${actionName} => Fail :: \n`, error);
      if (failureCb == null) {
        error.request !== undefined && (error.response || error.request._timedOut)
          ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
          : this.showAlert(error.message);
      } else {
        error.request !== undefined && (error.response || error.request._timedOut)
          ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
          : this.showAlert(error.message);
        failureCb(error);
      }
    });
  }


  async apiRequestPost(actionName, controllerName, moduleName, payLoadDataOb = {}, successCb, failureCb = null, exCb = null) {
    console.log('#################### apiRequestPost ################');
    const persistenceData = await this.getPersistenceData();
    console.log('1 @@@@@@@ Utills::apiRequestPost => payLoadDataOb', payLoadDataOb);
    console.log('2 @@@@@@@ Utills::apiRequestPost => persistenceData', persistenceData);
    const deviceData = await this.getDeviceInfo();
    let additionalParams;
    if (persistenceData !== null) {
      additionalParams = update(deviceData, { $merge: persistenceData });
    } else {
      additionalParams = deviceData;
    }
    const payLoadData = update(payLoadDataOb, { $merge: additionalParams });
    console.log('3 @@@@@@@ Utills::apiRequestPost => payLoadData', payLoadData);
    console.log('##### URL ####', this.createUrl(moduleName, controllerName, actionName));
    console.log(`4 @@@@@@@ Utills::apiRequestPost :: actionName : ${actionName} => Request PayLoad : \n`, JSON.stringify(payLoadData));

    const networkConnection = await NetConnection.checkNetworkConnectivityFunc();
    console.log('---------------NetworkConnectivity------------------');
    console.log('4.1 @@@@@@@ Utills::apiRequestPost:: networkConnection ', networkConnection);

    if (networkConnection) {
      await axios({
        method: 'post',
        timeout: TIME_OUT,
        url: this.createUrl(moduleName, controllerName, actionName),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: 'application/json'
        },
        data: payLoadData
      }).then((response) => {
        console.log('Success :: apiRequestPost :: actionName :', actionName);
        console.log(response);
        console.log('#@@@@@@@================== response.data :: stringify :: apiRequestPost =====#');
        console.log(`5 @@@@@@@ Utills::apiRequestPost :: actionName : ${actionName} => response.data : \n\n`, JSON.stringify(response.data));
        console.log('#@@@@@@@================== response.data :: apiRequestPost ==================#');
        if (response.data.force_logout) {
          this.onLogoOutMessage(response.data.message);
        } else if (response.data.success) {
          successCb(response);
        } else if (failureCb == null) {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : this.createUrl(moduleName, controllerName, actionName), 
            error_code : FormatUtils.getFirst100Characters(response.data.error),  
            description : 'False response from server', 
          });
          this.showAlert(response.data.error ? response.data.error : this.getSystemError(payLoadData.lang));
        } else {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : this.createUrl(moduleName, controllerName, actionName), 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          failureCb(response);
        }
      }).catch((error) => {
        Analytics.logFirebaseEvent('api_failure', {
          end_point : this.createUrl(moduleName, controllerName, actionName), 
          error_code : FormatUtils.getFirst100Characters(error), 
          description : 'Exception', 
        });
        console.log('Fail :: apiRequestPost :: actionName :', actionName);
        console.log(`#@@@@@@@ Utills::apiRequestPost :: actionName : ${actionName} => Fail :: \n`, error);
        if (exCb == null) {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
            : this.showAlert(error.message);
        } else {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? exCb(this.getSystemError(payLoadData.lang))
            : (exCb)(error.message);
        }
      });
    } else {
      Analytics.logFirebaseEvent('api_failure', {
        end_point : this.createUrl(moduleName, controllerName, actionName), 
        error_code : 'NETWORK_ERROR', 
        description : 'Network connection issue', 
      });
      if (exCb == null) {
        this.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      } else {
        exCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      }
    }
  }

  async apiRequestGet(actionName, controllerName, moduleName, payLoadData = {}, successCb, failureCb = null, exCb = null) {
    console.log('#################### apiRequestGet ################');
    const deviceData = await this.getDeviceInfo();
    let additionalParams;
    if (payLoadData !== null) {
      additionalParams = update(deviceData, { $merge: payLoadData });
    } else {
      additionalParams = deviceData;
    }
    console.log('#@@@@@@@ Utills::apiRequestGet => payLoadData', payLoadData);
    console.log('##### URL ####', this.createUrl(moduleName, controllerName, actionName));
    console.log(`#@@@@@@@ Utills::apiRequestGet :: actionName : ${actionName} => Request PayLoad : \n`, JSON.stringify(payLoadData));

    const networkConnection = await NetConnection.checkNetworkConnectivityFunc();
    console.log('#@@@@@@@ Utills::apiRequestGet :: NetworkConnectivity ', networkConnection);

    if (networkConnection) {
      await axios({
        method: 'get',
        timeout: TIME_OUT,
        url: this.createUrl(moduleName, controllerName, actionName),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: 'application/json'
        },
        params: additionalParams

      }).then((response) => {
        console.log('Success :: apiRequestGet :: actionName :', actionName);
        console.log(response);
        console.log('#@@@@@@@================== response.data :: stringify :: apiRequestGet =====#');
        console.log(`#@@@@@@@ Utills::apiRequestGet :: actionName : ${actionName} => response.data : \n\n`, JSON.stringify(response.data));
        console.log('#@@@@@@@================== response.data :: apiRequestGet ==================#');
        if (response.data.force_logout) {
          this.onLogoOutMessage(response.data.message);

        } else if (response.data.success) {
          successCb(response);

        } else if (failureCb == null) {
          this.showAlert(response.data.error ? response.data.error : this.getSystemError(payLoadData.lang));
        } else {
          failureCb(response);
        }
      }).catch((error) => {
        Analytics.logFirebaseEvent('api_failure', {
          end_point : this.createUrl(moduleName, controllerName, actionName), 
          error_code : FormatUtils.getFirst100Characters(error), 
          description : 'Exception', 
        });
        console.log('Fail :: apiRequestGet :: actionName :', actionName);
        console.log(`#@@@@@@@ Utills::apiRequestGet :: actionName : ${actionName} => Fail :: \n`, error);
        if (exCb == null) {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
            : this.showAlert(error.message);
        } else {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? exCb(this.getSystemError(payLoadData.lang))
            : exCb(error.message);
        }
      });

    } else {
      Analytics.logFirebaseEvent('api_failure', {
        end_point : this.createUrl(moduleName, controllerName, actionName), 
        error_code : 'NETWORK_ERROR', 
        description : 'Network connection issue', 
      });
      if (exCb == null) {
        this.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      } else {
        exCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      }
    }
  }

  async apiRequestPost2(actionName, controllerName, moduleName, payLoadDataOb = {}, successCb, failureCb = null, exCb = null, withHttps = true, url = null) {
    console.log('#################### apiRequestPost2 ################');
    const deviceData = await this.getDeviceInfo();
    const persistenceData = await this.getPersistenceData();
    let additionalParams;
    if (persistenceData !== null) {
      additionalParams = update(deviceData, { $merge: persistenceData });
    } else {
      additionalParams = deviceData;
    }
    console.log('1 @@@@@@@ Utills::apiRequestPost2 => payLoadDataOb', payLoadDataOb);
    console.log('2 @@@@@@@ Utills::apiRequestPost2 => deviceData', deviceData);
    let apiUrl = (url != null) ? url : this.createUrl(moduleName, controllerName, actionName);
    if (!withHttps) {
      apiUrl = this.convertToHttpUrl(apiUrl);
    }
    const payLoadData = update(payLoadDataOb, { $merge: additionalParams });
    console.log('3 @@@@@@@ Utills::apiRequestPost2 => payLoadData', payLoadData);
    console.log('##### URL ####', apiUrl);
    console.log(`4 @@@@@@@ Utills::apiRequestPost2 :: actionName : ${actionName} => Request PayLoad : \n`, JSON.stringify(payLoadData));
    const networkConnection = await NetConnection.checkNetworkConnectivityFunc();
    console.log(`@@@@@@@ Utills::apiRequestPost2 :: actionName : ${actionName} => NetworkConnectivity: `, networkConnection);
    if (networkConnection) {
      await axios({
        method: 'post',
        timeout: TIME_OUT,
        url: apiUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: 'application/json'
        },
        data: payLoadData
      }).then((response) => {
        console.log('Success :: apiRequestPost2 :: actionName :', actionName);
        console.log(response);
        console.log('#@@@@@@@================== response.data :: stringify :: apiRequestPost2 =====#');
        console.log(`5 @@@@@@@ Utills::apiRequestPost2 :: actionName : ${actionName} => response.data : \n\n`, JSON.stringify(response.data));
        console.log('#@@@@@@@================== response.data :: apiRequestPost2 ==================#');
        if (response.data.force_logout) {
          this.onLogoOutMessage(response.data.message);

        } else if (response.data.success) {
          successCb(response);

        } else if (failureCb == null) {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : apiUrl, 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          this.showAlert(response.data.error ? response.data.error : this.getSystemError(payLoadData.lang));
        } else {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : apiUrl, 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          failureCb(response);
        }
      }).catch((error) => {
        Analytics.logFirebaseEvent('api_failure', {
          end_point : apiUrl, 
          error_code : FormatUtils.getFirst100Characters(error), 
          description : 'Exception', 
        });
        console.log('Fail :: apiRequestPost2 :: actionName :', actionName);
        console.log(`#@@@@@@@ Utills::apiRequestPost2 :: actionName : ${actionName} => Fail :: \n`, error);
        if (exCb == null) {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
            : this.showAlert(error.message);
        } else {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? exCb(this.getSystemError(payLoadData.lang))
            : exCb(error.message);
        }
      });

    } else {
      Analytics.logFirebaseEvent('api_failure', {
        end_point : apiUrl, 
        error_code : 'NETWORK_ERROR', 
        description : 'Network connection issue', 
      });
      if (exCb == null) {
        this.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      } else {
        exCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      }
    }
  }

  /**
   * @description General method for API Post request
   * @param {String} actionName
   * @param {String} controllerName
   * @param {String} moduleName
   * @param {Object} [payLoadDataOb={}]
   * @param {Object} successCb
   * @param {fn} [failureCb=null]
   * @param {fn} [exCb=null]
   * @param {boolean} [withHttps=true]
   * @param {String} [url=null]
   * @memberof Utills
   */
  async apiRequestPostGeneral(actionName, controllerName, moduleName, payLoadDataOb = {}, successCb, failureCb = null, handleFalseResponse = false, withHttps = true, url = null) {
    console.log('#################### apiRequestPostGeneral ################');
    const deviceData = await this.getDeviceInfo();
    const persistenceData = await this.getPersistenceData();
    let additionalParams;
    if (persistenceData !== null) {
      additionalParams = update(deviceData, { $merge: persistenceData });
    } else {
      additionalParams = deviceData;
    }
    console.log('1 @@@@@@@ Utills::apiRequestPostGeneral => payLoadDataOb', payLoadDataOb);
    console.log('2 @@@@@@@ Utills::apiRequestPostGeneral => deviceData', deviceData);
    let apiUrl = (url != null) ? url : this.createUrl(moduleName, controllerName, actionName);
    if (!withHttps) {
      apiUrl = this.convertToHttpUrl(apiUrl);
    }
    const payLoadData = update(payLoadDataOb, { $merge: additionalParams });
    console.log('3 @@@@@@@ Utills::apiRequestPostGeneral => payLoadData', payLoadData);
    console.log('##### URL ####', apiUrl);
    console.log(`4 @@@@@@@ Utills::apiRequestPostGeneral :: actionName : ${actionName} => Request PayLoad : \n`, JSON.stringify(payLoadData));
    const networkConnection = await NetConnection.checkNetworkConnectivityFunc();
    console.log(`@@@@@@@ Utills::apiRequestPost2 :: actionName : ${actionName} => NetworkConnectivity: `, networkConnection);
    if (networkConnection) {
      await axios({
        method: 'post',
        timeout: TIME_OUT,
        url: apiUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: 'application/json'
        },
        data: payLoadData
      }).then((response) => {
        console.log('5 @@@@@@@ Success :: apiRequestPostGeneral :: actionName :', actionName);
        console.log(response);
        console.log(`6 @@@@@@@ Utills::apiRequestPostGeneral :: actionName : ${actionName} => response.data : \n`, response.data);
        console.log('#@@@@@@@================== response.data :: stringify :: apiRequestPostGeneral =====#');
        console.log(`7 @@@@@@@ Utills::apiRequestPostGeneral :: actionName : ${actionName} => response.data : \n\n`, JSON.stringify(response.data));
        console.log('#@@@@@@@================== response.data :: apiRequestPostGeneral ==================#');
        if (response.data.force_logout) {
          this.onLogoOutMessage(response.data.message);
        } else if (response.data.success) {
          successCb(response.data);
        } else if (!response.data.success && handleFalseResponse) {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : apiUrl, 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          successCb(response.data);
        } else if (failureCb !== null) {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : apiUrl, 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          this.failureCb(response.data);
        } else {
          Analytics.logFirebaseEvent('api_failure', {
            end_point : apiUrl, 
            error_code : FormatUtils.getFirst100Characters(response.data.error), 
            description : 'False response from server', 
          });
          this.showAlert(response.data.error ? response.data.error : this.getSystemError(payLoadData.lang));
        }
      }).catch((error) => {
        Analytics.logFirebaseEvent('api_failure', {
          end_point : apiUrl, 
          error_code : FormatUtils.getFirst100Characters(error), 
          description : 'Exception', 
        });
        console.log('Fail :: apiRequestPostGeneral :: actionName :', actionName);
        console.log(`#@@@@@@@ Utills::apiRequestPostGeneral :: actionName : ${actionName} => Fail :: \n`, error);
        if (failureCb == null) {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? this.showAlertMsg(this.getSystemError(payLoadData.lang))
            : this.showAlert(error.message);
        } else {
          error.request !== undefined && (error.response || error.request._timedOut)
            ? failureCb(this.getSystemError(payLoadData.lang))
            : failureCb(error.message);
        }
      });
    } else {
      Analytics.logFirebaseEvent('api_failure', {
        end_point : apiUrl, 
        error_code : 'NETWORK_ERROR', 
        description : 'Network connection issue', 
      });
      if (failureCb == null) {
        this.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      } else {
        failureCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
      }
    }
  }

  async executeExceptionCallback(error, failureCb, payLoadData, actionName) {
    console.log(`6 @@@@@@@ Utills :: executeExceptionCallback  :: actionName :: ${actionName} => error - \n`, error);
    let error_message = this.getSystemError(payLoadData.lang);
    try {
      if (!_.isNil(error.request) && (error.response || error.request._timedOut)) {
        error_message = this.getSystemError(payLoadData.lang);
      } else if (error == 'Network Error') {
        error_message = MessageUtils.getNetworkErrorMessage(payLoadData.lang);
      }

      if (_.isNil(failureCb)) {
        failureCb(error_message);
      } else {
        this.showAlert(error_message);
      }

    } catch (error) {
      this.showAlert(error_message);
    }
  }

  getErrorMessage(message, lang) {
    console.log('Utills :: getErrorMessage message,lang ', message, lang);
    if (lang == 'en') {
      return ERROR_MESSAGES.en.message;
    } else if (lang == 'si') {
      return ERROR_MESSAGES.si.message;
    } else if (lang == 'ta') {
      return ERROR_MESSAGES.ta.message;
    } else {
      return ERROR_MESSAGES.en.message;
    }

  }

  getSystemError(lang) {
    console.log('Utills :: getErrorMessage message,lang ', lang);
    if (lang == 'en') {
      return ERROR_MESSAGES.en.SYSTEM_ERROR;
    } else if (lang == 'si') {
      return ERROR_MESSAGES.si.SYSTEM_ERROR;
    } else if (lang == 'ta') {
      return ERROR_MESSAGES.ta.SYSTEM_ERROR;
    } else {
      return ERROR_MESSAGES.en.SYSTEM_ERROR;
    }
  }

  async getDeviceAndUserData() {
    console.log('Utills :: getDeviceAndUserData');
    let deviceAndUserData;
    const deviceData = await this.getDeviceInfo();
    const persistenceData = await this.getPersistenceData();

    if (persistenceData !== null && persistenceData !== undefined) {
      deviceAndUserData = update(deviceData, { $merge: persistenceData });   
    } else {
      deviceAndUserData = deviceData;
    }
    console.log('Utills :: getDeviceAndUserData :: deviceAndUserData =>', deviceAndUserData);
    return deviceAndUserData;
  }

  /**
   * @description Common method to show modal
   * @param {Object} screen
   * @param {Object} [passProps={}]
   * @param {boolean} [overrideBackPress=true]
   * @memberof Utills
   */
  showModalView(screen, passProps = {}, overrideBackPress = true) {
    console.log("xxx Utills :: showModalView :", screen);
    console.log("screen title :", screen.title);
    console.log("screen id :", screen.id);
    console.log("screen passProps :", passProps);
    Navigation.showModal({
      title: screen.title,
      screen: screen.id,
      overrideBackPress: overrideBackPress,
      passProps: passProps
    });
  }

  nicValidate(nic) {
    let imnicsi;
    if (typeof imnicsi !== 'string') {
      imnicsi = new String(nic);
      if (nic.length === 10 || nic.length === 12) {
        const reg = /^([1-9][0-9]{8}[vVxX])|([1-9][0-9]{6}[0-9]{5})|([2][0-9]{11})$/;

        if (reg.test(nic)) {
          console.log(nic.length);
          if (nic.length === 12) {
            let digits = imnicsi.substr(4, 3);
            let myInt = parseInt(digits);
            if (imnicsi.substr(0, 1) == 1 || imnicsi.substr(0, 1) == 2) {
              if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
                console.log('xxx validNIC');
                return nic.toLocaleUpperCase();
              }
            }
            return false;
          }
          if (nic.length === 10) {
            let digits = imnicsi.substr(2, 3);
            let myInt = parseInt(digits);
            if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
              console.log('xxx validNIC');
              return nic.toLocaleUpperCase();
            }
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  mobileNumValidate(num) {
    return /^(94|0)?[7][0-9]{8}$/.test(num.toString())
      ? num
      : false;
  }

  mobileNumValidateDialog(num) {
    return /^[0]?[7](7|6)[0-9]{7}$/.test(num.toString())
      ? num
      : false;
  }

  dtvNumValidate(connRef, regexp = undefined) {
    if (typeof connRef !== 'string') {
      connRef = new String(connRef);
    }

    if (regexp == undefined){
      if (
        (connRef.length === 8) && ((connRef.substr(0, 2) === '60') || (connRef.substr(0, 2) === '80'))
      ) {
        return connRef;
      }
    } else {
      if (regexp !== ""){
        let verificationCodeRegex = new RegExp(regexp);
        if (verificationCodeRegex.test(connRef)) {
          return connRef;
        }
      }
    }
    return false;
  }

  lteNumValidate(num) {
    if (typeof num !== 'string') {
      num = new String(num);
    }
    const reg = new RegExp('^(94|0)?[12]14[0-9]{6}');
    if (reg.test(num)) {
      return num.slice(-9);
    }
    console.log('lte_validation settings not valid');
    return false;
  }

  getStringifyText(text) {
    console.log('Utills :: getStringifyText => text :', text);
    let stringifiedText = '';
    if ((typeof text === 'string' || text instanceof String) && !_.isNil(text) && text !== '') {
      stringifiedText = text;
    } else {
      stringifiedText = this.getSystemError(this.getLang());
    }
    console.log('Utills :: getStringifyText => text :: after format =>', stringifiedText);
    return stringifiedText;
  }

  onLogoOutMessage = async (msg, title = '') => {
    console.log('### Utills :: onLogoOutMessage');
    if (await this.didUserLogout() == false) {
      console.log('### Utills :: SHOW ALERT');
      Alert.alert(
        title,
        this.getStringifyText(msg),
        [
          { text: 'OK', onPress: () => this.onLogout() },
        ],
        { cancelable: false }
      );
    } else {
      console.log('### Utills :: DO NOTHING');
    }
  }


  async didUserLogout() {
    console.log('### Utills :: didUserLogout');
    const token = await AsyncStorage.getItem('token');
    const allowedServices = await AsyncStorage.getItem('allowedServices');
    if (token == null || allowedServices == null) {
      console.log('### Utills :: didUserLogout TRUE');
      return true;
    } else {
      console.log('### Utills :: didUserLogout FALSE');
      return false;
    }
  }


  stopHeartBeatService() {
    console.log('### Utills :: stopHeartBeatService');
    NativeModules.GeoLocationService.stopService();
  }


  showAlert(msg, title = '') {
    console.log('### Utills :: showAlert - ', msg);
    if (msg !== undefined && msg !== null) {
      let message = this.getStringifyText(msg);
      console.log('Utills :: showAlert => message : ', message);
      Alert.alert(title, message);
    } else {
      Alert.alert(title, this.getSystemError(this.getLang()));
    }
  }

  showAlertMsg(msg, title = '') {
    console.log('### Utills :: showAlertMsg - ', msg);
    Alert.alert(title, this.getStringifyText(msg));
  }

  dismissModal = () => {
    Navigation.dismissModal({ animationType: 'slide-down' });
  };

  showAlertModal(data, voucherCode) {
    let passProps = {
      primaryText: "OK",
      disabled: false,
      hideTopImageView: false,
      primaryButtonStyle: { color: Colors.colorRed },
      icon: Images.icons.FailureAlert,
      descriptionTitle: `Voucher code : ${voucherCode}`,
      description: data.error,
      primaryPress: () => this.dismissModal()
    };

    Navigation.showModal({
      title: 'CommonModalAlert',
      screen: 'DialogRetailerApp.modals.CommonModalAlert',
      passProps: passProps
    });
  }

  createUrl(moduleName, controllerName, actionName) {
    return `${baseUrl + moduleName}/${controllerName}/${actionName}`;
  }

  convertToHttpUrl(url) {
    return url.replace(/^https:/, 'http:');
  }


  createApiUrl(actionName, controllerName, moduleName) {
    return `${baseUrl + moduleName}/${controllerName}/${actionName}`;
  }

  createLoginApiUrl(moduleName, controllerName, actionName) {
    return `${baseUrlLogin + moduleName}/${controllerName}/${actionName}`;
  }

  /**
   * @description Get button color
   * @param {*} status
   * @param {*} [activeColor=null]
   * @returns 
   * @memberof Utills
   */
  getButtonTextColor(status, activeColor = null) {
    console.log('xxx Utills :: getButtonTextColor => ', status);
    let color = activeColor;
    if (activeColor == null) {
      color = { color: Colors.gettingStartedButtonTextColor };
    }

    if (!status) {
      color = { color: Colors.btnDisableTxtColor };
    }
    return color;
  }

  numberMask(number) {
    const a = number.toString();
    const mask = 'XXX';
    let prefixNo;
    if (number.length === 9) {
      prefixNo = a.slice(0, 2);
    } else {
      prefixNo = a.slice(0, 3);
    }
    return (`${prefixNo}${mask}${a.slice(-4)}`);
  }

  numberMaskOtp(number) {
    const a = number.toString();
    const mask = 'XXX';
    let prefixNo;
    if (number.length === 9) {
      prefixNo = `0${a.slice(0, 2)}`;
    } else {
      prefixNo = a.slice(0, 3);
    }
    return (`${prefixNo}${mask}${a.slice(-4)}`);
  }

  fullNumberMask(number) {
    const a = number.toString();
    const mask = 'XXXXXXX';
    let prefixNo;
    if (number.length === 9) {
      prefixNo = a.slice(0, 2);
    } else {
      prefixNo = a.slice(0, 3);
    }
    return (`${prefixNo}${mask}`);
  }

  validateNumberRange(number, max, min = 0) {
    const value = parseInt(number);
    return (!isNaN(value) && value >= min && value <= max);
  }

  //
  checkNumber(number) {
    console.log('xxx checkNumber :: number', number);
    if (number !== undefined && number !== null) {
      let numberString = number.toString().replace(/,/g, '');
      if (numberString !== "" && !isNaN(numberString)) {
        return parseFloat(numberString).toFixed(2);
      } else {
        return 0.00;
      }
     
    } else {
      return 0.00;
    }
  }
  
  /**
   * @description Get Corrected Number from comma separated monetary value
   * @memberof Utills
   */
  getCorrectedNumber = (number) => {
    console.log('xxx Utills :: getCorrectedNumber => ', number);
    if (number !== undefined && number !== null) {
      let numberString = number.toString().replace(/,/g, '');
      if (numberString !== "" && !isNaN(numberString)) {
        return parseFloat(numberString).toFixed(2);
      } else {
        return 0.00;
      }
    } else {
      return 0.00;
    }
  };

  getKeyValueArray(objectVal) {
    console.log('xxx getKeyValueArray');
    let workOrderDetails = [];
    for (let prop in objectVal) {
      if (!objectVal.hasOwnProperty(prop)) {
        continue;
      }
      //console.log('xxxx getKeyValueArray', prop, objectVal[prop]);
      const rOb = {
        key: prop,
        value: objectVal[prop]
      };
      workOrderDetails.push(rOb);

    }
    return workOrderDetails;
  }

  gsmDialogNumvalidate(num) {
    console.log('xxx gsmDialogNumvalidate', num.toString()[0]);
    const compairNo = num
      .toString()
      .length == 10 && num.toString()[0] == 0
      ? (num.slice(-9)).toString()
      : (num).toString();
    console.log('xxx gsmDialogNumvalidate', compairNo);
    return /^[0]?[7](7|6)[0-9]{7}$/.test(compairNo);
  }

  gsmDigitValidation(num) {
    console.log('xxx gsmDigitValidation', num.length);
    let returnValue = false;
    if (num.toString()[0] == 0 && num.length == 10) {
      returnValue = true;
    } else if (num.toString()[0] == 0 && num.length == 9) {
      returnValue = false;
    } else if (num.length == 9 || num.length == 10) {
      returnValue = true;
    } else {
      returnValue = false;
    }
    return returnValue;
  }

  toFirstLetterUpperCase = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  getMobileNumberLengthLimit(number) {
    console.log('xxx Utills :: getMobileNumberLengthLimit => ', number);
    if (number[0] == 0) {
      return 10;
    }
    return 9;
  }

  /**
   * @description A custom debounce method to throttle user input in Search Bar
   * @param {Function} fn
   * @param {Number} delay
   * @returns
   * @memberof ActivationStatusMainView
   */
  debounce = (fn, delay) => {
    var timer = null;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    };
  }

  validateDigit(digit) {
    console.log('xxx Utills :: validateDigit => ', digit);
    return !isNaN(parseInt(digit));
  }

  validateNumberFormat(number) {
    console.log('xxx Utills :: validateNumberFormat => ', number);
    return /^[0-9]*$/.test(number.toString());
  }

  alphaNumericValidation(text) {
    console.log('xxx Utills :: alphaNumericValidation => ', text);
    return /^[a-zA-Z0-9]*$/.test(text.toString());
  }

  alphaNumericValidationWithSpace(text) {
    console.log('xxx Utills :: alphaNumericValidation => ', text);
    return /^[a-zA-Z0-9\s]*$/.test(text.toString());
  }

  addressValidation(text) {
    console.log('xxx Utills :: addressValidation => ', text);
    //const RegExp = /^[a-zA-Z0-9_\s ]*$/;
    const RegExp = /^[#.0-9a-zA-Z\s,-_]+$/;
    return RegExp.test(text.toString());
  }


  nameValidation(text) {
    console.log('xxx Utills :: nameValidation => ', text);
    return /^[a-zA-Z ]{2,100}$/.test(text.toString());
  }

  mobileNumberValidateDialog(num) {
    console.log('xxx Utills :: mobileNumberValidateDialog => ', num);
    return /^(94|0)?[7](7|6)[0-9]{7}$/.test(num.toString());
  }

  mobileNumberValidateAny(num) {
    console.log('xxx Utills :: mobileNumberValidateAny => ', num);
    return /^(94|0)?[7][0-9]{8}$/.test(num.toString());
  }

  landlineNumberValidation(text) {
    const regEx = /0((11)|(2(1|[3-7]))|(3[1-8])|(4(1|5|7))|(5(1|2|4|5|7))|(6(3|[5-7]))|([8-9]1))([2-4]|5|7|9)[0-9]{6}/;
    return regEx.test(text);
  }

  emailValidation(text) {
    console.log('xxx Utills :: emailValidation => ', text);
    const regEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regEx.test(text);
  }


  nicNumberValidate(number, needRegexMatch = true) {
    console.log('xxx nicNumberValidate :: ', number);
    if (needRegexMatch) {
      const NIC_VALIDATE_REGEX = /^((([1-9][0-9]{8})[vVxX]{1})|([12][0-9]{11}))$/;
      if (!NIC_VALIDATE_REGEX.test(number)) {
        console.log('xxx nicNumberValidate => Rexex NIC :: fail');
        return false;
      }
    }

    let nicString;
    if (typeof number !== 'string') {
      nicString = new String(number);
    } else {
      nicString = number;
    }
    if (number.length === 12) {
      let digits = nicString.substr(4, 3);
      let myInt = parseInt(digits);
      if (nicString.substr(0, 1) == 1 || nicString.substr(0, 1) == 2) {
        if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
          console.log('xxx nicNumberValidate => New NIC :: success');
          return true;
        }
      }
      console.log('xxx nicNumberValidate => New NIC :: fail');
      return false;
    }
    if (number.length === 10) {
      let digits = nicString.substr(2, 3);
      let myInt = parseInt(digits);
      if ((myInt > 0 && myInt <= 366) || (myInt > 500 && myInt <= 866)) {
        console.log('xxx nicNumberValidate => Old NIC :: success');
        return true;
      }
      console.log('xxx nicNumberValidate => Old NIC :: fail');
      return false;
    }
  }

  /**
   * @description Method to validate NIC number real time
   * @param {*} number
   * @returns 
   * @memberof Utills
   */
  nicNumberRealTimeValidate(number) {
    console.log('xxx Utills :: nicNumberRealTimeValidate : ', number);
    let status = false;
    let showMessage = false;
    const NIC_REAL_TIME_VALIDATE_REGEX = /^(([1-9][0-9]{0,8})|(([1-9][0-9]{8})[vVxX]{1})|([12][0-9]{0,11}))$/;
    const NIC_VALIDATE_REGEX = /^((([1-9][0-9]{8})[vVxX]{1})|([12][0-9]{11}))$/;

    if (!NIC_REAL_TIME_VALIDATE_REGEX.test(number) && number.length != 0) {
      console.log('Basic validation fail');
      showMessage = true;
    } else {
      console.log('Basic validation success');
      if (NIC_VALIDATE_REGEX.test(number)) {
        const needRegexMatch = false;
        if (this.nicNumberValidate(number, needRegexMatch)) {
          console.log('nicNumberValidate success');
          status = true;
        } else {
          console.log('nicNumberValidate fail');
          showMessage = true;
        }
      }
    }
    return { status, showMessage };
  }

  addZeroes(number) {
    // Cast as number
    var num = Number(number);
    // If not a number, return 0
    if (isNaN(num)) {
      return 0;
    }
    // If there is no decimal, or the decimal is less than 2 digits, toFixed
    if (String(num).split(".").length < 2 || String(num).split(".")[1].length <= 2) {
      num = num.toFixed(2);
    }
    // Return the number
    return num;
  }

  round(value) {
    console.log('val ', value);
    console.log('Number(Math.round(value * 100) / 100)', Number(Math.round(value * 100) / 100));
    console.log(this.addZeroes(Number(Math.round(value * 100) / 100)));
    return this.addZeroes(Number(Math.round(value * 100) / 100));
  }

  getCurrentTimeStamp () {
    let timestamp = Math.round((new Date()).getTime() / 1000);
    console.log('xxx Utills :: getCurrentTimeStamp : ', timestamp);
    return timestamp;
  }

  /**
   * @description A custom method to get formatted money string
   * @param {String} prefix
   * @param {Number} number
   * @returns {String}
   * @memberof Utills
   */
  getFormatedMoneyValue = (prefix, number) => {
    console.log('xxx Utills :: getFormatedMoneyValue : ', number);
  }

  numberWithCommas = (num) => {
    let x = this.round(num);
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

  /**
   * @description Method to validate Passport number real time
   * @param {*} number
   * @returns 
   * @memberof Utills
   */
  passportNumberRealTimeValidate(number) {
    console.log('xxx Utills :: passportNumberRealTimeValidate : ', number);
    let status = false;
    let showMessage = false;
    const PP_REAL_TIME_VALIDATE_REGEX = /^[0-9a-zA-Z]*$/;

    if (!PP_REAL_TIME_VALIDATE_REGEX.test(number) && number.length != 0) {
      console.log('Basic validation fail');
      showMessage = true;
    } else {
      console.log('Basic validation success');
      if (number.length >= 4) {
        status = true;
      }
    }
    return { status, showMessage };
  }

  //CFSS related Utills goes here
  async saveData(func, key, value) {
    try {
      switch (func) {
        case 'SAVE': await AsyncStorage.setItem(key, JSON.stringify(value));
          // console.log(`storaged value: ${JSON.parse(AsyncStorage.getItem(key))}`);
          break;
        case 'DELETE': await AsyncStorage.removeItem(key);
          break;
        case 'GET': await AsyncStorage.getItem(key);
          break;
        default: console.log('No Async function found');
      }

    } catch (error) {
      console.log(`AsyncStorage error: ${error.message}`);
    }
  }

  getPermissionStatus(area) {
    return new Promise((res, rej) => {
      Permissions.request(area).then(response => {
        console.log('getPermissionStatus ', response);
        if (response == 'authorized') {
          res(response);
        } else if (response == 'restricted' || response == 'denied') {
          rej(response);
        }
      });
    });
  }

  getGPSLocation() {
    const { REJECTED, DATA_ERROR, TIMEOUT, DEFAULT, NO_PERMISSION } = Constants.LOCATION_ERROR_CODES;

    return new Promise(async (res, rjct) => {
      let locationPermission = await this.requestPermission('LOCATION');

      if (locationPermission || locationPermission == PermissionsAndroid.RESULTS.GRANTED) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log("lat_long Successfully acquired user location", position);
            res(position.coords);
          },
          (error) => {
            console.log('lat_long error ', error);
            let errorMsg = '';
            switch (error.code) {
              case 1: errorMsg = REJECTED;
                break;
              case 2: errorMsg = DATA_ERROR;
                break;
              case 3: errorMsg = TIMEOUT;
                break;
              default: errorMsg = DEFAULT;
            }

            rjct({ message: errorMsg });
          }
        );
      } else {
        console.log('lat_long permission ', locationPermission);
        rjct({ message: NO_PERMISSION });
      }
    });
  }

  getWOMUserLocation(lang) {
    return new Promise((res, rjct) => {
      this.getGPSLocation()
        .then((location) => {
          res(location);
        }).catch((err) => {
          let message = '';
          message = this.getLocationAlertMsg(err, lang);
          rjct({ status: false, message });
        });
    });
  }

  getLocationAlertMsg(err, lang) {
    let message = '';

    if (err.message) {
      if (err.message == Constants.LOCATION_ERROR_CODES.NO_PERMISSION) {
        message = LOCATION_ALERT_MESSAGES(lang).askLocationPermission;
      } else if (err.message == Constants.LOCATION_ERROR_CODES.TIMEOUT) {
        message = LOCATION_ALERT_MESSAGES(lang).askHighAccuracyLocation;
      } else {
        message = LOCATION_ALERT_MESSAGES(lang).enableLocationService;
      }
    } else {
      message = LOCATION_ALERT_MESSAGES(lang).enableLocationService;
    }

    return message;
  }

  onMapsPress(item, localTxt) {
    console.log("xxx Utills :: onMapsPress");
    let url = "";
    console.log(item);
    // let srcLat= this.state.myPosition.latitude;
    // let srcLong = this.state.myPosition.longitude;
    // item.lat = '6.9227005';
    // item.lng = '79.8607591';
    if (item.lat != "" && item.lng != "") {
      url = 'http://maps.google.com/maps?daddr=' + item.lat + ',' + item.lng + '&directionsmode=driving';
    } else {
      let address = "";
      for (let index = 0; index < item.address.length; index++) {
        if (item.address[index] != "" && item.address[index] != null) {
          address = address + " " + item.address[index];
        }
      }
      if (address != "") {
        url = 'https://www.google.com/maps/dir/?api=1&destination=' + encodeURI(address);
      }
    }
    if (url != "") {
      Alert.alert('', localTxt, [
        {
          text: 'Cancel',
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => {
            Linking.openURL(url);

          }
        }
      ], { cancelable: true });
    }

  }


  appFlowManegement(app_screen, propsData, pageTitle) {
    console.log('xxx Utills :: appScreen accessed.>>>>>>>>>>>>>>>>>>>>>>>>' + app_screen);
    let navigateTo = '';
    let props = {};
    let title;
    switch (app_screen) {
      case 'VIEW_PROVISION_STATUS':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.DTVProvisionStatus';
        props = {
          screenTitle: 'IN-PROGRESS WORKORDER',
          prov_data: propsData.prov_data,
          prov_config: propsData.prov_config
        };
        break;

      case 'VIEW_SERIAL_LIST':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.DTVInstallation';
        props = {
          serialData: propsData.serial_data,
          card_option: propsData.card_option
        };
        break;

      case 'VIEW_EQUIPMENT_NO':
        this.props;
        navigateTo = 'DialogRetailerApp.views.InitialTRBView';
        title = pageTitle.inProgressTitle;
        props = {
          displayNoEqp: true
        };
        break;

      case 'VIEW_ROOT_CAUSE':
        title = 'IN-PROGRESS WORKORDER';
        navigateTo = 'DialogRetailerApp.views.RtCrsFrmScreen';
        props = {
          rootCauses: propsData.root_cause,
        };
        break;

      case 'VIEW_FEEDBACK':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.CustomerFeedback';
        props = {
          feedbackOptions: propsData.feedback_question,
        };
        break;

      case 'VIEW_FEEDBACK_SIQNATURE':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.CustomerFeedback';
        props = {
          feedbackOptions: propsData.feedback_question,
          enableSignaturePad: true
        };
        break;

      case 'VIEW_BREACHED':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.WomSlaBreachScreen';
        props = {
          lte_job_id: propsData.lte_job_id ? propsData.lte_job_id : null,
          lte_order_id: propsData.lte_order_id ? propsData.lte_order_id : null,
          data: propsData.delay_reason,
          reason_id: propsData.reason_id ? propsData.reason_id : null,
          sub_reason_id: propsData.sub_reason_id ? propsData.sub_reason_id : null
        };
        break;

      case 'VIEW_HYBRID_SELECTION':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.DTVHybridScreen';
        props = {
          rejectdata: propsData.reject_reason,
          hybridData: propsData.hybrid,
          prod_list: propsData.prod_list,
        };
        break;

      //For Removal navigation
      case 'VIEW_REMOVAL_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RemovalStatusScreen';
        props = {
          appFlow: 'DTV_REMOVAL',
          failureReasons: propsData.failure_reasons
        };
        break;

      //For Relocation Recovery navigation
      case 'VIEW_RECOVERY_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RecoveryItemsScreen';
        props = {
          //appFlow: 'DTV_RECOVERY',
          warrantyDetails: propsData.warranty_details
        };
        break;

      //Dialog Own Stb upgrade
      case 'VIEW_FEATURE_HD_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.StbDeliveryDialogOwnedScreen';
        props = {
          stbDetails: propsData.warranty_details
        };
        break;

      // Customer Own Stb upgrade
      case 'VIEW_FEATURE_HD_CX_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.stbDeliveryCustomerOwnedScreen';
        props = {
          stbDetails: propsData.warranty_details,
          card_option: propsData.card_option
        };
        break;

      //For Relocation Refix navigation
      case 'VIEW_RELOCATION_REFIX_INITIAL_AGAIN':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RefixItemsScreen';
        props = {
          //appFlow: 'DTV_REFIX',
          isProvisioned: propsData.prov_status,
          PmsId: propsData.PmsId,
          warrantyDetails: propsData.warranty_details,
        };
        break;

      case 'VIEW_RELOCATION_REFIX_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RefixItemsScreen';
        props = {
          //appFlow: 'DTV_REFIX',
          isProvisioned: propsData.prov_status,
          PmsId: propsData.PmsId,
          warrantyDetails: propsData.warranty_details
        };
        break;

      //For Normal Refix navigation
      case 'VIEW_REFIX_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RefixItemsScreen';
        props = {
          //appFlow: 'DTV_REFIX',
          isProvisioned: propsData.prov_status,
          warrantyDetails: propsData.warranty_details
        };
        break;

      //For accessory delivery navigation - NOT IN USE
      case 'VIEW_ACCESSORY_DELV_CX_INITIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.DTVInstallation';
        props = {
          //appFlow: 'ACCESSORY_DELV_CX',
          serialData: propsData.warranty_details
        };
        break;
      case 'VIEW_SIM_STB':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.SIMSTBChangeScreen';
        props = {
          propsData: propsData,
          changeType: propsData.change_type,
          serialData: propsData.warranty_details
        };
        break;
      case 'VIEW_COMMON_SERIAL':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RemovalItemsScreen';
        props = {
          warranty_details: propsData.warranty_details
        };
        break;
      case 'VIEW_REJECT':
        title = pageTitle.inProgressTitle;
        navigateTo = 'DialogRetailerApp.views.RejectIndex';
        props = {
          rejectArray: propsData.reject_reason,
          RejectResonprop: true
        };
        break;     
      default:
        navigateTo = 'DialogRetailerApp.views.WomDetailView';
    }
    return { title: title, screen: navigateTo, passProps: props };
  }



  render() {
    return (null);
  }
}

export default Utills;
