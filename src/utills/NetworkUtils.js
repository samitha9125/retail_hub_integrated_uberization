/*
 * File: NetworkUtils.js
 * Project: Dialog Sales App
 * File Created: Thursday, 7th February 2019 11:18:57 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 7th February 2019 5:15:54 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2019 Omobio (PVT) Ltd
 */

import { NetInfo } from 'react-native';

/**
 * @description CMethod to get network connection status (online or offline)
 * @memberof NetworkUtils
 */
const getNetworkConnectivity = async() => {
  console.log('### FNetworkUtils :: getNetworkConnectivity');
  let connection_status = false;
  await NetInfo.isConnected.fetch().then(isConnected => {
    console.log('### FNetworkUtils :: ' + (isConnected ? 'online' : 'offline'));
    connection_status = isConnected;
  });
  console.log('### FNetworkUtils :: getNetworkConnectivity => ',connection_status);
  return connection_status;
};


export default {
  getNetworkConnectivity,
};
