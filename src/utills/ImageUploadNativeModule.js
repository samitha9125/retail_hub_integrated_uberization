/*
 * File: ImageUploadNativeModule.js
 * Project: Dialog Sales App
 * File Created: Friday, 14th December 2018 1:45:01 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Friday, 14th December 2018 1:46:03 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { NativeModules } from 'react-native';

const { RNReactNativeImageuploader } = NativeModules;

export default RNReactNativeImageuploader;
