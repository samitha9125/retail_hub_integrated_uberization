/*
 * File: DeviceUtils.js
 * Project: Dialog Sales App
 * File Created: Thursday, 11th October 2018 11:06:32 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 11th October 2018 11:06:42 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { NativeModules, NetInfo } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import RNSimData from 'react-native-sim-data';
import Global from './../config/globalConfig';
import { AccountUtils } from './index';

//get SIM details
const getSimInfo = async () => {
  const simData = await RNSimData.getSimInfo();
  console.log('@@@ DeviceUtils :: getSimInfo', simData);
  return simData;
};
//get device details
const getDeviceInfo = async () => {
  console.log('@@@ DeviceUtils :: getDeviceInfo');
  let uniqueID = '';
  let isUserLogged = true;
  let deviceRef = '';
  let imsi = '';
  let imei = '';
  let imeiNumber = '';
  let appVersion = '';
  let versionNumber = '';
  let versionCode = '';
  let systemName = '';
  let systemVersion = '';
  let deviceName = '';
  let deviceModel = '';
  let manufacturer = '';
  let deviceMemory = '';
  let diskCapacity = '';
  let cameraResolution = '';
  let freeDiskCapacity = '';
  let carrier = '';
  let connectionType = '';
  let cellularType = '';
  let simSerialNumber = '';
  let simInfo = '';

  try {
    isUserLogged = await !AccountUtils.didUserLogout();
    await NativeModules.IMEI.getIMEI().then(result => {
      if (result && result.length > 0) {
        imei = result;
        deviceRef = imei;
        imeiNumber = imei;
        console.log('imei  success:', imei);
      }
    });

    await NativeModules.IMEI.getIMSI().then(result => {
      if (result && result.length > 0) {
        imsi = result;
        console.log('imsi  success:', imei);

      }
    });

    await NativeModules.IMEI.getSimSerialNumber().then(result => {
      if (result && result.length > 0) {
        simSerialNumber = result;
        console.log('simSerialNumber  success:', imei);

      }
    });

    await NativeModules.IMEI.getCameraRes().then(result => {
      console.log('getCameraRes');
      console.log('getCameraRes', result);
      if (result) {
        cameraResolution = result;
        console.log('CameraRes  success:', result);

      } else {
        cameraResolution = '';
      }
    });
  } catch (error) {
    console.log(`imei, imsi, simSerialNumber => error: ${error.message}`);
  }

  await NetInfo.getConnectionInfo().then((connectionInfo) => {
    console.log('connectionType: ' + connectionInfo.type + ', EffectiveConnectionType: ' + connectionInfo.effectiveType);
    connectionType = connectionInfo.type;
    cellularType = connectionInfo.effectiveType;
  });

  try {

    const info = DeviceInfo;
    const simInfoOb = await getSimInfo();
    uniqueID = info.getUniqueID();
    //conn = info.getPhoneNumber();
    appVersion = info.getVersion();
    versionNumber = Global.versionNumber;
    versionCode = Global.versionCode;
    systemName = info.getSystemName();
    systemVersion = info.getSystemVersion();
    deviceName = DeviceInfo.getBrand();
    deviceModel = DeviceInfo.getModel();
    manufacturer = info.getManufacturer();
    deviceMemory = DeviceInfo.getTotalMemory();
    diskCapacity = DeviceInfo.getTotalDiskCapacity();
    freeDiskCapacity = DeviceInfo.getFreeDiskStorage();
    carrier = info.getCarrier();
    simInfo = simInfoOb;
  } catch (error) {
    console.log(`getDeviceInfo error: ${error.message}`);
  }
  return {
    uniqueID,
    isUserLogged,
    deviceRef,
    imsi,
    imei,
    imeiNumber,
    appVersion,
    versionNumber,
    versionCode,
    systemName,
    systemVersion,
    deviceName,
    deviceModel,
    manufacturer,
    diskCapacity,
    deviceMemory,
    freeDiskCapacity,
    carrier,
    connectionType,
    cellularType,
    simSerialNumber,
    cameraResolution,
    simInfo,
  };
};

export default {
  getDeviceInfo,
  getSimInfo,
};
