/*
 * File: PermissionUtils.js
 * Project: Dialog Sales App
 * File Created: Thursday, 11th October 2018 6:31:44 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Thursday, 11th October 2018 6:32:47 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */


import { PermissionsAndroid } from 'react-native';
import { Screen } from './index';


/**
 * @description Common method to request permission
 * @param {String} permissionName
 * @param {String} [screen]
 * @memberof PermissionUtils
 */
const requestPermission = async (permissionName, screen ) => {
  console.log(`### PermissionUtils :: requestPermission ${permissionName} , screen  ${screen}`);
 
};

/**
 * @description Common method to request camera permission
 * @param {String} [screen]
 * @param {String} [message]
 * @memberof PermissionUtils
 */
const requestCameraPermission = async (screen, message='Please grant camera permission to use this service' ) => {
  console.log('### PermissionUtils :: requestCameraPermission - screen :', screen);
  try {
    let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
    console.log('### PermissionUtils :: Request CAMERA Permission L1');
    if (!granted) {
      console.log('### PermissionUtils :: CAMERA Permission Not Granted L2');
      granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('### PermissionUtils :: CAMERA Permission granted L3');
      } else {
        console.log('### PermissionUtils :: CAMERA Permission denied L4');
        //await Screen.showDialogBox(message, requestCameraPermission(screen, message));
        await Screen.showAlert(message);
      }
    } else {
      console.log('### PermissionUtils :: CAMERA Permission already granted L5');
    }
  } catch (err) {
    console.log('### PermissionUtils :: CAMERA Permission error L6', err);
    //await Screen.showDialogBox(message, requestCameraPermission(screen, message));
    await Screen.showAlert(message);
  }
 
};

/**
 * @description Common method to request camera permission
 * @param {String} [screen]
 * @param {String} [message]
 * @memberof PermissionUtils
 */
const requestStoragePermission = async (screen, message='Please grant camera permission to use this service') => {
  console.log('### PermissionUtils :: requestStoragePermission - screen :', screen);
  try {
    let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    console.log('************** Request WRITE_EXTERNAL_STORAGE Permission L1');
    if (!granted) {
      console.log('************** WRITE_EXTERNAL_STORAGE Permission Not Granted L2');
      granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('************** WRITE_EXTERNAL_STORAGE Permission granted L3');
      } else {
        console.log('************** WRITE_EXTERNAL_STORAGE Permission denied L4');
        await Screen.showAlert(message);
        //await requestStoragePermission(screen, message);
      }
    } else {
      console.log('************** WRITE_EXTERNAL_STORAGE Permission already granted L5');
    }
  } catch (err) {
    console.log('************** WRITE_EXTERNAL_STORAGE Permission error L6', err);
    await Screen.showAlert(message);
    //await requestStoragePermission(screen, message);
  }
 
};

export default {
  requestPermission,
  requestCameraPermission,
  requestStoragePermission,
};
