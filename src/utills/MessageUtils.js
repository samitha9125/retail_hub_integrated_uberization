/*
 * File: MessageUtils.js
 * Project: Dialog Sales App
 * File Created: Monday, 10th December 2018 3:20:32 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 10th December 2018 3:21:00 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import _ from 'lodash';
import LocalStorageUtils from './LocalStorageUtils';

const ERROR_MESSAGES = {
  en: {
    SYSTEM_ERROR: 'System error. Please try again',
    NETWORK_ERROR: 'Network connection issue, Please try again'
  },
  si: {
    SYSTEM_ERROR: 'පද්ධති දෝෂයකි. කරුණාකර නැවත උත්සාහ කරන්න',
    NETWORK_ERROR: 'ජාල දෝෂයකි. කරුණාකර නැවත උත්සාහ කරන්න'
  },
  ta: {
    SYSTEM_ERROR: 'கணினி பிழை. தயவு செய்து மீண்டும் முயற்சிக்கவும்',
    NETWORK_ERROR: 'நெட்வொர்க் பிழை. தயவு செய்து மீண்டும் முயற்சிக்கவும்'
  }
};

const NET_ERROR = 'Network Error';

const MESSAGES = {
  en: {
    OK: 'OK',
    CANCEL: 'CANCEL'
  },
  si: {
    OK: 'හරි',
    CANCEL: 'අවලංගු කරන්න'
  },
  ta: {
    OK: 'சரி',
    CANCEL: 'ரத்து செய்க'
  }
};

const getSystemErrorMessage = (lang) => {
  console.log('MessageUtils :: getSystemErrorMessage ', lang);
  let language = lang;
  if (_.isNil(lang)) {
    language = LocalStorageUtils.getGlobalLanguage();
    console.log('MessageUtils :: getSystemErrorMessage - LocalStorage lang ', language);
  }
  if (language == 'en') {
    return ERROR_MESSAGES.en.SYSTEM_ERROR;
  } else if (language == 'si') {
    return ERROR_MESSAGES.si.SYSTEM_ERROR;
  } else if (language == 'ta') {
    return ERROR_MESSAGES.ta.SYSTEM_ERROR;
  } else {
    return ERROR_MESSAGES.en.SYSTEM_ERROR;
  }
};

const getNetworkErrorMessage = (lang) => {
  console.log('MessageUtils :: getNetworkErrorMessage ', lang);
  let language = lang;
  if (_.isNil(lang)) {
    language = LocalStorageUtils.getGlobalLanguage();
    console.log('MessageUtils :: getNetworkErrorMessage - LocalStorage lang ', language);
  }
  if (language == 'en') {
    return ERROR_MESSAGES.en.NETWORK_ERROR;
  } else if (language == 'si') {
    return ERROR_MESSAGES.si.NETWORK_ERROR;
  } else if (language == 'ta') {
    return ERROR_MESSAGES.ta.NETWORK_ERROR;
  } else {
    return ERROR_MESSAGES.en.NETWORK_ERROR;
  }
};

const getStringifyText = (text) => {
  console.log('MessageUtils :: getStringifyText => text :', text);
  let stringifiedText = '';
  if ((typeof text === 'string' || text instanceof String) && !_.isNil(text) && text !== '') {
    stringifiedText = text;
  } else {
    stringifiedText = getSystemErrorMessage(LocalStorageUtils.getGlobalLanguage());
  }
  console.log('MessageUtils :: getStringifyText => text :: after format =>', stringifiedText);
  return stringifiedText;
};


const getOkText = (lang) => {
  console.log('MessageUtils :: getOkText ', lang);
  let language = lang;
  if (_.isNil(lang)) {
    language = LocalStorageUtils.getGlobalLanguage();
    console.log('MessageUtils :: getOkText - LocalStorage lang ', language);
  }
  if (language == 'en') {
    return MESSAGES.en.OK;
  } else if (language == 'si') {
    return MESSAGES.si.OK;
  } else if (language == 'ta') {
    return MESSAGES.ta.OK;
  } else {
    return MESSAGES.en.OK;
  }
};

const getCancelText = (lang) => {
  console.log('MessageUtils :: getCancelText ', lang);
  let language = lang;
  if (_.isNil(lang)) {
    language = LocalStorageUtils.getGlobalLanguage();
    console.log('MessageUtils :: getCancelText - LocalStorage lang ', language);
  }
  if (language == 'en') {
    return MESSAGES.en.CANCEL;
  } else if (language == 'si') {
    return MESSAGES.si.CANCEL;
  } else if (language == 'ta') {
    return MESSAGES.ta.CANCEL;
  } else {
    return MESSAGES.en.CANCEL;
  }
};

const isNetworkErrorMessage = (msg, lang) => {
  console.log('MessageUtils :: isNetworkErrorMessage ', msg, lang);
  let language = lang;
  let status = false;
  if (_.isNil(lang)) {
    language = LocalStorageUtils.getGlobalLanguage();
    console.log('MessageUtils :: isNetworkErrorMessage - LocalStorage lang ', language);
  }
  if (language == 'en') {
    console.log('MessageUtils :: isNetworkErrorMessage - en ', msg);
    status = ERROR_MESSAGES.en.NETWORK_ERROR == msg || NET_ERROR == msg;
  } else if (language == 'si') {
    console.log('MessageUtils :: isNetworkErrorMessage - si ', msg);
    status = ERROR_MESSAGES.si.NETWORK_ERROR == msg || NET_ERROR == msg;
  } else if (language == 'ta') {
    console.log('MessageUtils :: isNetworkErrorMessage - ta ', msg);
    status = ERROR_MESSAGES.ta.NETWORK_ERROR == msg || NET_ERROR == msg;
  } else {
    console.log('MessageUtils :: isNetworkErrorMessage - en_other ', msg);
    status = ERROR_MESSAGES.en.NETWORK_ERROR == msg || NET_ERROR == msg;
  }
  console.log('MessageUtils :: isNetworkErrorMessage - status =>', status);
  return status;
};

const getFormatedErrorMessage = (message, lang = null) => {
  console.log('MessageUtils :: getFormatedErrorMessage => message : ', message);
  let formated_message = '';
  try {
    if (_.isNil(message)) {
      formated_message = getSystemErrorMessage(lang);
      console.log('MessageUtils :: getFormatedErrorMessage => 1');
    } else if (_.isObject(message)) {
      if (message.error) {
        formated_message = message.error;
        console.log('MessageUtils :: getFormatedErrorMessage => 2.1');
      } else if (message.message) {
        formated_message = message.message;
        console.log('MessageUtils :: getFormatedErrorMessage => 2.2');
      } else {
        formated_message = getSystemErrorMessage(lang);
        console.log('MessageUtils :: getFormatedErrorMessage => 2.3');
      }
    } else if (typeof message === 'string' || message instanceof String && message != '') {
      if (message == NET_ERROR) {
        formated_message = getNetworkErrorMessage(lang);
        console.log('MessageUtils :: getFormatedErrorMessage => 3.1');
      } else {
        formated_message = message;
        console.log('MessageUtils :: getFormatedErrorMessage => 3.2');
      }
    } else {
      formated_message = getSystemErrorMessage(lang);
      console.log('MessageUtils :: getFormatedErrorMessage => 4');
    }
  } catch (error) {
    formated_message = getSystemErrorMessage(lang);
    console.log('MessageUtils :: getFormatedErrorMessage => 5');
  }
  console.log('MessageUtils :: getFormatedErrorMessage => formated_message : ', message);
  return formated_message;
};

const getExceptionMessage = (message, lang = null) => {
  console.log('MessageUtils :: getExceptionMessage => message : ', message);
  let formated_message = '';
  try {
    if (_.isNil(message)) {
      formated_message = getSystemErrorMessage(lang);
      console.log('MessageUtils :: getExceptionMessage => 1');
    } else if (_.isObject(message)) {
      if (message.error) {
        formated_message = message.error;
        console.log('MessageUtils :: getExceptionMessage => 2.1');
      } else if (message.message) {
        formated_message = message.message;
        console.log('MessageUtils :: getExceptionMessage => 2.2');
      } else {
        formated_message = getSystemErrorMessage(lang);
        console.log('MessageUtils :: getExceptionMessage => 2.3');
      }
    } else if (typeof message === 'string' || message instanceof String && message != '') {
      let isNetworkError = isNetworkErrorMessage(message);
      console.log('MessageUtils :: getExceptionMessage => message == isNetworkErrorMessage', message, isNetworkErrorMessage);
      if (isNetworkError) {
        formated_message = getNetworkErrorMessage(lang);
        console.log('MessageUtils :: getExceptionMessage => 3.1');
      } else {
        formated_message = getSystemErrorMessage(lang);
        console.log('MessageUtils :: getExceptionMessage => 3.2');
      }
    } else {
      formated_message = getSystemErrorMessage(lang);
      console.log('MessageUtils :: getExceptionMessage => 4');
    }
  } catch (error) {
    formated_message = getSystemErrorMessage(lang);
    console.log('MessageUtils :: getExceptionMessage => 5');
  }
  console.log('MessageUtils :: getExceptionMessage => formated_message : ', message);
  return formated_message;
};



export default {
  getNetworkErrorMessage,
  getSystemErrorMessage,
  getStringifyText,
  isNetworkErrorMessage,
  getOkText,
  getCancelText,
  getFormatedErrorMessage,
  getExceptionMessage,
};
