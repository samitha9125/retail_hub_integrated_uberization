/*
 * File: LocalStorageUtils.js
 * Project: Dialog Sales App
 * File Created: Monday, 15th October 2018 1:12:00 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 15th October 2018 1:12:07 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import { AsyncStorage } from 'react-native';
import update from 'immutability-helper';
import _ from 'lodash';
import { DeviceUtils } from './index';

const storeItem = async (key, item) => {
  console.log('### LocalStorageUtils :: storeItem');
  try {
    var item1 = await AsyncStorage.setItem(key, JSON.stringify(item));
    return item1;
  } catch (error) {
    console.log(`### LocalStorageUtils :: storeItem :: AsyncStorage error: ${error.message}`);
  }
};

const retrieveItem = async (key) => {
  console.log('### LocalStorageUtils :: retrieveItem');
  try {
    const retrievedItem = await AsyncStorage.getItem(key);
    const item = JSON.parse(retrievedItem);
    return item;
  } catch (error) {
    console.log(`### LocalStorageUtils :: retrieveItem :: AsyncStorage error: ${error.message}`);
  }
  return;
};

const getPersistenceData = async () => {
  console.log('### LocalStorageUtils :: getPersistenceData');
  let persistenceData = '';
  try {
    persistenceData = await AsyncStorage.getItem('persistenceData');
    if (persistenceData !== null) {
      persistenceData = await JSON.parse(persistenceData);
      console.log('getPersistenceData :: persistenceData ::\n', persistenceData);
    }
  } catch (error) {
    console.log(`### LocalStorageUtils :: getPersistenceData :: AsyncStorage error: ${error.message}`);
  }

  return persistenceData;
};

const getDeviceAndUserData = async () => {
  console.log('### LocalStorageUtils :: getDeviceAndUserData');
  let deviceAndUserData;
  const deviceData = await DeviceUtils.getDeviceInfo();
  const persistenceData = await getPersistenceData();

  if (persistenceData !== null && persistenceData !== undefined) {
    deviceAndUserData = update(deviceData, { $merge: persistenceData });
  } else {
    deviceAndUserData = deviceData;
  }
  console.log('### LocalStorageUtils :: getDeviceAndUserData :: deviceAndUserData =>', deviceAndUserData);
  return deviceAndUserData;
};

const savePersistenceData = async (data) => {
  console.log('### LocalStorageUtils :: saveSessionData :: data - Before', data);
  console.log('### LocalStorageUtils :: savePersistenceData ', JSON.stringify(data));
  try {
    await AsyncStorage.setItem('persistenceData', JSON.stringify(data));
    console.log('persistenceData ::\n', await AsyncStorage.getItem('persistenceData'));
  } catch (error) {
    console.log(`### LocalStorageUtils :: savePersistenceData :: AsyncStorage error:${error.message}`);
  }
};

const saveSessionData = async (data) => {
  console.log('### LocalStorageUtils :: saveSessionData :: data ', JSON.stringify(data));
  try {
    await AsyncStorage.setItem('isFirstTime', 'yes');
    await AsyncStorage.setItem('app_key', data.app_key);
    await AsyncStorage.setItem('token', data.token);
    await AsyncStorage.setItem('conn', data.pre_data.conn);
    await AsyncStorage.setItem('subAgentMsisdn', data.pre_data.sub_agent_msisdn);
    await AsyncStorage.setItem('imei', data.pre_data.imei);
    await AsyncStorage.setItem('device_model', data.pre_data.deviceModel);
    global.agentMssdn = data.pre_data.conn;
    global.subAgentMsisdn = data.pre_data.sub_agent_msisdn;
    global.imei = data.pre_data.imei;
    global.deviceModel = data.pre_data.deviceModel;
    console.log('tileData :: ', await AsyncStorage.getItem('isFirstTime'));
    console.log('app_key  :: ', await AsyncStorage.getItem('app_key'));
    console.log('token    :: ', await AsyncStorage.getItem('token'));
    console.log('conn     :: ', await AsyncStorage.getItem('conn'));
    console.log('subAgentMsisdn :: ', await AsyncStorage.getItem('subAgentMsisdn'));
    console.log('imei     :: ', await AsyncStorage.getItem('imei'));
    console.log('deviceModel :: ', await AsyncStorage.getItem('device_model'));
  } catch (error) {
    console.log(`### LocalStorageUtils :: saveSessionData :: AsyncStorage error: ${error.message}`);
  }
};

const saveAllowedServices = async (data) => {
  console.log('### LocalStorageUtils :: saveAllowedServices :: data', JSON.stringify(data));
  try {
    await AsyncStorage.setItem('allowedServices', JSON.stringify(data));
    console.log('allowedServices ::\n', await AsyncStorage.getItem('allowedServices'));
  } catch (error) {
    console.log(`### LocalStorageUtils :: saveAllowedServices :: AsyncStorage error: ${error.message}`);
  }
};

/**
 * @description Save user dat after login
 * @param {Object} requestData 
 */
const saveUserDataAfterLogin = (response) => {
  console.log('### LocalStorageUtils :: saveUserDataAfterLogin - ', response);
  savePersistenceData(response.data.pre_data);
  try {
    let lang = response.data.pre_data.lang;
    let langIndex = response.data.pre_data.langIndex;
    setLanguage(lang ? lang : 'en', langIndex ? langIndex : 0);
  } catch (e) {
    console.log('### LocalStorageUtils :: saveUserDataAfterLogin :: login persistance data save error - ', e);
  }
  saveAllowedServices(response.data.tileData);
  saveTileData(response.data.tileData);
  saveSessionData(response.data);

};

const saveTileData = async (data) => {
  console.log('### LocalStorageUtils :: saveTileData :: data', JSON.stringify(data));
  try {
    await AsyncStorage.setItem('tileData', JSON.stringify(data));
    console.log('tileData ::\n', await AsyncStorage.getItem('tileData'));
  } catch (error) {
    console.log(`### LocalStorageUtils :: saveTileData :: AsyncStorage error: ${error.message}`);
  }
};


const setLanguage = async (lang, langIndex) => {
  console.log('### LocalStorageUtils :: setLanguage :: ', lang, langIndex);
  try {
    await AsyncStorage.setItem('lang', lang);
    await AsyncStorage.setItem('langIndex', langIndex.toString());
    global.lang = lang;
    console.log(`getLang: ${await AsyncStorage.getItem('lang')}`);
    console.log(`getLang: ${await AsyncStorage.getItem('langIndex')}`);
  } catch (error) {
    console.log(`### LocalStorageUtils :: setLanguage :: AsyncStorage error: ${error.message}`);
  }
};

const savePersistenceLanguage = async (lang) => {
  console.log('### LocalStorageUtils :: savePersistenceLanguage :: ', lang);
  try {
    let persistenceData = await getPersistenceData();
    console.log('### LocalStorageUtils :: savePersistenceLanguage :: persistenceData ', persistenceData);
    if (_.isNil(persistenceData)) {
      persistenceData = {
        lang: lang
      };
    } else {
      persistenceData.lang = lang;
    }
    await savePersistenceData(persistenceData);
    global.lang = lang;
  } catch (error) {
    console.log(`### LocalStorageUtils :: savePersistenceLanguage :: AsyncStorage error: ${error.message}`);
  }
};

const saveBannerData = async (data) => {
  console.log('### LocalStorageUtils :: saveBannerData :: data', JSON.stringify(data));
  try {
    await AsyncStorage.setItem('bannerData', JSON.stringify(data));
    console.log('bannerData ::\n', await AsyncStorage.getItem('bannerData'));
  } catch (error) {
    console.log(`### LocalStorageUtils :: saveBannerData :: AsyncStorage error: ${error.message}`);
  }
};

const getPersistenceLanguage = async () => {
  console.log('### LocalStorageUtils :: getPersistenceLanguage');
  let currentLanguage = 'en';
  try {
    let persistenceData = await getPersistenceData();
    console.log('### LocalStorageUtils :: getPersistenceLanguage :: persistenceData ', persistenceData);
    currentLanguage = !_.isNil(persistenceData) ? persistenceData.lang : 'en';
  } catch (error) {
    console.log(`### LocalStorageUtils :: getPersistenceLanguage => AsyncStorage error: ${error.message}`);
  }
  return currentLanguage;
};

const getLanguage = async () => {
  console.log('### LocalStorageUtils :: getLanguage');
  let lang = 'en';
  try {
    lang = await AsyncStorage.getItem('lang');
    console.log(`### LocalStorageUtils :: getLanguage' : ${lang}`);
  } catch (error) {
    console.log(`### LocalStorageUtils :: getLanguage' => AsyncStorage error: ${error.message}`);
  }
  return lang;
};


const getGlobalLanguage = () => {
  console.log('### LocalStorageUtils :: getGlobalLanguage - ', global.lang);
  return global.lang;
};

const getToken = async () => {
  console.log('### LocalStorageUtils :: getToken');
  const token = await AsyncStorage.getItem('token');
  console.log('### LocalStorageUtils :: getToken =>', token);
  return token;
};
const getAllowedServices = async () => {
  console.log('### LocalStorageUtils :: getAllowedServices');
  const allowedServices = await AsyncStorage.getItem('allowedServices');
  console.log('### LocalStorageUtils :: getAllowedServices =>', allowedServices);
  return allowedServices;
};

const getIsCFSSAgent = async () => {
  console.log('### LocalStorageUtils :: getIsCFSSAgent');
  const isCFSSAgent = await AsyncStorage.getItem('isCFSSAgent');
  console.log('### LocalStorageUtils :: getIsCFSSAgent =>', isCFSSAgent);
  return isCFSSAgent;
};

const getTileData = async () => {
  console.log('### LocalStorageUtils :: getTileData');
  const tileData = await AsyncStorage.getItem('tileData');
  console.log('### LocalStorageUtils :: getTileData =>', tileData);
  return tileData;
};

const getBannerData = async () => {
  console.log('### LocalStorageUtils :: getBannerData');
  const bannerData = await AsyncStorage.getItem('bannerData');
  console.log('### LocalStorageUtils :: getBannerData =>', bannerData);
  return bannerData;
};

export default {
  storeItem,
  retrieveItem,
  getPersistenceData,
  savePersistenceData,
  saveSessionData,
  saveAllowedServices,
  saveTileData,
  saveBannerData,
  setLanguage,
  getLanguage,
  getGlobalLanguage,
  getDeviceAndUserData,
  saveUserDataAfterLogin,
  getPersistenceLanguage,
  savePersistenceLanguage,
  getToken,
  getAllowedServices,
  getIsCFSSAgent,
  getBannerData,
  getTileData,
};
