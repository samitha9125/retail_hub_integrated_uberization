/*
 * File: FuncUtils.js
 * Project: Dialog Sales App
 * File Created: Monday, 29th October 2018 8:22:26 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Monday, 29th October 2018 8:23:27 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import _ from 'lodash';

/**
 * @description Common method to iterate through an array and find availability
 * @param {Array} [largeArray}
 * @param {Array} [smallArray}
 * @memberof FuncUtils
 */
const findArray = (largeArray, smallArray) => {
  console.log('### FuncUtils :: findArray : largeArray, smallArray - ', largeArray, smallArray);
  var status = true;
  for (var i = 0; i < largeArray.length; i++) {
    if (smallArray.indexOf(largeArray[i]) !== -1) {
      status = false;
      break;
    }
  }
  return status;
};

/**
 * @description Common method to validate alpha
 * @param {String} [text}
 * @memberof FuncUtils
 */
const alphaValidation = (text) => {
  console.log('### FuncUtils :: alphaValidation -', text);
  return /^[a-zA-Z]*$/.test(text.toString());
};


/**
 * @description Common method to validate name
 * @param {String} [text}
 * @memberof FuncUtils
 */
const nameValidation = (text) => {
  console.log('### FuncUtils :: nameValidation -', text);
  return /^[a-zA-Z\s]*$/.test(text.toString());
};

/**
 * @description Common method to validate alpha numeric value
 * @param {String} [text}
 * @memberof FuncUtils
 */
const alphaNumericValidation = (text) => {
  console.log('### FuncUtils :: alphaNumericValidation -', text);
  return /^[a-zA-Z0-9]*$/.test(text.toString());
};

/**
 * @description Common method to validate alpha numeric value with space
 * @param {String} [text}
 * @memberof FuncUtils
 */
const alphaNumericValidationWithSpace = (text) => {
  console.log('### FuncUtils :: alphaNumericValidationWithSpace -', text);
  return /^[a-zA-Z0-9\s]*$/.test(text.toString());
};

/**
 * @description Common method to validate address
 * @param {String} [text}
 * @memberof FuncUtils
 */
const addressValidation = (text) => {
  console.log('### FuncUtils :: addressValidation -', text);
  //const RegExp = /^[a-zA-Z0-9_\s ]*$/;
  const RegExp = /^[#.0-9a-zA-Z\s,-_]+$/;
  return RegExp.test(text.toString());
};


/**
 * @description Common method to get extension type of file or url
 * @param {String} [text}
 * @memberof FuncUtils
 */
const getFileExtensionType = (text) => {
  console.log('### FuncUtils :: getFileExtensionType -', text);
  //return text.replace(/^.*\./, '');
  return  text.substr((text.lastIndexOf('.') + 1)).toLowerCase();
};

/**
 * @description Common method to get value safe way
 * @param {Any} [content}
 * @memberof FuncUtils
 */
const getValueSafe = (content) => {
  console.log('### FuncUtils :: getValueSafe -', content);
  let safe_value = '';
  try {
    safe_value = !_.isNil(content) ? content : '';
  } catch (error) {
    console.log('### FuncUtils :: getValueSafe - error', error);
  }
  return safe_value;
};


/**
 * @description Common method to get length of a array safe way
 * @param {Array} [array}
 * @memberof FuncUtils
 */
const getArraySize = (array) => {
  console.log('### FuncUtils :: getArraySize -', array);
  let array_size = 0;
  try {
    array_size = !_.isNil(array) ? array.length : 0 ;
  } catch (error) {
    console.log('### FuncUtils :: array_size - error', error);
  }
  return array_size;
};


/**
 * @description Common method to get value of a array safe way
 * @param {Array} [array}
 * @memberof FuncUtils
 */
const getArray = (array) => {
  console.log('### FuncUtils :: getArray -', array);
  let array_value = [];
  try {
    array_value = !_.isNil(array) ? array : [];
  } catch (error) {
    console.log('### FuncUtils :: getArray - error', error);
  }
  return array_value;
};

/**
 * @description Common method to get value with value.00
 * @param {Number} [Number}
 * @memberof FuncUtils
 */
const getRsValue = (number)=> {
  console.log('### FuncUtils :: getRsValue -', getRsValue);
  if (!_.isNil(number)) {
    let numberString = number.toString().replace(/,/g, '');
    if (numberString !== "" && !isNaN(numberString)) {
      return parseFloat(numberString).toFixed(2);
    } else {
      return 0.00;
    }  
  } else {
    return 0.00;
  }
};

/**
 * @description Common method to get value is null or undefined
 * @param {Any} [value}
 * @memberof FuncUtils
 */
const isNullOrUndefined = (value) => {
  console.log('### FuncUtils :: isNullOrUndefined -', value);
  let status  = false;
  try {
    status = _.isNil(value);
  } catch (error) {
    console.log('### FuncUtils :: isNullOrUndefined - error', error);
  }
  return status;
};

/**
 * @description Common method to get value is null 
 * @param {Any} [value}
 * @memberof FuncUtils
 */
const isNull = (value) => {
  console.log('### FuncUtils :: isNull -', value);
  let status  = false;
  try {
    status = _.isNull(value);
  } catch (error) {
    console.log('### FuncUtils :: isNull - error', error);
  }
  return status;
};

/**
 * @description Common method to get value is undefined  
 * @param {Any} [value}
 * @memberof FuncUtils
 */
const isUndefined = (value) => {
  console.log('### FuncUtils :: isUndefined -', value);
  let status  = false;
  try {
    status = _.isNull(value);
  } catch (error) {
    console.log('### FuncUtils :: isUndefined - error', error);
  }
  return status;
};



export default {
  findArray,
  alphaValidation,
  nameValidation,
  alphaNumericValidation,
  alphaNumericValidationWithSpace,
  addressValidation,
  getFileExtensionType,
  getValueSafe,
  getArraySize,
  getArray,
  getRsValue,
  isNullOrUndefined,
  isUndefined,
  isNull,
};
