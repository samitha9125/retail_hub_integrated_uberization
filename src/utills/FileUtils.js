import RNFS from "react-native-fs";
import { Alert, BackHandler } from "react-native";
import RNReactNativeImageuploader from '@Utils/ImageUploadNativeModule';
import Utills from "./Utills";

const Utill = new Utills();

const downloadFile = (url = "default", version) => {
  console.log("@@ downloadFile");
  console.log("@@ downloadFile :: url :", url);
  console.log("@@ downloadFile :: params :", version);

  let outputTxt = "";
  let imageUri = "";
  let returnData = null;
  const showInstallMsg = imgUri => {
    console.log("@@@ showInstallMsg");
    Alert.alert(
      "",
      "Do you want to inatall the latest version ?",
      [
        {
          text: "Cancel",
          onPress: () => {
            console.log("xx showInstallMsg :: Cancel Pressed");
            BackHandler.exitApp();
          },
          style: "cancel"
        },
        {
          text: "Ok",
          onPress: () => {
            console.log("xx showInstallMsg :: Install APK");
            RNReactNativeImageuploader.installApk(
              imgUri.replace("file://", "")
            );
          }
        }
      ],
      { cancelable: false }
    );
  };

  const progress = data => {
    const percentage = ((100 * data.bytesWritten) / data.contentLength) | 0;
    const text = `Progress ${percentage}%`;
    outputTxt = text;
    console.log(outputTxt);
  };

  const isBackground = false;

  const begin = res => {
    outputTxt = "Download has begun";
    console.log(outputTxt);
    console.log(res);
  };

  const progressDivider = 2;

  // Random file name needed to force refresh... const downloadDest =
  // `${RNFS.DocumentDirectoryPath}/${((Math.random() * 1000) | 0)}.apk`;
  const downloadDest = `${RNFS.PicturesDirectoryPath}/DialogRA-V${version}.apk`;

  const ret = RNFS.downloadFile({
    fromUrl: url,
    toFile: downloadDest,
    begin,
    progress,
    isBackground,
    progressDivider
  });

  ret.promise
    .then(res => {
      outputTxt = JSON.stringify(res);
      imageUri = `file://${downloadDest}`;
      console.log(`file://${downloadDest}`);
      console.log(outputTxt);
      returnData = {
        output: outputTxt,
        imagePath: {
          uri: imageUri
        }
      };

      if (res.statusCode === 200) {
        showInstallMsg(imageUri);
      } else {
        Utill.showAlertMsg("Dialog Retailer App download error..!");
      }

      console.log(returnData);
    })
    .catch(err => {
      console.log(err);
      Utill.showAlertMsg("Dialog Retailer App download error..!");
    });
};

const uploadFile = (url = "default", params = null, fileOb) => {
  console.log("@@ uploadFile");
  console.log("@@ uploadFile :: url :", url);
  console.log("@@ uploadFile :: params :", params);
  const uploadUrl = "http://dialog.lk/XXXXXXX";
  // create an array of objects of the files you want to upload
  /**
     * Example
     * const files = [
        {
            name: 'test1',
            filename: 'test1.w4a',
            filepath: `${RNFS.DocumentDirectoryPath}/test1.w4a`,
            filetype: 'audio/x-m4a'
        }, {
            name: 'test2',
            filename: 'test2.w4a',
            filepath: `${RNFS.DocumentDirectoryPath}/test2.w4a`,
            filetype: 'audio/x-m4a'
        }
    ];
    **/

  const files = [fileOb];

  const uploadBegin = response => {
    const jobId = response.jobId;
    console.log(`UPLOAD HAS BEGUN! JobId: ${jobId}`);
  };

  const uploadProgress = response => {
    const percentage = Math.floor(
      (response.totalBytesSent / response.totalBytesExpectedToSend) * 100
    );
    console.log(`UPLOAD IS ${percentage}% DONE!`);
  };

  // upload files
  RNFS.uploadFiles({
    toUrl: uploadUrl,
    files,
    method: "POST",
    headers: {
      Accept: "application/json"
    },
    fields: {
      hello: "world"
    },
    begin: uploadBegin,
    progress: uploadProgress
  })
    .promise.then(response => {
      if (response.statusCode === 200) {
        console.log("FILES UPLOADED!"); // response.statusCode, response.headers, response.body
      } else {
        console.log("SERVER ERROR");
      }
    })
    .catch(err => {
      if (err.description === "cancelled") {
        // cancelled by user
      }
      console.log(err);
    });
};
const readFile = filePath => {
  RNFS.readFile(filePath, "utf8").then(contents => {
    this.assert("Read File", contents, "File read error");
  });
};

export default {
  downloadFile,
  uploadFile,
  readFile
};
