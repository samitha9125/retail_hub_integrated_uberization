/*
 * File: ApiRequestUtils.js
 * Project: Dialog Sales App
 * File Created: Wednesday, 10th October 2018 7:18:42 pm
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Wednesday, 10th October 2018 7:19:01 pm
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import axios from 'axios';
import update from 'immutability-helper';
import { Screen, DeviceUtils, AuthenticationUtils, LocalStorageUtils, Analytics, FormatUtils, MessageUtils, NetworkUtils } from './index';
import Global from './../config/globalConfig';

const TIME_OUT = 60000;

const baseUrl = Global.baseUrl;

/**
 * @description General method for API Post request
 * @param {String} actionName
 * @param {String} controllerName
 * @param {String} moduleName
 * @param {Object} [payLoadDataOb={}]
 * @param {Object} successCb
 * @param {fn} [failureCb=null]
 * @param {boolean} [handleFalseResponse=true]
 * @param {boolean} [withHttps=true]
 * @param {String} [url=null]
 * @memberof ApiRequestUtils
 */
const apiPost = async (actionName, controllerName, moduleName, payLoadData = {}, successCb, failureCb = null, handleFalseResponse = false, withHttps = true, url = null) => {
  console.log('#################### ApiRequestUtils :: apiPost ################');
  const deviceData = await DeviceUtils.getDeviceInfo();
  const persistenceData = await LocalStorageUtils.getPersistenceData();
  let additionalParams;
  if (persistenceData !== null) {
    additionalParams = update(deviceData, { $merge: persistenceData });
  } else {
    additionalParams = deviceData;
  }
  console.log(`1 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => payLoadData :\n`, payLoadData);
  console.log(`2 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => deviceData :\n`, deviceData);
  let apiUrl = (url != null)
    ? url
    : createUrl(moduleName, controllerName, actionName);
  if (!withHttps) {
    apiUrl = convertToHttpUrl(apiUrl);
  }
  const requestPayLoad = update(payLoadData, { $merge: additionalParams });
  console.log(`3 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => Request :: requestPayLoad :\n`, requestPayLoad);
  console.log(`4 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => ##### URL ####\n`, apiUrl);
  console.log(`5 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => Request :: payLoadData :\n\n`, JSON.stringify(payLoadData));
  console.log(`6 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => Request :: requestPayLoad :\n\n`, JSON.stringify(requestPayLoad));

  const networkConnectivity = await NetworkUtils.getNetworkConnectivity();
  console.log(`6.1 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => NetworkConnectivity : `, networkConnectivity);

  if (networkConnectivity) {
    await axios({
      method: 'post',
      timeout: TIME_OUT,
      url: apiUrl,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      },
      data: requestPayLoad
    }).then((response) => {
      console.log(`7 @@@@@@@ SUCCESS :: ${actionName} => response.data :\n`, response.data);
      console.log(`8 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => response :\n`, response);
      console.log(`9 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => response.data :\n`, response.data);
      console.log(`10 @@@@@@@ ApiRequestUtils::apiPost :: actionName : ${actionName} => response.data :\n\n`, JSON.stringify(response.data));
      if (response.data.force_logout) {
        AuthenticationUtils.forceLogoutAlert(response.data.message);
      } else if (response.data.success) {
        successCb(response.data);
      } else if (!response.data.success && handleFalseResponse) {
        Analytics.logFirebaseEvent('api_failure', {
          end_point: apiUrl,
          error_code: FormatUtils.getFirst100Characters(response.data.error),
          description: 'False response from server',
        });
        successCb(response.data);
      } else if (failureCb !== null) {
        Analytics.logFirebaseEvent('api_failure', {
          end_point: apiUrl,
          error_code: FormatUtils.getFirst100Characters(response.data.error),
          description: 'False response from server',
        });
        failureCb(response.data);
      } else {
        Analytics.logFirebaseEvent('api_failure', {
          end_point: apiUrl,
          error_code: FormatUtils.getFirst100Characters(response.data.error),
          description: 'False response from server',
        });
        Screen.showAlert(response.data.error
          ? response.data.error
          : MessageUtils.getSystemErrorMessage(payLoadData.lang));
      }
    }).catch((error) => {
      Analytics.logFirebaseEvent('api_failure', {
        end_point: apiUrl,
        error_code: FormatUtils.getFirst100Characters(error),
        description: 'Exception',
      });
      console.log(`11 @@@@@@@ FAILED :: ${actionName} => error :\n`, error);
      if (failureCb == null) {
        Screen.showAlert(error.request !== undefined && (error.response || error.request._timedOut)
          ? MessageUtils.getSystemErrorMessage(payLoadData.lang)
          : error.message);
      } else {
        error.request !== undefined && (error.response || error.request._timedOut)
          ? failureCb(MessageUtils.getSystemErrorMessage(payLoadData.lang))
          : failureCb(error.message);
      }
    });
  } else {
    Analytics.logFirebaseEvent('api_failure', {
      end_point: apiUrl,
      error_code: 'NETWORK_ERROR',
      description: 'Network connection issue',
    });
    if (failureCb == null) {
      Screen.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
    } else {
      failureCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
    }
  }
};

/**
 * @description Legacy method for API Post request (Similar to apiPost to in Utills)
 * @param {String} actionName
 * @param {String} controllerName
 * @param {String} moduleName
 * @param {Object} [payLoadDataOb={}]
 * @param {Object} successCb
 * @param {fn} [failureCb=null]
 * @param {fn} [exCb=null]
 * @param {boolean} [withHttps=true]
 * @param {String} [url=null]
 * @memberof ApiRequestUtils
 */
const apiPostLegacy = async (actionName, controllerName, moduleName, payLoadData = {}, successCb, failureCb = null, exCb = null, withHttps = true, url = null) => {
  console.log('#################### ApiRequestUtils :: apiPostLegacy ################');
  const deviceData = await DeviceUtils.getDeviceInfo();
  const persistenceData = await LocalStorageUtils.getPersistenceData();
  let additionalParams;
  if (persistenceData !== null) {
    additionalParams = update(deviceData, { $merge: persistenceData });
  } else {
    additionalParams = deviceData;
  }
  console.log(`1 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => payLoadData :\n`, payLoadData);
  console.log(`2 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => deviceData :\n`, deviceData);
  let apiUrl = (url != null)
    ? url
    : createUrl(moduleName, controllerName, actionName);
  if (!withHttps) {
    apiUrl = convertToHttpUrl(apiUrl);
  }
  const requestPayLoad = update(payLoadData, { $merge: additionalParams });
  console.log(`3 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => Request :: requestPayLoad :\n`, requestPayLoad);
  console.log(`4 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => ##### URL ####\n`, apiUrl);
  console.log(`5 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => Request :: payLoadData :\n\n`, JSON.stringify(payLoadData));
  console.log(`6 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => Request :: requestPayLoad :\n\n`, JSON.stringify(requestPayLoad));

  const networkConnectivity = await NetworkUtils.getNetworkConnectivity();
  console.log(`6.1 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => NetworkConnectivity :`, networkConnectivity);

  if (networkConnectivity) {
    await axios({
      method: 'post',
      timeout: TIME_OUT,
      url: apiUrl,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      },
      data: requestPayLoad
    }).then((response) => {
      console.log(`7 @@@@@@@ SUCCESS :: ${actionName} => response.data :\n`, response.data);
      console.log(`8 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => response :\n`, response);
      console.log(`9 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => response.data :\n`, response.data);
      console.log(`10 @@@@@@@ ApiRequestUtils::apiPostLegacy :: actionName : ${actionName} => response.data :\n\n`, JSON.stringify(response.data));

      if (response.data.force_logout) {
        AuthenticationUtils.forceLogoutAlert(response.data.message);
      } else if (response.data.success) {
        successCb(response);
      } else if (failureCb !== null) {
        Analytics.logFirebaseEvent('api_failure', {
          end_point: apiUrl,
          error_code: FormatUtils.getFirst100Characters(response.data.error),
          description: 'False response from server',
        });
        failureCb(response);
      } else {
        Analytics.logFirebaseEvent('api_failure', {
          end_point: apiUrl,
          error_code: FormatUtils.getFirst100Characters(response.data.error),
          description: 'False response from server',
        });
        Screen.showAlert(response.data.error
          ? response.data.error
          : MessageUtils.getSystemErrorMessage(payLoadData.lang));
      }
    }).catch((error) => {
      Analytics.logFirebaseEvent('api_failure', {
        end_point: apiUrl,
        error_code: FormatUtils.getFirst100Characters(error),
        description: 'Exception',
      });
      console.log(`11 @@@@@@@ FAILED :: ${actionName} => error :\n`, error);
      if (exCb == null) {
        Screen.showAlert(error.request !== undefined && (error.response || error.request._timedOut)
          ? MessageUtils.getSystemErrorMessage(payLoadData.lang)
          : error.message);
      } else {
        error.request !== undefined && (error.response || error.request._timedOut)
          ? exCb(MessageUtils.getSystemErrorMessage(payLoadData.lang))
          : exCb(error.message);
      }
    });

  } else {
    Analytics.logFirebaseEvent('api_failure', {
      end_point: apiUrl,
      error_code: 'NETWORK_ERROR',
      description: 'Network connection issue',
    });
    if (exCb == null) {
      Screen.showAlert(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
    } else {
      exCb(MessageUtils.getNetworkErrorMessage(payLoadData.lang));
    }
  }
};

const convertToHttpUrl = (url) => {
  return url.replace(/^https:/, 'http:');
};

const createUrl = (moduleName, controllerName, actionName) => {
  return `${baseUrl + moduleName}/${controllerName}/${actionName}`;
};

export default {
  apiPost,
  apiPostLegacy,
  convertToHttpUrl,
  createUrl

};

