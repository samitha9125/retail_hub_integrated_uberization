/*
 * File: FormatUtils.js
 * Project: Dialog Sales App
 * File Created: Saturday, 1st December 2018 12:16:01 am
 * Author: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Last Modified: Saturday, 1st December 2018 12:16:17 am
 * Modified By: Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import _ from 'lodash';
import { Constants } from '@Config';
import MessageUtils from './MessageUtils';

/**
 * @description get common error message
 * @param {*} message
 * @return {String} message
 * @memberof FuncUtils
 * 
 */
const getCommonErrorMessage = (message, language = 'en') => {
  console.log('### FormatUtils :: getCommonErrorMessage :: message, language - ', message, language);
  let common_message = _.isNil(message) || _.isObject(message) ? Constants.ERROR_MESSAGES.en.SYSTEM_ERROR : message;
  console.log('### FormatUtils :: getCommonErrorMessage :: common_message - ', common_message);
  return common_message;
};

/**
 * @description get plain text
 * @param {*} text
 * @return {String} plain_text
 * @memberof FuncUtils
 * 
 */
const getPlainText = (text, language = 'en') => {
  console.log('### FormatUtils :: getPlainText :: text, language - ', text, language);
  let plain_text = _.isNil(text) || _.isObject(text) ? '' : text;
  console.log('### FormatUtils :: getPlainText :: plain_text - ', plain_text);
  return plain_text;
};

/**
 * @description Method to get formatted currency value
 * @param {*} value
 * @return {Float} formatted_value
 * @memberof FuncUtils
 * 
 */
const getFormattedCurrency = (value) => {
  console.log('### FormatUtils :: getFormattedCurrency :: value - ', value);
  let formatted_value = !_.isNil(value) || !_.isEmpty(value) || !_.isNaN(value) ? parseFloat(value).toFixed(2) : 0.00;
  console.log('### FormatUtils :: getFormattedCurrency :: formatted_value - ', formatted_value);
  return formatted_value;
};

/**
 * @description Common method to get first 100 character from string 
 * @param {String} [text}
 * @memberof FuncUtils
 */
const getFirst100Characters = (text) => {
  console.log('### FuncUtils :: getFirst100Characters :: text - ', text);
  let formated_text;
  try {
    formated_text = text.slice(0, 100);
  } catch (error) {
    formated_text = MessageUtils.getSystemErrorMessage();
  }
  console.log('### FuncUtils :: getFirst100Characters :: formated_text - ', text);
  return formated_text;
};


export default {
  getCommonErrorMessage,
  getPlainText,
  getFormattedCurrency,
  getFirst100Characters

};

