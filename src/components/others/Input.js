import React from 'react';

import { TextInput, View, Text } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
    const { inputStyle, labelStyle, containerStyle } = styles;

    return (
        <View style={containerStyle} > 
            <Text style={labelStyle}>{label}</Text>
            <TextInput 
                autoCorrect={false}
                value={value}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                onChangeText={onChangeText}
                style={inputStyle}                         
            />
        </View>
    );
};

const styles = {
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        color: '#000',
        fontSize: 18,
        lineHeight: 23,
        flex: 2
    },
    labelStyle: {  
        fontSize: 18,
        paddingLeft: 20,
        flex: 1

    },   
     containerStyle: {
        flexDirection: 'row', 
        height: 40,
        flex: 1,
        alignItems: 'center'
       
    }
  
};

export { Input };
