
/*
 * @export
 * @class  Button
 * @extends {React.Component}
 * Copyright (C) Dialog axiata PVT LTD
 * Author : Arafath Misree
 * Lastupdated by : Arafath Misree
 * Created on : N/A
 * This block of code is used to secondory button using child views and child styles depending on props.
 *
 */
import React, { } from 'react';
import { Text, TouchableOpacity } from 'react-native';



const Button = ({ onPress, children, childStyle, ContainerStyle, childView, disabled }) => {



  const { buttonStyle, textStyle } = styles;


  return (
    <TouchableOpacity
      onPress={onPress}
      style={ContainerStyle ? ContainerStyle : buttonStyle}
      disabled={disabled}
    >
      {childView ?
        (childView)
        :
        (<Text style={childStyle ? childStyle : textStyle}>
          {children}
        </Text>
        )}
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10

  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5
  }
};

export default Button;
