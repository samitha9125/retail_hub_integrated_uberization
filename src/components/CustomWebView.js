import React from 'react';
import { WebView, StyleSheet } from 'react-native';

class CustomWebView extends React.Component {
  render() {
    return (
      <WebView {...this.props} 
        style={styles.container}
      />
    );
  }
}

const styles = StyleSheet.create({
  container:{ 
    flex: 1
  }
});

export default CustomWebView;