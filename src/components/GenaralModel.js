/*
 * File: GeneralModel.js
 * This is the General Model Reusable Component.
 * Project: Dialog Sales App
 * File Created: Monday, 25th June 2018 12:19:48 pm
 * Author: Arafath Misree (arafath@omobio.net)
 * -----
 * Last Modified: Sunday, 8th July 2018 2:22:15 pm
 * Modified By: Arafath Misree (arafath@omobio.net)
 *              Damith Nuwan Sampath (nuwan@omobio.net)
 * -----
 * Copyright 2018 Omobio (PVT) Ltd
 */

import React from 'react';
import { KeyboardAvoidingView, ScrollView, Dimensions, Modal, View, Text, Image, StyleSheet } from 'react-native';
import Orientation from 'react-native-orientation';
import Button from '@Components/others/Button';
import Colors from '../config/colors';
const { width, height } = Dimensions.get('window');

export default class GeneralModel extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }


  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          style={styles.modalContainer}
          visible={this.props.modalVisible}
          hardwareAccelerated={true}
          onRequestClose={
            this.props.hideModel
          }>
          {this.props.parentView ? this.props.parentView : <KeyboardAvoidingView style={styles.keyboardAvoidingViewStyle} keyboardShouldPersistTaps="always">
            <ScrollView>
              <View style={styles.scrollViewContainer}>
                {this.props.childView ? this.props.childView :
                  <View style={styles.topContainerStyle} >
                    <View style={styles.imageContainerStyle}>
                      <Image
                        resizeMode="contain"
                        resizeMethod="scale"
                        style={styles.imageStyle}
                        source={this.props.icon}
                      />
                    </View>
                    <View style={styles.descriptionContainer}>
                      <Text textAlign="center" style={styles.descriptionTextStyle}>{this.props.description}</Text>
                    </View>
                  </View>}
                <View style={styles.buttonContainerStyle}>
                  {this.props.secondryText ? <Button children={this.props.secondryText}
                    childStyle={styles.secondaryButtonStyle}
                    ContainerStyle={styles.secondaryButtonContainerStyle}
                    onPress={this.props.secondoryPress}
                  /> : null}
                  <Button children={this.props.primaryText}
                    childStyle={[styles.primaryButtonStyle, this.props.ButtonColor ? { color: this.props.ButtonColor } : {}]}
                    ContainerStyle={styles.primaryButtonContainerStyle}
                    disabled={this.props.disabled}
                    onPress={this.props.primaryPress}
                  />
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  keyboardAvoidingViewStyle: {
    flex: 1, 
    height: height * 0.5,
    paddingTop: height / 4,
    paddingHorizontal: 14,
    backgroundColor: Colors.modalOverlayColorLow
  },

  buttonContainerStyle: {
    flex: 1, 
    flexDirection: 'row',
    paddingTop: 18, 
    paddingBottom: 15, 
    paddingRight: 20, 
    alignItems: 'flex-end', 
    justifyContent: 'flex-end', 
       
  },

  scrollViewContainer: { 
    backgroundColor: Colors.colorWhite 
  },

  topContainerStyle: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },

  imageContainerStyle: { 
    margin : 10,
    alignItems: 'center'
  },

  descriptionContainer: {     
    paddingTop: 20, 
    justifyContent: 'center', 
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    padding: 12, 
  },

  imageStyle: { 
    width: 54,
    height: 54
  },

  descriptionTextStyle: { 
    fontSize: 18, 
    textAlign: 'left' 
  },

  primaryButtonContainerStyle: { 
    paddingRight: 5,
    padding: 3 
  },

  secondaryButtonContainerStyle: { 
    paddingRight: 25,
    padding: 3 
  },

  primaryButtonStyle: { 
    color: Colors.btnActive, 
    fontSize: 20,
    fontWeight:'500'
  },
  secondaryButtonStyle: { 
    color: Colors.btnActive, 
    fontSize: 20,
    fontWeight:'500'
  }


});


