import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class PageWrapper extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { navigator } = this.props;
    return (
      <KeyboardAwareScrollView navigator={navigator}>
        {this.props.children}
      </KeyboardAwareScrollView>
    );
  }
}

export default PageWrapper;
