
/*
 * @export
 * @class  Empty Feed
 * @extends {React.Component}
 * Copyright (C) Dialog Axiata PVT LTD
 * Author : Arafath Misree
 * Last updated by : Arafath Misree
 * Created on : N/A
 * This block of code is used to present empty feed content among the application .
 *
 */
import React  from 'react';
import { View, Dimensions, Text } from 'react-native';
import Colors from '../config/colors';
const { width, height } = Dimensions.get('window');

export default class EmptyFeed extends React.Component {
  render() {
    const { messageTxtStyle = {}, containerStyle = { marginTop: height / 3.5 } } = this.props;
    return (
      <View style={[styles.messageBox, containerStyle ]}>
        <Text style={[styles.messageTxt, messageTxtStyle]} fontWeight="medium">
          {this.props.text}
        </Text>
      </View>
    );
  }
}

const styles = {
  messageBox: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: width
  },
  messageTxt: {
    fontSize: 16,
    textAlign: 'center',
    color: Colors.colorLightBlack
  }
};
