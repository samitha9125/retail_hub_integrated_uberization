import { GET_CONNECTION_TYPE_BILL, GET_CONNECTION_NUMBER_BILL, GET_AMOUNT_BILL, GET_EZ_CASH_PIN_BILL } from '../actions/types';

const INITIAL_STATE = {
  connection_type: 'GSM',
  connection_number: '',
  amount: '',
  ez_cash_pin: ''
};

export default(state = INITIAL_STATE, action) => {

  switch (action.type) {
    case GET_CONNECTION_TYPE_BILL:
      return {
        ...state,
        connection_type: action.payLoad
      };
    case GET_CONNECTION_NUMBER_BILL:
      return {
        ...state,
        connection_number: action.payLoad
      };
    case GET_AMOUNT_BILL:
      return {
        ...state,
        amount: action.payLoad
      };
    case GET_EZ_CASH_PIN_BILL:
      return {
        ...state,
        ez_cash_pin: action.payLoad
      };
    case 'RESET_BILL':
      console.log('Reset billpayment successful.');
      return INITIAL_STATE;
    default:
      return state;
  }
};