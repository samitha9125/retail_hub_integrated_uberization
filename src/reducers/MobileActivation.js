import {
  GET_MOBILE_ACT_UNIQUE_TX_ID,
  GET_ID_NUMBER_MOBILE_ACT,
  GET_MOBILE_NUMBER_MOBILE_ACT,
  GET_FIRST_RELOAD_MOBILE_ACT,
  GET_SIM_NUMBER_MOBILE_ACT,
  GET_INFORMATION_ID_TYPE_MOBILE_ACT,
  GET_NAME_MOBILE_ACT,
  GET_PRE_POST_MOBILE_ACT,
  GET_ALTERNATE_CONTACT_NUMBER_MOBILE_ACT,
  GET_EMAIL_ADDRESS_MOBILE_ACT,
  GET_LOCAL_FOREIGN_MOBILE_ACT,
  NEW_CONN_ACTIVATION_SUCCESS,
  NEW_CONN_ACTIVATION_FAIL,
  MOBILE_ACT_NIC_VALIDATION_SUCCESS,
  MOBILE_ACT_NIC_VALIDATION_FAIL,
  OTP_VALIDATION_SUCCESS,
  OTP_VALIDATION_FAIL,
  GET_OTP_STATUS_MOBILE_ACT,
  GET_ADDRESS_DIFFERENT_MOBILE_ACT,
  GET_INFORMATION_NOT_CLEAR_MOBILE_ACT,
  GET_OTP_NUMBER,
  GET_SKIP_VALIDATION_MOBILE_ACT,
  MOBILE_ACT_OTP_VALIDATION_SUCCESS,
  MOBILE_ACT_OTP_VALIDATION_FAIL,
  MOBILE_ACT_M_CONNECT_VALIDATION_SUCCESS,
  MOBILE_ACT_M_CONNECT_VALIDATION_FAIL,
  MOBILE_ACT_MODEL_POPPED,
  MOBILE_ACT_OTP_POPPED,
  MOBILE_ACT_M_CONNECT_POPPED,
  GET_BLOCKED_ID_MOBILE_ACT,
  GET_MOBILE_ACT_SIGNATURE,
  GET_MOBILE_ACT_KYC_CAPTURE,
  GET_IS_CAPTURED_MOBILE_ACT,
  GET_MOBILE_ACT_KYC_NIC_1,
  GET_MOBILE_ACT_KYC_NIC_2,
  GET_RELOAD_OFFER_CODE_MOBILE_ACT,
  GET_MOBILE_ACT_CAPTURE_TYPE_NO,
  GET_MOBILE_ACT_RE_CAPTURE_TYPE_NO,
  GET_MOBILE_ACT_CUSTOMER_KYC_CAPTURE,
  GET_MOBILE_ACT_POB_KYC_CAPTURE,
  GET_MOBILE_ACT_RESET_IMAGES,
  GET_API_RESPONSE_MOBILE_ACT,
  GET_SELECTED_PACKAGE,
  GET_PACKAGE_DEPOSITS_SUCCESS,
  GET_PACKAGE_DEPOSITS_FAIL,
  GET_EZ_CASH_PIN_MOBILE_ACT,
  SIM_VALIDATION_SUCCESS,
  //SIM_VALIDATION_FAIL,
  GET_TOTAL_PAYMENT,
  SELECTED_PACKAGE_RESET,
  IS_PACKAGE_LOADED,
  LOCAL_CALL_DEPOSITS_RESET,
  GET_SELECTED_PACKAGE_INDEX,
  USER_SELECTED_PACKAGE_INDEX_RESET,
  RESET_EZ_CASH_PIN,
  FOCUS_EZ_CASH_PIN,
  GET_MOBILE_ACT_LOADING,
  GET_MOBILE_ACT_API_LOADING,
  SET_TOTAL_PAYMENT_DATA,
  SET_PAYMENT_INFO, 
  RESET_PAYMENT_INFO
} from '../actions/types';

const INITIAL_STATE = {

  unique_tx_id: '',
  pre_post: 'pre',
  local_foreign: 'local',
  id_number: '',
  sim_number: '',
  mobile_number: '',
  first_reload: '',
  ezCashPIN: '',
  information_id_type: 'NIC',
  alternate_contact_number: '',
  email_address: '',
  name: '',
  otp_number: 'Select a Number',
  otp_status: '',
  mobile_activation: null,
  nic_validation: null,
  nic_api_fail: false,
  otp_validation: null,
  m_connect_validation: null,
  model_popped: false,
  otp_popped: false,
  m_connect_popped: false,
  skip_validation: false,
  address_different: false,
  information_not_clear: false,
  blocked_id_first: '',
  signature: null,
  kyc_capture: null,
  kyc_capture_nic_1: null,
  kyc_capture_nic_2: null,
  kyc_pob_capture: null,
  kyc_customer_capture: null,
  kyc_capture_type_no: '',
  kyc_recapture_type_no: '',
  reload_offer_code: '',
  api_response: null,
  isCaptured: false,
  selectedPackage: {
    pkg_CODE: '',
    pkg_NAME: '',
    pkg_RENTAL: "0",
    connection_fee: "",
    deposit_AMOUNT: "",
    package_info: []
  },
  // isPackageLoaded: false,
  didPackageLoad:false,
  packageDeposits: {
    depositType: "LCD",
    depositAmount: ''
  },
  paymentInfo: {
    totalPayValue: 0,
    conFeeValue: 0,
    outstandingFeeValue: 0,
    localCallValue: 0
  },
  material_code: '',
  total_payment: 0,
  userSelectedPackageIndex: -1,
  mobileActLoading: false,
  mActApiLoading: false,
  focusEzCashInput: false,
  totalPaymentData:{
    totalPayValue: '',
    conFeeValue: '',
    outstandingFeeValue: '',
    localCallValue: '',
    outstanding_fee: -1
  }
};

export default(state = INITIAL_STATE, action) => {

  switch (action.type) {
    case GET_MOBILE_ACT_UNIQUE_TX_ID:
      return {
        ...state,
        unique_tx_id: action.payLoad
      };
    case GET_PRE_POST_MOBILE_ACT:
      return {
        ...state,
        pre_post: action.payLoad
      };
    case SET_TOTAL_PAYMENT_DATA:
      return {
        ...state,
        totalPaymentData: action.payLoad
      };
    case GET_LOCAL_FOREIGN_MOBILE_ACT:
      return {
        ...state,
        local_foreign: action.payLoad
      };
    case GET_ID_NUMBER_MOBILE_ACT:
      return {
        ...state,
        id_number: action.payLoad
      };
    case GET_SIM_NUMBER_MOBILE_ACT:
      return {
        ...state,
        sim_number: action.payLoad
      };

    case GET_MOBILE_NUMBER_MOBILE_ACT:
      return {
        ...state,
        mobile_number: action.payLoad
      };

    case GET_FIRST_RELOAD_MOBILE_ACT:
      return {
        ...state,
        first_reload: action.payLoad
      };

    case GET_INFORMATION_ID_TYPE_MOBILE_ACT:
      return {
        ...state,
        information_id_type: action.payLoad
      };

    case GET_NAME_MOBILE_ACT:
      return {
        ...state,
        name: action.payLoad
      };

    case GET_ALTERNATE_CONTACT_NUMBER_MOBILE_ACT:
      return {
        ...state,
        alternate_contact_number: action.payLoad
      };

    case GET_EMAIL_ADDRESS_MOBILE_ACT:
      return {
        ...state,
        email_address: action.payLoad
      };

    case NEW_CONN_ACTIVATION_SUCCESS:
      return {
        ...state,
        mobile_activation: action.payLoad
      };
    case NEW_CONN_ACTIVATION_FAIL:
      return {
        ...state,
        mobile_activation: action.payLoad
      };

    case MOBILE_ACT_NIC_VALIDATION_SUCCESS:
      return {
        ...state,
        nic_validation: action.payLoad,
        nic_api_fail: action.nic_api_fail
      };

    case MOBILE_ACT_NIC_VALIDATION_FAIL:
      return {
        ...state,
        nic_validation: action.payLoad,
        nic_api_fail: action.nic_api_fail,
      };

    case OTP_VALIDATION_SUCCESS:
      return {
        ...state,
        otp_validation: action.payLoad
      };

    case OTP_VALIDATION_FAIL:
      return {
        ...state,
        otp_validation: action.payLoad
      };

    case GET_OTP_STATUS_MOBILE_ACT:
      return {
        ...state,
        otp_status: action.payLoad
      };

    case GET_OTP_NUMBER:
      return {
        ...state,
        otp_number: action.payLoad
      };

    case GET_SKIP_VALIDATION_MOBILE_ACT:
      return {
        ...state,
        skip_validation: action.payLoad
      };

    case GET_INFORMATION_NOT_CLEAR_MOBILE_ACT:
      return {
        ...state,
        information_not_clear: action.payLoad
      };

    case GET_ADDRESS_DIFFERENT_MOBILE_ACT:
      return {
        ...state,
        address_different: action.payLoad
      };

    case MOBILE_ACT_OTP_VALIDATION_SUCCESS:
      return {
        ...state,
        otp_validation: action.payLoad
      };

    case MOBILE_ACT_OTP_VALIDATION_FAIL:
      return {
        ...state,
        otp_validation: action.payLoad
      };

    case MOBILE_ACT_MODEL_POPPED:
      return {
        ...state,
        model_popped: action.payLoad
      };

    case MOBILE_ACT_OTP_POPPED:
      return {
        ...state,
        otp_popped: action.payLoad
      };

    case MOBILE_ACT_M_CONNECT_POPPED:
      return {
        ...state,
        m_connect_popped: action.payLoad
      };

    case MOBILE_ACT_M_CONNECT_VALIDATION_SUCCESS:
      return {
        ...state,
        m_connect_validation: action.payLoad
      };

    case MOBILE_ACT_M_CONNECT_VALIDATION_FAIL:
      return {
        ...state,
        m_connect_validation: action.payLoad
      };

    case GET_BLOCKED_ID_MOBILE_ACT:
      return {
        ...state,
        blocked_id_first: action.payLoad
      };

    case GET_MOBILE_ACT_SIGNATURE:
      return {
        ...state,
        signature: action.payLoad
      };

    case GET_MOBILE_ACT_KYC_CAPTURE:
      return {
        ...state,
        kyc_capture: action.payLoad
      };

    case GET_MOBILE_ACT_KYC_NIC_1:
      return {
        ...state,
        kyc_capture_nic_1: action.payLoad
      };

    case GET_MOBILE_ACT_KYC_NIC_2:
      return {
        ...state,
        kyc_capture_nic_2: action.payLoad
      };

    case GET_MOBILE_ACT_POB_KYC_CAPTURE:
      return {
        ...state,
        kyc_pob_capture: action.payLoad
      };

    case GET_MOBILE_ACT_CUSTOMER_KYC_CAPTURE:
      return {
        ...state,
        kyc_customer_capture: action.payLoad
      };

    case GET_MOBILE_ACT_CAPTURE_TYPE_NO:
      return {
        ...state,
        kyc_capture_type_no: action.payLoad
      };

    case GET_MOBILE_ACT_RE_CAPTURE_TYPE_NO:
      return {
        ...state,
        kyc_recapture_type_no: action.payLoad
      };

    case GET_RELOAD_OFFER_CODE_MOBILE_ACT:
      return {
        ...state,
        reload_offer_code: action.payLoad
      };

    case GET_IS_CAPTURED_MOBILE_ACT:
      return {
        ...state,
        isCaptured: action.payLoad
      };

    case GET_API_RESPONSE_MOBILE_ACT:
      return {
        ...state,
        api_response: action.payLoad
      };

    case GET_SELECTED_PACKAGE:
      return {
        ...state,
        selectedPackage: action.payLoad
      };

    case GET_PACKAGE_DEPOSITS_SUCCESS:
      return {
        ...state,
        packageDeposits: action.payLoad
      };

    case GET_PACKAGE_DEPOSITS_FAIL:
      return {
        ...state,
        packageDeposits:  {
          depositType: "LCD",
          depositAmount: ''
        }
      };

    case GET_MOBILE_ACT_LOADING:
      return {
        ...state,
        mobileActLoading: action.payLoad
      };

    case GET_MOBILE_ACT_API_LOADING:
      return {
        ...state,
        mActApiLoading: action.payLoad
      };

    case GET_EZ_CASH_PIN_MOBILE_ACT:
      return {
        ...state,
        ezCashPIN: action.payLoad
      };

    case SIM_VALIDATION_SUCCESS:
      console.log("SIM_VALIDATION_SUCCESS: ", action.payLoad.data);
      return {
        ...state,
        material_code: action.payLoad.data
      };

    case GET_TOTAL_PAYMENT:
      console.log("GET_TOTAL_PAYMENT: ", action.payLoad);
      return {
        ...state,
        total_payment: action.payLoad
      };

    case SET_PAYMENT_INFO:
      console.log("SET_PAYMENT_INFO: ", action.payLoad);
      return {
        ...state,
        paymentInfo: action.payLoad
      };

    case RESET_PAYMENT_INFO:
      console.log("RESET_PAYMENT_INFO: ", action.payLoad);
      return {
        ...state,
        paymentInfo: {
          totalPayValue: 0,
          conFeeValue: 0,
          outstandingFeeValue: 0,
          localCallValue: 0
        }
      };

    case IS_PACKAGE_LOADED:
      console.log("IS_PACKAGE_LOADED: ", action.payLoad);
      return {
        ...state,
        didPackageLoad: action.payLoad
      };

    case SELECTED_PACKAGE_RESET:
      console.log("SELECTED_PACKAGE_RESET: ", action.payLoad);
      return {
        ...state,
        selectedPackage: action.payLoad
      };

    //USER_SELECTED_PACKAGE_INDEX_RESET
    case USER_SELECTED_PACKAGE_INDEX_RESET:
      console.log("SELECTED_PACKAGE_RESET: ", action.payLoad);
      return {
        ...state,
        userSelectedPackageIndex: action.payLoad
      };

    case LOCAL_CALL_DEPOSITS_RESET:
      console.log("LCD_RESET: ", action.payLoad);
      return {
        ...state,
        packageDeposits: action.payLoad
      };

    case GET_SELECTED_PACKAGE_INDEX:
      console.log("Selected Package Index: ", action.payLoad);
      return {
        ...state,
        userSelectedPackageIndex: parseInt(action.payLoad)
      };

    case RESET_EZ_CASH_PIN:
      console.log("Resetting ex cash pin: ");
      return {
        ...state,
        ezCashPIN: action.payLoad
      };

    case FOCUS_EZ_CASH_PIN:
      console.log("focus ex cash pin");
      return {
        ...state,
        focusEzCashInput: action.payLoad
      };

    case GET_MOBILE_ACT_RESET_IMAGES:
      return {
        ...state,
        isCaptured: false,
        kyc_customer_capture: null,
        kyc_capture_nic_1: null,
        kyc_capture_nic_2: null,
        kyc_pob_capture: null,
        kyc_capture: null,
        signature: null,
        kyc_capture_type_no: '',
        kyc_recapture_type_no: ''
      };

    case 'RESET_MOBILE_ACT':
      console.log("Reset mobile activation successful.");
      return INITIAL_STATE;
    default:
      return state;
  }
};