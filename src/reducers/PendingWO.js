import {
  WORK_ORDER_DETAIL,
  WORK_ORDER_CUSTOMER_SIGNATURE,
  WORK_ORDER_DTV_PROVISIONING,
  WORK_ORDER_PROVISIONING_STATUS,
  WORK_ORDER_INSTALLED_ITEMS,
  WORK_ORDER_SUMMARY_RELOAD,
  WORK_ORDER_RESET,
  WORK_ORDER_CUSTOMER_FEEDBACK,
  WORK_ORDER_CUSTOMER_ANSWERS,
  WORK_ODER_DTV_INSTALLATION,
  WORK_ORDER_SERIAL_LIST,
  WORK_ORDER_SIGNATURE_DATA,
  WORK_ORDER_ROOT_CAUSES,
  WORK_ORDER_APP_SCREEN,
  WORK_ORDER_APP_FLOW,
  WORK_ORDER_TRB_ACCESSORY_SALE,
  CLEAR_STATES,
  CLEAR_ROOT_CAUSE_DATA,
  WORK_ORDER_REFIX_WARRANTY_DETAILS,
  PAYMENT_DETAILS,
  WORK_ORDER_JOB_STATUS,
  ADD_ADDITIONAL_HYBRID_PROD,
  CLEAR_TRB_WO,
  UPDATE_WORK_ORDER_ROOT_CAUSES,
  JOB_HISTORY_DETAILS
} from '../actions/types';

const INITIAL_STATE = {
  work_order_detail: '',
  work_order_customer_signature: null,
  work_order_dtv_provisioning: '',
  work_order_provisioning_status: '',
  work_order_installed_items: '',
  work_order_summary_reload: '',
  work_order_customer_feedback: '',
  work_order_customer_answers: '',
  work_oder_dtv_installation: '',
  work_order_serial_list: '',
  work_order_signature_data: '',
  work_order_app_screen: '',
  work_order_app_flow: '',
  work_order_trb_accessory_sale: [],
  work_order_root_causes: [],
  work_order_refix_warranty_details: [],
  work_order_add_additional_hybrid_prod: '',
  payment_details: {},
  job_history_details: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CLEAR_TRB_WO:
      return {
        ...state,
        work_order_trb_accessory_sale: []
      };

    case CLEAR_STATES:
      return INITIAL_STATE;

    case WORK_ORDER_CUSTOMER_SIGNATURE:
      return {
        ...state,
        work_order_customer_signature: action.payLoad
      };

    case WORK_ORDER_DTV_PROVISIONING:
      return {
        ...state,
        work_order_dtv_provisioning: action.payLoad
      };

    case WORK_ORDER_PROVISIONING_STATUS:
      return {
        ...state,
        work_order_provisioning_status: action.payLoad
      };
    case WORK_ORDER_SUMMARY_RELOAD:
      return {
        ...state,
        work_order_summary_reload: action.payLoad
      };

    case WORK_ORDER_INSTALLED_ITEMS:
      return {
        ...state,
        work_order_installed_items: action.payLoad
      };

    case WORK_ORDER_DETAIL:
      return {
        ...state,
        work_order_detail: action.payLoad
      };

    case WORK_ORDER_CUSTOMER_FEEDBACK:
      return {
        ...state,
        work_order_customer_feedback: action.payLoad
      };

    case WORK_ORDER_CUSTOMER_ANSWERS:
      return {
        ...state,
        work_order_customer_answers: action.payLoad
      };

    case WORK_ORDER_RESET:
      return INITIAL_STATE;

    case WORK_ODER_DTV_INSTALLATION:
      return {
        ...state,
        work_oder_dtv_installation: action.payLoad
      };

    case WORK_ORDER_SERIAL_LIST:
      return {
        ...state,
        work_order_serial_list: action.payLoad
      };

    case WORK_ORDER_SIGNATURE_DATA:
      return {
        ...state,
        work_order_signature_data: action.payLoad
      };

    case WORK_ORDER_ROOT_CAUSES:
      return {
        ...state,
        work_order_root_causes: [
          ...state.work_order_root_causes,
          action.payLoad
        ]
      };
    case CLEAR_ROOT_CAUSE_DATA:
      return {
        ...state,
        work_order_root_causes: []
      };
    case UPDATE_WORK_ORDER_ROOT_CAUSES:
      return {
        ...state,
        work_order_root_causes: action.payLoad
      };
    case WORK_ORDER_APP_SCREEN:
      return {
        ...state,
        work_order_app_screen: action.payLoad
      };

    case WORK_ORDER_APP_FLOW:
      return {
        ...state,
        work_order_app_flow: action.payLoad
      };

    case WORK_ORDER_TRB_ACCESSORY_SALE:
      return {
        ...state,
        work_order_trb_accessory_sale: action.payLoad
      };

    case WORK_ORDER_REFIX_WARRANTY_DETAILS:
      return {
        ...state,
        work_order_refix_warranty_details: action.payLoad
      };

    case WORK_ORDER_JOB_STATUS:
      return {
        ...state,
        work_order_detail: {
          ...state.work_order_detail,
          job_status: action.payLoad
        }
      };
    case PAYMENT_DETAILS:
      return {
        ...state,
        payment_details: action.payLoad
      };

    case ADD_ADDITIONAL_HYBRID_PROD:
      {
        let work_order_detail = state.work_order_detail;
        const newProdList = {
          ...work_order_detail,
          product_detail: {
            ...work_order_detail.product_detail,
            product_list: action.payLoad
          }
        };

        return {
          ...state,
          work_order_detail: newProdList
        };
      }

    case JOB_HISTORY_DETAILS:
      return {
        ...state,
        job_history_details: action.payLoad
      }
    default:
      return state;
  }
};