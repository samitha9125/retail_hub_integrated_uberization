import {
    WARRANTY_REPLACEMENT_DETAILS,
    WARRANTY_REPLACEMENT_GET_ITEM_TYPE,
    WARRANTY_REPLACEMENT_OFFER_CODE,
    WARRANTY_REPLACEMENT_ITEM_CODE,
    WARRANTY_REPLACEMENT_CONNECTION_TYPE,
    WARRANTY_REPLACEMENT_RESET,
} from '../actions/types';

const INITIAL_STATE = {
    warranty_replacement_details: {
        connectionNo: '',
        contactNo: '',
        contactNoAlt: '',
        accessoryType: '',
        oldSerial: '',
        newSerial: '',
        newExpiry: '',
        newPeriod: '',
        LOB: '',
        offerCode: '',
        itemCode: '',
        connectionType: '',
    },
    warranty_replacement_get_item_type: '',
    warranty_replacement_get_offer_code: '',
    warranty_replacement_get_item_code: '',
    warranty_replacement_get_connection_type: '',
    warranty_replacement_reset: '',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case WARRANTY_REPLACEMENT_DETAILS:
            return {
                ...state,
                warranty_replacement_details: action.payLoad
            };

        case WARRANTY_REPLACEMENT_GET_ITEM_TYPE:
            return {
                ...state,
                warranty_replacement_get_item_type: action.payLoad
            };

        case WARRANTY_REPLACEMENT_OFFER_CODE:
            return {
                ...state,
                warranty_replacement_get_offer_code: action.payLoad
            };

        case WARRANTY_REPLACEMENT_ITEM_CODE:
            return {
                ...state,
                warranty_replacement_get_item_code: action.payLoad
            };

        case WARRANTY_REPLACEMENT_CONNECTION_TYPE:
            return {
                ...state,
                warranty_replacement_get_connection_type: action.payLoad
            };

        case WARRANTY_REPLACEMENT_RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};