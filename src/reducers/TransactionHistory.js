import {
  TRN_LOADING_INDICATOR,
  RESET_ALL_TRANS_HISTORY,
  TRN_SIM_CHANGE_HISTORY_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_FAIL,
  TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_PENDING_FAIL,
  TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS,
  TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL,
  TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM,
  TRN_BILL_PAYMENT_HISTORY_SUCCESS,
  TRN_BILL_PAYMENT_HISTORY_FAIL,
  TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM,
  TRN_ACTIVATION_STATUS_ALL_SUCCESS,
  TRN_ACTIVATION_RESET_ALL_LIST,
  TRN_ACTIVATION_STATUS_ALL_FAIL,
  TRN_ACTIVATION_STATUS_REJECTED_SUCCESS,
  TRN_ACTIVATION_STATUS_REJECTED_FAIL,
  TRN_ACTIVATION_STATUS_SELECTED_ITEM,
  TRN_ACTIVATION_STATUS_FILTERS,
  TRN_ACTIVATION_CAPTURED_IMAGE_STATUS,
  // TRN_SIM_CHANGE_HISTORY_GET_RESULTS,
  TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING,
  TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING,
  TRN_SIM_CHANGE_HISTORY_RESET_RESULTS,
  TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING,
  TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS,
  // TRN_BILL_PAYMENT_HISTORY_GET_RESULTS,
  TRN_SIM_CHANGE_HISTORY_RESET_LISTS,
  TRN_ACTIVATION_RESET_REJECTED_LIST,
  TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING,
  TRN_SET_ACTIVATION_STATUS_SEARCH_STRING,
  GET_ACTIVATION_STATUS_DETAILS
  
} from '../actions/types';

const INITIAL_STATE = {
  api_loading: false,
  sim_change_history_all_list: [],
  sim_change_history_pending_list: [],
  sim_change_history_selected_item: {},
  sim_change_history_details_item: {},
  bill_payment_history_list: [],
  bill_payment_history_selected_item: {},
  activation_status_all_list: [],
  activation_status_rejected_list: [],
  activation_status_selected_item: {},
  activation_status_filters: null,
  sim_change_history_search_string: '',
  sim_change_pending_history_search_string: '',
  bill_payment_history_search_results: [],
  bill_payment_history_search_string: '',
  activation_status_history_rejected_search_string: '',
  activation_status_history_search_string: '',
  captured_images: [false, false, false, false, false, false, false, false],
  activation_status_details: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TRN_LOADING_INDICATOR:
      return {
        ...state,
        api_loading: action.payLoad
      };
    //----------------Sim Change History ------------------------
    case TRN_SIM_CHANGE_HISTORY_SUCCESS:
      if (action.payLoad.info.length == 0 ||
        (state.sim_change_history_all_list != null && (state.sim_change_history_all_list.length >= action.payLoad.total))) {
        return {
          ...state,
          api_loading: false,
        };
      } else {
        return {
          ...state,
          api_loading: false,
          sim_change_history_all_list: state.sim_change_history_all_list.concat(action.payLoad.info)
        };
      }
    case TRN_SIM_CHANGE_HISTORY_FAIL:
      if (state.sim_change_history_all_list == 0) {
        return {
          ...state,
          api_loading: false,
          sim_change_history_all_list: action.payLoad
        };
      } else {
        return {
          ...state,
          api_loading: false,
        };
      }

    case TRN_SIM_CHANGE_HISTORY_RESET_RESULTS:
      console.log("TRN_SIM_CHANGE_HISTORY_RESET_RESULTS");
      return {
        ...state,
        sim_change_history_search_string: '',
        sim_change_pending_history_search_string: ''
      };

    // case TRN_SIM_CHANGE_HISTORY_GET_RESULTS:
    //   return {
    //     ...state,
    //     api_loading: false,
    //   };

    case TRN_SIM_CHANGE_HISTORY_SET_SEARCH_STRING:
      console.log("%%%% Set Search string", action.payLoad);
      return {
        ...state,
        sim_change_history_search_string: action.payLoad
      };

    case TRN_SIM_CHANGE_PENDING_HISTORY_SET_SEARCH_STRING:
      console.log("%%%% Set Search string", action.payLoad);
      return {
        ...state,
        sim_change_pending_history_search_string: action.payLoad
      };

    case TRN_SET_ACTIVATION_STATUS_REJECTED_SEARCH_STRING:
      console.log("%%%% Set Activations Rejected Search string", action.payLoad);
      return {
        ...state,
        activation_status_history_rejected_search_string: action.payLoad
      };

    case TRN_SET_ACTIVATION_STATUS_SEARCH_STRING:
      console.log("%%%% Set Activations Search string", action.payLoad);
      return {
        ...state,
        activation_status_history_search_string: action.payLoad
      };

    case TRN_SIM_CHANGE_HISTORY_RESET_LISTS:
      return {
        ...state,
        api_loading: false,
        sim_change_history_all_list: [],
        sim_change_history_pending_list: []
      };

    //----------------Sim Change History Pending list ------------------------
    case TRN_SIM_CHANGE_HISTORY_PENDING_SUCCESS:
      if (action.payLoad.info.length == 0
        || (state.sim_change_history_pending_list !== null
          && (state.sim_change_history_pending_list.length >= action.payLoad.total))) {
        return {
          ...state,
          api_loading: false,
        };
      } else if (state.sim_change_history_pending_list !== null) {
        return {
          ...state,
          api_loading: false,
          sim_change_history_pending_list: state.sim_change_history_pending_list.concat(action.payLoad.info)
        };
      }
      break;
    case TRN_SIM_CHANGE_HISTORY_PENDING_FAIL:
      if (state.sim_change_history_pending_list == 0) {
        return {
          ...state,
          api_loading: false,
          sim_change_history_pending_list: action.payLoad
        };
      } else {
        return {
          ...state,
          api_loading: false,
        };
      }

    //----------------Sim Change History Details ------------------------
    case TRN_SIM_CHANGE_HISTORY_SELECTED_ITEM:
      return {
        ...state,
        sim_change_history_selected_item: action.payLoad
      };

    case TRN_SIM_CHANGE_HISTORY_DETAILS_SUCCESS:
      return {
        ...state,
        api_loading: false,
        sim_change_history_details_item: action.payLoad
      };

    case TRN_SIM_CHANGE_HISTORY_DETAILS_FAIL:
      return {
        ...state,
        api_loading: false,
        sim_change_history_details_item: action.payLoad
      };

    //----------------Activation status History Details ------------------------
    case TRN_ACTIVATION_STATUS_ALL_SUCCESS:
      console.log('isFirstLoading redux', action.isFirstLoading );
      console.log('isFirstLoading redux', action.payLoad );
      console.log('isFirstLoading redux', action.payLoad.search_text );
      if (action.payLoad.info.length == 0 ||
        (state.activation_status_all_list != null && (state.activation_status_all_list.length >= action.payLoad.total))) {
        return {
          ...state,
          api_loading: false,
        };
      } else if ( action.isFirstLoading || action.payLoad.search_text){
        console.log('isFirstLoading search_text', action.payLoad );
        return {
          ...state,
          api_loading: false,
          activation_status_all_list: action.payLoad.info
        };
      } else {
        return {
          ...state,
          api_loading: false,
          activation_status_all_list: state.activation_status_all_list.concat(action.payLoad.info)
        };
      }
    case TRN_ACTIVATION_RESET_ALL_LIST:
      return {
        ...state,
        activation_status_all_list: action.payLoad
      };

    case TRN_ACTIVATION_RESET_REJECTED_LIST:
      return {
        ...state,
        activation_status_rejected_list: []
      };

    case TRN_ACTIVATION_STATUS_ALL_FAIL:
      if (state.activation_status_all_list == 0) {
        return {
          ...state,
          api_loading: false,
          activation_status_all_list: action.payLoad
        };
      } else {
        return {
          ...state,
          api_loading: false,
        };
      }

    case TRN_ACTIVATION_STATUS_REJECTED_SUCCESS:
      console.log('isFirstLoading reject', action.isFirstLoading );
      if (action.payLoad.info.length == 0 ||
        (state.activation_status_rejected_list != null && (state.activation_status_rejected_list.length >= action.payLoad.total))) {
        return {
          ...state,
          api_loading: false,
        };
      } else if (action.isFirstLoading){
        return {
          ...state,
          api_loading: false,
          activation_status_rejected_list: action.payLoad.info
        };
      } else {
        return {
          ...state,
          api_loading: false,
          activation_status_rejected_list: state.activation_status_rejected_list.concat(action.payLoad.info)
        };
      }     
    case TRN_ACTIVATION_STATUS_REJECTED_FAIL:
      if (state.activation_status_rejected_list == 0) {
        return {
          ...state,
          api_loading: false,
          activation_status_rejected_list: action.payLoad
        };
      } else {
        return {
          ...state,
          api_loading: false,
        };
      }

    case TRN_ACTIVATION_STATUS_SELECTED_ITEM:
      return {
        ...state,
        activation_status_selected_item: action.payLoad
      };
    case TRN_ACTIVATION_CAPTURED_IMAGE_STATUS:
      return {
        ...state,
        captured_images: action.payLoad
      };
    case GET_ACTIVATION_STATUS_DETAILS:
      return {
        ...state,
        activation_status_details: []
      };


    //---------------- Bill Payment History ----------------------
    case TRN_BILL_PAYMENT_HISTORY_SUCCESS:
      if (action.payLoad.info.length == 0 || state.bill_payment_history_list.length >= action.payLoad.total) {
        return {
          ...state,
          api_loading: false,
        };
      } else {
        return {
          ...state,
          api_loading: false,
          bill_payment_history_list: state.bill_payment_history_list.concat(action.payLoad.info)
        };
      }
    case TRN_BILL_PAYMENT_HISTORY_FAIL:
      if (state.bill_payment_history_list == 0) {
        return {
          ...state,
          api_loading: false,
          bill_payment_history_list: action.payLoad
        };
      } else {
        return {
          ...state,
          api_loading: false,
        };
      }

    case TRN_BILL_PAYMENT_HISTORY_SELECTED_ITEM:
      return {
        ...state,
        bill_payment_history_selected_item: action.payLoad
      };

    // case TRN_BILL_PAYMENT_HISTORY_GET_RESULTS:
    //   return {
    //     ...state,
    //     bill_payment_history_search_results: action.payLoad
    //   };

    case TRN_BILL_PAYMENT_HISTORY_SET_SEARCH_STRING:
      return {
        ...state,
        bill_payment_history_search_string: action.payLoad
      };

    case TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS:
      console.log("TRN_BILL_PAYMENT_HISTORY_RESET_RESULTS");
      return {
        ...state,
        bill_payment_history_search_results: [],
        bill_payment_history_search_string: '',
        bill_payment_history_list: []
      };


    //----------------Activation status History Filter ------------------------
    case TRN_ACTIVATION_STATUS_FILTERS:
      return {
        ...state,
        activation_status_filters: action.payLoad
      };

    case RESET_ALL_TRANS_HISTORY:
      console.log("Reset transaction history successful.");
      return INITIAL_STATE;

    default:
      return state;
  }
};
