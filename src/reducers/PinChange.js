import { GET_CURRENT_PIN, GET_NEW_PIN, GET_CONFIRMED_PIN, RESET_PIN } from '../actions/types';

const INITIAL_STATE = {
  current_pin: '',
  new_pin: '',
  confirmed_pin: ''
};

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CURRENT_PIN:
      console.log(action.payLoad);
      return {
        ...state,
        current_pin: action.payLoad
      };
    case GET_NEW_PIN:
      console.log(action.payLoad);
      return {
        ...state,
        new_pin: action.payLoad
      };
    case GET_CONFIRMED_PIN:
      console.log(action.payLoad);
      return {
        ...state,
        confirmed_pin: action.payLoad
      };
    case RESET_PIN:
      console.log("Reset pin change successful.");
      return INITIAL_STATE;
    default:
      return state;
  }
};
