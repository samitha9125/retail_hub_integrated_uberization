import {

  GET_SIM_NUMBER_SIM,
  GET_ID_NUMBER_SIM,
  GET_MOBILE_NUMBER_SIM,
  SIM_VALIDATION_SUCCESS,
  SIM_VALIDATION_FAIL,
  SIM_VALIDATION_FAIL_RESET,
  SIM_API_FAIL_RESET,
  RESET,

  SIM_STB_CHANGE_COUNT

} from '../actions/types';

const INITIAL_STATE = {

  sim_number: '',
  id_number: '',
  mobile_number: '',
  sim_api_fail: false,
  sim_validation: null,
  sim_stb_count: 0
};

export default(state = INITIAL_STATE, action) => {

  switch (action.type) {
    case GET_SIM_NUMBER_SIM:
      return {
        ...state,
        sim_number: action.payLoad
      };
    case GET_ID_NUMBER_SIM:
      return {
        ...state,
        id_number: action.payLoad
      };
    case GET_MOBILE_NUMBER_SIM:
      return {
        ...state,
        mobile_number: action.payLoad
      };
    case SIM_VALIDATION_SUCCESS:
      return {
        ...state,
        sim_validation: action.payLoad,
        sim_api_fail: action.sim_api_fail
      };
    case SIM_VALIDATION_FAIL:
      return {
        ...state,
        sim_validation: action.payLoad,
        sim_api_fail: action.sim_api_fail
      };
    case SIM_VALIDATION_FAIL_RESET:
      return {
        ...state,
        sim_validation: action.payLoad
      };
    case SIM_API_FAIL_RESET:
      return {
        ...state,
        sim_api_fail: action.payLoad
      };
    case 'RESET_SIM':
      console.log("Reset sim change successful.");
      return INITIAL_STATE;
    case SIM_STB_CHANGE_COUNT:
      let count;
      if (action.payLoad == -1) {
        count = 0;
      } else {
        count++;
      }
      return {
        ...state,
        sim_stb_count: count
      };
    default:
      return state;
  }
};