import {
  GET_STOCK_ACCEPTANCE_API,
  GET_STOCK_ACCEPTANCE_DETAILS_API,
  GET_STOCK_ACCEPTANCE_ELEMENT_LIST,
  GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_LIST,
  GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_LIST,
  GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_STATUS,
  GET_STOCK_ACCEPTANCE_SESSION_SERIALS,
  GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_CHECK,
  GET_STOCK_ACCEPTANCE_MISMATCHED_SERIALS,
  GET_STOCK_ACCEPTANCE_SELECTED_STO_ID,
} from '../actions/types';

const INITIAL_STATE = {
  stock_accept_list: 'list list list',
  stock_acceptance_elements_list: '',
  stock_acceptance_scanned_serial_list: '',
  // stock_acceptance_issued_serial_list: {
  //     materialData: {
  //         itemList: []
  //     }
  // },
  stock_acceptance_issued_serial_list: '',
  stock_acceptance_session_serials: [],
  stock_acceptance_issued_serial_statsu: [],
  stock_acceptance_scanned_serial_check: '',
  stock_acceptance_mismatched_serials: [],
  stock_acceptance_selected_sto_id: '',
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case GET_STOCK_ACCEPTANCE_API:
      return {
        ...state,
        stock_accept_list: action.payLoad,
      };

    case GET_STOCK_ACCEPTANCE_DETAILS_API:
      return {
        ...state,
        stock_accept_list: action.payLoad,
      };
    case GET_STOCK_ACCEPTANCE_ELEMENT_LIST:
      return {
        ...state,
        stock_acceptance_elements_list: [...state.stock_acceptance_elements_list, action.payLoad],
      };
    case GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_LIST:
      return {
        ...state,
        // stock_acceptance_issued_serial_list: [state.stock_acceptance_issued_serial_list, action.payLoad],
        stock_acceptance_issued_serial_list: action.payLoad,
      };
    case GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_LIST:
      return {
        ...state,
        stock_acceptance_scanned_serial_list: [...state.stock_acceptance_scanned_serial_list, action.payLoad],
      };
    case GET_STOCK_ACCEPTANCE_ISSUED_SERIAL_STATUS:
      return {
        ...state,
        stock_acceptance_issued_serial_status: [...state.stock_acceptance_issued_serial_status, action.payLoad],
      };
    case GET_STOCK_ACCEPTANCE_SESSION_SERIALS:
      return {
        ...state,
        stock_acceptance_session_serials: [...state.stock_acceptance_session_serials, action.payLoad],
      };
    case GET_STOCK_ACCEPTANCE_SCANNED_SERIAL_CHECK:
      return {
        ...state,
        stock_acceptance_scanned_serial_check: action.payLoad,
      };
    case GET_STOCK_ACCEPTANCE_MISMATCHED_SERIALS:
      return {
        ...state,
        stock_acceptance_mismatched_serials: action.payLoad,
      };

    case GET_STOCK_ACCEPTANCE_SELECTED_STO_ID:
      return {
        ...state,
        stock_acceptance_selected_sto_id: action.payLoad,
      };
    default:
      return state;
  }
};