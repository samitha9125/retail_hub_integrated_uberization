import {
  GET_GENARAL_SIM_NUMBER,
  GET_GENARAL_API_LOADING,
  GET_GENARAL_SIM_VALIDATION_STATUS,
  GET_GENARAL_SIM_VALIDATION_SUCCESS,
  GET_GENARAL_SIM_VALIDATION_FAIL,
  RESET_SIM_VALIDATION_STATUS,
  SET_NUMBER_CHECK_STATUS,
  RESET_GENARAL
} from '../actions/types';

const INITIAL_STATE = {
  simNumber: '',
  api_loading: false,
  material_code: '',
  didSimValidated: false,
  simValidationStatus: 'initial',
  validateApiFail: false,
  didPerformNumberCheck: false
};

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_GENARAL_API_LOADING:
      return {
        ...state,
        api_loading: action.payLoad
      };
    case GET_GENARAL_SIM_NUMBER:
      return {
        ...state,
        simNumber: action.payLoad
      };
    case SET_NUMBER_CHECK_STATUS:
      return {
        ...state,
        didPerformNumberCheck: action.payLoad
      };
    case GET_GENARAL_SIM_VALIDATION_STATUS:
      return {
        ...state,
        simValidationStatus: action.payLoad
      };
    case GET_GENARAL_SIM_VALIDATION_FAIL:
      return {
        ...state,
        validateApiFail: true,
        simValidationStatus: 'fail',
        material_code: '',
        simNumber: '',
        api_loading: false,
      };
    case GET_GENARAL_SIM_VALIDATION_SUCCESS:
      return {
        ...state,
        validateApiFail: false,
        simValidationStatus: 'success',
        didSimValidated: true,
        material_code: action.payLoad,
        api_loading: false,
      };

    case RESET_SIM_VALIDATION_STATUS:
      return {
        ...state,
        validateApiFail: false,
        simValidationStatus: 'initial',
        didSimValidated: false,
        material_code: '',
        simNumber: '',
        api_loading: false,
      };
    case RESET_GENARAL:
      console.log("Reset general successful.");
      return INITIAL_STATE;
    default:
      return state;
  }
};