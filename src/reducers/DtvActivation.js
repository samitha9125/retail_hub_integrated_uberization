import {
  DTV_SET_API_LOADING,
  RESET_DTV_DATA,
  DTV_FORCE_UPDATE_VIEW,
  DTV_SELECTED_CONNECTION_TYPE,
  DTV_SET_CUSTOMER_TYPE,
  DTV_INPUT_ID_NUMBER,
  DTV_SET_ID_NUMBER_ERROR,
  DTV_ID_VALIDATION_SUCCESS,
  DTV_SET_TXN_REFERENCE,
  DTV_ID_VALIDATION_FAIL,
  DTV_SET_SECTIONS_VISIBLE,
  DTV_SET_ID_NUMBER_VALIDATION_STATUS,
  DTV_SET_PRODUCT_INFO_VALIDATION_STATUS,
  DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS,
  GET_DTV_DATA_PACKAGES_LIST,
  GET_DTV_DATA_PACKAGE_DETAIL,
  GET_DTV_DELIVERY_CHARGE,
  SET_DTV_DROPDOWN_INDEX,
  RESET_DTV_DROPDOWN_INDEX,
  DTV_OTP_VALIDATION_SUCCESS,
  DTV_OTP_VALIDATION_FAIL,
  DTV_OUTSTANDING_CHECK_SUCCESS,
  DTV_OUTSTANDING_CHECK_FAIL,
  DTV_PACK_TYPE,
  DTV_SAME_DAY_INSTALLATION_DATA,
  DTV_FULLFILLMENT_TYPE,
  RESET_DTV_FULLFILLMENT_TYPE,
  DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA,
  DTV_SET_CHANNEL_TYPE,
  DTV_SET_CHANNEL_GENRE,
  DTV_SELECTED_CHANNELS_N_PACKS,
  DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE,
  DTV_GET_OFFERS,
  DTV_RESET_OFFERS,
  DTV_STB_UPGRADE_CHECKBOX,
  DTV_SET_STB_UPGRADE_DATA,
  DTV_SET_WARRANTY_EXTENSION_DATA,
  DTV_SERIAL_TEXT_VALUE,
  DTV_SERIAL_VALIDATION,
  DTV_SET_FIRST_RELOAD_VALIDATION_ERROR,
  DTV_SET_SHOW_VOUCHER_CODE_FIELD,
  DTV_SET_CUSTOMER_INFO_ID_TYPE,
  DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT,
  DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION,
  DTV_RESET_KYC_CHECK_BOX_ALL,
  SET_DTV_VOUCHER_CODE,
  SET_DTV_VOUCHER_CODE_VALIDITY,
  DTV_SET_SERIAL_TYPE_BUNDLE,
  DTV_SET_SERIAL_TYPE_PACK,
  DTV_SET_SERIAL_TYPE_ACCESSORIES,
  DTV_BUNDLE_SERIAL,
  DTV_SERIAL_PACK,
  DTV_ACCESSORIES_SERIAL,
  DTV_RESET_INDIVIDUAL_SERIAL_DATA,
  DTV_RESET_SERIALS_DATA,
  DTV_EOAF_SERIAL,
  DTV_RESET_EOAF_SERIAL, 
  DTV_SET_INPUT_MOBILE_NUMBER, 
  DTV_SET_CUSTOMER_NAME,
  DTV_SET_CUSTOMER_ADDRESS_LINE1,
  DTV_SET_CUSTOMER_ADDRESS_LINE2,
  DTV_SET_CUSTOMER_ADDRESS_LINE3,
  DTV_SET_CUSTOMER_CITY,
  DTV_CITY_SEARCH_DATA,
  DTV_SET_RELOAD_AMOUNT,
  DTV_RESET_VOUCHER_VALUE,
  DTV_SET_INPUT_EZ_CASH_PIN,
  DTV_CUSTOMER_TOTAL_PAYMENT,
  DTV_EZ_CASH_BALANCE,
  DTV_RESET_EZCASH_BALANCE,
  DTV_RESET_SAMEDAY_INSTALLATION_STATUS,
  DTV_SET_INPUT_LANLINE_NUMBER,
  DTV_INPUT_EMAIL,
  DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS,
  SET_DTV_VOUCHER_CODE_ONLY,
  RESET_DTV_VOUCHER_CODE_DATA,
  DTV_SET_TOTAL_OUTSTANDING_AMOUNT,
  DTV_OTP_NUMBER,
  DTV_DEPOSIT_AMOUNT,
  DTV_RESET_DEPOSIT_AMOUNT

} from '../actions/types';
import Constants from '../config/constants';
import _ from 'lodash';

let packSerialDataValues = null;
let accessoriesSerialDataValues = null;
let bundleSerialDataValues = null;
let allSerialDataValues = [];

const INITIAL_STATE = {
  apiLoading: false,
  api_call_indicator_msg: 'Loading',
  random_value: 0,
  selected_connection_type: Constants.PREPAID,
  customerIdType: Constants.CX_ID_TYPE_NIC,
  customerInfoSectionIdType: Constants.CX_INFO_TYPE_NIC,
  customerInfoSectionIdTypeCode: Constants.ID_TYPE_NIC,
  customerType: Constants.CX_TYPE_LOCAL,
  customerExistence: Constants.CX_EXIST_NO,
  customerOtpStatus: Constants.CX_OTP_NOT_APPLICABLE,
  id_type: Constants.CX_ID_TYPE_NIC,

  //sections
  section_1_visible: true,
  section_2_visible: false,
  section_3_visible: false,
  sectionId_validation_status: false,
  sectionProductInfo_validation_status: false,
  sectionCustomerInfo_validation_status: false,

  //input values
  idNumber_input_value: '',
  id_input_error: '',

  //ID validation
  idValidationData: null,
  idValidationStatus: false,
  customer_id_info: {
    idNumber: '',
    documentStatus: 'N',
    existingCustomer: false
  },

  txn_reference: '',
  customerOutstandingData: null,
  totalOsAmount:'',

  //OTP validation 
  otpValidationData: null,
  cx_otp_number: '',
  otpValidationStatus: false,

  //Package details
  selectedDTVDataPackageDetails: [],

  //delivery Charge
  dtvDeliveryCharge: {
    delivery_charge: 0,
    delivery_charge_wotax: 0
  },

  //Input values
  input_mobile_number: '',

  //KYC related image capture
  opt_addressDifferent: false,
  opt_installationAddressDeferent: false,
  opt_customerFaceNotClear: false,
  opt_sameDayInstallation: false,
  enable_sameDayInstallation: true,
  sameDayInstallation_price: 0,

  //TODO
  selected_connection_number: { selected_number: "", blocked_id: "" },

  dtv_set_input_landLineNumber:'',
  email_input_value:'',

  //DTV data packages
  availableDTVDataPackagesList: {
    dropDownData: [],
    selectedValue: '',
    data_package_code: '',
    defaultIndex: -1,
    dataRental: '',
    packageList: '',
    idx: '',
    value:'',
    default_fullfil_status: '',
    selectedFullFillmentType: '',
    data_pakages_value: {
      package_rental: ''
    },
    selectedValueName: ''
  },
  
  dtv_drop_down: '',

  //available offers
  availableDtvOffers:{
    offer_name: '',
    offer_description: '',
    price: '',
    offer_code: '',
    offer_type: ''
  },

  multi_play_offer_type : 'MULTI_PLAY',

  //PackType
  dtv_pack_type: {
    dropDownData: [],
    selectedValue: '',
    defaultIndex: -1,
    packageType: []
  },

  //fulfillmentType
  dtv_fullfillment_type: {
    dropDownData: [],
    selectedValue: '',
    defaultIndex: -1,
    selectedFullFillmentType: [],
    package_code: '',
    default_fullfil_status: ''
  },

  //Additional channel 
  channel_selected_type: Constants.DTV_INDIVIDUAL_CHANNEL,
  additional_channel_list_data : {
    package_name: '',
    package_code: '',
    package_rental: '',
    genre_name_list: [],
    genre_code_list:  [],
    channel_list:  [],
    pack_list: [],
  },

  additional_channel_selected_genre : {
    genre_code: '',
    genre_name: ''
  },

  selected_channel_n_packs : {
    packs: [],
    channels: [],
    channels_in_pack: [],
  },

  selected_channel_total_package_rental: {
    additionalMonthlyRental : 0,
    totalDailyRental : 0
  },
  
  //Warranty Extension
  dtv_warranty_extensions_data: {},
  dtv_warranty_extensions: {
    dropDownData: [
    ],
    selectedValue: '',
    defaultIndex: -1,
    packCode: '',
    packPrice: 0,
    time_period: ''
  },

  //STB Upgrade
  stb_data: {
    stb_upgrade_available: false,
    stb_price: 0,
    stb_upgrade_text: 'Upgrade to hybrid box',
    additional_data: null,
    stb_type:'',
    free_visits_count: '',
    rcu_warranty: '',
    pcu_warranty:'',
    stb_warranty: '',
    lnb_warranty: '',
    lte_unit_eligibility: false,
    default_stb_type: ''
  },

  //Serial validation
  serialTextValue: '',
  bundleSerialValue: '',
  serialPackValue: '',
  accessoriesSerialValue: '',
  allSerialData: null,
  bundleSerialData: null,
  serialPackData: null,
  accessoriesSerialData: null,

  stb_upgrade_checked: false,
  first_reload_validation_error:'',
  enteredDTVVoucherCode:'',
  isValidDTVVoucherCode:false,

  //eoaf Serial
  eoafSerial: {
    material_serial_avaialable: false,
    material_serial: '',
    price: '',
    productFamily: '',
    sap_material_code: '',
    sap_material_serial: ''
  },

  //Customer mobile number
  input_MobileNumber: "",

  //Same day installation
  same_day_installation_data: {
    enabled: false,
    cut_off_time: '',
    price: 0,
  },
  customer_name : {
    value: '',
    error: ''
  },
  customer_address_line1 : {
    value: '',
    error: ''
  },
  customer_address_line2:  {
    value: '',
    error: ''
  },
  customer_address_line3 :  {
    value: '',
    error: ''
  },
  customer_city : {
    city_name: '',
    code: ''
  },
  customer_city_data: {
    all_data: [],
    city_names: [],
    error: ''
  }, 

  //reload amount
  reloadAmountValue: '',

  //voucher value
  voucherValue: '',
  reference_no:'',
  validatedVoucherCode:'',
  dtv_activation_status:false,  
  voucherCategory: '',
  voucherSubCategory: '',

  totalPaymentProps : {
    customerTotalPayment : '',
    outstandingFeeValue: '',
    deliveryInstallationCharge: '',
    unitPriceValue: '',
    voucherValue: '',
    warrentyExtCharge: '',
    reloadAmount: '',
    sameDayInstallationCharge: ''
  },

  //Dealer collection payment data
  dealerCollectionPaymentProps : {
    dealerCollectionPayment : '',
    outstandingFeeValue: '',
    unitPriceAdjustment: '',
    offerPriceValue: '',
    purchasedPrice: '',
    unitPriceAfterVoucerValue: '',
    deliveryInstallationCharge: '',
    sameDayInstallationCharge: '',
    warrentyExtCharge: '',
  }, 
  
  //eZ cash
  ez_cash_input_pin: '',
  eZCashInfo: null,
  ezCashAccountBalance: 0,
  didEzCashAccountValidated: false,
  accountNo: '',
  //TODO
  ezCashCheckBalance: {
    availableAmount: '',
    description: '',
    msisdn: '',
    status: ''
  },

  //DTV_DEPOSIT_AMOUNT

  deposit_amount: 0
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case DTV_SET_API_LOADING:
      return {
        ...state,
        apiLoading: action.payLoad,
        api_call_indicator_msg: action.message ? action.message : 'Loading'
      };
    case DTV_INPUT_ID_NUMBER:
      return {
        ...state,
        idNumber_input_value: action.payLoad
      };
    case DTV_SET_ID_NUMBER_ERROR:
      return {
        ...state,
        id_input_error: action.payLoad
      };
    case DTV_SELECTED_CONNECTION_TYPE:
      return {
        ...state,
        selected_connection_type: action.payLoad,
        id_input_error: ''
      };

    case DTV_SET_CUSTOMER_TYPE:
      if (action.payLoad === Constants.CX_TYPE_LOCAL) {
        return {
          ...state,
          customerType: Constants.CX_TYPE_LOCAL,
          customerIdType: Constants.CX_ID_TYPE_NIC,
          customerInfoSectionIdType: Constants.CX_INFO_TYPE_NIC,
          customerInfoSectionIdTypeCode: Constants.ID_TYPE_NIC,
          id_input_error: '',
          cx_otp_number: '',
          txn_reference: '',
        };
      } else {
        return {
          ...state,
          customerType: Constants.CX_TYPE_FOREIGNER,
          customerIdType: Constants.CX_ID_TYPE_PASSPORT,
          customerInfoSectionIdType: Constants.CX_INFO_TYPE_PASSPORT,
          customerInfoSectionIdTypeCode: Constants.ID_TYPE_PP,
          id_input_error: '',
          cx_otp_number: '',
          txn_reference: '',
        };
      }
    case DTV_SET_SECTIONS_VISIBLE:
      return {
        ...state,
        section_1_visible: action.payLoad.section_1,
        section_2_visible: action.payLoad.section_2,
        section_3_visible: action.payLoad.section_3
      };
    case GET_DTV_DATA_PACKAGES_LIST:
      return {
        ...state,
        availableDTVDataPackagesList: action.payLoad
      };
    case SET_DTV_DROPDOWN_INDEX:
      return {
        ...state,
        dtv_drop_down: action.payLoad
      };
    case RESET_DTV_DROPDOWN_INDEX:
      return {
        ...state,
        dtv_drop_down: []
      };
    case DTV_ID_VALIDATION_SUCCESS:
      return {
        ...state,
        idValidationData: action.payLoad,
        idValidationStatus: true,
        section_1_visible: false,
        section_2_visible: true,
        section_3_visible: false,
        sectionId_validation_status: true,
        id_input_error: '',
        customerOutstandingData: null,
        totalOsAmount:'',

        deposit_amount: INITIAL_STATE.deposit_amount,

        //DTV data packages
        availableDTVDataPackagesList: INITIAL_STATE.availableDTVDataPackagesList,
        
        //Package details
        selectedDTVDataPackageDetails: [],

        //delivery Charge
        dtvDeliveryCharge: INITIAL_STATE.dtvDeliveryCharge,

        //fulfillmentType
        dtv_fullfillment_type: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          selectedFullFillmentType: [],
          package_code: '',
          default_fullfil_status: ''
        },

        //PackType
        dtv_pack_type: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          packageType: []
        },
        //Additional channel 
        channel_selected_type: Constants.DTV_INDIVIDUAL_CHANNEL,
        additional_channel_list_data : INITIAL_STATE.additional_channel_list_data,

        additional_channel_selected_genre : INITIAL_STATE.additional_channel_selected_genre,
        selected_channel_n_packs : INITIAL_STATE.selected_channel_n_packs,
        
        //eoaf Serial
        eoafSerial: INITIAL_STATE.eoafSerial,

        //Customer info
        input_MobileNumber: INITIAL_STATE.input_MobileNumber,
        dtv_set_input_landLineNumber: INITIAL_STATE.dtv_set_input_landLineNumber,
        email_input_value: INITIAL_STATE.email_input_value,

        selected_channel_total_package_rental: {
          additionalMonthlyRental : 0,
          totalDailyRental : 0
        },
        //Warranty Extension
        dtv_warranty_extensions_data: {},
        dtv_warranty_extensions: {
          dropDownData: [
          ],
          selectedValue: '',
          defaultIndex: -1,
          packCode: '',
          packPrice: 0,
          time_period: ''
        },
        //STB Upgrade
        stb_data: {
          stb_upgrade_available: false,
          stb_price: 0,
          stb_upgrade_text: 'Upgrade to hybrid box',
          additional_data: null,
          stb_type:'',
          free_visits_count: '',
          rcu_warranty: '',
          pcu_warranty:'',
          stb_warranty: '',
          lnb_warranty: '',
          lte_unit_eligibility: false,
          default_stb_type: ''
        },
        stb_upgrade_checked: false,
        
        //KYC related image capture
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,
        opt_sameDayInstallation: false,
        enable_sameDayInstallation: true,
        sameDayInstallation_price : 0,

        //Same day installation
        same_day_installation_data: {
          enabled: false,
          cut_off_time: '',
          price: 0,
        },
        customer_name : {
          value: '',
          error: ''
        },
        customer_address_line1 : {
          value: '',
          error: ''
        },
        customer_address_line2:  {
          value: '',
          error: ''
        },
        customer_address_line3 :  {
          value: '',
          error: ''
        },
        customer_city : {
          city_name: '',
          code: ''
        },
        customer_city_data: {
          all_data: [],
          city_names: [],
          error: ''
        },

        //reload amount
        reloadAmountValue: '',

        //voucher value
        voucherValue: '',
        is_voucher_cancelled: false,
        
        //available offers
        availableDtvOffers:{
          offer_name: '',
          offer_description: '',
          price: '',
          offer_code: '',
          offer_type: ''
        },

        totalPaymentProps : {
          customerTotalPayment : '',
          outstandingFeeValue: '',
          deliveryInstallationCharge: '',
          unitPriceValue: '',
          voucherValue: '',
          warrentyExtCharge: '',
          reloadAmount: '',
          sameDayInstallationCharge: ''
        },  
      
        dealerCollectionPaymentProps : {
          dealerCollectionPayment : '',
          outstandingFeeValue: '',
          unitPriceAdjustment: '',
          offerPriceValue: '',
          purchasedPrice: '',
          unitPriceAfterVoucerValue: '',
          deliveryInstallationCharge: '',
          sameDayInstallationCharge: '',
          warrentyExtCharge: '',
        }, 

        //eZ cash
        ez_cash_input_pin: '',
        eZCashInfo: null,
        ezCashAccountBalance: 0,
        didEzCashAccountValidated: false,
        accountNo: '',

      };
    case DTV_ID_VALIDATION_FAIL:
      return {
        ...state,
        idValidationData: null,
        idValidationStatus: false,
        section_1_visible: true,
        section_2_visible: false,
        section_3_visible: false,
        sectionId_validation_status: false,
        cx_otp_number: '',
        txn_reference: '',
        customerOutstandingData: null,
        totalOsAmount:'',
        
        //fulfillmentType
        dtv_fullfillment_type: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          selectedFullFillmentType: [],
          package_code: '',
          default_fullfil_status: ''
        },

        //Package details
        selectedDTVDataPackageDetails: [],
        
        //delivery Charge
        dtvDeliveryCharge: INITIAL_STATE.dtvDeliveryCharge,

        //PackType
        dtv_pack_type: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          packageType: []
        },
        //Additional channel 
        channel_selected_type: Constants.DTV_INDIVIDUAL_CHANNEL,
        additional_channel_list_data : INITIAL_STATE.additional_channel_list_data,

        additional_channel_selected_genre : INITIAL_STATE.additional_channel_selected_genre,
        selected_channel_n_packs : INITIAL_STATE.selected_channel_n_packs,

        deposit_amount: INITIAL_STATE.deposit_amount,

        //eoaf Serial
        eoafSerial: INITIAL_STATE.eoafSerial,

        //Customer info
        input_MobileNumber: INITIAL_STATE.input_MobileNumber,
        dtv_set_input_landLineNumber: INITIAL_STATE.dtv_set_input_landLineNumber,
        email_input_value: INITIAL_STATE.email_input_value,

        selected_channel_total_package_rental: {
          additionalMonthlyRental : 0,
          totalDailyRental : 0
        },
        voucher_code_enabled: false,

        //Warranty Extension
        dtv_warranty_extensions_data: {},
        dtv_warranty_extensions: {
          dropDownData: [
          ],
          selectedValue: '',
          defaultIndex: -1,
          packCode: '',
          packPrice: 0,
          time_period: ''
        },

        //STB Upgrade
        stb_data: {
          stb_upgrade_available: false,
          stb_price: 0,
          stb_upgrade_text: 'Upgrade to hybrid box',
          additional_data: null,
          stb_type:'',
          free_visits_count: '',
          rcu_warranty: '',
          pcu_warranty:'',
          stb_warranty: '',
          lnb_warranty: '',
          lte_unit_eligibility: false,
          default_stb_type: ''
        },
        stb_upgrade_checked: false,

        //KYC related image capture
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,
        opt_sameDayInstallation: false,
        enable_sameDayInstallation: true,
        sameDayInstallation_price : 0,

        //DTV data packages
        availableDTVDataPackagesList: INITIAL_STATE.availableDTVDataPackagesList,

        //Same day installation
        same_day_installation_data: {
          enabled: false,
          cut_off_time: '',
          price: 0,
        },
        customer_name : {
          value: '',
          error: ''
        },
        customer_address_line1 : {
          value: '',
          error: ''
        },
        customer_address_line2:  {
          value: '',
          error: ''
        },
        customer_address_line3 :  {
          value: '',
          error: ''
        },
        customer_city : {
          city_name: '',
          code: ''
        },
        customer_city_data: {
          all_data: [],
          city_names: [],
          error: ''
        },

        //reload amount
        reloadAmountValue: '',

        //voucher value
        voucherValue: '',

        //available offers
        availableDtvOffers:{
          offer_name: '',
          offer_description: '',
          price: '',
          offer_code: '',
          offer_type: ''
        },

        totalPaymentProps : {
          customerTotalPayment : '',
          outstandingFeeValue: '',
          deliveryInstallationCharge: '',
          unitPriceValue: '',
          voucherValue: '',
          warrentyExtCharge: '',
          reloadAmount: '',
          sameDayInstallationCharge: ''
        },
        
        
        dealerCollectionPaymentProps : {
          dealerCollectionPayment : '',
          outstandingFeeValue: '',
          unitPriceAdjustment: '',
          offerPriceValue: '',
          purchasedPrice: '',
          unitPriceAfterVoucerValue: '',
          deliveryInstallationCharge: '',
          sameDayInstallationCharge: '',
          warrentyExtCharge: '',
        }, 

        //eZ cash
        ez_cash_input_pin: '',
        eZCashInfo: null,
        ezCashAccountBalance: 0,
        didEzCashAccountValidated: false,
        accountNo: '',

      };

    case DTV_SET_TXN_REFERENCE:
      return {
        ...state,
        txn_reference: action.payLoad
      };
    case DTV_SET_FIRST_RELOAD_VALIDATION_ERROR:
      return {
        ...state,
        first_reload_validation_error: action.payLoad,
        reloadAmountValue: ''
      };
    case DTV_SET_ID_NUMBER_VALIDATION_STATUS:
      return {
        ...state,
        sectionId_validation_status: action.payLoad
      };

    case DTV_SET_PRODUCT_INFO_VALIDATION_STATUS:
      return {
        ...state,
        sectionProductInfo_validation_status: action.payLoad
      };
    case DTV_SET_CUSTOMER_INFO_VALIDATION_STATUS:
      return {
        ...state,
        sectionCustomerInfo_validation_status: action.payLoad
      };
    case DTV_OTP_VALIDATION_SUCCESS:
      return {
        ...state,
        otpValidationData: action.payLoad,
        otpValidationStatus: true,
        customerOtpStatus: Constants.CX_OTP_VERIFIED
      };
    case DTV_OTP_VALIDATION_FAIL:
      return {
        ...state,
        otpValidationData: action.payLoad,
        otpValidationStatus: false,
        customerOtpStatus: Constants.CX_OTP_NOT_VERIFIED
      };
    case DTV_OUTSTANDING_CHECK_SUCCESS:
      return {
        ...state,
        customerOutstandingData: action.payLoad,
      };
    case DTV_SET_TOTAL_OUTSTANDING_AMOUNT:
      return {
        ...state,
        totalOsAmount: action.payLoad
      };
    case DTV_OUTSTANDING_CHECK_FAIL:
      return {
        ...state,
        customerOutstandingData: null,
        totalOsAmount: ''
      };

    case GET_DTV_DATA_PACKAGE_DETAIL:
      return {
        ...state,
        selectedDTVDataPackageDetails: action.payLoad
      };

    case GET_DTV_DELIVERY_CHARGE:
      return {
        ...state,
        dtvDeliveryCharge: action.payLoad
      };
    case DTV_PACK_TYPE:
      if (action.reset){
        return {
          ...state,
          dtv_pack_type : INITIAL_STATE.dtv_pack_type,
          input_MobileNumber: INITIAL_STATE.input_MobileNumber,
          dtv_set_input_landLineNumber: INITIAL_STATE.dtv_set_input_landLineNumber,
          email_input_value: INITIAL_STATE.email_input_value,
          first_reload_validation_error:''
        };
      } else {
        return {
          ...state,
          dtv_pack_type : action.payLoad
        };
      }
     
    case DTV_SAME_DAY_INSTALLATION_DATA:
      return {
        ...state,
        same_day_installation_data: action.payLoad
      };
    case DTV_FULLFILLMENT_TYPE:
      return {
        ...state,
        dtv_fullfillment_type: action.payLoad
      };
    case RESET_DTV_FULLFILLMENT_TYPE:
      return {
        ...state,
        dtv_fullfillment_type: INITIAL_STATE.dtv_fullfillment_type,
        //Customer info
        input_MobileNumber: INITIAL_STATE.input_MobileNumber,
        dtv_set_input_landLineNumber: INITIAL_STATE.dtv_set_input_landLineNumber,
        email_input_value: INITIAL_STATE.email_input_value,
        first_reload_validation_error:''

      };
    case DTV_SET_ADDITIONAL_CHANNEL_LIST_DATA:
      if (_.isNil(action.payLoad)) {
        return {
          ...state,
          additional_channel_list_data: INITIAL_STATE.additional_channel_list_data,
        };
      } else {
        return {
          ...state,
          additional_channel_list_data: action.payLoad,
        };
      }
    case DTV_SET_CHANNEL_GENRE:
      if (_.isNil(action.payLoad)) {
        return {
          ...state,
          additional_channel_selected_genre: INITIAL_STATE.additional_channel_selected_genre,
        };
      } else {
        return {
          ...state,
          additional_channel_selected_genre: action.payLoad,
        };
      }
    case DTV_SELECTED_CHANNELS_N_PACKS:
      return {
        ...state,
        selected_channel_n_packs: action.payLoad,
      };
    case DTV_FORCE_UPDATE_VIEW:
      return {
        ...state,
        random_value: action.payLoad
      };
    case SET_DTV_VOUCHER_CODE:
      return {
        ...state,
        enteredDTVVoucherCode: action.payLoad,
        isValidDTVVoucherCode:false
      };

    case SET_DTV_VOUCHER_CODE_ONLY:
      return {
        ...state,
        enteredDTVVoucherCode: action.payLoad,
      };

    case DTV_SET_INPUT_LANLINE_NUMBER:
      return {
        ...state,
        dtv_set_input_landLineNumber: action.payLoad
      };
    case DTV_INPUT_EMAIL:
      return {
        ...state,
        email_input_value: action.payLoad
      };
    case DTV_SET_CHANNEL_TYPE:
      return {
        ...state,
        channel_selected_type: action.payLoad
      };

    case DTV_SET_SHOW_VOUCHER_CODE_FIELD:
      return {
        ...state,
        voucher_code_enabled: action.payLoad
      };
    case DTV_SET_SELECTED_CHANNEL_TOTAL_PACKAGE: 
      return {
        ...state,
        selected_channel_total_package_rental: action.payLoad
      };

    case DTV_SET_STB_UPGRADE_DATA:
      return {
        ...state,
        stb_data: action.payLoad,
      };
    case DTV_STB_UPGRADE_CHECKBOX:
      return {
        ...state,
        stb_upgrade_checked: action.payLoad,
      };
    case DTV_SET_WARRANTY_EXTENSION_DATA:
      return {
        ...state,
        dtv_warranty_extensions:  action.payLoad,
        dtv_warranty_extensions_data:  action.apiData
      };
    case DTV_GET_OFFERS:
      return {
        ...state,
        availableDtvOffers: action.payLoad
      };
    case DTV_RESET_OFFERS:
      return {
        ...state,
        availableDtvOffers: {
          offer_name: '',
          offer_description: '',
          price: '',
          offer_code: '',
          offer_type: '',
        },

        first_reload_validation_error:'',
        //Customer info
        input_MobileNumber: INITIAL_STATE.input_MobileNumber,
        dtv_set_input_landLineNumber: INITIAL_STATE.dtv_set_input_landLineNumber,
        email_input_value: INITIAL_STATE.email_input_value,

        //Warranty Extension
        dtv_warranty_extensions_data: {},
        dtv_warranty_extensions: {
          dropDownData: [
          ],
          selectedValue: '',
          defaultIndex: -1,
          packCode: '',
          packPrice: 0,
          time_period: ''
        },
        //STB Upgrade
        stb_data: {
          stb_upgrade_available: false,
          stb_price: 0,
          stb_upgrade_text: 'Upgrade to hybrid box',
          additional_data: null,
          stb_type:'',
          free_visits_count: '',
          rcu_warranty: '',
          pcu_warranty:'',
          stb_warranty: '',
          lnb_warranty: '',
          lte_unit_eligibility: false,
          default_stb_type: ''
        },
        //eZ cash
        ez_cash_input_pin: '',
        eZCashInfo: null,
        ezCashAccountBalance: 0,
        didEzCashAccountValidated: false,
        accountNo: '',
        
      };

    case DTV_SET_CUSTOMER_INFO_ID_TYPE:
      return {
        ...state,
        customerInfoSectionIdType: action.payLoad.infoIdType,
        customerInfoSectionIdTypeCode: action.payLoad.code
      };

    //KYC related
    case DTV_SET_KYC_CHECK_BOX_POB_DIFFERENT:
      return {
        ...state,
        opt_addressDifferent: action.payLoad
      };
    case DTV_SET_KYC_CHECK_BOX_FACE_DIFFERENT:
      return {
        ...state,
        opt_customerFaceNotClear: action.payLoad
      };
    case DTV_SET_KYC_CHECK_BOX_SAME_DAY_INSTALLATION:
      return {
        ...state,
        opt_sameDayInstallation: action.payLoad
      };

    case DTV_SET_KYC_CHECK_BOX_POI_DIFFERENT:
      return {
        ...state,
        opt_installationAddressDeferent: action.payLoad
      };

    case DTV_RESET_KYC_CHECK_BOX_ALL:
      return {
        ...state,
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,
        opt_sameDayInstallation: false,
      };
    case SET_DTV_VOUCHER_CODE_VALIDITY:
      if (action.payLoad.success == true) {
        console.log("in valid success true");
        return {
          ...state,
          isValidDTVVoucherCode: true,
          // om_campaign_record_no: action.payLoad.om_campaign_record_no,
          // reservation_no: action.payLoad.reservation_no,
          // availableVaoucherCodeValue: action.payLoad,
          voucherValue: action.payLoad.voucher_value,
          validatedVoucherCode: action.payLoad.voucher_code,
          voucherCategory: action.payLoad.voucher_category,
          voucherSubCategory: action.payLoad.voucher_sub_category,
          reference_no: action.payLoad.reference_no,
        };
      } else {
        console.log("in valid success false");
        return {
          ...state,
          // enteredDTVVoucherCode: '',
          validatedVoucherCode: '',
          isValidDTVVoucherCode: false,
          voucherValue: '',
          voucherCategory: '',
          voucherSubCategory: '',
        };
      }

    case DTV_SERIAL_TEXT_VALUE:
      return {
        ...state,
        serialTextValue: action.payLoad,
      };
    case DTV_SERIAL_VALIDATION:
      if (action.payLoad !== undefined && action.payLoad !== null && action.payLoad) {
        return {
          ...state,
          serialValidation: action.payLoad,
          serialValidationStatus: true
        };
      } else {
        return {
          ...state,
          serialValidation: null,
          serialValidationStatus: false,
        };
      }
    case DTV_SET_SERIAL_TYPE_PACK:
      allSerialDataValues = [];
      packSerialDataValues = state.serialPackData;
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      if (packSerialDataValues !== null) {
        allSerialDataValues.push(packSerialDataValues);
      }
      return {
        ...state,
        serialPackData: action.payLoad,
        allSerialData: allSerialDataValues
      };
    case DTV_SET_SERIAL_TYPE_ACCESSORIES:

      allSerialDataValues = [];
      accessoriesSerialDataValues = state.accessoriesSerialData;
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      if (accessoriesSerialDataValues !== null) {
        allSerialDataValues.push(accessoriesSerialDataValues);
      }
      return {
        ...state,
        accessoriesSerialData: action.payLoad,
        allSerialData: allSerialDataValues
      };
    case DTV_SET_SERIAL_TYPE_BUNDLE:
      allSerialDataValues = [];
      bundleSerialDataValues = state.bundleSerialData;
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      if (bundleSerialDataValues !== null) {
        allSerialDataValues.push(bundleSerialDataValues);
      }
      return {
        ...state,
        bundleSerialData: action.payLoad,
        allSerialData: allSerialDataValues
      };
    case DTV_BUNDLE_SERIAL:
      return {
        ...state,
        bundleSerialValue: action.payLoad
      };
    case DTV_SERIAL_PACK:
      return {
        ...state,
        serialPackValue: action.payLoad
      };
    case DTV_ACCESSORIES_SERIAL:
      return {
        ...state,
        accessoriesSerialValue: action.payLoad
      };

    case DTV_RESET_INDIVIDUAL_SERIAL_DATA:
      allSerialDataValues = [];
      bundleSerialDataValues = state.bundleSerialData;
      packSerialDataValues = state.serialPackData;
      accessoriesSerialDataValues = state.accessoriesSerialData;

      if (action.payLoad  === 'DTV_STB') {
        console.log('DTV_RESET_INDIVIDUAL_SERIAL_DATA :: simSerial');
        if (packSerialDataValues !== null) {
          allSerialDataValues.push(packSerialDataValues);
        }
        return {
          ...state,
          accessoriesSerialValue: '',
          accessoriesSerialData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          bundleSerialValue: '',
          bundleSerialData: null,
          allSerialData: allSerialDataValues,
          serialPackValue: '',
          serialPackData: null
        };
      

      } else if (action.payLoad  === 'DTV_ACCESSORIES') {
        console.log('DTV_RESET_INDIVIDUAL_SERIAL_DATA :: accessories');
        if (accessoriesSerialDataValues !== null) {
          allSerialDataValues.push(accessoriesSerialDataValues);
        }
        return {
          ...state,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          accessoriesSerialValue: '',
          accessoriesSerialData: null,
          bundleSerialValue: '',
          bundleSerialData: null,
          allSerialData: allSerialDataValues
        };
      } else if (action.payLoad  === 'DTV_BUNDLE') {
        console.log('DTV_RESET_INDIVIDUAL_SERIAL_DATA :: bundle');
        if (bundleSerialDataValues !== null) {
          allSerialDataValues.push(bundleSerialDataValues);
        }
        return {
          ...state,
          accessoriesSerialValue: '',
          accessoriesSerialData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          serialPackValue: '',
          bundleSerialData: null,
          allSerialData: allSerialDataValues,
          bundleSerialValue: '',
          serialPackData: null
        };
      } else {
        console.log('DTV_RESET_INDIVIDUAL_SERIAL_DATA :: else');
        return {
          ...state,
          bundleSerialValue: '',
          bundleSerialData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          serialPackValue: '',
          accessoriesSerialValue: '',
          accessoriesSerialData: null,
          serialPackData: null,
          simSerialData: null,
          allSerialData: null
        };
      }

    case DTV_RESET_SERIALS_DATA:
      return {
        ...state,
        //Sections
        section_3_visible: false,
        sectionProductInfo_validation_status: false,
        sectionCustomerInfo_validation_status: false,

        // //Serial validation
        // serialTextValue: '',
        // bundleSerialValue: '',
        // serialPackValue: '',
        // accessoriesSerialValue: '',
        // allSerialData: null,
        // bundleSerialData: null,
        // serialPackData: null,
        // accessoriesSerialData: null,

      };
    case DTV_RESET_EOAF_SERIAL:
      return {
        ...state,
        eoafSerial: INITIAL_STATE.eoafSerial
      };
    case DTV_SET_CUSTOMER_NAME:
      return {
        ...state,
        customer_name: {
          value: action.payLoad.value,
          error: action.payLoad.error
        }
      };
    case DTV_SET_CUSTOMER_ADDRESS_LINE1:
      return {
        ...state,
        customer_address_line1: {
          value: action.payLoad.value,
          error: action.payLoad.error
        }
      };
    case DTV_SET_CUSTOMER_ADDRESS_LINE2:
      return {
        ...state,
        customer_address_line2: {
          value: action.payLoad.value,
          error: action.payLoad.error
        }
      };
    case DTV_SET_CUSTOMER_ADDRESS_LINE3:
      return {
        ...state,
        customer_address_line3: {
          value: action.payLoad.value,
          error: action.payLoad.error
        }
      };
    case DTV_SET_CUSTOMER_CITY:
      return {
        ...state,
        customer_city: action.payLoad
      };

    case DTV_CITY_SEARCH_DATA:
      return {
        ...state,
        customer_city_data: action.payLoad
      };   

    case DTV_EOAF_SERIAL:
      return {
        ...state,
        eoafSerial: action.payLoad
      };

    case DTV_SET_INPUT_MOBILE_NUMBER:
      return {
        ...state,
        input_MobileNumber: action.payLoad
      };

    case DTV_SET_RELOAD_AMOUNT:
      return {
        ...state,
        reloadAmountValue: action.payLoad
      };
    
    case RESET_DTV_VOUCHER_CODE_DATA:
      return {
        ...state,
        enteredDTVVoucherCode: '',
        validatedVoucherCode: '',
        isValidDTVVoucherCode: false
      };

    case DTV_RESET_VOUCHER_VALUE:
      return {
        ...state,
        voucherValue: '',
        enteredDTVVoucherCode: '',
        validatedVoucherCode: '',
        isValidDTVVoucherCode: false
      };
    
    case DTV_CUSTOMER_TOTAL_PAYMENT:
      return {
        ...state,
        totalPaymentProps : action.payLoad
      };

    case DTV_SET_INPUT_EZ_CASH_PIN:
      return {
        ...state,
        ez_cash_input_pin: action.payLoad
      };
    case DTV_RESET_EZCASH_BALANCE:
      return {
        ...state,
        eZCashInfo : null,
        didEzCashAccountValidated: false,
        ezCashAccountBalance: 0,
        accountNo: ''
      };
    case DTV_EZ_CASH_BALANCE:
      return {
        ...state,
        eZCashInfo: action.payLoad,
        didEzCashAccountValidated: action.validated,
        ezCashAccountBalance: action.balance,
        accountNo: action.account_no
      };
    case DTV_RESET_SAMEDAY_INSTALLATION_STATUS:
      return {
        ...state,
        opt_sameDayInstallation: false
      };
    case DTV_RESET_VALIDATE_VOUCHER_CODE_STATUS:
      return {
        ...state,
        isValidVoucherCode: false
      };
    case DTV_OTP_NUMBER:
      return {
        ...state,
        cx_otp_number: action.payLoad
      };
    case DTV_DEPOSIT_AMOUNT:
      return {
        ...state,
        deposit_amount: action.payLoad
      };
    case DTV_RESET_DEPOSIT_AMOUNT:
      return {
        ...state,
        deposit_amount: INITIAL_STATE.deposit_amount
      };
    case RESET_DTV_DATA:
      return INITIAL_STATE;
      
    default:
      return state;
  }
};