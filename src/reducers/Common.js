import {
  COMMON_SET_API_LOADING,
  RESET_COMMON,
  COMMON_SET_KYC_NIC_FRONT,
  COMMON_SET_KYC_NIC_BACK,
  COMMON_SET_KYC_PASSPORT,
  COMMON_SET_KYC_DRIVING_LICENCE,
  COMMON_SET_KYC_PROOF_OF_BILLING,
  COMMON_SET_KYC_CUSTOMER_IMAGE,
  COMMON_SET_KYC_SIGNATURE,
  COMMON_SET_KYC_ADDITIONAL_POB,
  RESET_KYC_IMAGES,
  COMMON_SET_ACTIVITY_START_TIME,
  COMMON_SET_ACTIVITY_END_TIME,
  COMMON_RESET_KYC_SIGNATURE,
  COMMON_SET_BANNER_IMAGES,
} from '../actions/types';

const INITIAL_STATE = {
  api_loading: false,
  kyc_nic_front: null,
  kyc_nic_back: null,
  kyc_passport: null,
  kyc_driving_licence: null,
  kyc_proof_of_billing: null,
  kyc_customer_image: null,
  kyc_signature: null,
  kyc_additional_pob: null,
  common_activity_start_time: '',
  common_activity_end_time: '',
  bannerImages: null,

 
};

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case COMMON_SET_API_LOADING:
      console.log('#>>>>> GET_COMMON_API_LOADING <<<<<#');
      return {
        ...state,
        api_loading: action.payLoad
      };

    case COMMON_SET_ACTIVITY_START_TIME:
      console.log('#>>>>> COMMON_SET_ACTIVITY_START_TIME <<<<<#');
      return {
        ...state,
        common_activity_start_time: action.payLoad
      };

    case COMMON_SET_ACTIVITY_END_TIME:
      console.log('#>>>>> COMMON_SET_ACTIVITY_END_TIME <<<<<#');
      return {
        ...state,
        common_activity_end_time: action.payLoad
      };

    case COMMON_SET_KYC_NIC_FRONT:
      console.log('#>>>>> COMMON_SET_KYC_NIC_FRONT <<<<<#');
      return {
        ...state,
        kyc_nic_front: action.payLoad
      };

    case COMMON_SET_KYC_NIC_BACK:
      console.log('#>>>>> COMMON_SET_KYC_NIC_BACK <<<<<#');
      return {
        ...state,
        kyc_nic_back: action.payLoad
      };
    case COMMON_SET_KYC_PASSPORT:
      console.log('#>>>>> COMMON_SET_KYC_PASSPORT <<<<<#');
      return {
        ...state,
        kyc_passport: action.payLoad
      };
    case COMMON_SET_KYC_DRIVING_LICENCE:
      console.log('#>>>>> COMMON_SET_KYC_DRIVING_LICENCE <<<<<#');
      return {
        ...state,
        kyc_driving_licence: action.payLoad
      };
    case COMMON_SET_KYC_PROOF_OF_BILLING:
      console.log('#>>>>> COMMON_SET_KYC_PROOF_OF_BILLING <<<<<#');
      return {
        ...state,
        kyc_proof_of_billing: action.payLoad
      };
    case COMMON_SET_KYC_CUSTOMER_IMAGE:
      console.log('#>>>>> COMMON_SET_KYC_CUSTOMER_IMAGE <<<<<#');
      return {
        ...state,
        kyc_customer_image: action.payLoad
      };
    case COMMON_SET_KYC_SIGNATURE:
      console.log('#>>>>> COMMON_SET_KYC_SIGNATURE <<<<<#');
      return {
        ...state,
        kyc_signature: action.payLoad
      };
    case COMMON_SET_KYC_ADDITIONAL_POB:
      console.log('#>>>>> COMMON_SET_KYC_ADDITIONAL_POB <<<<<#');
      return {
        ...state,
        kyc_additional_pob: action.payLoad
      };

    case RESET_KYC_IMAGES:
      console.log('#>>>>> RESET_KYC_IMAGES <<<<<#');
      return {
        ...state,
        kyc_nic_front: null,
        kyc_nic_back: null,
        kyc_passport: null,
        kyc_driving_licence: null,
        kyc_proof_of_billing: null,
        kyc_customer_image: null,
        kyc_signature: null,
        kyc_additional_pob: null,
      };

    case COMMON_RESET_KYC_SIGNATURE:
      console.log('#>>>>> COMMON_RESET_KYC_SIGNATURE <<<<<#');
      return {
        ...state,
        kyc_signature: null,
      };

    case COMMON_SET_BANNER_IMAGES:
      console.log('#>>>>> COMMON_SET_BANNER_IMAGES <<<<<#');
      if (action.payLoad ==null) {
        return {
          ...state,
          bannerImages: null,
        };
      } else {
        return {
          ...state,
          bannerImages: action.payLoad,
        };
      }
    
    case RESET_COMMON:
      console.log('#>>>>> RESET_COMMON <<<<<#');
      return INITIAL_STATE;
    default:
      return state;
  }
};