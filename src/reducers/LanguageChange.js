import { GET_CURRENT_LANGUAGE } from '../actions/types';

const INITIAL_STATE = {
  current_lang: 'en',
  langValue: 0
};
export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CURRENT_LANGUAGE:
      return {
        ...state,
        current_lang: action.payLoad.value,
        langValue: action.payLoad.index
      };

    default:
      return state;
  }
};
