import {
  
  RESET_DELIVERY,
  DELIVERY_SET_API_CALL,
  GET_PENDING_WORK_ORDERS_SUCCESS,
  GET_PENDING_WORK_ORDERS_FAIL,
  GET_WORK_ORDER_STATUS,
  API_CALL_SUCCESS,
  API_CALL_FAIL,
  GET_FILTER_WORK_ORDERS,
  GET_CURRENT_WO_ID,
  GET_CURRENT_WO_JOBID,
  GET_WOM_API_ORDER,
  GET_CURRENT_WO_JOB_STATUS,
  GET_CURRENT_WO_BASIC_INFO,
  GET_CURRENT_WO_SLA_BREACH_STATUS,
  DELIVERY_SHOW_LOADING_INDICATER,
  DELIVERY_HIDE_LOADING_INDICATER,
  DELIVERY_GET_WO_COUNT,
  DELIVERY_API_CALL_FAIL,
  DELIVERY_CCBS_ORDER_ID
} from '../actions/types';

const INITIAL_STATE = {
  apiLoading: false,
  api_call_indicator_msg: 'Loading',
  pendingWorkOrders: [],
  workOrdersSearchResults: [],
  isLoading: true,
  showLoadingIndicater: false,
  apiData: {},
  workOrderState: 1,
  currentWorkOrderId: '',
  currentWorkOrderJobId: '',
  currentWorkOrderJobStatus: '',
  currentWorkOrderSlaBreachStatus: false,
  currentWorkOrderSlaTime: '',
  currentWorkOrderBasicInfo: {
    accountNo: '',
    saleType: '',
    woBisUnit: '',
    idNumber: {
      displayName: '',
      key: '',
      type: '',
      value: ''
    },
    pmsId: '',
    pos_invoice_no: '',
    order_line_item_no: '',
    issued_user: '',
    ccbs_order_id: '',
    wom_sap_material_code: ''
  },
  workOrderRetrieveStatus: null,
  pendingOrders: {
    count: 0,
    orderList: []
  },
  dispatchedOrders: {
    count: 0,
    orderList: []
  },
  inprogressOrders: {
    count: 0,
    orderList: []
  },
  totalWoCount: {
    PENDING: 0,
    DISPATCHED: 0,
    INPROGRESS: 0
  },

  isDeliveryApiFail: true,
  wom_api_order_details: [],
  wom_api_order_customerDetails: {},
  wom_api_order_addionalDetails: [],
  ccbsOrderId: null
  
};

export default(state = INITIAL_STATE, action) => {

  switch (action.type) {
    case DELIVERY_SET_API_CALL:
      return {
        ...state,
        apiLoading: action.payLoad,
        api_call_indicator_msg: action.message ? action.message : 'Loading'
      };
    case GET_PENDING_WORK_ORDERS_SUCCESS:
      return {
        ...state,
        pendingWorkOrders: action.payLoad,
        workOrderRetrieveStatus: true
      };
    case GET_PENDING_WORK_ORDERS_FAIL:
      return {
        ...state,
        pendingWorkOrders: action.payLoad,
        workOrderRetrieveStatus: false
      };
    case GET_FILTER_WORK_ORDERS:
      return {
        ...state,
        workOrdersSearchResults: action.payLoad
      };
    case GET_WORK_ORDER_STATUS:
      return {
        ...state,
        workOrderState: action.payLoad
      };

    case DELIVERY_GET_WO_COUNT:
      return {
        ...state,
        totalWoCount: action.payLoad
      };
    case GET_CURRENT_WO_ID:
      return {
        ...state,
        currentWorkOrderId: action.payLoad
      };
    case GET_CURRENT_WO_JOBID:
      return {
        ...state,
        currentWorkOrderJobId: action.payLoad
      };
    case GET_CURRENT_WO_JOB_STATUS:
      return {
        ...state,
        currentWorkOrderJobStatus: action.payLoad
      };
    case GET_CURRENT_WO_SLA_BREACH_STATUS:
      return {
        ...state,
        currentWorkOrderSlaBreachStatus: action.payLoad.slaStatus,
        currentWorkOrderSlaTime: action.payLoad.slaTime
      };
    case GET_CURRENT_WO_BASIC_INFO:
      return {
        ...state,
        currentWorkOrderBasicInfo: action.payLoad
      };
    case GET_WOM_API_ORDER:
      return {
        ...state,
        wom_api_order_details: action.payLoad.details,
        wom_api_order_customerDetails: action.payLoad.customerDetails,
        wom_api_order_addionalDetails: action.payLoad.addionalDetails
      };
    case API_CALL_SUCCESS:
      return {
        ...state,
        apiData: action.payLoad
      };
    case DELIVERY_CCBS_ORDER_ID:
      return {
        ...state,
        ccbsOrderId: action.payLoad
      };
    case API_CALL_FAIL:
      return {
        ...state,
        apiData: action.payLoad
      };

    case DELIVERY_SHOW_LOADING_INDICATER:
      return {
        ...state,
        showLoadingIndicater: true
      };
    case DELIVERY_HIDE_LOADING_INDICATER:
      return {
        ...state,
        showLoadingIndicater: false
      };
    case DELIVERY_API_CALL_FAIL:
      return {
        ...state,
        isDeliveryApiFail: action.payLoad
      };
    case RESET_DELIVERY:
      console.log("Reset delivery successful.");
      return INITIAL_STATE;
    default:
      return state;
  }
};