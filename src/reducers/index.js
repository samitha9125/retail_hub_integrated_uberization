import { combineReducers } from 'redux';
import Common from './Common';
import Genaral from './Genaral';
import BillPayment from './BillPayment';
import SimChange from './SimChange';
import MobileActivation from './MobileActivation';
import PinChange from './PinChange';
import Auth from './Auth';
import SelectLanguage from './LanguageChange';
import DeliveryApp from './DeliveryApp';
import TransactionHistory from './TransactionHistory';
import LTEActivation from './LTEActivation';
import DtvActivation from './DtvActivation';
import Configuration from './Configuration';
//CFSS related
import StockAcceptance from './stockAccpetance';
import AccessorySales from './AccessorySales';
import WarrantyReplacement from './WarrantyReplacement';
import PendingWO from './PendingWO'

export default combineReducers({

  common: Common,
  configuration: Configuration,
  genaral: Genaral,
  bill: BillPayment,
  sim: SimChange,
  mobile: MobileActivation,
  auth: Auth,
  pin: PinChange,
  lang: SelectLanguage,
  delivery: DeliveryApp,
  transHistory: TransactionHistory,
  lteActivation: LTEActivation,
  dtvActivation: DtvActivation,
  //CFSS related
  acceptStock: StockAcceptance,
  accessorySales: AccessorySales,
  warrantyReplacement: WarrantyReplacement,
  wom: PendingWO, 
});
