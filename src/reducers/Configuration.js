import {
  CONFIG_SET_ACTIVITY_START_TIME,
  CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT,
  CONFIG_LTE_SET_CONFIGURATION,
  RESET_CONFIGURATION,
  CONFIG_LTE_SET_ACTIVATION_STATUS,
  CONFIG_DTV_SET_ACTIVATION_STATUS
} from '../actions/types';

const INITIAL_STATE = {
  activity_start_time: '',
  unique_id: '',
  default_ez_cash_account: '',
  default_rapid_ez_account: '',
  //LTE configuration values
  lte_activation_status:false,
  dtv_activation_status:false,
  lte_configuration : {
    serial_type: {
      enable: true,
      list: [
        'Individual serial',
        'Bundle serial'
      ],
      selected: 0
    },
    agreement_document: {
      PREPAID: 'https://www.dialog.lk/application-agreement-for-a-broadband-internet-prepaid-solution',
      POSTPAID: 'https://www.dialog.lk/tc/application-agreement-for-a-broadband-internet-voice-solution'
    }
  },
};

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONFIG_SET_ACTIVITY_START_TIME:
      return {
        ...state,
        activity_start_time: action.payLoad,
        unique_id:  action.payLoad,
      };

    case CONFIG_SET_DEFAULT_EZ_CASH_ACCOUNT:
      return {
        ...state,
        default_ez_cash_account: action.payLoad
      };

    case CONFIG_LTE_SET_CONFIGURATION:
      return {
        ...state,
        lte_configuration: action.status ? action.payLoad : state.lte_configuration,
  
      };
    case CONFIG_LTE_SET_ACTIVATION_STATUS:
      return {
        ...state,
        lte_activation_status:action.payLoad
      };
    case CONFIG_DTV_SET_ACTIVATION_STATUS:
      return {
        ...state,
        dtv_activation_status:action.payLoad
      };
    case RESET_CONFIGURATION:
      console.log('#>>>>> RESET_CONFIGURATION <<<<<#');
      return INITIAL_STATE;

    default:
      return state;
  }
};
