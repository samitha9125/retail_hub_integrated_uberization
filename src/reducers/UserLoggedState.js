export default(state = null, action) => {

  switch (action.type) {
    case 'set_user_state':
      return action.payLoad;
    case 'get_user_state':
      return state;
    default:
      return state;
  }
};