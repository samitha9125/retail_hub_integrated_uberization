import {
  LTE_API_CALL,
  RESET_LTE_DATA,
  LTE_SELECTED_CONNECTION_TYPE,
  LTE_SET_CUSTOMER_INFO_ID_TYPE,
  LTE_SERIAL_SCAN_MODE,
  LTE_SET_CUSTOMER_TYPE,
  LTE_SET_ID_TYPE,
  LTE_SET_ID_NUMBER_VALIDATION_STATUS,
  LTE_SET_PRODUCT_INFO_VALIDATION_STATUS,
  LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS,
  LTE_VISIBLE_SECTION_1,
  LTE_VISIBLE_SECTION_2,
  LTE_VISIBLE_SECTION_3,
  LTE_SET_SECTIONS_VISIBLE,
  LTE_INPUT_ID_NUMBER,
  LTE_SET_ID_NUMBER_ERROR,
  LTE_INPUT_EMAIL,
  LTE_INPUT_EZ_CASH_PIN,
  LTE_SET_CUSTOMER_ID_INFO,
  LTE_ID_VALIDATION_SUCCESS,
  LTE_ID_VALIDATION_FAIL,
  LTE_OUTSTANDING_CHECK_SUCCESS,
  LTE_OUTSTANDING_CHECK_FAIL,
  LTE_OTP_VALIDATION_SUCCESS,
  LTE_OTP_VALIDATION_FAIL,
  LTE_OTP_NUMBER,
  LTE_SERIAL_VALIDATION,
  LTE_SERIAL_TEXT_VALUE,
  LTE_BUNDLE_SERIAL,
  LTE_SERIAL_PACK,
  LTE_SIM_SERIAL,
  LTE_INPUT_ANALOG_PHONE_SERIAL,
  LTE_SET_SERIAL_TYPE_BUNDLE,
  LTE_SET_SERIAL_TYPE_PACK,
  LTE_SET_SERIAL_TYPE_SIM,
  LTE_SET_SERIAL_TYPE_ANALOG_PHONE,
  LTE_REST_ANALOG_PHONE_SERIAL_DATA,
  LTE_RESET_SERIALS,
  LTE_RESET_INDIVIDUAL_SERIAL_DATA,
  LTE_RESET_SERIALS_DATA,
  LTE_EZ_CASH_BALANCE,
  LTE_FORCE_UPDATE_PROPS,
  LTE_SET_RELOAD,
  GET_LTE_OFFERS_LIST,
  GET_LTE_OFFER_DETAIL,
  SET_LTE_MULTIPLAY_OFFER_DATA,
  GET_LTE_DATA_PACKAGE_DETAIL,
  GET_LTE_DATA_PACKAGES_LIST,
  GET_LTE_VOICE_PACKAGE_DETAIL,
  GET_LTE_VOICE_PACKAGES_LIST,
  GET_LTE_VOUCHER_CODE_STATUS,
  SET_LTE_VOUCHER_CODE,
  SET_LTE_VALIDATED_VOUCHER_CODE,
  SET_LTE_VOUCHER_CODE_VALIDITY,
  VALIDATE_VOUCHER_CODE_TRUE,
  SET_LTE_TELCO_AREA_DATA,
  SET_LTE_SELECTED_AREA,
  GET_LTE_PACKAGE_DEPOSITS_FAIL,
  GET_LTE_PACKAGE_DEPOSITS_SUCCESS,
  LTE_SET_DATA_PACKAGE_CODE,
  LTE_SET_VOICE_PACKAGE_CODE,

  SET_LTE_VOICE_CHECKBOX_DATA,
  GET_EZCASH_CHECK_BALANCE_SUCCESS,
  GET_EZCASH_CHECK_BALANCE_FAIL,
  GET_EZCASH_REVERSE_TRANSACTION_SUCCESS,
  GET_EZCASH_REVERSE_TRANSACTION_FAIL,
  GET_EZCASH_SUBMIT_TRANSACTION_SUCCESS,
  GET_EZCASH_SUBMIT_TRANSACTION_FAIL,
  GET_LTE_DEVICE_SELLING_PRICE_SUCCESS,
  GET_LTE_DEVICE_SELLING_PRICE_FAIL,
  LTE_SET_INPUT_LANLINE_NUMBER,
  LTE_SET_INPUT_MOBILE_NUMBER,

  LTE_RESERVE_NUMBER_POOL,
  LTE_SET_TOTAL_PAYMENT,
  LTE_BILLING_CYCLE_NUMBER,
  SET_SELECTED_CONNECTION_NUMBER,
  RESET_SELECTED_CONNECTION_NUMBER,

  LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT,
  LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT,
  LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT,
  LTE_RESET_KYC_CHECK_BOX_ALL,
  LTE_SET_REFERENCE_ID,
  SET_LTE_VOUCHER_CODE_GROUP_NAME,
  SET_LTE_ANALOG_PHONE_VISIBILITY,
  SET_LTE_VOUCHER_OFFER_CODE,
  LTE_SET_EZCASH_TOTAL_PAYMENT,
  LTE_RESET_EZCASH_BALANCE,
  LTE_RESET_VOUCHER_VALUE,
  RESET_LTE_DATA_PACKAGES_DATA_RENTAL,
  LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS,
  LTE_SET_VOUCHER_CANCELLATION_STATUS


} from '../actions/types';
import Constants from '../config/constants';

let packSerialDataValues = null;
let simSerialDataValues = null;
let bundleSerialDataValues = null;
let allSerialDataValues = [];


const INITIAL_STATE = {
  apiLoading: false,
  api_call_indicator_msg: 'Loading',
  randomValue: 0,
  selected_connection_type: Constants.PREPAID,
  customerIdType: Constants.CX_ID_TYPE_NIC,
  customerInfoSectionIdType: Constants.CX_INFO_TYPE_NIC,
  customerInfoSectionIdTypeCode: Constants.ID_TYPE_NIC,
  customerType: Constants.CX_TYPE_LOCAL,
  customerExistence: Constants.CX_EXIST_NO,
  customerOtpStatus: Constants.CX_OTP_NOT_APPLICABLE,
  id_type: Constants.CX_ID_TYPE_NIC,

  //Sections
  section_1_visible: true,
  section_2_visible: false,
  section_3_visible: false,
  sectionId_validation_status: false,
  sectionProductInfo_validation_status: false,
  sectionCustomerInfo_validation_status: false,

  //Input values
  idNumber_input_value: '',
  id_input_error: '',
  email_input_value: '',
  lte_set_input_MobileNumber: '',
  lte_set_input_landLineNumber: '',
  ez_cash_input_value: '',

  //ID validation
  idValidationData: null,
  idValidationStatus: false,
  customer_id_info: {
    idNumber: '',
    documentStatus: 'N',
    existingCustomer: false
  },
  customerOutstandingData: null,

  //OTP validation
  otpValidationData: null,
  cx_otp_number: '',
  otpValidationStatus: false,

  //Serial validation
  selected_serial_type: null,
  serialValidation: null,
  serialValidationStatus: false,
  serialTextValue: '',
  bundleSerialValue: '',
  serialPackValue: '',
  simSerialValue: '',
  analogPhoneSerialValue: '',
  allSerialData: null,
  bundleSerialData: null,
  serialPackData: null,
  simSerialData: null,
  analogPhoneSerialData: null,

  //Offers
  availableLTEOffersList: {
    dropDownData: [],
    selectedValue: '',
    offer_Code: '',
    offerPrice: '',
    defaultIndex: -1,
    offerlist: {
      package_rental: '',
    },
    additionalInfo: {
      audience_group_name: '',
      om_campaign_record_no: '',
      reservation_no: '',
    }
  },
  multiplayOfferData: {
    audience_group_name: '',
    om_campaign_record_no: '',
    reservation_no: '',
  },
  selectedLTEOfferDetails: [],
  selectedLTEOfferDetailsObject: [],
  selected_offer_code: '',

  //Voucher codes
  voucherCodeAvailable: false,
  enteredVoucherCode: '',
  validatedVoucherCode: '',
  same_voucher_code_validate:'',
  isValidVoucherCode: false,
  availableLTEDataPackagesList: {
    dropDownData: [],
    selectedValue: '',
    data_package_code: '',
    defaultIndex: -1,
    dataRental: '',
    data_pakages_value: {
      package_rental: ''
    }
  },
  voucher_group_name: '',
  analog_phone_visibility: false,
  voucher_code_validity_offer_code: '',
  selectedLTEDataPackageDetails: [],

  //Voice packages
  selectedLTEVoicePackageDetails: [],
  lteVoiceCheckboxData: {
    addVoiceChecked: false,
    analogPhoneChecked: false
  },

  availableLTEVoicePackagesList: {
    dropDownData: [],
    selectedValue: '',
    voice_package_code: '',
    defaultIndex: -1,
    packageList: {
      package_rental: '',
    }
  },

  selected_voice_package_code: '',

  //Data sachet / reload
  reload: {
    offer_key: null,
    offer_val: null,
  },
  reloadAmount: '',
  reloadOfferCode: '',

  //Area search
  telcoAreaData: {
    cityNames: [],
    areaCodes: [],
    searchResult: [],
    error: ''
  },
  lteSelectedArea: '',

  //Number selection
  reserved_number_list: {
    reserved_number_list: {},
    number_list: []
  },

  selected_connection_number: { selected_number: "", blocked_id: "" },
  reservation_no: '',
  availableVaoucherCodeValue: '',
  voucherValue: '',
  reference_no: '',

  om_campaign_record_no: '',

  //Billing cycle
  billingCycleDropdownDatalist: [],
  billingCycleDropdownData: {
    dropDownData: [],
    selectedValue: '',
    defaultIndex: -1
  },

  lte_billing_cycle_number: {
    dropDownData: [],
    selectedValue: '',
    defaultIndex: -1,
    dates: [],
    selectedBillingCycleCode: ''
  },

  lte_deposit_value: {
    depositAmount: '',
    depositType: ''
  },

  //KYC related image capture
  opt_addressDifferent: false,
  opt_installationAddressDeferent: false,
  opt_customerFaceNotClear: false,

  //eZ cash
  eZCashInfo: null,
  ezCashAccountBalance: 0,
  account_no: '',
  ezCashCheckBalance: {
    availableAmount: '',
    description: '',
    msisdn: '',
    status: ''
  },

  ezCashreverseTransaction: {
    error_description: '',
    tx_serial_no: '',
    status: ''
  },

  ezCashSubmitTransaction: {
  },

  lteDeviceSellingPrice: {
    delta_value: '',
    selling_price: ''
  },

  //Total Payment
  lte_total_payment_amount: 0,
  lteSetTotalPayment: {
    unitPriceValue: '',
    dataplanSachetValue: '',
    outstandingFeeValue: '',
    depositAmount: '',
    totalPayValue: ''
  },

  //ezcash total payment
  eZCashTotalPaymentProps: {
    eZCashtotalPay: '',
    depositAmount: '',
    outstandingFeeValue: '',
    unitPriceAdjustment: '',
    deviceSellingPrice: '',
    serialPackPrice: '',
    simSerialPrice: '',
    bundleSerialPrice: '',
    unitPriceValue: ''
  },
  is_voucher_cancelled: false

};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case LTE_API_CALL:
      return {
        ...state,
        apiLoading: action.payLoad,
        api_call_indicator_msg: action.message ? action.message : 'Loading'
      };
    case LTE_SET_ID_NUMBER_VALIDATION_STATUS:
      return {
        ...state,
        sectionId_validation_status: action.payLoad
      };
    case LTE_SET_PRODUCT_INFO_VALIDATION_STATUS:
      return {
        ...state,
        sectionProductInfo_validation_status: action.payLoad
      };
    case LTE_SET_CUSTOMER_INFO_VALIDATION_STATUS:
      return {
        ...state,
        sectionCustomerInfo_validation_status: action.payLoad
      };
    case LTE_VISIBLE_SECTION_1:
      return {
        ...state,
        section_1_visible: action.payLoad
      };
    case LTE_VISIBLE_SECTION_2:
      return {
        ...state,
        section_2_visible: action.payLoad
      };
    case LTE_VISIBLE_SECTION_3:
      return {
        ...state,
        section_3_visible: action.payLoad
      };

    case LTE_SET_SECTIONS_VISIBLE:
      return {
        ...state,
        section_1_visible: action.payLoad.section_1,
        section_2_visible: action.payLoad.section_2,
        section_3_visible: action.payLoad.section_3
      };
    case LTE_SELECTED_CONNECTION_TYPE:
      return {
        ...state,
        selected_connection_type: action.payLoad,
        id_input_error: ''
      };

    case LTE_INPUT_ID_NUMBER:
      return {
        ...state,
        idNumber_input_value: action.payLoad
      };
    case LTE_SET_ID_NUMBER_ERROR:
      return {
        ...state,
        id_input_error: action.payLoad
      };
    case LTE_INPUT_EMAIL:
      return {
        ...state,
        email_input_value: action.payLoad
      };
    case LTE_INPUT_EZ_CASH_PIN:
      return {
        ...state,
        ez_cash_input_value: action.payLoad
      };
    case LTE_SERIAL_SCAN_MODE:
      return {
        ...state,
        selected_serial_type: action.payLoad
      };

    case LTE_SET_CUSTOMER_INFO_ID_TYPE:
      return {
        ...state,
        customerInfoSectionIdType: action.payLoad.infoIdType,
        customerInfoSectionIdTypeCode: action.payLoad.code,
      };

    case LTE_SET_VOUCHER_CANCELLATION_STATUS:
      if (action.payLoad == false) {
        return {
          ...state,
          is_voucher_cancelled: action.payLoad
        };
      }
      return {
        ...state,
        is_voucher_cancelled: action.payLoad,
        enteredVoucherCode: '',
        validatedVoucherCode: '',
        same_voucher_code_validate:'',
        isValidVoucherCode: false
      };

    case LTE_SET_CUSTOMER_TYPE:
      if (action.payLoad === Constants.CX_TYPE_LOCAL) {
        return {
          ...state,
          customerType: Constants.CX_TYPE_LOCAL,
          customerIdType: Constants.CX_ID_TYPE_NIC,
          customerInfoSectionIdType: Constants.CX_INFO_TYPE_NIC,
          customerInfoSectionIdTypeCode: Constants.ID_TYPE_NIC,
          id_input_error: '',
          cx_otp_number: '',
        };
      } else {
        return {
          ...state,
          customerType: Constants.CX_TYPE_FOREIGNER,
          customerIdType: Constants.CX_ID_TYPE_PASSPORT,
          customerInfoSectionIdType: Constants.CX_INFO_TYPE_PASSPORT,
          customerInfoSectionIdTypeCode: Constants.ID_TYPE_PP,
          id_input_error: '',
          cx_otp_number: '',
        };
      }
    case LTE_SET_CUSTOMER_ID_INFO:
      return {
        ...state,
        customer_id_info: action.payLoad
      };
    case LTE_SET_ID_TYPE:
      return {
        ...state,
        id_type: action.payLoad,
        customerIdType: action.payLoad,
      };
    case LTE_ID_VALIDATION_SUCCESS:
      return {
        ...state,
        idValidationData: action.payLoad,
        idValidationStatus: true,
        section_1_visible: false,
        section_2_visible: true,
        section_3_visible: false,
        sectionId_validation_status: true,
        id_input_error: '',
        customerExistence: action.payLoad.cxExistStatus
      };
    case LTE_ID_VALIDATION_FAIL:
      return {
        ...state,
        idValidationData: null,
        idValidationStatus: false,
        section_1_visible: true,
        section_2_visible: false,
        section_3_visible: false,
        sectionId_validation_status: false,
        cx_otp_number: '',
        customerOutstandingData: null,
        customerExistence: null
      };
    case LTE_OUTSTANDING_CHECK_SUCCESS:
      return {
        ...state,
        customerOutstandingData: action.payLoad,
      };
    case LTE_OUTSTANDING_CHECK_FAIL:
      return {
        ...state,
        customerOutstandingData: null,
      };
    case LTE_OTP_VALIDATION_SUCCESS:
      return {
        ...state,
        otpValidationData: action.payLoad,
        otpValidationStatus: true,
        customerOtpStatus: Constants.CX_OTP_VERIFIED
      };
    case LTE_OTP_VALIDATION_FAIL:
      return {
        ...state,
        otpValidationData: action.payLoad,
        otpValidationStatus: false,
        customerOtpStatus: Constants.CX_OTP_NOT_VERIFIED
      };
    case LTE_SERIAL_VALIDATION:
      if (action.payLoad !== undefined && action.payLoad !== null && action.payLoad) {
        return {
          ...state,
          serialValidation: action.payLoad,
          serialValidationStatus: true
        };
      } else {
        return {
          ...state,
          serialValidation: null,
          serialValidationStatus: false,
        };
      }
    case LTE_SERIAL_TEXT_VALUE:
      return {
        ...state,
        serialTextValue: action.payLoad,
      };
    case LTE_BUNDLE_SERIAL:
      return {
        ...state,
        bundleSerialValue: action.payLoad
      };
    case LTE_SERIAL_PACK:
      return {
        ...state,
        serialPackValue: action.payLoad
      };
    case LTE_SIM_SERIAL:
      return {
        ...state,
        simSerialValue: action.payLoad
      };
    case LTE_INPUT_ANALOG_PHONE_SERIAL:
      return {
        ...state,
        analogPhoneSerialValue: action.payLoad
      };
    case LTE_SET_SERIAL_TYPE_BUNDLE:
      allSerialDataValues = [];
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      return {
        ...state,
        bundleSerialData: action.payLoad,
        allSerialData: allSerialDataValues

      };
    case LTE_SET_SERIAL_TYPE_PACK:
      allSerialDataValues = [];
      simSerialDataValues = state.simSerialData;
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      if (simSerialDataValues !== null) {
        allSerialDataValues.push(simSerialDataValues);
      }
      return {
        ...state,
        serialPackData: action.payLoad,
        allSerialData: allSerialDataValues
      };
    case LTE_SET_SERIAL_TYPE_SIM:
      allSerialDataValues = [];
      packSerialDataValues = state.serialPackData;
      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      if (packSerialDataValues !== null) {
        allSerialDataValues.push(packSerialDataValues);
      }
      return {
        ...state,
        simSerialData: action.payLoad,
        allSerialData: allSerialDataValues
      };
    case LTE_SET_SERIAL_TYPE_ANALOG_PHONE:
      allSerialDataValues = [];
      bundleSerialDataValues = state.bundleSerialData;
      packSerialDataValues = state.serialPackData;
      simSerialDataValues = state.simSerialData;

      if (bundleSerialDataValues !== null) {
        allSerialDataValues.push(bundleSerialDataValues);
      }
      if (packSerialDataValues !== null) {
        allSerialDataValues.push(packSerialDataValues);
      }
      if (simSerialDataValues !== null) {
        allSerialDataValues.push(simSerialDataValues);
      }

      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      return {
        ...state,
        analogPhoneSerialData: action.payLoad,
        allSerialData: allSerialDataValues

      };
    case LTE_REST_ANALOG_PHONE_SERIAL_DATA:
      allSerialDataValues = [];
      bundleSerialDataValues = state.bundleSerialData;
      packSerialDataValues = state.serialPackData;
      simSerialDataValues = state.simSerialData;

      if (bundleSerialDataValues !== null) {
        allSerialDataValues.push(bundleSerialDataValues);
      }
      if (packSerialDataValues !== null) {
        allSerialDataValues.push(packSerialDataValues);
      }
      if (simSerialDataValues !== null) {
        allSerialDataValues.push(simSerialDataValues);
      }

      if (action.payLoad !== null) {
        allSerialDataValues.push(action.payLoad);
      }
      return {
        ...state,
        analogPhoneSerialData: null,
        analogPhoneSerialValue: '',
        allSerialData: allSerialDataValues

      };
    case LTE_OTP_NUMBER:
      return {
        ...state,
        cx_otp_number: action.payLoad
      };
    case LTE_EZ_CASH_BALANCE:
      return {
        ...state,
        eZCashInfo: action.payLoad,
        ezCashAccountBalance: action.balance,
        account_no: action.account_no
      };
    case LTE_SET_RELOAD:
      return {
        ...state,
        reload: action.payLoad,
        reloadAmount: action.payLoad.offer_val.CHARGE,
        reloadOfferCode: action.payLoad.offer_val.CODE
      };
    case LTE_FORCE_UPDATE_PROPS:
      return {
        ...state,
        randomValue: action.payLoad
      };

    case LTE_SET_DATA_PACKAGE_CODE:
      return {
        ...state,
        selected_offer_code: action.payLoad
      };

    case LTE_SET_VOICE_PACKAGE_CODE:
      return {
        ...state,
        selected_voice_package_code: action.payLoad
      };

    case GET_LTE_OFFERS_LIST:
      return {
        ...state,
        availableLTEOffersList: action.payLoad
      };
    case GET_LTE_OFFER_DETAIL:
      return {
        ...state,
        selectedLTEOfferDetails: action.payLoad,
        voucherCodeAvailable: false,
        enteredVoucherCode: '',
        validatedVoucherCode: '',
        same_voucher_code_validate:'',
        isValidVoucherCode: false,
        selectedLTEOfferDetailsObject: action.payLoad.detailsObject,

      };
    case SET_LTE_MULTIPLAY_OFFER_DATA:
      return {
        ...state,
        multiplayOfferData: action.payLoad
      };
    case GET_LTE_VOUCHER_CODE_STATUS:
      return {
        ...state,
        voucherCodeAvailable: action.payLoad
      };
    case SET_LTE_VOUCHER_CODE:
      return {
        ...state,
        enteredVoucherCode: action.payLoad
      };
    case SET_LTE_VALIDATED_VOUCHER_CODE:
      return {
        ...state,
        validatedVoucherCode: action.payLoad
      };
    case SET_LTE_VOUCHER_CODE_GROUP_NAME:
      return {
        ...state,
        voucher_group_name: action.payLoad
      };
    case SET_LTE_ANALOG_PHONE_VISIBILITY:
      return {
        ...state,
        analog_phone_visibility: action.payLoad
      };
    case SET_LTE_VOUCHER_OFFER_CODE:
      return {
        ...state,
        voucher_code_validity_offer_code: action.payLoad
      };

    // case VALIDATE_VOUCHER_CODE_TRUE:
    //   return {
    //     ...state,
    //     same_voucher_code_validate: action.payLoad
    //   };
    case SET_LTE_VOUCHER_CODE_VALIDITY:
      if (action.payLoad.success == true) {
        console.log("in valid success true");
        return {
          ...state,
          isValidVoucherCode: true,
          om_campaign_record_no: action.payLoad.om_campaign_record_no,
          reservation_no: action.payLoad.reservation_no,
          availableVaoucherCodeValue: action.payLoad,
          voucherValue: action.payLoad.voucher_value,
          validatedVoucherCode: action.payLoad.voucher_code,
          reference_no: action.payLoad.reference_no
        };
      } else {
        console.log("in valid success false");
        return {
          ...state,
          enteredVoucherCode: '',
          validatedVoucherCode: '',
          isValidVoucherCode: false
        };
      }
    case LTE_RESET_SERIALS:
      return {
        ...state,
        //Sections
        section_3_visible: false,
        sectionProductInfo_validation_status: false,
        sectionCustomerInfo_validation_status: false,
        //Serial validation
        selected_serial_type: null,
        serialValidation: null,
        serialValidationStatus: false,
        serialTextValue: '',
        bundleSerialValue: '',
        serialPackValue: '',
        simSerialValue: '',
        analogPhoneSerialValue: '',
        bundleSerialData: null,
        serialPackData: null,
        simSerialData: null,
        analogPhoneSerialData: null,

        //Input values
        email_input_value: '',
        lte_set_input_MobileNumber: '',
        lte_set_input_landLineNumber: '',
        ez_cash_input_value: '',

        //Offers
        availableLTEOffersList: {
          dropDownData: [],
          selectedValue: '',
          offer_Code: '',
          offerPrice: '',
          defaultIndex: -1,
          offerlist: {
            package_rental: ''
          },
          additionalInfo: {
            audience_group_name: '',
            om_campaign_record_no: '',
            reservation_no: '',
          }
        },
        multiplayOfferData: {
          audience_group_name: '',
          om_campaign_record_no: '',
          reservation_no: '',
        },
        selectedLTEOfferDetails: [],
        selectedLTEOfferDetailsObject: [],
        selected_offer_code: '',

        //Voucher codes
        voucherCodeAvailable: false,
        enteredVoucherCode: '',
        validatedVoucherCode: '',
        same_voucher_code_validate:'',
        isValidVoucherCode: false,
        availableLTEDataPackagesList: {
          dropDownData: [],
          selectedValue: '',
          data_package_code: '',
          defaultIndex: -1,
          dataRental: '',
          data_pakages_value: {
            package_rental: ''
          }
        },

        selectedLTEDataPackageDetails: [],

        //Voice packages
        selectedLTEVoicePackageDetails: [],
        lteVoiceCheckboxData: {
          addVoiceChecked: false,
          analogPhoneChecked: false
        },

        availableLTEVoicePackagesList: {
          dropDownData: [],
          selectedValue: '',
          voice_package_code: '',
          defaultIndex: -1,
          packageList: {
            package_rental: '',
            package_code: ''
          }
        },

        selected_voice_package_code: '',

        //Data sachet / reload
        reload: {
          offer_key: null,
          offer_val: null,
        },
        reloadAmount: '',
        reloadOfferCode: '',

        //Area search
        telcoAreaData: {
          cityNames: [],
          areaCodes: [],
          searchResult: [],
          error: ''
        },
        lteSelectedArea: '',

        //Number selection
        reserved_number_list: {
          reserved_number_list: {},
          number_list: []
        },

        selected_connection_number: { selected_number: "", blocked_id: "" },
        reservation_no: '',
        availableVaoucherCodeValue: '',
        voucherValue: '',
        reference_no: '',


        om_campaign_record_no: '',

        //Billing cycle
        billingCycleDropdownDatalist: [],
        billingCycleDropdownData: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1
        },

        lte_billing_cycle_number: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          dates: [],
          selectedBillingCycleCode: ''
        },

        lte_deposit_value: {
          depositAmount: '',
          depositType: ''
        },

        //eZ cash
        eZCashInfo: null,
        ezCashAccountBalance: 0,
        account_no: '',

        ezCashCheckBalance: {
          availableAmount: '',
          description: '',
          msisdn: '',
          status: ''
        },

        ezCashreverseTransaction: {
          error_description: '',
          tx_serial_no: '',
          status: ''
        },

        ezCashSubmitTransaction: {
        },

        lteDeviceSellingPrice: {
          delta_value: '',
          selling_price: ''
        },

        //KYC related
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,

        //Total Payment
        lteSetTotalPayment: {
          unitPriceValue: '',
          dataplanSachetValue: '',
          outstandingFeeValue: '',
          depositAmount: '',
          totalPayValue: ''
        },

        //ezcash total payment
        eZCashTotalPaymentProps: {
          eZCashtotalPay: '',
          depositAmount: '',
          outstandingFeeValue: '',
          unitPriceAdjustment: '',
          deviceSellingPrice: '',
          serialPackPrice: '',
          simSerialPrice: '',
          bundleSerialPrice: '',
          unitPriceValue: ''
        },
      };

    case LTE_RESET_SERIALS_DATA:
      return {
        ...state,
        //Sections
        section_3_visible: false,
        sectionProductInfo_validation_status: false,
        sectionCustomerInfo_validation_status: false,

        //Offers
        availableLTEOffersList: {
          dropDownData: [],
          selectedValue: '',
          offer_Code: '',
          offerPrice: '',
          defaultIndex: -1,
          offerlist: {
            package_rental: ''
          },
          additionalInfo: {
            audience_group_name: '',
            om_campaign_record_no: '',
            reservation_no: '',
          }
        },
        multiplayOfferData: {
          audience_group_name: '',
          om_campaign_record_no: '',
          reservation_no: '',
        },
        selectedLTEOfferDetails: [],
        selectedLTEOfferDetailsObject: [],
        selected_offer_code: '',

        //Input values
        email_input_value: '',
        lte_set_input_MobileNumber: '',
        lte_set_input_landLineNumber: '',
        ez_cash_input_value: '',

        //Voucher codes
        voucherCodeAvailable: false,
        enteredVoucherCode: '',
        validatedVoucherCode: '',
        same_voucher_code_validate:'',
        isValidVoucherCode: false,
        availableLTEDataPackagesList: {
          dropDownData: [],
          selectedValue: '',
          data_package_code: '',
          defaultIndex: -1,
          dataRental: '',
          data_pakages_value: {
            package_rental: ''
          }
        },

        selectedLTEDataPackageDetails: [],

        //Voice packages
        selectedLTEVoicePackageDetails: [],
        lteVoiceCheckboxData: {
          addVoiceChecked: false,
          analogPhoneChecked: false
        },

        availableLTEVoicePackagesList: {
          dropDownData: [],
          selectedValue: '',
          voice_package_code: '',
          defaultIndex: -1,
          packageList: {
            package_rental: '',
            package_code: ''
          }
        },

        selected_voice_package_code: '',

        //Data sachet / reload
        reload: {
          offer_key: null,
          offer_val: null,
        },
        reloadAmount: '',
        reloadOfferCode: '',

        //Area search
        // telcoAreaData: {
        //   cityNames: [],
        //   areaCodes: [],
        //   searchResult: [],
        //   error: ''
        // },
        // lteSelectedArea: '',

        //Number selection
        // reserved_number_list: {
        //   reserved_number_list: {},
        //   number_list: []
        // },

        // selected_connection_number: { selected_number: "", blocked_id: "" },
        reservation_no: '',
        availableVaoucherCodeValue: '',
        voucherValue: '',
        reference_no: '',

        om_campaign_record_no: '',

        //Billing cycle
        billingCycleDropdownDatalist: [],
        billingCycleDropdownData: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1
        },

        lte_billing_cycle_number: {
          dropDownData: [],
          selectedValue: '',
          defaultIndex: -1,
          dates: [],
          selectedBillingCycleCode: ''
        },

        lte_deposit_value: {
          depositAmount: '',
          depositType: ''
        },

        //eZ cash
        eZCashInfo: null,
        ezCashAccountBalance: 0,
        account_no: '',

        ezCashCheckBalance: {
          availableAmount: '',
          description: '',
          msisdn: '',
          status: ''
        },

        ezCashreverseTransaction: {
          error_description: '',
          tx_serial_no: '',
          status: ''
        },

        ezCashSubmitTransaction: {
        },

        lteDeviceSellingPrice: {
          delta_value: '',
          selling_price: ''
        },
        //KYC related
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,

        //Total Payment
        lteSetTotalPayment: {
          unitPriceValue: '',
          dataplanSachetValue: '',
          outstandingFeeValue: '',
          depositAmount: '',
          totalPayValue: ''
        },

        //ezcash total payment
        eZCashTotalPaymentProps: {
          eZCashtotalPay: '',
          depositAmount: '',
          outstandingFeeValue: '',
          unitPriceAdjustment: '',
          deviceSellingPrice: '',
          serialPackPrice: '',
          simSerialPrice: '',
          bundleSerialPrice: '',
          unitPriceValue: ''
        },
      };

    case LTE_RESET_INDIVIDUAL_SERIAL_DATA:

      allSerialDataValues = [];
      bundleSerialDataValues = state.bundleSerialData;
      packSerialDataValues = state.serialPackData;
      simSerialDataValues = state.simSerialData;

      if (action.payLoad === 'serialPack') {
        console.log('LTE_RESET_INDIVIDUAL_SERIAL_DATA :: serialPack');
        if (simSerialDataValues !== null) {
          allSerialDataValues.push(simSerialDataValues);
        }
        return {
          ...state,
          serialPackValue: '',
          serialPackData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          bundleSerialValue: '',
          analogPhoneSerialValue: '',
          bundleSerialData: null,
          analogPhoneSerialData: null,
          allSerialData: allSerialDataValues
        };

      } else if (action.payLoad === 'simSerial') {
        console.log('LTE_RESET_INDIVIDUAL_SERIAL_DATA :: simSerial');
        if (packSerialDataValues !== null) {
          allSerialDataValues.push(packSerialDataValues);
        }
        return {
          ...state,
          simSerialValue: '',
          simSerialData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          bundleSerialValue: '',
          analogPhoneSerialValue: '',
          bundleSerialData: null,
          analogPhoneSerialData: null,
          allSerialData: allSerialDataValues
        };
      } else {
        console.log('LTE_RESET_INDIVIDUAL_SERIAL_DATA :: bundleSerial');
        return {
          ...state,
          bundleSerialValue: '',
          bundleSerialData: null,
          serialValidation: null,
          serialValidationStatus: false,
          serialTextValue: '',
          serialPackValue: '',
          simSerialValue: '',
          analogPhoneSerialValue: '',
          serialPackData: null,
          simSerialData: null,
          analogPhoneSerialData: null,
          allSerialData: null
        };
      }

    case GET_LTE_DATA_PACKAGES_LIST:
      return {
        ...state,
        availableLTEDataPackagesList: action.payLoad
      };
    case GET_LTE_DATA_PACKAGE_DETAIL:
      return {
        ...state,
        selectedLTEDataPackageDetails: action.payLoad
      };
    case RESET_LTE_DATA_PACKAGES_DATA_RENTAL:
      return {
        ...state,
        availableLTEDataPackagesList: {
          dataRental: ''
        },
      };
    case GET_LTE_VOICE_PACKAGES_LIST:
      return {
        ...state,
        availableLTEVoicePackagesList: action.payLoad
      };
    case GET_LTE_VOICE_PACKAGE_DETAIL:
      return {
        ...state,
        selectedLTEVoicePackageDetails: action.payLoad
      };
    case SET_LTE_TELCO_AREA_DATA:
      return {
        ...state,
        telcoAreaData: action.payLoad
      };
    case SET_LTE_SELECTED_AREA:
      return {
        ...state,
        lteSelectedArea: action.payLoad
      };
    case GET_LTE_PACKAGE_DEPOSITS_SUCCESS:
      return {
        ...state,
        lte_deposit_value: action.payLoad
      };
    case GET_LTE_PACKAGE_DEPOSITS_FAIL:
      return {
        ...state,
        lte_deposit_value: {}
      };
    case SET_LTE_VOICE_CHECKBOX_DATA:
      return {
        ...state,
        lteVoiceCheckboxData: action.payLoad
      };
    case GET_EZCASH_CHECK_BALANCE_SUCCESS:
      return {
        ...state,
        ezCashCheckBalance: action.payLoad
      };
    case GET_EZCASH_CHECK_BALANCE_FAIL:
      return {
        ...state,
        ezCashCheckBalance: {}
      };
    case GET_EZCASH_REVERSE_TRANSACTION_SUCCESS:
      return {
        ...state,
        ezCashreverseTransaction: action.payLoad
      };
    case GET_EZCASH_REVERSE_TRANSACTION_FAIL:
      return {
        ...state,
        ezCashreverseTransaction: {}
      };
    case GET_EZCASH_SUBMIT_TRANSACTION_SUCCESS:
      return {
        ...state,
        ezCashSubmitTransaction: action.payLoad
      };
    case GET_EZCASH_SUBMIT_TRANSACTION_FAIL:
      return {
        ...state,
        ezCashSubmitTransaction: {}
      };
    case GET_LTE_DEVICE_SELLING_PRICE_SUCCESS:
      return {
        ...state,
        lteDeviceSellingPrice: action.payLoad
      };
    case GET_LTE_DEVICE_SELLING_PRICE_FAIL:
      return {
        ...state,
        lteDeviceSellingPrice: {}
      };
    case LTE_SET_INPUT_LANLINE_NUMBER:
      return {
        ...state,
        lte_set_input_landLineNumber: action.payLoad
      };
    case LTE_SET_INPUT_MOBILE_NUMBER:
      return {
        ...state,
        lte_set_input_MobileNumber: action.payLoad
      };
    case LTE_RESERVE_NUMBER_POOL:
      console.log("In lteREserve: \n");
      console.log(action.payLoad.reserved_number_list);
      if (Object.keys(action.payLoad.reserved_number_list).length == 0 && action.payLoad.number_list.length == 0) {
        return {
          ...state,
          reserved_number_list: {
            reserved_number_list: action.payLoad.reserved_number_list,
            number_list: action.payLoad.number_list
          }
        };
      }
      return {
        ...state,
        reserved_number_list: {
          reserved_number_list: { ...state.reserved_number_list.reserved_number_list, ...action.payLoad.reserved_number_list },
          number_list: action.payLoad.number_list
        }
      };
    case SET_SELECTED_CONNECTION_NUMBER:
      return {
        ...state,
        selected_connection_number: action.payLoad
      };
    case RESET_SELECTED_CONNECTION_NUMBER:
      return {
        ...state,
        selected_connection_number: { selected_number: "", blocked_id: "" }
      };
    case LTE_SET_TOTAL_PAYMENT:
      return {
        ...state,
        lteSetTotalPayment: action.payLoad
      };
    case LTE_BILLING_CYCLE_NUMBER:
      return {
        ...state,
        lte_billing_cycle_number: action.payLoad
      };

    //KYC related
    case LTE_SET_KYC_CHECK_BOX_POB_DIFFERENT:
      return {
        ...state,
        opt_addressDifferent: action.payLoad
      };
    case LTE_SET_KYC_CHECK_BOX_FACE_DIFFERENT:
      return {
        ...state,
        opt_customerFaceNotClear: action.payLoad
      };
    case LTE_SET_KYC_CHECK_BOX_POI_DIFFERENT:
      return {
        ...state,
        opt_installationAddressDeferent: action.payLoad
      };

    case LTE_RESET_KYC_CHECK_BOX_ALL:
      return {
        ...state,
        opt_addressDifferent: false,
        opt_installationAddressDeferent: false,
        opt_customerFaceNotClear: false,
      };

    case LTE_SET_EZCASH_TOTAL_PAYMENT:
      return {
        ...state,
        eZCashTotalPaymentProps: action.payLoad
      };

    case LTE_RESET_EZCASH_BALANCE:
      return {
        ...state,
        ezCashAccountBalance: 0,
        account_no: ''
      };

    case LTE_RESET_VOUCHER_VALUE:
      return {
        ...state,
        voucherValue: ''
      };
    case LTE_RESET_VALIDATE_VOUCHER_CODE_STATUS:
      return {
        ...state,
        isValidVoucherCode: false
      };
    case RESET_LTE_DATA:
      return INITIAL_STATE;

    default:
      return state;
  }
};