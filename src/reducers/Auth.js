import { 
  GET_PIN, 
  GET_API_LOADING, 
  GET_ALLOWED_SERVICES, 
  GET_TILE_DATA, 
  GET_DOC_99_URL,
  GET_EZCASH_URL,
  GET_EZCASH_SCHEME_URL,
  AUTH_LOADING_INDICATOR,
  AUTH_SET_API_LOADING,
  AUTH_GETS_START_API_SUCCESS,
  AUTH_GETS_START_API_FAIL,
  AUTH_RESET_ALL,
  SET_RETAILER,
  SET_IS_CFSS,
  GET_CONFIGURATION
} from '../actions/types';

const INITIAL_STATE = {
  pin: '',
  api_loading: false,
  auth_api_loading: false,
  auth_get_start_data: {},
  allowedServices:{},
  tileData: [],
  doc990URL:'',
  isretailer: false,
  isCFSSAgent: false,
  ezCashSchemUrl:'',
  eZcashUrl:'',
  getConfiguration: {
    config: {
      enable_dealer_collection: false,
      enable_reload: false
    },
    rapid_ez_account: {
      rapid_ez_error: '',
      rapid_ez_availability: true
    },
    ez_cash_account: {
      ezCash_error: '',
      ez_cash_account_availability: true
    },
    agreement_document: {
      PREPAID: '',
      POSTPAID: ''
    }
  }
};

//GET_ALLOWED_SERVICES
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTH_LOADING_INDICATOR:
    case AUTH_SET_API_LOADING:
      return {
        ...state,
        auth_api_loading: action.payLoad
      };
    case GET_PIN:
      return {
        ...state,
        pin: action.payLoad
      };
    case GET_API_LOADING:
      return {
        ...state,
        api_loading: action.payLoad
      };
    case GET_DOC_99_URL:
      return {
        ...state,
        doc990URL: action.payLoad
      };
    case GET_EZCASH_URL:
      return {
        ...state,
        eZcashUrl: action.payLoad
      };
    case GET_EZCASH_SCHEME_URL:
      return {
        ...state,
        ezCashSchemUrl: action.payLoad
      };
    case GET_ALLOWED_SERVICES:
      return {
        ...state,
        allowedServices: action.payLoad
      };
    case SET_RETAILER:
      return {
        ...state,
        isretailer: action.payLoad
      };
    case SET_IS_CFSS:
      return {
        ...state,
        ...action.payLoad
      };
    case GET_TILE_DATA:
      return {
        ...state,
        tileData: action.payLoad
      };
    case AUTH_GETS_START_API_SUCCESS:
      return {
        ...state,
        auth_api_loading: false,
        auth_get_start_data : action.payLoad
      };
    case AUTH_GETS_START_API_FAIL:
      return {
        ...state,
        auth_api_loading: false,
        auth_get_start_data : {}
      };
    case GET_CONFIGURATION:
      return {
        ...state,
        getConfiguration: action.status ? action.payload : state.getConfiguration 
      };
    case 'RESET_AUTH':
      return INITIAL_STATE;
    case AUTH_RESET_ALL:
      return INITIAL_STATE;
    default:
      return state;
  }
};