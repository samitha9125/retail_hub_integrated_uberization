import {
    ACCESSORY_SALE_DETAIL,
    ACCESSORY_SALES_EZ_CASH_PIN,
    ACCESSORY_SALES_RESET,
} from '../actions/types';

const INITIAL_STATE = {
    accessory_sale_detail: {},
    accessory_sales_get_ez_cash_pin: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case ACCESSORY_SALE_DETAIL:
            return {
                ...state,
                accessory_sale_detail: action.payLoad
            };

        case ACCESSORY_SALES_EZ_CASH_PIN:
            return {
                ...state,
                accessory_sales_get_ez_cash_pin: action.payLoad
            };

        case ACCESSORY_SALES_RESET:
            return INITIAL_STATE;

        default:
            return state;
    }
};