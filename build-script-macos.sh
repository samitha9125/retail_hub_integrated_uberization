#! /bin/sh
# Developed by Nipuna Hasitha Herath

echo "====================================================".
echo "==Excuting Build script to config Dialog Sales App==".
echo "====================================================".

#work plan 

#check version
echo "node version - " $(node -v)
echo $(react-native -v)

echo "Removing node modules folder...".
rm -rf "$PWD"/node_modules

echo "Removal Done. Installing node modules...".
npm install

echo "Config Done. Happy Coding...".
echo "Run 'react-native run-android' to run the application".

#done  :) happy coding