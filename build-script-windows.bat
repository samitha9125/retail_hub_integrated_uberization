@ECHO off
REM Developed by Damith Nuwan Sampath

echo "********************************************************".
echo "********************************************************".
echo "*** Excuting Build script to config Dialog Sales App ***".
echo "********************************************************".
echo "********************************************************".

REM work plan 
set NODE_VERSION= null
set RN_VERSION = null

REM NODE_VERSION = echo call node -v
REM RN_VERSION = echo call react-native -v

REM check version
echo "node version - ".%NODE_VERSION%
call node -v
echo "react-native version - ".%RN_VERSION%
call react-native -v

echo "Removing node modules folder...".
RMDIR ".\node_modules"

echo "Removal Done. Installing node modules...".
call npm install

echo "Config Done. Happy Coding...".
echo "Run 'react-native run-android' to run the application".

REM done  :) happy coding